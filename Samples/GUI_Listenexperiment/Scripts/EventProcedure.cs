﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class EventProcedure : MonoBehaviour,IEventProcedure
{
    protected UnityEvent nextEvent=>controller.nextEvent;
    [SerializeField][HideInInspector] protected EventProcedureController controller;
    public string Name => name;
    public int Repetitions => repetitions;

    [SerializeField] protected string name;
    [SerializeField] protected int repetitions;

    
    public virtual void OnStart(EventProcedureController controller)
    {
        throw new NotImplementedException();
    }

    public virtual void OnEnd()
    {
        throw new NotImplementedException();
    }
}
