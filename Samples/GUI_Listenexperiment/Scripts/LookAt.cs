﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAt : MonoBehaviour
{
    public Transform lookAtTransform;

    private void Update()
    {
        transform.LookAt(lookAtTransform);
        transform.Rotate(Vector3.up, 180f);
    }
}
