﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UI;

public class RadioButtonController : MonoBehaviour
{

    public Toggle[] toggles;
    public int currentStatus;

    public int Result
    {
        get
        {
            for (var i = 0; i < toggles.Length; i++)
            {
                if (toggles[i].isOn)
                    return i;
            }

            return -1;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        currentStatus = -1;
        foreach (var toggle in toggles)
        {
            toggle.isOn = false;
        }
    }


    
}
