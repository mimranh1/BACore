﻿using UnityEngine.Events;

public interface IEventProcedure
{
    string Name { get; }
    int Repetitions { get; }
    void OnStart(EventProcedureController controller);
    void OnEnd();
}
