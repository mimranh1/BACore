﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(PsychoController))]
public class PhychoControllerEditor : Editor
{
    PsychoController psy;

    public void OnEnable()
    {
        psy = (PsychoController)target;
        
    }

    public void sdasdOnInspectorGUI()
    {
        
        base.OnInspectorGUI();
        /*
        GUILayout.BeginHorizontal();
        GUILayout.Label("Repetitions", GUILayout.MinWidth(120));
        psy.Repetitions = EditorGUILayout.IntField(psy.Repetitions, GUILayout.Width(150));
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Trail Sound Source", GUILayout.MinWidth(120));
        psy.TrailSourceId = EditorGUILayout.Popup("", psy.TrailSourceId, psy.AudioNameStrings, GUILayout.Width(150));
        GUILayout.EndHorizontal();
        
        GUILayout.BeginHorizontal();
        GUILayout.Label("Test Sound Source", GUILayout.MinWidth(120));
        psy.TestSourceId = EditorGUILayout.Popup("", psy.TestSourceId, psy.AudioNameStrings, GUILayout.Width(150));
        GUILayout.EndHorizontal();
        */

        base.OnInspectorGUI();

    }
}