﻿using System;
using UnityEngine;
using UnityEngine.Events;


[Serializable]
public class EP_Feedback : EventProcedure
{
    [SerializeField] private GameObject feedbackCanvas;

    [SerializeField]
    private RadioButtonController[] radio;
    
    public override void OnStart(EventProcedureController controller)
    {
        this.controller = controller;
        feedbackCanvas.SetActive(true);
    }

    public void Submit()
    {
        for (var i = 0; i < radio.Length; i++)
        {
            var result = radio[i].Result;
            controller.Log($"Question {i + 1} Given {result}");
        }
        nextEvent.Invoke();
        OnEnd();
    }

    public override void OnEnd()
    {
        feedbackCanvas.SetActive(false);
    }


}
