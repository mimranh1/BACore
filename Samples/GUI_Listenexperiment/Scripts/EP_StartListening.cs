﻿using System;
using BA.BACore;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


[Serializable]
public class EP_StartListening : EventProcedure
{
    
    [SerializeField] private bool logResults;
    [SerializeField] private GameObject playSoundCanvas;
    [SerializeField] private GameObject questionCanvas;
    [SerializeField] private Text[] statusTexts;
    
    [SerializeField] private SoundSignalController soundController;
    [SerializeField] private LatinSquareBalance codeBalanced;

    private UnityEvent soundStopped;
    
    private int counter;
    
    public override void OnStart(EventProcedureController controller)
    {
        this.controller = controller;
     
        soundStopped = new UnityEvent();
        counter = 0;
        UpdateText();
        playSoundCanvas.SetActive(true);
        soundStopped.AddListener(StartResultsCanvas);
    }
    
    public void PressStartSoundButton()
    {
        playSoundCanvas.SetActive(false);
        soundController.StartSpecialSound(codeBalanced.GetBalancedResult(controller.participantNumber,counter), null, soundStopped);
    }

    private void StartResultsCanvas()
    {
        questionCanvas.SetActive(true);
        
    }

    public void PressResult(int trajectoryIndex)
    {
        questionCanvas.SetActive(false);
        var currentTrajectory = soundController.CurrentTrajectoryName;
        if (logResults)
            controller.Log(codeBalanced.GetBalancedResult(controller.participantNumber, counter) + " TrajIndex: " +
                           currentTrajectory + " TrajChose: " + trajectoryIndex);

        counter++;
        UpdateText();
        if (counter < codeBalanced.Length)
        {
            // start next Round
            playSoundCanvas.SetActive(true);
        }
        else
        {
            nextEvent.Invoke();
            OnEnd();
        }
    }

    private void UpdateText()
    {
        if (statusTexts == null) return;
        
        foreach (var statusText in statusTexts)
        {
            if (statusText!=null)
                statusText.text = counter + 1 + " / " + codeBalanced.Length;
        }
    }

    public override void OnEnd()
    {
        if (controller != null)
            nextEvent.RemoveListener(StartResultsCanvas);
        soundController.sourceMover.ResetPositionAndSound();
        questionCanvas.SetActive(false);
        playSoundCanvas.SetActive(false);
    }
    
    
}
