﻿
using System;
using System.Runtime.InteropServices.WindowsRuntime;
using BA.BACore;
using UnityEngine;

[Serializable]
[ExecuteInEditMode]
public class CodeList : MonoBehaviour
{
    [SerializeField] private string[] prefixesList;
    [SerializeField] private string[] postfixesList;
    [SerializeField][ReadOnly] public string[] codeList;
    
    public int Length => codeList.Length;
    
    private void Update()
    {
        if ((postfixesList == null || postfixesList.Length == 0) && (prefixesList == null || prefixesList.Length == 0))
            return;
        if (postfixesList == null || postfixesList.Length == 0)
        {
            codeList = new string[prefixesList.Length];
            for (var i=0;i< prefixesList.Length;i++)
            {
                codeList[i] = prefixesList[i];
            }
        }
        else if (prefixesList == null || prefixesList.Length == 0)
        {
            codeList = new string[postfixesList.Length];
            for (var i=0;i< postfixesList.Length;i++)
            {
                codeList[i] = postfixesList[i];
            }
        }
        else
        {
            codeList = new string[prefixesList.Length * postfixesList.Length];
            var index = 0;
            foreach (var post in postfixesList)
            {
                foreach (var pre in prefixesList)
                {
                    codeList[index] = pre + post;
                    index++;
                }
            }
        }
    }
}
