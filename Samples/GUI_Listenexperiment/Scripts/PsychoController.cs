﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PsychoController : MonoBehaviour
{
    public int Repetitions = 12;
    private int _repetitionsCounter = 0;
    public UnityEvent UpdateImpulseResponse;
    //public VAUSignalSourceAudioFile SoundSource;
     public GameObject IntroText; 
     public ButtonController StartTrailButton;
    public ButtonController StartButton;
     public Text ShowNumbersText;
     public Text Heading;

     public GameObject[] QuaderGameObjects = new GameObject[3];
      public ButtonController[] Buttons = new ButtonController[9];
     public GameObject SliderGameObject;
     public Slider Slider;
     private float SliderTime;
     public int[] NumbersInts;
     public int countClicks;

     public AudioSource TrailAudioSource;
     public AudioSource TestAudioSource;
     private int ParticipantId;

    private int TestId = 0;

    private IEnumerator coroutine;
    private bool _buttonsActive;
    private bool isRoutineRunning;
    private string TimeString;
    private List<string> _lines = new List<string>();
    private float lastTime = 0f;
    private bool isClickEnabled = false;
    private string _headingTest;

    public string[] AudioNameStrings
    {
        get
        {
            return new []{"1 Speech G", "2 Speech B", "3 Speech", "4 Pink Noise"};
        }
    }


    public bool ButtonsActive
    {
        get { return _buttonsActive; }
        set
        {
            _buttonsActive = value;
            SetButtonsActive(_buttonsActive);
        }
    }

    void Start()
    {
        SliderTime = -1f;
        DeactivateQuader();
        TimeString = System.DateTime.Now.ToString("yyyyMMdd-HHmmss");       

        // Start function WaitAndPrint as a coroutine.
        if (SliderGameObject != null)
            SliderGameObject.SetActive(false);
        SetButtonsTextActive(true);
        ButtonsActive = false;
        StartButton.ObjectEnabled = true;
        StartTrailButton.ObjectEnabled = true;
        Heading.text = "";
        //StopSoundSources();
        //SetLoopForSecondarySources();

    }

    void Update()
    {
        // Update Slider
        if(SliderTime > 0)
        {
            var deltaTime = (Time.time - SliderTime);
            if (deltaTime > 10f)
            {
                if (SliderGameObject != null)
                    SliderGameObject.SetActive(false);
                _lines.Add(Time.time + " start " + _headingTest);

                // activate  
                SetButtonsActive(true);
                SetButtonsTextActive(true);
                isClickEnabled = true;
                SliderTime = -1f;

                var coroutineWait = Wait20Sec();
                StartCoroutine(coroutineWait);
                Wait20Sec();
            }
            else
            {
                if (SliderGameObject != null)
                    Slider.value = deltaTime / 10f;
            }
            
        }

        if (!isRoutineRunning & Input.GetKeyDown("m"))
        {
            StartTestRoutine();
        }
    }

    public void Log ()
    {
        _lines.Add(Time.time + " log " );
    }

    public void StartTestRoutine()
    {
        NumbersInts = GenerateRandomVector();
        SetNumbersToButtons(GenerateRandomVector());
        SetButtonsTextActive(true);
        ButtonsActive = false;

        IntroText.SetActive(false);
        SetButtonsTextActive(true);
        countClicks = 0;
        isRoutineRunning = true;
        coroutine = WaitAndPrint(NumbersInts);
        StartButton.ObjectEnabled = false;
        StartTrailButton.ObjectEnabled = false;
        _repetitionsCounter++;
        Heading.text = _headingTest + _repetitionsCounter.ToString() + "/" + Repetitions.ToString();
        StartCoroutine(coroutine);
    }
    
    /*
    public void StartSoundSource(int i)
    {
        //SoundSource.StartPlaying();
        //UpdateImpulseResponse.Invoke();
        AudioSources[i].Play();
        playingSourceId = i;
    }

    public void StopSoundSources()
    {
        if (playingSourceId != -1) 
            AudioSources[playingSourceId].Stop();
        playingSourceId = -1;
    }
    */

    public void ButtonClicked( ButtonController button)
    {
        if (!StartButton.ObjectEnabled)
        {
            if (isClickEnabled && button.TextEnabled)
            {
                button.TextEnabled = false;
                countClicks++;
                _lines.Add(Time.time + " buttonPressed " + button.Number);
                if (countClicks == 9)
                {
                    SetButtonsActive(false);
                    _lines.Add(Time.time + " TestEnd ClickedAll");
                    //WriteDataFile();
                    isRoutineRunning = false;
                    isClickEnabled = false;
                    Heading.text = "";
                    if (_repetitionsCounter < Repetitions)
                    {
                        // Start direct new Game
                        StartTestRoutine();
                        
                    }
                    else
                    {
                        // wait for press Start Button
                        StartButton.ObjectEnabled = true;
                        StartTrailButton.ObjectEnabled = true;
                        IntroText.SetActive(true);
                        //SoundSource.StopPlaying();
                        WriteDataFile();
                        _lines.Clear();
                        _repetitionsCounter = 0;
                        if (_headingTest == "Test ")
                            TestAudioSource.Stop();
                        if (_headingTest == "Trial ")
                            TrailAudioSource.Stop();
                    }
                    // Do Something
                    
                    //ReceiverObject.SendFilterToVa();
                }
            }
        }
        else
        {
            if (button == StartButton)
            {
                _headingTest = "Test ";
                Repetitions = 1;
                _lines.Add("ParticipantID " + ParticipantId);
                _lines.Add("Test");
                TestAudioSource.Play(); 
                _lines.Add(Time.time + " StartSourceFile " + TestAudioSource.clip.name);
                StartTestRoutine();
            }
            if (button == StartTrailButton)
            {
                _headingTest = "Trial ";
                Repetitions = 1;
                _lines.Add("ParticipantID " + ParticipantId);
                _lines.Add("PracticeTrail");
                TrailAudioSource.Play(); 
                _lines.Add(Time.time + " StartSourceFile " + TrailAudioSource.clip.name);
                StartTestRoutine();
            }
        }
    }
    private IEnumerator Wait20Sec()
    {
        yield return new WaitForSeconds(20f);
        if (isClickEnabled && !StartButton.ObjectEnabled)
        {
            SetButtonsActive(false);
            _lines.Add(Time.time + " TestEnd Timeout");
            //WriteDataFile();
            isRoutineRunning = false;
            isClickEnabled = false;
            Heading.text = "";
            if (_repetitionsCounter < Repetitions)
            {
                // Start direct new Game
                StartTestRoutine();
            }
            else
            {
                // wait for press Start Button
                StartButton.ObjectEnabled = true;
                StartTrailButton.ObjectEnabled = true;
                IntroText.SetActive(true);
                //SoundSource.StopPlaying();
                WriteDataFile();
                _lines.Clear();
                _repetitionsCounter = 0;
                if (_headingTest == "Test ")
                    TestAudioSource.Stop();
                if (_headingTest == "Trial ")
                    TrailAudioSource.Stop();
            }
        }
    }

    private IEnumerator WaitAndPrint(int[] numberInts)
    {
        // Start Countdown
        TestId++;
        foreach (var quader in QuaderGameObjects)
        {
            _lines.Add(Time.time + " countdown ");
            quader.gameObject.SetActive(true);
            yield return new WaitForSeconds(1f);
        }
        DeactivateQuader();

        // show Numbers
        foreach (var number in numberInts)
        {
            _lines.Add(Time.time + " Show " + number);
            yield return new WaitForSeconds(0.7f);
            ShowNumbersText.text = number.ToString();
            yield return new WaitForSeconds(0.3f);
            ShowNumbersText.text = "";
        }

        // wait 10 seconds
        SliderTime = Time.time;
        _lines.Add(SliderTime + " wait10s ");
        
        if (SliderGameObject != null)
            SliderGameObject.SetActive(true);
        
    }

    private int[] GenerateRandomVector()
    {
        var Numbers = new List<int>();
        for (var i = 1; i < 10; i++)
            Numbers.Add(i);
        return Shuffle(Numbers).ToArray();
    }

    public List<T> Shuffle<T>( List<T> ts) {
        var count = ts.Count;
        var last = count - 1;
        for (var i = 0; i < last; ++i) {
            var r = UnityEngine.Random.Range(i, count);
            var tmp = ts[i];
            ts[i] = ts[r];
            ts[r] = tmp;
        }
        return ts;
    }

    private void WriteDataFile()
    {
        string path = @"" + TimeString + "-Participant" + ParticipantId + ".txt";
        //const string path = @"Log\ListeningTest"+TestId+".txt";
        File.WriteAllLines(path, _lines.ToArray());
    }

    private void DeactivateQuader()
    {
        foreach (var quader in QuaderGameObjects)
        {
            quader.gameObject.SetActive(false);
        }
    }

    private void SetNumbersToButtons(int[] numberInts)
    {
        if ((numberInts.Length != 9) || (Buttons.Length != 9))
            Debug.LogError("wrong Length.");

        for(var i = 0; i < 9; i++)
        {
            Buttons[i].Number = numberInts[i];
        }
    }

    private void SetButtonsActive(bool enable)
    {
        for(var i = 0; i < 9; i++)
        {
            Buttons[i].gameObject.SetActive(enable);
        }
    }

    private void SetButtonsTextActive(bool enable)
    {
        for(var i = 0; i < 9; i++)
        {
            Buttons[i].TextEnabled = enable;
        }
    }
}
