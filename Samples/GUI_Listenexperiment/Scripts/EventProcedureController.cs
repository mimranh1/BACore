﻿using System;
using System.Collections.Generic;
using System.IO;
using BA.BACore;
using UnityEngine;
using UnityEngine.Events;


public class EventProcedureController : MonoBehaviour
{
    public int participantNumber;

    [SerializeField] private bool logInUnity;
    [SerializeField] private bool logInFile;
    
    [SerializeReference][DisplayProperties]
    public EventProcedure[] events;
    [SerializeReference][DisplayProperties]
    public EventProcedure debugEvent;

    private int currentEventId;
    [HideInInspector] public UnityEvent nextEvent;

    private string timeString;
    private StreamWriter file;


    private void Start()
    {
        file?.Close();

        CallOnEndEvents();
        timeString = DateTime.Now.ToString("yyyyMMdd-HHmmss");
        var path = @"" + timeString + "-Participant" + participantNumber + ".txt";
        file = new StreamWriter(path, true) {AutoFlush = true};



        currentEventId = 0;
        nextEvent = new UnityEvent();
        nextEvent.AddListener(NextStep);
        events[currentEventId].OnStart(this);
    }

    private void OnDisable()
    {
        file?.Close();
    }

    private void NextStep()
    {
        events[currentEventId].OnEnd();
        currentEventId++;
        if (currentEventId < events.Length)
            events[currentEventId].OnStart(this);
        else
        {
            Reset();
        }
    }

    public void Skip()
    {
        NextStep();
    }

    public void StartDebug()
    {
        CallOnEndEvents();
        debugEvent.OnStart(this);
    }

    private void CallOnEndEvents()
    {
        foreach (var eventProcedure in events)
        {
            eventProcedure.OnEnd();
        }
        debugEvent.OnEnd();
    }

    public void Reset()
    {
        CallOnEndEvents();
        Start();
    }

    public void Log(string logMessage)
    {
        if (logInUnity)
            Debug.Log(logMessage);
        
        if (logInFile)
        {
            file.WriteLine(DateTime.Now.ToString("HHmmss") + " " + logMessage);
        }    }

    public void StartCertainEvent(int index)
    {
        events[currentEventId].OnEnd();
        currentEventId = index;
        if (currentEventId < events.Length)
            events[currentEventId].OnStart(this);
        else
        {
            Reset();
        }
        
    }
}
