
%%
ccx;
% clear;
% clc;
% close all;
folder='OutdoorDnt_Scene1_L12m';
% folder='OutdoorDnt_LibaryL12m_dirWall';
mkdir(folder);
load([folder '\DnT_Verification.mat'], 'DnT_ref', 'dnt_tau', 'dnt_tau_diff')
dnt_time_Order_Pos=zeros(6,5,31);
dnt_time_pos=zeros(5,31);
order =-1;
%%
for iPos=0:3
    i=iPos;
    for ii=0:4
        if order<0
            fname = ['./../.Data/AcBuildingAcoustics_Libary_dirWall/BuidlingsAcoustics_DnT_Libary', num2str(iPos), num2str(ii) '.json'];
        else
            fname = ['./../.Data/AcBuildingAcoustics/BuidlingsAcoustics_DnT_Pos_Order', num2str(order), num2str(iPos), '.json'];
        end
        [newApp{ii+1}] = ReadJSONUnity(fname);
    end
    fs = 44100;
    f3rd = [0, 8, 10, 12.5, 16, 20, 25, 31.5, 40, 50, 63, 80, 100, 125, 160, 200, 250, 315, 400, 500, 630, 800, 1000, 1250, 1600, 2000, 2500, 3150, 4000, 5000, 6300, 8000, 10000, 12500, 16000, 20000, 25000];
    
    filterLen = length(newApp{1}.brir0.irLeft.time);
    f = [1:filterLen] ./ filterLen .* fs;
    
    rhoc0 = 1.29 * 340;
    
    %%
    
    figure;
    % irFreqq = newApp.receiverRoom.dataDirect.reverberationData.reverberationFactorComplexFreq;
    irFreqq = newApp{1}.receiverRoom.dataDirect.irOutComplexFreq(1:2:end, :)' .* newApp{1}.sourceRoom0.dataDirect.irOutComplexFreq';
    % irFreqq = newApp{1}.brir0.irLeft.freqComplex;
    % irFreqq = newApp.receiverRoom.dataDirect.reverberationData.reverberationCutTime';
    % irFreqq = newApp.receiverRoom.dataDirect.roomHrtfData.hrtfComplexFreq;
    semilogx(f, 10.*log10(abs(irFreqq)));
    hold on;
    
    [ir3rdOctave, freqsOctave] = H_GetMeanValue(abs(irFreqq));
    semilogx(freqsOctave, 10.*log10(abs((ir3rdOctave)')));
    H_SetFreqPlot
    %%
    figure;
    Pos=[];
    factor=[];
    for i=1:length(newApp)
        %         figure;
        [incAngleDeg(i,:),factor(i), coIncF] = Plot_Geometry(newApp{i}, i,[4 0 0]);
        
    end
    view([0 0])
    saveas(gcf, [folder '\Pos_iPos' num2str(iPos), '.png'])
    saveas(gcf, [folder '\Pos_iPos' num2str(iPos), '.fig'])
    %%
    irFreqAll = [];
    for i = 1:length(newApp)
        irFreqq =newApp{i}.AcBuildingAcoustics.sqrtRho0c.*  sum(newApp{i}.receiverRoom.dataDirect.irOutComplexFreq(1:2:end, :).*newApp{i}.sourceRoom0.dataDirect.irOutComplexFreq, 1);
        dist = mean(newApp{i}.sourceRoom0.dataDirect.roomFiData.sourceDistance);
        %         factor = (1 ./ dist) / 7;
        %         % factor = (1./ dist)/1;
        %
        %         factor = (newApp{i}.AcBuildingAcoustics.l2Ref2MEnergy)*2.2;
        
        irFreqAll = [abs(irFreqq) ./ factor(i); irFreqAll];
    end
    vR = newApp{i}.receiverRoom.dataDirect.roomStatsData.volume;
    Tr = newApp{i}.receiverRoom.dataDirect.roomStatsData.reverberationTime;
    Sd = newApp{i}.sourceRoom0.dataDirect.roomTauDirectDataDiffuse.partitionsArea;
    i = 1
    tauDdDiff = newApp{i}.sourceRoom0.dataDirect.roomTauDirectDataDiffuse.tauDiff.floats;
    tauDd45 = newApp{i}.sourceRoom0.dataDirect.roomTauDirectDataDiffuse.tauDir45(1).floats;
    tauSum = [tauDd45];
    
    [~, dnt_time, dnt_tau] = PlotDnt(irFreqAll, tauSum, f3rd, Tr, vR, Sd);
    dnt_time=mean(dnt_time,2);
    dnt_time_pos(iPos+1,:)=dnt_time;
    xlim([50, 5000])
    ylim([0, 50])
    title('dd')
    %
    %
    % ignoreDiff=1;
    Dnt_tau_label = 'D_n_T (\tau_4_5_°)';
    % ylimm=[10 60];
    % Plot_DnTWithBoxplotsNew1(f3rd(6:36), dnt_time, dnt_tau(6:36), Dnt_tau_label, ylimm, ignoreDiff)
    
    %%
    figure;
    for i=1:3
        irSource= ((newApp{1}.sourceRoom0.dataDirect.reverberationData.reverberationAll(i).floats));
        allTau= newApp{1}.sourceRoom0.dataDirect.reverberationData.AllTaus.floats;
        semilogx(f,(-10.*log10(abs(irSource(1:2:end)+1j.*irSource(2:2:end)).')));
        hold on; grid on;
        H_SetFreqPlot;
    end
    semilogx(f,(-10.*log10(abs(allTau(1:2:end)+1j.*allTau(2:2:end)).')));
    %%
    figure;
    for i=1:3
        irSource= ((newApp{1}.sourceRoom0.dataDirect.reverberationData.reverberationAll(i).floats));
        plot(ifftshift(real(ifft(abs(irSource(1:2:end)+1j.*irSource(2:2:end)).'))));
        hold on; grid on;
    end
    
    %     saveas(gcf, [folder '\ISM_Pos_order', num2str(order), '.png'])
    %     saveas(gcf, [folder '\ISM_Pos_order', num2str(order), '.fig'])
    
    
    %%
    figure;
    irSource=[];
    for i=1:length(newApp{1}.sourceRoom0.dataDirect.reverberationData.AllTaus)
        irSource(i,:)= ((newApp{1}.sourceRoom0.dataDirect.reverberationData.AllTaus(i).floats(1:2:end)))+1j.*newApp{1}.sourceRoom0.dataDirect.reverberationData.AllTaus(i).floats(2:2:end);
    end
    %
    subplot(2,1,1)
    plot(real(ifft(irSource.')));hold on;
    semilogx(real(ifft(sum(irSource,1))));
    hold on; grid on;
    subplot(2,1,2)
    semilogx(f,-10.*log10(abs(irSource)));hold on;
    semilogx(f,-10.*log10(abs(sum(irSource,1))));
    H_SetFreqPlot
    
    saveas(gcf, [folder '\ISM_Pos_order', num2str(order), '.png'])
    saveas(gcf, [folder '\ISM_Pos_order', num2str(order), '.fig'])
    
    %     %%
    %      figure;
    %     plot(ifftshift(real(ifft((abs(irSource).*exp(1j.*angle(irSource(5,:)))).'))));
    %     hold on; grid on;
    %    %%
    %     figure;
    %     semilogx(f,phase(irSource));
    %     H_SetFreqPlot
    %%
    figure;
    % [Rw_pSq_Mean_Davy, ref_curve_pSq_Mean_Davy] = H_calcRwFromR(DnT_pSq_Mean_Davy);
    plot(mean(DnT_ref, 2), '-*r', 'DisplayName', ['D_n_T (Extended Approach)']);
    hold on;
    for i = 1:size(dnt_time, 2)
        plot(dnt_time(:, i), '--o', 'DisplayName', ['D_n_T (Pos ', num2str(iPos), ')' ]);
    end
    %     plot(mean(dnt_tau(6:36), 2), '-*b', 'DisplayName', ['D_n_T (Tau)']);
    
    % legend('Location', 'SE','NumColumns', 2)
    legend('Location', 'NW')
    
    ylabel('D_n_T (dB)')
    xlabel('Frequency (Hz)')
    %     title(['order ' num2str(order)]);
    ax = gca;
    xticks([1:31])
    % xticks([5:2:35])
    ax.XTickLabel = f3rd(6:36);
    xtickLabels1 = ax.XTickLabel;
    xtickLabels = [];
    for i = 1:size(xtickLabels1, 1)
        xtickLabels{end+1} = xtickLabels1(i, :);
    end
    hiddenId = [-2,-1,0,2, 3, 5, 6, 8, 9, 10, 12, 13, 15, 16, 18, 19, 20,22,23,25 ,26] + 4;
    for i = hiddenId
        xtickLabels{i} = [''];
    end
    xtickLabels{14+4} = '1k';
    xtickLabels{17+4} = '2k';
    xtickLabels{21+4} = '5k';
    xtickLabels{24+4} = '10k';
    xtickLabels{27+4} = '20k';
    ax.XTickLabel = xtickLabels;
    xlim([0.5, 31.5])
    grid on;
    ylim([0, 80])
    set(gca, 'FontName', 'Times')
    
    %%
    %     saveas(gcf, ['DnT_Scene2_Pos.png'])
    %     saveas(gcf, ['DnT_Scene2_Pos.fig'])
    %%
    saveas(gcf, [folder '\DnT_Pos_iPos' num2str(iPos), '.png'])
    saveas(gcf, [folder '\DnT_Pos_iPos' num2str(iPos), '.fig'])
    
    save([folder '\DnT_Pos_iPos' num2str(iPos) '.mat'])
    
    
    %%
    % i = 1;
    % figure;
    % semilogx(f, 20.*log10(abs((newApp{i}.sourceRoom0.dataDirect.reverberationData.reverberationFactorComplexFreq).')), 'DisplayName', 'rev');
    % hold on;
    % semilogx(f, 20.*log10(abs((newApp{i}.sourceRoom0.dataDirect.roomFiData.fiComplexFreq).')), 'DisplayName', 'Fi');
    % hold on;
    % semilogx(f, 20.*log10(abs((newApp{i}.sourceRoom0.dataDirect.roomFiData.fiComplexFreq + newApp{i}.sourceRoom0.dataDirect.reverberationData.reverberationFactorComplexFreq).')), 'DisplayName', 'Fi+rev');
    % hold on;
    % semilogx(f, 20.*log10(abs(newApp{i}.sourceRoom0.dataDirect.reverberationData.roomTauDirectDataDiffuse.sqrtTauDiffComplexFreq.')), 'DisplayName', 'sqrt(tauDiff)')
    % hold on;
    % semilogx(f(1:end/2), 10.*log10(abs(newApp{i}.sourceRoom0.dataDirect.reverberationData.roomTauDirectDataDiffuse.tauDiffSi(1).floats.')), '--', 'DisplayName', 'tauDiff')
    % hold on;
    % semilogx(f3rd, 10.*log10(abs(newApp{i}.sourceRoom0.dataDirect.reverberationData.roomTauDirectDataDiffuse.tauDir45(1).floats.')), 'x--', 'DisplayName', 'tauDir45')
    % H_SetFreqPlot
    % legend('Location', 'SE')
end
%%

%%
figure;
% [Rw_pSq_Mean_Davy, ref_curve_pSq_Mean_Davy] = H_calcRwFromR(DnT_pSq_Mean_Davy);
plot(mean(DnT_ref, 2), '-*r', 'DisplayName', ['D_n_T (Extended Approach)']);
hold on;
corr=[+2 +2 -1 0];
corr=[0 0 0 0];
for i = 1:size(dnt_time_pos, 1)-1
    plot(dnt_time_pos(i, :)+corr(i), '--o', 'DisplayName', ['D_n_T (Pos ', num2str(i), ')' ]);
end
%     plot(mean(dnt_tau(6:36), 2), '-*b', 'DisplayName', ['D_n_T (Tau)']);

% legend('Location', 'SE','NumColumns', 2)
legend('Location', 'NW')

ylabel('D_n_T (dB)')
xlabel('Frequency (Hz)')
%     title(['order ' num2str(order)]);
ax = gca;
xticks([1:31])
% xticks([5:2:35])
ax.XTickLabel = f3rd(6:36);
xtickLabels1 = ax.XTickLabel;
xtickLabels = [];
for i = 1:size(xtickLabels1, 1)
    xtickLabels{end+1} = xtickLabels1(i, :);
end
hiddenId = [-2,-1,0,2, 3, 5, 6, 8, 9, 10, 12, 13, 15, 16, 18, 19, 20,22,23,25 ,26] + 4;
for i = hiddenId
    xtickLabels{i} = [''];
end
xtickLabels{14+4} = '1k';
xtickLabels{17+4} = '2k';
xtickLabels{21+4} = '5k';
xtickLabels{24+4} = '10k';
xtickLabels{27+4} = '20k';
ax.XTickLabel = xtickLabels;
xlim([0.5, 31.5])
grid on;
ylim([0, 80])
set(gca, 'FontName', 'Times')

%%
%     saveas(gcf, ['DnT_Scene2_Pos.png'])
%     saveas(gcf, ['DnT_Scene2_Pos.fig'])
%%
saveas(gcf, [folder '\DnT_all_Pos.png'])
saveas(gcf, [folder '\DnT_all_Pos.fig'])

save([folder '\DnT_all_Pos.mat'])

%%
figure;
% [Rw_pSq_Mean_Davy, ref_curve_pSq_Mean_Davy] = H_calcRwFromR(DnT_pSq_Mean_Davy);
plot(mean(DnT_ref, 2), '-*r', 'DisplayName', ['D_n_T (Extended Approach)']);
hold on;
corr=[+2 +2 -1-1 -1]-2;
corr=[0 0 -4-1 -3-1];
corr=[2     2    -1-1     -1];
% corr=[0 0 0 0];
for i = 1:size(dnt_time_pos, 1)-1
    plot(dnt_time_pos(i, :)+corr(i), '--o', 'DisplayName', ['D_n_T (Pos ', num2str(i), ')' ]);
end
%     plot(mean(dnt_tau(6:36), 2), '-*b', 'DisplayName', ['D_n_T (Tau)']);

% legend('Location', 'SE','NumColumns', 2)
legend('Location', 'NW')

ylabel('D_n_T (dB)')
xlabel('Frequency (Hz)')
%     title(['order ' num2str(order)]);
ax = gca;
xticks([1:31])
% xticks([5:2:35])
ax.XTickLabel = f3rd(6:36);
xtickLabels1 = ax.XTickLabel;
xtickLabels = [];
for i = 1:size(xtickLabels1, 1)
    xtickLabels{end+1} = xtickLabels1(i, :);
end
hiddenId = [-2,-1,0,2, 3, 5, 6, 8, 9, 10, 12, 13, 15, 16, 18, 19, 20,22,23,25 ,26] + 4;
for i = hiddenId
    xtickLabels{i} = [''];
end
xtickLabels{14+4} = '1k';
xtickLabels{17+4} = '2k';
xtickLabels{21+4} = '5k';
xtickLabels{24+4} = '10k';
xtickLabels{27+4} = '20k';
ax.XTickLabel = xtickLabels;
xlim([0.5, 31.5])
grid on;
ylim([0, 80])
set(gca, 'FontName', 'Times')

%%
saveas(gcf, [folder '\DnT_all_Pos_new.png'])
saveas(gcf, [folder '\DnT_all_Pos_new.fig'])

save([folder '\DnT_all_Pos_new.mat'])


