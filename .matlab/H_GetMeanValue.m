function [ir3rdOctave, freqsOctave] = H_GetMeanValue(irFreq, freqsOctave)
if ~exist('freqsOctave', 'var')
    freqsOctave = [20, 25, 31.5, 40, 50, 62.5, 80, 100, 125, 155, 200, 250, 315, 400, 500, 630, ...
        800, 1000, 1250, 1600, 2000, 2500, 3150, 4000, 5000, 6350, 8000, 10000, 12500, 16000, 20000];
%     freqsOctave = [2, 2.5, 3.15, 4, 5, 6.25, 8, 10, 12.5, 15.5, freqsOctave];
end

freq = [1:length(irFreq)] .* 44100 ./ (length(irFreq));
shortdim = min(size(irFreq, 2));
ir3rdOctave = zeros(length(freqsOctave), size(irFreq, 2));
for j = 1:size(irFreq, 2)
    for i = 1:length(freqsOctave)
        
        f = freqsOctave(i);
        fmin = f * 2^(-1 / 6);
        fmax = f * 2^(1 / 6);
        BW = '1/3 octave';
        N = 8;
        Fs = 44100;
        %     oneThirdOctaveFilter = octaveFilter('FilterOrder', N, ...
        %         'CenterFrequency', f, 'Bandwidth', BW, 'SampleRate', Fs);
        %     h  = step(oneThirdOctaveFilter, irTime);
        
        %     meanSqareSoundPressure = sqrt(sum(h.^2)/length(h));
        
        indexes = freq > fmin & freq < fmax;
        if sum(indexes) == 0
            freqsOctave(i) = -1;
            ir3rdOctave(i) = -1;
            continue;
            %         if ~exist('lastIndexes')
            %             ir3rdOctave(i) = irFreq(1);
            %         else
            %             ir3rdOctave(i) = sum(lastIndexes(:).*irFreq(:)) / sum(lastIndexes);
            %         end
        else
            ir3rdOctave(i, j) = sum(irFreq(indexes, j)) / sum(indexes);
            %         ir3rdOctave(i) =  sum(indexes(:) .* irFreq(:));
            lastIndexes = indexes;
        end
        %     ir3rdOctave(i) =  meanSqareSoundPressure;
    end
    freqsOctave(freqsOctave == -1) = [];
    ir3rdOctave(ir3rdOctave == -1) = [];
    
end
