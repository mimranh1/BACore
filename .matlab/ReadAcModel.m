clear all;clc;close all;
fname = './../Data/AcBuildingAcoustics/BuidlingsAcoustics.json';
[newApp] = ReadJSONUnity(fname);

fs = 44100;
f3rd = [50, 63, 80, 100, 125, 160, 200, 250, 315, 400, 500, 630, 800, 1000, 1250, 1600, 2000, 2500, 3150, 4000, 5000];
rho0c = 1.2*343;

%%
sqrtRho0c=sqrt(rho0c);
irFreq = [];
irFreq(1).freq(1, :) = sqrtRho0c./sqrt(rho0c).*newApp.sourceRoom0.dataDirect.irOutComplexFreq .* newApp.receiverRoom.dataDirect.irOutComplexFreq(1, :);
irFreq(1).freq(2, :) = sqrtRho0c./sqrt(rho0c).*newApp.sourceRoom0.dataDirect.irOutComplexFreq .* newApp.receiverRoom.dataDirect.irOutComplexFreq(2, :);

irFreq(1).time(1, :) = real(ifft(irFreq(1).freq(1, :)));
irFreq(1).time(2, :) = real(ifft(irFreq(1).freq(2, :)));
figure;
subplot(2, 1, 1)
plot(irFreq(1).time');
hold on;
grid on;

subplot(2, 1, 2)
Plot_FreqFromTime(irFreq(1).time');
hold on; grid on;
for i = 1:length(newApp.sourceRoom0.flankingFactors)
    factor = newApp.sourceRoom0.flankingFactors(i);
    iSource = factor.sourceWallRoomId;
    iReceiver = factor.receiverWallRoomId;
    fac=sqrtRho0c.*factor.factor./sqrt(rho0c);
    irFreq(1+i).iSource = iSource;
    irFreq(1+i).iReceiver = iReceiver;
    irFreq(1+i).freq(1, :) = newApp.sourceRoom0.dataFlanking.irOutComplexFreq(iSource+1, :) .* ...
        newApp.receiverRoom.dataFlanking.irOutComplexFreq(2*iReceiver+1, :).*fac;
    irFreq(1+i).freq(2, :) = newApp.sourceRoom0.dataFlanking.irOutComplexFreq(iSource+1, :) .* ...
        newApp.receiverRoom.dataFlanking.irOutComplexFreq(2*iReceiver+2, :).*fac;
    irFreq(1+i).time(1, :) = real(ifft(irFreq(1+i).freq(1, :)));
    irFreq(1+i).time(2, :) = real(ifft(irFreq(1+i).freq(2, :)));
    subplot(2, 1, 1)
    plot(irFreq(1+i).time');
    hold on;
    grid on;
    subplot(2, 1, 2)
    
    Plot_FreqFromTime(irFreq(1+i).time');
    hold on;
    grid on;
end

%%
figure;
subplot(2, 2, 1)
plot(real(ifft(newApp.sourceRoom0.dataDirect.irOutComplexFreq.')));
hold on; grid on;
title('sourceRoom0.dataDirect')

subplot(2, 2, 2)
plot(real(ifft(newApp.sourceRoom0.dataFlanking.irOutComplexFreq.')));
hold on; grid on;
title('sourceRoom0.dataFlanking')


subplot(2, 2, 3)
plot(real(ifft(newApp.receiverRoom.dataDirect.irOutComplexFreq.')));
hold on; grid on
title('receiverRoom.dataDirect')

subplot(2, 2, 4)
plot(real(ifft(newApp.receiverRoom.dataFlanking.irOutComplexFreq.')));
hold on; grid on;
title('receiverRoom.dataFlanking')

%%
figure;
subplot(2, 2, 1)
plot(real(ifft(newApp.sourceRoom0.dataDirect.roomFiData.fiComplexFreq.')));
hold on; grid on;
title('sourceRoom0.dataDirect')

subplot(2, 2, 2)
plot(real(ifft(newApp.sourceRoom0.dataFlanking.roomFiData.fiComplexFreq.')));
hold on; grid on;
title('sourceRoom0.dataFlanking')


subplot(2, 2, 3)
plot(real(ifft(newApp.receiverRoom.dataDirect.roomHrtfData.hrtfComplexFreq.')));
hold on; grid on
title('receiverRoom.dataDirect')

subplot(2, 2, 4)
plot(real(ifft(newApp.receiverRoom.dataFlanking.roomHrtfData.hrtfComplexFreq.')));
hold on; grid on;
title('receiverRoom.dataFlanking')

%%
f=[1:length(irFreq(1).freq)]./ length(irFreq(1).freq).*fs;

vR = newApp.receiverRoom.dataFlanking.roomStatsData.volume;
Tr = newApp.receiverRoom.dataFlanking.roomStatsData.reverberationTime;
Sd = newApp.sourceRoom0.flankingFactors(1).separatingArea;

irFreqAll=irFreq(1).freq;
tauDd = newApp.sourceRoom0.dataDirect.roomTauDirectData.tau.floats;
tauSum = tauDd;
diff(:,1)= PlotDnt(sum(irFreq(1).freq,1)   ,tauDd,f3rd,Tr,vR,Sd);
title('dd')

%
for i = 1:length(newApp.sourceRoom0.flankingFactors)
    tau=10.^(-0.1.*newApp.sourceRoom0.flankingFactors(i).reductionIndex);
    tauSum = tauSum + tau;
    irFreqAll=irFreqAll+irFreq(i+1).freq;
    diff(:,end+1)= PlotDnt(sum(irFreq(i+1).freq,1),tau,f3rd,Tr,vR,Sd);
    title(['flank ' num2str(i)])
       
end
PlotDnt(sum(irFreqAll,1),tauSum,f3rd,Tr,vR,Sd)
title(['sum'])

%%
figure;
plot(diff)


