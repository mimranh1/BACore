%% 
clear;
clc;
close all;
for i=0:4
fname = ['./../Data/AcBuildingAcoustics/BuidlingsAcoustics_DnT_Scene2_Pos' num2str(i) '.json'];
[newApp{i+1}] = ReadJSONUnity(fname);
end
fs = 44100;
f3rd = [0, 8, 10, 12.5, 16, 20, 25, 31.5, 40, 50, 63, 80, 100, 125, 160, 200, 250, 315, 400, 500, 630, 800, 1000, 1250, 1600, 2000, 2500, 3150, 4000, 5000, 6300, 8000, 10000, 12500, 16000, 20000, 25000];


filterLen = length(newApp{1}.brir0.irLeft.time);
f = [1:filterLen] ./ filterLen .* fs;


%%

figure;
% irFreqq = newApp.receiverRoom.dataDirect.reverberationData.reverberationFactorComplexFreq;
irFreqq = newApp{1}.receiverRoom.dataDirect.irOutComplexFreq(1:2:end, :)'.* newApp{1}.sourceRoom0.dataDirect.irOutComplexFreq';
% irFreqq = newApp{1}.brir0.irLeft.freqComplex;
% irFreqq = newApp.receiverRoom.dataDirect.reverberationData.reverberationCutTime';
% irFreqq = newApp.receiverRoom.dataDirect.roomHrtfData.hrtfComplexFreq;
semilogx(f, 10.*log10(abs(irFreqq)));
hold on;

[ir3rdOctave, freqsOctave] = H_GetMeanValue(abs(irFreqq));
semilogx(freqsOctave, 10.*log10(abs((ir3rdOctave)')));
H_SetFreqPlot

%%
irFreqAll=[];
for i=1:length(newApp)
irFreqq = sum(newApp{i}.receiverRoom.dataDirect.irOutComplexFreq(1:2:end, :).* newApp{i}.sourceRoom0.dataDirect.irOutComplexFreq,1);
dist = mean(newApp{i}.sourceRoom0.dataDirect.roomFiData.sourceDistance);
factor = (1./ dist)/7*15;

irFreqAll = [abs(irFreqq)./factor; irFreqAll];
end
vR = newApp{i}.receiverRoom.dataDirect.roomStatsData.volume;
Tr = newApp{i}.receiverRoom.dataDirect.roomStatsData.reverberationTime;
Sd = newApp{i}.sourceRoom0.dataDirect.roomTauDirectDataDiffuse.partitionsArea;

tauDdDiff = newApp{i}.sourceRoom0.dataDirect.roomTauDirectDataDiffuse.tauDiff.floats;
tauDd45 = newApp{i}.sourceRoom0.dataDirect.roomTauDirectDataDiffuse.tauDir45(1).floats;
tauSum = [tauDd45];
[~, dnt_time, dnt_tau]= PlotDnt(irFreqAll, tauSum, f3rd, Tr, vR, Sd(1));
706.49
xlim([50, 5000])
ylim([0, 50])
title('dd')
% 
% 
% ignoreDiff=1;
Dnt_tau_label='D_n_T (\tau_4_5_°)';
% ylimm=[10 60];
% Plot_DnTWithBoxplotsNew1(f3rd(6:36), dnt_time, dnt_tau(6:36), Dnt_tau_label, ylimm, ignoreDiff)

load('DnT_Verification.mat', 'DnT_ref')
%%
figure;
% [Rw_pSq_Mean_Davy, ref_curve_pSq_Mean_Davy] = H_calcRwFromR(DnT_pSq_Mean_Davy);
plot(mean(DnT_ref,2), '-*r', 'DisplayName', ['D_n_T (Extended Approach)']);
hold on;
for i=1:size(dnt_time,2)
plot(dnt_time(:,i), '--o', 'DisplayName', ['D_n_T (Pos ' num2str(i) ')']);
end
% legend('Location', 'SE','NumColumns', 2)
legend('Location', 'SE')

ylabel('D_n_T (dB)')
xlabel('Frequncy (Hz)')

ax = gca;
xticks([1:31])
% xticks([5:2:35])
ax.XTickLabel=f3rd(6:36);
xtickLabels1=ax.XTickLabel;
xtickLabels=[];
for i=1:size(xtickLabels1,1)
   xtickLabels{end+1}= xtickLabels1(i,:);
end
hiddenId = [2, 3, 5, 6, 8, 9, 10, 12, 13, 15, 16, 18, 19, 20]+4;
for i = hiddenId
    xtickLabels{i} = [''];
end
xtickLabels{14+4} = '1k';
xtickLabels{17+4} = '2k';
xtickLabels{21+4} = '5k';
ax.XTickLabel = xtickLabels;
xlim([4.5, 25.5])
grid on;
ylim([0 60])
set(gca, 'FontName', 'Times')


saveas(gcf, ['DnT_Scene2_Pos.png'])
saveas(gcf, ['DnT_Scene2_Pos.fig'])


save('DnT_Scene2_Pos.mat')


%%
% load('dntOutdoor.mat')
% 
% vR = newApp.receiverRoom.dataDirect.roomStatsData.volume;
% Tr = newApp.receiverRoom.dataDirect.roomStatsData.reverberationTime;
% Sd = newApp.sourceRoom0.dataDirect.roomTauDirectDataDiffuse.partitionsArea;
% % irFreqq = sum(newApp.receiverRoom.dataDirect.irOutComplexFreq(1:2:end, :).* newApp.sourceRoom0.dataDirect.irOutComplexFreq,1);
% % 
% % irFreqAll = [abs(irFreqq)*3000; irFrqOld];
% tauDdDiff = newApp.sourceRoom0.dataDirect.roomTauDirectDataDiffuse.tauDiff.floats;
% tauDd45 = newApp.sourceRoom0.dataDirect.roomTauDirectDataDiffuse.tauDir45(1).floats;
% tauSum = [tauDd45];
% diff(:, 1) = PlotDnt(irFreqAll, tauSum, f3rd, Tr, vR, Sd);
% 
% xlim([50, 5000])
% ylim([0, 50])
% title('dd')