function [val] = ReadJSONUnity(fname)
fid = fopen(fname); 
raw = fread(fid,inf); 
str = char(raw'); 
fclose(fid); 
val = jsondecode(str);
%
[val] = ChangeComplex(val);
end

function [val] = ChangeComplex(val)

fn = fieldnames(val);
if length(fn)==3
   if strcmp(fn{1},'x') && strcmp(fn{2},'y') && strcmp(fn{3},'z')
       for i=1:length(val)
       val(i).vector3= [val(i).x, val(i).y, val(i).z];
       val(i).x=[];
       end
       rmfield(val, 'x');
       rmfield(val, 'y');
       rmfield(val, 'z');

       return;
   end
end
for k=1:numel(fn)
    if length(val) == 1 && isstruct(  val(1).(fn{k}))
        [val.(fn{k})] = ChangeComplex(val.(fn{k}));
    end
    if( contains(fn{k},'Complex'))   
        if isstruct(val.(fn{k}))
        fn1 = fieldnames(val.(fn{k}));
        if length(fn1)==1 && strcmp(fn1{1},'floats' )
            clear valuesout;
            for i=1:length(val.(fn{k}))
            values=val.(fn{k})(i).floats;
                valuesout(i,:)=values(1:2:end)+1j.*values(2:2:end);
            end
            val.(fn{k})=valuesout;
            
        end
        else
        compl=val.(fn{k})(1:2:end)+1j*val.(fn{k})(2:2:end);
        val.(fn{k})=[];
        val.(fn{k})=compl;
        end
        % do stuff
    end
end

end