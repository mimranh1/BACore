function [] = Plot_DnTWithBoxplotsNew1(f, DnT_pSq_Mean_Davy, DnT_tau_DAVY_ref, Dnt_tau_label, ylimm, ignoreDiff)
if ~exist('ignoreDiff', 'var')
    ignoreDiff = 0;
end
figure;
% [Rw_pSq_Mean_Davy, ref_curve_pSq_Mean_Davy] = H_calcRwFromR(DnT_pSq_Mean_Davy);
plot(mean(DnT_pSq_Mean_Davy, 2), '-*r', 'DisplayName', ['D_n_T (Extended Approach)']);
hold on;
if ~ignoreDiff
    plot(DnT_tau_DAVY_ref, '--xb', 'DisplayName', Dnt_tau_label)
end
legend('Location', 'SE')

ylabel('D_n_T (dB)')
xlabel('Frequency (Hz)')

%
if ~ignoreDiff
    yyaxis right
    diff_dnt_davy = DnT_pSq_Mean_Davy - DnT_tau_DAVY_ref;
    diff_mean = mean(DnT_pSq_Mean_Davy, 2) - DnT_tau_DAVY_ref;
    disp(['Diff min:', num2str(min(diff_mean)), ' max:', num2str(max(diff_mean))])
    boxplot(diff_dnt_davy', f, 'Colors', 'r')
    hold on;
    plot([0.5, 31.5], [0, 0], 'b');
    yticks([-10, 0, 10, 20, 30])
    ylim([-50, 10])
    
    yyaxis left
end

grid on;
ax = gca;
% xticks([5:2:35])
xtickLabels = ax.XTickLabel;
hiddenId = [2, 3, 5, 6, 8, 9, 10, 12, 13, 15, 16, 18, 19, 20,22,23,24]+4;
    hiddenId = [-2,-1,0,2, 3, 5, 6, 8, 9, 10, 12, 13, 15, 16, 18, 19, 20,22,23,25 ,26] + 4;
for i = hiddenId
    xtickLabels{i} = [''];
end
xtickLabels{14+4} = '1k';
xtickLabels{17+4} = '2k';
xtickLabels{21+4} = '5k';
    xtickLabels{24+4} = '10k';
    xtickLabels{27+4} = '20k';
% xtickLabels={'50';'';'';'100';'';'';'200';'';'';'';'500';'';'';'1k';'';'';'2k';'';'';'';'5k'};
ax.XTickLabel = xtickLabels;
xlim([4.5, 25.5])
set(gca, 'FontName', 'Times')

ylim(ylimm)

if ~ignoreDiff
    
    yyaxis right
    if ignoreDiff
        yticks([-20, 0, 20])
        ylim([20 - 2 * (ylimm(2) - ylimm(1)), 20])
    else
        yticks([-5, 0, 5])
        ylim([5 - 1.*(ylimm(2) - ylimm(1)), 5])
    end
    hLegend = findobj(gcf, 'Type', 'Legend');
    % get text
    LegendString = hLegend.String;
    ylabel('\DeltaD_n_T (dB)')
    for i = 1:length(LegendString) - 1
        newLegend{i} = LegendString{i};
    end
    legend(newLegend);
    
    
    yyaxis right
    ax = gca;
    ax.YAxis(1).Color = 'k';
    ax.YAxis(2).Color = 'k';
    yyaxis left
end
end