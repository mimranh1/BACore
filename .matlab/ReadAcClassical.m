clear all;
%%
fname = './../Data/AcClassical/AcClassicalIndoor.json'; 
[thd] = ReadJSONUnity(fname);
fname = './../Data/AcClassical/ReceiverRoom.json'; 
[thd.receiverRoom.data] = ReadJSONUnity(fname);
fname = './../Data/AcClassical/SourceRoom0.json'; 
[thd.sourceRooms.data] = ReadJSONUnity(fname);
fs=44100;
f3rd=[50, 63, 80, 100, 125, 160, 200, 250, 315, 400, 500, 630, 800, 1000, 1250, 1600, 2000, 2500, 3150, 4000, 5000];

%%
figure;
subplot(2,1,1)
plot(thd.sourceRooms.data.brirs.irLeft.time);
hold on; grid on;
plot(thd.sourceRooms.data.brirs.irRight.time);

subplot(2,1,2)
f=[1:length(thd.sourceRooms.data.brirs.irLeft.freqComplex)]./ length(thd.sourceRooms.data.brirs.irLeft.freqComplex).*fs;
semilogx(f,20.*log10(abs(thd.sourceRooms.data.brirs.irLeft.freqComplex)));
hold on;
semilogx(f,20.*log10(abs(thd.sourceRooms.data.brirs.irRight.freqComplex)));
semilogx(f,10.*log10(sum(abs(thd.sourceRooms.data.tauComplexFreq))));

H_SetFreqPlot;

%%
figure;
subplot(2,1,1)
plot(thd.receiverRoom.data.reverberationData.reverberationCutTime);

subplot(2,1,2)
semilogx(f, 10.*log10(abs(fft(thd.receiverRoom.data.reverberationData.reverberationCutTime))));
H_SetFreqPlot
%%
hold on; grid on;
plot(thd.sourceRooms.data.brirs.irRight.time);

subplot(2,1,2)
f=[1:length(thd.sourceRooms.data.brirs.irLeft.freqComplex)]./ length(thd.sourceRooms.data.brirs.irLeft.freqComplex).*fs;
semilogx(f,20.*log10(abs(thd.sourceRooms.data.brirs.irLeft.freqComplex)));
hold on;
semilogx(f,20.*log10(abs(thd.sourceRooms.data.brirs.irRight.freqComplex)));
semilogx(f,10.*log10(sum(abs(thd.sourceRooms.data.tauComplexFreq))));

H_SetFreqPlot;
%%
tauComplexFreq = thd.sourceRooms.data.tauComplexFreq;
constFactor = thd.sourceRooms.data.constFactor;
dirFactor = thd.receiverRoom.data.dirFactor *constFactor;
hrtfComplex=thd.receiverRoom.data.hrtfDataWall.hrtfComplexFreq;
indexes=thd.receiverRoom.indexes;
xrev=thd.receiverRoom.data.reverberationData.reverberationComplexFreq';
for iWallFlanking=1:5
    iWallReceiver = indexes(iWallFlanking);
    reverberation=  xrev.* tauComplexFreq(iWallFlanking,:).*...
    thd.receiverRoom.data.revFactor(iWallReceiver+1) .*  constFactor;
    freqComplexLeft(iWallFlanking,:)=hrtfComplex(2*iWallReceiver+1,:).*tauComplexFreq(iWallFlanking,:) .* dirFactor(iWallReceiver+1)+ reverberation;
    freqComplexRight(iWallFlanking,:)=hrtfComplex(2*iWallReceiver+2,:).*tauComplexFreq(iWallFlanking,:) .* dirFactor(iWallReceiver+1)+ reverberation;
end
freqComplex=freqComplexLeft+freqComplexRight;

%%
% vR= thd.receiverRoom.data.;
% Tr= thd.receiverRoom.data.reverberationTime;
vR= 90;
Tr= 0.4;
Sd=thd.sourceRooms.data.flankingPaths(1).separatingArea;
tauSum=0;
for i=1:length(thd.sourceRooms.data.flankingPaths)
    tauij=thd.sourceRooms.data.flankingPaths(i).transferFunction;
    tauSum= tauSum+ tauij;
    PlotDnt(freqComplex(i,:),tauij,f3rd,Tr,vR,Sd)
end
irFreq=abs(sum(freqComplex,1));
PlotDnt(irFreq,tauSum,f3rd,Tr,vR,Sd)
