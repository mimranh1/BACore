%% 
clear;
clc;
close all;
folder='OutdoorDnt_88200AngleL12m_D1';
mkdir(folder);
for i=0:4
fname = ['./../.Data/AcBuildingAcoustics/BuidlingsAcoustics_DnT_Verification' num2str(i) '.json'];
[newApp{i+1}] = ReadJSONUnity(fname);
end
fs = 44100;
f3rd = [0, 8, 10, 12.5, 16, 20, 25, 31.5, 40, 50, 63, 80, 100, 125, 160, 200, 250, 315, 400, 500, 630, 800, 1000, 1250, 1600, 2000, 2500, 3150, 4000, 5000, 6300, 8000, 10000, 12500, 16000, 20000, 25000];


filterLen = length(newApp{1}.brir0.irLeft.time);
f = [1:filterLen] ./ filterLen .* fs;


%%

figure;
% irFreqq = newApp.receiverRoom.dataDirect.reverberationData.reverberationFactorComplexFreq;
irFreqq = newApp{1}.receiverRoom.dataDirect.irOutComplexFreq(1:2:end, :)'.* newApp{1}.sourceRoom0.dataDirect.irOutComplexFreq';
% irFreqq = newApp{1}.brir0.irLeft.freqComplex;
% irFreqq = newApp.receiverRoom.dataDirect.reverberationData.reverberationCutTime';
% irFreqq = newApp.receiverRoom.dataDirect.roomHrtfData.hrtfComplexFreq;
% irFreqq=newApp{1}.sourceRoom0.dataDirect.reverberationData.reverberationFactorComplexFreq.';
subplot(2,1,1)
semilogx(f, 10.*log10(abs(irFreqq)));
hold on;

[ir3rdOctave, freqsOctave] = H_GetMeanValue(abs(irFreqq));
semilogx(freqsOctave, 10.*log10(abs((ir3rdOctave))));
H_SetFreqPlot

subplot(2,1,2)
plot(real(ifft(irFreqq)));grid on;
%%
irFreqAll=[];
for i=1:length(newApp)
irFreqq = newApp{i}.AcBuildingAcoustics.sqrtRho0c .*sum(newApp{i}.receiverRoom.dataDirect.irOutComplexFreq(1:2:end, :).* newApp{i}.sourceRoom0.dataDirect.irOutComplexFreq,1);
% dist = mean(newApp{i}.sourceRoom0.dataDirect.roomFiData.sourceDistance);

% factor = (1./ dist)/7;
% factor=1;
factor = (newApp{i}.AcBuildingAcoustics.l2Ref2MEnergy);
irFreqAll = [abs(irFreqq)./factor; irFreqAll];
end

vR = newApp{i}.receiverRoom.dataDirect.roomStatsData.volume;
Tr = newApp{i}.receiverRoom.dataDirect.roomStatsData.reverberationTime;
Sd = newApp{i}.sourceRoom0.dataDirect.roomTauDirectDataDiffuse.partitionsArea;

tauDdDiff = newApp{i}.sourceRoom0.dataDirect.roomTauDirectDataDiffuse.tauDiff.floats;
tauDd45 = newApp{i}.sourceRoom0.dataDirect.roomTauDirectDataDiffuse.tauDir45(1).floats;
tauDddiff = newApp{i}.sourceRoom0.dataDirect.roomTauDirectDataDiffuse.tauDiff(1).floats;
tauSum = [tauDd45];

[~, dnt_time, dnt_tau]= PlotD(irFreqAll, tauSum, f3rd);
% tauSum = [tauDddiff];
% dnt_time=dnt_time+2.5;
% [~, ~, dnt_tau_diff]= PlotDnt(irFreqAll, tauSum, f3rd, Tr, vR, Sd);

xlim([50, 5000])
ylim([0, 50])
title('dd')

ignoreDiff=0;
Dnt_tau_label='D_n_T (\tau_4_5_°)';
ylimm=[0 60];
Plot_DnTWithBoxplotsNew1(f3rd(6:36), dnt_time, dnt_tau(6:36), Dnt_tau_label, ylimm, ignoreDiff)
DnT_ref=dnt_time;
%%
saveas(gcf, [folder '\DnT_Verification.png'])
saveas(gcf, [folder '\DnT_Verification.fig'])
% % 
save([ folder '\DnT_Verification.mat'])
%%
% figure;semilogx(f, 10.*log10(abs(irFreqAll)));H_SetFreqPlot
% 
% figure;semilogx(f, 10.*log10(abs(newApp{i}.sourceRoom0.dataDirect.irOutComplexFreq)));H_SetFreqPlot
% figure;semilogx(f, 10.*log10(abs(newApp{i}.receiverRoom.dataDirect.irOutComplexFreq(1:2:end, :))));H_SetFreqPlot
% 
% figure;semilogx(f, 10.*log10(abs(newApp{i}.receiverRoom.dataDirect.roomHrtfData.hrtfComplexFreq(1:2:end, :))));H_SetFreqPlot
% 
% figure;semilogx(f, 10.*log10(abs(newApp{i}.receiverRoom.dataDirect.reverberationData.reverberationFactorComplexFreq)));H_SetFreqPlot
% 
% figure;plot(real(ifft(newApp{i}.receiverRoom.dataDirect.reverberationData.reverberationFactorComplexFreq.')));

OutdoorDPos;
%%
% load('dntOutdoor.mat') 
% 
% vR = newApp.receiverRoom.dataDirect.roomStatsData.volume;
% Tr = newApp.receiverRoom.dataDirect.roomStatsData.reverberationTime;
% Sd = newApp.sourceRoom0.dataDirect.roomTauDirectDataDiffuse.partitionsArea;
% % irFreqq = sum(newApp.receiverRoom.dataDirect.irOutComplexFreq(1:2:end, :).* newApp.sourceRoom0.dataDirect.irOutComplexFreq,1);
% % 
% % irFreqAll = [abs(irFreqq)*3000; irFrqOld];
% tauDdDiff = newApp.sourceRoom0.dataDirect.roomTauDirectDataDiffuse.tauDiff.floats;
% tauDd45 = newApp.sourceRoom0.dataDirect.roomTauDirectDataDiffuse.tauDir45(1).floats;
% tauSum = [tauDd45];
% diff(:, 1) = PlotDnt(irFreqAll, tauSum, f3rd, Tr, vR, Sd);
% 
% xlim([50, 5000])
% ylim([0, 50])
% title('dd')