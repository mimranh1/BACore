%% 
clear;
clc;
close all;
fname = './../Data/AcBuildingAcoustics/BuidlingsAcoustics.json';
[newApp] = ReadJSONUnity(fname);

fs = 44100;
f3rd = [0, 8, 10, 12.5, 16, 20, 25, 31.5, 40, 50, 63, 80, 100, 125, 160, 200, 250, 315, 400, 500, 630, 800, 1000, 1250, 1600, 2000, 2500, 3150, 4000, 5000, 6300, 8000, 10000, 12500, 16000, 20000, 25000];

%%
filterLen = length(newApp.brir0.irLeft.time);
f = [1:filterLen] ./ filterLen .* fs;

figure;
subplot(2, 1, 1)
plot(newApp.brir0.irLeft.time)
hold on;
grid on;
plot(real(ifft(newApp.brir0.irLeft.freqComplex)));
subplot(2, 1, 2)
semilogx(f, 10.*log10(abs(newApp.brir0.irLeft.freqComplex)));
H_SetFreqPlot
%%
figure;
subplot(2, 2, 1)
semilogx(f, 10.*log10(abs(newApp.sourceRoom0.dataDirect.roomFiData.fiComplexFreq')));

H_SetFreqPlot;
title('source Room fiComplexFreq')
subplot(2, 2, 2)
plot(real(ifft(newApp.sourceRoom0.dataDirect.roomFiData.fiComplexFreq')));
title('source Room fiComplexFreq time')

subplot(2, 2, 3)
semilogx(f, 10.*log10(abs(newApp.sourceRoom0.dataDirect.reverberationData.reverberationFactorComplexFreq))');
H_SetFreqPlot;
title('source Room reverberationFactorComplexFreq')

subplot(2, 2, 4)
plot(real(ifft(newApp.sourceRoom0.dataDirect.reverberationData.reverberationFactorComplexFreq))');
hold on; grid on;
plot(newApp.sourceRoom0.dataDirect.reverberationData.reverberationCutTime');
title('source Room reverberationFactorComplexFreq time')

%%
figure;
subplot(2, 2, 1)
semilogx(f, 10.*log10(abs(newApp.sourceRoom0.dataDirect.irOutComplexFreq')));
H_SetFreqPlot;


subplot(2, 2, 2)
semilogx(f, 10.*log10(abs(newApp.receiverRoom.dataDirect.irOutComplexFreq')));
hold on;
[ir3rdOctave, freqsOctave] = H_GetMeanValue(abs(newApp.receiverRoom.dataDirect.irOutComplexFreq)');
semilogx(freqsOctave, 10.*log10(abs((ir3rdOctave)')));
H_SetFreqPlot;


subplot(2, 2, 3)
irFreqTot = newApp.receiverRoom.dataDirect.irOutComplexFreq(1:2:end, :) .* newApp.sourceRoom0.dataDirect.irOutComplexFreq;
semilogx(f, 10.*log10(abs((irFreqTot)')));
hold on;
[ir3rdOctave, freqsOctave] = H_GetMeanValue(abs(irFreqTot)');
semilogx(freqsOctave, 10.*log10(abs((ir3rdOctave)')));
H_SetFreqPlot;
xlim([50, 5000])


subplot(2, 2, 4)
semilogx(freqsOctave, -10.*log10(abs((ir3rdOctave)')));
H_SetFreqPlot;
xlim([50, 5000])
title('source and receiver Room')


%%
figure;
% irFreqTot = newApp.receiverRoom.dataDirect.irOutComplexFreq(1:2:end, :) ;
irFreqTot =  newApp.sourceRoom0.dataDirect.irOutComplexFreq;
irFreqTot = newApp.receiverRoom.dataDirect.irOutComplexFreq(1:2:end, :).* newApp.sourceRoom0.dataDirect.irOutComplexFreq;
semilogx(f, 10.*log10(abs((irFreqTot)')));
hold on;
[ir3rdOctave, freqsOctave] = H_GetMeanValue(abs(irFreqTot)');
semilogx(freqsOctave, 10.*log10(abs((ir3rdOctave)')));
H_SetFreqPlot;
xlim([50, 5000])


%%

figure;
% irFreqq = newApp.receiverRoom.dataDirect.reverberationData.reverberationFactorComplexFreq;
irFreqq = newApp.receiverRoom.dataDirect.irOutComplexFreq(1:2:end, :)'.* newApp.sourceRoom0.dataDirect.irOutComplexFreq';
% irFreqq = newApp.receiverRoom.dataDirect.reverberationData.reverberationCutTime';
% irFreqq = newApp.receiverRoom.dataDirect.roomHrtfData.hrtfComplexFreq;
semilogx(f, 10.*log10(abs(irFreqq)));
hold on;

[ir3rdOctave, freqsOctave] = H_GetMeanValue(abs(irFreqq));
semilogx(freqsOctave, 10.*log10(abs((ir3rdOctave)')));
H_SetFreqPlot

%%
irFrqOld=[];

vR = newApp.receiverRoom.dataDirect.roomStatsData.volume;
Tr = newApp.receiverRoom.dataDirect.roomStatsData.reverberationTime;
Sd = newApp.sourceRoom0.dataDirect.roomTauDirectDataDiffuse.partitionsArea;
irFreqq = sum(newApp.receiverRoom.dataDirect.irOutComplexFreq(1:2:end, :).* newApp.sourceRoom0.dataDirect.irOutComplexFreq,1);

irFreqAll = [abs(irFreqq)*100; irFrqOld];
tauDdDiff = newApp.sourceRoom0.dataDirect.roomTauDirectDataDiffuse.tauDiff.floats;
tauDd45 = newApp.sourceRoom0.dataDirect.roomTauDirectDataDiffuse.tauDir45(1).floats;
tauSum = [tauDd45];
diff(:, 1) = PlotDnt(irFreqAll, tauSum, f3rd, Tr, vR, Sd);

xlim([50, 5000])
ylim([0, 50])
title('dd')
