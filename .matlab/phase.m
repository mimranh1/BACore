function [res] = phase(x)
%PHASE Summary of this function goes here
%   Detailed explanation goes here
for j=1:size(x,1)
y = angle(x(j,:));
% figure;
% plot(y); hold on;
if y(1,1)-y(1,2) > 0
    % decresing
    for i=1:length(y)-1
        if y(1,i)-y(1,i+1) <0
            y(1,i+1:end)= y(1,i+1:end)-2*pi;
%             plot(y);
        end
    end
else
    % incresing
    
    for i=1:length(y)-1
        if y(1,i)-y(1,i+1) >0
            y(1,i+1:end)= y(1,i+1:end)+2*pi;
%             plot(y);
        end
    end
end
res(j,:)=y;
end
end

