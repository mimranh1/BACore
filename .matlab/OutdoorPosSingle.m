
%%
ccx;
% close all;
folder='OutdoorDnt_LibaryL12m';
sceneNr=1;
% close all;
if sceneNr==1
folder='OutdoorDnt_Scene1_L12m';
folderData='AcBuildingAcoustics_Scene1';
else
folder='OutdoorDnt_Scene2_L12m';
folderData='AcBuildingAcoustics_Scene2';
end
folder1=folder;
mkdir(folder1);
load([folder '\DnT_Verification.mat'], 'DnT_ref', 'dnt_tau', 'dnt_tau_diff')
folder=[folder '\Extra'];
mkdir(folder);
dnt_time_Order_Pos=zeros(6,5,31);
for order = 0:6
    for iPos=0:3
        clearvars -except folder dnt_time_Order_Pos DnT_ref dnt_tau dnt_tau_diff order iPos refPos dnt_time_pos folder1 folderData
        if order<0
            fname = ['./../.Data/' folderData '/BuidlingsAcoustics_DnT_Libary', num2str(iPos), '.json'];
        else
            fname = ['./../.Data/' folderData '/BuidlingsAcoustics_DnT_Pos_Order', num2str(order), num2str(iPos), '.json'];
        end
        [newApp] = ReadJSONUnity(fname);
        fs = 44100;
        f3rd = [0, 8, 10, 12.5, 16, 20, 25, 31.5, 40, 50, 63, 80, 100, 125, 160, 200, 250, 315, 400, 500, 630, 800, 1000, 1250, 1600, 2000, 2500, 3150, 4000, 5000, 6300, 8000, 10000, 12500, 16000, 20000, 25000];
        f3rd = [20, 25, 31.5, 40, 50, 63, 80, 100, 125, 160, 200, 250, 315, 400, 500, 630, 800, 1000, 1250, 1600, 2000, 2500, 3150, 4000, 5000, 6300, 8000, 10000, 12500, 16000, 20000];
        
        filterLen = length(newApp.brir0.irLeft.time);
        f = [1:filterLen] ./ filterLen .* fs;
        
        rhoc0 = 1.29 * 340;
        
        
        %%
        figure;
        if iPos ==1
        imagePos=[0 0 4];
        else
        imagePos=[4 0 0];
        end
        [incAngleDeg,factor, coIncF] = Plot_Geometry(newApp, 1,imagePos);
        angles(1,:)=newApp.sourceRoom0.dataDirect.reverberationData.AllAnglesDeg(1:2:end,:);
        angles(2,:)=newApp.sourceRoom0.dataDirect.reverberationData.AllAnglesDeg(2:2:end,:);
        angles(3,:) = CalcCoincFreq(angles(2,:));
        refFac=0.95;
        angles(4,:)=zeros(size(angles(1,:)));
        for iOrd=1:6
            angles(4,abs(angles(1,:)-(refFac.^iOrd))<1e-3)=iOrd;
        end
        patch=0;
        for ii=1:size(angles,2)
            if angles(4,ii)==0
                patch=patch+1;
            end
            angles(5,ii)=patch;
            
            
        end
        
        
        
        %%
        irFreqq =newApp.AcBuildingAcoustics.sqrtRho0c.*  sum(newApp.receiverRoom.dataDirect.irOutComplexFreq(1:2:end, :).*newApp.sourceRoom0.dataDirect.irOutComplexFreq, 1);
        %     dist = mean(newApp.sourceRoom0.dataDirect.roomFiData.sourceDistance);
        %     factor = (1 ./ dist) / 7;
        %     % factor = (1./ dist)/1;
        %
        %
        %     factor = (newApp.AcBuildingAcoustics.l2Ref2MEnergy)*2.2;
        disp(['Pos ' num2str(iPos+1) ' factor' num2str(factor)])
        irFreqAll = [abs(irFreqq) ./ factor];
        vR = newApp.receiverRoom.dataDirect.roomStatsData.volume;
        Tr = newApp.receiverRoom.dataDirect.roomStatsData.reverberationTime;
        Sd = newApp.sourceRoom0.dataDirect.roomTauDirectDataDiffuse.partitionsArea;
        i = 1
        tauDdDiff = newApp.sourceRoom0.dataDirect.roomTauDirectDataDiffuse.tauDiff.floats;
        tauDd45 = newApp.sourceRoom0.dataDirect.roomTauDirectDataDiffuse.tauDir45(1).floats;
        tauSum = [tauDd45];
%         if isnan(tauSum(1))
%             tauSum(1)=0;
%         end
        
        [~, dnt_time, dnt_tau] = PlotDnt(irFreqAll, tauSum, f3rd, Tr, vR, Sd(1));
%         figure;semilogx(f3rd(6:36),20.*log10(abs(dnt_time)));H_SetFreqPlot;
        %     xlim([50, 5000])
        %     ylim([0, 50])
        %     title('dd')
        %     %
        %     %
        %     % ignoreDiff=1;
        %     Dnt_tau_label = 'D_n_T (\tau_4_5_°)';
        % ylimm=[10 60];
        % Plot_DnTWithBoxplotsNew1(f3rd(6:36), dnt_time, dnt_tau(6:36), Dnt_tau_label, ylimm, ignoreDiff)
        
        %%
        figure;
        % [Rw_pSq_Mean_Davy, ref_curve_pSq_Mean_Davy] = H_calcRwFromR(DnT_pSq_Mean_Davy);
        semilogx(f3rd,mean(DnT_ref, 2), '-*r', 'DisplayName', ['D_n_T (Extended Approach)']);
        hold on;
        for i = 1:size(dnt_time, 2)
            semilogx(f3rd,dnt_time(:, i), '--o', 'DisplayName', ['D_n_T (Position ', num2str(iPos+1), ')' ]);
        end
%         legend('Location', 'NW')
        colors= winter(7);
        typ={'--' '-.' ':' '--' '-.' ':' 'square--' 'o--' 'x--'};
        for ii=1:size(angles,2)
            yylim=ylim;
            y1=yylim(1);
            y2=yylim(2);
            vec=y1:y2;
            plot(angles(3,ii)+[ zeros(size(vec))],vec,'--',...
                'Color',colors(angles(4,ii)+1,:),...
                'DisplayName',['order ' num2str(angles(4,ii)) ' patch ' num2str(angles(5,ii))])
        end
        H_SetFreqPlot
        title(['D_n_T (Pos ', num2str(iPos+1), ') (TotInc '  num2str(size(angles,2)) ')'])
        dnt_time_pos(:,iPos+1)=dnt_time;
        ylim([-10 70])
        if 0
            %%
            figure;
            % [Rw_pSq_Mean_Davy, ref_curve_pSq_Mean_Davy] = H_calcRwFromR(DnT_pSq_Mean_Davy);
            plot(mean(DnT_ref, 2), '-*r', 'DisplayName', ['D_n_T (Extended Approach)']);
            hold on;
            for i = 1:size(dnt_time, 2)
                plot(dnt_time(:, i), '--o', 'DisplayName', ['D_n_T (Pos ', num2str(iPos+1), ')' ]);
            end
            %     plot(mean(dnt_tau(6:36), 2), '-*b', 'DisplayName', ['D_n_T (Tau)']);
            
            % legend('Location', 'SE','NumColumns', 2)
            legend('Location', 'NW')
            
            ylabel('D_n_T (dB)')
            xlabel('Frequency (Hz)')
            %     title(['order ' num2str(order)]);
            ax = gca;
            xticks([1:31])
            % xticks([5:2:35])
            ax.XTickLabel = f3rd(6:36);
            xtickLabels1 = ax.XTickLabel;
            xtickLabels = [];
            for i = 1:size(xtickLabels1, 1)
                xtickLabels{end+1} = xtickLabels1(i, :);
            end
            hiddenId = [-2,-1,0,2, 3, 5, 6, 8, 9, 10, 12, 13, 15, 16, 18, 19, 20,22,23,25 ,26] + 4;
            for i = hiddenId
                xtickLabels{i} = [''];
            end
            xtickLabels{14+4} = '1k';
            xtickLabels{17+4} = '2k';
            xtickLabels{21+4} = '5k';
            xtickLabels{24+4} = '10k';
            xtickLabels{27+4} = '20k';
            ax.XTickLabel = xtickLabels;
            xlim([0.5, 31.5])
            grid on;
            ylim([0, 80])
            set(gca, 'FontName', 'Times')
            title(num2str(incAngleRad'));
        end
        %%
        saveas(gcf, [folder '\DnT_Pos_' num2str(iPos),'_iOrder_' num2str(order) '.png'])
        saveas(gcf, [folder '\DnT_Pos_' num2str(iPos),'_iOrder_' num2str(order) '.fig'])
        
close all;
        save([folder '\DnT_Pos_' num2str(iPos),'_iOrder_' num2str(order) '.mat'])
        
    end

%% Plot order
%
figure;
% [Rw_pSq_Mean_Davy, ref_curve_pSq_Mean_Davy] = H_calcRwFromR(DnT_pSq_Mean_Davy);
plot(mean(DnT_ref, 2), '-*r', 'DisplayName', ['D_n_T (Extended Approach)']);
hold on;
for iPos = 1:size( dnt_time_pos, 2)
    plot(dnt_time_pos(:,iPos), '--o', 'DisplayName', ['D_n_T (Position ', num2str(iPos), ')']);
end
plot(mean(dnt_tau(6:36), 2), '-*b', 'DisplayName', ['D_n_T (Tau)']);

% legend('Location', 'SE','NumColumns', 2)
legend('Location', 'NW')

ylabel('D_n_T (dB)')
xlabel('Frequency (Hz)')
ax = gca;
xticks([1:31])
% xticks([5:2:35])
ax.XTickLabel = f3rd;
xtickLabels1 = ax.XTickLabel;
xtickLabels = [];
for i = 1:size(xtickLabels1, 1)
    xtickLabels{end+1} = xtickLabels1(i, :);
end
hiddenId = [-2,-1,0,2, 3, 5, 6, 8, 9, 10, 12, 13, 15, 16, 18, 19, 20,22,23,25 ,26] + 4;
for i = hiddenId
    xtickLabels{i} = [''];
end
xtickLabels{14+4} = '1k';
xtickLabels{17+4} = '2k';
xtickLabels{21+4} = '5k';
xtickLabels{24+4} = '10k';
xtickLabels{27+4} = '20k';
ax.XTickLabel = xtickLabels;
xlim([0.5, 31.5])
grid on;
ylim([0, 70])
set(gca, 'FontName', 'Times')

%%
saveas(gcf, [folder1 '\DnT_allPos_order_' num2str(order) '.png'])
saveas(gcf, [folder1 '\DnT_allPos_order_' num2str(order) '.fig'])
close all;
save([folder1 '\DnT_allPos_order_' num2str(order) '.mat'])

end
