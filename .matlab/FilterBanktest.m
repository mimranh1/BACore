%%
clear all;clc;close all;
fname = './../../../Filter.json';
[filter] = ReadJSONUnity(fname);

%%
uiopen('D:\git\GeometricalAcoustics\Assets\BACore\matlab\BandpassFilterbank\ita_mpb_filter_oct3_44k.wav',1)
soll=single(ita_mpb_filter_oct3_44k.timeData(:));
ist=filter.filter;
f=1:44100;
figure;
subplot(2,2,1:2)
plot(soll);
hold on;
plot(ist)
hold on;
subplot(2,2,3)
% semilogx(f,10.*log10(abs(fft(ita_mpb_filter_oct3_44k.timeData,44100))));
% hold on;
semilogx(f,10.*log10(abs(filter.filterbankFreqComplex)));
hold on;
H_SetFreqPlot
subplot(2,2,4)
semilogx(f,10.*log10(abs(fft(ita_mpb_filter_oct3_44k.timeData,44100))));
hold on;
% semilogx(f,10.*log10(abs(filter.filterbankFreqComplex)));
% hold on;
H_SetFreqPlot

%%
figure;
subplot(2,1,1)
plot(real(ifft(filter.fftSignalOutFreqComplex.')));
subplot(2,1,2)
plot(real(ifft(filter.fftSignalResultFreqComplex.')));

%%
%%
fs=44100;
uiopen('D:\git\GeometricalAcoustics\Assets\BACore\matlab\BandpassFilterbank\ita_mpb_filter_oct3_44k.wav',1)
soll=single(ita_mpb_filter_oct3_44k.timeData(:));

channel=length(ita_mpb_filter_oct3_44k.channelNames);
name='ita_mpb_filter_oct3_44k';
WriteFilter(name,soll,channel,fs);

uiopen('D:\git\GeometricalAcoustics\Assets\BACore\matlab\BandpassFilterbank\ita_mpb_filter_oct1_44k.wav',1)
soll=single(ita_mpb_filter_oct1_44k.timeData(:));

channel=length(ita_mpb_filter_oct1_44k.channelNames);
name='ita_mpb_filter_oct1_44k';
WriteFilter(name,soll,channel,fs);

