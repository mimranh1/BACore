function [irFreq, freq] = Plot_FreqFromTime(ir, options, freq)

if nargin < 3
    freq = [0:length(ir) / 2 - 1] * 22050 / (length(ir) / 2 - 1);
end
if size(ir, 1) < size(ir, 2)
    ir = ir';
    warning(['Bitte Spaltenvektoren in ']);
end

irFreq = abs(fft(ir));
irFreq = irFreq(1:floor(end/2), :);


if nargin < 2
    semilogx(freq', 20*log10(irFreq));
else
    if options ~= -1
        semilogx(freq', 20*log10(irFreq), options);
    end
end

xticks([2, 5, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000]);
xticklabels({'2', '5', '10', '20', '50', '100', '200', '500', '1k', '2k', '5k', '10k', '20k'});
xlim([20, 22050])
grid on;
end
