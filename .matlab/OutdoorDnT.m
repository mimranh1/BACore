%% 
clear;
clc;
% close all;
folder='OutdoorDnt_Scene1_L12m';
folder='OutdoorDnt_Libary_L12m_Sigma1';
mkdir(folder);
% % fname = ['./../.Data/AcBuildingAcoustics/BuidlingsAcoustics_DnT_Libary_Verif2.json'];
% aaa=ReadJSONUnity(fname);
for i=0:4
fname = ['./../.Data/AcBuildingAcoustics/BuidlingsAcoustics_DnT_Libary_Verif' num2str(i) '.json'];
% fname = ['./../.Data/AcBuildingAcoustics/BuidlingsAcoustics_DnT_Pos_Order02.json'];
[newApp{i+1}] = ReadJSONUnity(fname);
end
fs = 44100;
f3rd = [0, 8, 10, 12.5, 16, 20, 25, 31.5, 40, 50, 63, 80, 100, 125, 160, 200, 250, 315, 400, 500, 630, 800, 1000, 1250, 1600, 2000, 2500, 3150, 4000, 5000, 6300, 8000, 10000, 12500, 16000, 20000, 25000];
f3rd1 = [20, 25, 31.5, 40, 50, 63, 80, 100, 125, 160, 200, 250, 315, 400, 500, 630, 800, 1000, 1250, 1600, 2000, 2500, 3150, 4000, 5000, 6300, 8000, 10000, 12500, 16000, 20000];


filterLen = length(newApp{1}.brir0.irLeft.time);
f = [1:filterLen] ./ filterLen .* fs;


%%

figure;
% irFreqq = newApp.receiverRoom.dataDirect.reverberationData.reverberationFactorComplexFreq;
irFreqq = newApp{1}.receiverRoom.dataDirect.irOutComplexFreq(1:2:end, :)'.* newApp{1}.sourceRoom0.dataDirect.irOutComplexFreq';
% irFreqq = newApp{1}.brir0.irLeft.freqComplex;
% irFreqq = newApp.receiverRoom.dataDirect.reverberationData.reverberationCutTime';
% irFreqq = newApp.receiverRoom.dataDirect.roomHrtfData.hrtfComplexFreq;
% irFreqq=newApp{1}.sourceRoom0.dataDirect.reverberationData.reverberationFactorComplexFreq.';
subplot(2,1,1)
semilogx(f, 10.*log10(abs(irFreqq)));
hold on;

[ir3rdOctave, freqsOctave] = H_GetMeanValue(abs(irFreqq));
semilogx(freqsOctave, 10.*log10(abs((ir3rdOctave))));
H_SetFreqPlot

subplot(2,1,2)
plot(real(ifft(irFreqq)));grid on;

  %%
    figure;
    Pos=[];
    factor=[];
    for i=1:length(newApp)
        %         figure;
      [incAngleDeg(i,:),factor(i), ~] = Plot_Geometry(newApp{i}, i,[4 0 0]);
        
    end
    view([0 0])
%
irFreqAll=[];
for i=1:length(newApp)
irFreqq = newApp{i}.AcBuildingAcoustics.sqrtRho0c .*sum(newApp{i}.receiverRoom.dataDirect.irOutComplexFreq(1:2:end, :).* newApp{i}.sourceRoom0.dataDirect.irOutComplexFreq,1);
% dist = mean(newApp{i}.sourceRoom0.dataDirect.roomFiData.sourceDistance);

% factor = (1./ dist)/7;
% factor=1;
% factor(i) = (newApp{i}.AcBuildingAcoustics.l2Ref2MEnergy)*2.2;
irFreqAll = [abs(irFreqq)./factor(i); irFreqAll];
end

vR = newApp{i}.receiverRoom.dataDirect.roomStatsData.volume;
Tr = newApp{i}.receiverRoom.dataDirect.roomStatsData.reverberationTime;
Sd = newApp{i}.sourceRoom0.dataDirect.roomTauDirectDataDiffuse.partitionsArea;


tauDdDiff = newApp{i}.sourceRoom0.dataDirect.roomTauDirectDataDiffuse.tauDiff.floats;
tauDd45 = newApp{i}.sourceRoom0.dataDirect.roomTauDirectDataDiffuse.tauDir45(1).floats;
tauDddiff = newApp{i}.sourceRoom0.dataDirect.roomTauDirectDataDiffuse.tauDiff(1).floats;
% tauDiffIso = newApp{i}.sourceRoom0.dataDirect.roomTauDirectDataDiffuse.tauDiffIso(1).floats;

tauSum = [tauDd45];

[~, dnt_time, dnt_tau]= PlotDnt(irFreqAll, tauSum, f3rd, Tr, vR, Sd);
% tauSum = [tauDddiff];
% dnt_time=dnt_time+2.5+1;
% [~, ~, dnt_tau_diff]= PlotDnt(irFreqAll, tauSum, f3rd, Tr, vR, Sd);

xlim([50, 5000])
ylim([0, 50])
title('dd')
%
ignoreDiff=0;
Dnt_tau_label='D_n_T (\tau_4_5_°)';
ylimm=[0 60];
Plot_DnTWithBoxplotsNew1(f3rd(6:36), dnt_time, dnt_tau(6:36), Dnt_tau_label, ylimm, ignoreDiff)
DnT_ref=dnt_time;
xlim([0.5 31.5])
ylim([0 80])
% title([num2str(factor)])
%%
saveas(gcf, [folder '\DnT_Verification.png'])
saveas(gcf, [folder '\DnT_Verification.fig'])
% % 
save([ folder '\DnT_Verification.mat'])


    %%
    figure;
    for i=1:3
        irSource= ((newApp{1}.sourceRoom0.dataDirect.reverberationData.reverberationAll(i).floats));
        allTau= newApp{1}.sourceRoom0.dataDirect.reverberationData.AllTaus.floats;
        semilogx(f,(-10.*log10(abs(irSource(1:2:end)+1j.*irSource(2:2:end)).')));
        hold on; grid on;
        H_SetFreqPlot;
    end
%     newApp{1}.sourceRoom0.dataDirect.reverberationData.reverberationCalculatorWallTaus{1
    semilogx(f,(-10.*log10(abs(allTau(1:2:end)+1j.*allTau(2:2:end)).')));
%%
% figure;semilogx(f, 10.*log10(abs(irFreqAll)));H_SetFreqPlot
% 
% figure;semilogx(f, 10.*log10(abs(newApp{i}.sourceRoom0.dataDirect.irOutComplexFreq)));H_SetFreqPlot
% figure;semilogx(f, 10.*log10(abs(newApp{i}.receiverRoom.dataDirect.irOutComplexFreq(1:2:end, :))));H_SetFreqPlot
% 
% figure;semilogx(f, 10.*log10(abs(newApp{i}.receiverRoom.dataDirect.roomHrtfData.hrtfComplexFreq(1:2:end, :))));H_SetFreqPlot
% 
% figure;semilogx(f, 10.*log10(abs(newApp{i}.receiverRoom.dataDirect.reverberationData.reverberationFactorComplexFreq)));H_SetFreqPlot
% 
% figure;plot(real(ifft(newApp{i}.receiverRoom.dataDirect.reverberationData.reverberationFactorComplexFreq.')));

% OutdoorPosSingle;
%%
% load('dntOutdoor.mat') 
% 
% vR = newApp.receiverRoom.dataDirect.roomStatsData.volume;
% Tr = newApp.receiverRoom.dataDirect.roomStatsData.reverberationTime;
% Sd = newApp.sourceRoom0.dataDirect.roomTauDirectDataDiffuse.partitionsArea;
% % irFreqq = sum(newApp.receiverRoom.dataDirect.irOutComplexFreq(1:2:end, :).* newApp.sourceRoom0.dataDirect.irOutComplexFreq,1);
% % 
% % irFreqAll = [abs(irFreqq)*3000; irFrqOld];
% tauDdDiff = newApp.sourceRoom0.dataDirect.roomTauDirectDataDiffuse.tauDiff.floats;
% tauDd45 = newApp.sourceRoom0.dataDirect.roomTauDirectDataDiffuse.tauDir45(1).floats;
% tauSum = [tauDd45];
% diff(:, 1) = PlotDnt(irFreqAll, tauSum, f3rd, Tr, vR, Sd);
% 
% xlim([50, 5000])
% ylim([0, 50])
% title('dd')