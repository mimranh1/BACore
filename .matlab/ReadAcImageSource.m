clear all;clc;
fname = './../Data/AcImageSource/hi.json';
[data] = ReadJSONUnity(fname);

ir(:,1) = data.rayDetectors.object.ImpulseResponse(1).floats;
ir(:,2) = data.rayDetectors.object.ImpulseResponse(2).floats;
%%
hold on;
plot(ir)