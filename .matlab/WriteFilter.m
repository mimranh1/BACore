function [] = WriteFilter(name,filter,channel,fs)
fileID = fopen([name '.filter.bin'],'w');
fwrite(fileID,int32(channel),'int32','n');
fwrite(fileID,int32(fs),'int32','n');
lenPerChannel = length(filter)/channel;
fwrite(fileID,int32(lenPerChannel),'int32','n');
fwrite(fileID,filter,'float','n');
fclose(fileID);


end

