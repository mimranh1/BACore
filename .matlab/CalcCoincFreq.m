function [coIncF] = CalcCoincFreq(incAngleDeg)

m=20;
B=2914.39038;
coIncF=(340^2./sind(incAngleDeg).^2).*sqrt(m./B)/(2.*pi);
end

