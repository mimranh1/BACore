function [incAngleDeg,factor, coIncF] = Plot_Geometry(newApp, i,imagePos)
for jj=1:length(newApp.sourceRoom0.dataDirect.roomFiData.positions)
    Pos1= newApp.sourceRoom0.dataDirect.roomFiData.positions(jj).Vectors;
    Pos(jj,:)=[Pos1.x Pos1.y Pos1.z];
end
sourcePos = newApp.sourceRoom0.dataDirect.roomFiData.sourcePos.vector3;
refPos=newApp.AcBuildingAcoustics.refPosition.vector3;
plot3(Pos(:,1),Pos(:,2),Pos(:,3), '*', 'DisplayName',['walls ' num2str(i)])
hold on;grid on;
plot3(sourcePos(1),sourcePos(2),sourcePos(3), 'o', 'DisplayName',['SourcePos' num2str(i)])
incAngleDeg = atand(abs(sourcePos(3)-Pos(:,3))./abs(sourcePos(1)-Pos(:,1)));
[coIncF] = CalcCoincFreq(incAngleDeg);
% d0=sqrt(sum((sourcePos-refPos-[2 0 0]).^2));
d0=sqrt(sum((sourcePos-refPos).^2));
if d0 <1
    d0=1;
end
d1=(sqrt(sum((sourcePos-refPos+imagePos).^2)));
factor=((1./d0)+(1./d1))*((1.7).^1.5);
% factor=((1./d0))*((1.7).^1.5);
% factor=((1./d0))*3;
title(num2str(incAngleDeg'));
axis equal
view([0 0])

end
