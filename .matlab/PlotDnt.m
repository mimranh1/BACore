function [diff, dnt_time, dnt_tau]=PlotDnt(irFreq,tau,f3rd,Tr,vR,Sd)
for j=1:size(irFreq,1)
[ir3rdOctave(:,j), freqsOctave(:,j)]=H_GetMeanValue(abs(irFreq(j,:))');
end
T0=0.5
dnt_time = -20 .* log10(ir3rdOctave) + 10 .* log10(Tr/0.5);
dnt_tau = -10 .* log10(tau) + 10 .* log10(0.32.*vR./Sd);
% dnt_time = -20 .* log10(ir3rdOctave) + 10.*log10(0.16*vR/(T0*Sd));
% % tauISO
% dnt_tau = -10 .* log10(tau) +  10.*log10(0.16*vR/(T0*Sd));%20.*log10(3);
% diff=dnt_time(5:25)-dnt_tau;
diff=0;
if 0
figure;
semilogx(freqsOctave, (dnt_time),'DisplayName','ir');
hold on;

semilogx(freqsOctave, mean(dnt_time,2),'DisplayName','ir mean','LineWidth',2);
semilogx(f3rd, dnt_tau,'DisplayName','tau','LineWidth',2);

H_SetFreqPlot;
ylim([0 100])
legend('Location','SE');
end
end