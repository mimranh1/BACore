clear all;clc;close all;
%%
fname = './../Data/AcBuildingAcoustics/BuidlingsAcoustics_LatencyClassical.json';
[LatIn] = ReadJSONUnity(fname);

fs = 44100;
f3rd = [50, 63, 80, 100, 125, 160, 200, 250, 315, 400, 500, 630, 800, 1000, 1250, 1600, 2000, 2500, 3150, 4000, 5000];
rho0c = 1.2*343;

%%
postfix='_VIVE';

figure;
plot(LatIn.latenciesSource,'DisplayName','Source')
hold on;
plot(LatIn.latenciesReceiver,'DisplayName','Receiver')
plot(LatIn.latenciesFilter,'DisplayName','Filter')
plot(LatIn.latenciesOffline+LatIn.latenciesInit,'DisplayName','Ofline')
legend('Location','SE')
grid on;

% saveas(gcf, ['IndoorLatency' postfix '.png'])
% saveas(gcf, ['IndoorLatency' postfix '.fig'])
% 
% save(['IndoorLatency' postfix '.mat'])

%%
limIndoor=[250:349];
lats=[];
lats(1,:)=LatIn.latenciesSource(limIndoor,1);
lats(2,:)=LatIn.latenciesReceiver(limIndoor,1);
lats(3,:)=LatIn.latenciesFilter(limIndoor,1);
lats(4,:)=LatIn.latenciesOffline(limIndoor,1)+LatIn.latenciesInit(limIndoor,1);
% lats(2,:)=LatOut.latenciesSource(limOutdoor,1);
% lats(4,:)=LatOut.latenciesReceiver(limOutdoor,1);
% lats(6,:)=LatOut.latenciesFilter(limOutdoor,1);

figure; boxplot(lats');
res=[];
res(1,:)=mean(lats');
res(2,:)=std(lats');
res=res';
res=res(:)';
