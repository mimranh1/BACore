function [diff, dnt_time, dnt_tau]=PlotDnt(irFreq,tau,f3rd)

[ir3rdOctave, freqsOctave]=H_GetMeanValue(abs(irFreq)');
dnt_time = -20 .* log10(ir3rdOctave) ;
dnt_tau = -10 .* log10(tau);
% diff=dnt_time(5:25)-dnt_tau;
diff=0;
figure;
semilogx(freqsOctave, (dnt_time),'DisplayName','ir');
hold on;

semilogx(freqsOctave, mean(dnt_time,2),'DisplayName','ir mean','LineWidth',2);
semilogx(f3rd, dnt_tau,'DisplayName','tau','LineWidth',2);

H_SetFreqPlot;
ylim([0 100])
legend('Location','SE');
end