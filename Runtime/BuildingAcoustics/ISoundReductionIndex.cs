﻿// namespace BA.BACore
// {
//     public interface ISoundReductionIndex
//     {
//       
//         /// <summary>
//         /// Return Reduction Index in dB for whole massive Wall or Patch
//         /// </summary>
//         TransferFunction ReductionIndex { get; }
//         TransferFunction EqualAbsorptionLength { get; }
//         
//         TransferFunction CalcSoundReductionIndex();
//
//     }
// }