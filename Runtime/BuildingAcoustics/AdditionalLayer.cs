﻿using UnityEngine;

namespace BA.BACore
{
    /// <summary>
    /// manage the additional Layers for the Walls
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    [System.Serializable]
    public class AdditionalLayer : MonoBehaviour
    {
        private WallBehaviour parent;
        [SerializeField] private int direction = 1;

        private Vector3 Normal => parent.Geometry.Normal;
        [SerializeField] private int id;

        /// <summary>
        /// reference to material Data
        /// </summary>
        public MaterialAddLayerData material;

        /// <summary>
        /// Thickness of Additional Layer in m
        /// </summary>
        public float thickness;

        /// <summary>
        /// reset the transform data of this game object und update material if avoidable
        /// </summary>
        public void UpdateData()
        {
            var trans = transform;
            parent = trans.parent.GetComponent<WallBehaviour>();
            var scale = parent.Geometry.Dimensions;
            var pos = Vector3.zero;
            if (Normal.x == 1)
            {
                scale.x = thickness;
                pos.x = direction * (parent.Geometry.Thickness / 2);
            }
            else if (Normal.y == 1)
            {
                scale.y = thickness;
                pos.y = direction * (parent.Geometry.Thickness / 2);
            }
            else if (Normal.z == 1)
            {
                scale.z = thickness;
                pos.z = direction * (parent.Geometry.Thickness / 2);
            }

            trans.localScale = scale;
            trans.localPosition = trans.localRotation * pos;

            if (material.material != null) GetComponent<Renderer>().material = material.material;
            // CalcDeltaRw();
        }

        /// <summary>
        /// return the Absorption coefficient of material
        /// </summary>
        public TransferFunction AbsorptionCoefficient => material.absorptionCoefficient;

        private float Density => material.density;


        /// <summary>
        /// Dynamic stiffness of the insulation layer in meganewtons per cubic metres
        /// </summary>
        public float StiffnessLayer => material.stiffness;


        public enum LiningEnum
        {
            Interior,
            Exterior,
        }

        /// <summary>
        /// 'Interior' or 'Exterior'
        /// </summary>
        public LiningEnum liningEnum;

        /// <summary>
        /// 1: For elements where the insulation layer is fixed directly to the basic construction (without studs or battens)
        /// 2: For additional layers built with metal or wooden studs or battens not directly connected to the basic structural element
        ///    where the cavity is filled with a porous insulation layer with an air resistivity r = 5 kPas/m2
        /// </summary>
        public int layerType;

        /// <summary>
        /// true: If anchors or battens are applied, (4/m2 to 10/m2), different from the reference situation, the correction true
        /// </summary>
        [Tooltip(
            "true: If anchors or battens are applied, (4/m2 to 10/m2), different from the reference situation, the correction true")]
        public bool anchors;

        /// <summary>
        /// The percentage of the area over which the layer is glued to the basic element
        /// </summary>
        public float relativeGlued = 40f;

        /// <summary>
        /// Property of Direction of the additional Layer on the wall, if direction chanced, the UpdateData will be called
        /// </summary>
        public int Direction
        {
            get => direction;
            set
            {
                if (value != direction)
                {
                    direction = value;
                    UpdateData();
                }
                else
                {
                    direction = value;
                }
            }
        }

        /// <summary>
        /// returns and update the thickness, if thickness change, UpdateData will be called
        /// </summary>
        public float Thickness
        {
            get => thickness;
            set
            {
                if (Mathf.Abs(value - thickness) < 1e-10)
                {
                    thickness = value;
                    UpdateData();
                }
                else
                {
                    thickness = value;
                }
            }
        }

        public float deltaRw;
        public float deltaRwSitu;
        public float deltaRa;
        public float deltaRaSitu;
        public float deltaRAtr;
        public float deltaRAtrSitu;

        /// <summary>
        /// returns the mass per unit area of Additional Layer
        /// </summary>
        public float Mass => Density * thickness;

        /*
        private void CalcDeltaRw()
        {
            if (parent.ReductionWeighted == 0)
                parent.CalcReductionIndexPatches();
            var Rw = parent.ReductionWeighted;
            var massElement = parent.Mass;
            var massLayer = Mass;

            // Type1: Depending on Stiffness of the Layers
            var fo1 = 1f / (2f * Mathf.PI) * Mathf.Sqrt(StiffnessLayer * (1f / massElement + 1f / massLayer));
            // Type2:
            var fo2 = 1f / (2f * Mathf.PI) * Mathf.Sqrt(0.111f / thickness * (1f / massElement + 1f / massLayer));
            var fo = layerType == 1 ? fo1 : fo2;

            deltaRw = 0;
            deltaRwSitu = 0;
            deltaRa = 0;
            deltaRaSitu = 0;
            deltaRAtr = 0;
            deltaRAtrSitu = 0;
            // Prediction of performance for interior linings
            if (liningEnum == LiningEnum.Interior)
            {
                if (Rw >= 20f && Rw <= 60f)
                {
                    if (fo >= 30f && fo <= 160f)
                        deltaRw = 74.4f - 20f * Mathf.Log10(fo) - Rw / 2f;
                    else if (fo <= 200f + 25f) //(Fo == 200)
                        deltaRw = -1f;
                    else if (fo <= 250f + 32.5f)
                        deltaRw = -3f;
                    else if (fo <= 315f + 42.5f)
                        deltaRw = -5f;
                    else if (fo <= 400f + 50f)
                        deltaRw = -7f;
                    else if (fo <= 500f + 65f)
                        deltaRw = -9f;
                    else if (fo >= 630f && fo <= 1600f)
                        deltaRw = -10f;
                    else if (fo > 1600f && fo <= 5000f)
                        deltaRw = -5f;
                    else
                        deltaRw = 0f;
                }
                else
                {
                    deltaRw = 0f;
                }
            }
            else if (massElement >= 250 && massElement <= 600 && liningEnum == LiningEnum.Exterior)
            {
                // Prediction of performance for exterior linings
                switch (material.type)
                {
                    // Mineral wool >= -4
                    case MaterialAddLayerData.LayerMaterialEnum.MineralWool:
                    {
                        deltaRw = -36f * Mathf.Log10(fo) + 82.5f;
                        deltaRw = deltaRw < -4f ? -4f : deltaRw;
                        deltaRa = -42f * Mathf.Log10(fo) + 92f;
                        deltaRa = deltaRa < -4f ? -4f : deltaRa;
                        deltaRAtr = -39f * Mathf.Log10(fo) + 87.7f;
                        deltaRAtr = deltaRAtr < -4f ? -4f : deltaRAtr;
                        break;
                    }

                    // Foam lie polystyrene (PS), extruded polystyrene (EPS) or elastified extruded polystyrene (EEPS) >= -3
                    case MaterialAddLayerData.LayerMaterialEnum.Foams:
                    {
                        deltaRw = -33f * Mathf.Log10(fo) + 76f;
                        deltaRw = deltaRw < -3f ? -3f : deltaRw;
                        deltaRa = -33f * Mathf.Log10(fo) + 74f;
                        deltaRa = deltaRa < -3f ? -3f : deltaRa;
                        deltaRAtr = -36f * Mathf.Log10(fo) + 77f;
                        deltaRAtr = deltaRAtr < -3 ? -3f : deltaRAtr;
                        break;
                    }

                    // Additional layers built with metal or wooden studs or battens not directly connected to the basic structural element
                    case MaterialAddLayerData.LayerMaterialEnum.StudsMetal:
                    {
                        deltaRw = -20f * Mathf.Log10(fo) + 48f;
                        deltaRw = deltaRw < -4f ? -4f : deltaRw;
                        deltaRa = -22f * Mathf.Log10(fo) + 51f;
                        deltaRa = deltaRa < -4f ? -4f : deltaRa;
                        deltaRAtr = -24f * Mathf.Log10(fo) + 54f;
                        deltaRAtr = deltaRAtr < -4f ? -4f : deltaRAtr;
                        break;
                    }
                }
            }
            else
            {
                Debug.LogWarning("");
                return;
            }

            if (anchors && (material.type == MaterialAddLayerData.LayerMaterialEnum.MineralWool ||
                            material.type == MaterialAddLayerData.LayerMaterialEnum.Foams))
            {
                deltaRw = 0.66f * deltaRw - 1.2f;
                deltaRa = 0.62f * deltaRa - 1.3f;
                deltaRAtr = 0.54f * deltaRAtr - 1.6f;
            }

            if (material.type == MaterialAddLayerData.LayerMaterialEnum.MineralWool ||
                material.type == MaterialAddLayerData.LayerMaterialEnum.Foams)
            {
                deltaRw = deltaRw - 0.05f * relativeGlued + 2f;
                deltaRa = deltaRa - 0.05f * relativeGlued + 2f;
                deltaRAtr = deltaRAtr - 0.05f * relativeGlued + 2f;
            }

            // Data transfer to field situation
            var a = 1.35f * Mathf.Log10(fo) - 3.5f;
            a = a > 0f ? 0f : a;
            var x = Rw - 53f;
            x = x > 7f ? 7f : x;
            x = x < -10f ? -10f : x;
            deltaRwSitu = deltaRw + a * x;
            deltaRaSitu = deltaRa + a * x;
            deltaRAtrSitu = deltaRAtr + a * x;
        }*/
    }
}