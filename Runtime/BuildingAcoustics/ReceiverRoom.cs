﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

namespace BA.BACore
{
    [Serializable]
    public abstract class ReceiverRoom
    {
        public SoundReceiverObject SoundReceiverObject => soundReceiverObject;
        [SerializeField] protected SoundReceiverObject soundReceiverObject;
        public RoomAcousticsBehaviour Room => room;
        [SerializeField] protected RoomAcousticsBehaviour room;

        [SerializeField][HideInInspector] protected int FilterLength;
        public virtual void Init(WallBehaviour[] partitions,int filterLength)
        {
            FilterLength = filterLength;
            soundReceiverObject.Init(filterLength);
        }
        
        public virtual void OnValidate()
        {
        }

        public abstract bool Update(bool forceUpdate=false);


        public abstract string Save(string path);

    }
}