﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public abstract class InterpolatorThaden : Interpolator
    {
        [Header("Interpolation")] [SerializeField]
        protected float dbDecreasePerThirdOctaveForLowerFrequencies = 1f;

        [SerializeField] protected float dbDecreasePerThirdOctaveFor5KTo10K = 2f;
        [SerializeField] protected float dbDecreasePerThirdOctaveFor10KTo20K = 3f;

        [Header("Phase")] [SerializeField] protected float phaseDelay = 0f;
        
        protected float[] TauFreqInterpolated(float[] tau3RdOctave, int filterLength)
        {
            if(tau3RdOctave.Length!= 21 && tau3RdOctave.Length!= 37)
                Debug.LogError("Invalid length of tau "+tau3RdOctave.Length);
            var tauExtrapolated = tau3RdOctave.Length == 21
                ? ExtrapolateSecondarySources(new TransferFunction(tau3RdOctave),
                    dbDecreasePerThirdOctaveForLowerFrequencies,
                    dbDecreasePerThirdOctaveFor5KTo10K, dbDecreasePerThirdOctaveFor10KTo20K)
                : new TransferFunction(tau3RdOctave);
            tauExtrapolated.Tf[0] = tauExtrapolated.Tf[1] * Mathf.Pow(10, -12.0f / 10.0f);
            var interpolationFreqBand = BaSettings.Instance.GetInterpolatedFrequencyBand(filterLength / 2);
            var tauFreq = Interpolate(tauExtrapolated, interpolationFreqBand);
            return tauFreq;
        }
    }
}