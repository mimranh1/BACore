﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class InterpolatorThadenEven : InterpolatorThaden
    {
        public override InterpolatorType Type => InterpolatorType.Even;

        public override float[] CalcSqrtTauComplex(float[] tau3RdOctave, int filterLength)
        {
            var tauFreq = tau3RdOctave.Length == filterLength/2
                ? tau3RdOctave
                : TauFreqInterpolated(tau3RdOctave, filterLength);
            return CalcSqrtTauComplexFromTauInterpolatedEven(tauFreq, "", 1, phaseDelay);
        }

        /// <summary>
        /// Mirror odd Tau and add phase 
        /// </summary>
        /// <param name="tau">input interpolated tau</param>
        /// <param name="logName"></param>
        /// <param name="tauFactor">factor const for tau</param>
        /// <param name="phaseFactor">const factor phase</param>
        /// <returns>mirrored tau with phase</returns>
        public static float[] CalcSqrtTauComplexFromTauInterpolatedEven(float[] tau, string logName = "",
            float tauFactor = 1,
            float phaseFactor = 1)
        {
            var lengthTau = 2 * tau.Length;
            var lengthM = lengthTau / 2;
            var tauFreqComplexLocal = new float[2 * lengthTau];
            DataLogger.Log(logName + "NewApproach.phase", phaseFactor);
            for (var k = 0; k < lengthTau; k++)
            {
                var phase = -phaseFactor * lengthM * 2 * Math.PI * k / BaSettings.Instance.SampleRate;

                // Mirror Signal
                var magnitude = k < tau.Length ? tau[k] : tau[tau.Length - k % tau.Length - 1];

                magnitude = Mathf.Sqrt(Mathf.Abs(magnitude));
                magnitude *= tauFactor;
                tauFreqComplexLocal[2 * k] = (float) (magnitude * Math.Cos(phase));
                tauFreqComplexLocal[2 * k + 1] = (float) (magnitude * Math.Sin(phase));
            }

            return tauFreqComplexLocal;
        }
    }
}