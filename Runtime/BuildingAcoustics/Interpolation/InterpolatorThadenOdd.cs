﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class InterpolatorThadenOdd : InterpolatorThaden
    {
        public override InterpolatorType Type => InterpolatorType.Odd;

        public override float[] CalcSqrtTauComplex(float[] tau3RdOctave, int filterLength)
        {
            var tauFreq = tau3RdOctave.Length == filterLength
                ? tau3RdOctave
                : TauFreqInterpolated(tau3RdOctave, filterLength);
            return CalcTauComplexFromTauInterpolatedOdd(tauFreq, phaseDelay, 1 );
        }

        /// <summary>
        /// Mirror odd Tau and add phase 
        /// </summary>
        /// <param name="tau">input interpolated tau</param>
        /// <param name="tauFactor">factor const for tau</param>
        /// <returns>mirrored tau with phase</returns>
        private static float[] CalcTauComplexFromTauInterpolatedOdd(float[] tau, float phaseFactor, float tauFactor = 1)
        {
            var lengthTau = 2 * tau.Length - 1;
            var lengthM = (lengthTau - 1) / 2;
            var tauFreqComplexLocal = new float[2 * lengthTau];

            for (var k = 0; k < lengthTau; k++)
            {
                var phase = -phaseFactor * lengthM * 2 * Math.PI * k / BaSettings.Instance.SampleRate;

                // Mirror Signal
                var magnitude = k < tau.Length ? tau[k] : tau[tau.Length - k % tau.Length - 2];

                magnitude = Mathf.Sqrt(Mathf.Abs(magnitude));
                magnitude *= tauFactor;
                tauFreqComplexLocal[2 * k] = (float) (magnitude * Math.Cos(phase));
                tauFreqComplexLocal[2 * k + 1] = (float) (magnitude * Math.Sin(phase));
            }

            return tauFreqComplexLocal;
        }


    }
}