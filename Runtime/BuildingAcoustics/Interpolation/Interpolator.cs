﻿using System;
using log4net;
using TestMySpline;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public abstract class Interpolator : ExtendedScriptableObject<Interpolator, InterpolatorType>
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(Interpolator));
        public abstract float[] CalcSqrtTauComplex(float[] tau3RdOctave, int filterLength);
       
        protected TransferFunction ExtrapolateSecondarySources(TransferFunction input,
            float dbDecreasePerThirdOctaveForLowerFrequencies,
            float dbDecreasePerThirdOctaveFor5KTo10K, float dbDecreasePerThirdOctaveFor10KTo20K)
        {
            var y = new float[37];
            input.Tf.CopyTo(y, 9);

            y[8] = y[9] * Mathf.Pow(10, -3.0f / 10.0f); // 40 Hz
            y[7] = y[8] * Mathf.Pow(10, -6.0f / 10.0f); // 31.5 Hz
            y[6] = y[7] * Mathf.Pow(10, -9.0f / 10.0f); // 25 Hz
            y[5] = y[6] * Mathf.Pow(10, -12.0f / 10.0f); // 20 Hz
            y[4] = y[5] * Mathf.Pow(10, -12.0f / 10.0f); // 16 Hz      
            y[3] = y[4] * Mathf.Pow(10, -12.0f / 10.0f); // 12.5 Hz
            y[2] = y[3] * Mathf.Pow(10, -12.0f / 10.0f); // 10 Hz
            y[1] = y[2] * Mathf.Pow(10, -12.0f / 10.0f); // 8 Hz
            y[0] = y[1] * Mathf.Pow(10, -12.0f / 10.0f); // 0 Hz

            /*
            y[8] = y[9] * Mathf.Pow(10, -dbDecreasePerThirdOctaveForLowerFrequencies / 10);       // 40 Hz
            y[7] = y[8] * Mathf.Pow(10, -dbDecreasePerThirdOctaveForLowerFrequencies / 10);       // 31.5 Hz
            y[6] = y[7] * Mathf.Pow(10, -dbDecreasePerThirdOctaveForLowerFrequencies / 10);       // 25 Hz
            y[5] = y[6] * Mathf.Pow(10, -dbDecreasePerThirdOctaveForLowerFrequencies / 10);      // 20 Hz
            y[4] = y[5] * Mathf.Pow(10, -dbDecreasePerThirdOctaveForLowerFrequencies / 10);      // 16 Hz      
            y[3] = y[4] * Mathf.Pow(10, -dbDecreasePerThirdOctaveForLowerFrequencies / 10);      // 12.5 Hz
            y[2] = y[3] * Mathf.Pow(10, -dbDecreasePerThirdOctaveForLowerFrequencies / 10);      // 10 Hz
            y[1] = y[2] * Mathf.Pow(10, -dbDecreasePerThirdOctaveForLowerFrequencies / 10);      // 8 Hz
            y[0] = y[1] * Mathf.Pow(10, -dbDecreasePerThirdOctaveForLowerFrequencies / 10);      // 0 Hz
           
            y[8] = 0;
            y[7] = 0;
            y[6] = 0;
            y[5] = 0;
            y[4] = 0;
            y[3] = 0;
            y[2] = 0;
            y[1] = 0;
            y[0] = 0;
            */

            y[30] = y[29] * Mathf.Pow(10, -dbDecreasePerThirdOctaveFor5KTo10K / 10.0f);
            y[31] = y[30] * Mathf.Pow(10, -dbDecreasePerThirdOctaveFor5KTo10K / 10.0f);
            y[32] = y[31] * Mathf.Pow(10, -dbDecreasePerThirdOctaveFor5KTo10K / 10.0f);

            y[33] = y[32] * Mathf.Pow(10, -dbDecreasePerThirdOctaveFor10KTo20K / 10.0f);
            y[34] = y[33] * Mathf.Pow(10, -dbDecreasePerThirdOctaveFor10KTo20K / 10.0f);
            y[35] = y[34] * Mathf.Pow(10, -dbDecreasePerThirdOctaveFor10KTo20K / 10.0f);
            y[36] = y[35] * Mathf.Pow(10, -dbDecreasePerThirdOctaveFor10KTo20K / 10.0f);

            foreach (var t in y)
                if (t > 1)
                    Log.Error("Extrapolated Tau should not be bigger than 1!");

            return new TransferFunction(y);
        }

        /// <summary>
        /// Interpolate tf with input array
        /// Octave20 = [0 8 10 12.5 16 20 25 31.5 40 50 63 80 100 125 160 200 250 315 400 500 630 800 1000 1250 1600 2000 2500 3150 4000 5000 6300 8000 10000 12500 16000 20000 25000];
        /// </summary>
        /// <param name="input"></param>
        /// <param name="xs">input array for x</param>
        /// <returns>new Interpolated TransferFunction</returns>
        protected float[] Interpolate(TransferFunction input, TransferFunction xs)
        {
            var x = BaSettings.Instance.ThirdOctaveFrequencyBandExtended.Tf;
            return CubicSpline.Compute(x, input.Tf, xs.Tf);
        }
    }
}