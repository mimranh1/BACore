﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class AttachedWallData
    {
        [ReadOnly] public WallBehaviour WallBehaviour;
        [ReadOnly]public float CriticalFrequency;
        [ReadOnly] public float Mass;
        [HideInInspector]public int id;

        public AttachedWallData(WallBehaviour wallBehaviour)
        {
            WallBehaviour = wallBehaviour;
            // WallBehaviour.Awake();
            id = wallBehaviour.gameObject.GetInstanceID();
            Mass = WallBehaviour.Mass;
            CriticalFrequency = WallBehaviour.CriticalFrequency;
        }
    }
}