﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.Analytics;

namespace BA.BACore
{
    public class Junctions
    {
        public int numJunctions => 4;
        [SerializeReference] public AttachedWalls[] AttachedWalls;
        [SerializeField] public TransferPaths[] Rij;
        
        public Junctions(Vector3 wallPosition, Vector3 wallDimension, Vector3 wallNormal,
            GeometryDetectionType detectionType)
        {
            AttachedWalls = new AttachedWalls[4];
            Rij = new TransferPaths[4];
            GetDirectionData(wallNormal, out var directions, out var junctionNormals);
            for (var i = 0; i < 4; i++)
            {
                var junctionPosition = GetJunctionPosition(wallPosition, wallDimension, directions[i]);
                var junctionLength = Vector3.Dot(junctionNormals[i], wallDimension);
                Debug.Log("junctionPosition: " + junctionPosition + " junctionNormal: " + junctionNormals[i]);
                AttachedWalls[i] = new AttachedWalls(0, "name", junctionPosition, junctionNormals[i], junctionLength,
                    detectionType);
            }
        }

        public void OnValidate()
        {

        }

        public TransferPaths[] CalculateFlanking(float partitionArea)
        {
            var rijs = new TransferPaths[4];
            for (var i = 0; i < 4; i++)
            {
                rijs[i] = AttachedWalls[i].CalcRij(partitionArea);
            }

            Rij = rijs;
            return rijs;
        }

        public TransferPaths[] CalcFlankingFactors()
        {
            var rijs = new TransferPaths[4];
            for (var i = 0; i < 4; i++)
            {
                rijs[i] = AttachedWalls[i].CalcFlankingFactors();
            }

            Rij = rijs;
            return rijs;
        }


     
        private int GetWallIndex(AttachedWalls attachedWalls, RoomAcousticsBehaviour room,int[] partitionId)
        {
            var walls = room.RoomGeometry.WallBehaviours;
            foreach (var wall in walls)
            {
                var sourceId = attachedWalls.FindWallInstanceIndex(wall.GetInstanceID());
                if (sourceId == -1)
                    continue;
                var contains = false;
                foreach (var id in partitionId)
                {
                    if (id == sourceId)
                        contains = true;
                }
                if (!contains)
                {
                    return sourceId;
                }
            }

            Debug.LogError("no Wall found");
            return -1;
        }
        
        public int GetWallIndex(AttachedWalls attachedWalls, RoomAcousticsBehaviour room,int partitionId)
        {
            var walls = room.RoomGeometry.WallBehaviours;
            for (var iWallRoom = 0; iWallRoom < walls.Length; iWallRoom++)
            {
                for (var iAttachedWall = 0; iAttachedWall < attachedWalls.NumberOfWalls; iAttachedWall++)
                {
                    var indexAttaches = attachedWalls.FindWallInstanceIndex(walls[iWallRoom].GetInstanceID());
                    if (indexAttaches != -1 && partitionId != indexAttaches)
                        return indexAttaches;

                }
            }
        

            Debug.LogError("no wall found");
            return -1;
        }

        private void GetDirectionData(Vector3 wallNormal, out Vector3[] directions, out Vector3[] junctionNormal)
        {
            if (Math.Abs(wallNormal.x - 1) < float.Epsilon)
            {
                directions = new[] {Vector3.up, Vector3.forward, Vector3.down, Vector3.back};
                junctionNormal = new[] {Vector3.forward, Vector3.up, Vector3.forward, Vector3.up};
            }
            else if (Math.Abs(wallNormal.y - 1) < float.Epsilon)
            {
                directions = new[] {Vector3.forward, Vector3.right, Vector3.back, Vector3.left};
                junctionNormal = new[] {Vector3.right, Vector3.forward, Vector3.right, Vector3.forward};
            }
            else if (Math.Abs(wallNormal.z - 1) < float.Epsilon)
            {
                directions = new[] {Vector3.right, Vector3.up, Vector3.left, Vector3.down};
                junctionNormal = new[] {Vector3.up, Vector3.right, Vector3.up, Vector3.right};
            }
            else
            {
                Debug.LogError("invalid Wall Normal " + wallNormal);
                directions = new Vector3[0];
                junctionNormal = new Vector3[0];
            }
        }


        /// <summary>
        ///     find all attached Junctions from Parent Wall (transform)
        /// </summary>
        /// <returns>normal vector of wall</returns>
        private Vector3 GetJunctionPosition(Vector3 position, Vector3 dimension, Vector3 direction)
        {
            return Vector3E.MultiplyElementwise((dimension / 2), direction) + position +
                   Vector3E.MultiplyElementwise(direction, 0.1f);

        }

        public void OnDrawGizmosSelected()
        {
            Gizmos.color=new Color(1f,0f,0f,0.4f);
            foreach (var attachedWall in AttachedWalls)
            {
                foreach (var data in attachedWall.AttachedWallDatas)
                {
                    Gizmos.DrawRay(attachedWall.position, (data.WallBehaviour.Geometry.Position - attachedWall.position).normalized);
                }
            }
            
        }
    }
}