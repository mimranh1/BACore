﻿using UnityEngine;

namespace BA.BACore
{
    public class SourceRoomFlankingDataIndoorSegments : SourceRoomFlankingDataIndoor
    {
        public override SourceRoomFlankingDataType Type => SourceRoomFlankingDataType.IndoorSegments;
        public override RoomFiData RoomFiData => roomFiData;
        [SerializeField] [HideInInspector] private RoomFiDataSegments roomFiData;

        public override void InitDataArray(TransferPath flankingPath, RoomAcousticsBehaviour sourceRoom,RoomAcousticsBehaviour receiverRoom, Interpolator interpolator,
            int filterLength, int numSoundObjects = 1)
        {
            roomFiData=new RoomFiDataSegments();
            var wallBehaviours = sourceRoom.RoomGeometry.WallBehaviours;
            RoomFiData.InitDataArray(sourceRoom, filterLength, numSoundObjects, wallBehaviours,interpolator);
            base.InitDataArray(flankingPath, sourceRoom, receiverRoom,interpolator, filterLength, numSoundObjects);
        }
    }
}