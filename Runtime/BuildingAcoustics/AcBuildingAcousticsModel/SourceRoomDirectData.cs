﻿namespace BA.BACore
{
    public abstract class SourceRoomDirectData : ExtendedScriptableObject<SourceRoomDirectData,SourceRoomDirectDataType>
    {
        public abstract bool NeedProcess { get; }
        public abstract int GetIndex(short iWall, short iSource,short iPatch);

        public abstract void InitDataArray(WallBehaviour[] partitions, RoomAcousticsBehaviour roomBehaviour,
            Interpolator interpolator, int filterLength, int numSoundObjects = 1);

        public abstract void UpdatePositionAndRotation(bool applyReverberation, SoundSourceObject[] soundSourceObjects);
        
        public abstract void Save(string path, int i = 0);

        public abstract FloatArrays[] IrOutComplexFreq { get; }

        public abstract void CalcTau();
    }
}