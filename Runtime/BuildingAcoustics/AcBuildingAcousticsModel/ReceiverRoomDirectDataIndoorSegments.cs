﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class ReceiverRoomDirectDataIndoorSegments : ReceiverRoomDirectDataIndoor
    {
        public override ReceiverRoomDirectDataType Type => ReceiverRoomDirectDataType.IndoorSegments;
        public override RoomHrtfData RoomHrtfData => roomHrtfData;
        [SerializeField] [HideInInspector] private RoomHrtfDataSegments roomHrtfData;

        public override void InitDataArray(RoomAcousticsBehaviour roomBehaviour, WallBehaviour[] partitions,
            Interpolator interpolator,
            int filterLength, int numSoundObjects = 1)
        {
            roomHrtfData = new RoomHrtfDataSegments();
            base.InitDataArray(roomBehaviour, partitions, interpolator, filterLength, numSoundObjects);
        }
    }
}