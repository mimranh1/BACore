﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace BA.BACore
{
    public class SourceRoomDirectDataOutdoor : SourceRoomDirectData
    {
        public override SourceRoomDirectDataType Type => SourceRoomDirectDataType.Outdoor;
        public override bool NeedProcess => true;

        [SerializeField][HideInInspector]private RoomFiDataPatch roomFiData;
        [SerializeField][HideInInspector]private RoomTauDirectDataPatchesDiffuse roomTauDirectDataDiffuse;
        [SerializeField][HideInInspector]private ReverberationDataTauRealtime reverberationData;
        [SerializeField][HideInInspector]private RoomStatsData roomStatsData;
        public override FloatArrays[] IrOutComplexFreq => irOutComplexFreq;
        [SerializeField][HideInInspector] protected FloatArrays[] irOutComplexFreq;

        
        public override int GetIndex(short iWall, short iSource,short iPatch)
        {
            return roomFiData.GetIndex(iWall, iSource,iPatch);
        }

        public override void InitDataArray(WallBehaviour[] partitions, RoomAcousticsBehaviour roomBehaviour, Interpolator interpolator,
            int filterLength, int numSoundObjects = 1)
        {
            roomStatsData = new RoomStatsData();
            roomStatsData.InitDataArray(roomBehaviour.RoomAcousticProperties);

            roomTauDirectDataDiffuse = new RoomTauDirectDataPatchesDiffuse();
            roomTauDirectDataDiffuse.InitDataArray(partitions, interpolator, filterLength);
            roomTauDirectDataDiffuse.CalcTau();

            reverberationData = new ReverberationDataTauRealtime();
            roomBehaviour.CalcReverberation();
            reverberationData.InitDataArray(roomBehaviour, filterLength, (1 / (Mathf.PI * 4f)), 0,
                roomTauDirectDataDiffuse);

            roomFiData = new RoomFiDataPatch();
            roomFiData.InitDataArray(roomBehaviour, filterLength, numSoundObjects, partitions,interpolator);
            
            irOutComplexFreq = new FloatArrays[roomFiData.fiComplexFreq.Length];
            for (var i = 0; i < irOutComplexFreq.Length; i++)
                irOutComplexFreq[i] = new FloatArrays(filterLength * 2);
        }

        public override void UpdatePositionAndRotation(bool applyReverberation, SoundSourceObject[] soundSourceObjects)
        {
            roomFiData.UpdateFi(soundSourceObjects);
            reverberationData.UpdateReverberation();

            for (short iSource = 0; iSource < soundSourceObjects.Length; iSource++)
            {
                for (short iWall = 0; iWall < roomFiData.numWalls; iWall++)
                {
                    for (short iPatch = 0; iPatch < roomFiData.numPatches[iWall]; iPatch++)
                    {
                        var index = GetIndex(iWall, iSource, iPatch);
                        if (applyReverberation)
                            ComplexFloats.Add(roomFiData.fiComplexFreq[index].floats,
                                reverberationData.reverberationFactorComplexFreq[index].floats,
                                irOutComplexFreq[index].floats);
                        else
                            Array.Copy(roomFiData.fiComplexFreq[index].floats, irOutComplexFreq[index].floats,
                                irOutComplexFreq[index].floats.Length);
                    }
                }
            }
        }

        public override void CalcTau()
        {
            roomTauDirectDataDiffuse.CalcTau();
        }

        public override void Save(string path, int i=0)
        {
            System.IO.File.WriteAllText($"{path}/SourceRoomDirect{i}.json", JsonUtility.ToJson(this));
        }
    }
}