﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public abstract class ReceiverRoomFlankingDataIndoor : ReceiverRoomFlankingData
    {
        public override FloatArrays[] IrOutComplexFreq => irOutComplexFreq;
        [SerializeField][HideInInspector] protected FloatArrays[] irOutComplexFreq;
        public abstract RoomHrtfData RoomHrtfData { get; }

        [SerializeField][HideInInspector] protected RoomStatsData roomStatsData;
        [SerializeField][HideInInspector] protected ReverberationData reverberationData;
        [SerializeField][HideInInspector] protected RoomTauFlankingData roomTauFlankingData;
        
        public override void InitDataArray(RoomAcousticsBehaviour roomBehaviour, Interpolator interpolator, int filterLength, int numSoundObjects=1)
        {
            roomStatsData = new RoomStatsData();
            roomStatsData.InitDataArray(roomBehaviour.RoomAcousticProperties);
            
            reverberationData = new ReverberationData();
            reverberationData.InitDataArray(roomBehaviour.Reverberation, filterLength, Mathf.Sqrt(4 / roomStatsData.equivalentAbsorptionArea),roomBehaviour.RoomAcousticProperties.DmfpSamples);
     
            roomTauFlankingData = new RoomTauFlankingData();
            roomTauFlankingData.InitDataArray(roomBehaviour, interpolator, filterLength);
            roomTauFlankingData.CalcTauForFlanking();

            var wallBehaviours = roomBehaviour.RoomGeometry.WallBehaviours;
            var numWalls = wallBehaviours.Length;
            RoomHrtfData.InitDataArray(wallBehaviours,filterLength);
            
            irOutComplexFreq = new FloatArrays[numWalls * 2];
            for (var i = 0; i < numWalls; i++)
            {
                irOutComplexFreq[2 * i] = new FloatArrays(filterLength * 2);
                irOutComplexFreq[2 * i + 1] = new FloatArrays(filterLength * 2);
            }
        }

        public override void Save(string path, int i = 0)
        {
            var potion = JsonUtility.ToJson(this);
            System.IO.File.WriteAllText($"{path}/ReceiverRoomFlanking.json", potion);
        }

        public override void UpdatePositionAndRotation(bool applyReverberation, SoundReceiverObject soundReceiverObject)
        {
            RoomHrtfData.UpdateHrtf(soundReceiverObject);
            if (applyReverberation)
            {
                var summandComplex = reverberationData.reverberationFactorComplexFreq;
                    var length = summandComplex.Length;
                // Parallel.For( 0, RoomHrtfData.numWalls, i=>
                for (var i = 0; i < RoomHrtfData.numWalls; i++)
                {
                    var xFactorComplexLeft = RoomHrtfData.hrtfComplexFreq[2 * i].floats;
                    var xFactorComplexRight = RoomHrtfData.hrtfComplexFreq[2 * i + 1].floats;
                    var multiplyAllComplex = roomTauFlankingData.sqrtSqrtTauAlphaComplexFreq[i].floats;
                    var resultLeft = irOutComplexFreq[2 * i].floats;
                    var resultRight = irOutComplexFreq[2 * i + 1].floats;
                    for (var n = 0; n < length; n += 2)
                    {
                        var sumReal = summandComplex[n];
                        var sumImag = summandComplex[n + 1];

                        var leftReal = xFactorComplexLeft[n] + sumReal;
                        var leftImag = xFactorComplexLeft[n + 1] + sumImag;
                        var rightReal = xFactorComplexRight[n] + sumReal;
                        var rightImag = xFactorComplexRight[n + 1] + sumImag;
                        var tauReal = multiplyAllComplex[n];
                        var tauImag = multiplyAllComplex[n + 1];

                        resultLeft[n] = (leftReal * tauReal - leftImag * tauImag);
                        resultLeft[n + 1] = (leftReal * tauImag + leftImag * tauReal);
                        resultRight[n] = (rightReal * tauReal - rightImag * tauImag);
                        resultRight[n + 1] = (rightReal * tauImag + rightImag * tauReal);
                    }
                } //);
            }
            else
                for (var i = 0; i < RoomHrtfData.numWalls; i++)
                {
                    ComplexFloats.Multiply(RoomHrtfData.hrtfComplexFreq[2 * i].floats,
                        roomTauFlankingData.sqrtSqrtTauAlphaComplexFreq[i].floats,
                        irOutComplexFreq[2 * i].floats);
                    ComplexFloats.Multiply(RoomHrtfData.hrtfComplexFreq[2 * i + 1].floats,
                        roomTauFlankingData.sqrtSqrtTauAlphaComplexFreq[i].floats,
                        irOutComplexFreq[2 * i + 1].floats);
                }
        }


        public override void CalcTau()
        {
            roomTauFlankingData.CalcTauForFlanking();
        }
    }
}