﻿using System;

namespace BA.BACore
{
    [Serializable]
    public class ReceiverRoomFlankingDataSelector:Selector<ReceiverRoomFlankingData,ReceiverRoomFlankingDataType>{}
}