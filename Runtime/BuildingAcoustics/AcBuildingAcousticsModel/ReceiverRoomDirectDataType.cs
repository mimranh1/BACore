﻿namespace BA.BACore
{
    public enum ReceiverRoomDirectDataType
    {
        None,
        Indoor,
        IndoorSegments,
        IndoorPatches
    }
}