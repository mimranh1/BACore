﻿namespace BA.BACore
{
    public enum SourceRoomDirectDataType
    {
        Indoor,
        IndoorSegments,
        Outdoor,
        OutdoorAngleTau
    }
}