﻿using System;

namespace BA.BACore
{
    [Serializable]
    public class ReceiverRoomDirectDataNone:ReceiverRoomDirectData
    {
        public override ReceiverRoomDirectDataType Type => ReceiverRoomDirectDataType.None;

        public override FloatArrays[] IrOutComplexFreq => null;

        public override void InitDataArray(RoomAcousticsBehaviour roomBehaviour, WallBehaviour[] partitions, Interpolator interpolator,
            int filterLength, int numSoundObjects = 1)
        {
        }

       

        public override void UpdatePositionAndRotation(bool applyReverberation, SoundReceiverObject soundReceiverObject)
        {
        }
    }
}