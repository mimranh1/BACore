﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class RoomFiDataSegments : RoomFiData
    {
        [SerializeField][HideInInspector] private GeometrySegments[] secondarySources;
        [SerializeField][HideInInspector] private Vector3S[] positions;
        [SerializeField][HideInInspector] private Vector3S[] normals;
        [SerializeField][HideInInspector] private int[] numSegments;

        public override void InitDataArray(RoomGeometry roomGeometry, int filterLength, int numSoundObjects,
            WallBehaviour[] wallBehaviours, Interpolator interpolator)
        {
            base.InitDataArray(roomGeometry, filterLength, numSoundObjects, wallBehaviours,interpolator);
            var numWall = wallBehaviours.Length;
            secondarySources = new GeometrySegments[numWalls];
            positions = new Vector3S[numWalls];
            normals = new Vector3S[numWalls];
            numSegments = new int[numWalls];
            for (var i = 0; i < numWall; i++)
            {
                var sourceSegments = wallBehaviours[i].WallSourceSegments;
                var geometrySses = sourceSegments.SecondarySources;
                secondarySources[i] = new GeometrySegments(geometrySses.ToArray());
                numSegments[i] = secondarySources[i].segments.Length;
                positions[i] = new Vector3S(sourceSegments.Positions);
                normals[i] = new Vector3S(sourceSegments.Normals);
                Debug.Log($"init Source wall {wallBehaviours[i].name} with {numSegments[i]} Secondary Sources.");
            }
        }

        public override void UpdateFi(SoundSourceObject[] soundSourceObjects)
        {
            for (short iSource = 0; iSource < numSources; iSource++)
            {
                for (short iWall = 0; iWall < numWalls; iWall++)
                {
                    var index = GetIndex(iWall, iSource);
                    soundSourceObjects[iSource].GetFiBySqrtSi4PiSum(false, fiComplexFreq[index].floats,
                        secondarySources[iWall], 1f, true, true);
                }
            }
        }
    }
}