﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class SourceRoomDirectDataIndoorWall : SourceRoomDirectDataIndoor
    {
        public override SourceRoomDirectDataType Type => SourceRoomDirectDataType.Indoor;
        
        protected override RoomFiData RoomFiData => roomFiData;
        [SerializeField][HideInInspector] private RoomFiDataWall roomFiData;

        public override int GetIndex(short iWall, short iSource, short iPatch)
        {
            return roomFiData.GetIndex(iWall, iSource, iPatch);
        }

        public override void InitDataArray(WallBehaviour[] partitions, RoomAcousticsBehaviour roomBehaviour, Interpolator interpolator,
            int filterLength, int numSoundObjects = 1)
        {
            roomFiData.InitDataArray(roomBehaviour, filterLength, numSoundObjects, partitions,interpolator);
            base.InitDataArray(partitions, roomBehaviour, interpolator, filterLength, numSoundObjects);
        }
    }
}