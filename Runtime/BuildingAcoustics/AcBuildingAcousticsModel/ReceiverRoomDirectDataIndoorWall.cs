﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class ReceiverRoomDirectDataIndoorWall : ReceiverRoomDirectDataIndoor
    {
        public override ReceiverRoomDirectDataType Type => ReceiverRoomDirectDataType.Indoor;
        public override RoomHrtfData RoomHrtfData => roomHrtfData;     
        [SerializeField] [HideInInspector] private RoomHrtfDataWall roomHrtfData;

        public override void InitDataArray(RoomAcousticsBehaviour roomBehaviour, WallBehaviour[] partitions, Interpolator interpolator,
            int filterLength, int numSoundObjects = 1)
        {
            roomHrtfData = new RoomHrtfDataWall();
            base.InitDataArray(roomBehaviour, partitions, interpolator, filterLength, numSoundObjects);
        }
    }
}