﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public abstract class RoomFiData 
    {
        [HideInInspector] public FloatArrays[] fiComplexFreq;
        [HideInInspector] public int numWalls;
        [HideInInspector] public int numSources;
        [SerializeField][HideInInspector] protected Dictionary<long, int> keys;

        
        public virtual void InitDataArray(RoomGeometry roomGeometry, int filterLength, int numSoundObjects,
            WallBehaviour[] wallBehaviours, Interpolator interpolator)
        {
            keys = new Dictionary<long, int>();
            numSources = numSoundObjects;
            numWalls = wallBehaviours.Length;
            fiComplexFreq = new FloatArrays[numWalls * numSources];
            var i = 0;
            for (var iWall = 0; iWall < numWalls ; iWall++)
            for (var iSource = 0; iSource <  numSources; iSource++)
            {
                var index = ((long) iWall << 32) | ((long)iSource << 16);
                keys.Add(index, i);
                fiComplexFreq[i] = new FloatArrays(filterLength * 2);
                i++;
            }
        }

        public abstract void UpdateFi(SoundSourceObject[] soundSourceObjects);

        public virtual int GetIndex(short iWall, short iSource, short iPatch=0)
        {
            var index = ((long) iWall << 32) | ((long)iSource << 16) | ((long)iPatch); 
            return keys[index];
        }
    }
}