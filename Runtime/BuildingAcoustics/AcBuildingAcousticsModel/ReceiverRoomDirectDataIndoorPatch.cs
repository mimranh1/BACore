﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class ReceiverRoomDirectDataIndoorPatch : ReceiverRoomDirectData
    {
        public override ReceiverRoomDirectDataType Type => ReceiverRoomDirectDataType.IndoorPatches;
        
        public override FloatArrays[] IrOutComplexFreq => irOutComplexFreq;
        [SerializeField][HideInInspector] protected FloatArrays[] irOutComplexFreq;
        

        [SerializeField] [HideInInspector] private RoomHrtfDataPatch roomHrtfData;

        public RoomStatsData roomStatsData;
        public ReverberationDataPatches reverberationData;

        public override void InitDataArray(RoomAcousticsBehaviour roomBehaviour, WallBehaviour[] partitions,
            Interpolator interpolator,
            int filterLength, int numSoundObjects = 1)
        {
            roomStatsData = new RoomStatsData();
            roomStatsData.InitDataArray(roomBehaviour.RoomAcousticProperties);

            roomHrtfData = new RoomHrtfDataPatch();
            roomHrtfData.InitDataArray(partitions, filterLength);
            
            reverberationData = new ReverberationDataPatches();
            var reverberations = new FloatArrays[roomHrtfData.hrtfComplexFreq.Length / 2];
            var index = 0;
            foreach (var partition in partitions)
            {
                for (var iPatch = 0; iPatch < partition.patchesHandler.NumOfPatches; iPatch++)
                {
                    reverberations[index] = new FloatArrays(roomBehaviour.Reverberation);
                    roomBehaviour.CalcReverberation();
                    index++;
                }
            }
            reverberationData.InitDataArray(reverberations, filterLength, Mathf.Sqrt(4 / roomStatsData.equivalentAbsorptionArea),roomBehaviour.RoomAcousticProperties.DmfpSamples);


            irOutComplexFreq = new FloatArrays[roomHrtfData.hrtfComplexFreq.Length];
            for (var i = 0; i < irOutComplexFreq.Length; i++)
            {
                irOutComplexFreq[i] = new FloatArrays(filterLength * 2);
            }
        }


        public override void UpdatePositionAndRotation(bool applyReverberation, SoundReceiverObject soundReceiverObject)
        {
            roomHrtfData.UpdateHrtf(soundReceiverObject);
            if (applyReverberation)
                for (var i = 0; i < roomHrtfData.hrtfComplexFreq.Length/2; i++)
                {
                    ComplexFloats.Add(roomHrtfData.hrtfComplexFreq[2 * i].floats,
                         reverberationData.reverberationFactorComplexFreq[i].floats,
                        irOutComplexFreq[2 * i].floats);
                    ComplexFloats.Add(roomHrtfData.hrtfComplexFreq[2 * i + 1].floats,
                         reverberationData.reverberationFactorComplexFreq[i].floats,
                        irOutComplexFreq[2 * i + 1].floats);
                }
            else
                for (var i = 0; i < roomHrtfData.hrtfComplexFreq.Length/2; i++)
                {
                    ComplexFloats.Copy(roomHrtfData.hrtfComplexFreq[2 * i].floats,
                         irOutComplexFreq[2 * i].floats);
                    ComplexFloats.Copy(roomHrtfData.hrtfComplexFreq[2 * i + 1].floats,
                         irOutComplexFreq[2 * i + 1].floats);
                }
        }
    }
}