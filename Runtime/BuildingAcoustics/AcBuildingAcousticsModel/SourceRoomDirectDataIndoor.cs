﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace BA.BACore
{
    [Serializable]
    public abstract class SourceRoomDirectDataIndoor : SourceRoomDirectData
    {
        [SerializeField][HideInInspector] protected FloatArrays[] irOutComplexFreq;
        public override bool NeedProcess => true;

        protected abstract RoomFiData RoomFiData { get; }
        [SerializeField][HideInInspector] protected RoomTauDirectDataDiffuse roomTauDirectDataDiffuse;
        [SerializeField][HideInInspector] protected ReverberationData reverberationData;
        [SerializeField][HideInInspector] protected RoomStatsData roomStatsData;

        public override FloatArrays[] IrOutComplexFreq => irOutComplexFreq;


        public int GetIndex(short iWall, short iSource)
        {
            return RoomFiData.GetIndex(iWall, iSource);
        }

        public override void InitDataArray(WallBehaviour[] partitions, RoomAcousticsBehaviour roomBehaviour, Interpolator interpolator,
            int filterLength, int numSoundObjects = 1)
        {
            roomStatsData = new RoomStatsData();
            roomStatsData.InitDataArray(roomBehaviour.RoomAcousticProperties);

            reverberationData = new ReverberationData();
            reverberationData.InitDataArray(roomBehaviour.Reverberation, filterLength, Mathf.Sqrt(1 / roomStatsData.equivalentAbsorptionArea),roomBehaviour.RoomAcousticProperties.DmfpSamples);

            roomTauDirectDataDiffuse = new RoomTauDirectDataDiffuse();
            roomTauDirectDataDiffuse.InitDataArray(partitions, interpolator, filterLength);
            roomTauDirectDataDiffuse.CalcTau();

            irOutComplexFreq = new FloatArrays[RoomFiData.numWalls * RoomFiData.numSources];
            for (var i = 0; i < RoomFiData.numWalls * RoomFiData.numSources; i++)
                irOutComplexFreq[i] = new FloatArrays(filterLength * 2);
        }

        public override void UpdatePositionAndRotation(bool applyReverberation, SoundSourceObject[] soundSourceObjects)
        {
            RoomFiData.UpdateFi(soundSourceObjects);

            for (short iSource = 0; iSource < soundSourceObjects.Length; iSource++)
            {
                for (short iWall = 0; iWall < RoomFiData.numWalls; iWall++)
                {
                    var index = GetIndex(iWall, iSource);
                    if (applyReverberation)
                        ComplexFloats.MultiplyAndSumAndMultiply(RoomFiData.fiComplexFreq[index].floats, 1,
                            reverberationData.reverberationFactorComplexFreq,
                            roomTauDirectDataDiffuse.sqrtTauDiffComplexFreq[iWall].floats,
                            irOutComplexFreq[index].floats);
                    else
                        ComplexFloats.Multiply(RoomFiData.fiComplexFreq[index].floats,
                            roomTauDirectDataDiffuse.sqrtTauDiffComplexFreq[iWall].floats,
                            irOutComplexFreq[index].floats);
                }
            }
        }

        public override void CalcTau()
        {
            roomTauDirectDataDiffuse.CalcTau();
        }

        public override void Save(string path, int i=0)
        {
            System.IO.File.WriteAllText($"{path}/SourceRoomDirect{i}.json", JsonUtility.ToJson(this));
        }
    }
}