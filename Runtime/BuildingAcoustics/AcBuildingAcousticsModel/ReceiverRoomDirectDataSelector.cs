﻿using System;

namespace BA.BACore
{
    [Serializable]
    public class ReceiverRoomDirectDataSelector:Selector<ReceiverRoomDirectData,ReceiverRoomDirectDataType>{}
}