﻿namespace BA.BACore
{
    public enum SourceRoomFlankingDataType
    {
        None,
        Indoor,
        IndoorSegments,
    }
}