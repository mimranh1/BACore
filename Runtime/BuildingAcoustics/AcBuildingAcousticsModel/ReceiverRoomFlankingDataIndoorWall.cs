﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class ReceiverRoomFlankingDataIndoorWall : ReceiverRoomFlankingDataIndoor
    {
        public override ReceiverRoomFlankingDataType Type => ReceiverRoomFlankingDataType.Indoor;
        public override RoomHrtfData RoomHrtfData => roomHrtfData;
        [SerializeField] [HideInInspector] private RoomHrtfDataWall roomHrtfData;

        public override void InitDataArray(RoomAcousticsBehaviour roomBehaviour, Interpolator interpolator, int filterLength,
            int numSoundObjects = 1)
        {
            roomHrtfData = new RoomHrtfDataWall();
            base.InitDataArray(roomBehaviour, interpolator, filterLength, numSoundObjects);
        }
    }
}