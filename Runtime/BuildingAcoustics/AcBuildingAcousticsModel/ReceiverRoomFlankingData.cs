﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public abstract class ReceiverRoomFlankingData : ExtendedScriptableObject<ReceiverRoomFlankingData,ReceiverRoomFlankingDataType>
    {
        public abstract void InitDataArray(RoomAcousticsBehaviour roomBehaviour, Interpolator interpolator, int filterLength, int numSoundObjects=1);
        public abstract void Save(string path, int i = 0);
        public abstract void UpdatePositionAndRotation(bool applyReverberation, SoundReceiverObject soundReceiverObject);
        
        public abstract FloatArrays[] IrOutComplexFreq { get; }

        public abstract void CalcTau();
    }
}