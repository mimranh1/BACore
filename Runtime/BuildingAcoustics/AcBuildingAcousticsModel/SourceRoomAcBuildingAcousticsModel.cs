﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class SourceRoomAcBuildingAcousticsModel : SourceRoom
    {
        [SerializeField] private bool applyReverberation = true;
        
        [SerializeField] private InterpolatorSelector interpolator;

        public TransferPath TransferPath => transferPath.@object;
        [SerializeField] protected TransferPathSelector transferPath;
        
        public bool DirectActive => dataDirect.@object.NeedProcess;
        public FloatArrays[] IrDirectOutComplexFreq => dataDirect.@object.IrOutComplexFreq;
        [SerializeField] private SourceRoomDirectDataSelector dataDirect;

        public bool FlankingActive => dataFlanking.@object.NeedProcess;
        public FloatArrays[] IrFlankingOutComplexFreq => dataFlanking.@object.IrOutComplexFreq;
        [SerializeField] private SourceRoomFlankingDataSelector dataFlanking;
        public TauFlankingFactor[] flankingFactors => dataFlanking.@object.FlankingFactors;


        public override void OnValidate()
        {
            base.OnValidate();
            if (interpolator == null) interpolator = ScriptableObject.CreateInstance<InterpolatorSelector>();
            if (transferPath == null) transferPath = ScriptableObject.CreateInstance<TransferPathSelector>();
            if (dataDirect == null) dataDirect = ScriptableObject.CreateInstance<SourceRoomDirectDataSelector>();
            if (dataFlanking == null) dataFlanking = ScriptableObject.CreateInstance<SourceRoomFlankingDataSelector>();
        }

        public override void Init(int filterResolution, ReceiverRoomAcClassical receiverRoom = null)
        {
            throw new NotImplementedException();
        }

        public override void Init(int filterResolution, RoomAcousticsBehaviour receiverRoom = null)
        {
            filterLength = filterResolution;
            foreach (var soundSource in soundSourceObjects)
            {
                soundSource.Init(filterResolution);
            }

            room.ReverberationCalculator.ProcessAll(room);
            
            dataDirect.@object.InitDataArray(TransferPath.Partitons, room, interpolator.@object, filterLength, soundSourceObjects.Length);

            dataFlanking.@object.InitDataArray(TransferPath, room, receiverRoom,interpolator.@object, filterLength, soundSourceObjects.Length);
        }
        

        public override bool Update(bool forceUpdate = false)
        {
            if (forceUpdate)
            {
                room.ReverberationCalculator.OnSourceUpdate();
                room.ReverberationCalculator.OnUpdateFilter(null);
                dataDirect.@object.UpdatePositionAndRotation(applyReverberation,soundSourceObjects);
                dataFlanking.@object.UpdatePositionAndRotation(applyReverberation,soundSourceObjects);

                return true;
            }
            
            var soundObjectChanged = false;
            foreach (var source in soundSourceObjects)
            {
                if (source.SoundObjectMove())
                    soundObjectChanged= true;
            }
            if (soundObjectChanged)
            {
                room.ReverberationCalculator.OnSourceUpdate();
                room.ReverberationCalculator.OnUpdateFilter(null);
                dataDirect.@object.UpdatePositionAndRotation(applyReverberation,soundSourceObjects);
                dataFlanking.@object.UpdatePositionAndRotation(applyReverberation, soundSourceObjects);
            }
            return soundObjectChanged;
        }

        public void UpdateTau()
        {
            dataDirect.@object.CalcTau();
            dataFlanking.@object.CalcTau();
        }

        public override string Save(string path, int i)
        {
            var dataDirectJson = JsonUtility.ToJson(dataDirect.@object, true);
            var dataFlankingJson = JsonUtility.ToJson(dataFlanking.@object, true);
            var json = "{\"dataDirect\": " + dataDirectJson + ",\"dataFlanking\": " + dataFlankingJson + "}";
            if (path != "")
                File.WriteAllText($"{path}/AcNewAppSource{i}.json", json);
            return json;
        }

        public int GetSourceIndex(int iWall, int iSource)
        {
            return dataFlanking.@object.GetIndex((short) iWall, (short) iSource);
        }

  
    }
}