﻿using System;

namespace BA.BACore
{
    [Serializable]
    public class ReceiverRoomFlankingDataNone:ReceiverRoomFlankingData
    {
        public override ReceiverRoomFlankingDataType Type => ReceiverRoomFlankingDataType.None;

        public override FloatArrays[] IrOutComplexFreq => null;

        public override void InitDataArray(RoomAcousticsBehaviour roomBehaviour, Interpolator interpolator, int filterLength,
            int numSoundObjects = 1)
        {
        }

        public override void Save(string path, int i = 0)
        {
        }

        public override void UpdatePositionAndRotation(bool applyReverberation, SoundReceiverObject soundReceiverObject)
        {
        }

        public override void CalcTau()
        {
        }
    }
}