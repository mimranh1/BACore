﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class ReceiverRoomFlankingDataIndoorSegments : ReceiverRoomFlankingDataIndoor
    {
        public override ReceiverRoomFlankingDataType Type => ReceiverRoomFlankingDataType.IndoorSegments;
        public override RoomHrtfData RoomHrtfData => roomHrtfData;
        [SerializeField] [HideInInspector] private RoomHrtfDataSegments roomHrtfData;

        public override void InitDataArray(RoomAcousticsBehaviour roomBehaviour, Interpolator interpolator, int filterLength,
            int numSoundObjects = 1)
        {
            roomHrtfData = new RoomHrtfDataSegments();
            base.InitDataArray(roomBehaviour, interpolator, filterLength, numSoundObjects);
        }
    }
}