﻿using UnityEngine;

namespace BA.BACore
{
    public abstract class SourceRoomFlankingData : ExtendedScriptableObject<SourceRoomFlankingData,SourceRoomFlankingDataType>
    {
        public abstract bool NeedProcess { get; }
        public abstract int GetIndex(short iWall, short iSource);

        public abstract void InitDataArray(TransferPath flankingPath,RoomAcousticsBehaviour sourceRoom,RoomAcousticsBehaviour receiverRoom, Interpolator interpolator,
            int filterLength, int numSoundObjects = 1);

        public abstract void UpdatePositionAndRotation(bool applyReverberation, SoundSourceObject[] soundSourceObjects);
        public abstract void Save(string path, int i=0);

        public abstract FloatArrays[] IrOutComplexFreq { get; }
        public abstract TauFlankingFactor[] FlankingFactors { get; }

        public abstract void CalcTau();
    }
}