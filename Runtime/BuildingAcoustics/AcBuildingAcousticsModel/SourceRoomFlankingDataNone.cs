﻿namespace BA.BACore
{
    public class SourceRoomFlankingDataNone :SourceRoomFlankingData
    {
        public override SourceRoomFlankingDataType Type => SourceRoomFlankingDataType.None;
        public override bool NeedProcess => false;

        public override TauFlankingFactor[] FlankingFactors => null;

        public override int GetIndex(short iWall, short iSource)
        {
            return -1;
        }

        public override void InitDataArray(TransferPath flankingPath, RoomAcousticsBehaviour sourceRoom,RoomAcousticsBehaviour receiverRoom, Interpolator interpolator,
            int filterLength, int numSoundObjects = 1)
        {
        }

        public override void UpdatePositionAndRotation(bool applyReverberation, SoundSourceObject[] soundSourceObjects)
        {
        }

        public override void Save(string path, int i = 0)
        {
        }

        public override FloatArrays[] IrOutComplexFreq => null;

        public override void CalcTau()
        {
        }
    }
}