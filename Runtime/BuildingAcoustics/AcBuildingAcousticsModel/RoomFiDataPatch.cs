﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class RoomFiDataPatch : RoomFiData
    {
        [SerializeField][HideInInspector] private GeometrySegments[][] secondarySources;
        [SerializeField][HideInInspector] private WallBehaviour[][] patches;
        [SerializeField][HideInInspector] private Vector3S[] positions;
        [SerializeField][HideInInspector] private Vector3S[] normals;
        [SerializeField][HideInInspector] private Vector3 sourcePos;
        [SerializeField][HideInInspector] public int[] numPatches;
        [SerializeField][HideInInspector] public float[] incAngleRad;
        [SerializeField][HideInInspector] private Interpolator _interpolator;
        [SerializeField] [HideInInspector] private AngleTauData[] angleTauData;
        public float[] sourceDistance;
        public bool calcFilter = true; 
        public override void InitDataArray(RoomGeometry roomGeometry, int filterLength, int numSoundObjects,
            WallBehaviour[] wallBehaviours, Interpolator interpolator)
        {
            base.InitDataArray(roomGeometry, filterLength, numSoundObjects, wallBehaviours, interpolator);
            _interpolator = interpolator;
            var totNumPatches = wallBehaviours.Sum(partition => partition.patchesHandler.NumOfPatches);

            secondarySources = new GeometrySegments[numWalls][];
            positions = new Vector3S[totNumPatches];
            normals = new Vector3S[totNumPatches];
            sourceDistance = new float[totNumPatches];
            numPatches = new int[numWalls];
            patches = new WallBehaviour[numWalls][];
            angleTauData=new AngleTauData[totNumPatches];
            var index = 0;
            keys.Clear();
            for (var iPartiton = 0; iPartiton < numWalls; iPartiton++)
            {
                patches[iPartiton] = wallBehaviours[iPartiton].patchesHandler.Patches.ToArray();
                var numberPatches = patches[iPartiton].Length;
                numPatches[iPartiton] = numberPatches;
                secondarySources[iPartiton] = new GeometrySegments[numberPatches];
                for (var iPatch = 0; iPatch < numberPatches; iPatch++)
                {
                    angleTauData[index] = new AngleTauData(90, patches[iPartiton][iPatch].ReductionIndex, _interpolator,
                        filterLength);
                    var sourceSegments = patches[iPartiton][iPatch].WallSourceSegments;
                    var geometrySses = sourceSegments.SecondarySources;
                    secondarySources[iPartiton][iPatch] = new GeometrySegments(geometrySses.ToArray());
                    positions[index] = new Vector3S(sourceSegments.Positions);
                    normals[index] = new Vector3S(sourceSegments.Normals);

                    var indexKey = ((long) iPartiton << 32) |  iPatch; 
                    keys.Add(indexKey, index);
                    index++;
                }

                Debug.Log(
                    $"init Source wall {wallBehaviours[iPartiton].name} with {numPatches[iPartiton]} Secondary Sources.");
            }
            incAngleRad= new float[totNumPatches];
            fiComplexFreq = new FloatArrays[keys.Count];
            for (var i = 0; i < fiComplexFreq.Length; i++)
            {
                fiComplexFreq[i] = new FloatArrays(2 * filterLength);
            }
        }

        public override void UpdateFi(SoundSourceObject[] soundSourceObjects)
        {
            for (short iSource = 0; iSource < numSources; iSource++)
            {
                for (short iWall = 0; iWall < numWalls; iWall++)
                {
                    for (short iPatch = 0; iPatch < numPatches[iWall]; iPatch++)
                    {
                        var index = GetIndex(iWall, iSource, iPatch);
                        if (calcFilter)
                        {
                            
                            var cosincAngle=soundSourceObjects[iSource].GetFiBySqrtSi4Pi(fiComplexFreq[index].floats,
                                secondarySources[iWall][iPatch], patches[iWall][iPatch], _interpolator, 1f,
                                angleTauData[index], true, true);
                            incAngleRad[index] = Mathf.Acos(cosincAngle[0]);
                        }
                        sourcePos = soundSourceObjects[iSource].transformObj.position;
                        sourceDistance[index] = Vector3.Distance(sourcePos, patches[iWall][iPatch].Geometry.Position);
                    }
                }
            }
        }
        
    }
}