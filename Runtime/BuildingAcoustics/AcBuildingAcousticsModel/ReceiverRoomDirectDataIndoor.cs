﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public abstract class ReceiverRoomDirectDataIndoor : ReceiverRoomDirectData
    {
        public override FloatArrays[] IrOutComplexFreq => irOutComplexFreq;
        [SerializeField][HideInInspector] protected FloatArrays[] irOutComplexFreq;
        
        public abstract RoomHrtfData RoomHrtfData { get; }
        public RoomStatsData roomStatsData;
        public ReverberationData reverberationData;
        
        public override void InitDataArray(RoomAcousticsBehaviour roomBehaviour, WallBehaviour[] partitions,Interpolator interpolator, int filterLength,
            int numSoundObjects = 1)
        {
            roomStatsData = new RoomStatsData();
            roomStatsData.InitDataArray(roomBehaviour.RoomAcousticProperties);
            
            reverberationData = new ReverberationData();
            reverberationData.InitDataArray(roomBehaviour.Reverberation, filterLength, Mathf.Sqrt(4 / roomStatsData.equivalentAbsorptionArea),roomBehaviour.RoomAcousticProperties.DmfpSamples);

            RoomHrtfData.InitDataArray(partitions, filterLength);
            
            irOutComplexFreq = new FloatArrays[RoomHrtfData.hrtfComplexFreq.Length];
            for (var i = 0; i < irOutComplexFreq.Length; i++)
            {
                irOutComplexFreq[i] = new FloatArrays(filterLength * 2);
            }
        }
        
        public override void UpdatePositionAndRotation(bool applyReverberation, SoundReceiverObject soundReceiverObject)
        {
            RoomHrtfData.UpdateHrtf(soundReceiverObject);
            if (applyReverberation)
                for (var i = 0; i < RoomHrtfData.hrtfComplexFreq.Length/2; i++)
                {
                    ComplexFloats.Add(RoomHrtfData.hrtfComplexFreq[2 * i].floats,
                         reverberationData.reverberationFactorComplexFreq,
                        irOutComplexFreq[2 * i].floats);
                    ComplexFloats.Add(RoomHrtfData.hrtfComplexFreq[2 * i + 1].floats,
                         reverberationData.reverberationFactorComplexFreq,
                        irOutComplexFreq[2 * i + 1].floats);
                }
            else
                for (var i = 0; i < RoomHrtfData.hrtfComplexFreq.Length/2; i++)
                {
                    ComplexFloats.Copy(RoomHrtfData.hrtfComplexFreq[2 * i].floats,
                         irOutComplexFreq[2 * i].floats);
                    ComplexFloats.Copy(RoomHrtfData.hrtfComplexFreq[2 * i + 1].floats,
                         irOutComplexFreq[2 * i + 1].floats);
                }
        }
    }
}