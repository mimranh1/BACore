﻿using System;
using System.IO;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class ReceiverRoomAcBuildingAcousticsModel : ReceiverRoom
    {
        [SerializeField] private bool applyReverberation = true;
        
        [SerializeField] private InterpolatorSelector interpolator;
        
        public FloatArrays[] IrDirectOutComplexFreq => dataDirect.@object.IrOutComplexFreq;
        [SerializeField] private ReceiverRoomDirectDataSelector dataDirect;
        
        public FloatArrays[] IrFlankingOutComplexFreq => dataFlanking.@object.IrOutComplexFreq;
        [SerializeField] private ReceiverRoomFlankingDataSelector dataFlanking;
        
        public override void OnValidate()
        {
            base.OnValidate();
            if (interpolator == null) interpolator = ScriptableObject.CreateInstance<InterpolatorSelector>();
            if (dataFlanking == null) dataFlanking = ScriptableObject.CreateInstance<ReceiverRoomFlankingDataSelector>();
            if (dataDirect == null) dataDirect = ScriptableObject.CreateInstance<ReceiverRoomDirectDataSelector>();
        }

        public override void Init(WallBehaviour[] partitions,int filterResolution)
        {
            FilterLength = filterResolution;
            base.Init(partitions, FilterLength);

            dataDirect.@object.InitDataArray(room, partitions, interpolator.@object, FilterLength);
            dataFlanking.@object.InitDataArray(room, interpolator.@object, FilterLength);
        }

        public override bool Update(bool forceUpdate = false)
        {
            var soundObjectChanged = forceUpdate || soundReceiverObject.SoundObjectChanged();
            if (!soundObjectChanged) return false;
            dataDirect.@object.UpdatePositionAndRotation(applyReverberation, soundReceiverObject);
            dataFlanking.@object.UpdatePositionAndRotation(applyReverberation, soundReceiverObject);
            return true;
        }

        public override string Save(string path)
        {
            var dataDirectJson = JsonUtility.ToJson(dataDirect.@object, true);
            var dataFlankingJson = JsonUtility.ToJson(dataFlanking.@object, true);
            var json = "{\"dataDirect\": " + dataDirectJson + ",\"dataFlanking\": " + dataFlankingJson + "}";
            if (path != "")
                File.WriteAllText($"{path}/AcNewAppReceiver.json", json);
            return json;
        }

        public void UpdateTau()
        {
            dataFlanking.@object.CalcTau();
        }
    }
}