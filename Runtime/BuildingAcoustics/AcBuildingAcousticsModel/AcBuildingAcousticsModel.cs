﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Serialization;


namespace BA.BACore
{
    [Serializable]
    public class AcBuildingAcousticsModel : AuralisationCalculation
    {
        public override AuralisationCalculationType Type => AuralisationCalculationType.BuildingAcousticsModel;
        

        public override SoundSourceObject[] SoundSourceObjects
        {
            get
            {
                var list = new List<SoundSourceObject>();
                foreach (var sourceRoom in sourceRooms)
                {
                    list.AddRange(sourceRoom.SoundSourceObjects);
                }
                return list.ToArray();
            }
        }

        [Header("New Approach Settings")] 
        public bool activatePartition = true;
        public bool activateFlanking = true;

        [Header("Room Settings")]
        
        [SerializeField] protected SourceRoomAcBuildingAcousticsModel[] sourceRooms;

        public override SoundReceiverObject SoundReceiverObject => receiverRoom.SoundReceiverObject;
        [SerializeField] protected ReceiverRoomAcBuildingAcousticsModel receiverRoom;

        [SerializeField][HideInInspector] private float sqrtRho0c;
        public WallBehaviour[] Partitions
        {
            get
            {
                var list = new List<WallBehaviour>();
                foreach (var sourceRoom in sourceRooms)
                {
                    list.AddRange(sourceRoom.TransferPath.Partitons);
                }
                return list.ToArray();
            }
        }


        private int[] _sourceWallIds;
        private int[] _receiverWallIds;
        private float[] _factors;
        private int[] _sourceWallSourcesIds;

        public override void OnValidate()
        {
            foreach (var sourceRoom in sourceRooms)
            {
                sourceRoom.OnValidate();
            }
            receiverRoom.OnValidate();
            ForceReceiverUpdate = true;
            ForceSourceUpdate = true;
        }

        protected override void Init()
        {
            receiverRoom.Init(Partitions, FilterResolution);
            foreach (var sourceRoom in sourceRooms)
            {
                sourceRoom.Init(FilterResolution, receiverRoom.Room);
            }
            sqrtRho0c = Mathf.Sqrt(BaSettings.Instance.Rho0 * BaSettings.Instance.C0);
            OnValidate();
        }

        protected override void OfflineCalculation()
        {
            var flankingFactorsLength = 0;
            foreach (var sourceRoom in sourceRooms)
            {
                var soundSource = sourceRoom.SoundSourceObjects;
                var sourceIds = new int[soundSource.Length];
                for (var i = 0; i < soundSource.Length; i++)
                {
                    sourceIds[i] = soundSource[i].sourceId;
                }

                if(activateFlanking)
                    flankingFactorsLength += sourceRoom.flankingFactors.Length;
            }

            var sources = SoundSourceObjects;
            foreach (var source in sources)
            {
                source.brir.InitArrays(FilterResolution);
            }

            if(activateFlanking)
            {
                _sourceWallIds = new int[flankingFactorsLength];
                _receiverWallIds = new int[flankingFactorsLength];
                _factors = new float[flankingFactorsLength];
                _sourceWallSourcesIds = new int[flankingFactorsLength];
                var index = 0;
                foreach (var sourceRoom in sourceRooms)
                {
                    for (var iSource = 0; iSource < sourceRoom.SoundSourceObjects.Length; iSource++)
                    {
                        for (var i = 0; i < flankingFactorsLength; i++)
                        {
                            var flankingFactor = sourceRoom.flankingFactors[index];
                            _sourceWallIds[index] = flankingFactor.sourceWallRoomId;
                            _receiverWallIds[index] = flankingFactor.receiverWallRoomId;
                            _factors[index] = flankingFactor.factor;
                            _sourceWallSourcesIds[index] = sourceRoom.GetSourceIndex(_sourceWallIds[index], iSource);
                            index++;
                        }
                    }
                }
            }
        }

        protected override void RealTimeCalculation(AudioRender audioRender,bool log = false)
        {
            // Debug.Log("Update");
            var amplitude = Mathf.Pow(10, defaultGainDb * 0.1f);
            var constFactor = sqrtRho0c * amplitude;
            if (sourceRooms.Length == 0)
                Debug.LogError("SourceRooms not set anymore.");
            var isaudioRenderNotNull = audioRender!=null;
            foreach (var sourceRoom in sourceRooms)
            {
                if (!sourceRoom.isActive) continue;
                for (var iSource = 0; iSource < sourceRoom.SoundSourceObjects.Length; iSource++)
                {
                    var soundSource = sourceRoom.SoundSourceObjects[iSource];
                    var brir = soundSource.brir;

                    var irLeftFreqComplex = brir.IrLeft.freqComplex;
                    var irRightFreqComplex = brir.IrRight.freqComplex;
                    if (activateFlanking&&sourceRoom.FlankingActive)
                    {
                        // ComputeFlanking2(sourceRoom, iSource, constFactor, irLeftFreqComplex, irRightFreqComplex);
                        ComputeFlanking3(sourceRoom, iSource, constFactor, irLeftFreqComplex, irRightFreqComplex);
                    }

                    if (activatePartition && sourceRoom.DirectActive)
                    {
                        var xMono = sourceRoom.IrDirectOutComplexFreq;
                        var yBinaural=receiverRoom.IrDirectOutComplexFreq;
                        ComputeDirect(sourceRoom, xMono, yBinaural, irLeftFreqComplex, constFactor, irRightFreqComplex);
                        
                    }

                    brir.RunIfft();
                    if(isaudioRenderNotNull)
                        foreach (var sourceId in brir.sourceIds)
                        {
                            audioRender.UpdateFilter(brir.IrLeft.time, brir.IrRight.time, sourceId);
                        }

                    if (log)
                        brir.UpdatePlot();
                }
            }
        }

        private void ComputeDirect(SourceRoomAcBuildingAcousticsModel sourceRoom, FloatArrays[] xMono, FloatArrays[] yBinaural,
            float[] irLeftFreqComplex, float constFactor, float[] irRightFreqComplex)
        {
            var xMonoLength = xMono.Length;
            var length = xMono[0].Length;
            if (activateFlanking && sourceRoom.FlankingActive)
            {
                for (var n = 0; n < length; n += 2)
                {
                    var xReal = 0f;
                    var xImag = 0f;
                    var yLeftReal = 0f;
                    var yLeftImag = 0f;
                    var yRightReal = 0f;
                    var yRightImag = 0f;
                    for (var i = 0; i < xMonoLength; i++)
                    {
                        xReal += xMono[i].floats[n];
                        xImag += xMono[i].floats[n + 1];
                        yLeftReal += yBinaural[2 * i].floats[n];
                        yLeftImag += yBinaural[2 * i].floats[n + 1];
                        yRightReal += yBinaural[2 * i+1].floats[n];
                        yRightImag += yBinaural[2 * i+1].floats[n + 1];
                    }

                    irLeftFreqComplex[n] += (xReal * yLeftReal - xImag * yLeftImag) * constFactor;
                    irLeftFreqComplex[n + 1] += (xReal * yLeftImag + xImag * yLeftReal) * constFactor;
                    irRightFreqComplex[n] += (xReal * yRightReal - xImag * yRightImag) * constFactor;
                    irRightFreqComplex[n + 1] += (xReal * yRightImag + xImag * yRightReal) * constFactor;
                }
            }
            else
                for (var n = 0; n < length; n += 2)
                {
                    var xReal = 0f;
                    var xImag = 0f;
                    var yLeftReal = 0f;
                    var yLeftImag = 0f;
                    var yRightReal = 0f;
                    var yRightImag = 0f;
                    for (var i = 0; i < xMonoLength; i++)
                    {
                        xReal += xMono[i].floats[n];
                        xImag += xMono[i].floats[n + 1];
                        yLeftReal += yBinaural[2 * i].floats[n];
                        yLeftImag += yBinaural[2 * i].floats[n + 1];
                        yRightReal += yBinaural[2 * i+1].floats[n];
                        yRightImag += yBinaural[2 * i+1].floats[n + 1];
                    }

                    irLeftFreqComplex[n] = (xReal * yLeftReal - xImag * yLeftImag) * constFactor;
                    irLeftFreqComplex[n + 1] = (xReal * yLeftImag + xImag * yLeftReal) * constFactor;
                    irRightFreqComplex[n] = (xReal * yRightReal - xImag * yRightImag) * constFactor;
                    irRightFreqComplex[n + 1] = (xReal * yRightImag + xImag * yRightReal) * constFactor;
                }
        }

        private void ComputeFlanking(SourceRoomAcBuildingAcousticsModel sourceRoom, int iSource, float constFactor,
            float[] irLeftFreqComplex, float[] irRightFreqComplex)
        {
            foreach (var flankingFactor in sourceRoom.flankingFactors)
            {
                var sourceWallId = flankingFactor.sourceWallRoomId;
                var receiverWallId = flankingFactor.receiverWallRoomId;
                var sourceWallSourcesId = sourceRoom.GetSourceIndex(sourceWallId, iSource);

                var factor = flankingFactor.factor * constFactor;
                var source = sourceRoom.IrFlankingOutComplexFreq[sourceWallSourcesId].floats;
                var receiverLeft = receiverRoom.IrFlankingOutComplexFreq[2 * receiverWallId].floats;
                ComplexFloats.MultiplyAdd(source, receiverLeft, factor, irLeftFreqComplex);
                var receiverRight = receiverRoom.IrFlankingOutComplexFreq[2 * receiverWallId + 1].floats;
                ComplexFloats.MultiplyAdd(source, receiverRight, factor, irRightFreqComplex);
            }
        }
        
        private void ComputeFlanking1(SourceRoomAcBuildingAcousticsModel sourceRoom, int iSource, float constFactor,
            float[] irLeftFreqComplex, float[] irRightFreqComplex)
        {
            var flankingFactorsLength = sourceRoom.flankingFactors.Length;
            for (var index = 0; index < flankingFactorsLength; index++)
            {
                var flankingFactor = sourceRoom.flankingFactors[index];
                _sourceWallIds[index] = flankingFactor.sourceWallRoomId;
                _receiverWallIds[index] = flankingFactor.receiverWallRoomId;
                _factors[index] = flankingFactor.factor * constFactor;
                _sourceWallSourcesIds[index] = sourceRoom.GetSourceIndex(_sourceWallIds[index], iSource);
            }
            var sourceRoomIrFlankingOutComplexFreq = sourceRoom.IrFlankingOutComplexFreq;
            var receiverRoomIrFlankingOutComplexFreq = receiverRoom.IrFlankingOutComplexFreq;

            Parallel.For(0, sourceRoomIrFlankingOutComplexFreq[_sourceWallSourcesIds[0]].floats.Length / 2,
                new ParallelOptions() {MaxDegreeOfParallelism = Environment.ProcessorCount}, j =>
                {
                    var n = 2 * j;
                    var sourceReal = 0f;
                    var sourceImag = 0f;
                    var receiverLeftReal = 0f;
                    var receiverLeftImag = 0f;
                    var receiverRightReal = 0f;
                    var receiverRightImag = 0f;
                    for (var index = 0; index < flankingFactorsLength; index++)
                    {
                        var factor = _factors[index];
                        var sourceRoomFloats = sourceRoomIrFlankingOutComplexFreq[_sourceWallSourcesIds[index]].floats;
                        var receiverRoomLeftFloats = receiverRoomIrFlankingOutComplexFreq[2 * _receiverWallIds[index]].floats;
                        var receiverRoomRightFloats = receiverRoomIrFlankingOutComplexFreq[2 * _receiverWallIds[index] + 1].floats;
                        sourceReal += sourceRoomFloats[n] * factor;
                        sourceImag += sourceRoomFloats[n + 1] * factor;
                        receiverLeftReal += receiverRoomLeftFloats[n] * factor;
                        receiverLeftImag += receiverRoomLeftFloats[n + 1] * factor;
                        receiverRightReal += receiverRoomRightFloats[n] * factor;
                        receiverRightImag += receiverRoomRightFloats[n + 1] * factor;
                    }

                    irLeftFreqComplex[n] = (sourceReal * receiverLeftReal - sourceImag * receiverLeftImag);
                    irLeftFreqComplex[n + 1] = (sourceReal * receiverLeftImag + sourceImag * receiverLeftReal);
                    irRightFreqComplex[n] = (sourceReal * receiverRightReal - sourceImag * receiverRightImag);
                    irRightFreqComplex[n + 1] = (sourceReal * receiverRightImag + sourceImag * receiverRightReal);
                });

        }

        private void ComputeFlanking3(SourceRoomAcBuildingAcousticsModel sourceRoom, int iSource, float constFactor,
            float[] irLeftFreqComplex, float[] irRightFreqComplex)
        {
            var flankingFactorsLength = sourceRoom.flankingFactors.Length;
            for (var index = 0; index < flankingFactorsLength; index++)
            {
                var flankingFactor = sourceRoom.flankingFactors[index];
                _sourceWallIds[index] = flankingFactor.sourceWallRoomId;
                _receiverWallIds[index] = flankingFactor.receiverWallRoomId;
                _factors[index] = flankingFactor.factor * constFactor;
                _sourceWallSourcesIds[index] = sourceRoom.GetSourceIndex(_sourceWallIds[index], iSource);
            }

            var sourceRoomIrFlankingOutComplexFreq = sourceRoom.IrFlankingOutComplexFreq;
            var receiverRoomIrFlankingOutComplexFreq = receiverRoom.IrFlankingOutComplexFreq;

            for (var n = 0; n < sourceRoomIrFlankingOutComplexFreq[_sourceWallSourcesIds[0]].floats.Length; n += 2)
            {
                var sourceReal = 0f;
                var sourceImag = 0f;
                var receiverLeftReal = 0f;
                var receiverLeftImag = 0f;
                var receiverRightReal = 0f;
                var receiverRightImag = 0f;
                for (var index = 0; index < flankingFactorsLength; index++)
                {
                    var factor = _factors[index];
                    var sourceRoomFloats = sourceRoomIrFlankingOutComplexFreq[_sourceWallSourcesIds[index]].floats;
                    var receiverRoomLeftFloats = receiverRoomIrFlankingOutComplexFreq[2 * _receiverWallIds[index]].floats;
                    var receiverRoomRightFloats = receiverRoomIrFlankingOutComplexFreq[2 * _receiverWallIds[index] + 1].floats;
                    sourceReal += sourceRoomFloats[n] * factor;
                    sourceImag += sourceRoomFloats[n + 1] * factor;
                    receiverLeftReal += receiverRoomLeftFloats[n] * factor;
                    receiverLeftImag += receiverRoomLeftFloats[n + 1] * factor;
                    receiverRightReal += receiverRoomRightFloats[n] * factor;
                    receiverRightImag += receiverRoomRightFloats[n + 1] * factor;
                }

                irLeftFreqComplex[n] = (sourceReal * receiverLeftReal - sourceImag * receiverLeftImag);
                irLeftFreqComplex[n + 1] = (sourceReal * receiverLeftImag + sourceImag * receiverLeftReal);
                irRightFreqComplex[n] = (sourceReal * receiverRightReal - sourceImag * receiverRightImag);
                irRightFreqComplex[n + 1] = (sourceReal * receiverRightImag + sourceImag * receiverRightReal);
            }

        }


        private void ComputeFlanking2(SourceRoomAcBuildingAcousticsModel sourceRoom, int iSource, float constFactor,
            float[] irLeftFreqComplex, float[] irRightFreqComplex)
        {
            var flankingFactorsLength = sourceRoom.flankingFactors.Length;

            var sourceRoomIrFlankingOutComplexFreq = sourceRoom.IrFlankingOutComplexFreq;
            var receiverRoomIrFlankingOutComplexFreq = receiverRoom.IrFlankingOutComplexFreq;
            for (var index = 0; index < flankingFactorsLength; index++)
            {
                var flankingFactor = sourceRoom.flankingFactors[index];
                var sourceWallId = flankingFactor.sourceWallRoomId;
                var receiverWallId = flankingFactor.receiverWallRoomId;
                var factor = flankingFactor.factor * constFactor;
                var sourceWallSourcesId = sourceRoom.GetSourceIndex(sourceWallId, iSource);

                var sourceArray = sourceRoomIrFlankingOutComplexFreq[sourceWallSourcesId].floats;
                var receiverLeftArray = receiverRoomIrFlankingOutComplexFreq[2 * receiverWallId].floats;
                var receiverRightArray = receiverRoomIrFlankingOutComplexFreq[2 * receiverWallId + 1].floats;
                for (var n = 0; n < sourceRoomIrFlankingOutComplexFreq[sourceWallSourcesId].floats.Length; n += 2)
                {
                    var sourceReal = sourceArray[n];
                    var sourceImag = sourceArray[n + 1];
                    var receiverLeftReal = receiverLeftArray[n];
                    var receiverLeftImag = receiverLeftArray[n + 1];
                    var receiverRightReal = receiverRightArray[n];
                    var receiverRightImag = receiverRightArray[n + 1];

                    irLeftFreqComplex[n] += (sourceReal * receiverLeftReal - sourceImag * receiverLeftImag) * factor;
                    irLeftFreqComplex[n + 1] += (sourceReal * receiverLeftImag + sourceImag * receiverLeftReal) *
                                                factor;
                    irRightFreqComplex[n] += (sourceReal * receiverRightReal - sourceImag * receiverRightImag) *
                                             factor;
                    irRightFreqComplex[n + 1] += (sourceReal * receiverRightImag + sourceImag * receiverRightReal) *
                                                 factor;
                }
            }
        }

        protected override bool RealTimeReceiverUpdate()
        {
            var output= receiverRoom.Update(ForceReceiverUpdate);
            ForceReceiverUpdate = false;
            return output;
        }

        protected override bool RealTimeSourceUpdate()
        {
            var hasChanged = ForceSourceUpdate;
            foreach (var sourceRoom in sourceRooms)
            {
                if (sourceRoom.Update(ForceSourceUpdate))
                    hasChanged = true;
            }

            ForceSourceUpdate = false;
            return hasChanged;
        }

        public override void SaveImpulseResponse(string name="")
        {
            var fullPath = $"{BaSettings.Instance.DataPath}/AcBuildingAcoustics";
            Directory.CreateDirectory(fullPath);
            var json = "{";
            json += "\"AcBuildingAcoustics\":" + JsonUtility.ToJson(this, true) + ",";
            for (var i = 0; i < sourceRooms.Length; i++)
            {
                var sourceJson = sourceRooms[i].Save("", i);
                json += "\"sourceRoom" + i + "\":" + sourceJson +",";
                var brir = JsonUtility.ToJson(sourceRooms[i].SoundSourceObjects[0].brir, true);
                json += "\"brir" + i + "\":" + brir +",";
            }

            var receiverJson = receiverRoom.Save("");
            json += "\"receiverRoom\":" + receiverJson + "}";
            File.WriteAllText($"{fullPath}/BuidlingsAcoustics{name}.json", json);
        }
    }
   
}