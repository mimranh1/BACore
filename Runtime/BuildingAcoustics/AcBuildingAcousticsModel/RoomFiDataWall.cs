﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class RoomFiDataWall : RoomFiData
    {
        
        [SerializeField][HideInInspector] private Vector3[] wallNormals;
        [SerializeField][HideInInspector] private Vector3[] wallPositions;
        [SerializeField][HideInInspector] private bool[] activeWalls;

        public override void InitDataArray(RoomGeometry roomGeometry, int filterLength, int numSoundObjects, WallBehaviour[] wallBehaviours, Interpolator interpolator)
        {
            base.InitDataArray(roomGeometry, filterLength, numSoundObjects, wallBehaviours,interpolator);
            activeWalls = roomGeometry.IsUsedForTau;
            wallPositions = new Vector3[numWalls];
            wallNormals = new Vector3[numWalls];
            
            for (var i = 0; i < numWalls; i++)
            {
                wallNormals[i] = wallBehaviours[i].Geometry.Normal;
                wallPositions[i] = wallBehaviours[i].Geometry.Position;
            }
        }

        public override void UpdateFi(SoundSourceObject[] soundSourceObjects)
        {
            for (short iSource = 0; iSource < soundSourceObjects.Length; iSource++)
            {
                for (short iWall = 0; iWall < wallNormals.Length; iWall++)
                {
                    var index = GetIndex(iWall, iSource);
                    soundSourceObjects[iSource].GetFiBySqrtSi4Pi(false, fiComplexFreq[index].floats,
                        wallPositions[iWall], wallNormals[iWall], true, true);
                }
            }
        }
    }
}