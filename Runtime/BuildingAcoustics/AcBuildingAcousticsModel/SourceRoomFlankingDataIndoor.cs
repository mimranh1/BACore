﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public abstract class SourceRoomFlankingDataIndoor : SourceRoomFlankingData
    {
        public override bool NeedProcess => true;

        [SerializeField][HideInInspector] protected FloatArrays[] irOutComplexFreq;
        [SerializeField][HideInInspector] protected int numSoundObjects;

        public abstract RoomFiData RoomFiData { get; }
        [SerializeField][HideInInspector]private RoomStatsData roomStatsData;
        [SerializeField][HideInInspector]private ReverberationData reverberationData;
        [SerializeField][HideInInspector]private RoomTauFlankingData roomTauFlankingData;

        public override FloatArrays[] IrOutComplexFreq => irOutComplexFreq;

        [SerializeField][HideInInspector]private TauFlankingFactor[] flankingFactors;
        public override TauFlankingFactor[] FlankingFactors => flankingFactors;

        public override int GetIndex(short iWall, short iSource)
        {
            return RoomFiData.GetIndex(iWall, iSource);
        }

        public override void InitDataArray(TransferPath flankingPath,RoomAcousticsBehaviour sourceRoom,RoomAcousticsBehaviour receiverRoom, Interpolator interpolator,
            int filterLength, int numSoundObjects = 1)
        {
            flankingFactors = flankingPath.CalcFlankingFactorsPaths(sourceRoom, receiverRoom);
            
            roomStatsData = new RoomStatsData();
            roomStatsData.InitDataArray(sourceRoom.RoomAcousticProperties);

            reverberationData = new ReverberationData();
            reverberationData.InitDataArray(sourceRoom.Reverberation, filterLength, Mathf.Sqrt(1 / roomStatsData.equivalentAbsorptionArea),sourceRoom.RoomAcousticProperties.DmfpSamples);

            roomTauFlankingData = new RoomTauFlankingData();
            roomTauFlankingData.InitDataArray(sourceRoom, interpolator, filterLength);
            roomTauFlankingData.CalcTauForFlanking();

            irOutComplexFreq = new FloatArrays[RoomFiData.numWalls * RoomFiData.numSources];
            for (var i = 0; i < RoomFiData.numWalls * RoomFiData.numSources; i++)
                irOutComplexFreq[i] = new FloatArrays(filterLength * 2);
        }


        public override void UpdatePositionAndRotation(bool applyReverberation, SoundSourceObject[] soundSourceObjects)
        {
            RoomFiData.UpdateFi(soundSourceObjects);

            var summandComplex = reverberationData.reverberationFactorComplexFreq;
            for (short iWall = 0; iWall < RoomFiData.numWalls; iWall++)
            {
                var multiplyAllComplex = roomTauFlankingData.sqrtSqrtTauAlphaComplexFreq[iWall].floats;
                for (short iSource = 0; iSource < soundSourceObjects.Length; iSource++)
                {
                    var index = GetIndex(iWall, iSource);
                    if (applyReverberation)
                    {
                        var xFactorComplex = RoomFiData.fiComplexFreq[index].floats;
                        var result = irOutComplexFreq[index].floats;
                        for (var n = 0; n < xFactorComplex.Length; n += 2)
                        {
                            var real = (xFactorComplex[n]) + summandComplex[n];
                            var imag = (xFactorComplex[n + 1]) + summandComplex[n + 1];
                            var tauReal = multiplyAllComplex[n];
                            var tauImag = multiplyAllComplex[n + 1];
                            
                            result[n] = (real * tauReal - imag * tauImag);
                            result[n + 1] = (real * tauImag + imag * tauReal);
                        }
                    }
                    else
                        ComplexFloats.Multiply(RoomFiData.fiComplexFreq[index].floats,
                            roomTauFlankingData.sqrtSqrtTauAlphaComplexFreq[iWall].floats,
                            irOutComplexFreq[index].floats);
                }
            }
        }

        public override void CalcTau()
        {
            roomTauFlankingData.CalcTauForFlanking();
        }


        public override void Save(string path, int i=0)
        {
            System.IO.File.WriteAllText($"{path}/SourceRoomFlanking{i}.json", JsonUtility.ToJson(this));
        }
    }
    
}