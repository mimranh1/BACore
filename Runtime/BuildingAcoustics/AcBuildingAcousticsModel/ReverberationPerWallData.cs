﻿using UnityEngine;

namespace BA.BACore
{
    public class ReverberationPerWallData : ScriptableObject
    {
        public float[] reverberationCutTime;
        public float[] reverberationComplexFreq;
        public FloatArrays[] reverberationFactorComplexFreq;

        public void InitDataArray(RoomAcousticsBehaviour roomBehaviour, int filterLength, float[] factors)
        {
            var receiverRoomReverberation = roomBehaviour.Reverberation;
            reverberationFactorComplexFreq = new FloatArrays[roomBehaviour.RoomGeometry.WallBehaviours.Length];

            reverberationCutTime = new float[filterLength];
            for (var n = 0; n < Mathf.Min(reverberationCutTime.Length, receiverRoomReverberation.Length); n++)
                reverberationCutTime[n] = receiverRoomReverberation[n];
            var fftPlan = new FftRealtimeR2C(filterLength);
            reverberationComplexFreq = new float[2 * filterLength];
            fftPlan.RunFftReal(reverberationCutTime, reverberationComplexFreq);
            for (var i = 0; i < reverberationFactorComplexFreq.Length; i++)
            {
                reverberationFactorComplexFreq[i] = new FloatArrays(filterLength*2);
            }
            UpdateReverberation(factors);
        }

        public void UpdateReverberation(float[] factor)
        {
            for (var i = 0; i < reverberationFactorComplexFreq.Length; i++)
            {
                reverberationFactorComplexFreq[i] = new FloatArrays(reverberationComplexFreq.Length);
                for (var n = 0; n < reverberationComplexFreq.Length; n++)
                {
                    reverberationFactorComplexFreq[i][n] = reverberationComplexFreq[n] * factor[i];
                }
            }
        }
    }
}