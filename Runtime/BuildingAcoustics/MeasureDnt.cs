﻿using System;
using UnityEngine;

namespace BA.BACore
{
    public class MeasureDnt : MonoBehaviour
    {
        [SerializeField] private Transform[] sourcePositions;
        [SerializeField] private Transform[] receiverPositions;
        [SerializeField] private Transform l2Ref2M;
        [SerializeField] private SoundSourceObject source;
        [SerializeField] private SoundReceiverObject receiver;
        [SerializeField] private DirectivityIR directivityOmni;
        [SerializeField] private string prefix="_DnT";
        [SerializeField] private bool startAuto=false;
        [SerializeField] private int frameNumber = 5;
        [SerializeField][Range(0,4)] private int CurrentNum = 0;
        private int _frameCounter = 0;
        

        private void Update()
        {
            if (_frameCounter == frameNumber && startAuto)
                MeasureDnTLite();
            _frameCounter++;
        }

        [ContextMenu("Measure DnT")]
        public void MeasureDnT()
        {
            var auralisation = GetComponent<AuralisationBehavior>();
            var audioRenderer = auralisation.audioRenderer;
            auralisation.audioRenderer = null;
            var sourceTransform = source.transform;
            var receiverTransform = receiver.transform;
            var oldReceiverHrir = receiver.hrir;
            var oldSourceDir = source.directivityIr;
            receiver.hrir = directivityOmni;
            source.directivityIr = directivityOmni;
            var oldSourcePos = sourceTransform.position;
            var oldReceiverPos = receiverTransform.position;
            var i = 0;
            var l2RefPosition = l2Ref2M.position;
            var mirror = new Vector3(-4,0,0);
            for (var iSource = 0; iSource < sourcePositions.Length; iSource++)
            {
                var d0 = Vector3.Distance(l2RefPosition, sourcePositions[iSource].position);
                var d1 = Vector3.Distance(l2RefPosition + mirror, sourcePositions[iSource].position);

                auralisation.AuralisationCalculation.refPosition = l2RefPosition;
                sourceTransform.position = sourcePositions[iSource].position;
                for (var iReceiver = 0; iReceiver < receiverPositions.Length; iReceiver++)
                {
                    receiverTransform.position = receiverPositions[iReceiver].position;
                    // auralisation.Awake();
                    // auralisation.CalculateOfflineFilter();
                    auralisation.ForceUpdate();
                    auralisation.Update();
                    auralisation.SaveData($"{prefix}{i}");
                    i++;
                }

            }
            
            // sourceTransform.position = oldSourcePos;
            // receiverTransform.position = oldReceiverPos;
            auralisation.audioRenderer = audioRenderer;
            receiver.hrir = oldReceiverHrir;
            source.directivityIr = oldSourceDir;
            Debug.Log("Done.");

        }
        
        [ContextMenu("Measure DnT Lite")]
        public void MeasureDnTLite()
        {
            var auralisation = GetComponent<AuralisationBehavior>();
            
            var l2RefPosition = l2Ref2M.position;
            var mirror = new Vector3(-4,0,0);
            var d0 = Vector3.Distance(l2RefPosition, sourcePositions[0].position);
            var d1 = Vector3.Distance(l2RefPosition + mirror, sourcePositions[0].position);

            auralisation.AuralisationCalculation.refPosition = l2RefPosition;
            auralisation.Update();
            auralisation.SaveData($"{prefix}{CurrentNum}");

            
            Debug.Log("Done. " + prefix);

            UnityEditor.EditorApplication.isPlaying = false;
           
        }

        
        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            foreach (var source in sourcePositions)
            {
                Gizmos.DrawSphere(source.position,0.3f);
                Gizmos.DrawRay(source.position,source.forward);
            }
            Gizmos.color = Color.blue;
            foreach (var receiver in receiverPositions)
            {
                Gizmos.DrawSphere(receiver.position,0.3f);
                Gizmos.DrawRay(receiver.position,receiver.forward);
            }
        }
    }
}