﻿using System.Globalization;
using BA.BACore;
using UnityEditor;
using UnityEngine;


[CustomEditor(typeof(AdditionalLayer))]
[System.Serializable]
[CanEditMultipleObjects]
public class AdditionalLayerEditor : Editor
{
    private AdditionalLayer addLayer;
    private bool foldoutEqual;
    private bool foldoutD1;
    private bool foldoutD2;

    private void OnEnable()
    {
        addLayer = (AdditionalLayer) target;
    }

    /// <summary>
    /// overrides the Inspector view in the Editor mode
    /// </summary>
    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();

        addLayer.material = (MaterialAddLayerData) EditorGUILayout.ObjectField("material",
            addLayer.material, typeof(MaterialAddLayerData), true);

        DrawAdditionalLayer(addLayer);
    }


    public static void DrawAdditionalLayer(AdditionalLayer additionalLayer)
    {
        additionalLayer.Thickness =
            EditorGUILayout.FloatField("Thickness", additionalLayer.Thickness);


        additionalLayer.liningEnum = (AdditionalLayer.LiningEnum)
            EditorGUILayout.EnumPopup("Lining Type", additionalLayer.liningEnum);

        GUILayout.BeginHorizontal();
        {
            additionalLayer.layerType =
                EditorGUILayout.Toggle(
                    new GUIContent("fixed layer",
                        "true: For elements where the insulation layer is fixed directly to the basic construction (without studs or battens) \n\nfalse: For additional layers built with metal or wooden studs or battens not directly connected to the basic structural element where the cavity is filled with a porous insulation layer with an air resistivity r = 5 kPas/m2"),
                    additionalLayer.layerType == 1)
                    ? 1
                    : 2;

            additionalLayer.anchors =
                EditorGUILayout.Toggle(
                    new GUIContent("anchors",
                        "true: If anchors or battens are applied, (4/m2 to 10/m2), different from the reference situation, the correction true"),
                    additionalLayer.anchors);
        }
        GUILayout.EndHorizontal();

        additionalLayer.relativeGlued =
            EditorGUILayout.Slider(
                new GUIContent("Glued Area in %",
                    "The percentage of the area over which the layer is glued to the basic element"),
                additionalLayer.relativeGlued, 0f, 100f);


        GUILayout.BeginHorizontal();
        GUILayout.Label("Name", GUILayout.MinWidth(100));
        GUILayout.Label("Value", GUILayout.Width(70));
        GUILayout.Label("in situ", GUILayout.Width(70));
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("deltaRw", GUILayout.MinWidth(100));
        GUILayout.Label(additionalLayer.deltaRw.ToString("0.00"), GUILayout.Width(70));
        GUILayout.Label(additionalLayer.deltaRwSitu.ToString("0.00"), GUILayout.Width(70));
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("deltaRa", GUILayout.MinWidth(100));
        GUILayout.Label(additionalLayer.deltaRa.ToString("0.00"), GUILayout.Width(70));
        GUILayout.Label(additionalLayer.deltaRaSitu.ToString("0.00"), GUILayout.Width(70));
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("deltaRAtr", GUILayout.MinWidth(100));
        GUILayout.Label(additionalLayer.deltaRAtr.ToString("0.00"), GUILayout.Width(70));
        GUILayout.Label(additionalLayer.deltaRAtrSitu.ToString("0.00"), GUILayout.Width(70));
        GUILayout.EndHorizontal();

        if (GUILayout.Button("Switch Direction"))
            additionalLayer.Direction *= -1;

        if (GUILayout.Button("Update"))
            additionalLayer.UpdateData();
    }
}