﻿using System;
using System.Linq;
using UnityEngine;

namespace BA.BACore
{
    public class GeometryDetectorTouching:GeometryDetector
    {
        public override GeometryDetectionType DetectionType => GeometryDetectionType.Touching;

        public override void GetAttachedWalls(Vector3 position, Vector3 normal,out JunctionTypes  junctionType, out AttachedWallData[] attachedWallDatas)
        {
            GetWalls(position, normal, out junctionType, out attachedWallDatas);
        }
        // TODO
        
        /// <summary>
        ///     Find all attached Walls to the Parent Object GameObject
        /// </summary>
        private void GetWalls(Vector3 position, Vector3 normal, out JunctionTypes  junctionType, out AttachedWallData[] attachedWallDatas)
        {
            // Log.Debug("Set Attached Walls for ");
            var directions = new Vector3[4];
            if (Math.Abs(normal.x - 1) < float.Epsilon)
                directions = new[] {Vector3.forward, Vector3.down, Vector3.back, Vector3.up};
            if (Math.Abs(normal.z - 1) < float.Epsilon)
                directions = new[] {Vector3.left, Vector3.down, Vector3.right, Vector3.up};
            if (Math.Abs(normal.y - 1) < float.Epsilon)
                directions = new[] {Vector3.forward, Vector3.right, Vector3.back, Vector3.left};


            var allColliders = Physics.OverlapSphere(position, 0.4f, BaSettings.Instance.LayerMaskWall);
            var walls = new WallBehaviour[allColliders.Length];

            // Look for next Walls for every direction
            for (var i = 0; i < allColliders.Length; i++)
            {
                walls[i] = allColliders[i].GetComponent<WallBehaviour>();
            }

            var numNull = walls.Count(wall => wall == null);
            if (numNull > 0) Debug.LogError("Cannot detect Attached Walls in ");

            SetAttachedWalls(walls, out junctionType, out attachedWallDatas);
        }
        
        
        /// <summary>
        ///     reorganize Order of Walls
        ///     necessary if it isn't Cross Walls
        /// </summary>
        /// <param name="walls">Array of Walls.</param>
        /// <returns>Junction Type</returns>
        private void SetAttachedWalls(WallBehaviour[] walls, out JunctionTypes junctionType,out AttachedWallData[] attachedWallDatas)
        {
            // Log.Debug("Attached Walls configured: ");
            var wallCounter = 0;

            // Find JunctionType
            var numberOfWalls = walls.Count(wall => wall != null);

            // Sort Junctions
            var wallBehaviours = new WallBehaviour[numberOfWalls];
            switch (numberOfWalls)
            {
                case 2:
                    // L Junction
                    foreach (var wall in walls)
                        if (wall != null)
                        {
                            wallBehaviours[wallCounter] = wall;
                            wallCounter++;
                        }

                    junctionType = JunctionTypes.L_Junction;
                    break;
                case 3:
                    // T-Junction
                    var tJunctionIndexes = GetMainIndexTJunction(walls);
                    wallBehaviours[0] = walls[tJunctionIndexes[0]];
                    wallBehaviours[1] = walls[tJunctionIndexes[1]];
                    wallBehaviours[2] = walls[tJunctionIndexes[2]];
                    junctionType = JunctionTypes.T_Junction;
                    break;
                case 4:
                    // X-Junction
                    wallBehaviours = walls;
                    var xJunctionIndexes = GetMainIndexXJunction(walls);
                    wallBehaviours[0] = walls[xJunctionIndexes[0]];
                    wallBehaviours[1] = walls[xJunctionIndexes[1]];
                    wallBehaviours[2] = walls[xJunctionIndexes[2]];
                    wallBehaviours[3] = walls[xJunctionIndexes[3]];
                    junctionType = JunctionTypes.X_Junction;
                    break;
                default:
                    // ERROR
                    Debug.LogWarning("No Walls Found!");
                    junctionType = JunctionTypes.Undefined;
                    break;
            }

           attachedWallDatas = new AttachedWallData[wallBehaviours.Length];
            for (var i = 0; i < wallBehaviours.Length; i++)
            {
                attachedWallDatas[i]=new AttachedWallData(wallBehaviours[i]);
                if (attachedWallDatas[i].WallBehaviour == null)
                    Debug.LogError("WallBehaviour need to be attached to " + wallBehaviours[i].name,
                        wallBehaviours[i]);
                
            }
            
        }

        private int[] GetMainIndexTJunction(WallBehaviour[] walls)
        {
            if (walls[0].Geometry.Normal == walls[1].Geometry.Normal)
                return new[] {0, 2, 1};
            if (walls[0].Geometry.Normal == walls[2].Geometry.Normal)
                return new[] {0, 1, 2};
            if (walls[1].Geometry.Normal == walls[2].Geometry.Normal)
                return new[] {1, 0, 2};
            Debug.LogError("Cannot Find prependucular index.");
            return new[] {-1, -1, -1};
        }
        private int[] GetMainIndexXJunction(WallBehaviour[] walls)
        {
            if (walls[0].Geometry.Normal == walls[1].Geometry.Normal &&
                walls[2].Geometry.Normal == walls[3].Geometry.Normal)
                return new[] {0, 2, 1, 3};
            if (walls[0].Geometry.Normal == walls[2].Geometry.Normal &&
                walls[1].Geometry.Normal == walls[3].Geometry.Normal)
                return new[] {0, 1, 2, 3};
            if (walls[0].Geometry.Normal == walls[3].Geometry.Normal &&
                walls[1].Geometry.Normal == walls[2].Geometry.Normal)
                return new[] {0, 1, 3, 2};
            
            Debug.LogError("Cannot Find prependucular index.");
            return new[] {-1, -1, -1};
        }
    }
}