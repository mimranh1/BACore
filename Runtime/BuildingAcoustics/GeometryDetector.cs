﻿using System;
using System.Linq;
using UnityEngine;

namespace BA.BACore
{
    /// <summary>
    /// ashkdasdkd
    /// </summary>
    public abstract class GeometryDetector
    {
        public abstract GeometryDetectionType DetectionType { get; }

        public abstract void GetAttachedWalls(Vector3 position, Vector3 normal, out JunctionTypes junctionType,
            out AttachedWallData[] attachedWallDatas);

    }
}