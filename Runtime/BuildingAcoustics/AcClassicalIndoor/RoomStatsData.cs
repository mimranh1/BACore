﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class RoomStatsData
    {
        [SerializeField] protected float volume;
        public float equivalentAbsorptionArea;
        [SerializeField] protected float reverberationTime;

        public void InitDataArray(RoomAcousticProperties roomBehaviour)
        {
            reverberationTime = roomBehaviour.ReverberationTime;
            volume = roomBehaviour.volume;
            equivalentAbsorptionArea = roomBehaviour.EquivalentAbsorptionArea;
        }
    }
}