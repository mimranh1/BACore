﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class ReverberationDataTauRealtimeTauAngle
    {
        public FloatArrays[] reverberationFactorComplexFreq => roomBehaviour.ReverberationCalculator.ReverberationAll;
        [HideInInspector] public FloatArrays[] reverberationAll;
        private RoomAcousticsBehaviour roomBehaviour;
        [SerializeField][HideInInspector] private RoomTauDirectDataPatchesDiffuse roomTauDirectDataDiffuse;
        [SerializeField] [HideInInspector] public FloatArrays[] AllTaus;
        [SerializeField] [HideInInspector] public List<float> AllAnglesDeg;

        public void InitDataArray(RoomAcousticsBehaviour roomBehaviour, int filterLength)
        {
            this.roomBehaviour = roomBehaviour;
        }

        public void UpdateReverberation()
        {
            AllTaus = roomBehaviour.ReverberationCalculator.ReverberationBinaural;
            AllAnglesDeg = roomBehaviour.ReverberationCalculator.AllAnglesDeg;
            reverberationAll = roomBehaviour.ReverberationCalculator.ReverberationAll;
            
            for (var iPatch = 0; iPatch < reverberationFactorComplexFreq.Length; iPatch++)
            {
                Array.Copy(reverberationAll[iPatch].floats, reverberationFactorComplexFreq[iPatch].floats, reverberationFactorComplexFreq[iPatch].floats.Length);
            }
        }
    }
}