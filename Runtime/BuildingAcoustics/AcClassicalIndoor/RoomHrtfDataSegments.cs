﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

namespace BA.BACore
{
    [Serializable]
    public class RoomHrtfDataSegments : RoomHrtfData
    {
       
        [HideInInspector] public GeometrySegments[] secondarySources;
        [HideInInspector] public Vector3S[] positions;
        [HideInInspector] public int[] numSegments;

        public override void InitDataArray(WallBehaviour[] wallBehaviours, int filterLength)
        {
            base.InitDataArray(wallBehaviours, filterLength);
            var numWall = wallBehaviours.Length;
            secondarySources=new GeometrySegments[numWalls];
            positions=new Vector3S[numWalls];
            numSegments=new int[numWalls];
            for (var i = 0; i < numWall; i++)
            {
                var geometrySses = wallBehaviours[i].WallReceiverSegments.SecondarySources;
                secondarySources[i] = new GeometrySegments(geometrySses.ToArray());
                positions[i] = new Vector3S(wallBehaviours[i].WallReceiverSegments.Positions);
                numSegments[i] = positions[i].Vectors.Length;
                Debug.Log($"init Receiver wall {wallBehaviours[i].name} with {numSegments[i]} Secondary Sources.");
            }
        }

        public override void UpdateHrtf(SoundReceiverObject soundReceiverObject)
        {
            for (var i = 0; i < positions.Length; i++)
            {
                soundReceiverObject.GetBinauralDirectivitySum(false, 
                    hrtfComplexFreq[2 * i].floats, hrtfComplexFreq[2 * i + 1].floats,
                    positions[i].Vectors, sqrtOneBy4Pi/numSegments[i],true, true);
            }
        }
    }
}