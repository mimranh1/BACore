﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class ReceiverRoomAcClassical : ReceiverRoom
    {
        [SerializeField] private bool applyReverberation = true;
        [SerializeField] [HideInInspector] private ReceiverRoomAcClassicalData data;
        [SerializeField] [HideInInspector] private int[] indexes;

        public override void Init(WallBehaviour[] partitions, int filterResolution)
        {
            FilterLength = filterResolution;
            base.Init(partitions, FilterLength);

            data = ScriptableObject.CreateInstance<ReceiverRoomAcClassicalData>();
            data.InitDataArray(room, null, FilterLength);

            data.UpdatePosition(SoundReceiverObject.transform.position);
        }

        public void OnValidate()
        {

        }

        public void RegisterSourceRoom(SourceRoomAcClassicalData sourceRoom)
        {
            data.sourceRooms.Add(sourceRoom);
            var index = new List<int>();
            foreach (var flankingPath in sourceRoom.flankingPaths)
            {
                index.Add(FindWallInstanceIndex(flankingPath.id));
            }

            data.SourceRoomsDictionary.Add(sourceRoom, index.ToArray());

        }

        public int FindWallInstanceIndex(int index)
        {
            var walls = room.RoomGeometry.WallBehaviours;
            for (var i = 0; i < walls.Length; i++)
                if (index == walls[i].GetInstanceID())
                    return i;

            Debug.LogError("Wall " + index + " was not found!", EditorUtility.InstanceIDToObject(index));
            return -1;
        }




        public override bool Update(bool forceUpdate = false)
        {
            var hasChanged = forceUpdate || soundReceiverObject.SoundObjectMove();
            if (hasChanged)
            {
                data.UpdatePosition(SoundReceiverObject.transform.position);
            }
            else
            {
                hasChanged = soundReceiverObject.SoundObjectRotate();
            }

            if (!hasChanged) return false;

            data.UpdateOrientation(SoundReceiverObject);

            return true;
        }

        public void UpdateRealtimeFilter(float defaultGainDb, AudioRender audioRender, bool log = false)
        {

            var amplitude = Mathf.Pow(10, defaultGainDb * 0.1f);
            if (data.sourceRooms.Count == 0)
                Debug.LogError("SourceRooms not set anymore.");
            foreach (var sourceRoom in data.sourceRooms)
            {
                if (!sourceRoom.isActive) continue;

                var brir = sourceRoom.brirs;
                brir.Clear();
                indexes = data.SourceRoomsDictionary[sourceRoom];
                var tauComplexFreq = sourceRoom.tauComplexFreq;
                var constFactor = sourceRoom.constFactor;
                for (var iWallFlanking = 0; iWallFlanking < indexes.Length; iWallFlanking++)
                {
                    var iWallReceiver = indexes[iWallFlanking];
                    var dirFactor = data.dirFactor[iWallReceiver] * amplitude * constFactor;
                    var revFactor = data.revFactor[iWallReceiver] * constFactor * amplitude;
                    var tau = tauComplexFreq[iWallFlanking].floats;

                    var xFactorComplexLeft = data.hrtfDataWall.hrtfComplexFreq[2 * iWallReceiver].floats;
                    var xFactorComplexRight = data.hrtfDataWall.hrtfComplexFreq[2 * iWallReceiver + 1].floats;
                    if (applyReverberation)
                    {
                        var reverberation = data.ReverberationComplexFreq;
                        var irLeftFreqComplex = brir.IrLeft.freqComplex;
                        var irRightFreqComplex = brir.IrRight.freqComplex;
                        for (var n = 0; n < tau.Length; n += 2)
                        {
                            var tauReal = tau[n];
                            var tauImag = tau[n + 1];
                            var revReal = reverberation[n];
                            var revImag = reverberation[n + 1];
                            var reverberationLeft = (revReal * tauReal - revImag * tauImag) * revFactor;
                            var reverberationRight = (revReal * tauImag + revImag * tauReal) * revFactor;

                            var xFactorLeftReal = xFactorComplexLeft[n];
                            var xFactorLeftImag = xFactorComplexLeft[n + 1];
                            irLeftFreqComplex[n] +=
                                (xFactorLeftReal * tauReal - 
                                 xFactorLeftImag * tauImag) * dirFactor + reverberationLeft;
                            irLeftFreqComplex[n + 1] +=
                                (xFactorLeftReal * tauImag +
                                 xFactorLeftImag * tauReal) * dirFactor + reverberationRight;

                            irRightFreqComplex[n] +=
                                (xFactorComplexRight[n] * tauReal -
                                 xFactorComplexRight[n + 1] * tauImag) * dirFactor + reverberationLeft;
                            irRightFreqComplex[n + 1] +=
                                (xFactorComplexRight[n] * tauImag +
                                 xFactorComplexRight[n + 1] * tauReal) * dirFactor + reverberationRight;
                        }
                    }
                    else
                    {
                        var totFactor = revFactor + dirFactor;
                        ComplexFloats.MultiplyAdd(xFactorComplexLeft, tau,
                            totFactor,
                            brir.IrLeft.freqComplex);
                        ComplexFloats.MultiplyAdd(xFactorComplexRight, tau,
                            totFactor,
                            brir.IrRight.freqComplex);
                    }
                }

                brir.RunIfft();

                if (audioRender != null)
                    foreach (var sourceId in brir.sourceIds)
                    {
                        audioRender.UpdateFilter(brir.IrLeft.time, brir.IrRight.time, sourceId);
                    }

                if (log)
                    brir.UpdatePlot();
            }

        }

        public override string Save(string path)
        {
            data.Save(path);
            return "";
        }

    }
}