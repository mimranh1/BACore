﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class ReverberationDataPatches
    {
        [SerializeField][HideInInspector] private FloatArrays[] reverberationCutTime;
        [SerializeField][HideInInspector] public FloatArrays[] reverberationComplexFreq;
        [HideInInspector] public FloatArrays[] reverberationFactorComplexFreq;

        public void InitDataArray(FloatArrays[] reverberation, int filterLength, float factor, int delaySamples)
        {
            var receiverRoomReverberation = reverberation;
            reverberationFactorComplexFreq = new FloatArrays[receiverRoomReverberation.Length];
            reverberationCutTime = new FloatArrays[receiverRoomReverberation.Length];
            reverberationComplexFreq = new FloatArrays[receiverRoomReverberation.Length];
            var fftPlan = new FftRealtimeR2C(filterLength);
            for (var j = 0; j < reverberation.Length; j++)
            {
                reverberationFactorComplexFreq[j] = new FloatArrays(filterLength * 2);
                reverberationCutTime[j] = new FloatArrays(new float[filterLength]);
                for (var n = 0;
                    n < Mathf.Min(reverberationCutTime[j].Length - delaySamples, receiverRoomReverberation[j].Length);
                    n++)
                    reverberationCutTime[j][n + delaySamples] = receiverRoomReverberation[j][n];
                var floats = new FloatArrays(filterLength * 2);
                fftPlan.RunFftReal(reverberationCutTime[j].floats, floats.floats);
                reverberationComplexFreq[j] = floats;
                var reverberationComplexFreqFloats = reverberationComplexFreq[j].floats;
                var reverberationFactorComplexFreqFloats = reverberationFactorComplexFreq[j].floats;

                for (var i = 0; i < reverberationFactorComplexFreqFloats.Length; i++)
                {
                    reverberationFactorComplexFreqFloats[i] = reverberationComplexFreqFloats[i] * factor;
                }
            }
        }
    }
}