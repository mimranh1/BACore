﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class SourceRoomAcClassical : SourceRoom
    {
        [SerializeField] private InterpolatorSelector interpolator;
        [HideInInspector] public SourceRoomAcClassicalData data;
        
        public TransferPath TransferPath => transferPath.@object;
        [SerializeField] private TransferPathSelector transferPath;

        public override void Init(int filterResolution, RoomAcousticsBehaviour receiverRoom = null)
        {
            throw new NotImplementedException();
        }

        public override void OnValidate()
        {
            if (interpolator == null) interpolator = ScriptableObject.CreateInstance<InterpolatorSelector>();
            if (transferPath == null) transferPath = ScriptableObject.CreateInstance<TransferPathSelector>();

            if (data != null)
                data.isActive = isActive;
        }

        public override void Init(int filterResolution, ReceiverRoomAcClassical receiverRoom)
        {
            filterLength = filterResolution;
            foreach (var soundSource in soundSourceObjects)
            {
                soundSource.Init(filterResolution);
            }

            var transfer = TransferPath.CalcTauPaths(room, receiverRoom.Room);
            data = ScriptableObject.CreateInstance<SourceRoomAcClassicalData>();
            data.InitDataArray(transfer, receiverRoom.Room, filterResolution, interpolator.@object, isActive);
            receiverRoom.RegisterSourceRoom(data);
        }

        public override bool Update(bool forceUpdate=false)
        {
            return false;
        }
        

        public override string Save(string path,int i)
        {
            data.Save(path,i);
            return "";
        }
    }
}