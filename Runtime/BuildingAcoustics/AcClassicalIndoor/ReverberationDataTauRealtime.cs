﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class ReverberationDataTauRealtime
    {
        [SerializeField][HideInInspector] private float[] reverberationCutTime;
        [SerializeField][HideInInspector] public float[] reverberationComplexFreq;
        [HideInInspector] public FloatArrays[] reverberationFactorComplexFreq;
        [HideInInspector] public FloatArrays[] reverberationAll;
        private int filterLength;
        private int delaySamples;
        private float factor;
        private RoomAcousticsBehaviour roomBehaviour;
        private FftRealtimeR2C fftOut;
        [SerializeField][HideInInspector] private RoomTauDirectDataPatchesDiffuse roomTauDirectDataDiffuse;

        public void InitDataArray(RoomAcousticsBehaviour roomBehaviour, int filterLength, float factor,
            int delaySamples, RoomTauDirectDataPatchesDiffuse roomTauDirectDataDiffuse)
        {
            this.filterLength = filterLength;
            this.delaySamples = delaySamples;
            this.factor = factor;
            this.roomBehaviour = roomBehaviour;
            this.roomTauDirectDataDiffuse = roomTauDirectDataDiffuse;
            reverberationFactorComplexFreq = new FloatArrays[roomTauDirectDataDiffuse.sqrtTauDiffComplexFreq.Length];
            for (var i = 0; i < reverberationFactorComplexFreq.Length; i++)
            {
                reverberationFactorComplexFreq[i] = new FloatArrays(filterLength * 2);
            }

            reverberationCutTime = new float[filterLength];
            fftOut = new FftRealtimeR2C(filterLength);
            reverberationComplexFreq = new float[2 * filterLength];
            // roomBehaviour.CalcReverberation();
            // UpdateReverberation();
        }

        public void UpdateReverberation()
        {
            reverberationAll = roomBehaviour.ReverberationCalculator.ReverberationAll;
            
            for (var iPatch = 0; iPatch < reverberationFactorComplexFreq.Length; iPatch++)
            {
                var fac = factor;
                Array.Clear(reverberationCutTime, 0, reverberationCutTime.Length);
                var reverberation = reverberationAll[iPatch].floats;
                var min = Mathf.Min(reverberationCutTime.Length - delaySamples, reverberation.Length);
                for (var n = 0; n < min; n++)
                {
                    var sample = reverberation[n];
                    if (sample != 0)
                    {
                        reverberationCutTime[n + delaySamples] = sample*fac;
                    }
                }

                fftOut.RunFftReal(reverberationCutTime, reverberationComplexFreq);
                ComplexFloats.Multiply(reverberationComplexFreq,
                    roomTauDirectDataDiffuse.sqrtTauDiffComplexFreq[iPatch].floats,
                    reverberationFactorComplexFreq[iPatch].floats);
            }
        }
    }
}