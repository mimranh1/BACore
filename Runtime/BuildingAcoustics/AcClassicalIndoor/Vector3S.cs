﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class Vector3S
    {
        public Vector3[] Vectors;

        public Vector3S(List<Vector3> input)
        {
            Vectors = input.ToArray();
        } public Vector3S(Vector3[] input)
        {
            Vectors = input;
        }
    }
}