﻿using System;
using System.Linq;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class RoomHrtfDataPatch : RoomHrtfData
    {
        [SerializeField][HideInInspector] private GeometrySegments[] secondarySources;
        [SerializeField][HideInInspector] private WallBehaviour[] patches;
        [SerializeField][HideInInspector] private Vector3S[] positions;
        [SerializeField][HideInInspector] private Vector3S[] normals;
        [SerializeField][HideInInspector] public int[] numPatches;

        public override void InitDataArray(WallBehaviour[] wallBehaviours, int filterLength)
        {
            base.InitDataArray(wallBehaviours, filterLength);
            var totNumPatches = wallBehaviours.Sum(partition => partition.patchesHandler.NumOfPatches);

            secondarySources = new GeometrySegments[totNumPatches];
            positions = new Vector3S[totNumPatches];
            normals = new Vector3S[totNumPatches];
            hrtfComplexFreq = new FloatArrays[2 * totNumPatches];
            numPatches = new int[numWalls];
            patches = new WallBehaviour[totNumPatches];
            var index = 0;
            keys.Clear();
            for (var iPartition = 0; iPartition < numWalls; iPartition++)
            {
                var numberPatches = wallBehaviours[iPartition].patchesHandler.Patches.Count;
                for (var iPatch = 0; iPatch < numberPatches; iPatch++)
                {
                    patches[index] = wallBehaviours[iPartition].patchesHandler.Patches[iPatch];
                    var sourceSegments = patches[index].WallSourceSegments;
                    var geometrySses = sourceSegments.SecondarySources;
                    secondarySources[index] = new GeometrySegments(geometrySses.ToArray());
                    positions[index] = new Vector3S(sourceSegments.Positions);
                    normals[index] = new Vector3S(sourceSegments.Normals);
                    hrtfComplexFreq[2*index] = new FloatArrays(2 * filterLength);
                    hrtfComplexFreq[2*index+1] = new FloatArrays(2 * filterLength);
                    keys.Add(iPartition + "_" + iPatch, index);
                    index++;
                }

                Debug.Log(
                    $"init Source wall {wallBehaviours[iPartition].name} with {numPatches[iPartition]} Secondary Sources.");
            }

        }

        public override void UpdateHrtf(SoundReceiverObject soundReceiverObject)
        {
            for (var i = 0; i < positions.Length; i++)
            {
                soundReceiverObject.GetBinauralDirectivitySum(false, 
                    hrtfComplexFreq[2 * i].floats, hrtfComplexFreq[2 * i + 1].floats,
                    positions[i].Vectors, sqrtOneBy4Pi,true, true);
            }
        }
    }
}