﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class RoomHrtfDataWall : RoomHrtfData
    {
        
        [HideInInspector] public Vector3[] wallPositions;

        public override void InitDataArray(WallBehaviour[] wallBehaviours, int filterLength)
        {
            base.InitDataArray(wallBehaviours,filterLength);
            wallPositions = new Vector3[numWalls];
            for (var i = 0; i < numWalls; i++)
            {
                wallPositions[i] = wallBehaviours[i].Geometry.Position;
            }

            sqrtOneBy4Pi = Mathf.Sqrt(1 / (4 * Mathf.PI));
        }

        public override void UpdateHrtf(SoundReceiverObject soundReceiverObject)
        {
            soundReceiverObject.GetBinauralDirectivity(false, hrtfComplexFreq, wallPositions, sqrtOneBy4Pi,true, true);
        }
    }
}