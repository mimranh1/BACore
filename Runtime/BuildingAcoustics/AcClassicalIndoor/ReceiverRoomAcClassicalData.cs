﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class ReceiverRoomAcClassicalData : ScriptableObject
    {
        public RoomHrtfDataWall hrtfDataWall;
        public float[] dirFactor;
        public float[] revFactor;
        public float absorptionArea;

        [SerializeField] private ReverberationData reverberationData;
        
        public float[] ReverberationComplexFreq=> reverberationData.reverberationComplexFreq;
        public float[] ReverberationFactorComplexFreq=> reverberationData.reverberationFactorComplexFreq;
        
        public List<SourceRoomAcClassicalData> sourceRooms;
        
        public Dictionary<SourceRoomAcClassicalData, int[]> SourceRoomsDictionary;
        
        public void InitDataArray(RoomAcousticsBehaviour roomBehaviour,Interpolator interpolator, int filterLength, int numSoundObjects=1)
        {
            var walls = roomBehaviour.RoomGeometry.WallBehaviours;
            
            revFactor = new float[walls.Length];
            dirFactor = new float[walls.Length];
            hrtfDataWall = new RoomHrtfDataWall();
            hrtfDataWall.InitDataArray(walls, filterLength);
            
            reverberationData = new ReverberationData();
            reverberationData.InitDataArray(roomBehaviour.Reverberation, filterLength, 1,0);

            absorptionArea = roomBehaviour.RoomAcousticProperties.EquivalentAbsorptionArea;

          
            if (sourceRooms == null)
                sourceRooms = new List<SourceRoomAcClassicalData>();
            sourceRooms.Clear();
            if (SourceRoomsDictionary == null)
                SourceRoomsDictionary = new Dictionary<SourceRoomAcClassicalData, int[]>();
            SourceRoomsDictionary.Clear();
        }
        public void UpdatePosition(Vector3 receiverPosition)
        {
            dirFactor = new float[hrtfDataWall.wallPositions.Length];

            for (var i = 0; i < hrtfDataWall.wallPositions.Length; i++)
            {
                var dist = Vector3.Distance(hrtfDataWall.wallPositions[i], receiverPosition);
                var distFactor = Mathf.Sqrt(16f * Mathf.PI) * dist;
                dirFactor[i] = absorptionArea / (absorptionArea + distFactor);
                revFactor[i] = distFactor / (absorptionArea + distFactor);
            }
        }
        
        public void UpdateOrientation(SoundReceiverObject receiverObject)
        {
            hrtfDataWall.UpdateHrtf(receiverObject);
        }

        public void Save(string path,int i=0)
        {
            // AssetDatabase.CreateAsset( this, $"{Application.dataPath}/{path}/ReceiverRoom.asset" );
            System.IO.File.WriteAllText($"{path}/ReceiverRoom.json", JsonUtility.ToJson(this));
            for (var j = 0; j < sourceRooms.Count; j++)
            {
                sourceRooms[j].Save(path,j);
            }
        }
    }
}