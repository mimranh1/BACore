﻿using System;

namespace BA.BACore
{
    [Serializable]
    public class IndexDictionary : SerializableDictionary<string, int> { }
}