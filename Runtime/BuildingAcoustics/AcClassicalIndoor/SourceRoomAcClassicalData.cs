﻿using UnityEngine;

namespace BA.BACore
{
    public class SourceRoomAcClassicalData : ScriptableObject
    {
        public bool isActive = true;
        public FloatArrays[] tauComplexFreq;
        public TauFlankingSecondarySource[] flankingPaths;
        public float constFactor;
        [SerializeField] public BinauralImpulseResponse brirs;
        

        public void InitDataArray(TauFlankingSecondarySource[] tauFlankingSecondarySources,
            RoomAcousticsBehaviour receiverRoom, int filterResolution, Interpolator interpolator, bool active)
        {
            isActive = active;
            brirs = new BinauralImpulseResponse(filterResolution);

            flankingPaths = tauFlankingSecondarySources;
            tauComplexFreq = new FloatArrays[flankingPaths.Length];
            
            Recalculate(receiverRoom, filterResolution, interpolator);
        }

        private void Recalculate(RoomAcousticsBehaviour receiverRoom, int filterResolution, Interpolator interpolator)
        {
            for (var i = 0; i < flankingPaths.Length; i++)
            {
                tauComplexFreq[i] = new FloatArrays
                {
                    floats = interpolator.CalcSqrtTauComplex(flankingPaths[i].transferFunction, filterResolution)
                };
            }

            constFactor = Mathf.Sqrt(flankingPaths[0].separatingArea *
                                     receiverRoom.RoomAcousticProperties.ReverberationTime /
                                     (0.32f * receiverRoom.RoomAcousticProperties.volume * 0.5f));
        }

        public void Save(string path,int i)
        {
            var potion = JsonUtility.ToJson(this);
            System.IO.File.WriteAllText($"{path}/SourceRoom{i}.json", potion);
        }
    }
}