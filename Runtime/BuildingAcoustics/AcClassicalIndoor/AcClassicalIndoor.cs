﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class AcClassicalIndoor : AuralisationCalculation
    {
        public override AuralisationCalculationType Type=> AuralisationCalculationType.ClassicalIndoorModel;
        
        [Subtitle] private string sourceRoomSettings;

        public override SoundSourceObject[] SoundSourceObjects
        {
            get
            {
                var list = new List<SoundSourceObject>();
                foreach (var sourceRoom in sourceRooms)
                {
                    list.AddRange(sourceRoom.SoundSourceObjects);
                }
                return list.ToArray();
            }
        }

        [SerializeField] protected SourceRoomAcClassical[] sourceRooms;

        
        [Subtitle] private string _receiverRoomSettings;
        public override SoundReceiverObject SoundReceiverObject => receiverRoom.SoundReceiverObject;
        [SerializeField] protected ReceiverRoomAcClassical receiverRoom;
   

        [SerializeField][HideInInspector] private bool _isInit = false;

        public WallBehaviour[] Partitions
        {
            get
            {
                var list = new List<WallBehaviour>();
                foreach (var sourceRoom in sourceRooms)
                {
                    list.AddRange(sourceRoom.TransferPath.Partitons);
                }
                return list.ToArray();
            }
        }
        

        public override void OnValidate()
        {
            foreach (var sourceRoom in sourceRooms)
            {
                sourceRoom.OnValidate();
            }

            receiverRoom.OnValidate();
        }

        protected override void Init()
        {
            receiverRoom.Init(Partitions,FilterResolution);
            foreach (var sourceRoom in sourceRooms)
            {
                sourceRoom.Init(FilterResolution, receiverRoom);
            }

            OnValidate();
        }

        protected override void OfflineCalculation()
        {
            _isInit = true;
            foreach (var sourceRoom in sourceRooms)
            {
                var soundSource = sourceRoom.SoundSourceObjects;
                var sourceIds = new int[soundSource.Length];
                for (var i = 0; i < soundSource.Length; i++)
                {
                    sourceIds[i] = soundSource[i].sourceId;
                }

                sourceRoom.data.brirs.sourceIds = sourceIds;
            }
        }
        
        protected override void RealTimeCalculation(AudioRender audioRender,bool log=false)
        {
            // Debug.Log("RealTimeCalculation");
            receiverRoom.UpdateRealtimeFilter(defaultGainDb, audioRender, log);
        }

        protected override bool RealTimeReceiverUpdate()
        {
            var output= receiverRoom.Update(ForceReceiverUpdate);
            ForceReceiverUpdate = false;
            return output;
        }

        protected override bool RealTimeSourceUpdate()
        {
            var hasChanged = ForceSourceUpdate;
            foreach (var sourceRoom in sourceRooms)
            {
                if (sourceRoom.Update())
                    hasChanged = true;
            }

            ForceSourceUpdate = false;
            return hasChanged;
        }

        public override void SaveImpulseResponse(string name="")
        {
            var fullPath = $"{BaSettings.Instance.DataPath}/AcClassical";
            Directory.CreateDirectory(fullPath);
            receiverRoom.Save(fullPath);
            File.WriteAllText($"{fullPath}/AcClassicalIndoor.json", JsonUtility.ToJson(this));
            Debug.Log($"FilterData is Logged to {fullPath}");
        }

        public override void OnDisable()
        {
        }
    }
}