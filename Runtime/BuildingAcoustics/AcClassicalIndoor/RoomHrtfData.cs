﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public abstract class RoomHrtfData 
    {
        [HideInInspector] public FloatArrays[] hrtfComplexFreq;
        [SerializeField][HideInInspector] protected float sqrtOneBy4Pi;
        public int numWalls;
        [SerializeField][HideInInspector] protected IndexDictionary keys;
        
        public virtual void InitDataArray(WallBehaviour[] wallBehaviours, int filterLength)
        {
            keys=new IndexDictionary();
            numWalls = wallBehaviours.Length;
            hrtfComplexFreq = new FloatArrays[numWalls * 2];
            for (var i = 0; i < numWalls; i++)
            {
                hrtfComplexFreq[2 * i] = new FloatArrays(filterLength * 2);
                hrtfComplexFreq[2 * i + 1] = new FloatArrays(filterLength * 2);
                keys.Add(i + "_" + 0, i);
            }

            sqrtOneBy4Pi = Mathf.Sqrt(1 / (4 * Mathf.PI));
        }
        public int GetIndex(int iPartition, int iPatch)
        {
            return keys[iPartition + "_" + iPatch];
        }
        public abstract void UpdateHrtf(SoundReceiverObject soundReceiverObject);
    }
}