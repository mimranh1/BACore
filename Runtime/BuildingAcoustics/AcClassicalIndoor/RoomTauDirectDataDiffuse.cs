﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace BA.BACore
{
    [Serializable]
    public class RoomTauDirectDataDiffuse
    {
        [SerializeField][HideInInspector] protected Interpolator interpolator;
        [SerializeField][HideInInspector] protected FloatArrays[] tauDiff;
        [SerializeField][HideInInspector] protected FloatArrays[] tauDiffSi;
        [HideInInspector] public FloatArrays[] sqrtTauDiffComplexFreq;

        public virtual void InitDataArray(WallBehaviour[] partitions, Interpolator interpolator, int filterLength)
        {
            this.interpolator = interpolator;
            tauDiff = new FloatArrays[partitions.Length];
            tauDiffSi = new FloatArrays[partitions.Length];
            sqrtTauDiffComplexFreq = new FloatArrays[partitions.Length];
            for (var i = 0; i < partitions.Length; i++)
            {
                var tau = 10 ^ (partitions[i].ReductionIndex.ReductionIndex * -0.1f);
                var tauSd = partitions[i].Geometry.Area * tau;
                tauDiff[i] = new FloatArrays(tau.Tf);
                tauDiffSi[i] = new FloatArrays(tauSd.Tf);
                sqrtTauDiffComplexFreq[i] = new FloatArrays(filterLength * 2);
            }
        }

        public virtual void CalcTau()
        {
            for (var i = 0; i < sqrtTauDiffComplexFreq.Length; i++)
            {
                sqrtTauDiffComplexFreq[i].floats =
                    interpolator.CalcSqrtTauComplex(tauDiffSi[i].floats, sqrtTauDiffComplexFreq[i].Length/2);
            }
        }
    }
}