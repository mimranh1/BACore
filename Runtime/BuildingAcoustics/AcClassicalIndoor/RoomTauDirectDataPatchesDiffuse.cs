﻿using System;
using System.Linq;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class RoomTauDirectDataPatchesDiffuse
    {
        [SerializeField][HideInInspector] protected Interpolator interpolator;
        [SerializeField][HideInInspector] protected FloatArrays[] tauDiff;
        [SerializeField][HideInInspector] protected FloatArrays[] tauDiffSi;
        [SerializeField][HideInInspector] protected FloatArrays[] tauDir45;
        [SerializeField][HideInInspector] protected FloatArrays[] tauDiffIso;
        [SerializeField][HideInInspector] protected float[] partitionsArea;
        [SerializeField][HideInInspector] public float[] patchesArea;
        [HideInInspector] public FloatArrays[] sqrtTauDiffComplexFreq;

        public virtual void InitDataArray(WallBehaviour[] partitions, Interpolator interpolator, int filterLength)
        {
            this.interpolator = interpolator;
            var totNumPatches = partitions.Sum(partition => partition.patchesHandler.NumOfPatches);
            tauDiff = new FloatArrays[totNumPatches];
            tauDiffIso = new FloatArrays[totNumPatches];
            tauDir45 = new FloatArrays[totNumPatches];
            tauDiffSi = new FloatArrays[totNumPatches];
            sqrtTauDiffComplexFreq = new FloatArrays[totNumPatches];
            var arrayCounter = 0;
            patchesArea=new float[totNumPatches];
            partitionsArea=new float[partitions.Length];
            for (var iPartition = 0; iPartition < partitions.Length; iPartition++)
            {
                var partition = partitions[iPartition];
                partitionsArea[iPartition] = partition.Geometry.Area;
                for (var iPatch = 0; iPatch < partition.patchesHandler.NumOfPatches; iPatch++)
                {
                    // var tau = 10 ^ (partition.patchesHandler.ReductionIndexPatches[iPatch] * -0.1f);
                    patchesArea[arrayCounter] = partition.patchesHandler.Patches[iPatch].Geometry.Area;
                    var tau45 = partition.patchesHandler.Patches[iPatch].ReductionIndex
                        .CalcSoundReductionIndexAngle(Mathf.PI / 4f);
                    var tauIso = partition.patchesHandler.Patches[iPatch].ReductionIndex
                        .CalcSoundReductionIndexAngle(-Mathf.PI / 4f);
                    
                    var tau = 10^ (partition.patchesHandler.Patches[iPatch].ReductionIndex
                        .CalcSoundReductionIndex(BaSettings.Instance.GetInterpolatedFrequencyBand(filterLength/2))*-0.1f);
                    var tauSi = patchesArea[arrayCounter] * tau;
                    tauDiff[arrayCounter] = new FloatArrays(tau.Tf);
                    tauDiffIso[arrayCounter] = new FloatArrays(tauIso.Tf);
                    tauDir45[arrayCounter] = new FloatArrays(tau45.Tf);
                    tauDiffSi[arrayCounter] = new FloatArrays(tauSi.Tf);
                    sqrtTauDiffComplexFreq[arrayCounter] = new FloatArrays(filterLength * 2);
                    arrayCounter++;
                }
            }

            CalcTau();
        }

        public virtual void CalcTau()
        {
            for (var i = 0; i < sqrtTauDiffComplexFreq.Length; i++)
            {
                sqrtTauDiffComplexFreq[i].floats =
                    interpolator.CalcSqrtTauComplex(tauDiffSi[i].floats, sqrtTauDiffComplexFreq[i].Length/2);
            }
        }
    }
}