﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class RoomTauFlankingData
    {
        [SerializeField][HideInInspector] protected Interpolator interpolator;
        [SerializeField][HideInInspector] protected FloatArrays[] tau;
        [SerializeField][HideInInspector] protected FloatArrays[] alpha;
        [SerializeField][HideInInspector] protected FloatArrays[] sqrtTauAlpha;
        [HideInInspector] public FloatArrays[] sqrtSqrtTauAlphaComplexFreq;

        public void InitDataArray(RoomAcousticsBehaviour roomBehaviour, Interpolator interpolator, int filterLength)
        {
            var walls = roomBehaviour.RoomGeometry.WallBehaviours;
            var activeWalls = roomBehaviour.RoomGeometry.WallBehaviours;
            this.interpolator = interpolator;

            tau = new FloatArrays[activeWalls.Length];
            alpha = new FloatArrays[activeWalls.Length];
            sqrtTauAlpha = new FloatArrays[activeWalls.Length];
            sqrtSqrtTauAlphaComplexFreq = new FloatArrays[activeWalls.Length];
            for (var i = 0; i < activeWalls.Length; i++)
            {
                var wall = walls[i] as WallBehaviour;
                if(wall==null)
                    Debug.LogError("Wall is not of type WallBehaviourBuilding",wall);
                var tau = 10 ^ (wall.ReductionIndex.ReductionIndex * -0.1f);
                var alpha = wall.ReductionIndex.EqualAbsorptionLength;
                var result = (tau / alpha).Sqrt();
                
                this.tau[i] = new FloatArrays {floats = tau.Tf};
                this.alpha[i] = new FloatArrays {floats = alpha.Tf};
                sqrtTauAlpha[i] = new FloatArrays {floats = result.Tf};
                sqrtSqrtTauAlphaComplexFreq[i] = new FloatArrays(filterLength * 2);
            }
        }

        public virtual void CalcTauForFlanking()
        {
            for (var i = 0; i < sqrtSqrtTauAlphaComplexFreq.Length; i++)
            {
                sqrtSqrtTauAlphaComplexFreq[i].floats =
                    interpolator.CalcSqrtTauComplex(sqrtTauAlpha[i].floats, sqrtSqrtTauAlphaComplexFreq[i].Length/2);
            }
        }
    }
}