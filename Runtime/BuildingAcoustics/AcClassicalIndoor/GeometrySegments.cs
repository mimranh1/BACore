﻿using System;
using System.Collections.Generic;

namespace BA.BACore
{
    [Serializable]
    public class GeometrySegments : IGeometrySegments
    {
        public Geometry[] segments;

        public GeometrySegments(List<GeometrySs> input)
        {
            segments = input.ToArray();
        }

        public GeometrySegments(GeometrySs[] input)
        {
            segments = input;
        }

        public Geometry[] Segments => segments;
    }
}