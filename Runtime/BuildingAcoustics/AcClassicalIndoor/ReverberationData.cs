﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class ReverberationData
    {
        [SerializeField][HideInInspector] private float[] reverberationCutTime;
        [SerializeField][HideInInspector] public float[] reverberationComplexFreq;
        [HideInInspector] public float[] reverberationFactorComplexFreq;

        public void InitDataArray(float[] reverberation, int filterLength, float factor, int delaySamples)
        {
            var receiverRoomReverberation = reverberation;
            reverberationFactorComplexFreq = new float[filterLength * 2];

            reverberationCutTime = new float[filterLength];
            for (var n = 0; n < Mathf.Min(reverberationCutTime.Length-delaySamples, receiverRoomReverberation.Length); n++)
                reverberationCutTime[n+delaySamples] = receiverRoomReverberation[n];
            var fftPlan = new FftRealtimeR2C(filterLength);
            reverberationComplexFreq = new float[2 * filterLength];
            fftPlan.RunFftReal(reverberationCutTime, reverberationComplexFreq);
            for (var i = 0; i < reverberationFactorComplexFreq.Length; i++)
            {
                reverberationFactorComplexFreq[i] = reverberationComplexFreq[i] * factor;
            }
        }
    }
}