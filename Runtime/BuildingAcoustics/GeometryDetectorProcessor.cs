﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace BA.BACore
{
    public static class GeometryDetectorProcessor
    {
        private static Dictionary<GeometryDetectionType, GeometryDetector> _detectors=new Dictionary<GeometryDetectionType, GeometryDetector>();
        private static bool _initialized;

        private static void Initialize()
        {
            _detectors.Clear();

            var allDetectorTypes = Assembly.GetAssembly(typeof(GeometryDetector)).GetTypes()
                .Where(t => typeof(GeometryDetector).IsAssignableFrom(t) && t.IsAbstract == false);

            foreach (var detectorType in allDetectorTypes)
            {
                var detector = Activator.CreateInstance(detectorType) as GeometryDetector;
                _detectors.Add(detector.DetectionType, detector);
            }

            _initialized = true;
        }

        public static void ProcessGeometry(Vector3 position, Vector3 normal, out JunctionTypes junctionType,
            out AttachedWallData[] attachedWallDatas, GeometryDetectionType detectionType)
        {
            if (_initialized==false)
                Initialize();

            var detector = _detectors[detectionType];
            detector.GetAttachedWalls(position, normal, out junctionType, out attachedWallDatas);
        }
    }
}