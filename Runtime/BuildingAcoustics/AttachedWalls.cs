﻿using System;
using System.Globalization;
using System.Linq;
using log4net;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class AttachedWalls
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(AttachedWalls));
        [HideInInspector] public int id;
        public AttachedWallData[] AttachedWallDatas => _attachedWallDatas;
        [SerializeReference] private AttachedWallData[] _attachedWallDatas;
        
        [SerializeField][ReadOnly] private JunctionTypes junctionType;
        public JunctionTypes JunctionType => junctionType;
        public float JunctionLength => junctionLength;
        [SerializeField][ReadOnly] private float junctionLength;

        private string _name;
        [SerializeField] [ReadOnly] public Vector3 position;
        [SerializeField] [ReadOnly] private Vector3 normal;
        [SerializeField] [ReadOnly] private float[] alphaKSitu;
        [SerializeField] [ReadOnly] private float[] alphaKLab;
        
        [SerializeField] private float[,] kij;

        
        public AttachedWalls(int id, string name, Vector3 position, Vector3 normal, float junctionLength, GeometryDetectionType detectionType)
        {
            this.id = id;
            _name = name;
            this.position = position;
            this.normal = normal;
            this.junctionLength = junctionLength;
            try
            {
                GeometryDetectorProcessor.ProcessGeometry(position, normal, out junctionType, out _attachedWallDatas,
                    detectionType);

                CalcKij();
                alphaKLab=new float[NumberOfWalls];
                alphaKSitu=new float[NumberOfWalls];
                for (var i = 0; i < NumberOfWalls; i++)
                {
                    alphaKSitu[i] = GetAbsorptionCoefficientSitu(i, kij);
                    alphaKLab[i] = GetAbsorptionCoefficientLab(_attachedWallDatas[i]);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Debug.LogError(name + ": Cannot find Attached Walls.");
                throw;
            }
        }

        /// <summary>
        ///     Number of Walls
        /// </summary>
        public int NumberOfWalls => _attachedWallDatas.Length;

        public float GetAlphaKSitu(int index) => alphaKSitu[FindWallInstanceIndex(index)];

        public float GetAlphaKLab(int index) => alphaKLab[FindWallInstanceIndex(index)];

        /// <summary>
        /// Returns Absorption Coefficient in Situ
        /// </summary>
        /// <returns>float alphaKSitu</returns>
        private float GetAbsorptionCoefficientSitu(int i, float[,] kij)
        {
            var alphaK = -1.0f;
            if (junctionType == JunctionTypes.L_Junction)
            {
                // L Junction
                alphaK = Mathf.Pow(10, kij[i, (i + 1) % 2] / -10.0f) *
                         Mathf.Sqrt(_attachedWallDatas[(i + 1) % 2].CriticalFrequency / BaSettings.Instance.Fref);
            }
            else if (junctionType == JunctionTypes.T_Junction)
            {
                // T Junction
                if (i == 1)
                {
                    alphaK = Mathf.Pow(10, kij[i, 0]/ -10.0f) *
                             Mathf.Sqrt(_attachedWallDatas[0].CriticalFrequency / BaSettings.Instance.Fref)
                             + Mathf.Pow(10, kij[i, 2] / -10.0f) *
                             Mathf.Sqrt(_attachedWallDatas[2].CriticalFrequency / BaSettings.Instance.Fref);
                }
                else
                {
                    var j = i == 0 ? 2 : 0;
                    alphaK = Mathf.Pow(10, kij[i, 1] / -10.0f) *
                             Mathf.Sqrt(_attachedWallDatas[1].CriticalFrequency / BaSettings.Instance.Fref)
                             + Mathf.Pow(10, kij[i, j] / -10.0f)
                             * Mathf.Sqrt(_attachedWallDatas[j].CriticalFrequency / BaSettings.Instance.Fref);
                }
            }
            else if (junctionType == JunctionTypes.X_Junction)
            {
                // Cross Junction
                alphaK = Mathf.Pow(10, kij[i, (i + 1) % 4] / -10.0f) *
                         Mathf.Sqrt(_attachedWallDatas[(i + 1) % 4].CriticalFrequency / BaSettings.Instance.Fref)
                         + Mathf.Pow(10, kij[i, (i + 2) % 4] / -10.0f) *
                         Mathf.Sqrt(_attachedWallDatas[(i + 2) % 4].CriticalFrequency / BaSettings.Instance.Fref)
                         + Mathf.Pow(10, kij[i, (i + 3) % 4] / -10.0f) *
                         Mathf.Sqrt(_attachedWallDatas[(i + 3) % 4].CriticalFrequency / BaSettings.Instance.Fref);
            }

            DataLogger.Log("Junctions." + _name + ".alphaKSitu(:," + (i + 1) + ")", alphaK);
            return alphaK;
        }


        /// <summary>
        /// Returns Absorption Coefficient in Laboratory
        /// </summary>
        /// <param name="wall">WallData</param>
        /// <returns>float alphaKLab</returns>
        public float GetAbsorptionCoefficientLab(AttachedWallData wall)
        {
            //Debug.Log(" wall.CriticalFrequency " + wall.CriticalFrequency);
            var chi = Mathf.Sqrt(31.1f / wall.CriticalFrequency);
            var psy = 44.3f * (wall.CriticalFrequency / wall.Mass);
            var alpha = 1.0f / 3.0f *
                        Mathf.Pow(
                            2 * Mathf.Sqrt(chi * psy) * (1 + chi) * (1 + psy) /
                            (chi * Mathf.Pow(1 + psy, 2) + 2 * psy * (1 + Mathf.Pow(chi, 2))), 2);
            if (Mathf.Abs(alpha) < 0.0001f) Debug.LogError("AlphaK cannot be zero. psy: " + psy + " chi " + chi);
            var alphaKLab = alpha * (1 - 0.9999f * alpha);
            return alphaKLab;
        }
        /// <summary>
        /// Calculate all possible Kijs
        /// </summary>
        public void CalcKij()
        {
            kij = new float[NumberOfWalls, NumberOfWalls];
            var mass0 = _attachedWallDatas[0].Mass;
            var mass1 = _attachedWallDatas[1].Mass;
            switch (junctionType)
            {
                case JunctionTypes.L_Junction:
                    // L-Junction
                    kij[0, 1] = 15 * Mathf.Abs(Mathf.Log10(mass1 / mass0)) - 3;
                    kij[0, 1] = Mathf.Min(kij[0, 1], -2f);
                    kij[1, 0] = 15 * Mathf.Abs(Mathf.Log10(mass1 / mass0)) - 3;
                    kij[1, 0] = Mathf.Min(kij[1, 0], -2f);

                    break;
                case JunctionTypes.T_Junction:
                    // T-Junction
                    kij[0, 1] = 5.7f + 5.7f * Mathf.Pow(Mathf.Log10(mass1 / mass0), 2);
                    kij[2, 1] = 5.7f + 5.7f * Mathf.Pow(Mathf.Log10(mass1 / mass0), 2);
                    kij[1, 0] = 5.7f + 5.7f * Mathf.Pow(Mathf.Log10(mass0 / mass1), 2);
                    kij[1, 2] = 5.7f + 5.7f * Mathf.Pow(Mathf.Log10(mass1 / mass0), 2);
                    kij[0, 2] = kij[0, 1] + 14.1f * Mathf.Log10(mass1 / mass0);
                    kij[2, 0] = kij[0, 2];

                    break;
                case JunctionTypes.X_Junction:
                    // Cross Junction
                    kij[0, 1] = 5.7f + 5.7f * Mathf.Pow(Mathf.Log10(mass1 / mass0), 2);
                    kij[1, 2] = kij[0, 1];
                    kij[2, 3] = kij[0, 1];
                    kij[0, 3] = kij[0, 1];
                    kij[0, 2] = kij[0, 1] + 17.1f * Mathf.Log10(mass1 / mass0);
                    kij[1, 3] = kij[0, 1] + 17.1f * Mathf.Log10(mass0 / mass1);
                    break;
            }

        }



        public TransferPaths CalcRij(float partitionArea)
        {
            if (kij == null)
                CalcKij();
            var rij = new TransferPaths(NumberOfWalls, false);
            var coordinates = rij.GetCoordinatesForRij(NumberOfWalls);
            foreach (var coordinate in coordinates)
            {
                var i = coordinate[0];
                var j = coordinate[1];
                
                var redi = _attachedWallDatas[i].WallBehaviour.ReductionIndex.ReductionIndex;
                var alphai = _attachedWallDatas[i].WallBehaviour.ReductionIndex.EqualAbsorptionLength;
                var si = _attachedWallDatas[i].WallBehaviour.Geometry.Area;
                
                var redj = _attachedWallDatas[j].WallBehaviour.ReductionIndex.ReductionIndex;
                var alphaj = _attachedWallDatas[j].WallBehaviour.ReductionIndex.EqualAbsorptionLength;
                var sj = _attachedWallDatas[j].WallBehaviour.Geometry.Area;
                
                var dvij = kij[i, j] - 10f * (junctionLength / (alphai * alphaj).Sqrt()).Log10();
                rij[i, j] = redi / 2 + (redj / 2) + dvij + 10f * Mathf.Log10(partitionArea / Mathf.Sqrt(si * sj));
            }
            return rij;
        }
        public TransferPaths CalcFlankingFactors()
        {
            if (kij == null)
                CalcKij();
            var rij = new TransferPaths(NumberOfWalls, false);
            var coordinates = rij.GetCoordinatesForRij(NumberOfWalls);
            foreach (var coordinate in coordinates)
            {
                var i = coordinate[0];
                var j = coordinate[1];
                
                var si = _attachedWallDatas[i].WallBehaviour.Geometry.Area;
                var sj = _attachedWallDatas[j].WallBehaviour.Geometry.Area;

                var SdKijlij = Mathf.Pow(10, -0.1f * kij[i, j]) * Mathf.Sqrt(si * sj) * junctionLength;
                rij[i, j] = new TransferFunction(new[] {Mathf.Sqrt(SdKijlij)});
            }
            return rij;
        }
        
        /// <summary>
        /// Calculate All Dvijs
        /// </summary>
        private TransferPaths CalcDvijs(float[,] kij)
        {
            var dvijs = new TransferPaths(NumberOfWalls, false);            
            var alpha = new TransferFunction[NumberOfWalls];
            for (var k = 0; k < NumberOfWalls; k++)
            {
                alpha[k] = _attachedWallDatas[k].WallBehaviour.ReductionIndex.EqualAbsorptionLength;
                if (ReferenceEquals(alpha[k], null) || alpha[k].NumFrequency == 0)
                {
                    Log.Error(_attachedWallDatas[k].WallBehaviour.name + " not Processed jet!");
                    Debug.LogError(_attachedWallDatas[k].WallBehaviour.name + " not Processed jet.", _attachedWallDatas[k].WallBehaviour);
                }
            }

            DataLogger.Log("Junctions." + _name + ".alphaK", alpha);
            var coordinates = dvijs.GetCoordinatesForRij(NumberOfWalls);
            dvijs.numberOfWalls = NumberOfWalls;
            foreach (var coordinate in coordinates)
            {
                var i = coordinate[0];
                var j = coordinate[1];
                if (ReferenceEquals(alpha[i], null) || alpha[i].NumFrequency == 0)
                {
                    Log.Error(_attachedWallDatas[i].WallBehaviour.name + " not Processed jet!");
                    Debug.LogError(_attachedWallDatas[i].WallBehaviour.name + " not Processed jet.", _attachedWallDatas[i].WallBehaviour);
                }

                if (ReferenceEquals(alpha[j], null) || alpha[j].NumFrequency == 0)
                {
                    Log.Error(_attachedWallDatas[j].WallBehaviour.name + " not Processed jet!");
                    Debug.LogError(_attachedWallDatas[j].WallBehaviour.name + " not Processed jet.", _attachedWallDatas[j].WallBehaviour);
                }

                dvijs[i, j] = kij[i, j] - 10 * (junctionLength / (alpha[i] * alpha[j]).Sqrt()).Log(10);
            }


            return dvijs;
        }

        /// <summary>
        ///     find Index of Wall by Name
        /// </summary>
        /// <returns>Index in _wallsGameObjects</returns>
        public int FindWallInstanceIndex(int index)
        {
            for (var i = 0; i < NumberOfWalls; i++)
            {
                var instanceID = _attachedWallDatas[i].WallBehaviour.GetInstanceID();
                if (index == instanceID)
                    return i;
            }

            //Log.Error("Wall " + name + " was not found!");
            return -1;
        }
        
    }
}