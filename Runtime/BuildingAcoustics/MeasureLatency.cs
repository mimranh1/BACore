﻿using System.IO;
using UnityEngine;

namespace BA.BACore
{
    public class MeasureLatency : MonoBehaviour
    {
        [SerializeField] private Transform[] sourcePositions;
        [SerializeField] private Transform[] receiverPositions;
        [SerializeField] private SoundSourceObject source;
        [SerializeField] private SoundReceiverObject receiver;
        [SerializeField] private int numberOfRays;
        [SerializeField] private double[] latenciesSource;
        [SerializeField] private double[] latenciesReceiver;
        [SerializeField] private double[] latenciesFilter;
        
        [SerializeField] private double[] latenciesInit;
        [SerializeField] private double[] latenciesOffline;
        
        [SerializeField] private string prefix="_Latency";
        
        
        [ContextMenu("Measure Latency")]
        public void MeasureLatencies()
        {
            var auralisation = GetComponent<AuralisationBehavior>();
            var audioRenderer = auralisation.audioRenderer;
            latenciesSource = new double[numberOfRays];
            latenciesReceiver = new double[numberOfRays];
            latenciesFilter = new double[numberOfRays];
            latenciesInit = new double[numberOfRays];
            latenciesOffline = new double[numberOfRays];
            auralisation.audioRenderer = null;
            var sourceTransform = source.transform;
            var receiverTransform = receiver.transform;
            var oldSourcePos = sourceTransform.position;
            var oldReceiverPos = receiverTransform.position;
            for (var n = 0; n < numberOfRays; n++)
            {
                auralisation.Init();
                auralisation.CalculateOfflineFilter();
                sourceTransform.position = sourcePositions[n%sourcePositions.Length].position;
                receiverTransform.position = receiverPositions[n%receiverPositions.Length].position;

                auralisation.Update();
                // auralisation.SaveData($"{prefix}{n}");
                latenciesSource[n] = auralisation.AuralisationCalculation.SourceUpdate;
                latenciesReceiver[n] = auralisation.AuralisationCalculation.ReceiverUpdate;
                latenciesFilter[n] = auralisation.AuralisationCalculation.FilterUpdate;
                latenciesOffline[n] = auralisation.AuralisationCalculation.Offline;
                latenciesInit[n] = auralisation.AuralisationCalculation.InitTime;
            }
            sourceTransform.position = oldSourcePos;
            receiverTransform.position = oldReceiverPos;
            auralisation.audioRenderer = audioRenderer;
            
            var brir = JsonUtility.ToJson(this, true);
            var fullPath = $"{BaSettings.Instance.DataPath}/AcBuildingAcoustics";
            Directory.CreateDirectory(fullPath);
            File.WriteAllText($"{fullPath}/BuidlingsAcoustics{prefix}.json", brir);
            Debug.Log("Done.");
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            if(sourcePositions.Length>0)
                foreach (var source in sourcePositions)
                {
                    if (source != null)
                        Gizmos.DrawSphere(source.position, 0.3f);
                }
            Gizmos.color = Color.blue;
            if(receiverPositions.Length>0)
                foreach (var receiver in receiverPositions)
                {
                    if (receiver != null)
                        Gizmos.DrawSphere(receiver.position, 0.3f);
                }
        }
    }
}