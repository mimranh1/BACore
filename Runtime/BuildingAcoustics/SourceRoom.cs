﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public  abstract class SourceRoom
    {
        public bool isActive = true;
        public SoundSourceObject[] SoundSourceObjects => soundSourceObjects;
        [SerializeField] protected SoundSourceObject[] soundSourceObjects;
        public RoomAcousticsBehaviour Room => room;
        [SerializeField] protected RoomAcousticsBehaviour room;

        [SerializeField][HideInInspector] protected int filterLength;

        public abstract void Init(int filterResolution, ReceiverRoomAcClassical receiverRoom = null);


        public abstract void Init(int filterResolution, RoomAcousticsBehaviour receiverRoom = null);

        public virtual void OnValidate()
        {
            
        }
       
        
        public abstract bool Update(bool forceUpdate=false);


        public abstract string Save(string path, int i);

    }
}