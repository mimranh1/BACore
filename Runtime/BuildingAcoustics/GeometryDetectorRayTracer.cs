﻿using System;
using System.Linq;
using UnityEngine;

namespace BA.BACore
{
    public class GeometryDetectorRayTracer:GeometryDetector
    {
        public override GeometryDetectionType DetectionType => GeometryDetectionType.RayTracing;

        public override void GetAttachedWalls(Vector3 position, Vector3 normal,out JunctionTypes  junctionType, out AttachedWallData[] attachedWallDatas)
        {
            GetWalls(position, normal, out junctionType, out attachedWallDatas);
        }
        
        
        /// <summary>
        ///     Find all attached Walls to the Parent Object GameObject
        /// </summary>
        private void GetWalls(Vector3 position, Vector3 normal, out JunctionTypes  junctionType, out AttachedWallData[] attachedWallDatas)
        {
            // Log.Debug("Set Attached Walls for ");
            var directions = new Vector3[4];
            if (Math.Abs(normal.x - 1) < float.Epsilon)
                directions = new[] {Vector3.forward, Vector3.down, Vector3.back, Vector3.up};
            if (Math.Abs(normal.z - 1) < float.Epsilon)
                directions = new[] {Vector3.left, Vector3.down, Vector3.right, Vector3.up};
            if (Math.Abs(normal.y - 1) < float.Epsilon)
                directions = new[] {Vector3.forward, Vector3.right, Vector3.back, Vector3.left};

            var walls = new WallBehaviour[] {null, null, null, null};

            // Look for next Walls for every direction
            for (var i = 0; i < 4; i++)
            {
                var ray = new Ray
                {
                    origin = position,
                    direction = directions[i]
                };

                if (!Physics.Raycast(ray, out var hit, 3f, BaSettings.Instance.LayerMaskWall))
                    continue;

                if (hit.collider.gameObject.layer == BaSettings.Instance.LayerWall)
                    walls[i] = hit.collider.gameObject.GetComponent<WallBehaviour>();
            }

            var numNotNull = walls.Count(wall => wall != null);
            if (numNotNull == 0) Debug.LogError("Cannot detect Attached Walls in ");

            SetAttachedWalls(walls, out junctionType, out attachedWallDatas);
        }
        
        
        /// <summary>
        ///     reorganize Order of Walls
        ///     necessary if it isn't Cross Walls
        /// </summary>
        /// <param name="walls">Array of Walls.</param>
        /// <returns>Junction Type</returns>
        private void SetAttachedWalls(WallBehaviour[] walls, out JunctionTypes junctionType,out AttachedWallData[] attachedWallDatas)
        {
            // Log.Debug("Attached Walls configured: ");
            var numberOfWalls = 0;
            var wallCounter = 0;

            // Find JunctionType
            foreach (var wall in walls)
                if (wall != null)
                    numberOfWalls++;

            // Sort Junctions
            var wallBehaviours = new WallBehaviour[numberOfWalls];
            switch (numberOfWalls)
            {
                case 2:
                    // L Junction
                    foreach (var wall in walls)
                        if (wall != null)
                        {
                            wallBehaviours[wallCounter] = wall;
                            wallCounter++;
                        }

                    junctionType = JunctionTypes.L_Junction;
                    break;
                case 3:
                    // T-Junction
                    for (wallCounter = 0; wallCounter < 4; wallCounter++)
                        if (walls[wallCounter] == null)
                        {
                            wallBehaviours[0] = walls[(wallCounter + 1) % 4];
                            wallBehaviours[1] = walls[(wallCounter + 2) % 4];
                            wallBehaviours[2] = walls[(wallCounter + 3) % 4];
                        }

                    junctionType = JunctionTypes.T_Junction;
                    break;
                case 4:
                    // X-Junction
                    wallBehaviours = walls;
                    junctionType = JunctionTypes.X_Junction;
                    break;
                default:
                    // ERROR
                    Debug.LogWarning("No Walls Found!");
                    junctionType = JunctionTypes.Undefined;
                    break;
            }

           attachedWallDatas = new AttachedWallData[wallBehaviours.Length];
            for (var i = 0; i < wallBehaviours.Length; i++)
            {
                attachedWallDatas[i]=new AttachedWallData(wallBehaviours[i]);
                if (attachedWallDatas[i].WallBehaviour == null)
                    Debug.LogError("WallBehaviour need to be attached to " + wallBehaviours[i].name,
                        wallBehaviours[i]);
                
            }
            
        }
    }
}