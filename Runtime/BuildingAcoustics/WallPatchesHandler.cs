﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class WallPatchesHandler : IWallPatchesHandler
    {
        [HideInInspector] public int id;
        [SerializeField] [ReadOnly] private List<WallBehaviour> patches = new List<WallBehaviour>();
        [SerializeField] private bool[] isActive;
        private Vector3[][] _secondarySourcesPosPatches;
        [SerializeField] private TransferFunction[] _reductionIndexPatches;

        private readonly WallGeometry _geometry;
        [HideInInspector]public WallGeometry Geometry;
        private readonly Transform _transform;

        public WallPatchesHandler(int id, WallGeometry geometry, Transform transform)
        {
            this.id = id;
            _geometry = geometry;
            _transform = transform;
            GetNumberOfPatches();
            SetPositions();
            SetReductionIndex();
        }

        public List<WallBehaviour> Patches => patches;

        public bool[] IsActive => isActive;

        public Vector3[][] SecondarySourcesPosPatches => _secondarySourcesPosPatches;

        public TransferFunction[] ReductionIndexPatches => _reductionIndexPatches;
        public int NumOfPatches => patches.Count;

        

        private void SetPositions()
        {
            if (NumOfPatches > 0)
            {
                _secondarySourcesPosPatches = new Vector3[NumOfPatches][];
                for (var iPatch = 0; iPatch < NumOfPatches; iPatch++)
                {
                    _secondarySourcesPosPatches[iPatch] = patches[iPatch].WallReceiverSegments.Positions.ToArray();
                }
            }
            else
            {
                _secondarySourcesPosPatches = null;
            }
        }
        
        private int GetNumberOfPatches()
        {
            patches.Clear();
            var childNum = _transform.childCount;
            var patchesNum = 0;
            if (childNum == 0) return -1;

            for (var i = 0; i < childNum; i++)
            {
                var wallProp = _transform.GetChild(i).GetComponent<WallBehaviour>();
                if (wallProp != null)
                {
                    patches.Add(wallProp);
                    patchesNum++;
                }
            }

            return patchesNum;
        }

        private void SetReductionIndex()
        {
            _reductionIndexPatches = new TransferFunction[NumOfPatches];
            isActive = new bool[NumOfPatches];
            for (var i = 0; i < NumOfPatches; i++)
            {
                _reductionIndexPatches[i] = patches[i].ReductionIndex.ReductionIndex;
            }
        }

    }
}