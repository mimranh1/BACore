﻿namespace BA.BACore
{
    public enum GeometryDetectionType
    {
        RayTracing,
        Touching
    }
}