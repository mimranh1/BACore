﻿using System;
using log4net;
using UnityEngine;

namespace BA.BACore
{
    /// <summary>
    /// Manage arrays of TransferFunctions  with source wall id i and receiver wall id j
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    [Serializable]
    public class TransferPaths
    {
        #region Constructor

        /// <summary>
        /// Constructor define coordinates according if it is Kij style or not
        /// </summary>
        /// <param name="numOfWalls">Number of Walls</param>
        /// <param name="isKij">Kij Style or not</param>
        public TransferPaths(int numOfWalls, bool isKij)
        {
            this.isKij = isKij;
            coordinates = GetCoordinatesForRij(numOfWalls);
            numberOfWalls = numOfWalls;
            tps = new TransferFunction[coordinates.Length];
        }

        #endregion

        /// <summary>
        /// Save the Number of Walls 
        /// </summary>
        public int numberOfWalls = -1;

        #region Private Members

        [SerializeField] private int[][] coordinates;
        [SerializeField] private TransferFunction[] tps;
        [SerializeField] private bool isKij;
        private static readonly ILog Log = LogManager.GetLogger(typeof(TransferPaths));

        #endregion

        #region Properties

        /// <summary>
        /// Read and Write TransferFunction
        /// </summary>
        /// <param name="i">input i</param>
        /// <param name="j">input j</param>
        /// <returns></returns>
        public TransferFunction this[int i, int j]
        {
            get => Getij(i, j);
            set => Add(value, i, j);
        }

        /// <summary>
        /// return Number of Walls
        /// </summary>
        public int NumOfWalls => numberOfWalls == -1 ? coordinates.Length : numberOfWalls;

        /// <summary>
        /// Return true if the TransferPaths contain Kij (single values)
        /// </summary>
        public bool IsKij => isKij;

        #endregion

        #region Public Methods

        /// <summary>
        /// Debug Line in Unity
        /// </summary>
        public void DebugLog()
        {
            Log.Info("counter " + coordinates.Length);
        }


        /// <summary>
        /// find Coordinate List according if it is Kij style or not
        /// </summary>
        /// <param name="numOfWalls">Number of Walls</param>
        /// <returns></returns>
        public int[][] GetCoordinatesForRij(int numOfWalls)
        {
            if (isKij)
                switch (numOfWalls)
                {
                    case 2:
                        return new[] {new[] {0, 1}, new[] {1, 0}};
                    case 3:
                        return new[]
                        {
                            new[] {0, 1}, new[] {0, 2},
                            new[] {1, 2}, new[] {1, 0}, new[] {2, 0},
                            new[] {2, 1}
                        };
                    case 4:
                    {
                        isKij = false;
                        return new[]
                        {
                            new[] {0, 1}, new[] {0, 2},
                            new[] {0, 3}, new[] {1, 2}, new[] {1, 3},
                            new[] {2, 3}
                        };
                    }

                    default:
                        Log.Warn("number of wallBehaviours invalid");
                        return new[] {new[] {0, 0}};
                }

            switch (numOfWalls)
            {
                case 2:
                    return new[] {new[] {0, 1}};
                case 3:
                    return new[]
                    {
                        new[] {0, 1}, new[] {0, 2},
                        new[] {1, 2}
                    };
                case 4:
                    return new[]
                    {
                        new[] {0, 1}, new[] {0, 2},
                        new[] {0, 3}, new[] {1, 2}, new[] {1, 3},
                        new[] {2, 3}
                    };
                default:
                    Log.Warn("number of wallBehaviours invalid");
                    return new[] {new[] {0, 0}};
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Add TransferFunction to correct coordinate
        /// </summary>
        /// <param name="tf">Input TransferFunction</param>
        /// <param name="i">Input i</param>
        /// <param name="j">Input j</param>
        private void Add(TransferFunction tf, int i, int j)
        {
            var indexji = -1;
            var index = -1;
            for (var k = 0; k < coordinates.Length; k++)
                if (coordinates[k][0] == i && coordinates[k][1] == j)
                    index = k;
                else if (coordinates[k][0] == j && coordinates[k][1] == i) indexji = k;

            if (index == -1)
            {
                if (indexji != -1)
                    index = indexji;
                else
                    Log.Error("TransferFunction i " + i + " j " + j + " not found.");
            }

            tps[index] = tf;
        }

        /// <summary>
        /// Returns TransferFunction to correct coordinate
        /// </summary>
        /// <param name="i">Input i</param>
        /// <param name="j">Input j</param>
        private TransferFunction Getij(int i, int j)
        {
            var indexji = -1;
            var index = -1;
            for (var k = 0; k < coordinates.Length; k++)
            {
                if (coordinates[k][0] == i && coordinates[k][1] == j)
                    index = k;
                if (coordinates[k][0] == j && coordinates[k][1] == i)
                    indexji = k;
            }

            if (index != -1)
                return tps[index];

            if (indexji != -1)
            {
                index = indexji;
            }
            else
            {
                Log.Error("Index cannot be found." + " i " + i + " j " + j);
                return new TransferFunction();
            }

            try
            {
                if (tps[index].NumFrequency == 0)
                    i = 0;
            }
            catch
            {
                Log.Error("tf was not init.");
            }

            return tps[index];
        }

        #endregion
    }
}