﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class SingleTauFlankingPath
    {
        public TransferFunction transferFunction;
        public FlankingWallData sourceWall;
        public FlankingWallData receiverWall;

        public SingleTauFlankingPath(WallBehaviour sourceWall, WallBehaviour receiverWall, TransferFunction transferFunction)
        {
            this.transferFunction = transferFunction;
            this.sourceWall = new FlankingWallData
            {
                id=sourceWall.GetInstanceID(),
                Position = sourceWall.Geometry.Position,
                Dimension = sourceWall.Geometry.Dimensions,
                Normal = sourceWall.Geometry.Normal,
                PatchesPosition = sourceWall.WallReceiverSegments.Positions.ToArray(),
            };
            this.receiverWall = new FlankingWallData
            {
                id=sourceWall.GetInstanceID(),
                Position = receiverWall.Geometry.Position,
                Dimension = receiverWall.Geometry.Dimensions,
                Normal = receiverWall.Geometry.Normal,
                PatchesPosition = receiverWall.WallReceiverSegments.Positions.ToArray(),
            };
        }
    }

    
}