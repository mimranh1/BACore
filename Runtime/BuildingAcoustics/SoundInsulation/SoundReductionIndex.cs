﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public abstract class SoundReductionIndex : ExtendedScriptableObject<SoundReductionIndex,SoundReductionIndexType>
    {
        [HideInInspector] public int id;
        
        public abstract TransferFunction ReductionIndex {get; }
        
        public abstract TransferFunction EqualAbsorptionLength { get; }
        /// <summary>
        /// Return Mass per m^2 of the Wall
        /// </summary>
        public abstract float Mass {get; }
        /// <summary>
        /// Get Critical Frequency of the Wall
        /// </summary>
        public abstract float CriticalFrequency { get; }

        /// <summary>
        /// correction term for fc
        /// </summary>
        public abstract TransferFunction CriticalFrequencyEff { get; }


        public abstract TransferFunction CalcSoundReductionIndex(TransferFunction f=null);
        public abstract TransferFunction CalcSoundReductionIndexAngle(float incidentAngleRad, TransferFunction f=null);

        public abstract void OnValidate();
        public abstract void Init(GameObject gameObject);
    }
}