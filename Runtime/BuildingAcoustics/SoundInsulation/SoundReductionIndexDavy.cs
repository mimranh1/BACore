﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class SoundReductionIndexDavy : SoundReductionIndex
    {
        public override SoundReductionIndexType Type => SoundReductionIndexType.Davy;
        public override TransferFunction ReductionIndex => reductionIndex;
        [SerializeField] private TransferFunction reductionIndex;

        public override TransferFunction EqualAbsorptionLength => null;
        [SerializeField] [HideInInspector] private WallBehaviour wallBehaviour;

        [SerializeField] public WallStructureMaterial wallStructureMaterial;

        public bool plotAngleTau;

        /// <summary>
        /// Return Mass per m^2 of the Wall
        /// </summary>
        public override float Mass => wallStructureMaterial.Density * wallBehaviour.Geometry.Thickness;
        /// <summary>
        /// Get Critical Frequency of the Wall
        /// </summary>
        public override float CriticalFrequency => Mathf.Pow(BaSettings.Instance.C0, 2) / (1.8f * wallStructureMaterial.QuasiLongPhaseVelocity * wallBehaviour.Geometry.Thickness);

        /// <summary>
        /// correction term for fc
        /// </summary>
        public override TransferFunction CriticalFrequencyEff => CriticalFrequency *
                                                                 (4.05f * wallBehaviour.Geometry.Thickness * BaSettings.Instance.ThirdOctaveFrequencyBandShort /
                                                                  wallStructureMaterial.QuasiLongPhaseVelocity);

        public override void Init(GameObject gameObject)
        {
            this.id = gameObject.GetInstanceID();
            this.wallBehaviour = gameObject.GetComponent<WallBehaviour>();
        }
        
        public override TransferFunction CalcSoundReductionIndex(TransferFunction f=null)
        {
            if(wallStructureMaterial?.MaterialStructure==null)
                Debug.LogError("Wall Material not set.", wallBehaviour);
            if (f == null)
                f = BaSettings.Instance.ThirdOctaveFrequencyBandExtended;
            var c0 = BaSettings.Instance.C0;
            var rho0 = BaSettings.Instance.Rho0;
            reductionIndex =-10f*(DavyTauRes(wallBehaviour, f, c0, rho0)).Log10();
            if (reductionIndex.NumFrequency == 37)
                reductionIndex.Tf[0] = reductionIndex.Tf[1] - 3f;
            // _reductionIndex = DavyTauRes(wallBehaviour, f, c0,rho0,Mathf.PI/4f);
            reductionIndex.UpdateCurve();
            return reductionIndex;
        }

        public override TransferFunction CalcSoundReductionIndexAngle(float incidentAngleRad, TransferFunction f=null)
        {
            if(wallStructureMaterial?.MaterialStructure==null)
                Debug.LogError("Wall Material not set.", wallBehaviour);
            if (f == null)
                f = BaSettings.Instance.ThirdOctaveFrequencyBandExtended;
            var c0 = BaSettings.Instance.C0;
            var rho0 = BaSettings.Instance.Rho0;
            if (!plotAngleTau)
                return DavyTauRes(wallBehaviour, f, c0, rho0, incidentAngleRad);
            var davyTauRes = DavyTauRes(wallBehaviour, f, c0, rho0, incidentAngleRad);
            reductionIndex =-10f*davyTauRes.Log10();
            if (reductionIndex.NumFrequency == 37)
                reductionIndex.Tf[0] = reductionIndex.Tf[1] - 3f;
            // _reductionIndex = DavyTauRes(wallBehaviour, f, c0,rho0,Mathf.PI/4f);
            reductionIndex.UpdateCurve();
            return davyTauRes;
        }
        
        

        public override void OnValidate()
        {

        }

        private TransferFunction DavyTauRes(WallBehaviour wallBehaviour, TransferFunction f, float c0,
            float rho0, float incAngleRad = Single.PositiveInfinity)
        {
            var length1 = wallBehaviour.Geometry.Length;
            var length2 = wallBehaviour.Geometry.Width;
            var h = wallBehaviour.Geometry.Thickness;
            // var rho = wallStructureMaterial.Density;
            // var etaInit = wallStructureMaterial.InternalLossFactor;
            // var v = wallStructureMaterial.PoissonsRatio;
            // var youngsModulus = wallStructureMaterial.YoungsModulus;
            // var shearModulus = wallStructureMaterial.ShearModulus;
            var rho = 2500f;
            var etaInit = 0.001f;
            var v = 0.22f;
            var youngsModulus = 6.5e10f;
            var shearModulus = youngsModulus / (2f * (1f + v));;
            var cL = 5227f;
            var m = rho*h;
            var fc = Mathf.Pow(BaSettings.Instance.C0, 2) / (1.8f * cL * h);
            
            if (incAngleRad>=0)
                return CalcDavyComplete(f, c0, rho0, etaInit, m, v, shearModulus, youngsModulus, rho, h, length1, length2,
                    fc, incAngleRad);
            else
            {
                var sigmaThetaC = SigmaC(length1, length2, fc, f, c0);
                // var sigmaThetaC = SigmaOblique(f, fc, c0, length1, length2, thetaC); //other davy paper

                var alphaK = 29.5387f/3;
                var eta = TotalLossFactorSingle(f, m, etaInit, length1 * length2, fc,
                    sigmaThetaC, c0, rho0, alphaK);
                return ReductionIndexCalculator.GetTauSitu(eta, length2, length1, fc,
                    f, rho0, c0, cL, rho, m);
            }
        }

        
        public static TransferFunction CalcDavyCompleteOld(TransferFunction f, float c0, float rho0, float etaInit,
            float m, float v,
            float shearModulus, float youngsModulus, float rho, float h, float length1, float length2, float fc,
            float incAngleRad = Single.PositiveInfinity,TransferFunction eta=null)
        {
            var omega = 2f * Mathf.PI * f;
            // var eta = EtaDavy(f, etaInit, m); // Davy (11)
            var abc = GetDavyAbc(omega, v, shearModulus, youngsModulus, rho, h);
            var a = omega * m / (2f * rho0 * c0); // Davy (13)
            var sigmaForced = SigmaForced(length1, length2, fc, f, c0);
            var sigmaThetaC = SigmaC(length1, length2, fc, f, c0);
            var thetaC = GetThetaC(f, fc);
            // var sigmaThetaC = SigmaOblique(f, fc, c0, length1, length2, thetaC); //other davy paper

            var alphaK = 29.5387f;
            if (eta == null)
                eta = TotalLossFactorSingle(f, m, etaInit, length1 * length2, fc,
                    sigmaThetaC, c0, rho0, alphaK);
            if (float.IsPositiveInfinity(incAngleRad))
            {
                var tauRes = TauRes(omega, sigmaThetaC, eta, m, rho0, c0, f, fc);
                var tauFor = (2f * sigmaForced) / (a * a); // Davy (42)
                var tauDiff = tauRes * abc + tauFor;
                return tauDiff;
            }
            else
            {
                var sigmaOblique = SigmaOblique(f, fc, c0, length1, length2, incAngleRad);
                sigmaOblique = sigmaThetaC;
                if (eta == null)
                    eta = etaInit + ((sigmaOblique * rho0 * c0 * 2) / (m * omega)); // Davy (9+10)
                var tauResAngle = TauResAngle(omega, sigmaThetaC, eta, m, rho0, c0, f, fc, incAngleRad);
                var tauForAngle = (sigmaOblique * sigmaOblique) / (a * a); // Davy (41)
                var tauAngle = tauResAngle * abc + tauForAngle;
                return tauAngle;
            }
        }

        private static TransferFunction TotalLossFactorSingle(TransferFunction f, float m, float etaInitial, float S,
            float fc,
            TransferFunction sigmaFreeWave, float c0, float rho0, float alphaK)
        {
            var etaTotalSitu = etaInitial + ((2f * rho0 * c0 * sigmaFreeWave) / (2f * Mathf.PI * f * m)) +
                               (c0 / ((Mathf.PI * Mathf.PI) * S * (f * fc).Sqrt())) * alphaK;
            // return new TransferFunction(new[]
            // {
            //     0.582715847560751f, 0.519646065678519f, 0.461688008657858f, 0.413466797405491f, 0.370341517767787f,
            //     0.327921658093948f, 0.293835944576311f, 0.263352456460814f, 0.235173098775519f, 0.209279255114715f,
            //     0.187740585512957f, 0.167840731813497f, 0.149581003571168f, 0.134462399908905f, 0.121294055136648f,
            //     0.115724370997428f, 0.103210947641903f, 0.0910950630832299f, 0.0807898074062449f, 0.0717330611174826f,
            //     0.0643764469948250f
            // });
            return etaTotalSitu;
        }

        private static float CoincidentAngle( float c0, float m, float h, float e, float v, float incAngleRad )
        {
            var b = h * h * h * e / (12f * (1f - (v * v)));
            return (c0 * c0 / (Mathf.Sin(incAngleRad) * Mathf.Sin(incAngleRad))) * Mathf.Sqrt(m / b) / (2 * Mathf.PI);
        }

        public static TransferFunction CalcDavyComplete(TransferFunction f, float c0, float rho0, float etaInit,
            float m, float v,
            float shearModulus, float youngsModulus, float rho, float h, float length1, float length2, float fc,
            float incAngleRad = Single.PositiveInfinity,TransferFunction eta=null)
        {
            var omega = 2f * Mathf.PI * f;
            // var eta = EtaDavy(f, etaInit, m); // Davy (11)
            var abc = GetDavyAbc(omega, v, shearModulus, youngsModulus, rho, h); // 52
            var a = omega * m / (2f * rho0 * c0); // Davy (13)
            var sigmaForced = SigmaForced(length1, length2, fc, f, c0);
            var sigmaThetaC = SigmaC(length1, length2, fc, f, c0);
            // var sigmaThetaC = SigmaOblique(f, fc, c0, length1, length2, thetaC); //other davy paper

            var forcedBand = ForcedBand(f, fc);
            var alphaK = 29.5387f/3;
            if (eta == null)
                eta = TotalLossFactorSingle(f, m, etaInit, length1 * length2, fc,
                    sigmaThetaC, c0, rho0, alphaK);
            if (float.IsPositiveInfinity(incAngleRad))
            {
                var tauRes = TauRes(omega, sigmaThetaC, eta, m, rho0, c0, f, fc);
                var tauFor = (2f * sigmaForced) / (a * a)*forcedBand; // Davy (42)
                var tauDiff = tauRes * abc + tauFor;
                return tauDiff;
            }
            else
            {
                //var sigmaForcedAngle = 1f / Mathf.Cos(incAngleRad);
                var sigmaForcedAngleRindel = GetSigmaForcedAngleRindel(length1, length2, f, c0, incAngleRad);
                //var sigmaForcedAngle = GetSigmaForcedAngle(length1,length2,f,c0,incAngleRad);
                //var tauFor = ((2f * sigmaForced) / (a * a))*forcedBand; // Davy (42)
                var tauFor = ((2f * sigmaForced) / (a * a))*forcedBand; // Davy (42)

                var tauResAngle = TauResAngle(omega, sigmaThetaC, eta, m, rho0, c0, f, fc, incAngleRad);
                //var tauForAngle = forcedBand*((sigmaForcedAngle * sigmaForcedAngle) / (a * a)); // Davy (41)
                var tauForAngle = ((sigmaForcedAngleRindel * sigmaForcedAngleRindel) / (a * a)); // Davy (41)
                // var tauAngle = (tauResAngle * abc) + tauForAngle;
                var tauAngle = (tauResAngle * abc)+tauForAngle;
                var coIncFreq = CoincidentAngle(c0, m, h, youngsModulus, v, incAngleRad);
                // Debug.Log($"coIncFreq: {coIncFreq} Hz with angle {Mathf.Rad2Deg*incAngleRad}");
                return tauAngle;
            }
        }

        private static TransferFunction ForcedBand(TransferFunction f, float fc)
        {
            var forcedBand=new TransferFunction(f.NumFrequency);
            for (int i = 0; i < forcedBand.NumFrequency; i++)
            {
                forcedBand.Tf[i] = f.Tf[i] < fc ? 1f : 0f;
            }

            return forcedBand;
        }
        private static TransferFunction GetSigmaForcedAngle(float length, float width, TransferFunction f, float c0, float incAngleRad)
        {
            length /= 2;
            width /= 2;
            var sigma = new TransferFunction(f.NumFrequency);
            for (int i = 0; i < f.NumFrequency; i++)
            {
                var k = 2f * Mathf.PI * f.Tf[i] / c0;
                var ka = k * length;
                var thetaL = ka <= Mathf.PI / 2f ? 0f : Mathf.Acos(Mathf.Sqrt(Mathf.PI / (2f * ka)));
                var sigma0 = Mathf.PI / (2f * k * ka * width);
                var cosTheta = Mathf.Cos(incAngleRad);
                var cosThetaL = Mathf.Cos(thetaL);
                var sigma1 = 1 / (sigma0 + cosTheta);
                var sigma2 = 1 / (sigma0 + (3f*cosThetaL- cosTheta)/2);
                sigma.Tf[i] = Mathf.Abs(incAngleRad) <= thetaL ? sigma1 : sigma2;

            }

            return sigma;
        }
        
        private static TransferFunction GetSigmaForcedAngleRindel(float length, float width, TransferFunction f, float c0, float incAngleRad)
        {
            var sigma = new TransferFunction(f.NumFrequency);
            for (int i = 0; i < f.NumFrequency; i++)
            {
                var area = length * width;
                var perimeter = 2 * (length + width);
                var a = ((2 * area) / perimeter); // 6.35
                var ko = (2 * Mathf.PI * f.Tf[i]) / c0; // Wave Number
                var ka = ko * a;
                var sigma1 = Mathf.Pow((Mathf.Cos(incAngleRad) * Mathf.Cos(incAngleRad) - ((0.6f * Mathf.PI) / ka)), 2);
                var sigma2 = Mathf.Pow(Mathf.PI * (0.6f * Mathf.PI / ka), 2);
                var sigma3 = Mathf.Pow((Mathf.PI / (2f * (ka * ka))), 4);
                sigma.Tf[i] = Mathf.Pow(sigma1 + sigma2 + sigma3, -1f / 4f);
            }

            return sigma;
        }
        
        public static TransferFunction GetThetaC(TransferFunction f, float fc)
        {
            var thetaC = (fc / f).Sqrt().ASin(); // Davy (14)
            // for (var i = 0; i < thetaC.NumFrequency; i++)
            // {
            //     if (float.IsNaN(thetaC.Tf[i]))
            //         thetaC.Tf[i] = 0f;
            // }
            return thetaC;
        }

        public static TransferFunction EtaDavy(TransferFunction f, float etaInit, float m)
        {
            return etaInit + (m / (485f * f.Sqrt()));
        }

        public static TransferFunction SigmaOblique(TransferFunction f, float fc, float c0, float length1,
            float length2, float incAngleRad1)
        {
            var incAngleRad = new TransferFunction(f.NumFrequency) + incAngleRad1;
            var sigmaOblique = SigmaOblique(f, fc, c0, length1, length2, incAngleRad);
            return sigmaOblique;
        }

        public static TransferFunction SigmaOblique(TransferFunction f, float fc, float c0, float length1,
            float length2, TransferFunction incAngleRad)
        {
            var k0 = (2 * Mathf.PI * f) / c0;
            var a = length1 / 2;
            var d = length2 / 2;
            // SI_constants;
            // additional quantities for the calc
            var thetaL = (Mathf.PI / (2 * k0 * a)).Sqrt().ACos();
            for (var i = 0; i < thetaL.NumFrequency; i++)
            {
                if (k0.Tf[i] * a <= Mathf.PI / 2)
                    thetaL.Tf[i] = 0;
            }
            // abs(incAngle)<= theta_l


            //// Calculation of the radiation factor
            // Davy (11)
            var sigma1 = 1f / ((Mathf.PI / (2f * k0 * k0 * a * d)) + (incAngleRad).Cos());
            var sigma2 = 1f / ((Mathf.PI / (2f * k0 * k0 * a * d)) +
                               (((3f * thetaL.Cos()) - (incAngleRad).Cos()) / 2f));
            var sigmaOblique = new TransferFunction(f.NumFrequency);
            for (var i = 0; i < f.NumFrequency; i++)
            {
                if (Mathf.Abs(incAngleRad.Tf[i]) <= thetaL.Tf[i])
                    sigmaOblique.Tf[i] = sigma1.Tf[i];
                else
                {
                    sigmaOblique.Tf[i] = sigma2.Tf[i];
                }

                if (float.IsNaN(incAngleRad.Tf[i]))
                    sigmaOblique.Tf[i] = 0f;
            }

            return sigmaOblique;
        }


        public static TransferFunction TauRes(TransferFunction omega, TransferFunction sigma, TransferFunction eta,
            float m, float rho0, float c0, TransferFunction f, float fc)
        {
            var a = (omega * m) / (2f * rho0 * c0); // Davy (13)
            var r = f / fc;

            // Davy (24)
            return ((sigma * sigma) / (2f * a * r * (sigma + (a * eta)))) *
                   (((2 * a) / (sigma + a * eta)).ATan() - ((2f * a * (1f - r)) / (sigma + a * eta)).ATan());
        }

        public static TransferFunction TauRes2(TransferFunction omega, TransferFunction sigma, TransferFunction eta,
            float m, float rho0, float c0, TransferFunction f, float fc)
        {
            var result = new float[omega.NumFrequency];
            for (int i = 0; i < omega.NumFrequency; i++)
            {
                var a = (omega.Tf[i] * m) / (2f * rho0 * c0); // Davy (13)
                var r = f.Tf[i] / fc;

                // Davy (24)
                result[i] = ((sigma.Tf[i] * sigma.Tf[i]) / (2f * a * r * (sigma.Tf[i] + (a * eta.Tf[i])))) *
                            (Mathf.Atan((2 * a) / (sigma.Tf[i] + a * eta.Tf[i])) -
                             Mathf.Atan((2f * a * (1f - r)) / (sigma.Tf[i] + a * eta.Tf[i])));

            }

            return new TransferFunction(result);
        }

        public static TransferFunction TauResAngle(TransferFunction omega, TransferFunction sigma, TransferFunction eta,
            float m, float rho, float c0, TransferFunction f, float fc, float incAngleRad)
        {
            var a = omega * m / (2f * rho * c0); // Davy (13)
            var r = f / fc;

            var summand1 = (1f + (a * eta / sigma));
            var summand2 = (2f * a * r / sigma);
            var summand3 = ((1f / r) - (Mathf.Pow(Mathf.Sin(incAngleRad), 2)));
            return 1 / (summand1 * summand1 + (summand2 * summand2 * summand3 * summand3)); // Davy (15)
        }

        public static TransferFunction GetDavyAbc(TransferFunction omega, float v, float G, float E, float rho, float h)
        {
            var Chi = Mathf.Pow(((1 + v) / (0.87f + (1.12f * v))), 2); // Davy(48)
            var GStar = G / Chi; // Davy(47)

            // Solve squared equation
            var a = (E / (rho * (1 - (v * v)))) * (h * h / 12f); // Davy(53)
            var b = -1f * (1f + ((2f * Chi) / (1f - v))) * (h * h / 12f) * (omega * omega); // Davy(53)
            var c = -1f * (omega * omega); // Davy(53)
            // p = b/ a;
            // q = c/ a;
            var k_M_Sq = new TransferFunction(c.NumFrequency);
            for (int i = 0; i < c.NumFrequency; i++)
            {
                double aa = a;
                double bb = b.Tf[i];
                double cc = c.Tf[i];
                var k_M_Sq_1 = (-1f * bb + Math.Sqrt(bb * bb - (4 * aa * cc))) / (2 * aa); // MIM 53
                var k_M_Sq_2 = (-1f * bb - Math.Sqrt(bb * bb - (4 * aa * cc))) / (2 * aa); // MIM 53
                k_M_Sq.Tf[i] = (float) Math.Max(k_M_Sq_1, k_M_Sq_2);
            }

            // k_M_Sq_11 = -p/2 + sqrt((p/2)^2 - q); // p-q-Formel
            // k_M_Sq_21 = -p/2 - sqrt((p/2)^2 - q); // p-q-Formel


            var k_T_Sq = rho / GStar * (omega * omega); // Davy(45)
            var k_B_Sq = ((12 * rho * (1 - (v * v)) / (E * h * h)) * omega * omega).Sqrt(); // Davy(55)
            var k_L_Sq = (rho * (1 - (v * v)) / E) * (omega * omega); // Davy(56)
            var k_S_Sq = k_T_Sq + k_L_Sq; // Davy(46)


            var A = (1f + ((h * h / 12f) * ((k_M_Sq * k_T_Sq / k_L_Sq) - k_T_Sq))); // Davy(49)
            A *= A;
            var B = 1f - (k_T_Sq * h * h / 12f) + (k_M_Sq * k_S_Sq / (k_B_Sq * k_B_Sq)); // Davy(50)
            var C = (1f - ((k_T_Sq * h * h) / 12f) + (k_S_Sq * k_S_Sq / (4f * k_B_Sq * k_B_Sq))).Sqrt(); // Davy(51)

            return A / (B * C); // Davy(52)
        }

        public static TransferFunction SigmaForced(float length1, float length2, float fc, TransferFunction f, float c0)
        {
            var result=new TransferFunction(f.NumFrequency);
            for (var index = 0; index < result.NumFrequency; index++)
            {
                if (fc < f.Tf[index])
                {
                    result.Tf[index] = 0f;
                    continue;
                }
                const double w = 1.3d;
                const double beta = 0.124d;
                
                var s =1d* length1 * length2;
                var u = 2d * (length1 + length2);
                var a = ((2d * s) / u); // Davy (34)
                var ko = (2d * Math.PI * f.Tf[index]) / c0; // Wave Number
                var p = w * Math.Sqrt(Math.PI / (2d * ko * a)); // Davy (35)
                if (p > 1d)
                    p = 1d;
                // calc for h and alpha and q
                var h = 1f / (((2f / 3f) * Math.Sqrt((2f * ko * a) / Math.PI)) - beta); // Davy (36)

                var alpha = (h / p) - 1f; // Davy (37)

                var q = 2f * Math.PI / ((ko * ko) * s); // Davy (38)

                //// Calculation of the Radiation factor
                // low freq. no angle dependency!!!

                var qSq = q * q;
                var pSq = p * p;
                var hSq = h * h;
                var pq = Math.Sqrt(pSq + qSq);
                var hq = Math.Sqrt(hSq + qSq);
                var log1 = Math.Log((1f + Math.Sqrt(1f + qSq)) / (p + pq));
                var log2 = Math.Log((h + hq) / (p + pq));
                result.Tf[index] = (float) (log1 + ((1d / alpha) * log2));
            }
          
            return result;
        }

        public static TransferFunction SigmaC(float length1, float length2, float fc, TransferFunction f, float c0)
        {
            var result=new TransferFunction(f.NumFrequency);
            for (var index = 0; index < result.NumFrequency; index++)
            {
                const double n = 2d;
                const double w = 1.3d;
                const double beta = 0.124d;

                var g = Math.Sqrt(1d - (fc / f.Tf[index]));
                if (f.Tf[index] < fc)
                    g = 0d;

                var s = 1d * length1 * length2;
                var u = 2d * (length1 + length2);
                var a = ((2d * s) / u); // Davy (34)
                var ko = (2d * Math.PI * f.Tf[index]) / c0; // Wave Number
                var p = w * Math.Sqrt(Math.PI / (2d * ko * a)); // Davy (35)
                if (p > 1d)
                    p = 1d;
                // calc for h and alpha and q
                var h = 1d / (((2d / 3d) * Math.Sqrt((2d * ko * a) / Math.PI)) - beta); // Davy (36)

                var alpha = (h / p) - 1d; // Davy (37)

                var q = 2f * Math.PI / ((ko * ko) * s); // Davy (38)

                //Calculation of the radiation factor
                // 39
                var sigma1 = 1d / Math.Sqrt(Math.Pow(g, n) + Math.Pow(q, n));
                var sigma2 = 1d / Math.Sqrt(Math.Pow((h - (alpha * g)), n) + Math.Pow(g, n));
                var sig = 0f;
                if (1d >= g && g >= p)
                    sig = (float) sigma1;
                if (p > g && g >= 0d)
                    sig = (float) sigma2;

                result.Tf[index] = sig;
            }

            return result;
        }
    }
}