using log4net;
using UnityEngine;

namespace BA.BACore
{
    /// <summary>
    /// Calculate the Sound Reduction Index based on ISO 12354-1
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    public class ReductionIndexCalculator
    {
        private  readonly ILog Log = LogManager.GetLogger(typeof(ReductionIndexCalculator));
       
        /// <summary>
        /// Calculate Reduction Index
        /// </summary>
        /// <returns>TransferFunction[4]</returns>
        public  TransferFunction CalcAndGetReductionIndexSitu(SoundReductionIndexIso wall)
        {
            if (DataLogger.DebugData)
                WriteInitialData(wall);
            var tauSitu = GetTauSitu(wall);
            var reductionIndexSitu = -10 * tauSitu.Log(10);

            DataLogger.Log(wall.LogName + "tauSitu", tauSitu);
            DataLogger.Log(wall.LogName + "reductionIndexSitu", reductionIndexSitu);
            return reductionIndexSitu;
        }

        private  void WriteInitialData(SoundReductionIndexIso wall)
        {
            DataLogger.Log(wall.LogName + "Thickness", wall.Geometry.Thickness);
            DataLogger.Log(wall.LogName + "Name", wall.LogName);
            DataLogger.Log(wall.LogName + "Length", wall.Geometry.Length);
            DataLogger.Log(wall.LogName + "Width", wall.Geometry.Width);
            DataLogger.Log(wall.LogName + "MaterialStructureData.Type", wall.Material.Type);
            DataLogger.Log(wall.LogName + "MaterialStructureData.density", wall.Material.Density);
            DataLogger.Log(wall.LogName + "MaterialStructureData.internalLossFactor",
                wall.Material.InternalLossFactor);
            DataLogger.Log(wall.LogName + "MaterialStructureData.quasiLongPhaseVelocity",
                wall.Material.QuasiLongPhaseVelocity);
            DataLogger.Log(wall.LogName + "criticalFrequency", wall.CriticalFrequency);
            DataLogger.Log(wall.LogName + "Dimensions", wall.Geometry.Dimensions);
            DataLogger.Log(wall.LogName + "Normal", wall.Geometry.Normal);
            // DataLogger.Log(wall.LogName + "JunctionLengths", wall.AttachedJunctions.JunctionLengths);
        }

        /// <summary>
        /// Calculate R_w ReductionIndex Weighted
        /// </summary>
        /// <returns></returns>
        public float ReductionWeighted(SoundReductionIndexIso wall)
        {
            var massRatio = Mathf.Log10(wall.WallBehaviour.Mass / BaSettings.Instance.ReferenceMass);
            var reductionWeighted = 0f;
            if (wall.Material.isHeavyWall && wall.WallBehaviour.Mass >= 65 && wall.WallBehaviour.Mass <= 720)
                reductionWeighted = 30.9f * massRatio - 22.2f;

            reductionWeighted = 37.5f * massRatio - 42;
            DataLogger.Log(wall.LogName + "ReductionIndexWeighted", reductionWeighted);
            return reductionWeighted;
        }

        /// <summary>
        /// Based on In-Situ Total Loss factor including Kij
        /// This Condition is only for Calculations of EN: 12354-1 Building Situation
        /// (Problem to Select the Higher Frequency Limit)
        /// </summary>
        /// <returns>TauSitu</returns>
        private  TransferFunction GetTauSitu(SoundReductionIndexIso wall)
        {
            var sigmaForced = GetSigmaForced(wall);
            var sigmaFw = GetSigmaFw(wall);
            var etaTotalSitu = GetEtaTotalSitu(wall);
            var tauSitu = new TransferFunction(21);
            var tauPlateau =
                Mathf.Pow(
                    4 * BaSettings.Instance.Rho0 * BaSettings.Instance.C0 /
                    (1.1f * wall.Material.QuasiLongPhaseVelocity * wall.Material.Density), 2) *
                (0.02f / etaTotalSitu);
            var temp = (2 * BaSettings.Instance.Rho0 * BaSettings.Instance.C0 /
                        (2 * Mathf.PI * BaSettings.Instance.ThirdOctaveFrequencyBandShort * wall.Mass)) ^ 2;

            for (var i = 0; i < BaSettings.Instance.ThirdOctaveFrequencyBandShort.NumFrequency; i++)
                if (BaSettings.Instance.ThirdOctaveFrequencyBandShort.Tf[i] < wall.WallBehaviour.CriticalFrequency / 1.12f)
                {
                    tauSitu.Tf[i] = temp.Tf[i] *
                                    (2 * sigmaForced.Tf[i] *
                                     Mathf.Pow(
                                         (1 - Mathf.Pow(BaSettings.Instance.ThirdOctaveFrequencyBandShort.Tf[i], 2)) /
                                         Mathf.Pow(wall.WallBehaviour.CriticalFrequency, 2), -2)
                                     + 2 * Mathf.PI * wall.WallBehaviour.CriticalFrequency * Mathf.Pow(sigmaFw.Tf[i], 2) /
                                     (4 * BaSettings.Instance.ThirdOctaveFrequencyBandShort.Tf[i] * etaTotalSitu.Tf[i]));
                }
                else if (BaSettings.Instance.ThirdOctaveFrequencyBandShort.Tf[i] >= wall.WallBehaviour.CriticalFrequency / 1.12f &&
                         BaSettings.Instance.ThirdOctaveFrequencyBandShort.Tf[i] <= wall.WallBehaviour.CriticalFrequency * 1.4)
                {
                    tauSitu.Tf[i] = temp.Tf[i] * (Mathf.PI * Mathf.Pow(sigmaFw.Tf[i], 2) / (2 * etaTotalSitu.Tf[i]));
                }
                else if (BaSettings.Instance.ThirdOctaveFrequencyBandShort.Tf[i] > wall.WallBehaviour.CriticalFrequency * 1.4)
                {
                    tauSitu.Tf[i] = temp.Tf[i] * (Mathf.PI * wall.WallBehaviour.CriticalFrequency * Mathf.Pow(sigmaFw.Tf[i], 2) /
                                                  (2 * BaSettings.Instance.ThirdOctaveFrequencyBandShort.Tf[i] * etaTotalSitu.Tf[i]));
                    if (tauSitu.Tf[i] < tauPlateau.Tf[i])
                        tauSitu.Tf[i] = tauPlateau.Tf[i];
                }


            DataLogger.Log(wall.LogName + "TauSitu", tauSitu);

            return tauSitu;
        }

        private static TransferFunction GetTauSitu1(TransferFunction etaTotalSitu, SoundReductionIndexIso wall)
        {
            var l1 = wall.Geometry.Width;
            var l2 = wall.Geometry.Length;
            var cL = wall.Material.QuasiLongPhaseVelocity;
            var criticalFrequency = wall.WallBehaviour.CriticalFrequency;
            var rho0 = BaSettings.Instance.Rho0;
            var c0 = BaSettings.Instance.C0;
            var frequencyBandShort = BaSettings.Instance.ThirdOctaveFrequencyBandShort;
            var wallMass = wall.Mass;
            var materialDensity = wall.Material.Density;

            var tauSitu = GetTauSitu(etaTotalSitu, l2, l1, criticalFrequency, frequencyBandShort, rho0, c0, cL, materialDensity, wallMass);


            DataLogger.Log(wall.LogName + "TauSitu", tauSitu);

            return tauSitu;
        }

        public static TransferFunction GetTauSitu(TransferFunction etaTotalSitu, float l2, float l1, float criticalFrequency,
            TransferFunction frequencyBandShort, float rho0, float c0, float cL, float materialDensity, float wallMass)
        {
            var sigmaForced = GetSigmaForced(frequencyBandShort, l2, l1, c0);

            var sigmaFw = GetSigmaFw(criticalFrequency, frequencyBandShort, l1, l2);

            var tauSitu = new TransferFunction(frequencyBandShort.NumFrequency);
            var tauPlateau =
                Mathf.Pow(
                    4 * rho0 * c0 /
                    (1.1f * cL * materialDensity), 2) *
                (0.02f / etaTotalSitu);
            var temp = (2 * rho0 * c0 /
                        (2 * Mathf.PI * frequencyBandShort * wallMass)) ^ 2;

            for (var i = 0; i < frequencyBandShort.NumFrequency; i++)
            {
                if (frequencyBandShort.Tf[i] < criticalFrequency / 1.12f)
                {
                    tauSitu.Tf[i] = temp.Tf[i] *
                                    (2 * sigmaForced.Tf[i] *
                                     Mathf.Pow(
                                         (1 - Mathf.Pow(frequencyBandShort.Tf[i], 2)) /
                                         Mathf.Pow(criticalFrequency, 2), -2)
                                     + 2 * Mathf.PI * criticalFrequency * Mathf.Pow(sigmaFw.Tf[i], 2) /
                                     (4 * frequencyBandShort.Tf[i] * etaTotalSitu.Tf[i]));
                }
                else if (frequencyBandShort.Tf[i] >= criticalFrequency / 1.12f &&
                         frequencyBandShort.Tf[i] <= criticalFrequency * 1.4)
                {
                    tauSitu.Tf[i] = temp.Tf[i] * (Mathf.PI * Mathf.Pow(sigmaFw.Tf[i], 2) / (2 * etaTotalSitu.Tf[i]));
                }
                else if (frequencyBandShort.Tf[i] > criticalFrequency * 1.4)
                {
                    tauSitu.Tf[i] = temp.Tf[i] * (Mathf.PI * criticalFrequency * Mathf.Pow(sigmaFw.Tf[i], 2) /
                                                  (2 * frequencyBandShort.Tf[i] * etaTotalSitu.Tf[i]));
                    if (tauSitu.Tf[i] < tauPlateau.Tf[i])
                        tauSitu.Tf[i] = tauPlateau.Tf[i];
                }
            }

            return tauSitu;
        }

        /// <summary>
        /// Get Eta Total Situ
        /// </summary>
        /// <returns>TransferFunction</returns>
        public  TransferFunction GetEtaTotalSitu(SoundReductionIndexIso wall)
        {
            if (DataLogger.DebugData)
                WriteInitialData(wall);
            var etaInit = wall.Material.InternalLossFactor;
            var wallBehaviourMass = wall.WallBehaviour.Mass;
            if (wallBehaviourMass < 150)
                etaInit = 0.005f;

            var rho0 = BaSettings.Instance.Rho0;
            var c0 = BaSettings.Instance.C0;
            var frequencyBandShort = BaSettings.Instance.ThirdOctaveFrequencyBandShort;
            var geometryArea = wall.Geometry.Area;
            var criticalFrequency = wall.WallBehaviour.CriticalFrequency;
            var l1 = wall.Geometry.Width;
            var l2 = wall.Geometry.Length;
            var sigmaFw = GetSigmaFw(criticalFrequency, frequencyBandShort, l1, l2);
            var junctionsLengths = GetJunctionsLengths(wall);
            var absorptionCoefficientSitu = GetAbsorptionCoefficientSitu(wall);
            var etaTotalSitu = GetEtaTotalSitu(etaInit, rho0, c0, sigmaFw, frequencyBandShort, wallBehaviourMass, geometryArea, criticalFrequency, junctionsLengths, absorptionCoefficientSitu);
            // Log.Info("Factor " + Factor);
            DataLogger.Log(wall.LogName + "EtaTotalSitu", etaTotalSitu);
            return etaTotalSitu;
        }

        private static TransferFunction GetEtaTotalSitu(float etaInit, float rho0, float c0, TransferFunction sigmaFw,
            TransferFunction frequencyBandShort, float wallBehaviourMass, float geometryArea, float criticalFrequency,
            TransferFunction junctionsLengths, TransferFunction absorptionCoefficientSitu)
        {
            var etaTotalSitu = etaInit
                               + 2.0f * rho0 * c0 * sigmaFw
                               / (2.0f * Mathf.PI * frequencyBandShort * wallBehaviourMass)
                               + c0 / (Mathf.Pow(Mathf.PI, 2) * geometryArea *
                                       (frequencyBandShort * criticalFrequency)
                                       .Sqrt()) *
                               (junctionsLengths * absorptionCoefficientSitu).Sum();
            return etaTotalSitu;
        }

        /// <summary>
        /// calculate Sigma Forced
        /// </summary>
        /// <returns></returns>
        private  TransferFunction GetSigmaForced(SoundReductionIndexIso wall)
        {
            var l1 = wall.Geometry.Width;
            var l2 = wall.Geometry.Length;
            var sigmaForced = GetSigmaForced(BaSettings.Instance.ThirdOctaveFrequencyBandShort,l2, l1,BaSettings.Instance.C0);

            DataLogger.Log(wall.LogName + "SigmaForced", sigmaForced);

            return sigmaForced;
        }

        private static TransferFunction GetSigmaForced(TransferFunction f, float l2, float l1, float c0)
        {
            var k0 = 2.0f * (float) Mathf.PI / c0 * f;
            var delta = -0.964f - (0.5f + l2 / (Mathf.PI * l1)) * Mathf.Log(l2 / l1, Mathf.Exp(1)) +
                5 * l2 / (2 * Mathf.PI * l1) - 1 / ((4 * Mathf.PI * l1 * l2 * k0) ^ 2);
            var sigmaForced = 0.5f * ((Mathf.Sqrt(l1 * l2) * k0).Log(Mathf.Exp(1)) - delta);
            sigmaForced.SetMaxValue(2.0f);
            return sigmaForced;
        }

        /// <summary>
        /// Calculate Radiation Efficiency Index
        /// </summary>
        /// <returns></returns>
        private  TransferFunction GetRadiationEfficiency(SoundReductionIndexIso wall)
        {
            var radiationEfficiency = 10 * GetSigmaForced(wall).Log(10);

            DataLogger.Log(wall.LogName + "RadiationEfficiency", radiationEfficiency);

            return radiationEfficiency;
        }

        /// <summary>
        /// Calculate SigmaFW
        /// </summary>
        /// <returns></returns>
        private  TransferFunction GetSigmaFw(SoundReductionIndexIso wall)
        {
            var l1 = wall.Geometry.Width;
            var l2 = wall.Geometry.Length;
                var criticalFrequency = wall.WallBehaviour.CriticalFrequency;
                var frequencyBandShort = BaSettings.Instance.ThirdOctaveFrequencyBandShort;

                var sigmaFw = GetSigmaFw(criticalFrequency, frequencyBandShort, l1, l2);

                DataLogger.Log(wall.LogName + "SigmaFW", sigmaFw);

            return sigmaFw;
        }

        private static TransferFunction GetSigmaFw(float criticalFrequency, TransferFunction frequencyBandShort, float l1,
            float l2)
        {
            var k0NumFrequency = frequencyBandShort.NumFrequency;
            var sigmaFw = new TransferFunction(k0NumFrequency);
            for (var i = 0; i < k0NumFrequency; i++)
            {
                // Radiation factor for Free bending waves

                var sigma1 = 1 / Mathf.Sqrt(1 - criticalFrequency / frequencyBandShort.Tf[i]);
                var c0 = BaSettings.Instance.C0;
                var sigma2 = 4 * l1 * l2 * Mathf.Pow(frequencyBandShort.Tf[i] / c0, 2);
                var sigma3 = Mathf.Sqrt(2 * Mathf.PI * frequencyBandShort.Tf[i] * (l1 + l2) /
                                        (16 * c0));
                var f11 = Mathf.Pow(c0, 2) / (4 * criticalFrequency) * (1 / (l1 * l1) + 1 / (l2 * l2));
                if (f11 <= criticalFrequency / 2)
                {
                    if (frequencyBandShort.Tf[i] >= criticalFrequency)
                    {
                        sigmaFw.Tf[i] = sigma1;
                    }
                    else if (frequencyBandShort.Tf[i] < criticalFrequency)
                    {
                        float delta2;
                        var lamda = Mathf.Sqrt(frequencyBandShort.Tf[i] / criticalFrequency);
                        var delta1 = ((1 - lamda * lamda) * Mathf.Log((1 + lamda) / (1 - lamda), BaSettings.Instance.Euler) +
                                      2 * lamda) /
                                     (4 * Mathf.Pow(Mathf.PI, 2) * Mathf.Pow(1 - lamda * lamda, 1.5f));
                        if (frequencyBandShort.Tf[i] > criticalFrequency / 2)
                            delta2 = 0;
                        else
                            delta2 = 8 * Mathf.Pow(c0, 2) * (1 - 2 * Mathf.Pow(lamda, 2)) /
                                     (Mathf.Pow(criticalFrequency, 2) * Mathf.Pow(Mathf.PI, 4) * l1 * l2 * lamda *
                                      Mathf.Sqrt(1 - Mathf.Pow(lamda, 2)));
                        sigmaFw.Tf[i] = 2 * (l1 + l2) / (l1 * l2) * (c0 / criticalFrequency) * delta1 +
                                        delta2;
                    }

                    if (f11 > frequencyBandShort.Tf[i] && f11 < criticalFrequency / 2 &&
                        sigmaFw.Tf[i] > sigma2)
                        sigmaFw.Tf[i] = sigma2;
                }
                else
                {
                    if (frequencyBandShort.Tf[i] < criticalFrequency && sigma2 < sigma3)
                        sigmaFw.Tf[i] = sigma2;
                    else if (frequencyBandShort.Tf[i] > criticalFrequency && sigma1 < sigma3)
                        sigmaFw.Tf[i] = sigma1;
                    else
                        sigmaFw.Tf[i] = sigma3;
                }
            }

            sigmaFw.SetMaxValue(2);
            return sigmaFw;
        }

        /// <summary>
        /// This Function Calculates the RadiationFactorResonant for airborne in accordance with ISO 717-1
        /// </summary>
        /// <returns>SigmaS = Structural excitation</returns>
        private  TransferFunction GetSigmaS(SoundReductionIndexIso wall)
        {
            var sigmaS = GetSigmaFw(wall);
            DataLogger.Log(wall.LogName + "SigmaS", sigmaS);

            return sigmaS;
        }


        /// <summary>
        /// This Function Calculates the RadiationFactorResonant for airborne in accordance with ISO 717-1
        /// </summary>
        /// <returns>SigmaA = Radiation factor for airborne excitation</returns>
        private  TransferFunction GetSigmaA(SoundReductionIndexIso wall)
        {
            var sigmaFw = GetSigmaFw(wall);

            var tempr = Mathf.PI * wall.WallBehaviour.CriticalFrequency * sigmaFw /
                        (4 * BaSettings.Instance.ThirdOctaveFrequencyBandShort * GetEtaTotalSitu(wall));
            var sigmaA = (GetSigmaForced(wall) + tempr * sigmaFw) / (1 + tempr);
            DataLogger.Log(wall.LogName + "SigmaA", sigmaA);

            return sigmaA;
        }


        /// <summary>
        /// Based on Loss factor from Lab Situation
        /// </summary>
        /// <returns></returns>
        private  TransferFunction GetTauLab(SoundReductionIndexIso wall)
        {
            var sigmaForced = GetSigmaForced(wall);
            var sigmaFw = GetSigmaFw(wall);
            var etaTotalLab1 = GetEtaTotalLab(wall, 1);
            var tauLab = new TransferFunction(BaSettings.Instance.ThirdOctaveFrequencyBandShort.NumFrequency);

            var tauPlateau =
                Mathf.Pow(
                    4 * BaSettings.Instance.Rho0 * BaSettings.Instance.C0 /
                    (1.1f * wall.Material.QuasiLongPhaseVelocity * BaSettings.Instance.Rho0),
                    2) * (0.02f / etaTotalLab1);
            for (var i = 0; i < BaSettings.Instance.ThirdOctaveFrequencyBandShort.NumFrequency; i++)
                if (BaSettings.Instance.ThirdOctaveFrequencyBandShort.Tf[i] < wall.WallBehaviour.CriticalFrequency / 1.12)
                {
                    tauLab.Tf[i] =
                        Mathf.Pow(
                            2 * BaSettings.Instance.Rho0 * BaSettings.Instance.C0 /
                            (2 * Mathf.PI * BaSettings.Instance.ThirdOctaveFrequencyBandShort.Tf[i] * wall.WallBehaviour.Mass),
                            2)
                        * (2 * sigmaForced.Tf[i] *
                            Mathf.Pow(
                                Mathf.Pow(1 - BaSettings.Instance.ThirdOctaveFrequencyBandShort.Tf[i], 2) /
                                Mathf.Pow(wall.WallBehaviour.CriticalFrequency, 2),
                                -2) + 2 * Mathf.PI * wall.WallBehaviour.CriticalFrequency * Mathf.Pow(sigmaFw.Tf[i], 2) /
                            (4 * BaSettings.Instance.ThirdOctaveFrequencyBandShort.Tf[i] * etaTotalLab1.Tf[i]));
                }
                else if (BaSettings.Instance.ThirdOctaveFrequencyBandShort.Tf[i] >= wall.WallBehaviour.CriticalFrequency / 1.12 &&
                         BaSettings.Instance.ThirdOctaveFrequencyBandShort.Tf[i] <= wall.WallBehaviour.CriticalFrequency * 1.4)
                {
                    tauLab.Tf[i] =
                        Mathf.Pow(2 * BaSettings.Instance.Rho0 * BaSettings.Instance.C0 / (2 * Mathf.PI * wall.WallBehaviour.CriticalFrequency * wall.WallBehaviour.Mass),
                            2) *
                        (Mathf.PI * Mathf.Pow(sigmaFw.Tf[i], 2) / (2 * etaTotalLab1.Tf[i]));
                }
                else if (BaSettings.Instance.ThirdOctaveFrequencyBandShort.Tf[i] > wall.WallBehaviour.CriticalFrequency * 1.4 &&
                         BaSettings.Instance.ThirdOctaveFrequencyBandShort.Tf[i] < 3000)
                {
                    tauLab.Tf[i] =
                        Mathf.Pow(
                            2 * BaSettings.Instance.Rho0 * BaSettings.Instance.C0 /
                            (2 * Mathf.PI * BaSettings.Instance.ThirdOctaveFrequencyBandShort.Tf[i] * wall.WallBehaviour.Mass),
                            2)
                        * (Mathf.PI * wall.WallBehaviour.CriticalFrequency * Mathf.Pow(sigmaFw.Tf[i], 2) /
                           (2 * BaSettings.Instance.ThirdOctaveFrequencyBandShort.Tf[i] * etaTotalLab1.Tf[i]));
                    if (tauLab.Tf[i] < tauPlateau.Tf[i])
                        tauLab.Tf[i] = tauPlateau.Tf[i];
                }

            DataLogger.Log(wall.LogName + "TauLab", tauLab);

            return tauLab;
        }


        /// <summary>
        /// Based on In-Situ Total Loss factor General
        /// </summary>
        /// <returns>float[21] TauSituGen</returns>
        private  TransferFunction GetTauSituGen(SoundReductionIndexIso wall)
        {
            var sigmaForced = GetSigmaForced(wall);
            var sigmaFw = GetSigmaFw(wall);
            var etaTotalSituGeneral = GetEtaTotalSituGeneral(wall);
            var tauPlateau =
                Mathf.Pow(
                    4 * BaSettings.Instance.Rho0 * BaSettings.Instance.C0 /
                    (1.1f * wall.Material.QuasiLongPhaseVelocity * wall.Material.Density),
                    2) * (0.02f / etaTotalSituGeneral);
            var temp = (2 * BaSettings.Instance.Rho0 * BaSettings.Instance.C0 /
                        (2 * Mathf.PI * BaSettings.Instance.ThirdOctaveFrequencyBandShort * wall.Mass)) ^ 2;
            var tauSituGen = new TransferFunction(21);

            for (var i = 0; i < BaSettings.Instance.ThirdOctaveFrequencyBandShort.NumFrequency; i++)
                if (BaSettings.Instance.ThirdOctaveFrequencyBandShort.Tf[i] < wall.WallBehaviour.CriticalFrequency / 1.12)
                {
                    tauSituGen.Tf[i] = temp.Tf[i] *
                                       (2 * sigmaForced.Tf[i] *
                                           Mathf.Pow(
                                               (1 - Mathf.Pow(BaSettings.Instance.ThirdOctaveFrequencyBandShort.Tf[i], 2)) /
                                               Mathf.Pow(wall.WallBehaviour.CriticalFrequency, 2), -2) + 2 * Mathf.PI *
                                           wall.WallBehaviour.CriticalFrequency *
                                           Mathf.Pow(sigmaFw.Tf[i], 2) /
                                           (4 * BaSettings.Instance.ThirdOctaveFrequencyBandShort.Tf[i] * etaTotalSituGeneral.Tf[i]));
                }
                else if (BaSettings.Instance.ThirdOctaveFrequencyBandShort.Tf[i] >= wall.WallBehaviour.CriticalFrequency / 1.12 &&
                         BaSettings.Instance.ThirdOctaveFrequencyBandShort.Tf[i] <= wall.WallBehaviour.CriticalFrequency * 1.4)
                {
                    tauSituGen.Tf[i] =
                        temp.Tf[i] * (Mathf.PI * Mathf.Pow(sigmaFw.Tf[i], 2) / (2 * etaTotalSituGeneral.Tf[i]));
                }
                else if (BaSettings.Instance.ThirdOctaveFrequencyBandShort.Tf[i] > wall.WallBehaviour.CriticalFrequency * 1.4)
                {
                    tauSituGen.Tf[i] = temp.Tf[i] * (Mathf.PI * wall.WallBehaviour.CriticalFrequency * Mathf.Pow(sigmaFw.Tf[i], 2) /
                                                     (2 * BaSettings.Instance.ThirdOctaveFrequencyBandShort.Tf[i] *
                                                      etaTotalSituGeneral.Tf[i]));
                    if (tauSituGen.Tf[i] < tauPlateau.Tf[i])
                        tauSituGen.Tf[i] = tauPlateau.Tf[i];
                }

            DataLogger.Log(wall.LogName + "TauSituGen", tauSituGen);

            return tauSituGen;
        }


        /// <summary>
        /// >Calculate Correction Factor for Reduction Weighted
        /// </summary>
        /// <returns>TransferFunction.tf[2]</returns>
        private  TransferFunction ReductionWeightedCorrection(SoundReductionIndexIso wall)
        {
            var massRatio = Mathf.Log10(wall.WallBehaviour.Mass / BaSettings.Instance.ReferenceMass);
            var weightedCorrection = new TransferFunction(2);
            if (wall.Material.isHeavyWall && wall.WallBehaviour.Mass >= 65 && wall.WallBehaviour.Mass <= 720)
            {
                weightedCorrection.Tf[0] = -1.6f;
                weightedCorrection.Tf[1] = -4.6f;
                return weightedCorrection;
            }

            weightedCorrection.Tf[0] = -1;
            weightedCorrection.Tf[1] = 16 - 9 * massRatio;

            weightedCorrection.Tf[1] = weightedCorrection.Tf[1] > -1 ? -1 : weightedCorrection.Tf[1];
            weightedCorrection.Tf[1] = weightedCorrection.Tf[1] < -7 ? -7 : weightedCorrection.Tf[1];

            DataLogger.Log(wall.LogName + "weightedCorrection", weightedCorrection);

            return weightedCorrection;
        }

        /// <summary>
        /// Case-I: Average Absorption Coefficient (for heavy constructions (around 400 kg/m2))
        /// </summary>
        /// <returns>single float</returns>
        private  float GetAverageAbsorptionCoefficient(SoundReductionIndexIso wall)
        {
            DataLogger.Log(wall.LogName + "AverageAbsorptionCoefficient", 0.15f);

            return 0.15f;
        }

        /// <summary>
        /// Case-II: Average Absorption Coefficient for Laboratory(for a heavy frame of 600 mm concrete around the test
        /// opening)
        /// </summary>
        /// <returns>single float</returns>
        private  float GetAverageAbsorptionCoefficientForLaboratory(SoundReductionIndexIso wall)
        {
            //Case - II: Average Absorption Coefficient for Laboratory(for a heavy frame of 600 mm concrete around the test opening)
            var chi = Mathf.Sqrt(31.1f / wall.WallBehaviour.CriticalFrequency);
            var psy = 44.3f * (wall.WallBehaviour.CriticalFrequency / wall.WallBehaviour.Mass);
            var alpha = 1.0f / 3 * (2 * Mathf.Sqrt(chi * psy) * (1 + chi) * (1 + psy) /
                                    Mathf.Pow(chi * Mathf.Pow(1 + psy, 2) + 2 * psy * (1 + Mathf.Pow(chi, 2)), 2));

            DataLogger.Log(wall.LogName + "AverageAbsorptionCoefficientForLaboratory", alpha * (1 - 0.9999f * alpha));

            return alpha * (1 - 0.9999f * alpha);
        }

        /// <summary>
        /// Calculate AbsorptionCoefficientSitu (alphaKSitu)
        /// </summary>
        /// <returns></returns>
        private  TransferFunction GetAbsorptionCoefficientSitu(SoundReductionIndexIso wall)
        {
            var alphaKSitu = new TransferFunction(4);
            for (var i = 0; i < 4; i++)
                if (wall.Junctions.AttachedWalls[i] == null)
                    Log.Warn("_attachedJunctions[" + i + "] (" + wall.LogName + ") not found");
                else
                    alphaKSitu.Tf[i] = wall.Junctions.AttachedWalls[i].GetAlphaKSitu(wall.WallBehaviour.GetInstanceID());

            DataLogger.Log(wall.LogName + "AlphaKSitu", alphaKSitu);
            return alphaKSitu;
        }

        /// <summary>
        /// Case - III: Absorption Coefficient for in-Situ(between 0.05 and 0.5)
        /// </summary>
        /// <returns></returns>
        private  TransferFunction GetAbsorptionCoefficientLab(SoundReductionIndexIso wall)
        {
            var alphaKLab = new TransferFunction(4);

            for (var i = 0; i < 4; i++)
            {
                if (wall.Junctions.AttachedWalls[i] == null)
                    Log.Warn("_attachedJunctions[" + i + "] (" + wall.LogName + ") not found!");
                else
                    alphaKLab.Tf[i] = wall.Junctions.AttachedWalls[i].GetAlphaKLab(wall.WallBehaviour.GetInstanceID());

                if (alphaKLab.Tf[i] < 1e-12f)
                    Log.Warn("AlphaKLab[" + i + "] cannot be zero!");
            }

            DataLogger.Log(wall.LogName + "AlphaKLab", alphaKLab);

            return alphaKLab;
        }

        /// <summary>
        /// Calculate EtaTotalSituGeneral
        /// </summary>
        /// <returns></returns>
        private  TransferFunction GetEtaTotalSituGeneral(SoundReductionIndexIso wall)
        {
            var etaInit = wall.Material.InternalLossFactor;
            if (wall.WallBehaviour.Mass < 150)
                etaInit = 0.005f;

            var etaTotalSituGeneral = etaInit
                                      + BaSettings.Instance.C0 / BaSettings.Instance.ThirdOctaveFrequencyBandShort.Sqrt();

            DataLogger.Log(wall.LogName + "EtaTotalSituGeneral", etaTotalSituGeneral);

            return etaTotalSituGeneral;
        }

        /// <summary>
        /// Calculate EtaTotalLab1 or EtaTotalLab2
        /// </summary>
        /// <param name="wall"> input Wall </param>
        /// <param name="index">
        /// for EtaTotalLab1 use internalLossFactor Index = 1
        /// for EtaTotalLab2 use 0.01 Index = 2
        /// </param>
        /// <returns></returns>
        private  TransferFunction GetEtaTotalLab(SoundReductionIndexIso wall, int index)
        {
            TransferFunction etaTotalLab;
            float etaInitial;
            switch (index)
            {
                case 1:
                    etaInitial = wall.Material.InternalLossFactor;
                    break;
                case 2:
                    etaInitial = 0.01f;
                    break;
                default:
                    Log.Error("Wrong Index for GetEtaTotalLab");
                    etaInitial = 0.0f;
                    break;
            }

            if (wall.WallBehaviour.Mass < 150)
                etaInitial = 0.005f;

            if (wall.WallBehaviour.Mass >= 400 && wall.WallBehaviour.Mass <= 800)
                etaTotalLab = etaInitial + wall.WallBehaviour.Mass / (485 * BaSettings.Instance.ThirdOctaveFrequencyBandShort.Sqrt());
            else
                etaTotalLab = etaInitial + wall.WallBehaviour.Mass / (485 * BaSettings.Instance.ThirdOctaveFrequencyBandShort.Sqrt());

            DataLogger.Log(wall.LogName + "EtaTotalLab" + index, etaTotalLab);

            return etaTotalLab;
        }

        /// <summary>
        /// Calculate EtaTotalLabK
        /// </summary>
        /// <returns>Transferfunction (21)</returns>
        private  TransferFunction GetEtaTotalLabK(SoundReductionIndexIso wall)
        {
            /// TODO Check this function
            var etaInit = wall.Material.InternalLossFactor;
            if (wall.WallBehaviour.Mass < 150)
                etaInit = 0.005f;

            var etaTotalLabK =
                etaInit +
                2 * BaSettings.Instance.Rho0 * BaSettings.Instance.C0 * GetSigmaFw(wall) /
                (2 * Mathf.PI * BaSettings.Instance.ThirdOctaveFrequencyBandShort * wall.WallBehaviour.Mass) +
                BaSettings.Instance.C0 / (Mathf.Pow(Mathf.PI, 2) * wall.Geometry.Area *
                                (BaSettings.Instance.ThirdOctaveFrequencyBandShort * wall.WallBehaviour.CriticalFrequency).Sqrt()) *
                (GetJunctionsLengths(wall) * GetAbsorptionCoefficientLab(wall)).Sum();

            DataLogger.Log(wall.LogName + "EtaTotalLabK", etaTotalLabK);

            return etaTotalLabK;
        }

        /// <summary>
        /// calculate TauS LabK
        /// </summary>
        /// <returns></returns>
        private  TransferFunction GetTsLabK(SoundReductionIndexIso wall)
        {
            var tsLabK = 2.2f / (BaSettings.Instance.ThirdOctaveFrequencyBandShort * GetEtaTotalLabK(wall));

            DataLogger.Log(wall.LogName + "TsLabK", tsLabK);

            return tsLabK;
        }

        /// <summary>
        /// Calculate Ts in Situ
        /// </summary>
        /// <returns></returns>
        public  TransferFunction GetTsSitu(SoundReductionIndexIso wall)
        {
            if (DataLogger.DebugData)
                WriteInitialData(wall);

            var tsSitu = 2.2f / (BaSettings.Instance.ThirdOctaveFrequencyBandShort * GetEtaTotalSitu(wall));

            DataLogger.Log(wall.LogName + "TsSitu", tsSitu);

            return tsSitu;
        }

        /// <summary>
        /// returns all Junction Lengths
        /// </summary>
        /// <returns></returns>
        private  TransferFunction GetJunctionsLengths(SoundReductionIndexIso wall)
        {
            //Log.Info("JunctionLength NumberOfJunctions " + AttachedJunctions.NumberOfJunctions);
            var junctionLengths = new TransferFunction(wall.Junctions.numJunctions);
            for (var i = 0; i < wall.Junctions.numJunctions; i++)
                if (wall.Junctions.AttachedWalls[i] != null)
                    junctionLengths.Tf[i] = wall.Junctions.AttachedWalls[i].JunctionLength;
                else
                    junctionLengths.Tf[i] = -1.0f;

            DataLogger.Log(wall.LogName + "JunctionLengths", junctionLengths);

            return junctionLengths;
        }

        /// <summary>
        /// Calculate Reduction Index general  with tauSituGen
        /// </summary>
        /// <returns>Reduction Index general</returns>
        public  TransferFunction GetReductionIndexSituGen(SoundReductionIndexIso wall)
        {
            if (DataLogger.DebugData)
                WriteInitialData(wall);

            var tauSituGen = GetTauSituGen(wall);
            var reductionIndexSituGen = -10 * tauSituGen.Log(10);

            DataLogger.Log(wall.LogName + "ReductionIndexSituGen", reductionIndexSituGen);

            return reductionIndexSituGen;
        }

        /// <summary>
        /// Calculate Reduction Index Situ  with tauSitu
        /// </summary>
        /// <returns>Reduction Index Situ</returns>
        public  TransferFunction GetReductionIndexSitu(SoundReductionIndexIso wall)
        {
            if (DataLogger.DebugData)
                WriteInitialData(wall);

            var tauSituGen = GetTauSitu(wall);
            var reductionIndexSitu = -10 * tauSituGen.Log(10);

            DataLogger.Log(wall.LogName + "reductionIndexSitu", reductionIndexSitu);

            return reductionIndexSitu;
        }

        /// <summary>
        /// Calculate Reduction Index Lab  with tauSituLab
        /// </summary>
        /// <returns>Reduction Index Laboratory</returns>
        public  TransferFunction GetReductionIndexSituLab(SoundReductionIndexIso wall)
        {
            if (DataLogger.DebugData)
                WriteInitialData(wall);

            var tauSituLab = GetTauLab(wall);
            var reductionIndexSituLab = -10 * tauSituLab.Log(10);

            DataLogger.Log(wall.LogName + "ReductionIndexSituLab", reductionIndexSituLab);

            return reductionIndexSituLab;
        }

        /// <summary>
        /// Get Reduction Resonance
        /// </summary>
        /// <returns>Reduction Index Laboratory</returns>
        public  TransferFunction GetReductionResonance(SoundReductionIndexIso wall)
        {
            if (DataLogger.DebugData)
                WriteInitialData(wall);

            var sigmaA = GetSigmaA(wall);
            var sigmaS = GetSigmaS(wall);
            var reductionResonance = GetReductionIndexSituGen(wall) + 10 * (sigmaA / sigmaS).Log(10);

            DataLogger.Log(wall.LogName + "ReductionResonance", reductionResonance);

            return reductionResonance;
        }

        /// <summary>
        /// Get Reduction Index Situ
        /// </summary>
        /// <returns>Reduction Index Situ</returns>
        public  TransferFunction CalcReductionIndexSituWithTsCorrection(SoundReductionIndexIso wall)
        {
            if (DataLogger.DebugData)
                WriteInitialData(wall);

            var tsSitu = GetTsSitu(wall);
            var tsLabK = GetTsLabK(wall);
            var reductionIndexSitu = GetReductionIndexSitu(wall) - 10 * (tsSitu / tsLabK).Log(10);

            DataLogger.Log(wall.LogName + "ReductionIndexSitu", reductionIndexSitu);

            return reductionIndexSitu;
        }


        /// <summary>
        /// calculate equal absorption Length
        /// </summary>
        /// <returns></returns>
        public  TransferFunction GetEqualAbsorptionLength(SoundReductionIndexIso wall)
        {
            if (DataLogger.DebugData)
                WriteInitialData(wall);

            // if (!ReferenceEquals(wall.EquivalentAbsorptionCoefficient, null))
            //     if (wall.EquivalentAbsorptionCoefficient.NumFrequency != 0)
            //         return wall.EquivalentAbsorptionCoefficient;

            var tsSitu = GetTsSitu(wall);
            var equAbsorptionLengthTransferFunction = 2.2f * Mathf.Pow(Mathf.PI, 2) * wall.Geometry.Area / (BaSettings.Instance.C0 * tsSitu) *
                                                      (BaSettings.Instance.Fref / BaSettings.Instance.ThirdOctaveFrequencyBandShort).Sqrt();

            DataLogger.Log(wall.LogName + "equAbsorptionLengthTransferFunction", equAbsorptionLengthTransferFunction);

            return equAbsorptionLengthTransferFunction;
        }


        // /// <summary>
        // /// Return Delta Reduction for additional Layer
        // /// </summary>
        // /// <param name="wall">input wall</param>
        // /// <param name="direction">positive = 0, negative = 1</param>
        // /// <returns>Delta Reduction as TransferFunction</returns>
        // public  float GetDeltaR(SoundReductionIndexISO wall, int direction)
        // {
        //     if (DataLogger.DebugData)
        //         WriteInitialData(wall);
        //
        //     if (!wall.ActivateAdditionalLayer | (wall.additionalLayers == null))
        //         return 0;
        //
        //     foreach (var layer in wall.additionalLayers)
        //         if (direction == layer.Direction)
        //             return layer.deltaRw;
        //
        //     return 0;
        // }
        //
        // /// <summary>
        // /// Return Delta Reduction for additional Layer in energetic Form
        // /// </summary>
        // /// <param name="wall">input wall</param>
        // /// <param name="direction">positive = 0, negative = 1</param>
        // /// <returns>Delta Reduction as TransferFunction in Energetic Form</returns>
        // public  float GetDeltaREnergetic(SoundReductionIndexISO wall, int direction)
        // {
        //     if (DataLogger.DebugData)
        //         WriteInitialData(wall);
        //
        //     if (!wall.ActivateAdditionalLayer)
        //         return 0;
        //
        //     foreach (var layer in wall.additionalLayers)
        //         if (direction == layer.Direction)
        //             return Mathf.Pow(10, layer.deltaRw) / -10f;
        //
        //     return 0;
        // }
    }
}