﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class FlankingWallData
    {
        public int id;
        public Vector3 Position;
        public Vector3[] PatchesPosition;
        public Vector3 Dimension;
        public Vector3[] PatchesDimension;
        public Vector3 Normal;
        public float Area;
        public float[] PatchesArea;
    }
}