﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class SoundReductionIndexIso : SoundReductionIndex
    {
        public override SoundReductionIndexType Type => SoundReductionIndexType.ISO;
        private ReductionIndexCalculator _reductionIndexCalculator;


        public override TransferFunction ReductionIndex =>
            (reductionIndex == null) || reductionIndex.NumFrequency == 0 ? CalcSoundReductionIndex() : reductionIndex;

        [SerializeField] private TransferFunction reductionIndex = null;


        public Junctions Junctions => wallBehaviour.Junctions;


        // [SerializeField] private AttJunctions attJunctions;
        public WallStructureMaterial Material => wallStructureMaterial;
        [SerializeField] public WallStructureMaterial wallStructureMaterial;


        // [SerializeField] private WallStructureMaterial wallStructureMaterial;

        public WallGeometry Geometry => wallBehaviour.Geometry;


        // [SerializeField][HideInInspector] private WallGeometry geometry;

        public WallBehaviour WallBehaviour => wallBehaviour;
        [SerializeField][HideInInspector] private WallBehaviour wallBehaviour;


        public override TransferFunction EqualAbsorptionLength =>
             equalAbsorptionLength==null || equalAbsorptionLength.NumFrequency != 21
                ? CalcEqualAbsorptionLength()
                : equalAbsorptionLength;

        public override float Mass => wallStructureMaterial.Density * wallBehaviour.Geometry.Thickness;
        /// <summary>
        /// Get Critical Frequency of the Wall
        /// </summary>
        public override float CriticalFrequency => Mathf.Pow(BaSettings.Instance.C0, 2) / (1.8f * wallStructureMaterial.QuasiLongPhaseVelocity * wallBehaviour.Geometry.Thickness);

        /// <summary>
        /// correction term for fc
        /// </summary>
        public override TransferFunction CriticalFrequencyEff => CriticalFrequency *
                                                                 (4.05f * wallBehaviour.Geometry.Thickness * BaSettings.Instance.ThirdOctaveFrequencyBandShort /
                                                                  wallStructureMaterial.QuasiLongPhaseVelocity);


        [SerializeField] [HideInInspector] private TransferFunction equalAbsorptionLength = null;


        public readonly string LogName;


        public override void Init(GameObject gameObject)
        {
            wallBehaviour = gameObject.GetComponent<WallBehaviour>();
            id = wallBehaviour.GetInstanceID();
        }



        public override TransferFunction CalcSoundReductionIndex(TransferFunction f=null)
        {
            if (_reductionIndexCalculator == null)
                _reductionIndexCalculator = new ReductionIndexCalculator();
            try
            {
                CalcReductionIndex();
                var tsSitu = _reductionIndexCalculator.GetTsSitu(this);
                equalAbsorptionLength=2.2f * Mathf.Pow(Mathf.PI, 2) * Geometry.Area / (BaSettings.Instance.C0 * tsSitu) *
                                      (BaSettings.Instance.Fref / BaSettings.Instance.ThirdOctaveFrequencyBandShort).Sqrt();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Debug.LogError(LogName + ": Reduction IndexCalculation Failed");
                throw;
            }

            return reductionIndex;
        }

        public override TransferFunction CalcSoundReductionIndexAngle(float incidentAngleRad,TransferFunction f=null)
        {
            return new TransferFunction();
        }

        private TransferFunction CalcEqualAbsorptionLength()
        {
            CalcSoundReductionIndex();
            return equalAbsorptionLength;
        }

        public override void OnValidate()
        {
            
        }

        private TransferFunction CalcReductionIndex()
        {
            reductionIndex = _reductionIndexCalculator.CalcReductionIndexSituWithTsCorrection(this);
           
            reductionIndex.UpdateCurve();
            return reductionIndex;
        }
    }
}