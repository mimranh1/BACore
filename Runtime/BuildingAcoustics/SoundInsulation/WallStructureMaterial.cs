﻿using System;
using UnityEditor;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class WallStructureMaterial
    {
        [HideInInspector] public int id;
        /// <summary>
        /// returns the Material Structure Object of this Class
        /// </summary>
        public MaterialStructureData MaterialStructure => materialStructure == null ? SetDefaultMaterial() : materialStructure;

        [SerializeField] private MaterialStructureData materialStructure;

        public float Density =>  MaterialStructure.density;

        public string Type => MaterialStructure.type;

        public float InternalLossFactor =>  MaterialStructure.internalLossFactor;

        /// <summary>
        /// Returns the Quasi-long Phase Velocity from the Material Structure
        /// </summary>
        public float QuasiLongPhaseVelocity => MaterialStructure == null ? 0f : MaterialStructure.quasiLongPhaseVelocity;
        
        public float PoissonsRatio => 0.22f; // Hopkins (2.21)
        public float YoungsModulus => Mathf.Pow(QuasiLongPhaseVelocity,2)* Density * (1f -
            (PoissonsRatio * PoissonsRatio)); // Hopkins (2.21)
        public float ShearModulus => YoungsModulus / (2f * (1f + PoissonsRatio)); // Hopkins (2.26)

        public bool isHeavyWall => MaterialStructure.isHeavyWall;

        public WallStructureMaterial(int id,MaterialStructureData materialStructure)
        {
            this.id = id;
            this.materialStructure = materialStructure;
        }

        private MaterialStructureData SetDefaultMaterial()
        {
            if (materialStructure == null)
                materialStructure = (MaterialStructureData) AssetDatabase.LoadAssetAtPath(
                    "Assets/BACore/Runtime/Database/MaterialStructureData/Concrete.asset", typeof(MaterialStructureData));
            return materialStructure;
        }

        public void OnValidate()
        {
        }

    }
}