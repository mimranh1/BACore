﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class TauFlankingSecondarySource
    {
        public float[] transferFunction;
        public int id;
        public float separatingArea;
        public Vector3 position;
        public Vector3 dimension;
        
        public TauFlankingSecondarySource(WallBehaviour wallReceiver, TransferFunction transferFunction,float separatingArea)
        {
            id = wallReceiver.GetInstanceID();
            dimension = wallReceiver.Geometry.Dimensions;
            position = wallReceiver.Geometry.Position;
            this.transferFunction = transferFunction.Tf;
            this.separatingArea = separatingArea;
        }
    }

}