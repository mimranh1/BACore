using System;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace BA.BACore
{
    /// <summary>
    /// Additional Layer Structure Form
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    [CreateAssetMenu(fileName = "Data", menuName = "BACore/Runtime/MaterialAddLayerData", order = 1)]
    [Serializable]
    public class MaterialAddLayerData : ScriptableObject
    {
        public enum LayerMaterialEnum
        {
            MineralWool,

            /**<  */
            Foams,

            /**< Foam lie polystyrene (PS), extruded polystyrene (EPS) or elastified extruded polystyrene (EEPS) >= -3 */
            StudsMetal, /**< Additional layers built with metal or wooden studs or battens not directly connected to the basic structural element*/
        }

        public LayerMaterialEnum type;

        /// <summary>
        /// Name of Additional Layer
        /// </summary>
        public string Materialname;

        /// <summary>
        /// detailed description of Additional Layer Material
        /// </summary>
        public string description;

        /// <summary>
        /// reference to material to color the additional layer
        /// </summary>
        public Material material;

        /// <summary>
        /// density of materialStructure (kg/m^2)
        /// </summary>
        public float density;

        /// <summary>
        /// Dynamic stiffness of the insulation layer in meganewtons per cubic metres
        /// </summary>
        public float stiffness;

        /// <summary>
        /// Absorption Coefficient
        /// </summary>
        public TransferFunction absorptionCoefficient = new TransferFunction(31);

        /// <summary>
        /// Frequency Band 
        /// </summary>
        public TransferFunction FrequencyBand => new TransferFunction(new[]
        {
            20f, 25f, 31.5f, 40f, 50f, 63f, 80f, 100f, 125f, 160f, 200f, 250f, 315f, 400f, 500f, 630f, 800f, 1000f,
            1250f, 1600f, 2000f, 2500f, 3150f, 4000f, 5000f, 6300f, 8000f, 10000f, 12500f, 16000f, 20000f
        });

        /// <summary>
        /// Write the assets to Material Database based on ISO
        /// </summary>
        public static void CreateAssets()
        {
            var folder = $"{Application.dataPath}/BACore/Runtime/Database/MaterialAddLayerData/";
            Directory.CreateDirectory(folder);
            using (var reader = new StreamReader($"{Application.dataPath}/BACore/Runtime/Database/AddLayer.csv"))
            {
                while (!reader.EndOfStream)
                {
                    //Name;description;type;stiffness;Density
                    var asset = ScriptableObject.CreateInstance<MaterialAddLayerData>();
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    // asset.type = values[0];
                    switch (values[2])
                    {
                        case "foam":
                            asset.type = MaterialAddLayerData.LayerMaterialEnum.Foams;
                            break;
                        case "wool":
                            asset.type = MaterialAddLayerData.LayerMaterialEnum.MineralWool;
                            break;
                        case "StudsMetal":
                            asset.type = MaterialAddLayerData.LayerMaterialEnum.StudsMetal;
                            break;
                        default:
                            Debug.LogError("Cannnot find type " + values[2] + " of addition Layer Materials.");
                            asset.type = MaterialAddLayerData.LayerMaterialEnum.Foams;
                            break;
                    }

                    asset.Materialname = values[0];
                    asset.name = values[0];
                    asset.description = values[1];
                    asset.stiffness = float.Parse(values[3].Replace(",", "."));
                    asset.density = float.Parse(values[4].Replace(",", "."));

                    var assetPathAndName =
                        AssetDatabase.GenerateUniqueAssetPath("Assets/BACore/Runtime/Database/MaterialAddLayerData/" +
                                                              asset.name + ".asset");

                    AssetDatabase.CreateAsset(asset, assetPathAndName);

                    AssetDatabase.SaveAssets();
                    AssetDatabase.Refresh();
                    EditorUtility.FocusProjectWindow();
                    Selection.activeObject = asset;
                }
            }
        }

    }
}