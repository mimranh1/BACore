﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace BA.BACore
{
    /// <summary>
    /// Reduction Index Data Holder from Database
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    [CreateAssetMenu(fileName = "Data", menuName = "BACore/Runtime/ReductionIndex", order = 1)]
    public class ReductionIndexData : ScriptableObject
    {
        /// <summary>
        /// Reduction Index Name
        /// </summary>
        public string objectName = "Reduction Index";

        /// <summary>
        /// Type of Reduction Index
        /// </summary>
        [SerializeField] public string type = "Wall";

        /// <summary>
        /// Reduction Index als TransferFunction
        /// </summary>
        [SerializeField] public TransferFunction red = new TransferFunction();

        /// <summary>
        /// detailed description of Reduction Index
        /// </summary>
        [SerializeField] public string comment = "";

        /// <summary>
        /// unique Id for Reduction Index
        /// </summary>
        [SerializeField] public int id;

        /// <summary>
        /// Length of Reduction Index (numFreq)
        /// </summary>
        [SerializeField] public int numFreq = 0;

        private void OnEnable()
        {
            red.UpdateCurve();
        }

        private void OnValidate()
        {
            red.UpdateCurve();
        }

        /// <summary>
        /// Write the assets to Reduction Index Database based on Thaden
        /// </summary>
        public static void CreateAssets()
        {
            var folder = BaSettings.Instance.RootPath;
            var files = Directory.GetFiles(folder, "BastianData.csv", SearchOption.AllDirectories);
            if (files.Length != 1)
                Debug.LogError("File BastianData.csv not found.");
            var dataFile = files[0];
            var dataPath=Path.GetDirectoryName(dataFile);
            var fullPath = new Uri(dataPath, UriKind.Absolute);
            var relRoot = new Uri($"{Application.dataPath}/../", UriKind.Absolute);
            var savePath =  relRoot.MakeRelativeUri(fullPath).ToString() + "/Bastian/";
            Directory.CreateDirectory(savePath);
            using (var reader = new StreamReader(dataFile))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    var assetType = values[2];
                    var assetObjectName = values[0];
                    var assetPath = savePath + 
                                       assetType + "_" + assetObjectName.Replace(",", "\0") + ".asset";
                    if (File.Exists(assetPath))
                    {
                        Debug.Log(assetPath + " already exists.");
                        continue;
                    }
                    
                    var asset = ScriptableObject.CreateInstance<ReductionIndexData>();
                    var reductionIndex = new TransferFunction(21);
                    asset.numFreq = values.Length - 3;
                    for (var i = 0; i < asset.numFreq; i++)
                    {
                        var num = values[i + 3].Replace(".", ",");
                        if (num == "")
                            reductionIndex.Tf[i] = float.NaN;
                        else
                            reductionIndex.Tf[i] = float.Parse(num);
                    }

                    asset.objectName = assetObjectName;
                    asset.red = reductionIndex;
                    asset.type = assetType;
                    asset.comment = values[1];
                    asset.id = 1;

                    var assetPathAndName =
                        AssetDatabase.GenerateUniqueAssetPath(assetPath);
                    AssetDatabase.CreateAsset(asset, assetPathAndName);

                    AssetDatabase.SaveAssets();
                    AssetDatabase.Refresh();
                    EditorUtility.FocusProjectWindow();
                    Selection.activeObject = asset;
                }
            }
        }


    }
}