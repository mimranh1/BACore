﻿namespace BA.BACore
{
    public enum SoundReductionIndexType
    {
        None,
        ISO,
        Database,
        Davy
    }
}