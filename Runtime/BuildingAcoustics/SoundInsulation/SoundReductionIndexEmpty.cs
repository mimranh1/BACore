﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class SoundReductionIndexEmpty : SoundReductionIndex
    {
        public override SoundReductionIndexType Type => SoundReductionIndexType.None;
        public override TransferFunction ReductionIndex { get; }
        public override TransferFunction EqualAbsorptionLength { get; }
        public override float Mass { get; }
        public override float CriticalFrequency { get; }
        public override TransferFunction CriticalFrequencyEff { get; }

        public override TransferFunction CalcSoundReductionIndex(TransferFunction f=null)
        {
            return null;
        }

        public override TransferFunction CalcSoundReductionIndexAngle(float incidentAngleRad,TransferFunction f=null)
        {
            return new TransferFunction();
        }

        public override void OnValidate()
        {
        }

        public override void Init(GameObject gameObject)
        {
            
        }
    }
}