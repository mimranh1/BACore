using System;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace BA.BACore
{
    /// <summary>
    /// Material Structure Form
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de)
    /// Version:        1.2
    /// First release:  2017
    /// Last revision:  2019
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    [CreateAssetMenu(fileName = "Data", menuName = "BACore/Runtime/MaterialStructureData", order = 1)]
    [Serializable]
    public class MaterialStructureData : ScriptableObject
    {
        /// <summary>
        /// Set default Parameter
        /// </summary>
        private void OnEnable()
        {
            type = "Default";
            description = "like Concrete Wall";
            density = 2200f;
            quasiLongPhaseVelocity = 3800;
            internalLossFactor = 0.005f;
        }

        /// <summary>
        /// Type of Material
        /// </summary>
        public string type;

        /// <summary>
        /// detailed description of Material
        /// </summary>
        public string description;

        /// <summary>
        /// is the wall heavy, important for the Reduction Index calculation 
        /// </summary>
        public bool isHeavyWall = true;

        /// <summary>
        /// density of materialStructure (kg/m^2)
        /// </summary>
        public float density;

        /// <summary>
        /// QuasiLong Phase Velocity for materialStructure (m/s)
        /// </summary>
        public float quasiLongPhaseVelocity;

        /// <summary>
        /// Internal loss factor
        /// </summary>
        public float internalLossFactor;
    
    
        /// <summary>
        /// Write the assets to Material Database based on ISO
        /// </summary>
        public static void CreateAssets()
        {
            var folder = BaSettings.Instance.RootPath;
            var files = Directory.GetFiles(folder, "MaterialStructureData.csv", SearchOption.AllDirectories);
            if (files.Length != 1)
                Debug.LogError("File MaterialStructureData.csv not found.");
            var dataFile = files[0];
           var dataPath=Path.GetDirectoryName(dataFile);
            var fullPath = new Uri(dataPath, UriKind.Absolute);
            var relRoot = new Uri($"{Application.dataPath}/../", UriKind.Absolute);
            var savePath =  relRoot.MakeRelativeUri(fullPath).ToString() + "/MaterialStructureData/";
            Directory.CreateDirectory(savePath);
            using (var reader = new StreamReader(dataFile))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    var assetType = values[0];
                    var assetPath = savePath + assetType + ".asset";
                    if (File.Exists(assetPath))
                    {
                        Debug.Log(assetType + " already exists.");
                        continue;
                    }
                    var asset = CreateInstance<MaterialStructureData>();
                    asset.type = assetType;
                    asset.description = values[1];
                    asset.isHeavyWall = values[2] == "1";
                    asset.density = float.Parse(values[3].Replace(",", "."));
                    asset.quasiLongPhaseVelocity = float.Parse(values[4].Replace(",", "."));
                    asset.internalLossFactor = float.Parse(values[5].Replace(",", "."));

                    var assetPathAndName =
                        AssetDatabase.GenerateUniqueAssetPath(assetPath);
                    AssetDatabase.CreateAsset(asset, assetPathAndName);

                    AssetDatabase.SaveAssets();
                    AssetDatabase.Refresh();
                    EditorUtility.FocusProjectWindow();
                    Selection.activeObject = asset;
                }
            }
        }
    }
}