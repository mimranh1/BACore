﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class SoundReductionIndexDataBase : SoundReductionIndex
    {
        public override SoundReductionIndexType Type => SoundReductionIndexType.Database;
        [SerializeField] public ReductionIndexData reductionIndexData;


        public override TransferFunction ReductionIndex => reductionIndex;
        [SerializeField] private TransferFunction reductionIndex;

        [SerializeField] [HideInInspector] private float area;


        public override TransferFunction EqualAbsorptionLength =>
            2.2f * Mathf.Pow(Mathf.PI, 2) * area / (BaSettings.Instance.C0 * reductionIndex) *
            (BaSettings.Instance.Fref / BaSettings.Instance.ThirdOctaveFrequencyBandShort).Sqrt();

        public override float Mass { get; }
        public override float CriticalFrequency { get; }
        public override TransferFunction CriticalFrequencyEff { get; }

        public override void Init(GameObject gameObject)
        {
            id = gameObject.GetInstanceID();
            area = gameObject.GetComponent<WallBehaviour>().Geometry.Area;
        }
        
        public override TransferFunction CalcSoundReductionIndex(TransferFunction f=null)
        {
            reductionIndex = reductionIndexData.red;
            reductionIndex.UpdateCurve();
            return reductionIndex;
        }

        public override TransferFunction CalcSoundReductionIndexAngle(float incidentAngleRad,TransferFunction f=null)
        {
            throw new NotImplementedException();
        }

        public override void OnValidate()
        {
            if (reductionIndexData != null)
                CalcSoundReductionIndex();
            else
                reductionIndex = new TransferFunction(31);
        }
    }
}