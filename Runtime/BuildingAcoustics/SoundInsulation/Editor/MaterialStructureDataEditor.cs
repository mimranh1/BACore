using System;
using System.Globalization;
using BA.BACore;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

[CustomEditor(typeof(MaterialStructureData))]
public class MaterialStructureDataEditor : Editor
{
    private MaterialStructureData material;
    private GUIStyle boldStyle;
    private int[] numFreq = new[] {0, 21, 31};

    private void OnEnable()
    {
        material = (MaterialStructureData) target;
        boldStyle = new GUIStyle {fontStyle = FontStyle.Bold};
    }

    /// <summary>
    /// overrides the Inspector view in the Editor mode
    /// </summary>
    public override void OnInspectorGUI()
    {
        GUILayout.Label("", GUILayout.Height(5));
        GUILayout.Label("Information", boldStyle);
        material.type = EditorGUILayout.TextField("Name", material.type);

        GUILayout.Label("Comment");
        material.description = EditorGUILayout.TextArea(material.description);

        GUILayout.Label("", GUILayout.Height(5));
        GUILayout.Label("Material Settings", boldStyle);

        material.isHeavyWall = EditorGUILayout.Toggle("is Heavy Wall", material.isHeavyWall);
        material.density = EditorGUILayout.FloatField("Density", material.density);
        material.quasiLongPhaseVelocity =
            EditorGUILayout.FloatField("Quasilong Phase Velocity", material.quasiLongPhaseVelocity);
        material.internalLossFactor = EditorGUILayout.FloatField("Internal Loss Factor", material.internalLossFactor);
        EditorUtility.SetDirty(material);
    }
}