using System;
using System.Globalization;
using BA.BACore;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

[CustomEditor(typeof(MaterialAddLayerData))]
public class MaterialAddLayerDataEditor : Editor
{
    private MaterialAddLayerData material;
    private GUIStyle boldStyle;
    private int[] numFreq = new[] {0, 21, 31};

    private void OnEnable()
    {
        material = (MaterialAddLayerData) target;
        boldStyle = new GUIStyle {fontStyle = FontStyle.Bold};
    }

    /// <summary>
    /// overrides the Inspector view in the Editor mode
    /// </summary>
    public override void OnInspectorGUI()
    {
        GUILayout.Label("", GUILayout.Height(5));
        GUILayout.Label("Information", boldStyle);
        material.Materialname = EditorGUILayout.TextField("Name", material.Materialname);

        material.type = (MaterialAddLayerData.LayerMaterialEnum) EditorGUILayout.EnumPopup("Type", material.type);

        GUILayout.Label("Comment");
        material.description = EditorGUILayout.TextArea(material.description);

        GUILayout.Label("", GUILayout.Height(5));
        GUILayout.Label("Material Settings", boldStyle);

        material.material =
            EditorGUILayout.ObjectField("Material", material.material, typeof(Material), true) as Material;

        material.density = EditorGUILayout.FloatField("density", material.density);
        material.stiffness = EditorGUILayout.FloatField("stiffness", material.stiffness);

        var freq = BaSettings.Instance.ThirdOctaveFrequencyBandNorm;

        material.absorptionCoefficient.Curve =
            EditorGUILayout.CurveField(material.absorptionCoefficient.Curve, GUILayout.MinHeight(100));

        GUILayout.BeginHorizontal();
        GUILayout.Label("Frequency", boldStyle, GUILayout.Width(100));
        GUILayout.Label("R Situ (dB)", boldStyle, GUILayout.MinWidth(70));
        GUILayout.EndHorizontal();

        for (var i = 0; i < freq.NumFrequency; i++)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label(freq.Tf[i].ToString(), GUILayout.Width(100));
            material.absorptionCoefficient.Tf[i] =
                EditorGUILayout.FloatField("", material.absorptionCoefficient.Tf[i], GUILayout.MinWidth(70));
            GUILayout.EndHorizontal();
        }

        EditorUtility.SetDirty(material);
    }
}