﻿namespace BA.BACore
{
    public enum JunctionTypes
    {
        Undefined=-1,
        L_Junction=1,
        T_Junction=2,
        X_Junction=3,
    }
}