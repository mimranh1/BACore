﻿using System.Collections.Generic;
using UnityEngine;

namespace BA.BACore
{
    public interface IWallPatchesHandler
    {
        List<WallBehaviour> Patches { get; }
        bool[] IsActive { get; }
        /// <summary>
        /// saves all the positions of the secondary sources of the patches of this wall
        /// </summary>
        Vector3[][] SecondarySourcesPosPatches { get; }
        
        /// <summary>
        /// Return Reduction Index in dB for whole massive Wall or Patch
        /// </summary>
        TransferFunction[] ReductionIndexPatches { get; }
        
        int NumOfPatches { get; }
    }
}