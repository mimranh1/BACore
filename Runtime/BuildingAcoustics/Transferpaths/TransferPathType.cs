﻿using System;

namespace BA.BACore
{
    [Serializable]
    public enum TransferPathType
    {
        Partition,
        Junction,
        MultiplePartitions
    }
}