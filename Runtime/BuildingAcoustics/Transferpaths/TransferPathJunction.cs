﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class TransferPathJunction : TransferPath
    {
        public override  TransferPathType Type => TransferPathType.Junction;
        public override WallBehaviour[] Partitons => null;
        [SerializeField] protected WallBehaviour partition1;
        [SerializeField] protected WallBehaviour partition2;

        public override  TauFlankingSecondarySource[] CalcTauPaths(RoomAcousticsBehaviour sourceRoom,
            RoomAcousticsBehaviour receiverRoom)
        {
            return CalcTauPaths(partition1, partition2, sourceRoom, receiverRoom, 10f);
        }

        public override  TauFlankingFactor[] CalcFlankingFactorsPaths(RoomAcousticsBehaviour sourceRoom,
            RoomAcousticsBehaviour receiverRoom)
        {
            throw new NotImplementedException();
        }

        private TauFlankingSecondarySource[] CalcTauPaths(WallBehaviour partition1, WallBehaviour partition2,
            RoomAcousticsBehaviour sourceRoom, RoomAcousticsBehaviour receiverRoom, float separatingArea)
        {
            var junction = partition1.Junctions;
            var attachedWalls = junction.AttachedWalls;
            var rijs = junction.CalculateFlanking(separatingArea);
            var tauPaths = new List<TauFlankingSecondarySource>();
            var partitionTf = new TransferFunction(21);
            for (var i = 0; i < rijs.Length; i++)
            {
                if (attachedWalls[i].JunctionType != JunctionTypes.X_Junction)
                    continue;
                var sourceId1 = attachedWalls[i].FindWallInstanceIndex(partition1.GetInstanceID());
                var sourceId2 = attachedWalls[i].FindWallInstanceIndex(partition2.GetInstanceID());
                if (sourceId1 == -1 || sourceId2 == -1)
                    continue;
                var otherIndexes = new List<int>(new[] {0, 1, 2, 3});
                otherIndexes.Remove(sourceId1);
                otherIndexes.Remove(sourceId2);
                var receiverId1 = otherIndexes[0];
                var receiverId2 = otherIndexes[1];


                var tf1 = (10 ^ (rijs[i][sourceId1, receiverId1] * -0.1f)) +
                          (10 ^ (rijs[i][sourceId2, receiverId1] * -0.1f));
                tauPaths.Add(
                    new TauFlankingSecondarySource(attachedWalls[i].AttachedWallDatas[receiverId1].WallBehaviour, tf1,
                        separatingArea));

                var tf2 = (10 ^ (rijs[i][sourceId1, receiverId2] * -0.1f)) +
                          (10 ^ (rijs[i][sourceId2, receiverId2] * -0.1f));
                tauPaths.Add(
                    new TauFlankingSecondarySource(attachedWalls[i].AttachedWallDatas[receiverId2].WallBehaviour, tf2,
                        separatingArea));
                return tauPaths.ToArray();

            }

            Debug.LogError("Cannot find paths for ", sourceRoom);
            return tauPaths.ToArray();
        }

    }
}