﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class TauFlankingFactor
    {
        public float factor;
        public int sourceWallInstanceId;
        public int receiverWallInstanceId;
        public int sourceWallRoomId;
        public int receiverWallRoomId;
        public float separatingArea;
        public float[] reductionIndex;

        public TauFlankingFactor(WallBehaviour wallSource, WallBehaviour wallReceiver, float factor,
            float separatingArea,int sourceWallRoomId,int receiverWallRoomId,float[] reductionIndex)
        {
            sourceWallInstanceId = wallSource.GetInstanceID();
            receiverWallInstanceId = wallReceiver.GetInstanceID();
            this.sourceWallRoomId = sourceWallRoomId;
            this.receiverWallRoomId = receiverWallRoomId;
            this.factor = factor;
            this.separatingArea = separatingArea;
            this.reductionIndex = reductionIndex;
        }
    }
}