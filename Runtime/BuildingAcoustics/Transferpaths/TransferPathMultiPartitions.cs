﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class TransferPathMultiPartitions : TransferPath
    {
        public override TransferPathType Type => TransferPathType.MultiplePartitions;
        [SerializeField] protected WallBehaviour[] partitions;
        public override WallBehaviour[] Partitons => partitions;

        public override TauFlankingSecondarySource[] CalcTauPaths(RoomAcousticsBehaviour sourceRoom,
            RoomAcousticsBehaviour receiverRoom)
        {
            var list = new List<TauFlankingSecondarySource>();
            foreach (var partition in partitions)
            {
                list.AddRange(CalcTauPaths(partition, sourceRoom, receiverRoom,
                    partition.Geometry.Area));
            }

            return list.ToArray();
        }

        public override TauFlankingFactor[] CalcFlankingFactorsPaths(RoomAcousticsBehaviour sourceRoom, RoomAcousticsBehaviour receiverRoom)
        {
            
            var list = new List<TauFlankingFactor>();
            foreach (var partition in partitions)
            {
                list.AddRange(CalcFlankingFactorsPaths(partition, sourceRoom, receiverRoom,
                    partition.Geometry.Area));
            }

            return list.ToArray();
        }
    }
}