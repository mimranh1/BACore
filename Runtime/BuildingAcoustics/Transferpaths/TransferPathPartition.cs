﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class TransferPathPartition : TransferPath
    {
        [SerializeField] private  WallBehaviour partition;
        public override  TransferPathType Type => TransferPathType.Partition;
        public override  WallBehaviour[] Partitons => new[] {partition};

        public override  TauFlankingSecondarySource[] CalcTauPaths(RoomAcousticsBehaviour sourceRoom, RoomAcousticsBehaviour receiverRoom)
        {
            return CalcTauPaths(partition, sourceRoom, receiverRoom, partition.Geometry.Area);
        }

        public override  TauFlankingFactor[] CalcFlankingFactorsPaths(RoomAcousticsBehaviour sourceRoom, RoomAcousticsBehaviour receiverRoom)
        {
            return CalcFlankingFactorsPaths(partition, sourceRoom, receiverRoom, partition.Geometry.Area);
        }
        
    }
}