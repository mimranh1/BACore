﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public abstract class TransferPath : ExtendedScriptableObject<TransferPath,TransferPathType>
    {
        public abstract WallBehaviour[] Partitons { get; }

        public abstract TauFlankingSecondarySource[] CalcTauPaths(RoomAcousticsBehaviour sourceRoom,
            RoomAcousticsBehaviour receiverRoom);

        public abstract TauFlankingFactor[] CalcFlankingFactorsPaths(RoomAcousticsBehaviour sourceRoom,
            RoomAcousticsBehaviour receiverRoom);
        
        
        
        protected TauFlankingFactor[] CalcFlankingFactorsPaths(WallBehaviour partition, RoomAcousticsBehaviour sourceRoom,
            RoomAcousticsBehaviour receiverRoom, float separatingArea)
        {
            var junction = partition.Junctions;
            var attachedWalls = junction.AttachedWalls;
            var rijFactors = junction.CalcFlankingFactors();
            var rijs = junction.CalculateFlanking(partition.Geometry.Area);
            var tauPaths = new List<TauFlankingFactor>();
            var sourcePartition = sourceRoom.RoomGeometry.FindWallIndex(partition.GetInstanceID());
            var receiverPartition = receiverRoom.RoomGeometry.FindWallIndex(partition.GetInstanceID());
            if (sourcePartition == -1 || receiverPartition == -1)
                Debug.LogError("Room Id is not found");
            
            for (var i = 0; i < rijFactors.Length; i++)
            {
                var partitionId = attachedWalls[i].FindWallInstanceIndex(partition.GetInstanceID());
                var sourceId = junction.GetWallIndex(attachedWalls[i], sourceRoom,partitionId);
                var sourceRoomId = sourceRoom.RoomGeometry.FindWallIndex(attachedWalls[i].AttachedWallDatas[sourceId].WallBehaviour.GetInstanceID());
                var receiverId = junction.GetWallIndex(attachedWalls[i], receiverRoom,partitionId);
                var receiverRoomId = receiverRoom.RoomGeometry.FindWallIndex(attachedWalls[i].AttachedWallDatas[receiverId].WallBehaviour.GetInstanceID());
                if (receiverRoomId == -1 || sourceRoomId == -1)
                    Debug.LogError("Room Id is not found");
                Debug.Log($"flankingWalls: source: {attachedWalls[i].AttachedWallDatas[sourceId].WallBehaviour.name} partitionId: {attachedWalls[i].AttachedWallDatas[partitionId].WallBehaviour.name} receiverId: {attachedWalls[i].AttachedWallDatas[receiverId].WallBehaviour.name}");
                // Ff
                tauPaths.Add(
                    new TauFlankingFactor(attachedWalls[i].AttachedWallDatas[sourceId].WallBehaviour,
                        attachedWalls[i].AttachedWallDatas[receiverId].WallBehaviour,
                        rijFactors[i][sourceId, receiverId].Tf[0], separatingArea, sourceRoomId, receiverRoomId,
                        rijs[i][sourceId, receiverId].Tf));
                // Fd
                tauPaths.Add(
                    new TauFlankingFactor(attachedWalls[i].AttachedWallDatas[sourceId].WallBehaviour,
                        attachedWalls[i].AttachedWallDatas[partitionId].WallBehaviour,
                        rijFactors[i][sourceId, partitionId].Tf[0], separatingArea, sourceRoomId, receiverPartition,
                        rijs[i][sourceId, partitionId].Tf));
                // Df
                tauPaths.Add(
                    new TauFlankingFactor(attachedWalls[i].AttachedWallDatas[partitionId].WallBehaviour,
                        attachedWalls[i].AttachedWallDatas[receiverId].WallBehaviour,
                        rijFactors[i][partitionId, receiverId].Tf[0], separatingArea, sourcePartition, receiverRoomId,
                        rijs[i][partitionId, receiverId].Tf));

            }

            return tauPaths.ToArray();
        }

        
        protected TauFlankingSecondarySource[] CalcTauPaths(WallBehaviour partition, RoomAcousticsBehaviour sourceRoom,
            RoomAcousticsBehaviour receiverRoom, float separatingArea)
        {
            var junction = partition.Junctions;
            var AttachedWalls = junction.AttachedWalls;
            var rijs = junction.CalculateFlanking(partition.Geometry.Area);
            var tauPaths = new TauFlankingSecondarySource[5];
            var partitionTf = 10 ^ (partition.ReductionIndex.ReductionIndex * -0.1f);
            for (var i = 0; i < rijs.Length; i++)
            {
                var partionId = AttachedWalls[i].FindWallInstanceIndex(partition.GetInstanceID());
                var sourceId = junction.GetWallIndex(AttachedWalls[i], sourceRoom,partionId);
                var receiverId = junction.GetWallIndex(AttachedWalls[i], receiverRoom,partionId);
                var tf = (10 ^ (rijs[i][sourceId, receiverId] * -0.1f)) +
                         (10 ^ (rijs[i][partionId, receiverId] * -0.1f));
                partitionTf += 10 ^ (rijs[i][sourceId, partionId] * -0.1f);
                tauPaths[i + 1] =
                    new TauFlankingSecondarySource(AttachedWalls[i].AttachedWallDatas[receiverId].WallBehaviour, tf,separatingArea);

            }

            tauPaths[0] = new TauFlankingSecondarySource(partition, partitionTf,separatingArea);
            return tauPaths;
        }

    }
}