﻿using System;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace BA.BACore
{
    [Serializable]
    public class SoundSignalController : MonoBehaviour
    {

        public SoundSourceMover sourceMover;

        [System.Serializable]
        public class Trajectory
        {
            public string id;
            public WayPaths trajectory;
            public string path;

            public Trajectory(WayPaths trajectory)
            {
                this.trajectory = trajectory;
            }
        }

        [SerializeReference] public Trajectory[] trajectorys;

        [Range(0,2)]
        public int currentTrajectoryIndex=0;

        private int _currentTrajectoryIndexShadow=0;

        public DirectivityIR directivityIr;

        [Range(0,3)]
        public int reflectionOrder = 0;

        [Range(0,1)]
        public int diffractionOrder = 0;


        [SerializeReference][ReadOnly]
        public SoundSignalSources[] soundSignalSources;

        public WallBehaviour[] partitions;
        public string CurrentTrajectoryName => trajectorys[currentTrajectoryIndex].id;

        
        private string GetFileCode(int position)
        {
            return "_" + position + "_" + reflectionOrder + "_" + diffractionOrder + "_" +
                   Mathf.Max(reflectionOrder, diffractionOrder);
        }
    
        public void Awake()
        {
            if (sourceMover == null)
                Debug.LogError("Source Mover need to be set.", this);
            soundSignalSources = new SoundSignalSources[partitions.Length];
            var trajectories = FindObjectsOfType<WayPaths>().ToArray();
            if (trajectorys?.Length != trajectories.Length)
                trajectorys = new Trajectory[trajectories.Length];

            for (var i = 0; i < trajectories.Length; i++)
            {
                if (trajectorys[i] == null) trajectorys[i] = new Trajectory(trajectories[i]);
            }

            trajectorys = trajectorys.OrderBy(tr => tr.trajectory.name).ToArray();
            var position = 1;
            if (_currentTrajectoryIndexShadow != currentTrajectoryIndex || sourceMover.wayPointList == null)
                sourceMover.wayPointList = trajectorys[currentTrajectoryIndex].trajectory;

            for (var i = 0; i < partitions.Length; i++)
            {
                var comp1 = partitions[i].gameObject.GetComponent<SoundSignalSources>();
                soundSignalSources[i] = comp1 == null
                    ? partitions[i].gameObject.AddComponent<SoundSignalSources>()
                    : comp1;
                
                soundSignalSources[i].SoundSources = new SoundSourceObject[partitions[i].patchesHandler.NumOfPatches];
                for (var j = 0; j < partitions[i].patchesHandler.NumOfPatches; j++)
                {
                    var obj = partitions[i].Patches[j].gameObject;
                    var comp = obj.GetComponent<SoundSourceObject>();
                    soundSignalSources[i].SoundSources[j] = comp == null
                        ? obj.AddComponent<SoundSourceObject>()
                        : comp;
                    soundSignalSources[i].SoundSources[j].transformObj = sourceMover.transform;
                    var guids2 = AssetDatabase.FindAssets(GetFileCode(position)+" t:AudioClip", new[] {trajectorys[currentTrajectoryIndex].path});
                    if(guids2==null || guids2.Length<1 || guids2[0]==null)
                    {
                        Debug.LogError("Cannot Find " + GetFileCode(position) + " in " +
                                       trajectorys[currentTrajectoryIndex].path);
                        return;
                    }

                    var audioPath = AssetDatabase.GUIDToAssetPath(guids2[0]);
                    soundSignalSources[i].SoundSources[j].CreateNewVaSoundSource(audioPath);
                    soundSignalSources[i].SoundSources[j].hasChanged = true;
                    soundSignalSources[i].SoundSources[j].AudioLoop = false;
                    soundSignalSources[i].SoundSources[j].AudioPlayOnAwake = false;
                    soundSignalSources[i].SoundSources[j].directivityIr = directivityIr;

                    position++;
                }

                // baEnvironments[i].soundSourceObjects = soundSignalSources[i];
                Debug.Log("Updated Partition " + i);
            }
        
            _currentTrajectoryIndexShadow = currentTrajectoryIndex;
        }
        
        public void StartSpecialSound(string code, UnityEvent sourceReachedEvent=null,UnityEvent sourceStoppedPlaying=null)
        {
            var inds = code.Split('_');
            this.reflectionOrder = Int16.Parse(inds[1]);
            this.diffractionOrder = Int16.Parse(inds[2]);
            currentTrajectoryIndex = Int16.Parse(inds[0]);
            Awake();
            sourceMover.RestartPositionAndSound(sourceReachedEvent,sourceStoppedPlaying);
        }
        public void StartSound(string code)
        {
            StartSpecialSound(code);
        }

        public void StartSound()
        {
            Awake();
            sourceMover.RestartPositionAndSound();
        }
    }
}

