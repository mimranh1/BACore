﻿// using System.Globalization;
// using BA.BACore;
// using UnityEditor;
// using UnityEngine;
//
//
// [CustomEditor(typeof(WallProperties))]
// [System.Serializable]
// [CanEditMultipleObjects]
// public class WallPropertiesEditor : Editor
// {
//     private WallProperties _wall;
//     private int _tab;
//     private GUIStyle _boldStyle;
//     private bool _foldoutR = true;
//     private bool _foldoutEqual;
//     private bool _foldoutD1;
//     private bool _foldoutD2;
//
//     private void OnEnable()
//     {
//         _wall = (WallProperties) target;
//         _wall.ConfigureDefaultMembers();
//         _boldStyle = new GUIStyle {fontStyle = FontStyle.Bold};
//     }
//
//     /// <summary>
//     /// overrides the Inspector view in the Editor mode
//     /// </summary>
//     public override void OnInspectorGUI()
//     {
//         // base.OnInspectorGUI();
//         // return;
//         _tab = GUILayout.Toolbar(_tab, new[] {"General Infos", "Reduction Index"});
//         switch (_tab)
//         {
//             case 0:
//                 if (_wall.materialStructure == null)
//                     _wall.materialStructure = (MaterialStructureData) AssetDatabase.LoadAssetAtPath(
//                         "Assets/BACore/Runtime/Database/MaterialStructureData/Concrete.asset", typeof(MaterialStructureData));
//                 // Geometrical infos
//                 DrawGeometricalInfos();
//
//                 // materialStructure Settings
//                 DrawMaterialStructure();
//
//                 // Show additional Layer
//                 DrawAdditionalLayers();
//
//                 // Patches
//                 DrawPatches();
//
//                 // Attached Walls
//                 DrawAttachedWalls();
//
//                 // Calculate
//                 GUILayout.Label("", GUILayout.Height(5));
//                 GUILayout.Label("Calculate", _boldStyle);
//                 _wall.reductionIndexCalculationMethod =
//                     (WallProperties.ReductionIndexCalculationMethod) EditorGUILayout.EnumPopup("Method",
//                         _wall.reductionIndexCalculationMethod);
//                 if (_wall.reductionIndexCalculationMethod == WallProperties.ReductionIndexCalculationMethod.Database)
//                     _wall.rData = (ReductionIndexData) EditorGUILayout.ObjectField("", _wall.rData,
//                         typeof(ReductionIndexData), true, GUILayout.MinWidth(100));
//
//                 if (GUILayout.Button("Reduction Index"))
//                     _wall.CalcReductionIndexPatches();
//                 if (GUILayout.Button("Reduction Index with Flanking"))
//                     _wall.CalcSecondarySourcesForEachJunction();
//
//                 // Secondary Source Settings
//                 GUILayout.Label("", GUILayout.Height(5));
//                 GUILayout.Label("Secondary Source Settings", _boldStyle);
//                 _wall.ssMethod =
//                     (WallProperties.SecondarySourceMethod) EditorGUILayout.EnumPopup("Method", _wall.ssMethod);
//                 switch (_wall.ssMethod)
//                 {
//                     case WallProperties.SecondarySourceMethod.CalculateByFixedNumber:
//                         _wall.numberOfSecondarySourcesPerDim =
//                             EditorGUILayout.IntSlider("Number of SS per Dimension",
//                                 _wall.numberOfSecondarySourcesPerDim, 1, 10);
//                         break;
//                     case WallProperties.SecondarySourceMethod.CalculateByMaxLength:
//                         _wall.maxLengthForSs =
//                             EditorGUILayout.FloatField("Max Length of SS Area (m)", _wall.maxLengthForSs);
//                         break;
//                 }
//
//                 break;
//             case 1:
//
//                 GUILayout.Label("", GUILayout.Height(5));
//                 GUILayout.Label("Plots", _boldStyle);
//                 // Reduction Index
//                 _foldoutR = EditorGUILayout.Foldout(_foldoutR, "Reduction Index");
//                 if (_foldoutR)
//                 {
//                     var level = EditorGUI.indentLevel;
//                     EditorGUI.indentLevel++;
//                     if (!ReferenceEquals(_wall.ReductionIndex, null)) DrawCurveR(_wall.ReductionIndex);
//
//                     EditorGUI.indentLevel = level;
//                 }
//
//                 _foldoutD1 = EditorGUILayout.Foldout(_foldoutD1, "Delta R1 (Additional Layer)");
//                 if (_foldoutD1)
//                 {
//                     var level = EditorGUI.indentLevel;
//                     EditorGUI.indentLevel++;
//                     if (_wall.DeltaRForAdditionalLayer.Length != 0) DrawCurveR(_wall.DeltaRForAdditionalLayer[0]);
//
//                     EditorGUI.indentLevel = level;
//                 }
//
//                 _foldoutD2 = EditorGUILayout.Foldout(_foldoutD2, "Delta R2 (Additional Layer)");
//                 if (_foldoutD2)
//                 {
//                     var level = EditorGUI.indentLevel;
//                     EditorGUI.indentLevel++;
//                     if (_wall.DeltaRForAdditionalLayer.Length != 0) DrawCurveR(_wall.DeltaRForAdditionalLayer[1]);
//
//                     EditorGUI.indentLevel = level;
//                 }
//
//                 // Equal Absorption Coefficient
//                 _foldoutEqual = EditorGUILayout.Foldout(_foldoutEqual, "Equivalent Absorption Coefficient");
//                 if (_foldoutEqual)
//                 {
//                     var level = EditorGUI.indentLevel;
//                     EditorGUI.indentLevel++;
//                     if (_wall.EquivalentAbsorptionCoefficient.NumFrequency != 0)
//                         DrawCurveR(_wall.EquivalentAbsorptionCoefficient);
//
//                     EditorGUI.indentLevel = level;
//                 }
//
//
//                 GUILayout.Label("", GUILayout.Height(5));
//                 GUILayout.Label("Table", _boldStyle);
//
//                 var freq = BaSettings.Instance.ThirdOctaveFrequencyBandShort;
//                 GUILayout.BeginHorizontal();
//                 GUILayout.Label("Frequency", _boldStyle, GUILayout.Width(70));
//                 GUILayout.Label("R_Situ", _boldStyle, GUILayout.Width(70));
//                 GUILayout.Label("Delta R 1", _boldStyle, GUILayout.Width(70));
//                 GUILayout.Label("Delta R 2", _boldStyle, GUILayout.Width(70));
//                 GUILayout.Label("Equivalent Absorption Length", _boldStyle, GUILayout.Width(70));
//                 GUILayout.EndHorizontal();
//                 if (!ReferenceEquals(_wall.ReductionIndex, null))
//                     if (_wall.ReductionIndex.NumFrequency != 0)
//                     {
//                         for (var i = 0; i < freq.NumFrequency; i++)
//                         {
//                             GUILayout.BeginHorizontal();
//                             GUILayout.Label(freq.Tf[i].ToString(), _boldStyle, GUILayout.Width(70));
//                             GUILayout.Label(_wall.ReductionIndex.Tf[i].ToString("0.00"), GUILayout.Width(70));
//                             if (_wall.DeltaRForAdditionalLayer == null || _wall.DeltaRForAdditionalLayer.Length != 0)
//                             {
//                                 GUILayout.Label(
//                                     _wall.DeltaRForAdditionalLayer[0].Tf[i].ToString("0.00"),
//                                     GUILayout.Width(70));
//                                 GUILayout.Label(
//                                     _wall.DeltaRForAdditionalLayer[1].Tf[i].ToString("0.00"),
//                                     GUILayout.Width(70));
//                             }
//
//                             if (_wall.EquivalentAbsorptionCoefficient.NumFrequency != 0)
//                                 GUILayout.Label(
//                                     _wall.EquivalentAbsorptionCoefficient.Tf[i].ToString("0.00"),
//                                     GUILayout.Width(70));
//                             GUILayout.EndHorizontal();
//                         }
//
//                         if (GUILayout.Button("Export to Database"))
//                             _wall.ExportReductionIndex2Database();
//                     }
//
//                 if (GUILayout.Button("Calculate Reduction Index"))
//                     _wall.CalcReductionIndexPatches();
//
//                 break;
//         }
//     }
//
//     private void DrawCurveR(TransferFunction tf)
//     {
//         GUILayout.BeginHorizontal();
//         EditorGUILayout.CurveField(tf.Curve, GUILayout.MinHeight(100));
//         MinMaxNextToCurve(tf.Min, tf.Max);
//         GUILayout.EndHorizontal();
//     }
//
//     private void DrawAttachedWalls()
//     {
//         GUILayout.Label("", GUILayout.Height(5));
//         GUILayout.Label("Attached Junctions", _boldStyle);
//         // var junctionTypes = wall.AttachedJunctions.JunctionTypes;
//         for (var i = 0; i < _wall.AttachedJunctions.NumberOfJunctions; i++)
//         {
//             GUILayout.BeginHorizontal();
//             if (_wall.AttachedJunctions[i] != null)
//             {
//                 _wall.AttachedJunctions[i].FinishAttachedWalls();
//                 EditorGUI.BeginDisabledGroup(true);
//                 EditorGUILayout.ObjectField("", _wall.AttachedJunctions[i].gameObject, typeof(GameObject), true,
//                     GUILayout.MinWidth(100));
//                 EditorGUI.EndDisabledGroup();
//                 GUILayout.Label("", GUILayout.Width(5));
//                 GUILayout.Label("Type", GUILayout.Width(55));
//                 GUILayout.Label(_wall.AttachedJunctions[i].JunctionTypeString, GUILayout.Width(100));
//             }
//
//             GUILayout.EndHorizontal();
//         }
//     }
//
//     private void DrawPatches()
//     {
//         GUILayout.Label("", GUILayout.Height(5));
//         GUILayout.Label("Patches Infos", _boldStyle);
//
//         if (!_wall.isPatch)
//         {
//             GUILayout.BeginHorizontal();
//             GUILayout.Label("Number of Patches", GUILayout.MinWidth(120));
//             EditorGUILayout.LabelField(_wall.NumOfPatches.ToString(), GUILayout.Width(150));
//             GUILayout.EndHorizontal();
//         }
//         else
//         {
//             _wall.patchIsActive = GUILayout.Toggle(_wall.patchIsActive, "Is Active");
//             GUILayout.BeginHorizontal();
//             EditorGUI.BeginDisabledGroup(true);
//             EditorGUILayout.ObjectField("", _wall.parent, typeof(GameObject), true,
//                 GUILayout.MinWidth(100));
//             EditorGUI.EndDisabledGroup();
//             GUILayout.Label("", GUILayout.Width(5));
//             var vec = _wall.parent.BoundVector;
//             GUILayout.Label(vec.x.ToString("0.00m x") + vec.y.ToString(" 0.00m x") + vec.z.ToString(" 0.00m"),
//                 GUILayout.Width(150));
//             GUILayout.EndHorizontal();
//         }
//
//         for (var i = 0; i < _wall.NumOfPatches; i++)
//         {
//             GUILayout.BeginHorizontal();
//             if (_wall.patches[i] != null)
//             {
//                 _wall.patches[i].patchIsActive = GUILayout.Toggle(_wall.patches[i].patchIsActive, "");
//
//                 EditorGUI.BeginDisabledGroup(true);
//                 EditorGUILayout.ObjectField("", _wall.patches[i], typeof(GameObject), true,
//                     GUILayout.MinWidth(100));
//                 EditorGUI.EndDisabledGroup();
//                 GUILayout.Label("", GUILayout.Width(5));
//                 var vec = _wall.patches[i].BoundVector;
//                 GUILayout.Label(
//                     vec.x.ToString("0.00m x") + vec.y.ToString(" 0.00m x") + vec.z.ToString(" 0.00m"),
//                     GUILayout.Width(150));
//             }
//
//             GUILayout.EndHorizontal();
//         }
//     }
//
//     private void DrawAdditionalLayers()
//     {
//         GUILayout.Label("", GUILayout.Height(5));
//         GUILayout.BeginHorizontal();
//         _wall.ActivateAdditionalLayer = GUILayout.Toggle(_wall.ActivateAdditionalLayer, "", GUILayout.Width(10));
//         GUILayout.Label("Additional Layer", _boldStyle);
//         if (GUILayout.Button("+", GUILayout.Width(20)))
//             AddAdditionalLayer();
//         GUILayout.EndHorizontal();
//         if (_wall.ActivateAdditionalLayer)
//             for (var i = 0; i < _wall.additionalLayers.Count; i++)
//             {
//                 if (_wall.additionalLayers[i] != null)
//                 {
//                     GUILayout.BeginHorizontal();
//                     _wall.additionalLayers[i].material = (MaterialAddLayerData) EditorGUILayout.ObjectField("",
//                         _wall.additionalLayers[i].material, typeof(MaterialAddLayerData), true, GUILayout.MinWidth(100));
//                     if (GUILayout.Button("X", GUILayout.Width(20)))
//                     {
//                         RemoveAdditionalLayer(i);
//                         break;
//                     }
//
//                     GUILayout.EndHorizontal();
//                 }
//                 else
//                 {
//                     RemoveAdditionalLayer(i);
//                 }
//
//                 if (_wall.additionalLayers[i] == null)
//                     _wall.additionalLayers.RemoveAt(i);
//
//                 if (_wall.additionalLayers[i].material != null)
//                     AdditionalLayerEditor.DrawAdditionalLayer(_wall.additionalLayers[i]);
//             }
//     }
//
//     private void MinMaxNextToCurve(float min, float max)
//     {
//         GUILayout.BeginVertical();
//         GUILayout.Label(max.ToString("0.00"),
//             GUILayout.MinHeight(90), GUILayout.Width(50));
//         GUILayout.Label(min.ToString("0.00"),
//             GUILayout.MinHeight(10), GUILayout.Width(50));
//         GUILayout.EndVertical();
//     }
//
//     private void DrawMaterialStructure()
//     {
//         GUILayout.Label("", GUILayout.Height(5));
//         GUILayout.Label("Material Structure Settings", _boldStyle);
//
//         _wall.materialStructure = (MaterialStructureData) EditorGUILayout.ObjectField("", _wall.materialStructure,
//             typeof(MaterialStructureData), true, GUILayout.MinWidth(100));
//
//         if (_wall.materialStructure != null)
//         {
//             GUILayout.BeginHorizontal();
//             GUILayout.Label("Density", GUILayout.MinWidth(120));
//             EditorGUILayout.LabelField(_wall.materialStructure.density.ToString("0.00") + " kg/m3",
//                 GUILayout.Width(150));
//             GUILayout.EndHorizontal();
//
//             GUILayout.BeginHorizontal();
//             GUILayout.Label("Quasilong Phase Velocity", GUILayout.MinWidth(120));
//             EditorGUILayout.LabelField(_wall.materialStructure.quasiLongPhaseVelocity.ToString("0.00") + " m/s",
//                 GUILayout.Width(150));
//             GUILayout.EndHorizontal();
//
//             GUILayout.BeginHorizontal();
//             GUILayout.Label("InternalLossFactor", GUILayout.MinWidth(120));
//             EditorGUILayout.LabelField(_wall.materialStructure.internalLossFactor.ToString("0.000"),
//                 GUILayout.Width(150));
//             GUILayout.EndHorizontal();
//         }
//     }
//
//     private void DrawGeometricalInfos()
//     {
//         GUILayout.Label("", GUILayout.Height(5));
//         GUILayout.Label("Geometrical Infos", _boldStyle);
//         _wall.useBoundManually = EditorGUILayout.Toggle("Manual Input", _wall.useBoundManually);
//         if (_wall.useBoundManually)
//             _wall.boundVectorManual = EditorGUILayout.Vector3Field("Wall Dimensions", _wall.BoundVector);
//         else
//             EditorGUILayout.Vector3Field("Wall Dimensions", _wall.BoundVector);
//
//
//         EditorGUILayout.Vector3Field("Wall Position", _wall.Position);
//         if (_wall.useBoundManually)
//             _wall.wallNormalVectorManueal = EditorGUILayout.Vector3Field("Wall Normal", _wall.wallNormalVectorManueal);
//         else
//             EditorGUILayout.Vector3Field("Wall Normal", _wall.WallNormalVector);
//
//         if (_wall.materialStructure != null)
//         {
//             GUILayout.BeginHorizontal();
//             GUILayout.Label("Mass", GUILayout.MinWidth(120));
//             EditorGUILayout.LabelField(_wall.Mass.ToString("0.00") + " kg/m2", GUILayout.Width(150));
//             GUILayout.EndHorizontal();
//
//             GUILayout.BeginHorizontal();
//             GUILayout.Label("CriticalFrequency", GUILayout.MinWidth(120));
//             EditorGUILayout.LabelField(_wall.CriticalFrequency.ToString("0.00") + " Hz", GUILayout.Width(150));
//             GUILayout.EndHorizontal();
//         }
//     }
//
//     private void AddAdditionalLayer()
//     {
//         var newChild = GameObject.CreatePrimitive(PrimitiveType.Cube);
//         newChild.AddComponent<AdditionalLayer>();
//         newChild.name = "AdditionalLayer";
//         newChild.transform.parent = _wall.transform;
//         _wall.additionalLayers.Add(newChild.GetComponent<AdditionalLayer>());
//     }
//
//     private void RemoveAdditionalLayer(int index)
//     {
//         if (_wall.additionalLayers[index] != null)
//             DestroyImmediate(_wall.additionalLayers[index].gameObject);
//         _wall.additionalLayers.RemoveAt(index);
//     }
// }