﻿// using System.Globalization;
// using BA.BACore;
// using UnityEditor;
// using UnityEngine;
//
//
// [CustomEditor(typeof(JunctionProperties))]
// public class JunctionPropertiesEditor : Editor
// {
//     private JunctionProperties _junction;
//     private GUIStyle _headerStyle;
//
//     private void OnEnable()
//     {
//         _junction = (JunctionProperties) target;
//         _junction.FinishAttachedWalls();
//
//         _headerStyle = new GUIStyle {fontStyle = FontStyle.Bold};
//     }
//
//     /// <summary>
//     /// overrides the Inspector view in the Editor mode
//     /// </summary>
//     public override void OnInspectorGUI()
//     {
//         _headerStyle = new GUIStyle();
//         _headerStyle.fontStyle = FontStyle.Bold;
//
//         // Geometrical infos
//         GUILayout.Label("", GUILayout.Height(5));
//         GUILayout.Label("Geometrical Infos", _headerStyle);
//
//         EditorGUILayout.Vector3Field("Wall Position", _junction.Position);
//         EditorGUILayout.Vector3Field("Junction Normal", _junction.Normal);
//
//         GUILayout.BeginHorizontal();
//         GUILayout.Label("Junction Length", GUILayout.MinWidth(120));
//         EditorGUILayout.FloatField(_junction.JunctionLength, GUILayout.Width(150));
//         GUILayout.EndHorizontal();
//
//         GUILayout.BeginHorizontal();
//         GUILayout.Label("Junction Type", GUILayout.MinWidth(120));
//         EditorGUILayout.LabelField(_junction.JunctionTypeString, GUILayout.Width(150));
//         GUILayout.EndHorizontal();
//
//         // Attached Junctions
//         GUILayout.Label("", GUILayout.Height(5));
//         GUILayout.Label("Attached Walls", _headerStyle);
//         for (var i = 0; i < _junction.AttachedWalls.NumberOfWalls; i++)
//         {
//             GUILayout.BeginHorizontal();
//             if (_junction.AttachedWalls[i] != null)
//             {
//                 EditorGUILayout.ObjectField("", _junction.AttachedWalls[i].gameObject, typeof(GameObject), true,
//                     GUILayout.MinWidth(100));
//                 GUILayout.Label("", GUILayout.Width(10));
//                 GUILayout.Label("materialStructure", GUILayout.Width(50));
//                 if (_junction.AttachedWalls[i].materialStructure == null)
//                     _junction.AttachedWalls[i].OnEnable();
//                 EditorGUILayout.TextField(_junction.AttachedWalls[i].materialStructure.type, GUILayout.Width(100));
//             }
//
//             GUILayout.EndHorizontal();
//         }
//     }
// }