﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using FFTWSharp;
using log4net;
using UnityEngine;

namespace BA.BACore
{
    /// <summary>
    /// Implementation of new Approach Outdoor considering Direct and reverberation in Source and Receiver room
    /// Fast realtime update implementation
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    [Serializable]
    public class AcOutdoorStaticBakedRealtime : AcNewApproachPartition
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(AcOutdoorStaticBakedRealtime));

        private float[] revReceiverTime;
        private float[][][] rtReceiver;
        private float[][][] rtSource;
        private int[][] riSamplesDelay;
        private int[][] rjSamplesDelay;

        private float[][][] irReceiverRoomFreq;

        public float deltaZ;

        /**< length of step of moving Source in meter */
        public float maxLengthZ;

        /**< total length of moving Source in meter */

        private Vector3 oldSourcePos;
        private SoundSourceMover sourceMover;
        public Vector3 startPos;

        /**< defines the start position of moving Sound source (baked or realtime) */
        private Vector3 soundPos;

        private Quaternion soundOrientation;
        private Vector3[][][] ssPartitionPatches;

        private readonly float[] irLeftFreq = new float[44100 * 2];
        private readonly float[] irRightFreq = new float[44100 * 2];
        private readonly float[] irLeftTime = new float[44100];
        private readonly float[] irRightTime = new float[44100];

        private GCHandle ifftInputLeftHandle;
        private GCHandle ifftResultLeftHandle;
        private IntPtr ifftPlanLeft;

        // IFFT Right
        private GCHandle ifftInputRightHandle;
        private GCHandle ifftResultRightHandle;
        private IntPtr ifftPlanRight;

        private string filePath;
        public bool activateSoundInsulation = true;

        /**< if checked sound insulation is added (baked or realtime) */
        public bool binauralReverberation = false;

        /**< if checked reverberation is binaural (baked or realtime) */
        private int logId = 1;

        private float[][][] irSSFreq = new float[2][][];
        private byte[] byteBuffer = new byte[88200 * sizeof(float)];
        private FftRealtimeR2C fftRealtimeUpdateMonoIrs;

        public override AuralisationMethod AuralisationMethod => AuralisationMethod.OutdoorModelStaticBakedRealTime;

        /// <summary>
        /// Implementation of Initialisation with Init BuildingAcoustics Array from FilterManager
        /// </summary>
        /// <param name="bas">buildingAcoustics Array from FilterManager</param>
        public override void Init()
        {
            moduleName = "AcOutdoorStaticBakedRealtime";
            CultureInfo.CreateSpecificCulture("en-GB");
            init = true;
            oldSourcePos = Source.position;

            var postfix = "";
            if (binauralReverberation)
                postfix += "-binRev";
            filePath =
                $"{BaSettings.Instance.RootPath}/Resources/Sounds/OutdoorStaticBakedRealTime/Irs{postfix}";
            InitIfftIr();
            fftRealtimeUpdateMonoIrs = new FftRealtimeR2C(FilterResolution);
        }

        /// <summary>
        /// Implementation of Calculate pre-calculation and and set AudioRenderer
        /// </summary>
        /// <param name="ar">AudioRenderer from FilterManager</param>
        protected override void OfflineCalculation(AudioRender ar)
        {
            ForceReceiverUpdate = true;
            rtReceiver = new float[buildingAcoustics.Length][][];
            irReceiverRoomFreq = new float[buildingAcoustics.Length][][];
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            {
                var numPatches = buildingAcoustics[iBa].NumOfPatches;
                rtReceiver[iBa] = new float[2 * numPatches][];
                irSSFreq[iBa] = new float[numPatches][];
                irReceiverRoomFreq[iBa] = new float[buildingAcoustics[iBa].NumOfPatches][];
                for (var iPatch = 0; iPatch < buildingAcoustics[iBa].NumOfPatches; iPatch++)
                {
                    irSSFreq[iBa][iPatch] = new float[FilterResolution * 2];
                    rtReceiver[iBa][2 * iPatch] = new float[FilterResolution * 2];
                    rtReceiver[iBa][2 * iPatch + 1] = new float[FilterResolution * 2];
                    irReceiverRoomFreq[iBa][iPatch] = new float[FilterResolution * 2];
                }

                buildingAcoustics[iBa].ProcessAll();
                SoundSourceObjects[0].Init(FilterResolution);
                SoundReceiverObject.Init(FilterResolution);
            }

            var constFactorRev = Mathf.Sqrt(4 / buildingAcoustics[0].receiverRoom.Properties.EquivalentAbsorptionArea);
            revReceiverTime = buildingAcoustics[0].receiverRoom.reverberation;
            for (var i = 0; i < revReceiverTime.Length; i++)
                revReceiverTime[i] *= constFactorRev;
            rtSource = new float[buildingAcoustics.Length][][];
            rjSamplesDelay = new int[buildingAcoustics.Length][];

            var neEndPos = sourceMover.StartPos;
            neEndPos.z -= maxLengthZ;
            audioRenderer = ar;
            sourceMover = audioRenderer.SourceMover;
            CalcReceiverUpdate();
            UpdateMonoIrs();
        }

        /// <summary>
        /// Call on Disable
        /// </summary>
        public override void Disable()
        {
            fftRealtimeUpdateMonoIrs.Destroy();
        }


        /// <summary>
        /// Implementation of Realtime Update, called if Source or Receiver Updates
        /// </summary>
        /// <param name="logId">write log file if logId > 1</param>
        protected override void RealTimeCalculation(int logId = 0)
        {
            var sw = new Stopwatch();
            sw.Start();
            ReadIrsFromBinaryAndUpdate(soundPos, audioRenderer);
            sw.Stop();
            FilterUpdate = sw.Elapsed.TotalMilliseconds;
        }

        /// <summary>
        /// Implementation of Update Receiver Data for filter Construction if Receiver Update
        /// </summary>
        /// <returns>Receiver Update</returns>
        protected override bool RealTimeReceiverUpdate()
        {
            var sw = new Stopwatch();
            sw.Start();
            var posUpdate = false;


            if (SoundReceiverObject.SoundObjectMove() | ForceReceiverUpdate)
            {
                UpdateMonoIrs();
                ForceReceiverUpdate = true;
            }

            if (SoundReceiverObject.SoundObjectRotate() | ForceReceiverUpdate)
                for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
                {
                    //Debug.Log("Receiver Update " + iBa);
//                rtReceiver[iBa] = buildingAcoustics[iBa].receiverRoom.GetJustHrtfForPatches(filterLength);
                    SoundReceiverObject.GetHrtfsForAllAddSs(rtReceiver[iBa],
                        buildingAcoustics[iBa].partition.SecondarySourcesPosPatches);
                    posUpdate = true;
                }

            ForceReceiverUpdate = false;
            sw.Stop();
            ReceiverUpdate = sw.Elapsed.TotalMilliseconds;
            return posUpdate;
        }

        /// <summary>
        /// Implementation of Update Source Data for filter Construction if Source Update
        /// </summary>
        /// <returns>Source Update</returns>
        protected override bool RealTimeSourceUpdate()
        {
            var sw = new Stopwatch();
            sw.Start();
            soundPos = Source.position;
            if (soundPos == oldSourcePos && !ForceSourceUpdate)
                return false;

            var fileName = $"{filePath}/irMono-SourcePos_{Vec32Str(soundPos)}.dat";
            if (File.Exists(fileName))
            {
                using (var br = new BinaryReader(File.Open(fileName, FileMode.Open)))
                {
                    var offset = br.ReadInt32();
                    for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
                    {
                        if (!buildingAcoustics[iBa].IsVisible)
                            continue;
                        for (var iPatch = 0; iPatch < buildingAcoustics[iBa].NumOfPatches; iPatch++)
                        {
                            // br.Read(byteBuffer, pointer, 88200 * sizeof(float));
                            byteBuffer = br.ReadBytes(88200 * sizeof(float));
                            Buffer.BlockCopy(byteBuffer, 0, irSSFreq[iBa][iPatch], 0, byteBuffer.Length);
                        }
                    }
                }

                ForceSourceUpdate = true;
                oldSourcePos = soundPos;
            }
            else
            {
                Log.Warn("Could not find " + fileName);
                return false;
            }

            sw.Stop();
            SourceUpdate = sw.Elapsed.TotalMilliseconds;

            return true;
        }

        private float[][][] CalcReceiverUpdate()
        {
            var rtReceiverMono = new float[buildingAcoustics.Length][][];
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            {
                SoundReceiverObject.SoundObjectChanged();
                rtReceiverMono[iBa] = SoundReceiverObject
                    .GetHrtfMonoForSsPatches(buildingAcoustics[iBa].partition.SecondarySourcesPosPatches, FilterResolution,
                        out rjSamplesDelay[iBa]);
            }

            ForceReceiverUpdate = false;
            return rtReceiverMono;
        }

        private bool CalcSourceUpdate()
        {
            var posUpdate = false;
            if (riSamplesDelay == null || riSamplesDelay.Length != buildingAcoustics.Length)
                riSamplesDelay = new int[buildingAcoustics.Length][];
            logId++;
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            {
                //Debug.Log("Receiver Update " + iBa);
                //rtSource[iBa] = buildingAcoustics[iBa].sourceRoom
                //    .GetSourceFiForPatches(filterLength, out riSamplesDelay[iBa], true, logId);

                var partition = buildingAcoustics[iBa].partition;
                var numPatches = buildingAcoustics[iBa].NumOfPatches;
                for (var iPatch = 0; iPatch < numPatches; iPatch++)
                {
                    if (!buildingAcoustics[iBa].partition.patchesHandler.IsActive[iPatch])
                        continue;
                    var siPatches = Mathf.Sqrt(buildingAcoustics[iBa].partition.Patches[iPatch].Geometry.Area);
                    var wallUp = partition.Geometry.Normal;
                    rtSource[iBa][iPatch] = SoundSourceObjects[0].CalcFiForOneWall(FilterResolution, soundPos,
                        wallUp, soundOrientation, ssPartitionPatches[iBa][iPatch], siPatches,
                        out riSamplesDelay[iBa][iPatch], false, iPatch, logId);
                }

                posUpdate = true;
            }

            ForceSourceUpdate = false;
            return posUpdate;
        }

        private void UpdateMonoIrs()
        {
            var rtReceiverMono = CalcReceiverUpdate();

            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            for (var iPatch = 0; iPatch < buildingAcoustics[iBa].NumOfPatches; iPatch++)
            {
                var irMono = new float[FilterResolution];

                // Add Direct Energy
                rtReceiverMono[iBa][iPatch].CopyTo(irMono, 0);

                // Add Reverberation
                for (var n = 0;
                    n < Mathf.Min(revReceiverTime.Length, FilterResolution - rjSamplesDelay[iBa][iPatch]);
                    n++)
                    irMono[n + rjSamplesDelay[iBa][iPatch]] += revReceiverTime[n];

                fftRealtimeUpdateMonoIrs.RunFft(irMono, irReceiverRoomFreq[iBa][iPatch]);
            }
        }

        private void ReadIrsFromBinaryAndUpdate(Vector3 sourcePos, AudioRender ar)
        {
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            {
                if (!buildingAcoustics[iBa].IsVisible)
                    continue;
                for (var iPatch = 0; iPatch < buildingAcoustics[iBa].NumOfPatches; iPatch++)
                {
                    var irSsFreqRef = irSSFreq[iBa][iPatch];
                    var irReceiverRoomFreqRef = irReceiverRoomFreq[iBa][iPatch];
                    var rtReceiverLeftRef = rtReceiver[iBa][2 * iPatch];
                    var rtReceiverRightRef = rtReceiver[iBa][2 * iPatch + 1];
                    for (var n = 0; n < irSsFreqRef.Length; n += 2)
                    {
                        var irSsReal = irSsFreqRef[n];
                        var irSsImag = irSsFreqRef[n + 1];
                        var irReceiverRoomFreqReal = irReceiverRoomFreqRef[n];
                        var irReceiverRoomFreqImag = irReceiverRoomFreqRef[n + 1];
                        var rtLReal = rtReceiverLeftRef[n];
                        var rtLImag = rtReceiverLeftRef[n + 1];
                        var rtRReal = rtReceiverRightRef[n];
                        var rtRImag = rtReceiverRightRef[n + 1];

                        var monoReal = irSsReal * irReceiverRoomFreqReal - irSsImag * irReceiverRoomFreqImag;
                        var monoImag = irSsReal * irReceiverRoomFreqImag + irSsImag * irReceiverRoomFreqReal;


                        if (iBa == 0 && iPatch == 0)
                        {
                            irLeftFreq[n] = monoReal * rtLReal - monoImag * rtLImag;
                            irLeftFreq[n + 1] = monoReal * rtLImag + monoImag * rtLReal;
                            irRightFreq[n] = monoReal * rtRReal - monoImag * rtRImag;
                            irRightFreq[n + 1] = monoReal * rtRImag + monoImag * rtRReal;
                        }
                        else
                        {
                            irLeftFreq[n] += monoReal * rtLReal - monoImag * rtLImag;
                            irLeftFreq[n + 1] += monoReal * rtLImag + monoImag * rtLReal;
                            irRightFreq[n] += monoReal * rtRReal - monoImag * rtRImag;
                            irRightFreq[n + 1] += monoReal * rtRImag + monoImag * rtRReal;
                        }
                    }
                }
            }

            fftwf.execute(ifftPlanLeft);
            fftwf.execute(ifftPlanRight);
            ar.UpdateFilter(irLeftTime, irRightTime);
        }

        private void InitIfftIr()
        {
            ifftInputLeftHandle = GCHandle.Alloc(irLeftFreq, GCHandleType.Pinned);
            ifftResultLeftHandle = GCHandle.Alloc(irLeftTime, GCHandleType.Pinned);
            ifftPlanLeft = fftwf.dft_c2r_1d(FilterResolution, ifftInputLeftHandle.AddrOfPinnedObject(),
                ifftResultLeftHandle.AddrOfPinnedObject(), fftw_flags.Estimate);

            ifftInputRightHandle = GCHandle.Alloc(irRightFreq, GCHandleType.Pinned);
            ifftResultRightHandle = GCHandle.Alloc(irRightTime, GCHandleType.Pinned);
            ifftPlanRight = fftwf.dft_c2r_1d(FilterResolution, ifftInputRightHandle.AddrOfPinnedObject(),
                ifftResultRightHandle.AddrOfPinnedObject(), fftw_flags.Estimate);
        }

        private string Vec32Str(Vector3 vec)
        {
            CultureInfo.CurrentCulture = new CultureInfo("en-GB");
            return vec.x.ToString("000.0") + "_" + vec.y.ToString("000.0") + "_" + vec.z.ToString("000.0");
        }

        /// <summary>
        /// Log the current binaural ir and the receiver room ir without Hrtf to binary files 
        /// </summary>
        public void LogBirSum()
        {
            var receiverString = Vec32Str(Receiver.position) + "-Rot" + Vec32Str(Receiver.rotation.eulerAngles);
            var fileName = $"{filePath}/BianuralLog-ReceiverPos_{receiverString}.dat";
            var irLog = new float[2][];
            irLog[0] = irLeftFreq;
            irLog[1] = irRightFreq;
            if (File.Exists(fileName))
                return;

            using (var file = File.Open(fileName, FileMode.Create))
            {
                using (var writer = new BinaryWriter(file))
                {
                    WriteIrsToBinary(writer, irLog);
                }
            }

            fileName = $"{filePath}/irReceiver-ReceiverPos_{receiverString}.dat";
            using (var file = File.Open(fileName, FileMode.Create))
            {
                using (var writer = new BinaryWriter(file))
                {
                    WriteIrsToBinary(writer, FftHelper.Ifft(irReceiverRoomFreq), 0, true);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bas"></param>
        /// <param name="writeLog"></param>
        public void LogMovingSourceBrir(bool writeLog = false)
        {
            Log.Info("Start LogMovingOutdoorSourceSsIr");
            LoggingConfiguration.ConfigureViaFile();
            LogRealtime = false;
            DataLogger.DebugData = false;
            Init();

            ssPartitionPatches = new Vector3[buildingAcoustics.Length][][];
            riSamplesDelay = new int[buildingAcoustics.Length][];
            rtSource = new float[buildingAcoustics.Length][][];
            rtReceiver = new float[buildingAcoustics.Length][][];
            SoundSourceObjects[0].Init(FilterResolution);
            SoundReceiverObject.Init(FilterResolution);
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            {
                buildingAcoustics[iBa].ProcessAll();
                ssPartitionPatches[iBa] = buildingAcoustics[iBa].partition.SecondarySourcesPosPatches;
                var numPatches = buildingAcoustics[iBa].partition.patchesHandler.NumOfPatches;
                riSamplesDelay[iBa] = new int[numPatches];
                rtSource[iBa] = new float[numPatches][];
                rtReceiver[iBa] = new float[numPatches][];
            }

            var tauPatchesFreqComplex = GetTauPatchesFreqComplex(true);
            rjSamplesDelay = new int[buildingAcoustics.Length][];
            ForceSourceUpdate = true;
            ForceReceiverUpdate = true;
            this.logId = 1;
            soundPos = startPos;
            soundPos.z += 5f;
            soundOrientation = sourceMover.transform.rotation;

            CalcReceiverUpdate();
            CalcSourceUpdate();


            var logId = writeLog ? 0 : -1;
            //var oldStart = SourceMover.startPos;
            DataLogger.DebugData = false;
            // var oldEnd = SourceMover.endPos;
            Directory.CreateDirectory(filePath);

            if (!activateSoundInsulation)
                for (var iBa = 0; iBa < tauPatchesFreqComplex.Length; iBa++)
                for (var iPatch = 0; iPatch < tauPatchesFreqComplex[iBa].Length; iPatch++)
                {
                    tauPatchesFreqComplex[iBa][iPatch] = new float[FilterResolution * 2];
                    for (var n = 0; n < tauPatchesFreqComplex[iBa][iPatch].Length; n += 2)
                        tauPatchesFreqComplex[iBa][iPatch][n] = 1;
                }

            while (true)
            {
                soundPos.z -= deltaZ;
                var sourcePosString = Vec32Str(soundPos);
                sourceMover.transform.position = soundPos;
                CalcSourceUpdate();

                // Mono SS Partiton
                var fileName = $"{filePath}/irMono-SourcePos_{sourcePosString}.dat";
                if (File.Exists(fileName))
                    continue;
                var irSingle = CalcNewApproachOutdoorSsMono(tauPatchesFreqComplex);
                using (var file = File.Open(fileName, FileMode.Create))
                {
                    using (var writer = new BinaryWriter(file))
                    {
                        WriteIrsToBinary(writer, irSingle, 0, true);
                    }
                }

                if (soundPos.z < startPos.z - maxLengthZ - 5f)
                    break;

                if (writeLog)
                    logId++;
            }
        }

        private float[][][] GetTauPatchesFreqComplex(bool logTau = false)
        {
            var baLength = buildingAcoustics.Length;
            UpdateBaVisible();
            var tauSqrt1PhaseFreqComplex = new float[baLength][][];
            for (var iBa = 0; iBa < baLength; iBa++)
            {
                var partition = buildingAcoustics[iBa].partition;
                var numPatches = partition.patchesHandler.NumOfPatches;
                tauSqrt1PhaseFreqComplex[iBa] = new float[numPatches][];
                for (var iPatch = 0; iPatch < numPatches; iPatch++)
                {
                    var tau3RdOctaveFreq = 10 ^ (partition.patchesHandler.ReductionIndexPatches[iPatch] / -10f);
                    var tauInterpolatedFreq = CalcTauExtrapolatedFrom3RdOctaveTau(tau3RdOctaveFreq, FilterResolution);
                    tauSqrt1PhaseFreqComplex[iBa][iPatch] =
                        CalcSqrtTauComplexFromTauInterpolatedEven(tauInterpolatedFreq.Tf, LogName, 1, 2);
                    if (logTau)
                    {
                        DataLogger.DebugData = true;
                        DataLogger.Log("interpol.tau3RdOctaveFreq{" + (iBa + 1) + ",1}(:," + (iPatch + 1) + ")",
                            tau3RdOctaveFreq);
                        DataLogger.Log("interpol.tauInterpolatedFreq{" + (iBa + 1) + ",1}(:," + (iPatch + 1) + ")",
                            tauInterpolatedFreq);
                        DataLogger.Log("interpol.tauSqrt1PhaseFreqComplex{" + (iBa + 1) + ",1}(:," + (iPatch + 1) + ")",
                            tauSqrt1PhaseFreqComplex[iBa][iPatch]);
                        DataLogger.DebugData = false;
                    }
                }
            }

            if (logTau)
            {
                DataLogger.DebugData = true;
                var interpolationFreqBand = BaSettings.Instance.GetInterpolatedFrequencyBand(FilterResolution / 2);
                DataLogger.Log("interpol.fInterpolation", interpolationFreqBand);
                var thirdOctaveFrequencyBand = BaSettings.Instance.ThirdOctaveFrequencyBandNorm;
                var frequencyBand = BaSettings.Instance.ThirdOctaveFrequencyBandShort;
                var extendedFrequencyBand = BaSettings.Instance.ThirdOctaveFrequencyBandExtended;
                DataLogger.Log("interpol.thirdOctaveFrequencyBand", thirdOctaveFrequencyBand);
                DataLogger.Log("interpol.frequencyBand", frequencyBand);
                DataLogger.Log("interpol.extendedFrequencyBand", extendedFrequencyBand);
                DataLogger.DebugData = false;
            }

            return tauSqrt1PhaseFreqComplex;
        }

        private float[][][] CalcNewApproachOutdoorSsMono(float[][][] tauPatchesFreqComplex, int logId = 0,
            bool logAll = false)
        {
            // Start Partition Wall
            var irSs = new float[buildingAcoustics.Length][][];
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            {
                var patchesActive = buildingAcoustics[iBa].partition.patchesHandler.IsActive;
                irSs[iBa] = new float[tauPatchesFreqComplex[iBa].Length][];

                for (var iPatch = 0; iPatch < tauPatchesFreqComplex[iBa].Length; iPatch++)
                {
                    if (!patchesActive[iPatch])
                        continue;
                    if (!buildingAcoustics[iBa].IsVisible)
                    {
                        irSs[iBa][iPatch] = new float[tauPatchesFreqComplex[iBa][iPatch].Length / 2];
                    }
                    else
                    {
                        var factor = SourceSoundPowerNewApproach * buildingAcoustics[iBa].partition.Patches[iPatch].Geometry.Area /
                                     (buildingAcoustics[iBa].partition.Geometry.Area * 4 * Mathf.PI);
                        var irPatchFreqComplex =
                            ComplexFloats.Multiply(rtSource[iBa][iPatch], tauPatchesFreqComplex[iBa][iPatch],
                                Mathf.Sqrt(factor));

                        if (logAll)
                        {
                            DataLogger.Log(
                                LogName + "irPatchFreqComplex{" + (iBa + 1) + "," + (iPatch + 1) + "}(:," + (logId + 1) +
                                ")", irPatchFreqComplex);
                            DataLogger.Log(
                                LogName + "rtSourceFreqComplex{" + (iBa + 1) + "," + (iPatch + 1) + "}(:," + (logId + 1) +
                                ")", rtSource[iBa][iPatch]);
                            DataLogger.Log(
                                LogName + "factor{" + (iBa + 1) + "," + (iPatch + 1) + "}(:," + (logId + 1) + ")",
                                Mathf.Sqrt(factor));
                        }

                        irSs[iBa][iPatch] = irPatchFreqComplex;
                    }

                    if (logId >= 0)
                        DataLogger.Log(LogName + "irSs{" + (iBa + 1) + "," + (iPatch + 1) + "}(:," + (logId + 1) + ")",
                            irSs[iBa][iPatch]);
                }
            }

            return irSs;
        }

        private void WriteIrsToBinary(BinaryWriter bw, float[][][] array, int offset = 0, bool ignoreWarn = false)
        {
            bw.Write(offset);
            for (var iBa = 0; iBa < array.Length; iBa++)
            for (var iPatch = 0; iPatch < array[iBa].Length; iPatch++)
            {
                if (!ignoreWarn && array[iBa][iPatch].Length != 44100)
                    Log.Error("Invalid Length of Ir for writing to Binary.");

                for (var i = 0; i < array[iBa][iPatch].Length; i++) bw.Write(array[iBa][iPatch][i]);
            }
        }

        private void WriteIrsToBinary(BinaryWriter bw, float[][] array, int offset = 0)
        {
            bw.Write(offset);
            for (var i = 0; i < array.Length; i++)
            {
                if (array[i].Length != 44100)
                    Log.Error("Invalid Length of Ir for writing to Binary.");

                for (var n = 0; n < array[i].Length; n++) bw.Write(array[i][n]);
            }
        }
    }
}