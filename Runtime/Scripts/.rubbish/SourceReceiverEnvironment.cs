using System;
using System.Runtime.InteropServices;
using log4net;
using UnityEngine;
using UnityEngine.Serialization;

namespace BA.BACore
{
    public class SourceReceiverEnvironment : MonoBehaviour
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(SourceReceiverEnvironment));
        /// <summary>
        /// is this MonoBehaviour is active
        /// </summary>
        public bool isActive = true;

        
        [Subtitle] public string sourceReceiverSettings;
        
        /// <summary>
        /// path to directivity file for the source (DAFF file, ir)
        /// </summary>
        public SoundSignalSources soundSourceObjects;
        
        public SoundReceiverObject soundReceiverObject;
        
        public virtual bool IsVisible => true;

        protected BaSettings _baSetting;

        private void Awake()
        {
            _baSetting = FindObjectOfType<BaSettings>();
        }

        public virtual void ProcessAll()
        {
            throw new System.NotImplementedException();
        }
    }
}