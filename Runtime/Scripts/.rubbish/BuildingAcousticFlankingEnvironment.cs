﻿using System;
using System.Diagnostics;
using log4net;
using UnityEngine;
using UnityEngine.Serialization;

namespace BA.BACore
{
    /// <summary>
    /// Manage the Geometry and the Sound Insulation Metric Calculation
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    public class BuildingAcousticFlankingEnvironment : BuildingAcousticEnvironment
    {
        [Subtitle] public string flankingSettings = "";

        /// <summary>
        /// if set Side Walls Manually
        /// </summary>
        public bool setWallsManually = true;

        /// <summary>
        /// list off all wallBehaviours in source room, partition index 0
        /// </summary>
        public WallBehaviour[] sourceWalls;

        /// <summary>>
        /// list off all wallBehaviours in receiver room, partition index 0
        /// </summary>
        public WallBehaviour[] receiverWalls;

        /// <summary>
        /// all global positions of Source Room Walls
        /// </summary>
        public Vector3[] SourcePositions{ get; private set; }

        /// <summary>
        /// all global positions of Receiver Room Walls
        /// </summary>
        public Vector3[] ReceiverPositions { get; private set; }

        /// <summary>
        /// all global positions of secondary sources of Source Room Walls
        /// </summary>
        public Vector3[][] SourcePositionsSs { get; private set; }

        /// <summary>
        /// all global positions of secondary sources of Receiver Room Walls
        /// </summary>
        public Vector3[][] ReceiverPositionsSs { get; private set; }

        /// <summary>
        /// all sources Wall indexes in the array
        /// </summary>
        public int[] SourceWallIndexes { get; private set; }

        /// <summary>
        /// all receiver Wall indexes in the array
        /// </summary>
        public int[] ReceiverWallIndexes { get; private set; }

        /// <summary>
        /// all Partition Wall indexes in the array
        /// </summary>
        public int[] PartitionWallIndexes { get; private set; }

        /// <summary>
        /// Property set if Source Room is Outdoor
        /// </summary>
        private bool IsSourceRoomOutdoor => sourceRoom==null;

        /// <summary>
        /// Property set if Receiver Room is Outdoor
        /// </summary>
        private bool IsReceiverRoomOutdoor => receiverRoom==null;


        private static readonly ILog Log = LogManager.GetLogger(typeof(BuildingAcousticFlankingEnvironment));


        /// <summary>
        /// return if the Partition is visible from the source
        /// </summary>
        public override bool IsVisible
        {
            get
            {
                var norm = partition.Geometry.Normal;
                var wall = Vector3.Dot(norm, partition.Geometry.Position);
                var source = Vector3.Dot(norm, soundSourceObjects.SoundSources[0].transform.position);
                var receiver = Vector3.Dot(norm, soundReceiverObject.transform.position);
                return Math.Abs(Mathf.Sign(source - wall) - Mathf.Sign(wall - receiver)) < float.Epsilon;
            }
        }

        private void OnValidate()
        {
            SetGeometry();
        }

        /// <summary>
        /// Initial Init to set Geometry
        /// </summary>
        public void Awake()
        {
            SetGeometry();
        }

        /// <summary>
        /// Process all rooms 
        /// </summary>
        public override void ProcessAll()
        {
            sourceWalls = null;
            receiverWalls = null;
            var sw = new Stopwatch();
            sw.Start();
            Awake();
            receiverRoom.ProcessAll();
            sourceRoom.ProcessAll();
            sw.Stop();
            Log.Info("Process All for Building Acoustics runs in " + sw.Elapsed.TotalSeconds + " seconds!");
        }

        private new void SetGeometry()
        {
            base.SetGeometry();
            PartitionJunctions = partition.Junctions;

            if (!setWallsManually)
            {
                if (!IsSourceRoomOutdoor) SourceWallIndexes = GetWallIndexesForJunctions(soundSourceObjects.SoundSources[0].transform.position);

                sourceWalls = IsSourceRoomOutdoor ? new WallBehaviour[1] : new WallBehaviour[5];
                receiverWalls = IsReceiverRoomOutdoor ? new WallBehaviour[1] : new WallBehaviour[5];

                ReceiverWallIndexes = GetWallIndexesForJunctions(soundReceiverObject.transform.position);
                sourceWalls[0] = partition;
                receiverWalls[0] = partition;
            }
            else
            {
                sourceWalls = new WallBehaviour[1];
                receiverWalls = new WallBehaviour[1];
                sourceWalls[0] = partition;
                receiverWalls[0] = partition;
                SourceWallIndexes = GetIndexesInts(sourceWalls);
                ReceiverWallIndexes = GetIndexesInts(receiverWalls);
            }

            SourcePositions = new Vector3[sourceWalls.Length];
            ReceiverPositions = new Vector3[receiverWalls.Length];
            SourcePositionsSs = new Vector3[sourceWalls.Length][];
            ReceiverPositionsSs = new Vector3[receiverWalls.Length][];

            SourcePositionsSs[0] = partition.Geometry.SecondarySourcesPosition;
            ReceiverPositionsSs[0] = partition.Geometry.SecondarySourcesPosition;

            SourcePositions[0] = partition.Geometry.Position;
            ReceiverPositions[0] = partition.Geometry.Position;


            PartitionWallIndexes = new int[4];
            for (var i = 0; i < 4; i++)
            {
                PartitionWallIndexes[i] = PartitionJunctions.AttachedWalls[i].FindWallInstanceIndex(partition.GetInstanceID());
                if (setWallsManually)
                    continue;
                if (!IsSourceRoomOutdoor)
                {
                    sourceWalls[i + 1] = PartitionJunctions.AttachedWalls[i].AttachedWallDatas[SourceWallIndexes[i]].WallBehaviour;
                    SourcePositions[i + 1] = sourceWalls[i].Geometry.Position;
                    SourcePositionsSs[i + 1] = sourceWalls[i].Geometry.SecondarySourcesPosition;
                }

                if (!IsReceiverRoomOutdoor)
                {
                    receiverWalls[i + 1] = PartitionJunctions.AttachedWalls[i].AttachedWallDatas[ReceiverWallIndexes[i]].WallBehaviour;
                    ReceiverPositions[i + 1] = receiverWalls[i].Geometry.Position;
                    ReceiverPositionsSs[i + 1] = receiverWalls[i].Geometry.SecondarySourcesPosition;
                }
            }
        }

        /// <summary>
        /// Calculate and return an array to the distances from receiver to the receiver wallBehaviours
        /// </summary>
        /// <returns></returns>
        public float[] CalcReceiverDistanceWalls()
        {
            var distance = new float[ReceiverPositions.Length];
            var receiverPos = soundReceiverObject.transform.position;
            for (var i = 0; i < ReceiverPositions.Length; i++)
                distance[i] = Vector3.Distance(ReceiverPositions[i], receiverPos);
            return distance;
        }


        #region Tau Calculation

        /// <summary>
        /// Get the Secondary sources for Indoor thaden approach
        /// </summary>
        /// <returns></returns>
        public TransferFunction[] GetSecondarySources()
        {
            var tauSecondarySources = new TransferFunction[5];

            var reductionIndexFlanking = GetReductionIndexFlanking();
            tauSecondarySources[0] =
                10 ^ (partition.ReductionIndex.ReductionIndex / -10f);
            for (var i = 0; i < 4; i++)
            {
                tauSecondarySources[0] +=
                    10 ^ (reductionIndexFlanking[i][SourceWallIndexes[i], PartitionWallIndexes[i]] / -10f);
                tauSecondarySources[i + 1] =
                    10 ^ (reductionIndexFlanking[i][SourceWallIndexes[i], ReceiverWallIndexes[i]] / -10f +
                          10) ^ (reductionIndexFlanking[i][PartitionWallIndexes[i], ReceiverWallIndexes[i]] / -10f);
            }

            return tauSecondarySources;
        }

        /// <summary>
        /// Calc the Flanking paths 
        /// </summary>
        /// <returns></returns>
        public TransferPaths[] GetReductionIndexFlanking()
        {
            var reductionIndexFlanking = new TransferPaths[4];
            // for (var i = 0; i < 4; i++)
                // reductionIndexFlanking[i] = PartitionJunctions.AttachedWalls[i].CalcReductionIndexPathsRij(partition.GetComponent<SoundReductionIndexIso>());
            // TODO
            return reductionIndexFlanking;
        }

        private int[] GetIndexesInts(WallBehaviour[] walls)
        {
            var junctionIds = new[] {-1, -1, -1, -1};
            for (var i = 0; i < walls.Length; i++)
            {
                GetJunctionIdByInstanceId(walls[i].gameObject, out var junctionIndex, out var wallIndex);
                junctionIds[junctionIndex] = wallIndex;
                walls[i].Awake();
            }

            return junctionIds;
        }

        private int[] GetWallIndexesForJunctions(Vector3 position)
        {
            // Set all directions for ray-cast in other directions
            var directions = new Vector3[4];
            var normalVectorWall = partition.Geometry.Normal;
            if (Mathf.Abs(normalVectorWall.x) > 1e-3f)
                directions = new[]
                    {Vector3.up, Vector3.down, Vector3.forward, Vector3.back};
            else if (Mathf.Abs(normalVectorWall.y) > 1e-3f)
                directions = new[]
                    {Vector3.right, Vector3.left, Vector3.forward, Vector3.back};
            else if (Mathf.Abs(normalVectorWall.z) > 1e-3f)
                directions = new[]
                    {Vector3.right, Vector3.left, Vector3.up, Vector3.down};
            var ray = new Ray
            {
                origin = position,
            };
            var junctionIds = new[] {-1, -1, -1, -1};
            for (var i = 0; i < 4; i++)
            {
                ray.direction = directions[i];

                if (Physics.Raycast(ray, out var hit, Mathf.Infinity, _baSetting.LayerMaskWall))
                {
                    //GetJunctionIdByName(hit.transform.name, out var junctionIndex, out var wallIndex);
                    // GetJunctionIdByInstanceId(hit.transform.gameObject.GetInstanceID(), out var junctionIndex, out var wallIndex);
                    GetJunctionIdByInstanceId(hit.transform.gameObject,
                        out var junctionIndex, out var wallIndex);
                    junctionIds[junctionIndex] = wallIndex;
                    hit.transform.gameObject.GetComponent<WallBehaviour>().Awake();
                }
                else
                {
                    ray.direction = ray.direction - Vector3.one;
                    if (Physics.Raycast(ray, out hit, Mathf.Infinity, _baSetting.LayerMaskWall))
                    {
                        // GetJunctionIdByName(hit.transform.name, out var junctionIndex, out var wallIndex);
                        // GetJunctionIdByInstanceId(hit.transform.gameObject.GetInstanceID(), out var junctionIndex, out var wallIndex);
                        GetJunctionIdByInstanceId(hit.transform.gameObject,
                            out var junctionIndex, out var wallIndex);
                        junctionIds[junctionIndex] = wallIndex;
                        hit.transform.gameObject.GetComponent<WallBehaviour>().Awake();
                    }
                    else
                    {
                        Log.Error("Wall cannot found in Direction " + ray.direction + "!");
                    }
                }
            }

            // Check indexes
            foreach (var junctionId in junctionIds)
                if (junctionId == -1)
                    Log.Error("Junction Id  " + junctionId + " could not find any Wall!");

            return junctionIds;
        }

        private void GetJunctionIdByInstanceId(GameObject gameObject, out int junctionIndex, out int wallIndex)
        {
            junctionIndex = -1;
            wallIndex = -1;
            for (var j = 0; j < 4; j++)
            {
                var wallIndexTemp =
                    PartitionJunctions.AttachedWalls[j].FindWallInstanceIndex(gameObject.GetInstanceID());
                if (wallIndexTemp != -1)
                {
                    wallIndex = wallIndexTemp;
                    junctionIndex = j;
                    return;
                }
            }

            Log.Error("Can not find Junction Wall " + gameObject.name + " in Junctions");
            //Log.Error("Can not find Junction Wall " + inputName + " in Junctions", GameObject.Find(inputName));
        }


        #endregion

        /// <summary>
        /// Test ProcessAll
        /// </summary>
        public void TestProcessAll()
        {
            LoggingConfiguration.ConfigureViaFile();
            ProcessAll();
        }
    }
}