﻿using System.Diagnostics;
using log4net;
using UnityEngine;

namespace BA.BACore
{
    public class BuildingAcousticPatchesEnvironment : RoomEnvironment
    {
        [Subtitle] public string buildingAcousticSettings;
        private static readonly ILog Log = LogManager.GetLogger(typeof(BuildingAcousticPatchesEnvironment));

        /// <summary>
        /// reference to Partition
        /// </summary>
        public WallPatchesHandler partition;
        /// <summary>
        /// Process all rooms 
        /// </summary>
        public override void ProcessAll()
        {
            var sw = new Stopwatch();
            sw.Start();
            receiverRoom.ProcessAll();
            sourceRoom.ProcessAll();
            sw.Stop();
            Log.Info("Process All for Building Acoustics runs in " + sw.Elapsed.TotalSeconds + " seconds!");
        }
    }
}