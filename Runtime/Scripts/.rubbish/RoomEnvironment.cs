using System.Diagnostics;
using log4net;
using UnityEngine.Serialization;

namespace BA.BACore
{
    public class RoomEnvironment : SourceReceiverEnvironment
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(RoomEnvironment));

        [Subtitle] public string RoomSettings;

        /// <summary>
        /// reference to the source room
        /// </summary>
        public ReverberationCalculator sourceRoom;

        public ReverberationCalculator receiverRoom;

        public override void ProcessAll()
        {

            var sw = new Stopwatch();
            sw.Start();
            receiverRoom.ProcessAll();
            sourceRoom.ProcessAll();
            sw.Stop();
            Log.Info("Process All for Rooms RoomEnvironment runs in " + sw.Elapsed.TotalSeconds + " seconds!");
        }
    }
}