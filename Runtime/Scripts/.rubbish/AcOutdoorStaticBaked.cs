﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using log4net;
using TestMySpline;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace BA.BACore
{
    /// <summary>
    /// Implementation of new Approach Outdoor considering Direct and reverberation in Source and Receiver room
    /// Second try to bake the results beforehand, all Binaural impulse responses are pre-calculated and write in Binary files
    /// not working well - experimental
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    [Serializable]
    public class AcOutdoorStaticBaked : AcNewApproachPartition
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(AcOutdoorStaticBaked));

        private float[] revReceiverTime;
        private int filterLength;
        private float[][][] rtReceiver;
        private float[][][] rtSource;
        private int[][] riSamplesDelay;
        private int[][] rjSamplesDelay;
        private float[][][][] hrirDataLeft;
        private float[][][][] hrirDataRight;

        public float deltaZ;

        /**< length of step of moving Source in meter */
        public float maxLengthZ;

        /**< total length of moving Source in meter */
        public Vector3 startPos;

        /**< defines the start position of moving Sound source (baked or realtime) */

        private Vector3 oldSourcePos;
        private SoundSourceMover sourceMover;
        private Vector3 soundPos;
        private Quaternion soundOrientation;
        private Vector3[][][] ssPartitionPatches;

        private float[] irLeft = new float[44100];
        private float[] irRight = new float[44100];
        private string filePath;
        public bool applyDoppler = false;

        /**< if check doppler is used (baked or realtime) */
        public bool applyReverberation = true;

        /**< if checked reverberation is added (baked or realtime) */
        public bool applyDirect = true;

        /**< if checked direct sound is added (baked or realtime) */
        public bool activateSoundInsulation = true;

        /**< if checked sound insulation is added (baked or realtime) */
        public bool binauralReverberation = false;

        /**< if checked reverberation is binaural (baked or realtime) */
        public bool measuredRev = false;

        /**< if checked measured reverberation is checked (realtime) */
        public bool applyDeltaDi = false;

        /**< if checked delta delay between Patches is applied (baked or realtime) */
        private int logId = 1;


        public override AuralisationMethod AuralisationMethod => AuralisationMethod.OutdoorModelStaticBaked;

        /// <summary>
        /// Implementation of Initialisation with Init BuildingAcoustics Array from FilterManager
        /// </summary>
        /// <param name="bas">buildingAcoustics Array from FilterManager</param>
        public override void Init()
        {
            moduleName = "AcOutdoorStaticBaked";
            CultureInfo.CreateSpecificCulture("en-GB");
            init = true;
            oldSourcePos = Source.position;

            var postfix = "";
            if (binauralReverberation)
                postfix += "-binRev";
            if (!applyDirect)
                postfix += "-noDir";
            if (!applyReverberation)
                postfix += "-noRev";
            if (measuredRev)
                postfix += "-measuredRev";
            if (applyDeltaDi)
                postfix += "-DeltaDi";
            filePath =
                $"{BaSettings.Instance.RootPath}/Resources/Sounds/OutdoorStaticBaked/ReceiverPos_{Vec32Str(Receiver.position)}-Euler_{Vec32Str(Receiver.eulerAngles)}{postfix}";
        }


        /// <summary>
        /// Implementation of Calculate pre-calculation and and set AudioRenderer
        /// </summary>
        /// <param name="ar">AudioRenderer from FilterManager</param>
        protected override void OfflineCalculation(AudioRender ar)
        {
            var neEndPos = sourceMover.StartPos;
            neEndPos.z -= maxLengthZ;
            audioRenderer = ar;
            sourceMover = audioRenderer.SourceMover;
        }


        /// <summary>
        /// Implementation of Realtime Update, called if Source or Receiver Updates
        /// </summary>
        /// <param name="logId">write log file if logId > 1</param>
        protected override void RealTimeCalculation(int logId = 0)
        {
            var sw = new Stopwatch();
            sw.Start();
            ReadIrsFromBinaryAndUpdate(oldSourcePos, audioRenderer);
            sw.Stop();
            FilterUpdate = sw.ElapsedMilliseconds;
        }

        /// <summary>
        /// Implementation of Update Receiver Data for filter Construction if Receiver Update
        /// </summary>
        /// <returns>Receiver Update</returns>
        protected override bool RealTimeReceiverUpdate()
        {
            return false;
        }

        /// <summary>
        /// Implementation of Update Source Data for filter Construction if Source Update
        /// </summary>
        /// <returns>Source Update</returns>
        protected override bool RealTimeSourceUpdate()
        {
            var sw = new Stopwatch();
            sw.Start();
            var sourcePos = Source.position;

            oldSourcePos = sourcePos;
            sw.Stop();
            SourceUpdate = sw.ElapsedMilliseconds;
            return true;
        }

        private bool CalcReceiverUpdate()
        {
            var posUpdate = false;
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
                if (SoundReceiverObject.SoundObjectChanged() | ForceReceiverUpdate)
                {
                    //Debug.Log("Receiver Update " + iBa);
                    rtReceiver[iBa] = SoundReceiverObject
                        .GetHrirForSsPatches(filterLength, buildingAcoustics[iBa].partition.SecondarySourcesPosPatches,
                            out rjSamplesDelay[iBa], out hrirDataLeft[iBa],
                            out hrirDataRight[iBa]);
                    posUpdate = true;
                }

            ForceReceiverUpdate = false;
            return posUpdate;
        }


        private bool CalcSourceUpdate()
        {
            var posUpdate = false;
            if (riSamplesDelay == null || riSamplesDelay.Length != buildingAcoustics.Length)
                riSamplesDelay = new int[buildingAcoustics.Length][];
            logId++;
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            {
                //Debug.Log("Receiver Update " + iBa);
                //rtSource[iBa] = buildingAcoustics[iBa].sourceRoom
                //    .GetSourceFiForPatches(filterLength, out riSamplesDelay[iBa], true, logId);

                var partition = buildingAcoustics[iBa].partition;
                var numPatches = buildingAcoustics[iBa].NumOfPatches;
                for (var iPatch = 0; iPatch < numPatches; iPatch++)
                {
                    if (!buildingAcoustics[iBa].partition.patchesHandler.IsActive[iPatch])
                        continue;
                    var siPatches = Mathf.Sqrt(buildingAcoustics[iBa].partition.Patches[iPatch].Geometry.Area);
                    var wallUp = partition.Geometry.Normal;
                    rtSource[iBa][iPatch] = SoundSourceObjects[0].CalcFiForOneWall(filterLength, soundPos,
                        wallUp, soundOrientation, ssPartitionPatches[iBa][iPatch], siPatches,
                        out riSamplesDelay[iBa][iPatch], false, iPatch, logId);
                }

                posUpdate = true;
            }

            if (applyDeltaDi)
                ApplyDeltaDi();

            ForceSourceUpdate = false;
            return posUpdate;
        }

        private void ApplyDeltaDi()
        {
            var minRi = Min(riSamplesDelay);
            var rtSourceTime = FftHelper.Ifft(rtSource);
            for (var i = 0; i < riSamplesDelay.Length; i++)
            for (var j = 0; j < riSamplesDelay[i].Length; j++)
            {
                var deltaRi = riSamplesDelay[i][j] - minRi;
                rtSourceTime[i][j] = ShiftSignal(rtSourceTime[i][j], deltaRi);
            }

            rtSource = FftHelper.Fft(rtSourceTime, true);
        }

        private float[] ShiftSignal(float[] input, int delaySamples)
        {
            var fOut = new float[input.Length];
            for (var i = 0; i < input.Length - delaySamples; i++) fOut[i + delaySamples] = input[i];

            return fOut;
        }

        private int Min(int[][] input)
        {
            var min = input[0][0];
            for (var i = 0; i < input.Length; i++)
                if (min > Mathf.Min(input[i]))
                    min = Mathf.Min(input[i]);

            return min;
        }


        private float[][][] GetTauPatchesFreqComplex(bool logTau = false)
        {
            var baLength = buildingAcoustics.Length;
            UpdateBaVisible();
            var tauSqrt1PhaseFreqComplex = new float[baLength][][];
            for (var iBa = 0; iBa < baLength; iBa++)
            {
                var partition = buildingAcoustics[iBa].partition;
                var numPatches = partition.patchesHandler.NumOfPatches;
                tauSqrt1PhaseFreqComplex[iBa] = new float[numPatches][];
                for (var iPatch = 0; iPatch < numPatches; iPatch++)
                {
                    var tau3RdOctaveFreq = 10 ^ (partition.patchesHandler.ReductionIndexPatches[iPatch] / -10f);
                    var tauInterpolatedFreq = CalcTauExtrapolatedFrom3RdOctaveTau(tau3RdOctaveFreq, FilterResolution);
                    tauSqrt1PhaseFreqComplex[iBa][iPatch] =
                        CalcSqrtTauComplexFromTauInterpolatedEven(tauInterpolatedFreq.Tf, LogName, 1, 2);
                    if (logTau)
                    {
                        DataLogger.DebugData = true;
                        DataLogger.Log("interpol.tau3RdOctaveFreq{" + (iBa + 1) + ",1}(:," + (iPatch + 1) + ")",
                            tau3RdOctaveFreq);
                        DataLogger.Log("interpol.tauInterpolatedFreq{" + (iBa + 1) + ",1}(:," + (iPatch + 1) + ")",
                            tauInterpolatedFreq);
                        DataLogger.Log("interpol.tauSqrt1PhaseFreqComplex{" + (iBa + 1) + ",1}(:," + (iPatch + 1) + ")",
                            tauSqrt1PhaseFreqComplex[iBa][iPatch]);
                        DataLogger.DebugData = false;
                    }
                }
            }

            if (logTau)
            {
                DataLogger.DebugData = true;
                var interpolationFreqBand = BaSettings.Instance.GetInterpolatedFrequencyBand(FilterResolution / 2);
                DataLogger.Log("interpol.fInterpolation", interpolationFreqBand);
                var thirdOctaveFrequencyBand = BaSettings.Instance.ThirdOctaveFrequencyBandNorm;
                var frequencyBand = BaSettings.Instance.ThirdOctaveFrequencyBandShort;
                var extendedFrequencyBand = BaSettings.Instance.ThirdOctaveFrequencyBandExtended;
                DataLogger.Log("interpol.thirdOctaveFrequencyBand", thirdOctaveFrequencyBand);
                DataLogger.Log("interpol.frequencyBand", frequencyBand);
                DataLogger.Log("interpol.extendedFrequencyBand", extendedFrequencyBand);
                DataLogger.DebugData = false;
            }

            return tauSqrt1PhaseFreqComplex;
        }

        /// <summary>
        /// bake the binaural full ir and write to binary file
        /// </summary>
        /// <param name="bas">input building acoustics array</param>
        /// <param name="writeLog">bool wirite log file</param>
        public void LogMovingSourceBrir( bool writeLog = false)
        {
            Log.Info("Start LogMovingOutdoorSourceSsIr");
            LoggingConfiguration.ConfigureViaFile();
            LogRealtime = false;
            DataLogger.DebugData = false;
            Init();
            filterLength = 44100;

            ssPartitionPatches = new Vector3[buildingAcoustics.Length][][];
            var oldPos = sourceMover.transform.position;
            riSamplesDelay = new int[buildingAcoustics.Length][];
            rtSource = new float[buildingAcoustics.Length][][];
            SoundSourceObjects[0].Init(FilterResolution);
            SoundReceiverObject.Init(FilterResolution);
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            {
                buildingAcoustics[iBa].ProcessAll();
                ssPartitionPatches[iBa] = buildingAcoustics[iBa].partition.SecondarySourcesPosPatches;
                var numPatches = buildingAcoustics[iBa].NumOfPatches;
                riSamplesDelay[iBa] = new int[numPatches];
                rtSource[iBa] = new float[numPatches][];
            }

            var tauPatchesFreqComplex = GetTauPatchesFreqComplex(true);
            rtReceiver = new float[buildingAcoustics.Length][][];
            rjSamplesDelay = new int[buildingAcoustics.Length][];
            hrirDataLeft = new float[buildingAcoustics.Length][][][];
            hrirDataRight = new float[buildingAcoustics.Length][][][];
            ForceSourceUpdate = true;
            ForceReceiverUpdate = true;
            this.logId = 1;
            soundPos = startPos;
            soundOrientation = sourceMover.transform.rotation;

            CalcReceiverUpdate();
            CalcSourceUpdate();

            var constFactorRev = applyReverberation
                ? Mathf.Sqrt(4 / buildingAcoustics[0].receiverRoom.Properties.EquivalentAbsorptionArea)
                : 0f;
            revReceiverTime = buildingAcoustics[0].receiverRoom.reverberation;
            for (var i = 0; i < revReceiverTime.Length; i++)
                revReceiverTime[i] *= constFactorRev;

            var logId = writeLog ? 0 : -1;
            //var oldStart = SourceMover.startPos;
            DataLogger.DebugData = false;
            // var oldEnd = SourceMover.endPos;
            for (var dir = 0; dir < 2; dir++)
            {
                /*
            if (dir == 0)
            {
                SourceMover.startPos = new Vector3(oldStart.x, oldStart.y, oldStart.z);
                SourceMover.endPos = new Vector3(oldEnd.x, oldEnd.y, oldStart.z - maxLengthZ);
                Debug.Log(SourceMover.startPos);
                Debug.Log(SourceMover.endPos);
                SourceMover.Start();
            }
            else
            {
                SourceMover.startPos = new Vector3(oldEnd.x, oldEnd.y, oldEnd.z);
                SourceMover.endPos = new Vector3(oldStart.x, oldStart.y, oldEnd.z + maxLengthZ);
                Debug.Log(SourceMover.startPos);
                Debug.Log(SourceMover.endPos);
                SourceMover.Start();
                SourceMover.dir = 1f;
            }*/

                Directory.CreateDirectory(filePath);
                var fileName = $"{filePath}/rtReceiverTime.dat";
                if (!File.Exists(fileName))
                    using (var file = File.Open(fileName, FileMode.Create))
                    {
                        using (var writer = new BinaryWriter(file))
                        {
                            WriteIrsToBinary(writer, rtReceiver);
                        }
                    }

                Directory.CreateDirectory(filePath);
                fileName = $"{filePath}/hrirLeft.dat";
                if (!File.Exists(fileName))
                    using (var file = File.Open(fileName, FileMode.Create))
                    {
                        using (var writer = new BinaryWriter(file))
                        {
                            WriteIrsToBinary(writer, hrirDataLeft);
                        }
                    }

                fileName = $"{filePath}/hrirRight.dat";
                if (!File.Exists(fileName))
                    using (var file = File.Open(fileName, FileMode.Create))
                    {
                        using (var writer = new BinaryWriter(file))
                        {
                            WriteIrsToBinary(writer, hrirDataRight);
                        }
                    }
                /*
            fileName = $"{filePath}/revReceiverTime.dat";
            if (!File.Exists(fileName))
            {
                using (FileStream file = File.Open(fileName, FileMode.Create))
                {
                    using (BinaryWriter writer = new BinaryWriter(file))
                    {
                        WriteIrsToBinary(writer, revReceiverTime);
                    }
                }
            }
            */

                if (!activateSoundInsulation)
                    for (var iBa = 0; iBa < tauPatchesFreqComplex.Length; iBa++)
                    for (var iPatch = 0; iPatch < tauPatchesFreqComplex[iBa].Length; iPatch++)
                    {
                        tauPatchesFreqComplex[iBa][iPatch] = new float[filterLength * 2];
                        for (var n = 0; n < tauPatchesFreqComplex[iBa][iPatch].Length; n += 2)
                            tauPatchesFreqComplex[iBa][iPatch][n] = 1;
                    }

                while (true)
                {
                    soundPos.z -= deltaZ;
                    var sourcePosString = Vec32Str(soundPos);
                    sourceMover.transform.position = soundPos;
                    CalcSourceUpdate();

                    // ir source 2 patches + tau (single)
                    // if (WriteRtSTau(sourcePosString, tauPatchesFreqComplex)) continue;


                    // full binaural impulse response summed up
                    if (WriteIrSum(sourcePosString, tauPatchesFreqComplex)) continue;


                    // Mono SS Partiton
                    // if (WriteIrSingle(sourcePosString, tauPatchesFreqComplex)) continue;


                    // full binaural impulse response single
                    // if (WriteIrSingle(sourcePosString, tauPatchesFreqComplex)) continue;


                    // just ir source 2 patches (single)
                    // if (WriteRtSource(sourcePosString)) continue;


                    if (soundPos.z < startPos.z - maxLengthZ)
                        break;

                    if (writeLog)
                        logId++;
                }

                if (!applyDoppler)
                    break;
            }
        }

        private bool WriteRtSTau(string sourcePosString, float[][][] tauPatchesFreqComplex)
        {
            string fileName;
            fileName = $"{filePath}/rtSTau-SourcePos_{sourcePosString}.dat";
            if (File.Exists(fileName))
                return true;

            var rtSourceTau = new float[buildingAcoustics.Length][][];
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            {
                rtSourceTau[iBa] = new float[buildingAcoustics[iBa].NumOfPatches][];
                for (var iPatch = 0; iPatch < buildingAcoustics[iBa].NumOfPatches; iPatch++)
                    rtSourceTau[iBa][iPatch] =
                        FftHelper.Ifft(ComplexFloats.Multiply(rtSource[iBa][iPatch],
                            tauPatchesFreqComplex[iBa][iPatch]));
            }

            using (var file = File.Open(fileName, FileMode.Create))
            {
                using (var writer = new BinaryWriter(file))
                {
                    WriteIrsToBinary(writer, rtSourceTau);
                }
            }

            return false;
        }

        private bool WriteRtSource(string sourcePosString)
        {
            string fileName;
            fileName = $"{filePath}/rtSource-SourcePos_{sourcePosString}.dat";
            if (File.Exists(fileName))
                return true;
            using (var file = File.Open(fileName, FileMode.Create))
            {
                using (var writer = new BinaryWriter(file))
                {
                    WriteIrsToBinary(writer, FftHelper.Ifft(rtSource));
                }
            }

            return false;
        }

        private bool WriteIrSingle(string sourcePosString, float[][][] tauPatchesFreqComplex)
        {
            string fileName;
            fileName = $"{filePath}/irSingle-SourcePos_{sourcePosString}.dat";
            if (File.Exists(fileName))
                return true;
            var irSingle = CalcNewApproachOutdoorBir(tauPatchesFreqComplex);
            using (var file = File.Open(fileName, FileMode.Create))
            {
                using (var writer = new BinaryWriter(file))
                {
                    WriteIrsToBinary(writer, irSingle, 0);
                }
            }

            return false;
        }

        private bool WriteIrSum(string sourcePosString, float[][][] tauPatchesFreqComplex)
        {
            string fileName;
            fileName = $"{filePath}/irSum-SourcePos_{sourcePosString}.dat";
            if (File.Exists(fileName))
                return true;
            var irSingle = CalcNewApproachOutdoorBir(tauPatchesFreqComplex);
            var irSs = CalcNewApproachOutdoorBirSumWithShift(irSingle, out var offset);
            using (var file = File.Open(fileName, FileMode.Create))
            {
                using (var writer = new BinaryWriter(file))
                {
                    WriteIrsToBinary(writer, irSs, offset);
                }
            }

            return false;
        }

        private bool WriteIrMono(string sourcePosString, float[][][] tauPatchesFreqComplex)
        {
            string fileName;
            fileName = $"{filePath}/irMono-SourcePos_{sourcePosString}.dat";
            if (File.Exists(fileName))
                return true;
            var irSingle = CalcNewApproachOutdoorSsMono(tauPatchesFreqComplex);
            using (var file = File.Open(fileName, FileMode.Create))
            {
                using (var writer = new BinaryWriter(file))
                {
                    WriteIrsToBinary(writer, irSingle, 0);
                }
            }

            return false;
        }

        private string Vec32Str(Vector3 vec)
        {
            CultureInfo.CurrentCulture = new CultureInfo("en-GB");
            return vec.x.ToString("000.0") + "_" + vec.y.ToString("000.0") + "_" + vec.z.ToString("000.0");
        }

        private void WriteIrsToBinary(BinaryWriter bw, float[][][] array, int offset = 0)
        {
            bw.Write(offset);
            for (var iBa = 0; iBa < array.Length; iBa++)
            for (var iPatch = 0; iPatch < array[iBa].Length; iPatch++)
            {
                if (array[iBa][iPatch].Length != 44100)
                    Log.Error("Invalid Length of Ir for writing to Binary.");

                for (var i = 0; i < array[iBa][iPatch].Length; i++) bw.Write(array[iBa][iPatch][i]);
            }
        }

        private void WriteIrsToBinary(BinaryWriter bw, float[][][][] array, int offset = 0)
        {
            bw.Write(offset);
            for (var iBa = 0; iBa < array.Length; iBa++)
            for (var iPatch = 0; iPatch < array[iBa].Length; iPatch++)
            for (var iShutUp = 0; iShutUp < array[iBa][iPatch].Length; iShutUp++)
                //                    if (array[iBa][iPatch][iShutUp].Length != 44100)
//                        Log.Error("Invalid Length of Ir for writing to Binary.");

            for (var i = 0; i < array[iBa][iPatch][iShutUp].Length; i++)
                bw.Write(array[iBa][iPatch][iShutUp][i]);
        }

        private void WriteIrsToBinary(BinaryWriter bw, float[][] array, int offset = 0)
        {
            bw.Write(offset);
            for (var i = 0; i < array.Length; i++)
            {
                if (array[i].Length != 44100)
                    Log.Error("Invalid Length of Ir for writing to Binary.");

                for (var n = 0; n < array[i].Length; n++) bw.Write(array[i][n]);
            }
        }


        private void WriteIrsToBinary(BinaryWriter bw, float[] array, int offset = 0)
        {
            bw.Write(offset);
            if (array.Length != 44100)
                Log.Error("Invalid Length of Ir for writing to Binary.");

            for (var n = 0; n < array.Length; n++) bw.Write(array[n]);
        }

        private void ReadIrsFromBinaryAndUpdate(Vector3 sourcePos, AudioRender ar)
        {
            var fileName = $"{filePath}/irSum-SourcePos_{Vec32Str(sourcePos)}.dat";
            if (File.Exists(fileName))
                using (var br = new BinaryReader(File.Open(fileName, FileMode.Open)))
                {
                    var offset = br.ReadInt32();
                    var byteBuffer = br.ReadBytes(44100 * sizeof(float));
                    Buffer.BlockCopy(byteBuffer, 0, irLeft, 0, byteBuffer.Length);

                    byteBuffer = br.ReadBytes(44100 * sizeof(float));
                    Buffer.BlockCopy(byteBuffer, 0, irRight, 0, byteBuffer.Length);

                    if (applyDoppler)
                    {
                        var recPos = Receiver.position;
                        irLeft = DopplerShift(irLeft, sourceMover, recPos);
                        irRight = DopplerShift(irRight, sourceMover, recPos);
                    }

                    ar.UpdateFilter(irLeft, irRight, offset);
                }
            else
                Log.Warn("Could not find " + fileName);
        }

        private float[][] CalcNewApproachOutdoorBirSum(float[][][] irAll)
        {
            var irSum = new float[2][];
            irSum[0] = new float[44100];
            irSum[1] = new float[44100];
            for (var iBa = 0; iBa < irAll.Length; iBa++)
            for (var iPatch = 0; iPatch < irAll[iBa].Length / 2; iPatch++)
            for (var n = 0; n < irSum[0].Length; n++)
            {
                irSum[0][n] += irAll[iBa][2 * iPatch][n];
                irSum[1][n] += irAll[iBa][2 * iPatch + 1][n];
            }

            if (applyDoppler)
            {
                var recPos = Receiver.position;
                irSum[0] = DopplerShift(irSum[0], sourceMover, recPos);
                irSum[1] = DopplerShift(irSum[1], sourceMover, recPos);
            }

            return irSum;
        }


        private float[][] CalcNewApproachOutdoorBirSumWithShift(float[][][] irAll, out int minSamples)
        {
            if (rjSamplesDelay.Length == 2)
            {
                minSamples = Mathf.Min(Mathf.Min(rjSamplesDelay[0]), Mathf.Min(rjSamplesDelay[1])) +
                    Mathf.Min(Mathf.Min(riSamplesDelay[0]), Mathf.Min(riSamplesDelay[1])) - 4000;
            }
            else if (rjSamplesDelay.Length == 1)
            {
                minSamples = Mathf.Min(rjSamplesDelay[0]) + Mathf.Min(riSamplesDelay[0]);
            }
            else
            {
                minSamples = 0;
                Log.Error("No supported Length");
            }

            var irSum = new float[2][];
            irSum[0] = new float[44100];
            irSum[1] = new float[44100];
            // minSamples = 0;
            for (var iBa = 0; iBa < irAll.Length; iBa++)
            for (var iPatch = 0; iPatch < irAll[iBa].Length / 2; iPatch++)
            {
                if (irAll[iBa][2 * iPatch] == null)
                    continue;

                for (var n = 0; n < irSum[0].Length; n++)
                {
                    var irAllId = (n + minSamples) % irSum[0].Length;
                    irSum[0][n] += irAll[iBa][2 * iPatch][irAllId];
                    irSum[1][n] += irAll[iBa][2 * iPatch + 1][irAllId];
                }
            }

            for (var n = 22050; n < 44100; n++)
            {
                irSum[0][n] = 0f;
                irSum[1][n] = 0f;
            }

            if (applyDoppler)
            {
                var recPos = Receiver.position;
                irSum[0] = DopplerShift(irSum[0], sourceMover, recPos);
                irSum[1] = DopplerShift(irSum[1], sourceMover, recPos);
            }

            return irSum;
        }

        private float[][][] CalcNewApproachOutdoorBir(float[][][] tauPatchesFreqComplex, int logId = 0, bool logAll = false)
        {
            // Start Partition Wall
            var irSs = new float[buildingAcoustics.Length][][];
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            {
                var patchesActive = buildingAcoustics[iBa].partition.patchesHandler.IsActive;
                irSs[iBa] = new float[2 * tauPatchesFreqComplex[iBa].Length][];

                for (var iPatch = 0; iPatch < tauPatchesFreqComplex[iBa].Length; iPatch++)
                {
                    if (!patchesActive[iPatch])
                        continue;
                    if (!buildingAcoustics[iBa].IsVisible)
                    {
                        irSs[iBa][2 * iPatch] = new float[tauPatchesFreqComplex[iBa][iPatch].Length / 2];
                        irSs[iBa][2 * iPatch + 1] = new float[tauPatchesFreqComplex[iBa][iPatch].Length / 2];
                    }
                    else
                    {
                        var factor = SourceSoundPowerNewApproach * buildingAcoustics[iBa].partition.Patches[iPatch].Geometry.Area /
                                     (buildingAcoustics[iBa].partition.Geometry.Area * 4 * Mathf.PI);
                        var irPatchFreqComplex =
                            ComplexFloats.Multiply(rtSource[iBa][iPatch], tauPatchesFreqComplex[iBa][iPatch],
                                Mathf.Sqrt(factor));


                        var irRoomLeft = new float[filterLength];
                        var irRoomRight = new float[filterLength];

                        // Add Direct Energy
                        if (applyDirect)
                        {
                            rtReceiver[iBa][2 * iPatch].CopyTo(irRoomLeft, 0);
                            rtReceiver[iBa][2 * iPatch + 1].CopyTo(irRoomRight, 0);
                        }

                        // Add Reverberation
                        var deltaSample = rjSamplesDelay[iBa][iPatch];
                        if (binauralReverberation)
                        {
                            var revFreq = FftHelper.Fft(revReceiverTime, true, FilterResolution);

                            var revFreqLeft = FftHelper.Ifft(ComplexFloats.Multiply(
                                FftHelper.Fft(hrirDataLeft[iBa][iPatch][0], true, FilterResolution), revFreq));
                            var revFreqRight = FftHelper.Ifft(ComplexFloats.Multiply(
                                FftHelper.Fft(hrirDataRight[iBa][iPatch][0], true, FilterResolution), revFreq));

                            // var deltaSample = 0;
                            for (var n = 0; n < Mathf.Min(revReceiverTime.Length, irRoomLeft.Length - deltaSample); n++)
                            {
                                irRoomLeft[n + deltaSample] += revFreqLeft[n];
                                irRoomRight[n + deltaSample] += revFreqRight[n];
                            }
                        }
                        else
                        {
                            // var deltaSample = 0;
                            for (var n = 0; n < Mathf.Min(revReceiverTime.Length, irRoomLeft.Length - deltaSample); n++)
                            {
                                irRoomLeft[n + deltaSample + 128] += revReceiverTime[n];
                                irRoomRight[n + deltaSample + 128] += revReceiverTime[n];
                            }
                        }

                        var birLeftFreqComplex =
                            ComplexFloats.Multiply(irPatchFreqComplex, FftHelper.Fft(irRoomLeft, true));
                        var birRightFreqComplex =
                            ComplexFloats.Multiply(irPatchFreqComplex, FftHelper.Fft(irRoomRight, true));

                        if (logAll)
                        {
                            DataLogger.Log(
                                LogName + "irPatchFreqComplex{" + (iBa + 1) + "," + (iPatch + 1) + "}(:," + (logId + 1) +
                                ")",
                                irPatchFreqComplex);
                            DataLogger.Log(
                                LogName + "rtSourceFreqComplex{" + (iBa + 1) + "," + (iPatch + 1) + "}(:," + (logId + 1) +
                                ")",
                                rtSource[iBa][iPatch]);
                            DataLogger.Log(
                                LogName + "factor{" + (iBa + 1) + "," + (iPatch + 1) + "}(:," + (logId + 1) + ")",
                                Mathf.Sqrt(factor));
                        }

                        irSs[iBa][2 * iPatch] = FftHelper.Ifft(birLeftFreqComplex);
                        irSs[iBa][2 * iPatch + 1] = FftHelper.Ifft(birRightFreqComplex);
                    }

                    if (logId >= 0)
                        DataLogger.Log(LogName + "irSs{" + (iBa + 1) + "," + (iPatch + 1) + "}(:," + (logId + 1) + ")",
                            irSs[iBa][iPatch]);
                }
            }

            return irSs;
        }


        private float[][][] CalcNewApproachOutdoorSsMono(float[][][] tauPatchesFreqComplex, int logId = 0,
            bool logAll = false)
        {
            // Start Partition Wall
            var irSs = new float[buildingAcoustics.Length][][];
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            {
                var patchesActive = buildingAcoustics[iBa].partition.patchesHandler.IsActive;
                irSs[iBa] = new float[tauPatchesFreqComplex[iBa].Length][];

                for (var iPatch = 0; iPatch < tauPatchesFreqComplex[iBa].Length; iPatch++)
                {
                    if (!patchesActive[iPatch])
                        continue;
                    if (!buildingAcoustics[iBa].IsVisible)
                    {
                        irSs[iBa][2 * iPatch] = new float[tauPatchesFreqComplex[iBa][iPatch].Length / 2];
                        irSs[iBa][2 * iPatch + 1] = new float[tauPatchesFreqComplex[iBa][iPatch].Length / 2];
                    }
                    else
                    {
                        var factor = SourceSoundPowerNewApproach * buildingAcoustics[iBa].partition.Patches[iPatch].Geometry.Area /
                                     (buildingAcoustics[iBa].partition.Geometry.Area * 4 * Mathf.PI);
                        var irPatchFreqComplex =
                            ComplexFloats.Multiply(rtSource[iBa][iPatch], tauPatchesFreqComplex[iBa][iPatch],
                                Mathf.Sqrt(factor));

                        if (logAll)
                        {
                            DataLogger.Log(
                                LogName + "irPatchFreqComplex{" + (iBa + 1) + "," + (iPatch + 1) + "}(:," + (logId + 1) +
                                ")",
                                irPatchFreqComplex);
                            DataLogger.Log(
                                LogName + "rtSourceFreqComplex{" + (iBa + 1) + "," + (iPatch + 1) + "}(:," + (logId + 1) +
                                ")",
                                rtSource[iBa][iPatch]);
                            DataLogger.Log(
                                LogName + "factor{" + (iBa + 1) + "," + (iPatch + 1) + "}(:," + (logId + 1) + ")",
                                Mathf.Sqrt(factor));
                        }

                        irSs[iBa][iPatch] = FftHelper.Ifft(irPatchFreqComplex);
                    }

                    if (logId >= 0)
                        DataLogger.Log(LogName + "irSs{" + (iBa + 1) + "," + (iPatch + 1) + "}(:," + (logId + 1) + ")",
                            irSs[iBa][iPatch]);
                }
            }

            return irSs;
        }


        private float[] DopplerShift(float[] ir, SoundSourceMover source, Vector3 patchPos)
        {
            var deltaDiff = source.Position - patchPos;
            var deltaZ = deltaDiff.z;
            var deltaX = deltaDiff.x;
            var alpha = Mathf.Atan(deltaX / deltaZ);
            var directionX = source.DirectionForward.x;
            var directionZ = source.DirectionForward.z;
            var v = Vector3.Dot(Vector3.one, source.speedKmh * source.DirectionForward / 3.6f);
            var vDoppler = 0f;
            if (directionX != 0)
                vDoppler = Mathf.Sin(alpha) * v;
            else if (directionZ != 0)
                vDoppler = Mathf.Cos(alpha) * v;
            else
                Debug.LogError("Invalid Direction Given");

            var c = BaSettings.Instance.C0;
            var fCoeff = (c + vDoppler) / c;

            var kDoppler = (int) (FilterResolution * fCoeff);
            var hp = new float[kDoppler];
            for (var i = 0; i < Mathf.Min(ir.Length, hp.Length); i++)
                hp[i] = ir[i];

            var x = new float[kDoppler];
            for (var i = 0; i < kDoppler; i++)
                x[i] = i / (float) kDoppler;
            var xs = new float[FilterResolution];
            for (var i = 0; i < FilterResolution; i++)
                xs[i] = i / (float) FilterResolution;
            return CubicSpline.Compute(x, hp, xs);
            // hp = resample(hp, fftLength, kDoppler);
        }

        private float[] MultiplyComplex(float[] tf1, float[] tf2)
        {
            if (tf1.Length != tf2.Length)
                Debug.LogError("Have to have same Length.");

            var tfResult = new float[tf1.Length];
            for (var n = 0; n < tf1.Length; n += 2)
            {
                tfResult[n] = tf1[n] * tf2[n] - tf1[n + 1] * tf2[n + 1];
                tfResult[n + 1] = tf1[n + 1] * tf2[n] + tf1[n] * tf2[n + 1];
            }

            return tfResult;
        }
    }
}