﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using FFTWSharp;
using log4net;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace BA.BACore
{
    /// <summary>
    /// Implementation of new Approach Outdoor considering Direct and reverberation in Source and Receiver room
    /// First try to implement everything in realtime, but this one is very slow
    /// not working well
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    [Serializable]
    public class AcOutdoorDavyOutdoorPreCalc : AcNewApproachPatches
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(AcOutdoorDavyOutdoorPreCalc));

        private float[] revReceiverTime;
        private float[][][] rtReceiver;
        private float[][][] rtSource;
        private int[][] riSamplesDelay;
        private int[][] rjSamplesDelay;


        private float[] irLeftFreq = new float[44100 * 2];
        private float[] irRightFreq = new float[44100 * 2];
        private float[] irLeftTime = new float[44100];
        private float[] irRightTime = new float[44100];

        private GCHandle ifftInputLeftHandle;
        private GCHandle ifftResultLeftHandle;
        private IntPtr ifftPlanLeft;

        // IFFT Right
        private GCHandle ifftInputRightHandle;
        private GCHandle ifftResultRightHandle;
        private IntPtr ifftPlanRight;

        private string filePath;

        private float[][][] tauPatchesFreq;
        private float[][][] irReceiverRoomFreq;

        private float[] zeros;

        public override AuralisationMethod AuralisationMethod => AuralisationMethod.OutdoorDavyPreCalc;

        /// <summary>
        /// Overload of Initialisation with Init BuildingAcoustics Array from FilterManager
        /// </summary>
        /// <param name="bas">buildingAcoustics Array from FilterManager</param>
        public override void Init()
        {
            moduleName = "AcOutdoorDavyOutdoorPreCalc";
            CultureInfo.CreateSpecificCulture("en-GB");
            init = true;
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            {
                buildingAcoustics[iBa].ProcessAll();
            }

            var postfix = "";
            filePath =
                $"{BaSettings.Instance.RootPath}/Resources/Sounds/OutdoorStaticBakedRealTime/Irs{postfix}";
            irLeftFreq = new float[FilterResolution * 2];
            irRightFreq = new float[FilterResolution * 2];
            irLeftTime = new float[FilterResolution];
            irRightTime = new float[FilterResolution];
            zeros = new float[(uint) (FilterResolution * 0.2f)];
            InitIfftIr();
        }

        /// <summary>
        /// Implementation of Calculate pre-calculation and and set AudioRenderer
        /// </summary>
        /// <param name="ar">AudioRenderer from FilterManager</param>
        protected override void OfflineCalculation(AudioRender ar)
        {
            ForceReceiverUpdate = true;
            ForceSourceUpdate = true;
            rtReceiver = new float[buildingAcoustics.Length][][];
            rtSource = new float[buildingAcoustics.Length][][];
            riSamplesDelay = new int[buildingAcoustics.Length][];

            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            {
                var numPatches = buildingAcoustics[iBa].partition.NumOfPatches;
                rtReceiver[iBa] = new float[2 * numPatches][];
                rtSource[iBa] = new float[numPatches][];
                riSamplesDelay[iBa] = new int[numPatches];
                if (numPatches < 1)
                    Debug.LogWarning(buildingAcoustics[iBa].partition + ": Wall does not contain Patches.");
                for (var iPatch = 0; iPatch < numPatches; iPatch++)
                {
                    rtReceiver[iBa][2 * iPatch] = new float[FilterResolution * 2];
                    rtReceiver[iBa][2 * iPatch + 1] = new float[FilterResolution * 2];
                    rtSource[iBa][iPatch] = new float[FilterResolution * 2];
                }

                buildingAcoustics[iBa].ProcessAll();
            }

            SoundSourceObjects[0].Init(FilterResolution);
            SoundReceiverObject.Init(FilterResolution);

            var constFactorRev = Mathf.Sqrt(4 / buildingAcoustics[0].receiverRoom.Properties.EquivalentAbsorptionArea);
            revReceiverTime = buildingAcoustics[0].receiverRoom.reverberation;
            for (var i = 0; i < revReceiverTime.Length; i++)
                revReceiverTime[i] *= constFactorRev;
            rjSamplesDelay = new int[buildingAcoustics.Length][];
            audioRenderer = ar;
            UpdateMonoIrs();
        }


        /// <summary>
        /// Implementation of Realtime Update, called if Source or Receiver Updates
        /// </summary>
        /// <param name="logId">write log file if logId > 1</param>
        protected override void RealTimeCalculation(int logId = 0)
        {
            var sw = new Stopwatch();
            sw.Start();
            UpdateFilter(audioRenderer);
            sw.Stop();
            FilterUpdate = sw.Elapsed.TotalMilliseconds;
        }

        /// <summary>
        /// Implementation of Update Receiver Data for filter Construction if Receiver Update
        /// </summary>
        /// <returns>Receiver Update</returns>
        protected override bool RealTimeReceiverUpdate()
        {
            if (!init)
                return false;
            var posUpdate = false;
            if (SoundReceiverObject.SoundObjectChanged() | ForceReceiverUpdate)
            {
                var sw = new Stopwatch();
                sw.Start();
                for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
                {
                    // Debug.Log("Receiver Update " + iBa);
                    // rtReceiver[iBa] = buildingAcoustics[iBa].receiverRoom.GetJustHrtfForPatches(fftLength);
                    SoundReceiverObject.GetHrtfsForAllAddSs(rtReceiver[iBa],
                        buildingAcoustics[iBa].partition.SecondarySourcesPosPatches);
                    posUpdate = true;
                }

                sw.Stop();
                ReceiverUpdate = sw.Elapsed.TotalMilliseconds;
            }

            ForceReceiverUpdate = false;
            return posUpdate;
        }

        /// <summary>
        /// Implementation of Update Source Data for filter Construction if Source Update
        /// </summary>
        /// <returns>Source Update</returns>
        protected override bool RealTimeSourceUpdate()
        {
            if (!init)
                return false;
            var posUpdate = false;
            if (SoundSourceObjects[0].SoundObjectChanged() | ForceSourceUpdate)
            {
                var sw = new Stopwatch();
                sw.Start();
                for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
                {
                    SoundSourceObjects[0]
                        .GetSourceTauDavyPatches(buildingAcoustics[iBa].partition, rtSource[iBa]);
                    posUpdate = true;
                }

                sw.Stop();
                SourceUpdate = sw.Elapsed.TotalMilliseconds;
            }

            ForceSourceUpdate = false;
            return posUpdate;
        }

        private float[][][] CalcReceiverUpdate()
        {
            var rtReceiverMono = new float[buildingAcoustics.Length][][];
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            {
                SoundReceiverObject.SoundObjectChanged();
                rtReceiverMono[iBa] = SoundReceiverObject
                    .GetHrtfMonoForSsPatchesDavy(buildingAcoustics[iBa].partition.SecondarySourcesPosPatches, FilterResolution,
                        out rjSamplesDelay[iBa]);
            }

            ForceReceiverUpdate = false;
            return rtReceiverMono;
        }

        private void UpdateMonoIrs()
        {
            var rtReceiverMono = CalcReceiverUpdate();
            irReceiverRoomFreq = new float[buildingAcoustics.Length][][];

            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            {
                irReceiverRoomFreq[iBa] = new float[buildingAcoustics[iBa].partition.NumOfPatches][];
                for (var iPatch = 0; iPatch < buildingAcoustics[iBa].partition.NumOfPatches; iPatch++)
                {
                    var irMono = new float[FilterResolution];

                    // Add Direct Energy
                    rtReceiverMono[iBa][iPatch].CopyTo(irMono, 0);

                    // Add Reverberation
                    for (var n = 0;
                        n < Mathf.Min(revReceiverTime.Length, FilterResolution - rjSamplesDelay[iBa][iPatch]);
                        n++)
                        irMono[n + rjSamplesDelay[iBa][iPatch]] += revReceiverTime[n];

                    var factor = Mathf.Sqrt(SourceSoundPowerNewApproach * buildingAcoustics[iBa].partition.Patches[iPatch].Geometry.Area /
                                            (buildingAcoustics[iBa].partition.Geometry.Area * 4 * Mathf.PI));
                    irReceiverRoomFreq[iBa][iPatch] =
                        ComplexFloats.Multiply(FftHelper.Fft(irMono, true), factor);
                }
            }
        }

        private void UpdateFilter(AudioRender ar)
        {
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            {
                if (!buildingAcoustics[iBa].IsVisible)
                    continue;
                for (var iPatch = 0; iPatch < buildingAcoustics[iBa].partition.NumOfPatches; iPatch++)
                {
                    var rtSourceRef = rtSource[iBa][iPatch];
                    var irReceiverRoomFreqRef = irReceiverRoomFreq[iBa][iPatch];
                    var rtReceiverLeftRef = rtReceiver[iBa][2 * iPatch];
                    var rtReceiverRightRef = rtReceiver[iBa][2 * iPatch + 1];
                    for (var n = 0; n < rtSourceRef.Length; n += 2)
                    {
                        var irSsReal = rtSourceRef[n];
                        var irSsImag = rtSourceRef[n + 1];
                        var irReceiverRoomFreqReal = irReceiverRoomFreqRef[n];
                        var irReceiverRoomFreqImag = irReceiverRoomFreqRef[n + 1];
                        var rtLReal = rtReceiverLeftRef[n];
                        var rtLImag = rtReceiverLeftRef[n + 1];
                        var rtRReal = rtReceiverRightRef[n];
                        var rtRImag = rtReceiverRightRef[n + 1];

                        var monoReal = irSsReal * irReceiverRoomFreqReal - irSsImag * irReceiverRoomFreqImag;
                        var monoImag = irSsReal * irReceiverRoomFreqImag + irSsImag * irReceiverRoomFreqReal;

                        if (iBa == 0 && iPatch == 0)
                        {
                            irLeftFreq[n] = monoReal * rtLReal - monoImag * rtLImag;
                            irLeftFreq[n + 1] = monoReal * rtLImag + monoImag * rtLReal;
                            irRightFreq[n] = monoReal * rtRReal - monoImag * rtRImag;
                            irRightFreq[n + 1] = monoReal * rtRImag + monoImag * rtRReal;
                        }
                        else
                        {
                            irLeftFreq[n] += monoReal * rtLReal - monoImag * rtLImag;
                            irLeftFreq[n + 1] += monoReal * rtLImag + monoImag * rtLReal;
                            irRightFreq[n] += monoReal * rtRReal - monoImag * rtRImag;
                            irRightFreq[n + 1] += monoReal * rtRImag + monoImag * rtRReal;
                        }
                    }
                }
            }

            fftwf.execute(ifftPlanLeft);
            fftwf.execute(ifftPlanRight);
            // zeros.CopyTo(irLeftTime, irLeftTime.Length - zeros.Length);
            // zeros.CopyTo(irRightTime, irRightTime.Length - zeros.Length);
            ar.UpdateFilter(irLeftTime, irRightTime);
            if (LogRealtime)
            {
                DataLogger.DebugData = true;
                DataLogger.Log(LogName + ".irLeftTime(:," + HrirCounter + ")", irLeftTime);
                DataLogger.Log(LogName + ".irRightTime(:," + HrirCounter + ")", irRightTime);
                DataLogger.DebugData = false;
            }
        }
    
        public override void SaveImpulseResponse()
        {
            var samplesOut = new float[irRightTime.Length * 2];
            for (var i = 0; i < irRightTime.Length ; i++)
            {
                samplesOut[2 * i] = irLeftTime[i];
                samplesOut[2 * i + 1] = irRightTime[i];
            }
            const string postFix = "irDavyOutdoor";
            SavWav.SaveWavFile(samplesOut, postFix);
        }

        private void InitIfftIr()
        {
            ifftInputLeftHandle = GCHandle.Alloc(irLeftFreq, GCHandleType.Pinned);
            ifftResultLeftHandle = GCHandle.Alloc(irLeftTime, GCHandleType.Pinned);
            ifftPlanLeft = fftwf.dft_c2r_1d(FilterResolution, ifftInputLeftHandle.AddrOfPinnedObject(),
                ifftResultLeftHandle.AddrOfPinnedObject(), fftw_flags.Estimate);

            ifftInputRightHandle = GCHandle.Alloc(irRightFreq, GCHandleType.Pinned);
            ifftResultRightHandle = GCHandle.Alloc(irRightTime, GCHandleType.Pinned);
            ifftPlanRight = fftwf.dft_c2r_1d(FilterResolution, ifftInputRightHandle.AddrOfPinnedObject(),
                ifftResultRightHandle.AddrOfPinnedObject(), fftw_flags.Estimate);
        }

        /// <summary>
        /// Log the current binaural impulse response (sum of all SS)
        /// </summary>
        public void LogBirSum()
        {
            var receiverString = Vec32Str(Receiver.position) + "-Rot" + Vec32Str(Receiver.rotation.eulerAngles);
            var fileName = $"{filePath}/BinauralLog-ReceiverPos_{receiverString}.dat";
            var irLog = new float[2][];
            irLog[0] = irLeftFreq;
            irLog[1] = irRightFreq;
            if (File.Exists(fileName))
                return;

            using (var file = File.Open(fileName, FileMode.Create))
            {
                using (var writer = new BinaryWriter(file))
                {
                    WriteIrsToBinary(writer, irLog);
                }
            }

            fileName = $"{filePath}/irReceiver-ReceiverPos_{receiverString}.dat";
            using (var file = File.Open(fileName, FileMode.Create))
            {
                using (var writer = new BinaryWriter(file))
                {
                    WriteIrsToBinary(writer, FftHelper.Ifft(irReceiverRoomFreq), 0, true);
                }
            }
        }

        private string Vec32Str(Vector3 vec)
        {
            CultureInfo.CurrentCulture = new CultureInfo("en-GB");
            return vec.x.ToString("000.0") + "_" + vec.y.ToString("000.0") + "_" + vec.z.ToString("000.0");
        }

        private void WriteIrsToBinary(BinaryWriter bw, float[][][] array, int offset = 0, bool ignoreWarn = false)
        {
            bw.Write(offset);
            for (var iBa = 0; iBa < array.Length; iBa++)
            for (var iPatch = 0; iPatch < array[iBa].Length; iPatch++)
            {
                if (!ignoreWarn && array[iBa][iPatch].Length != 44100)
                    Log.Error("Invalid Length of Ir for writing to Binary.");

                for (var i = 0; i < array[iBa][iPatch].Length; i++) bw.Write(array[iBa][iPatch][i]);
            }
        }

        private void WriteIrsToBinary(BinaryWriter bw, float[][] array, int offset = 0)
        {
            bw.Write(offset);
            for (var i = 0; i < array.Length; i++)
            {
                if (array[i].Length != 44100)
                    Log.Error("Invalid Length of Ir for writing to Binary.");

                for (var n = 0; n < array[i].Length; n++) bw.Write(array[i][n]);
            }
        }
    }
}