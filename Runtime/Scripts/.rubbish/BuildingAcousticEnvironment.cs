﻿using System.Diagnostics;
using log4net;
using UnityEngine;

namespace BA.BACore
{
    /// <summary>
    /// Manage the Geometry and the Sound Insulation Metric Calculation
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) 
    /// Version:        1.2  
    /// First release:  2017 
    /// Last revision:  2019 
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    public class BuildingAcousticEnvironment : RoomEnvironment
    {
        [Subtitle] public string buildingAcousticSettings;

        /// <summary>
        /// if set Partition manually
        /// </summary>
        public bool setPartitionManually = true;


        /// <summary>
        /// reference to Partition
        /// </summary>
        public WallBehaviour partition;
        
        public override bool IsVisible => true;

        /// <summary>
        /// Return number of patches
        /// </summary>
        public int NumOfPatches => partition.patchesHandler.NumOfPatches;
        
        protected Junctions PartitionJunctions { get; set; }
        
        private static readonly ILog Log = LogManager.GetLogger(typeof(BuildingAcousticEnvironment));

        /// <summary>
        /// Process all rooms 
        /// </summary>
        public override void ProcessAll()
        {
            var sw = new Stopwatch();
            sw.Start();
            SetGeometry();
            receiverRoom.ProcessAll();
            sourceRoom.ProcessAll();
            sw.Stop();
            Log.Info("Process All for Building Acoustics runs in " + sw.Elapsed.TotalSeconds + " seconds!");
        }

        protected void SetGeometry()
        {
            if (!setPartitionManually)
                SetPartition();
        }
        
        
        private void SetPartition()
        {
            var receiverPosition = soundReceiverObject.transform.position;
            var sourcePosition = soundSourceObjects.SoundSources[0].transform.position;
            var ray = new Ray
            {
                origin = receiverPosition,
                direction = sourcePosition - receiverPosition
            };

            if (Physics.Raycast(ray, out var hit, Mathf.Infinity, _baSetting.LayerMaskWall))
            {
                Log.Info("Found Partition " + hit.collider.gameObject.name);
                partition = hit.collider.gameObject.GetComponent<WallBehaviour>();
                partition.Awake();
            }
            else
            {
                Log.Error("Could not detect Partition");
            }
        }

    }
}