﻿using System;
using System.Diagnostics;
using log4net;
using UnityEngine;


namespace BA.BACore
{
    /// <summary>
    /// Implementation of Classical Indoor Formula Approach considering diffuse Sound field in Source room and direct and reverberation in receiver room 
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) 
    /// Version:        1.2  
    /// First release:  2017 
    /// Last revision:  2019 
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    [Serializable]
    public class AcClassicalIndoorFormula : AcClassicalIndoor
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(AcClassicalIndoorFormula));

        // Formula
        private float[] _factorDirectFormula = new float[5];
        private float[] _tauRevMonoFormula;
        private readonly int[] _directSamplesDelayFormula = new int[5];

        public override AuralisationMethod AuralisationMethod => AuralisationMethod.ClassicalIndoorModelFormula;


        /// <summary>
        /// Implementation of Initialisation with Init BuildingAcoustics Array from FilterManager
        /// </summary>
        public override void Init()
        {
            moduleName = "AcClassicalIndoorFormula";
            init = true;
            LogName = "FilterManager.AcClassicalIndoorFormula";
        }


        /// <summary>
        /// Implementation of Calculate pre-calculation and and set AudioRenderer
        /// </summary>
        /// <param name="ar">AudioRenderer from FilterManager</param>
        public override void OfflineCalculation(AudioRender ar)
        {
            InterpolateSecondarySources();
            OfflineCalculationFormula();
        }


        /// <summary>
        /// Implementation of Realtime Update, called if Source or Receiver Updates
        /// </summary>
        /// <param name="logId">write log file if logId > 1</param>
        public override void RealTimeCalculation(int logId = 0)
        {
            var sw = new Stopwatch();
            sw.Start();
            RealtimeCalculationFormula();
            sw.Stop();
            ReceiverUpdate = sw.Elapsed.TotalMilliseconds;
        }

        /// <summary>
        /// Implementation of Update Receiver Data for filter Construction if Receiver Update
        /// </summary>
        /// <returns>Receiver Update</returns>
        public override bool RealTimeReceiverUpdate()
        {
            return SoundReceiverObject.SoundObjectChanged();
        }

        /// <summary>
        /// Implementation of Update Source Data for filter Construction if Source Update
        /// </summary>
        /// <returns>Source Update</returns>
        public override bool RealTimeSourceUpdate()
        {
            return false;
        }


        private void RealtimeCalculationFormula()
        {
            var hrtf = SoundReceiverObject.GetHrtf(TauFreqComplex[0].Length / 2, _directSamplesDelayFormula);
            var directBinauralAllPathsFreqComplex = GetBinauralIrFreq(TauFreqComplex, hrtf);
            var directBinauralFreqComplex = SumUpPaths(directBinauralAllPathsFreqComplex, _factorDirectFormula);
            var directBinauralTime = FftHelper.Ifft(directBinauralFreqComplex);
            //var directBinauralTimeAll = Ifft(directBinauralAllPathsFreqComplex);
            //var directBinauralTime = SumUpPaths(directBinauralTimeAll, _factor1AbsorptionArea);


            var irTime = CombineFiltersForFormula(directBinauralTime, _tauRevMonoFormula);

            audioRenderer.UpdateFilter(irTime[0], irTime[1]);

            if (LogRealtime)
            {
                DataLogger.Log(LogName + "Formula.irTime{1," + (HrirCounter + 1) + "}", irTime);
                DataLogger.Log(LogName + "Formula.factor1AbsorptionArea{1," + (HrirCounter + 1) + "}",
                    _factorDirectFormula);
                DataLogger.Log(LogName + "Formula.directBinauralFreqComplex{1," + (HrirCounter + 1) + "}",
                    directBinauralFreqComplex);
                DataLogger.Log(LogName + "Formula.directBinauralTime{1," + (HrirCounter + 1) + "}", directBinauralTime);
                DataLogger.Log(LogName + "Formula.irTime{1," + (HrirCounter + 1) + "}", irTime);
                var directBinauralAllPathsTime = FftHelper.Ifft(directBinauralFreqComplex);
                DataLogger.Log(LogName + "Formula.directBinauralAllPathsTime{1," + (HrirCounter + 1) + "}",
                    directBinauralAllPathsTime);
                DataLogger.Log(LogName + "Formula.hrtfComplex{1," + (HrirCounter + 1) + "}", hrtf);
                DataLogger.Log(LogName + "Formula.directBinauralAllPathsFreqComplex{1," + (HrirCounter + 1) + "}",
                    directBinauralAllPathsFreqComplex);
            }
        }

        private void OfflineCalculationFormula()
        {
            InterpolateSecondarySources();
            TauFreqComplex = CalcTauFreq(TauInterpolatedSecondarySources);
            var lengthHrtf = 256;
            var rev = roomAcousticsBehaviour.RoomAcousticProperties.Re;
            var reverberationTailFormula = new float[rev.Length + lengthHrtf];

            for (var i = 0; i < rev.Length; i++)
                reverberationTailFormula[i + lengthHrtf] = rev[i];

            var revTime500 = roomAcousticsBehaviour.RoomAcousticProperties.ReverberationTime;

            var factorReverberationFormula = new float[5];
            var distance = new float[5];
            for (var i = 0; i < 5; i++)
                distance[i] = Vector3.Distance(buildingAcoustic.ReceiverPositions[i],
                    SoundReceiverObject.transform.position);
            var factor1 = GetPreFactorForNewFormula(buildingAcoustic.receiverRoom, partition.Geometry.Area,
                distance);
            for (var i = 0; i < 5; i++)
                factorReverberationFormula[i] = Mathf.Sqrt(16f * Mathf.PI * distance[i] * distance[i]) * factor1[i];

            // Add A to factor1
            _factorDirectFormula = new float[5];
            for (var i = 0; i < 5; i++)
                _factorDirectFormula[i] =
                    Mathf.Sqrt(roomAcousticsBehaviour.RoomAcousticProperties.EquivalentAbsorptionArea) * factor1[i];

            _tauRevMonoFormula =
                SumUpPathsMono(GetTauReverberationTailTime(TauFreqComplex, reverberationTailFormula,
                    factorReverberationFormula));
            if (!DataLogger.DebugData)
            {
                DataLogger.Log(LogName + "Formula.rev", rev);
                DataLogger.Log(LogName + "Formula.revTime500", revTime500);
                DataLogger.Log(LogName + "Formula.absorptionArea",
                    roomAcousticsBehaviour.RoomAcousticProperties.EquivalentAbsorptionArea);
                DataLogger.Log(LogName + "Formula.PartitionArea", partition.Geometry.Area);
                DataLogger.Log(LogName + "Formula.tauRevMonoFormula", _tauRevMonoFormula);
                DataLogger.Log(LogName + "Formula.factor1", factor1);
            }

            SoundReceiverObject.Init(FilterResolution);
        }


        private float[][] CombineFiltersForFormula(float[][] direct, float[] reverberationTail)
        {
            var numChannelDirect = direct.Length;
            if (numChannelDirect != 2)
                Log.Error("channel num is not valid!");

            var ir = new float[2][];
            ir[0] = new float[reverberationTail.Length];
            ir[1] = new float[reverberationTail.Length];

            for (var i = 0; i < reverberationTail.Length; i++)
            {
                ir[0][i] = reverberationTail[i];
                ir[1][i] = reverberationTail[i];
            }

            for (var i = 0; i < direct[0].Length; i++)
            {
                ir[0][i] += direct[0][i];
                ir[1][i] += direct[1][i];
            }

            return ir;
        }


        private static float[] GetPreFactorForNewFormula(ReverberationCalculator room, float sd, float[] distance)
        {
            var factor1 = new float[5];
            for (var i = 0; i < 5; i++)
            {
                var r = distance[i];
                var r1 = 16f * Mathf.PI * r * r;
                factor1[i] = Mathf.Sqrt(sd * room.Properties.ReverberationTime /
                                        (0.32f * room.Properties.volume * 0.5f * (r1 + room.Properties.EquivalentAbsorptionArea)));
            }

            return factor1;
        }

        /// <summary>
        /// run Classical Indoor Approach Formula for Latency Test 
        /// </summary>
        public void TestCalcFormula()
        {
            LoggingConfiguration.ConfigureViaFileRealTimeLatency();
            DataLogger.DebugData = false;
            LogRealtime = false;
            var sw = new Stopwatch();
            var swRt = new Stopwatch();
            var receiver = SoundReceiverObject.transform;
            var oldTPosition = receiver.position;
            var oldTRotation = receiver.rotation;

            sw.Start();
            // Awake();
            OfflineCalculationFormula();
            sw.Stop();
            var runNumbers = 100;
            var latencyRealtime = new double[runNumbers];
            var oldTransformRotation = SoundReceiverObject.transform.rotation;
            for (var i = 0; i < runNumbers; i++)
            {
                SoundReceiverObject.transform.Rotate(Vector3.up, 20);
                SoundReceiverObject.UpdateHrirIndex(buildingAcoustic.ReceiverPositions);
                swRt.Restart();
                RealtimeCalculationFormula();
                swRt.Stop();
                latencyRealtime[i] = swRt.Elapsed.TotalMilliseconds;
            }

            swRt.Stop();
            SoundReceiverObject.transform.rotation = oldTransformRotation;
            Log.Info("Test Formula runs in " + sw.Elapsed.TotalSeconds + " seconds (Realtime(mean n=" +
                     runNumbers + "): " + swRt.Elapsed.TotalMilliseconds / runNumbers + " ms).!");
            SoundReceiverObject.transform.position = oldTPosition;
            SoundReceiverObject.transform.rotation = oldTRotation;
            DataLogger.DebugData = true;
            DataLogger.Log("Indoor.Formula.latencyRealtime", latencyRealtime);
            DataLogger.Log("Indoor.Formula.OfflineTime", sw.Elapsed.TotalSeconds);
        }
    }
}