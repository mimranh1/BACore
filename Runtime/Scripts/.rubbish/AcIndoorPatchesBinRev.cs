﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using FFTWSharp;
using log4net;
using UnityEngine;

namespace BA.BACore
{
    /// <summary>
    /// Implementation of new Approach Indoor considering Direct and reverberation in Source and Receiver room
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    [Serializable]
    public class AcIndoorPatchesBinRev : AcNewApproachFlanking
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(AcIndoorPatchesBinRev));

        private float[][] _dirDirFreqPartitionPatches;
        private float[][] _dirRevFreqPartitionPatches;
        private float[][] _revDirFreqPartitionPatches;
        private float[][] _revRevFreqPartitionPatches;

        private float[][] _dirDirFreqPartition = new float[5][];
        private float[][] _dirRevFreqPartition = new float[5][];
        private float[][] _revDirFreqPartition = new float[5][];
        private float[][] _revRevFreqPartition = new float[5][];
        private float[][] _dirDirFreqFlanking = new float[8][];
        private float[][] _dirRevFreqFlanking = new float[8][];
        private float[][] _revDirFreqFlanking = new float[8][];
        private float[][] _revRevFreqFlanking = new float[8][];

        private float[][][] _irMonoFreqPatches;
        private float[][][] _irMonoFreqWalls;
        private bool _updateMonoFilter;

        private float[][][] _rtReceiverFactorPatches;
        private float[][][] _rtReceiverFactorWalls;
        private float[][][] _rtReceiverHrtfPatches;
        private float[][][] _rtReceiverHrtfWalls;
        private float[][][] _rtSourcePatches;
        private float[][][] _rtSourceWalls;

        private readonly float[] _irLeftFreq = new float[44100 * 2];
        private readonly float[] _irRightFreq = new float[44100 * 2];
        private readonly float[] _irLeftTime = new float[44100];
        private readonly float[] _irRightTime = new float[44100];

        private GCHandle _ifftInputLeftHandle;
        private GCHandle _ifftResultLeftHandle;
        private IntPtr _ifftPlanLeft;

        // IFFT Right
        private GCHandle _ifftInputRightHandle;
        private GCHandle _ifftResultRightHandle;
        public IntPtr ifftPlanRight;


        private bool usePatches;


        public override AuralisationMethod AuralisationMethod => AuralisationMethod.IndoorModelPatchesBinRev;

        /// <summary>
        /// Implementation of Initialisation with Init BuildingAcoustics Array from FilterManager
        /// </summary>
        /// <param name="bas">buildingAcoustics Array from FilterManager</param>
        public override void Init()
        {
            moduleName = "AcIndoorPatchesBinRev";
            init = true;
            LogName = "FilterManager.AcIndoorPatchesBinRev";
            var baLength = buildingAcoustics.Length;
            _rtReceiverFactorPatches = new float[baLength][][];
            _rtReceiverFactorWalls = new float[baLength][][];
            _rtReceiverHrtfPatches = new float[baLength][][];
            _rtReceiverHrtfWalls = new float[baLength][][];
            _rtSourcePatches = new float[baLength][][];
            _rtSourceWalls = new float[baLength][][];
            _irMonoFreqPatches = new float[baLength][][];
            _irMonoFreqWalls = new float[baLength][][];
            for (var iBa = 0; iBa < baLength; iBa++)
            {
                var numOfPatches = buildingAcoustics[iBa].NumOfPatches;
                _rtReceiverFactorWalls[iBa] = new float[5][];
                _rtReceiverHrtfWalls[iBa] = new float[2 * 5][];
                _rtSourceWalls[iBa] = new float[5][];
                _irMonoFreqWalls[iBa] = new float[5][];
                for (var iWall = 0; iWall < 5; iWall++)
                {
                    _rtReceiverFactorWalls[iBa][iWall] = new float[2 * FilterResolution];
                    _rtReceiverHrtfWalls[iBa][2 * iWall] = new float[2 * FilterResolution];
                    _rtReceiverHrtfWalls[iBa][2 * iWall + 1] = new float[2 * FilterResolution];
                    _rtSourceWalls[iBa][iWall] = new float[2 * FilterResolution];
                    _irMonoFreqWalls[iBa][iWall] = new float[2 * FilterResolution];
                }

                _rtReceiverFactorPatches[iBa] = new float[numOfPatches][];
                _rtReceiverHrtfPatches[iBa] = new float[2 * numOfPatches][];
                _rtSourcePatches[iBa] = new float[numOfPatches][];
                _irMonoFreqPatches[iBa] = new float[numOfPatches][];
                for (var iPatch = 0; iPatch < numOfPatches; iPatch++)
                {
                    _rtReceiverFactorPatches[iBa][iPatch] = new float[2 * FilterResolution];
                    _rtReceiverHrtfPatches[iBa][2 * iPatch] = new float[2 * FilterResolution];
                    _rtReceiverHrtfPatches[iBa][2 * iPatch + 1] = new float[2 * FilterResolution];
                    _rtSourcePatches[iBa][iPatch] = new float[2 * FilterResolution];
                    _irMonoFreqPatches[iBa][iPatch] = new float[2 * FilterResolution];
                }

                _dirDirFreqPartitionPatches = new float[numOfPatches][];
                _dirRevFreqPartitionPatches = new float[numOfPatches][];
                _revDirFreqPartitionPatches = new float[numOfPatches][];
                _revRevFreqPartitionPatches = new float[numOfPatches][];
            }

            SoundSourceObjects[0].Init(FilterResolution);
            SoundReceiverObject.Init(FilterResolution);

            InitIfftIr();
        }

        private void InitIfftIr()
        {
            _ifftInputLeftHandle = GCHandle.Alloc(_irLeftFreq, GCHandleType.Pinned);
            _ifftResultLeftHandle = GCHandle.Alloc(_irLeftTime, GCHandleType.Pinned);
            _ifftPlanLeft = fftwf.dft_c2r_1d(FilterResolution, _ifftInputLeftHandle.AddrOfPinnedObject(),
                _ifftResultLeftHandle.AddrOfPinnedObject(), fftw_flags.Estimate);

            _ifftInputRightHandle = GCHandle.Alloc(_irRightFreq, GCHandleType.Pinned);
            _ifftResultRightHandle = GCHandle.Alloc(_irRightTime, GCHandleType.Pinned);
            ifftPlanRight = fftwf.dft_c2r_1d(FilterResolution, _ifftInputRightHandle.AddrOfPinnedObject(),
                _ifftResultRightHandle.AddrOfPinnedObject(), fftw_flags.Estimate);
        }

        /// <summary>
        /// Implementation of Calculate pre-calculation and and set AudioRenderer
        /// </summary>
        /// <param name="ar">AudioRenderer from FilterManager</param>
        protected override void OfflineCalculation(AudioRender ar)
        {
            audioRenderer = ar;
            buildingAcoustics[0].ProcessAll();
            var areaSi = new float[5];
            areaSi[0] = buildingAcoustics[0].partition.Geometry.Area;
            for (var i = 0; i < 4; i++)
                areaSi[i + 1] = buildingAcoustics[0].sourceWalls[i].Geometry.Area;
            var areaSd = areaSi[0];
            var rhoC0 = BaSettings.Instance.C0 * BaSettings.Instance.Rho0;
            var equivalentAbsAreaSource = buildingAcoustics[0].sourceRoom.Properties.EquivalentAbsorptionArea;
            var equivalentAbsAreaReceiver = buildingAcoustics[0].receiverRoom.Properties.EquivalentAbsorptionArea;
            var sId = buildingAcoustics[0].SourceWallIndexes;
            var rId = buildingAcoustics[0].ReceiverWallIndexes;
            var pId = buildingAcoustics[0].PartitionWallIndexes;
            var revSourceFreqComplex = FftHelper.Fft(buildingAcoustics[0].sourceRoom.reverberation, true, FilterResolution);
            var revReceiverFreqComplex =
                FftHelper.Fft(buildingAcoustics[0].receiverRoom.reverberation, true, FilterResolution);
            var revAll = ComplexFloats.Multiply(revSourceFreqComplex, revReceiverFreqComplex);

            usePatches = buildingAcoustics[0].partition.patchesHandler.NumOfPatches != 0;

            var tauPatches = buildingAcoustics[0].partition.patchesHandler.ReductionIndexPatches;
            for (var iPatch = 0; iPatch < buildingAcoustics[0].NumOfPatches; iPatch++)
            {
                var tauInterpolatedFreq =
                    CalcTauExtrapolatedFrom3RdOctaveTau(10 ^ (-0.1f * tauPatches[iPatch]), FilterResolution);
                var tau1PhaseFreqComplex = CalcSqrtTauComplexFromTauInterpolatedEven(tauInterpolatedFreq.Tf, LogName, 1, 2);
                var preFactor = Mathf.Sqrt(400 * 400 * sourceSoundPowerNewApproach *
                                           buildingAcoustics[0].partition.Patches[iPatch].Geometry.Area /
                                           (areaSd * rhoC0));

                var factorDirDir = preFactor / (4 * Mathf.PI);
                var factorDirRev = preFactor * Mathf.Sqrt(1 / (Mathf.PI * equivalentAbsAreaReceiver));
                var factorRevDir = preFactor * Mathf.Sqrt(areaSd / (4 * Mathf.PI * equivalentAbsAreaSource));
                var factorRevRev =
                    preFactor * Mathf.Sqrt(4 * areaSd / (equivalentAbsAreaSource * equivalentAbsAreaReceiver));

                // Calculate Direct-Direct Pre Filter
                _dirDirFreqPartitionPatches[iPatch] = ComplexFloats.Multiply(tau1PhaseFreqComplex, factorDirDir);

                // Calculate Direct-Rev Pre Filter
                _dirRevFreqPartitionPatches[iPatch] =
                    ComplexFloats.Multiply(revReceiverFreqComplex, tau1PhaseFreqComplex, factorDirRev);

                // Calculate Rev-Direct Pre Filter
                _revDirFreqPartitionPatches[iPatch] =
                    ComplexFloats.Multiply(revSourceFreqComplex, tau1PhaseFreqComplex, factorRevDir);

                // Calculate Rev-Rev Pre Filter
                _revRevFreqPartitionPatches[iPatch] = ComplexFloats.Multiply(revAll, tau1PhaseFreqComplex, factorRevRev);
            }

            // Calc Flanking
            for (var i = 0; i < 13; i++)
            {
                var pathId = (i - 1) / 4;
                var iJun = (i - 1) % 4;
                var sIdGlobal = 0;
                var metricId = 0;
                TransferFunction tau3RdOctaveFreq;
                if (i == 0)
                {
                    tau3RdOctaveFreq = 10 ^ (buildingAcoustics[0].partition.ReductionIndex.ReductionIndex / -10f);
                    sIdGlobal = 0;
                    iJun = 0;
                    metricId = 0;
                }
                else
                {
                    var reductionIndexFlanking = buildingAcoustics[0].GetReductionIndexFlanking();
                    switch (pathId)
                    {
                        case 0:
                            tau3RdOctaveFreq =
                                10 ^ (reductionIndexFlanking[iJun][sId[iJun], rId[iJun]] / -10f);
                            sIdGlobal = iJun + 1;
                            metricId = 2 * iJun + 0;
                            break;
                        case 1:
                            tau3RdOctaveFreq =
                                10 ^ (reductionIndexFlanking[iJun][sId[iJun], pId[iJun]] / -10f);
                            sIdGlobal = iJun + 1;
                            metricId = iJun + 1;
                            break;
                        case 2:
                            tau3RdOctaveFreq =
                                10 ^ (reductionIndexFlanking[iJun][pId[iJun], rId[iJun]] / -10f);
                            sIdGlobal = 0;

                            metricId = 2 * iJun + 1;
                            break;
                        default:
                            tau3RdOctaveFreq =
                                10 ^ (reductionIndexFlanking[iJun][sId[iJun], rId[iJun]] / -10f);
                            Log.Error("Cannot find Reduction Index!");
                            break;
                    }
                }

                var tauInterpolatedFreq = CalcTauExtrapolatedFrom3RdOctaveTau(tau3RdOctaveFreq, FilterResolution);
                var tau1PhaseFreqComplex = CalcSqrtTauComplexFromTauInterpolatedEven(tauInterpolatedFreq.Tf, LogName, 1, 2);
                //tau0PhaseFreqComplex = tau1PhaseFreqComplex;
                var preFactor = Mathf.Sqrt(400 * 400 * sourceSoundPowerNewApproach * areaSd / (areaSi[sIdGlobal] * rhoC0));

                var factorDirDir = preFactor / (4 * Mathf.PI);
                var factorDirRev = preFactor * Mathf.Sqrt(1 / (Mathf.PI * equivalentAbsAreaReceiver));
                var factorRevDir = preFactor * Mathf.Sqrt(areaSi[sIdGlobal] / (4 * Mathf.PI * equivalentAbsAreaSource));
                var factorRevRev =
                    preFactor * Mathf.Sqrt(4 * areaSi[sIdGlobal] / (equivalentAbsAreaSource * equivalentAbsAreaReceiver));
                if (i == 0 || pathId == 1)
                {
                    // Calculate Direct-Direct Pre Filter
                    _dirDirFreqPartition[metricId] = ComplexFloats.Multiply(tau1PhaseFreqComplex, factorDirDir);

                    // Calculate Direct-Rev Pre Filter
                    _dirRevFreqPartition[metricId] =
                        ComplexFloats.Multiply(revReceiverFreqComplex, tau1PhaseFreqComplex, factorDirRev);

                    // Calculate Rev-Direct Pre Filter
                    _revDirFreqPartition[metricId] =
                        ComplexFloats.Multiply(revSourceFreqComplex, tau1PhaseFreqComplex, factorRevDir);

                    // Calculate Rev-Rev Pre Filter
                    _revRevFreqPartition[metricId] = ComplexFloats.Multiply(revAll, tau1PhaseFreqComplex, factorRevRev);
                }
                else
                {
                    // Calculate Direct-Direct Pre Filter
                    _dirDirFreqFlanking[metricId] = ComplexFloats.Multiply(tau1PhaseFreqComplex, factorDirDir);

                    // Calculate Direct-Rev Pre Filter
                    _dirRevFreqFlanking[metricId] =
                        ComplexFloats.Multiply(revReceiverFreqComplex, tau1PhaseFreqComplex, factorDirRev);

                    // Calculate Rev-Direct Pre Filter
                    _revDirFreqFlanking[metricId] =
                        ComplexFloats.Multiply(revSourceFreqComplex, tau1PhaseFreqComplex, factorRevDir);

                    // Calculate Rev-Rev Pre Filter
                    _revRevFreqFlanking[metricId] = ComplexFloats.Multiply(revAll, tau1PhaseFreqComplex, factorRevRev);
                }
            }

            ForceReceiverUpdate = true;
            ForceSourceUpdate = true;
            RealTimeSourceUpdate();
            RealTimeReceiverUpdate();
        }

        private void LogAllData()
        {
            DataLogger.DebugData = true;
            DataLogger.Log(LogName + ".rtSourcePatchesComplex", _rtSourcePatches);
            DataLogger.Log(LogName + ".rtSourceWallsComplex", _rtSourceWalls);
            DataLogger.Log(LogName + ".rtReceiverFactorPatchesComplex", _rtReceiverFactorPatches);
            DataLogger.Log(LogName + ".rtReceiverFactorWallsComplex", _rtReceiverFactorWalls);
            DataLogger.Log(LogName + ".rtReceiverHrtfPatchesComplex", _rtReceiverHrtfPatches);
            DataLogger.Log(LogName + ".rtReceiverHrtfWallsComplex", _rtReceiverHrtfWalls);
            DataLogger.Log(LogName + ".revRevFreqFlankingComplex", _revRevFreqFlanking);
            DataLogger.Log(LogName + ".revDirFreqFlankingComplex", _revDirFreqFlanking);
            DataLogger.Log(LogName + ".dirRevFreqFlankingComplex", _dirRevFreqFlanking);
            DataLogger.Log(LogName + ".dirDirFreqFlankingComplex", _dirDirFreqFlanking);
            DataLogger.Log(LogName + ".revRevFreqPartitionComplex", _revRevFreqPartition);
            DataLogger.Log(LogName + ".revDirFreqPartitionComplex", _revDirFreqPartition);
            DataLogger.Log(LogName + ".dirRevFreqPartitionComplex", _dirRevFreqPartition);
            DataLogger.Log(LogName + ".dirDirFreqPartitionComplex", _dirDirFreqPartition);
            DataLogger.Log(LogName + ".revDirFreqPartitionPatchesComplex", _revDirFreqPartitionPatches);
            DataLogger.Log(LogName + ".revRevFreqPartitionPatchesComplex", _revRevFreqPartitionPatches);
            DataLogger.Log(LogName + ".dirRevFreqPartitionPatchesComplex", _dirRevFreqPartitionPatches);
            DataLogger.Log(LogName + ".dirDirFreqPartitionPatchesComplex", _dirDirFreqPartitionPatches);
            DataLogger.DebugData = false;
        }

        private void UpdateMonoIr()
        {
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            {
                // Calc Patches
                for (var iPatch = 0; iPatch < _dirDirFreqPartitionPatches.Length; iPatch++)
                {
                    var revDirFreqPartitionOne = _revDirFreqPartitionPatches[iPatch];
                    var dirDirFreqPartitionOne = _dirDirFreqPartitionPatches[iPatch];
                    var dirRevFreqPartitionOne = _dirRevFreqPartitionPatches[iPatch];
                    var revRevFreqPartitionOne = _revRevFreqPartitionPatches[iPatch];
                    var rtSourceWall = _rtSourcePatches[iBa][iPatch];
                    var rtReceiverFactorPatch = _rtReceiverFactorPatches[iBa][iPatch];
                    var irMonoFreqPatch = _irMonoFreqPatches[iBa][iPatch];
                    for (var n = 0; n < 2 * FilterResolution; n += 2)
                    {
                        var ssBinReal = dirDirFreqPartitionOne[n] * rtSourceWall[n] -
                                        dirDirFreqPartitionOne[n + 1] * rtSourceWall[n + 1] +
                                        revDirFreqPartitionOne[n];

                        var ssBinImag = dirDirFreqPartitionOne[n + 1] * rtSourceWall[n] +
                                        dirDirFreqPartitionOne[n] * rtSourceWall[n + 1] +
                                        revDirFreqPartitionOne[n + 1];

                        var ssMonoReal = dirRevFreqPartitionOne[n] * rtSourceWall[n] -
                                         dirRevFreqPartitionOne[n + 1] * rtSourceWall[n + 1];

                        var ssMonoImag = dirRevFreqPartitionOne[n + 1] * rtSourceWall[n] +
                                         dirRevFreqPartitionOne[n] * rtSourceWall[n + 1];


                        irMonoFreqPatch[n] += ssBinReal * rtReceiverFactorPatch[n] -
                                              ssBinImag * rtReceiverFactorPatch[n + 1] +
                                              ssMonoReal;
                        irMonoFreqPatch[n + 1] += ssBinReal * rtReceiverFactorPatch[n + 1] +
                                                  ssBinImag * rtReceiverFactorPatch[n] + ssMonoImag;
                        irMonoFreqPatch[n] += revRevFreqPartitionOne[n];
                        irMonoFreqPatch[n + 1] += revRevFreqPartitionOne[n + 1];
                    }
                }

                // Calc Partition Walls
                var rtReceiverFactorPartition = _rtReceiverFactorWalls[iBa][0];
                var irMonoFreqPartition = _irMonoFreqWalls[iBa][0];
                for (var iWall = 0; iWall < _dirDirFreqPartition.Length; iWall++)
                {
                    if (usePatches & (iWall == 0))
                        continue;
                    var revDirFreqPartitionOne = _revDirFreqPartition[iWall];
                    var dirDirFreqPartitionOne = _dirDirFreqPartition[iWall];
                    var dirRevFreqPartitionOne = _dirRevFreqPartition[iWall];
                    var revRevFreqPartitionOne = _revRevFreqPartition[iWall];
                    var rtSourceWall = _rtSourceWalls[iBa][iWall];
                    for (var n = 0; n < 2 * FilterResolution; n += 2)
                    {
                        var ssBinReal = dirDirFreqPartitionOne[n] * rtSourceWall[n] -
                                        dirDirFreqPartitionOne[n + 1] * rtSourceWall[n + 1] +
                                        revDirFreqPartitionOne[n];

                        var ssBinImag = dirDirFreqPartitionOne[n + 1] * rtSourceWall[n] +
                                        dirDirFreqPartitionOne[n] * rtSourceWall[n + 1] +
                                        revDirFreqPartitionOne[n + 1];

                        var ssMonoReal = dirRevFreqPartitionOne[n] * rtSourceWall[n] -
                                         dirRevFreqPartitionOne[n + 1] * rtSourceWall[n + 1];

                        var ssMonoImag = dirRevFreqPartitionOne[n + 1] * rtSourceWall[n] +
                                         dirRevFreqPartitionOne[n] * rtSourceWall[n + 1];


                        irMonoFreqPartition[n] += ssBinReal * rtReceiverFactorPartition[n] -
                                                  ssBinImag * rtReceiverFactorPartition[n + 1] +
                                                  ssMonoReal;
                        irMonoFreqPartition[n + 1] += ssBinReal * rtReceiverFactorPartition[n + 1] +
                                                      ssBinImag * rtReceiverFactorPartition[n] + ssMonoImag;
                        irMonoFreqPartition[n] += revRevFreqPartitionOne[n];
                        irMonoFreqPartition[n + 1] += revRevFreqPartitionOne[n + 1];
                    }
                }

                // Calc Flanking Walls
                for (var iWall = 0; iWall < 4; iWall++)
                {
                    var dirDirFf = _dirDirFreqFlanking[2 * iWall];
                    var dirDirDf = _dirDirFreqFlanking[2 * iWall + 1];
                    var revDirFf = _revDirFreqFlanking[2 * iWall];
                    var revDirFd = _revDirFreqFlanking[2 * iWall + 1];
                    var dirRevFf = _dirRevFreqFlanking[2 * iWall];
                    var dirRevFd = _dirRevFreqFlanking[2 * iWall + 1];
                    var revRevFf = _revRevFreqFlanking[2 * iWall];
                    var revRevFd = _revRevFreqFlanking[2 * iWall + 1];
                    var rtSourceFlanking = _rtSourceWalls[iBa][iWall + 1];
                    var rtSourcePartition = _rtSourceWalls[iBa][0];
                    var rtReceiverFlanking = _rtReceiverFactorWalls[iBa][iWall + 1];
                    var irMonoFreqWall = _irMonoFreqWalls[iBa][iWall + 1];

                    for (var n = 0; n < 2 * FilterResolution; n += 2)
                    {
                        var ssBinReal = dirDirFf[n] * rtSourceFlanking[n] - dirDirFf[n + 1] * rtSourceFlanking[n + 1] +
                                        dirDirDf[n] * rtSourcePartition[n] - dirDirDf[n + 1] * rtSourcePartition[n + 1] +
                                        revDirFf[n] + revDirFd[n];
                        var ssBinImag = dirDirFf[n + 1] * rtSourceFlanking[n] + dirDirFf[n] * rtSourceFlanking[n + 1] +
                                        dirDirDf[n + 1] * rtSourcePartition[n] + dirDirDf[n] * rtSourcePartition[n + 1] +
                                        revDirFf[n + 1] + revDirFd[n + 1];

                        var ssMonoReal = dirRevFf[n] * rtSourceFlanking[n] - dirRevFf[n + 1] * rtSourceFlanking[n + 1] +
                            dirRevFd[n] * rtSourcePartition[n] - dirRevFd[n + 1] * rtSourcePartition[n + 1];
                        var ssMonoImag = dirRevFf[n + 1] * rtSourceFlanking[n] + dirRevFf[n] * rtSourceFlanking[n + 1] +
                                         dirRevFd[n + 1] * rtSourcePartition[n] + dirRevFd[n] * rtSourcePartition[n + 1];

                        irMonoFreqWall[n] +=
                            ssBinReal * rtReceiverFlanking[n] - ssBinImag * rtReceiverFlanking[n + 1] + ssMonoReal;
                        irMonoFreqWall[n + 1] +=
                            ssBinReal * rtReceiverFlanking[n + 1] + ssBinImag * rtReceiverFlanking[n] + ssMonoImag;

                        irMonoFreqWall[n] += revRevFf[n] + revRevFd[n];
                        irMonoFreqWall[n + 1] += revRevFf[n + 1] + revRevFd[n + 1];
                    }
                }
            }
        }

        /// <summary>
        /// Implementation of Realtime Update, called if Source or Receiver Updates
        /// </summary>
        /// <param name="logId">write log file if logId > 1</param>
        protected override void RealTimeCalculation(int logId = 0)
        {
            var sw = new Stopwatch();
            sw.Start();
            if (_updateMonoFilter)
            {
                UpdateMonoIr();
                _updateMonoFilter = false;
            }


            // Calc Patches
            if (usePatches)
                for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
                for (var iPatch = 0; iPatch < buildingAcoustics[iBa].NumOfPatches; iPatch++)
                {
                    var irMonoFreqPatch = _irMonoFreqPatches[iBa][iPatch];
                    var rtReceiverHrtfPatchLeft = _rtReceiverHrtfPatches[iBa][2 * iPatch];
                    var rtReceiverHrtfPatchRight = _rtReceiverHrtfPatches[iBa][2 * iPatch + 1];
                    for (var n = 0; n < 2 * FilterResolution; n += 2)
                    {
                        _irLeftFreq[n] += irMonoFreqPatch[n] * rtReceiverHrtfPatchLeft[n] -
                                         irMonoFreqPatch[n + 1] * rtReceiverHrtfPatchLeft[n + 1];
                        _irLeftFreq[n + 1] += irMonoFreqPatch[n] * rtReceiverHrtfPatchLeft[n + 1] +
                                             irMonoFreqPatch[n + 1] * rtReceiverHrtfPatchLeft[n];

                        _irRightFreq[n] += irMonoFreqPatch[n] * rtReceiverHrtfPatchRight[n] -
                                          irMonoFreqPatch[n + 1] * rtReceiverHrtfPatchRight[n + 1];
                        _irRightFreq[n + 1] += irMonoFreqPatch[n] * rtReceiverHrtfPatchRight[n + 1] +
                                              irMonoFreqPatch[n + 1] * rtReceiverHrtfPatchRight[n];
                    }
                }


            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            for (var iWall = 0; iWall < 5; iWall++)
            {
                var irMonoFreqWall = _irMonoFreqWalls[iBa][iWall];
                var rtReceiverHrtfWallLeft = _rtReceiverHrtfWalls[iBa][2 * iWall];
                var rtReceiverHrtfWallRight = _rtReceiverHrtfWalls[iBa][2 * iWall + 1];
                for (var n = 0; n < 2 * FilterResolution; n += 2)
                {
                    _irLeftFreq[n] += irMonoFreqWall[n] * rtReceiverHrtfWallLeft[n] -
                                     irMonoFreqWall[n + 1] * rtReceiverHrtfWallLeft[n + 1];
                    _irLeftFreq[n + 1] += irMonoFreqWall[n] * rtReceiverHrtfWallLeft[n + 1] +
                                         irMonoFreqWall[n + 1] * rtReceiverHrtfWallLeft[n];

                    _irRightFreq[n] += irMonoFreqWall[n] * rtReceiverHrtfWallRight[n] -
                                      irMonoFreqWall[n + 1] * rtReceiverHrtfWallRight[n + 1];
                    _irRightFreq[n + 1] += irMonoFreqWall[n] * rtReceiverHrtfWallRight[n + 1] +
                                          irMonoFreqWall[n + 1] * rtReceiverHrtfWallRight[n];
                }
            }

            fftwf.execute(_ifftPlanLeft);
            fftwf.execute(ifftPlanRight);

            for (var n = 0; n < _irLeftTime.Length; n++)
                _irLeftTime[n] /= FilterResolution;
            for (var n = 0; n < _irRightTime.Length; n++)
                _irRightTime[n] /= FilterResolution;

            if (audioRenderer != null)
            {
                audioRenderer.UpdateFilter(_irLeftTime, _irRightTime);
            }
            else
            {
                DataLogger.DebugData = true;
                DataLogger.Log(LogName + ".irTimeLeft(:," + (HrirCounter + 1) + ")", _irLeftTime);
                DataLogger.Log(LogName + ".irTimeRight(:," + (HrirCounter + 1) + ")", _irRightTime);
                DataLogger.Log(LogName + ".irLeftFreqComplex", _irLeftFreq);
                DataLogger.Log(LogName + ".irRightFreqComplex", _irRightFreq);
                DataLogger.DebugData = false;
            }

            Array.Clear(_irLeftFreq, 0, _irLeftFreq.Length);
            Array.Clear(_irRightFreq, 0, _irRightFreq.Length);
            sw.Stop();
            FilterUpdate = sw.Elapsed.TotalMilliseconds;
            if (LogRealtime)
            {
                LogAllData();
                DataLogger.DebugData = true;
                DataLogger.Log(LogName + ".irTimeLeft", _irLeftTime);
                DataLogger.Log(LogName + ".irTimeRight", _irRightTime);
                DataLogger.Log(LogName + ".sumAllIrLeftFreqComplex", _irLeftFreq);
                DataLogger.Log(LogName + ".sumAllIrRightFreqComplex", _irRightFreq);
                DataLogger.DebugData = false;
                LogRealtime = false;
            }
        }

        /// <summary>
        /// Implementation of Update Receiver Data for filter Construction if Receiver Update
        /// </summary>
        /// <returns>Receiver Update</returns>
        protected override bool RealTimeReceiverUpdate()
        {
            var sw = new Stopwatch();
            sw.Start();
            var posUpdate = false;

            // Update Movement
            if (SoundReceiverObject.SoundObjectMove() | ForceReceiverUpdate)
            {
                for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
                {
                    SoundReceiverObject
                        .GetReceiverFactorFreqAll(buildingAcoustics[iBa].partition.SecondarySourcesPosPatches,
                            _rtReceiverFactorPatches[iBa]);
                    SoundReceiverObject
                        .GetReceiverFactorFreqAll(buildingAcoustics[iBa].ReceiverPositionsSs, _rtReceiverFactorWalls[iBa]);
                    posUpdate = true;
                }

                _updateMonoFilter = true;
                // if Position updates orientation have also changed
                ForceReceiverUpdate = true;
                //Debug.Log("Update Receiver Filter!");
            }

            // Update Rotation
            if (SoundReceiverObject.SoundObjectRotate() | ForceReceiverUpdate)
                for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
                {
                    SoundReceiverObject.GetHrtfsForAllAddSs(_rtReceiverHrtfPatches[iBa],
                        buildingAcoustics[iBa].partition.SecondarySourcesPosPatches);
                    SoundReceiverObject.GetHrtfsForAllAddSs(_rtReceiverHrtfWalls[iBa],
                        buildingAcoustics[iBa].ReceiverPositionsSs);
                    posUpdate = true;
                }

            //Debug.Log("Update Receiver Filter!");

            ForceReceiverUpdate = false;
            sw.Stop();
            if (sw.Elapsed.TotalMilliseconds > 0)
                ReceiverUpdate = sw.Elapsed.TotalMilliseconds;
            return posUpdate;
        }

        /// <summary>
        /// Implementation of Update Source Data for filter Construction if Source Update
        /// </summary>
        /// <returns>Source Update</returns>
        protected override bool RealTimeSourceUpdate()
        {
            var sw = new Stopwatch();
            sw.Start();
            var posUpdate = false;
            if (SoundSourceObjects[0].SoundObjectChanged() | ForceSourceUpdate)
            {
                for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
                {
                    SoundSourceObjects[0]
                        .GetSourceFiForAllSumSs(buildingAcoustics[iBa].sourceWalls, _rtSourceWalls[iBa]);
                    SoundSourceObjects[0]
                        .GetSourceFiForAllSumSs(buildingAcoustics[iBa].partition.Patches.ToArray(), _rtSourcePatches[iBa]);
                }

                _updateMonoFilter = true;
            }

            ForceSourceUpdate = false;
            sw.Stop();
            if (sw.Elapsed.TotalMilliseconds > 0)
                SourceUpdate = sw.Elapsed.TotalMilliseconds;
            return posUpdate;
        }

        #region Tests

        /// <summary>
        /// Run New Approach Indoor and Log results separably for each part of ir (dd, dr, rd, rr)
        /// </summary>
        public void TestCalcNewApproachWithLogging()
        {
            OfflineCalculation(null);
            LogRealtime = false;
            HrirCounter = 0;

            var dirDirPartPatches = ComplexFloats.CopyTo(_dirDirFreqPartitionPatches);
            var dirRevPartPatches = ComplexFloats.CopyTo(_dirRevFreqPartitionPatches);
            var revDirPartPatches = ComplexFloats.CopyTo(_revDirFreqPartitionPatches);
            var revRevPartPatches = ComplexFloats.CopyTo(_revRevFreqPartitionPatches);

            var dirDirFlanking = ComplexFloats.CopyTo(_dirDirFreqFlanking);
            var dirRevFlanking = ComplexFloats.CopyTo(_dirRevFreqFlanking);
            var revDirFlanking = ComplexFloats.CopyTo(_revDirFreqFlanking);
            var revRevFlanking = ComplexFloats.CopyTo(_revRevFreqFlanking);

            var dirDirPartition = ComplexFloats.CopyTo(_dirDirFreqPartition);
            var dirRevPartition = ComplexFloats.CopyTo(_dirRevFreqPartition);
            var revDirPartition = ComplexFloats.CopyTo(_revDirFreqPartition);
            var revRevPartition = ComplexFloats.CopyTo(_revRevFreqPartition);

            LogAllData();

            // All Paths
            RealTimeCalculation();
            HrirCounter++;

            // Just DirDir
            SetAllNewApproachOfflineArraysZero();
            _dirDirFreqPartitionPatches = ComplexFloats.CopyTo(dirDirPartPatches);
            _dirDirFreqFlanking = ComplexFloats.CopyTo(dirDirFlanking);
            _dirDirFreqPartition = ComplexFloats.CopyTo(dirDirPartition);
            RealTimeCalculation();
            HrirCounter++;

            // Just DirRev
            SetAllNewApproachOfflineArraysZero();
            _dirRevFreqFlanking = ComplexFloats.CopyTo(dirRevFlanking);
            _dirRevFreqPartitionPatches = ComplexFloats.CopyTo(dirRevPartPatches);
            _dirRevFreqPartition = ComplexFloats.CopyTo(dirRevPartition);
            RealTimeCalculation();
            HrirCounter++;

            // Just RevDir
            SetAllNewApproachOfflineArraysZero();
            _revDirFreqFlanking = ComplexFloats.CopyTo(revDirFlanking);
            _revDirFreqPartitionPatches = ComplexFloats.CopyTo(revDirPartPatches);
            _revDirFreqPartition = ComplexFloats.CopyTo(revDirPartition);
            RealTimeCalculation();
            HrirCounter++;

            // Just RevRev
            SetAllNewApproachOfflineArraysZero();
            _revRevFreqFlanking = ComplexFloats.CopyTo(revRevFlanking);
            _revRevFreqPartitionPatches = ComplexFloats.CopyTo(revRevPartPatches);
            _revRevFreqPartition = ComplexFloats.CopyTo(revRevPartition);
            RealTimeCalculation();
            HrirCounter++;

            var timePrefix = DateTime.Now.ToString("-yyyyMMdd-HHmmss-");
            timePrefix = "TestCalcNewApproachWithLogging" + timePrefix;

            MoveLogFiles(timePrefix);
        }


        private void SetAllNewApproachOfflineArraysZero()
        {
            SetToZero(_dirDirFreqPartitionPatches);
            SetToZero(_dirRevFreqPartitionPatches);
            SetToZero(_revDirFreqPartitionPatches);
            SetToZero(_revRevFreqPartitionPatches);
            SetToZero(_dirDirFreqFlanking);
            SetToZero(_dirRevFreqFlanking);
            SetToZero(_revDirFreqFlanking);
            SetToZero(_revRevFreqFlanking);
            SetToZero(_dirDirFreqPartition);
            SetToZero(_dirRevFreqPartition);
            SetToZero(_revDirFreqPartition);
            SetToZero(_revRevFreqPartition);
        }


        private void SetToZero(float[][] ir)
        {
            for (var i = 0; i < ir.Length; i++)
                SetToZero(ir[i]);
        }

        private void SetToZero(float[] ir)
        {
            for (var i = 0; i < ir.Length; i++)
                ir[i] = 0f;
        }

        #endregion
    }
}