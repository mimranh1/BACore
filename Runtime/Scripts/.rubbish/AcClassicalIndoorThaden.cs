﻿using System;
using System.Diagnostics;
using log4net;
using UnityEngine;

namespace BA.BACore
{
    /// <summary>
    /// Implementation of Classical Indoor Thaden Approach considering diffuse Sound field in Source room and direct and reverberation in receiver room 
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    [Serializable]
    public class AcClassicalIndoorThaden : AcClassicalIndoor
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(AcClassicalIndoorThaden));

        /// <summary>
        /// Defines if the reverberation will be convolved with hrtf or not
        /// </summary>
        public bool binauralReverberation;

        // Thaden 
        private float[][] _tauTimeRevThaden = new float[5][];
        private float[][] _irMonoFreqComplexThaden = new float[5][];
        private float[][] _irDirectFreqComplexThaden = new float[5][];
        private int _directSamplesThaden;
        private readonly int[] _directSamplesDelayFormula = new int[5];


        public override AuralisationMethod AuralisationMethod => AuralisationMethod.ClassicalIndoorModelThaden;

        /// <summary>
        /// Implementation of Initialisation with Init BuildingAcoustics Array from FilterManager
        /// </summary>
        /// <param name="bas">buildingAcoustics Array from FilterManager</param>
        public override void Init()
        {
            moduleName = "AcClassicalIndoorThaden";
            init = true;
            LogName = "FilterManager.AcClassicalIndoor";
        }


        /// <summary>
        /// Implementation of Calculate pre-calculation and and set AudioRenderer
        /// </summary>
        /// <param name="ar">AudioRenderer from FilterManager</param>
        public override void OfflineCalculation(AudioRender ar)
        {
            InterpolateSecondarySources();
            SoundReceiverObject.Init(FilterResolution);
            OfflineCalculationThadenNewApproach();
        }


        /// <summary>
        /// Implementation of Realtime Update, called if Source or Receiver Updates
        /// </summary>
        /// <param name="logId">write log file if logId > 1</param>
        public override void RealTimeCalculation(int logId = 0)
        {
            var sw = new Stopwatch();
            sw.Start();
            RealtimeCalculationThadenNewApproach();
            sw.Stop();
            ReceiverUpdate = sw.Elapsed.TotalMilliseconds;
        }

        /// <summary>
        /// Implementation of Update Receiver Data for filter Construction if Receiver Update
        /// </summary>
        /// <returns>Receiver Update</returns>
        public override bool RealTimeReceiverUpdate()
        {
            return SoundReceiverObject.SoundObjectChanged();
        }

        /// <summary>
        /// Implementation of Update Source Data for filter Construction if Source Update
        /// </summary>
        /// <returns>Source Update</returns>
        public override bool RealTimeSourceUpdate()
        {
            return false;
        }


        private void OfflineCalculationThadenNewApproach()
        {
            buildingAcoustic.ProcessAll();
            var nTFactor = Mathf.Sqrt(buildingAcoustic.receiverRoom.Properties.ReverberationTime / 0.5f *
                                      (buildingAcoustic.partition.Geometry.Area /
                                       (0.32f * buildingAcoustic.receiverRoom.Properties.volume)));
            TauFreqComplex = CalcTauFreq(TauInterpolatedSecondarySources, nTFactor);

            _directSamplesThaden = (int) (0.05 * 44100.0f);

            //var rev = GetReverberationTrail();
            var irMono = GetImpulseResponseForThadenBinauralReverberation(TauFreqComplex,
                buildingAcoustic.receiverRoom.reverberation, _directSamplesThaden);
            _irMonoFreqComplexThaden = FftHelper.Fft(irMono, true);
            var irDirect = GetImpulseResponseDirectForThadenMonoReverberation();

            // prepare for monaural Reverberation
            _irDirectFreqComplexThaden = FftHelper.Fft(irDirect, true);
            //InitHrtfFft(irMonoFreqComplexThaden[0].Length / 2);
            _tauTimeRevThaden = GetTauReverberationTailTime(TauFreqComplex, buildingAcoustic.receiverRoom.reverberation);
            if (!DataLogger.DebugData)
            {
                DataLogger.Log(LogName + "Thaden.TauFreqComplex", TauFreqComplex);
                DataLogger.Log(LogName + "Thaden.IrMonoFreqComplex{1," + (HrirCounter + 1) + "}", _irMonoFreqComplexThaden);
                DataLogger.Log(LogName + "Thaden.IrMonoTime{1," + (HrirCounter + 1) + "}", irMono);
                DataLogger.Log(LogName + "Thaden.TauRev{1," + (HrirCounter + 1) + "}", _tauTimeRevThaden);
                DataLogger.Log(LogName + "Thaden.DirectSamples", _directSamplesThaden);
                DataLogger.Log(LogName + "Thaden.Reverberation", buildingAcoustic.receiverRoom.reverberation);
            }
        }


        private void RealtimeCalculationThadenNewApproach()
        {
            var irTime = new float[2][];
            var distanceReceiver = buildingAcoustic.CalcReceiverDistanceWalls();
            for (var i = 0; i < 5; i++)
                _directSamplesDelayFormula[i] = (int) (BaSettings.Instance.SampleRate * (distanceReceiver[i] / BaSettings.Instance.C0));

            if (binauralReverberation)
            {
                var hrtf = SoundReceiverObject.GetHrtf(_irMonoFreqComplexThaden[0].Length / 2,
                    _directSamplesDelayFormula);
                var irFreqComplex = GetBinauralIrFreqAndSum(_irMonoFreqComplexThaden, hrtf);
                irTime = FftHelper.Ifft(irFreqComplex);
                if (LogRealtime)
                {
                    DataLogger.Log(LogName + "Thaden.IrFreqComplex{1," + (HrirCounter + 1) + "}", irFreqComplex);
                    DataLogger.Log(LogName + "Thaden.HrtfFreqComplex{1," + (HrirCounter + 1) + "}", hrtf);
                }
            }
            else
            {
                var hrtf = SoundReceiverObject.GetHrtf(TauFreqComplex[0].Length / 2,
                    _directSamplesDelayFormula);
                var directBinauralFreqComplex = GetBinauralIrFreq(_irDirectFreqComplexThaden, hrtf);
                var directBinauralTime = FftHelper.Ifft(directBinauralFreqComplex);
                irTime = CombineFiltersForThadenMonauralReverberation(directBinauralTime, _tauTimeRevThaden,
                    _directSamplesThaden, true);
                if (LogRealtime)
                {
                    DataLogger.Log(LogName + "Thaden.DirectBinauralFreqComplex{1," + (HrirCounter + 1) + "}",
                        directBinauralFreqComplex);
                    DataLogger.Log(LogName + "Thaden.DirectBinauralTime{1," + (HrirCounter + 1) + "}",
                        directBinauralTime);
                    DataLogger.Log(LogName + "Thaden.HrtfFreqComplex{1," + (HrirCounter + 1) + "}", hrtf);
                }
            }

            audioRenderer.UpdateFilter(irTime[0], irTime[1]);
            if (LogRealtime) DataLogger.Log(LogName + "Thaden.irTime{1," + (HrirCounter + 1) + "}", irTime);
        }

        private float[][] GetImpulseResponseForThadenBinauralReverberation(float[][] localTauFreqComplex,
            float[] reverberationTail, int directLength)
        {
            var ir = new float[5][];
            var distance = buildingAcoustic.CalcReceiverDistanceWalls();
            ;
            var equalAbsArea = buildingAcoustic.receiverRoom.Properties.EquivalentAbsorptionArea;
            var revTau = GetTauReverberationTailTime(localTauFreqComplex, reverberationTail);

            for (var i = 0; i < 5; i++)
            {
                var tau = FftHelper.Ifft(localTauFreqComplex[i]);
                var lengthIr = revTau[i].Length + directLength;
                ir[i] = new float[lengthIr];
                var sqrtEnergyTauByEnergyRev = Mathf.Sqrt(GetSignalEnergy(tau) / GetSignalEnergy(revTau[i]));
                var sqrtEnergyDirByEnergyRev = Mathf.Sqrt(equalAbsArea / (16 * Mathf.PI * distance[i] * distance[i]));
                for (var j = directLength; j < revTau[i].Length + directLength; j++)
                    ir[i][j] = sqrtEnergyTauByEnergyRev * revTau[i][j - directLength];
                for (var j = 0; j < tau.Length; j++)
                    ir[i][j] += sqrtEnergyDirByEnergyRev * tau[j];
                if (LogRealtime)
                {
                    DataLogger.Log(LogName + "Thaden.TauTimeMono(:," + (i + 1) + ")", tau);
                    DataLogger.Log(LogName + "Thaden.SqrtEnergyTauByEnergyRev(:," + (i + 1) + ")",
                        sqrtEnergyTauByEnergyRev);
                    DataLogger.Log(LogName + "Thaden.SqrtEnergyDirByEnergyRev(:," + (i + 1) + ")",
                        sqrtEnergyDirByEnergyRev);
                    DataLogger.Log(LogName + "Thaden.DirectLength(:," + (i + 1) + ")", directLength);
                }
            }

            return ir;
        }

        /// <summary>
        ///     Add Direct and Reverberation Part with delay directLength and adjust the Energies according to Thaden
        /// </summary>
        /// <param name="direct">the direct float[][] of length 2, 5 or 10</param>
        /// <param name="reverberationThaden">ReverberationTail of length 5</param>
        /// <param name="directLength">Number of samples of Direct Sound, before reverberation will be added</param>
        /// <param name="sumOutput">if output channels should be summed up</param>
        /// <returns></returns>
        private float[][] CombineFiltersForThadenMonauralReverberation(float[][] direct, float[][] reverberationThaden,
            int directLength, bool sumOutput)
        {
            var numChannelDirect = direct.Length;
            if (numChannelDirect != 2 && numChannelDirect != 5 && numChannelDirect != 10 || reverberationThaden.Length != 5)
                Log.Error("channel num is not valid!");

            float[][] ir;
            if (sumOutput)
            {
                ir = new float[2][];
                var lengthIr = reverberationThaden[0].Length + directLength;
                ir[0] = new float[lengthIr];
                ir[1] = new float[lengthIr];
            }
            else
            {
                ir = new float[numChannelDirect][];
            }

            var equalAbsArea = buildingAcoustic.receiverRoom.Properties.EquivalentAbsorptionArea;
            var indexReverberation = 0;
            var distance = buildingAcoustic.CalcReceiverDistanceWalls();
            ;
            for (var i = 0; i < numChannelDirect; i++)
            {
                int indexIr;
                if (numChannelDirect == 10)
                    indexReverberation = i / 2;
                if (numChannelDirect == 5)
                    indexReverberation = i;
                if (numChannelDirect == 2)
                    indexReverberation = 0;
                if (sumOutput)
                {
                    indexIr = i % 2;
                }
                else
                {
                    indexIr = i;
                    var lengthIr = reverberationThaden[indexReverberation].Length + directLength;
                    ir[indexIr] = new float[lengthIr];
                }

                var energyDirect =
                    Mathf.Min(GetSignalEnergy(direct[indexReverberation]), GetSignalEnergy(direct[indexReverberation + 1]));
                var sqrtEnergyTauByEnergyRev =
                    Mathf.Sqrt(energyDirect / GetSignalEnergy(reverberationThaden[indexReverberation]));
                var sqrtEnergyDirByEnergyRev = Mathf.Sqrt(equalAbsArea /
                                                          (16 * Mathf.PI * distance[indexReverberation] *
                                                           distance[indexReverberation]));
                for (var j = directLength; j < reverberationThaden[indexReverberation].Length + directLength; j++)
                    ir[indexIr][j] += sqrtEnergyTauByEnergyRev * reverberationThaden[indexReverberation][j - directLength];
                for (var j = 0; j < direct[i].Length; j++)
                    ir[indexIr][j] += sqrtEnergyDirByEnergyRev * direct[i][j];

                if (LogRealtime)
                {
                    DataLogger.Log(
                        LogName + "Thaden.SqrtEnergyTauByEnergyRev{1," + (HrirCounter + 1) + "}(:," + (i + 1) + ")",
                        sqrtEnergyTauByEnergyRev);
                    DataLogger.Log(
                        LogName + "Thaden.DirectBinauralTime{1," + (HrirCounter + 1) + "}(:," + (i + 1) + ")",
                        sqrtEnergyDirByEnergyRev);
                }
            }

            return ir;
        }


        private float[][] GetImpulseResponseDirectForThadenMonoReverberation()
        {
            var ir = new float[5][];

            for (var i = 0; i < 5; i++)
                ir[i] = FftHelper.Ifft(TauFreqComplex[i]);

            return ir;
        }


        private static float GetSignalEnergy(float[] input)
        {
            var sumSquare = 0f;
            foreach (var sample in input) sumSquare += sample * sample;

            return sumSquare;
        }


        /// <summary>
        /// Run Thaden Setting and Log the latency
        /// </summary>
        /// <param name="runNumbers">number of the runs for the realtime update</param>
        public void TestCalcThaden(int runNumbers = 100)
        {
            LoggingConfiguration.ConfigureViaFileRealTimeLatency();
            DataLogger.DebugData = false;
            LogRealtime = false;
            var sw = new Stopwatch();
            var swRt = new Stopwatch();
            var receiver = SoundReceiverObject.transform;
            var oldTPosition = receiver.position;
            var oldTRotation = receiver.rotation;

            sw.Start();

            OfflineCalculationThadenNewApproach();
            sw.Stop();
            var latencyRealtime = new double[runNumbers];
            var oldTransformRotation = SoundReceiverObject.transform.rotation;
            for (var i = 0; i < runNumbers; i++)
            {
                SoundReceiverObject.transform.Rotate(Vector3.up, 20);
                swRt.Restart();
                SoundReceiverObject.UpdateHrirIndex(partition.Junctions.ReceiverPositions);
                RealtimeCalculationThadenNewApproach();
                swRt.Stop();
                latencyRealtime[i] = swRt.Elapsed.TotalMilliseconds;
            }

            Log.Info("Test Thaden runs in " + sw.Elapsed.TotalSeconds + " seconds (Realtime(mean n=" +
                     runNumbers + "): " + swRt.Elapsed.TotalMilliseconds / runNumbers + " ms).!");
            SoundReceiverObject.transform.position = oldTPosition;
            SoundReceiverObject.transform.rotation = oldTRotation;
            DataLogger.DebugData = true;
            DataLogger.Log("Indoor.Thaden.latencyRealtime", latencyRealtime);
            DataLogger.Log("Indoor.Thaden.OfflineTime", sw.Elapsed.TotalSeconds);
        }
    }
}