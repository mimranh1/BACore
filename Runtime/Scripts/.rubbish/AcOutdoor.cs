﻿using System;
using System.Diagnostics;
using System.IO;
using log4net;
using UnityEngine;

namespace BA.BACore
{
    /// <summary>
    /// Implementation of new Approach Outdoor with Patches
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    [Serializable]
    public class AcOutdoor : AcNewApproachPartition
    {
        private float[][][] realTimeSourceNewApproachOutdoorFreqPartition = new float[1][][];
        private float[][][] realTimeReceiverNewApproachOutdoorFreqPartition = new float[1][][];
        private float[][][] dirDirNewApproachOutdoorFreqPartition = new float[1][][];
        private float[][][] dirRevNewApproachOutdoorFreqPartition = new float[1][][];
        private int[][] rjSamplesDelay = new int[1][];

        private static readonly ILog Log = LogManager.GetLogger(typeof(AcOutdoor));


        public override AuralisationMethod AuralisationMethod => AuralisationMethod.OutdoorModel;

        /// <summary>
        /// Implementation of Initialisation with Init BuildingAcoustics Array from FilterManager
        /// </summary>
        /// <param name="bas">buildingAcoustics Array from FilterManager</param>
        public override void Init()
        {
            moduleName = "AcOutdoor";
            init = true;
            LogName = "FilterManager.AcOutdoor";
        }

        /// <summary>
        /// Implementation of Calculate pre-calculation and and set AudioRenderer
        /// </summary>
        /// <param name="ar">AudioRenderer from FilterManager</param>
        protected override void OfflineCalculation(AudioRender ar)
        {
            audioRenderer = ar;
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++) buildingAcoustics[iBa].ProcessAll();
            SoundSourceObjects[0].Init(FilterResolution);
            SoundReceiverObject.Init(FilterResolution);

            var baLength = buildingAcoustics.Length;
            dirDirNewApproachOutdoorFreqPartition = new float[baLength][][];
            dirRevNewApproachOutdoorFreqPartition = new float[baLength][][];
            UpdateBaVisible();
            for (var iBa = 0; iBa < baLength; iBa++)
            {
                var partition = buildingAcoustics[iBa].partition;
                var areaSd = partition.Geometry.Area;
                var rhoC0 = BaSettings.Instance.C0 * BaSettings.Instance.Rho0;
                var equivalentAbsAreaReceiver = buildingAcoustics[iBa].receiverRoom.Properties.EquivalentAbsorptionArea;
                var revReceiverFreqComplex =
                    FftHelper.Fft(buildingAcoustics[iBa].receiverRoom.reverberation, true, FilterResolution);

                if (DataLogger.DebugData)
                {
                    DataLogger.Log(LogName + "OutdoorModel.areaSd(:," + (iBa + 1) + ")", areaSd);
                    DataLogger.Log(LogName + "OutdoorModel.rhoC0", rhoC0);
                    DataLogger.Log(LogName + "OutdoorModel.revReceiverFreqComplex{" + (iBa + 1) + "}",
                        revReceiverFreqComplex);
                    DataLogger.Log(LogName + "OutdoorModel.Ar{" + (iBa + 1) + "}", equivalentAbsAreaReceiver);
                    DataLogger.Log(LogName + "OutdoorModel.SourceSoundPowerNewApproach",
                        SourceSoundPowerNewApproach);
                }

                var numPatches = partition.patchesHandler.NumOfPatches;
                dirDirNewApproachOutdoorFreqPartition[iBa] = new float[numPatches][];
                dirRevNewApproachOutdoorFreqPartition[iBa] = new float[numPatches][];
                for (var iPatch = 0; iPatch < partition.patchesHandler.NumOfPatches; iPatch++)
                {
                    var tau3RdOctaveFreq = 10 ^ (partition.patchesHandler.ReductionIndexPatches[iPatch] / -10f);
                    DataLogger.Log(
                        LogName + "OutdoorModel.tau3rdOctaveFreq{1," + (iBa + 1) + "}(:," + (iPatch + 1) + ")",
                        tau3RdOctaveFreq);
                    var patchArea = partition.Patches[iPatch].Geometry.Area;
                    var tauInterpolatedFreq = CalcTauExtrapolatedFrom3RdOctaveTau(tau3RdOctaveFreq, FilterResolution);
                    var tauSqrt1PhaseFreqComplex =
                        CalcSqrtTauComplexFromTauInterpolatedEven(tauInterpolatedFreq.Tf, LogName, 1, 2);
                    //tau0PhaseFreqComplex = tau1PhaseFreqComplex;
                    var preFactor = Mathf.Sqrt(400 * 400 * SourceSoundPowerNewApproach * areaSd / (rhoC0 * patchArea));

                    DataLogger.Log(LogName + "OutdoorModel.TestCalcNewApproachOutdoor{" + (iBa + 1) + "}(:," +
                                   (iPatch + 1) + ")", tauInterpolatedFreq);
                    DataLogger.Log(LogName + "OutdoorModel.patchArea{" + (iBa + 1) + "}(:," +
                                   (iPatch + 1) + ")", patchArea);
                    DataLogger.Log(LogName + "OutdoorModel.tauIdJunIdPath3rdOctaveFreq{" + (iBa + 1) + "}(:," +
                                   (iPatch + 1) + ")", tau3RdOctaveFreq);
                    DataLogger.Log(LogName + "OutdoorModel.tauIdJunIdPathFreqComplex{" + (iBa + 1) + "}(:," +
                                   (iPatch + 1) + ")", tauSqrt1PhaseFreqComplex);
                    DataLogger.Log(LogName + "OutdoorModel.preFactor{" + (iBa + 1) + "}(:," +
                                   (iPatch + 1) + ")", preFactor);

                    var factorDirDir = preFactor / (4 * Mathf.PI);
                    var factorDirRev = preFactor * Mathf.Sqrt(1 / (Mathf.PI * equivalentAbsAreaReceiver));

                    // Calculate Direct-Direct Pre Filter
                    dirDirNewApproachOutdoorFreqPartition[iBa][iPatch] =
                        ComplexFloats.Multiply(tauSqrt1PhaseFreqComplex, factorDirDir);

                    // Calculate Direct-Rev Pre Filter
                    dirRevNewApproachOutdoorFreqPartition[iBa][iPatch] =
                        ComplexFloats.Multiply(revReceiverFreqComplex, tauSqrt1PhaseFreqComplex, factorDirRev);


                    DataLogger.Log(
                        LogName + "OutdoorModel.factorPartitionDirDir(" + (iBa + 1) + "," + (iPatch + 1) + ")",
                        factorDirDir);
                    DataLogger.Log(
                        LogName + "OutdoorModel.factorPartitionDirRev(" + (iBa + 1) + "," + (iPatch + 1) + ")",
                        factorDirRev);
                }
            }

            if (DataLogger.DebugData)
            {
                DataLogger.Log(LogName + "OutdoorModel.dirDirPartitionFreqComplex",
                    dirDirNewApproachOutdoorFreqPartition);
                DataLogger.Log(LogName + "OutdoorModel.dirRevPartitionFreqComplex",
                    dirRevNewApproachOutdoorFreqPartition);
            }

            Log.Debug("Call OfflineCalculationNewApproachOutdoor");
            realTimeSourceNewApproachOutdoorFreqPartition = new float[baLength][][];
            realTimeReceiverNewApproachOutdoorFreqPartition = new float[baLength][][];
            for (var iBa = 0; iBa < baLength; iBa++)
            {
                realTimeSourceNewApproachOutdoorFreqPartition[iBa] =
                    SoundSourceObjects[0].GetSourceFiForPatches(buildingAcoustics[iBa].partition,
                        FilterResolution, out var ri, true);
                realTimeReceiverNewApproachOutdoorFreqPartition[iBa] = SoundReceiverObject
                    .GetHrtfForSsPatches(FilterResolution, buildingAcoustics[iBa].partition.SecondarySourcesPosPatches,
                        out rjSamplesDelay[iBa], out _, out _, true);
            }
        }


        /// <summary>
        /// Implementation of Realtime Update, called if Source or Receiver Updates
        /// </summary>
        /// <param name="logId">write log file if logId > 1</param>
        protected override void RealTimeCalculation(int logId = 0)
        {
            var ifDataLog = LogRealtime;
            if (ifDataLog)
                Log.Debug("Call RealtimeCalculationNewApproachFilterOutdoor");
            var sw = new Stopwatch();
            sw.Start();
            var sumAllIrLeftFreqComplex = new float[2 * FilterResolution];
            var sumAllIrRightFreqComplex = new float[2 * FilterResolution];
            var ifAnythingChanged = false;
            // Start Partition Wall
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            {
                var patchesActive = buildingAcoustics[iBa].partition.patchesHandler.IsActive;
                if (!buildingAcoustics[iBa].IsVisible)
                    continue;
                for (var iPatch = 0; iPatch < realTimeSourceNewApproachOutdoorFreqPartition[iBa].Length; iPatch++)
                {
                    if (!patchesActive[iPatch])
                        continue;
                    ifAnythingChanged = true;
                    var ssBinComplex = new float[1];
                    if (ifDataLog) ssBinComplex = new float[2 * FilterResolution];

                    var deltaRj = FftHelper.GetPhaseWithDelay(rjSamplesDelay[iBa][iPatch], FilterResolution);
                    var dirDir = dirDirNewApproachOutdoorFreqPartition[iBa][iPatch];
                    var dirRev = dirRevNewApproachOutdoorFreqPartition[iBa][iPatch];
                    //var dirRev = new float[2 * filterResolution];
                    var rtReceiverLeft = realTimeReceiverNewApproachOutdoorFreqPartition[iBa][2 * iPatch];
                    var rtReceiverRight = realTimeReceiverNewApproachOutdoorFreqPartition[iBa][2 * iPatch + 1];
                    var rtSource = realTimeSourceNewApproachOutdoorFreqPartition[iBa][iPatch];

                    for (var n = 0; n < 2 * FilterResolution; n += 2)
                    {
                        var dirDirReal = dirDir[n];
                        var dirDirImag = dirDir[n + 1];

                        var dirRevReal = dirRev[n] * deltaRj[n] - dirRev[n + 1] * deltaRj[n + 1];
                        var dirRevImag = dirRev[n] * deltaRj[n + 1] + dirRev[n + 1] * deltaRj[n];

                        var ssBinRealLeft = dirDirReal * rtReceiverLeft[n] - dirDirImag * rtReceiverLeft[n + 1] +
                                            dirRevReal;
                        var ssBinImagLeft = dirDirImag * rtReceiverLeft[n] + dirDirReal * rtReceiverLeft[n + 1] +
                                            dirRevImag;

                        var ssBinRealRight = dirDirReal * rtReceiverRight[n] - dirDirImag * rtReceiverRight[n + 1] +
                                             dirRevReal;
                        var ssBinImagRight = dirDirImag * rtReceiverRight[n] + dirDirReal * rtReceiverRight[n + 1] +
                                             dirRevImag;

                        if (ifDataLog)
                        {
                            ssBinComplex[n] = ssBinRealLeft;
                            ssBinComplex[n + 1] = ssBinImagRight;
                        }

                        var rtSourceReal = rtSource[n];
                        var rtSourceImag = rtSource[n + 1];

                        sumAllIrLeftFreqComplex[n] += ssBinRealLeft * rtSourceReal - ssBinImagLeft * rtSourceImag;
                        sumAllIrLeftFreqComplex[n + 1] += ssBinRealLeft * rtSourceImag + ssBinImagLeft * rtSourceReal;

                        sumAllIrRightFreqComplex[n] += ssBinRealRight * rtSourceReal - ssBinImagRight * rtSourceImag;
                        sumAllIrRightFreqComplex[n + 1] += ssBinRealRight * rtSourceImag + ssBinImagRight * rtSourceReal;
                    }

                    if (ifDataLog)
                    {
                        Log.Debug("Call RealtimeCalculationNewApproachFilterOutdoor");
                        DataLogger.Log(
                            LogName + "OutdoorModel.ssBinComplexFreqComplex{1," + (HrirCounter + 1) + "}(:,1)",
                            ssBinComplex);
                        DataLogger.Log(
                            LogName + "OutdoorModel.IrSumLeftFreqComplex{1," + (HrirCounter + 1) + "}(:,1)",
                            sumAllIrLeftFreqComplex);
                        DataLogger.Log(
                            LogName + "OutdoorModel.IrSumRightFreqComplex{1," + (HrirCounter + 1) + "}(:,1)",
                            sumAllIrRightFreqComplex);
                    }
                }
            }

            var irTimeLeft = new float[FilterResolution];
            var irTimeRight = new float[FilterResolution];
            if (ifAnythingChanged)
            {
                irTimeLeft = FftHelper.Ifft(sumAllIrLeftFreqComplex);
                irTimeRight = FftHelper.Ifft(sumAllIrRightFreqComplex);
                if (ifDataLog)
                {
                    Log.Debug("Call RealtimeCalculationNewApproachFilterOutdoor");
                    DataLogger.Log(
                        LogName + "OutdoorModel.realTimeReceiverNewApproachOutdoorFreqPartitionFreqComplex",
                        realTimeReceiverNewApproachOutdoorFreqPartition);
                    DataLogger.Log(
                        LogName + "OutdoorModel.realTimeSourceNewApproachOutdoorFreqPartitionFreqComplex",
                        realTimeSourceNewApproachOutdoorFreqPartition);
                    DataLogger.Log(LogName + "OutdoorModel.IrTimeLeft{1," + (HrirCounter + 1) + "}", irTimeLeft);
                    DataLogger.Log(LogName + "OutdoorModel.IrTimeRight{1," + (HrirCounter + 1) + "}", irTimeRight);
                }
            }

            if (!ifDataLog)
                audioRenderer.UpdateFilter(irTimeLeft, irTimeRight);
            sw.Stop();
            FilterUpdate = sw.ElapsedMilliseconds;
        }

        /// <summary>
        /// Implementation of Update Receiver Data for filter Construction if Receiver Update
        /// </summary>
        /// <returns>Receiver Update</returns>
        protected override bool RealTimeReceiverUpdate()
        {
            var sw = new Stopwatch();
            sw.Start();
            var posUpdate = false;
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
                if (SoundReceiverObject.SoundObjectChanged() | ForceReceiverUpdate)
                {
                    //Debug.Log("Receiver Update " + iBa);
                    realTimeReceiverNewApproachOutdoorFreqPartition[iBa] =
                        SoundReceiverObject
                            .GetHrtfForSsPatches(FilterResolution,
                                buildingAcoustics[iBa].partition.SecondarySourcesPosPatches, out rjSamplesDelay[iBa], out _,
                                out _);
                    posUpdate = true;
                    ForceReceiverUpdate = false;
                }

            sw.Stop();
            if (sw.ElapsedMilliseconds > 0)
                ReceiverUpdate = sw.ElapsedMilliseconds;
            return posUpdate;
        }

        /// <summary>
        /// Implementation of Update Source Data for filter Construction if Source Update
        /// </summary>
        /// <returns>Source Update</returns>
        protected override bool RealTimeSourceUpdate()
        {
            var sw = new Stopwatch();
            sw.Start();
            var posUpdate = false;
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
                if (SoundSourceObjects[0].SoundObjectChanged() | ForceSourceUpdate)
                {
                    //Debug.Log("Source Update " + iBa);
                    posUpdate = true;
                    realTimeSourceNewApproachOutdoorFreqPartition[iBa] =
                        SoundSourceObjects[0].GetSourceFiForPatches(buildingAcoustics[iBa].partition,
                            FilterResolution, out _);
                    ForceSourceUpdate = false;
                }

            sw.Stop();
            if (sw.ElapsedMilliseconds > 0)
                SourceUpdate = sw.ElapsedMilliseconds;
            return posUpdate;
        }

        /// <summary>
        /// test and Log the New Approach Outdoor
        /// </summary>
        public void TestCalcNewApproachOutdoor()
        {
            DataLogger.DebugData = true;
            LogRealtime = true;
            Log.Debug("Call TestCalcNewApproachOffline(true)");
            OfflineCalculation(null);
            Log.Debug("Call RealtimeCalculationNewApproachFilterOutdoor");
            RealTimeCalculation();

            var timePrefix = DateTime.Now.ToString("-yyyyMMdd-HHmmss-");
            timePrefix = "TestCalcNewApproachRealTime" + timePrefix;
            Log.Debug("Call OnDisable");

            MoveLogFiles(timePrefix);
            LogRealtime = false;
        }

        /// <summary>
        /// Test moving Outdoor Source 
        /// </summary>
        public void TestMovingOutdoorSource()
        {
            for (var deltaX = 0.1f; deltaX < 1f; deltaX += 0.1f)
            {
                var timePrefix = "testOutdoorMoving " + deltaX.ToString("0.00") + " ";
                if (File.Exists($"{BaSettings.Instance.RootPath}/Log/" + timePrefix + "logDataFile.txt"))
                    continue;

                LoggingConfiguration.ConfigureViaFile();
                LogRealtime = false;
                DataLogger.DebugData = false;
                Init();
                OfflineCalculation(null);
                var source = audioRenderer.SourceMover;
                source.RestartPositionAndSound();
                var oldPos = source.transform.position;
                var logId = 0;

                LogRealtime = false;
                DataLogger.DebugData = false;
                source.RestartPositionAndSound();
                while (logId < 100)
                {
                    source.transform.position = source.UpdatePosition(source.transform.position, deltaX);
                    HrirCounter++;
                    RealTimeSourceUpdate();
                    RealTimeCalculation(logId);

                    DataLogger.DebugData = true;
                    DataLogger.Log(LogName + "sourceSS.Position(" + (logId + 1) + ",:)", source.Position);
                    //DataLogger.Log(LogName + "rtSourceFreqComplex", realTimeSourceNewApproachOutdoorFreqPartition, logId);
                    //DataLogger.Log(LogName + "rtReceiverFreqComplex", realTimeReceiverNewApproachOutdoorFreqPartition, logId);
                    DataLogger.DebugData = false;
                    logId++;
                }

                source.transform.position = oldPos;
                MoveLogFiles(timePrefix);
            }
        }
    }
}