﻿using System;
using log4net;
using UnityEngine;
using UnityEngine.Serialization;

namespace BA.BACore
{
    /// <summary>
    /// Base class of the Classical Indoor Approaches (Thaden and Formula) 
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) 
    /// Version:        1.2  
    /// First release:  2017 
    /// Last revision:  2019 
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    [Serializable]
    public abstract class AcClassicalIndoor : AuralisationCalculation
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(AcClassicalIndoor));

        protected TransferFunction InterpolatedFrequencyBand;
        protected readonly TransferFunction[] TauInterpolatedSecondarySources = new TransferFunction[5];
        protected float[][] TauFreqComplex = new float[5][];

        public override SoundSourceObject[] SoundSourceObjects => new[] {soundSourceObjects};
        [SerializeField] protected SoundSourceObject soundSourceObjects;

        public override SoundReceiverObject SoundReceiverObject => soundReceiverObject;
        [SerializeField] protected SoundReceiverObject soundReceiverObject;
        
        [SerializeField] protected RoomAcousticsBehaviour roomAcousticsBehaviour;
        [SerializeField] protected WallBehaviour partition;
        
        
        
         /// <summary>
        /// Overload of Initialisation with Init BuildingAcoustics Array from FilterManager
        /// </summary>
        public override void Init()
        {
            moduleName = "AcClassicalIndoor";
            init = true;
            LogName = "FilterManager.AcClassicalIndoor";
        }


        /// <summary>
        /// Implementation of Update Receiver Data for filter Construction if Receiver Update
        /// </summary>
        /// <returns>Receiver Update</returns>
        public override bool RealTimeReceiverUpdate()
        {
            return false;
        }

        /// <summary>
        /// Implementation of Update Source Data for filter Construction if Source Update
        /// </summary>
        /// <returns>Source Update</returns>
        public override bool RealTimeSourceUpdate()
        {
            return false;
        }

        /// <summary>
        /// Get tau for all Secondary Sources and Interpolate in tauInterpolatedSecondarySources
        /// </summary>
        protected void InterpolateSecondarySources()
        {
            var tauSecondarySources = buildingAcoustic.GetSecondarySources();
            InterpolatedFrequencyBand = BaSettings.Instance.GetInterpolatedFrequencyBand(FilterResolution / 2);

            for (var i = 0; i < 5; i++)
                TauInterpolatedSecondarySources[i] =
                    CalcTauExtrapolatedFrom3RdOctaveTau(tauSecondarySources[i], FilterResolution);

            if (DataLogger.DebugData)
            {
                DataLogger.Log(LogName + "FrequencyBand.ThirdOctave", BaSettings.Instance.ThirdOctaveFrequencyBandShort);
                DataLogger.Log(LogName + "FrequencyBand.Extrapolated", BaSettings.Instance.ThirdOctaveFrequencyBandExtended);
                DataLogger.Log(LogName + "FrequencyBand.Interpolated", InterpolatedFrequencyBand);
                DataLogger.Log(LogName + "FrequencyBand.ThirdOctave", BaSettings.Instance.ThirdOctaveFrequencyBandShort);
                DataLogger.Log(LogName + "tauSecondarySources3rdOctave", tauSecondarySources);
                DataLogger.Log(LogName + "tauInterpolatedSecondarySources", TauInterpolatedSecondarySources);
            }
        }


        #region Filter Construction Helper

        /// <summary>
        /// Convolve tau for all Secondary Sources with Reverberation in Time Domain
        /// </summary>
        /// <param name="localTauFreqComplex">input Tau in Frequency Domain</param>
        /// <param name="reverberationTail">input Reverberation in Time Domain</param>
        /// <param name="factor">Factor array for each Secondary Sources</param>
        /// <returns></returns>
        protected float[][] GetTauReverberationTailTime(float[][] localTauFreqComplex, float[] reverberationTail,
            float[] factor = null)
        {
            var revTau = new float[5][];

            for (var i = 0; i < 5; i++)
            {
                var tau = FftHelper.Ifft(localTauFreqComplex[i]);
                revTau[i] = FftHelper.Convolution(tau, reverberationTail);
                if (factor != null)
                    for (var j = 0; j < revTau[i].Length; j++)
                        revTau[i][j] *= factor[i];
                if (LogRealtime) DataLogger.Log(LogName + "tauRev{1," + (HrirCounter + 1) + "}(:," + (i + 1) + ")", revTau);
            }


            return revTau;
        }

        /// <summary>
        /// Convolve ir and hrtf in Frequency Domain
        /// </summary>
        /// <param name="ir">5 Channels in Frequency Domain</param>
        /// <param name="hrtf">10 Channels of Hrtf in Frequency Domain</param>
        /// <returns>Binaural Impulse response in Frequency Domain</returns>
        protected float[][] GetBinauralIrFreqAndSum(float[][] ir, float[][] hrtf)
        {
            if (ir.Length != 5 || hrtf.Length != 10)
                Log.Error("Length of Hrtf or Impulse Response are invalid!");
            var binauralIr = new float[2][];
            binauralIr[0] = new float[ir[0].Length];
            binauralIr[1] = new float[ir[1].Length];
            for (var i = 0; i < 5; i++)
            {
                var pathLeft = ComplexFloats.Multiply(ir[i], hrtf[2 * i]);
                var pathRight = ComplexFloats.Multiply(ir[i], hrtf[2 * i + 1]);

                for (var j = 0; j < pathLeft.Length; j++)
                {
                    binauralIr[0][j] += pathLeft[j];
                    binauralIr[1][j] += pathRight[j];
                }
            }

            return binauralIr;
        }

        /// <summary>
        /// Convolve in Frequency Domain the Mono ir from with hrtfs for each Secondary Sources
        /// </summary>
        /// <param name="ir">input irs for Secondary Sources (5)</param>
        /// <param name="hrtf">input hrtf for Secondary Sources (10)</param>
        /// <returns></returns>
        protected float[][] GetBinauralIrFreq(float[][] ir, float[][] hrtf)
        {
            if (ir.Length != 5 || hrtf.Length != 10)
                Log.Error("Length of Hrtf or Impulse Response are invalid!");
            var binauralIr = new float[10][];
            for (var i = 0; i < 5; i++)
            {
                binauralIr[2 * i] = ComplexFloats.Multiply(ir[i], hrtf[2 * i]);
                binauralIr[2 * i + 1] = ComplexFloats.Multiply(ir[i], hrtf[2 * i + 1]);
            }

            return binauralIr;
        }

        /// <summary>
        /// Sum Up the input array for a Binaural ir
        /// </summary>
        /// <param name="input">binaural input for each Secondary Source length 10</param>
        /// <param name="factor">factor for each channel length 5</param>
        /// <returns>sum</returns>
        protected float[][] SumUpPaths(float[][] input, float[] factor = null)
        {
            if (input.Length != 10)
                Log.Error("input Vector has to have Length of 10!");

            var sum = new float[2][];
            if (!IsLengthEqual(input))
            {
                var maxLength = 0;
                for (var i = 0; i < 10; i++)
                    maxLength = Mathf.Max(maxLength, input[i].Length);
                sum[0] = new float[maxLength];
                sum[1] = new float[maxLength];
                if (factor != null)
                {
                    for (var j = 0; j < input[0].Length; j++)
                    {
                        sum[0][j] += input[0][j] * factor[0];
                        sum[1][j] += input[1][j] * factor[0];
                    }

                    for (var j = 0; j < input[2].Length; j++)
                    {
                        sum[0][j] += input[2][j] * factor[1];
                        sum[1][j] += input[3][j] * factor[1];
                    }

                    for (var j = 0; j < input[4].Length; j++)
                    {
                        sum[0][j] += input[4][j] * factor[2];
                        sum[1][j] += input[5][j] * factor[2];
                    }

                    for (var j = 0; j < input[6].Length; j++)
                    {
                        sum[0][j] += input[6][j] * factor[3];
                        sum[1][j] += input[7][j] * factor[3];
                    }

                    for (var j = 0; j < input[8].Length; j++)
                    {
                        sum[0][j] += input[8][j] * factor[4];
                        sum[1][j] += input[9][j] * factor[4];
                    }
                }
                else
                {
                    for (var j = 0; j < input[0].Length; j++)
                    {
                        sum[0][j] += input[0][j];
                        sum[1][j] += input[1][j];
                    }

                    for (var j = 0; j < input[2].Length; j++)
                    {
                        sum[0][j] += input[2][j];
                        sum[1][j] += input[3][j];
                    }

                    for (var j = 0; j < input[4].Length; j++)
                    {
                        sum[0][j] += input[4][j];
                        sum[1][j] += input[5][j];
                    }

                    for (var j = 0; j < input[6].Length; j++)
                    {
                        sum[0][j] += input[6][j];
                        sum[1][j] += input[7][j];
                    }

                    for (var j = 0; j < input[8].Length; j++)
                    {
                        sum[0][j] += input[8][j];
                        sum[1][j] += input[9][j];
                    }
                }
            }
            else
            {
                if (factor == null)
                {
                    sum[0] = input[0];
                    sum[1] = input[1];
                    for (var i = 0; i < sum[0].Length; i++)
                    {
                        sum[0][i] += input[2][i] + input[4][i] + input[6][i] + input[8][i];
                        sum[1][i] += input[3][i] + input[5][i] + input[7][i] + input[9][i];
                    }
                }
                else
                {
                    sum[0] = new float[input[0].Length];
                    sum[1] = new float[input[0].Length];
                    for (var i = 0; i < sum[0].Length; i++)
                    {
                        sum[0][i] = input[0][i] * factor[0] + input[2][i] * factor[1] + input[4][i] * factor[2] +
                                    input[6][i] * factor[3] + input[8][i] * factor[4];
                        sum[1][i] = input[1][i] * factor[0] + input[3][i] * factor[1] + input[5][i] * factor[2] +
                                    input[7][i] * factor[3] + input[9][i] * factor[4];
                    }
                }
            }

            return sum;
        }

        /// <summary>
        /// Sum Up the input array for a Monaural ir
        /// </summary>
        /// <param name="input">monaural input for each Secondary Source length 5</param>
        /// <returns>sum</returns>
        protected float[] SumUpPathsMono(float[][] input)
        {
            if (input.Length != 5)
                Log.Error("input Vector has to have Length of 5!");

            if (!IsLengthEqual(input))
            {
                var maxLength = 0;
                for (var i = 0; i < 10; i++)
                    maxLength = Mathf.Max(maxLength, input[i].Length);
                var sum = new float[maxLength];

                for (var j = 0; j < input[0].Length; j++) sum[j] += input[0][j];
                for (var j = 0; j < input[1].Length; j++) sum[j] += input[1][j];
                for (var j = 0; j < input[2].Length; j++) sum[j] += input[2][j];
                for (var j = 0; j < input[3].Length; j++) sum[j] += input[3][j];
                for (var j = 0; j < input[4].Length; j++) sum[j] += input[4][j];
                return sum;
            }
            else
            {
                var sum = new float[input[0].Length];
                for (var i = 0; i < sum.Length; i++)
                    sum[i] = input[0][i] + input[1][i] + input[2][i] + input[3][i] + input[4][i];
                return sum;
            }
        }

        /// <summary>
        /// Check if the filter Length are equal
        /// </summary>
        /// <param name="input"></param>
        /// <returns>bool if </returns>
        private bool IsLengthEqual(float[][] input)
        {
            var lastLength = input[0].Length;
            var inputLength = input.Length;
            for (var i = 1; i < inputLength; i++)
            {
                if (lastLength != input[i].Length)
                    return false;
                lastLength = input[i].Length;
            }

            return true;
        }

        /// <summary>
        /// Set input array to zero without changing Ref
        /// </summary>
        /// <param name="input"></param>
        private void SetZero(float[][] input)
        {
            for (var i = 0; i < input.Length; i++) Array.Clear(input[i], 0, input[i].Length);
        }

        #endregion
    }
}