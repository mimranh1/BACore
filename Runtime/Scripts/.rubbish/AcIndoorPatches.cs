﻿using System;
using System.Diagnostics;
using log4net;
using UnityEngine;

namespace BA.BACore
{
    /// <summary>
    /// Implementation of new Approach Indoor considering Direct and reverberation in Source and Receiver room
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    [Serializable]
    public class AcIndoorPatches : AcNewApproachFlanking
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(AcIndoorPatches));

        private float[][] _dirDirFreqPartitionPatches;
        private float[][] _dirRevFreqPartitionPatches;
        private float[][] _revDirFreqPartitionPatches;

        private float[][] _dirDirFreqPartition = new float[5][];
        private float[][] _dirRevFreqPartition = new float[5][];
        private float[][] _revDirFreqPartition = new float[5][];
        private float[][] _dirDirFreqFlanking = new float[8][];
        private float[][] _dirRevFreqFlanking = new float[8][];
        public float[][] RevDirFreqFlanking = new float[8][];
        private float[] _revRevNewApproachFreqPatches;
        private float[] _revRevNewApproachFreq;
        private float[][] _realTimeSource = new float[5][];
        private float[][] _realTimeReceiver = new float[5][];
        private float[][][] _rtSourcePatches;
        private float[][][] _rtReceiverPatches;

        private bool _usePatches;


        public override AuralisationMethod AuralisationMethod => AuralisationMethod.IndoorModelPatches;

        /// <summary>
        /// Implementation of Initialisation with Init BuildingAcoustics Array from FilterManager
        /// </summary>
        /// <param name="bas">buildingAcoustics Array from FilterManager</param>
        public override void Init()
        {
            moduleName = "AcIndoorPatches";
            init = true;
            LogName = "FilterManager.AcIndoorPatches";

            for (var i = 0; i < 5; i++)
            {
                _realTimeSource[i] = new float[2 * FilterResolution];
                _realTimeReceiver[i] = new float[2 * FilterResolution];
            }

            _rtReceiverPatches = new float[buildingAcoustics.Length][][];
            _rtSourcePatches = new float[buildingAcoustics.Length][][];
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            {
                _rtSourcePatches[iBa] = new float[buildingAcoustics[iBa].NumOfPatches][];
                _dirDirFreqPartitionPatches = new float[buildingAcoustics[iBa].NumOfPatches][];
                _dirRevFreqPartitionPatches = new float[buildingAcoustics[iBa].NumOfPatches][];
                _revDirFreqPartitionPatches = new float[buildingAcoustics[iBa].NumOfPatches][];
            }

            SoundSourceObjects[0].Init(FilterResolution);
            SoundReceiverObject.Init(FilterResolution);
        }


        /// <summary>
        /// Implementation of Calculate pre-calculation and and set AudioRenderer
        /// </summary>
        /// <param name="ar">AudioRenderer from FilterManager</param>
        protected override void OfflineCalculation(AudioRender ar)
        {
            audioRenderer = ar;
            buildingAcoustics[0].ProcessAll();
            var areaSi = new float[5];
            areaSi[0] = buildingAcoustics[0].partition.Geometry.Area;
            for (var i = 0; i < 4; i++)
                areaSi[i + 1] = buildingAcoustics[0].sourceWalls[i].Geometry.Area;
            var areaSd = areaSi[0];
            var rhoC0 = BaSettings.Instance.C0 * BaSettings.Instance.Rho0;
            var equivalentAbsAreaSource = buildingAcoustics[0].sourceRoom.Properties.EquivalentAbsorptionArea;
            var equivalentAbsAreaReceiver = buildingAcoustics[0].receiverRoom.Properties.EquivalentAbsorptionArea;
            _revRevNewApproachFreq = new float[2 * FilterResolution];
            _revRevNewApproachFreqPatches = new float[2 * FilterResolution];
            var sId = buildingAcoustics[0].SourceWallIndexes;
            var rId = buildingAcoustics[0].ReceiverWallIndexes;
            var pId = buildingAcoustics[0].PartitionWallIndexes;
            var revSourceFreqComplex = FftHelper.Fft(buildingAcoustics[0].sourceRoom.reverberation, true, FilterResolution);
            var revReceiverFreqComplex =
                FftHelper.Fft(buildingAcoustics[0].receiverRoom.reverberation, true, FilterResolution);
            var revAll = ComplexFloats.Multiply(revSourceFreqComplex, revReceiverFreqComplex);

            _usePatches = buildingAcoustics[0].partition.patchesHandler.NumOfPatches != 0;

            var tauPatches = buildingAcoustics[0].partition.patchesHandler.ReductionIndexPatches;
            for (var iPatch = 0; iPatch < buildingAcoustics[0].NumOfPatches; iPatch++)
            {
                var tauInterpolatedFreq =
                    CalcTauExtrapolatedFrom3RdOctaveTau(10 ^ (-0.1f * tauPatches[iPatch]), FilterResolution);
                var tau1PhaseFreqComplex = CalcSqrtTauComplexFromTauInterpolatedEven(tauInterpolatedFreq.Tf, LogName);
                var preFactor = sourceSoundPowerNewApproach * buildingAcoustics[0].partition.Patches[iPatch].Geometry.Area /
                                (buildingAcoustics[0].partition.Geometry.Area * 4 * Mathf.PI);

                var factorDirDir = preFactor / (4 * Mathf.PI);
                var factorDirRev = preFactor * Mathf.Sqrt(1 / (Mathf.PI * equivalentAbsAreaReceiver));
                var factorRevDir = preFactor * Mathf.Sqrt(areaSi[0] / (4 * Mathf.PI * equivalentAbsAreaSource));
                var factorRevRev =
                    preFactor * Mathf.Sqrt(4 * areaSi[0] / (equivalentAbsAreaSource * equivalentAbsAreaReceiver));

                // Calculate Direct-Direct Pre Filter
                _dirDirFreqPartitionPatches[iPatch] = ComplexFloats.Multiply(tau1PhaseFreqComplex, factorDirDir);

                // Calculate Direct-Rev Pre Filter
                _dirRevFreqPartitionPatches[iPatch] =
                    ComplexFloats.Multiply(revReceiverFreqComplex, tau1PhaseFreqComplex, factorDirRev);

                // Calculate Rev-Direct Pre Filter
                _revDirFreqPartitionPatches[iPatch] =
                    ComplexFloats.Multiply(revSourceFreqComplex, tau1PhaseFreqComplex, factorRevDir);

                // Calculate Rev-Rev Pre Filter
                var revRevFreqTemp = ComplexFloats.Multiply(revAll, tau1PhaseFreqComplex, factorRevRev);
                if (revRevFreqTemp.Length != _revRevNewApproachFreq.Length)
                    Log.Error("Length of irs are not matching!");
                for (var n = 0; n < revRevFreqTemp.Length; n++) _revRevNewApproachFreqPatches[n] += revRevFreqTemp[n];
            }

            // Calc Flanking
            for (var i = 0; i < 13; i++)
            {
                var pathId = (i - 1) / 4;
                var iJun = (i - 1) % 4;
                var sIdGlobal = 0;
                var metricId = 0;
                TransferFunction tau3RdOctaveFreq;
                if (i == 0)
                {
                    tau3RdOctaveFreq = 10 ^ (buildingAcoustics[0].partition.ReductionIndex.ReductionIndex / -10f);
                    sIdGlobal = 0;
                    iJun = 0;
                    metricId = 0;
                }
                else
                {
                    var reductionIndexFlanking = buildingAcoustics[0].GetReductionIndexFlanking();
                    switch (pathId)
                    {
                        case 0:
                            tau3RdOctaveFreq =
                                10 ^ (reductionIndexFlanking[iJun][sId[iJun], rId[iJun]] / -10f);
                            sIdGlobal = iJun + 1;
                            metricId = 2 * iJun + 0;
                            break;
                        case 1:
                            tau3RdOctaveFreq =
                                10 ^ (reductionIndexFlanking[iJun][sId[iJun], pId[iJun]] / -10f);
                            sIdGlobal = iJun + 1;
                            metricId = iJun + 1;
                            break;
                        case 2:
                            tau3RdOctaveFreq =
                                10 ^ (reductionIndexFlanking[iJun][pId[iJun], rId[iJun]] / -10f);
                            sIdGlobal = 0;

                            metricId = 2 * iJun + 1;
                            break;
                        default:
                            tau3RdOctaveFreq =
                                10 ^ (reductionIndexFlanking[iJun][sId[iJun], rId[iJun]] / -10f);
                            Log.Error("Cannot find Reduction Index!");
                            break;
                    }
                }

                var tauInterpolatedFreq = CalcTauExtrapolatedFrom3RdOctaveTau(tau3RdOctaveFreq, FilterResolution);
                var tau1PhaseFreqComplex = CalcSqrtTauComplexFromTauInterpolatedEven(tauInterpolatedFreq.Tf, LogName, 1, 1);
                //tau0PhaseFreqComplex = tau1PhaseFreqComplex;
                var preFactor = Mathf.Sqrt(400 * 400 * sourceSoundPowerNewApproach * areaSd / (areaSi[sIdGlobal] * rhoC0));

                var factorDirDir = preFactor / (4 * Mathf.PI);
                var factorDirRev = preFactor * Mathf.Sqrt(1 / (Mathf.PI * equivalentAbsAreaReceiver));
                var factorRevDir = preFactor * Mathf.Sqrt(areaSi[sIdGlobal] / (4 * Mathf.PI * equivalentAbsAreaSource));
                var factorRevRev =
                    preFactor * Mathf.Sqrt(4 * areaSi[sIdGlobal] / (equivalentAbsAreaSource * equivalentAbsAreaReceiver));
                if (i == 0)
                {
                    // Calculate Direct-Direct Pre Filter
                    _dirDirFreqPartition[metricId] = new float[2 * FilterResolution];

                    // Calculate Direct-Rev Pre Filter
                    _dirRevFreqPartition[metricId] = new float[2 * FilterResolution];

                    // Calculate Rev-Direct Pre Filter
                    _revDirFreqPartition[metricId] = new float[2 * FilterResolution];
                }
                else if (pathId == 1)
                {
                    // Calculate Direct-Direct Pre Filter
                    _dirDirFreqPartition[metricId] = ComplexFloats.Multiply(tau1PhaseFreqComplex, factorDirDir);

                    // Calculate Direct-Rev Pre Filter
                    _dirRevFreqPartition[metricId] =
                        ComplexFloats.Multiply(revReceiverFreqComplex, tau1PhaseFreqComplex, factorDirRev);

                    // Calculate Rev-Direct Pre Filter
                    _revDirFreqPartition[metricId] =
                        ComplexFloats.Multiply(revSourceFreqComplex, tau1PhaseFreqComplex, factorRevDir);
                }
                else
                {
                    // Calculate Direct-Direct Pre Filter
                    _dirDirFreqFlanking[metricId] = ComplexFloats.Multiply(tau1PhaseFreqComplex, factorDirDir);

                    // Calculate Direct-Rev Pre Filter
                    _dirRevFreqFlanking[metricId] =
                        ComplexFloats.Multiply(revReceiverFreqComplex, tau1PhaseFreqComplex, factorDirRev);

                    // Calculate Rev-Direct Pre Filter
                    RevDirFreqFlanking[metricId] =
                        ComplexFloats.Multiply(revSourceFreqComplex, tau1PhaseFreqComplex, factorRevDir);
                }

                // Calculate Rev-Rev Pre Filter
                var revRevFreqTemp = ComplexFloats.Multiply(revAll, tau1PhaseFreqComplex, factorRevRev);
                if (revRevFreqTemp.Length != _revRevNewApproachFreq.Length)
                    Log.Error("Length of irs are not matching!");
                for (var n = 0; n < revRevFreqTemp.Length; n++)
                {
                    _revRevNewApproachFreq[n] += revRevFreqTemp[n];
                    if (i != 0)
                        _revRevNewApproachFreqPatches[n] += revRevFreqTemp[n];
                }
            }


            SoundSourceObjects[0]
                .GetSourceFiForSs(buildingAcoustics[0].sourceWalls, _realTimeSource, FilterResolution);
            _realTimeReceiver = SoundReceiverObject
                .GetHrtfForSs(FilterResolution, buildingAcoustics[0].ReceiverPositionsSs, true);
            RealTimeSourceUpdate();
            RealTimeReceiverUpdate();
            CalcSourceUpdate();
            CalcReceiverUpdate();
        }

        private void LogAllData()
        {
            DataLogger.DebugData = true;
            DataLogger.Log(LogName + ".rtSourcePatchesComplex", _rtSourcePatches);
            DataLogger.Log(LogName + ".rtReceiverPatchesComplex", _rtReceiverPatches);
            DataLogger.Log(LogName + ".realTimeSourceComplex", _realTimeSource);
            DataLogger.Log(LogName + ".realTimeReceiverComplex", _realTimeReceiver);
            DataLogger.Log(LogName + ".revRevNewApproachFreqComplex", _revRevNewApproachFreq);
            DataLogger.Log(LogName + ".revDirFreqFlankingComplex", RevDirFreqFlanking);
            DataLogger.Log(LogName + ".dirRevFreqFlankingComplex", _dirRevFreqFlanking);
            DataLogger.Log(LogName + ".dirDirFreqFlankingComplex", _dirDirFreqFlanking);
            DataLogger.Log(LogName + ".revDirFreqPartitionComplex", _revDirFreqPartition);
            DataLogger.Log(LogName + ".dirRevFreqPartitionComplex", _dirRevFreqPartition);
            DataLogger.Log(LogName + ".dirDirFreqPartitionComplex", _dirDirFreqPartition);
            DataLogger.Log(LogName + ".revRevNewApproachFreqPatchesComplex", _revRevNewApproachFreqPatches);
            DataLogger.Log(LogName + ".revDirFreqPartitionPatchesComplex", _revDirFreqPartitionPatches);
            DataLogger.Log(LogName + ".dirRevFreqPartitionPatchesComplex", _dirRevFreqPartitionPatches);
            DataLogger.Log(LogName + ".dirDirFreqPartitionPatchesComplex", _dirDirFreqPartitionPatches);
            DataLogger.DebugData = false;
        }

        /// <summary>
        /// Implementation of Realtime Update, called if Source or Receiver Updates
        /// </summary>
        /// <param name="logId">write log file if logId > 1</param>
        protected override void RealTimeCalculation(int logId = 0)
        {
            var sw = new Stopwatch();
            sw.Start();
            var sumAllIrLeftFreqComplex = ComplexFloats.CopyTo(_revRevNewApproachFreq);
            var sumAllIrRightFreqComplex = ComplexFloats.CopyTo(_revRevNewApproachFreq);

            var ifDataLog = DataLogger.DebugData;

            var ssBinComplex = new float[sumAllIrLeftFreqComplex.Length];
            var ssMonoComplex = new float[sumAllIrLeftFreqComplex.Length];
            // Start Partition Wall 
            var startPartitionId = 0;
            if (_usePatches)
            {
                for (var n = 0; n < 2 * FilterResolution; n += 2)
                {
                    float ssBinReal = 0f, ssBinImag = 0f, ssMonoReal = 0f, ssMonoImag = 0f;

                    for (var i = 0; i < _dirDirFreqPartitionPatches.Length; i++)
                    {
                        ssBinReal += _dirDirFreqPartitionPatches[i][n] * _rtSourcePatches[0][i][n] -
                                     _dirDirFreqPartitionPatches[i][n + 1] * _rtSourcePatches[0][i][n + 1] +
                                     _revDirFreqPartitionPatches[i][n];

                        ssBinImag += _dirDirFreqPartitionPatches[i][n + 1] * _rtSourcePatches[0][i][n] +
                                     _dirDirFreqPartitionPatches[i][n] * _rtSourcePatches[0][i][n + 1] +
                                     _revDirFreqPartitionPatches[i][n + 1];

                        ssMonoReal += _dirRevFreqPartitionPatches[i][n] * _rtSourcePatches[0][i][n] -
                                      _dirRevFreqPartitionPatches[i][n + 1] * _rtSourcePatches[0][i][n + 1];

                        ssMonoImag += _dirRevFreqPartitionPatches[i][n + 1] * _rtSourcePatches[0][i][n] +
                                      _dirRevFreqPartitionPatches[i][n] * _rtSourcePatches[0][i][n + 1];
                    }

                    if (ifDataLog)
                    {
                        ssBinComplex[n] = ssBinReal;
                        ssBinComplex[n + 1] = ssBinImag;
                        ssMonoComplex[n] = ssMonoReal;
                        ssMonoComplex[n + 1] = ssMonoImag;
                    }

                    sumAllIrLeftFreqComplex[n] += ssBinReal * _rtReceiverPatches[0][0][n] -
                        ssBinImag * _rtReceiverPatches[0][0][n + 1] + ssMonoReal;
                    sumAllIrLeftFreqComplex[n + 1] += ssBinReal * _rtReceiverPatches[0][0][n + 1] +
                                                      ssBinImag * _rtReceiverPatches[0][0][n] + ssMonoImag;

                    sumAllIrRightFreqComplex[n] += ssBinReal * _rtReceiverPatches[0][1][n] -
                        ssBinImag * _rtReceiverPatches[0][1][n + 1] + ssMonoReal;
                    sumAllIrRightFreqComplex[n + 1] += ssBinReal * _rtReceiverPatches[0][1][n + 1] +
                                                       ssBinImag * _rtReceiverPatches[0][1][n] + ssMonoImag;
                }

                startPartitionId = 1;
            }


            // Start Partition Wall
            for (var n = 0; n < 2 * FilterResolution; n += 2)
            {
                float ssBinReal = 0f, ssBinImag = 0f, ssMonoReal = 0f, ssMonoImag = 0f;

                for (var i = startPartitionId; i < 5; i++)
                {
                    ssBinReal += _dirDirFreqPartition[i][n] * _realTimeSource[i][n] -
                                 _dirDirFreqPartition[i][n + 1] * _realTimeSource[i][n + 1] +
                                 _revDirFreqPartition[i][n];

                    ssBinImag += _dirDirFreqPartition[i][n + 1] * _realTimeSource[i][n] +
                                 _dirDirFreqPartition[i][n] * _realTimeSource[i][n + 1] +
                                 _revDirFreqPartition[i][n + 1];

                    ssMonoReal += _dirRevFreqPartition[i][n] * _realTimeSource[i][n] -
                                  _dirRevFreqPartition[i][n + 1] * _realTimeSource[i][n + 1];

                    ssMonoImag += _dirRevFreqPartition[i][n + 1] * _realTimeSource[i][n] +
                                  _dirRevFreqPartition[i][n] * _realTimeSource[i][n + 1];
                }

                if (ifDataLog)
                {
                    ssBinComplex[n] = ssBinReal;
                    ssBinComplex[n + 1] = ssBinImag;
                    ssMonoComplex[n] = ssMonoReal;
                    ssMonoComplex[n + 1] = ssMonoImag;
                }

                sumAllIrLeftFreqComplex[n] += ssBinReal * _realTimeReceiver[0][n] -
                    ssBinImag * _realTimeReceiver[0][n + 1] + ssMonoReal;
                sumAllIrLeftFreqComplex[n + 1] += ssBinReal * _realTimeReceiver[0][n + 1] +
                                                  ssBinImag * _realTimeReceiver[0][n] + ssMonoImag;

                sumAllIrRightFreqComplex[n] += ssBinReal * _realTimeReceiver[1][n] -
                    ssBinImag * _realTimeReceiver[1][n + 1] + ssMonoReal;
                sumAllIrRightFreqComplex[n + 1] += ssBinReal * _realTimeReceiver[1][n + 1] +
                                                   ssBinImag * _realTimeReceiver[1][n] + ssMonoImag;
            }


            var rtSourcePartition = _realTimeSource[0];
            // Start Flanking Wall 
            for (var i = 0; i < 4; i++)
            {
                var dirDirFf = _dirDirFreqFlanking[2 * i];
                var dirDirDf = _dirDirFreqFlanking[2 * i + 1];
                var rtSourceFlanking = _realTimeSource[i + 1];
                var revDirFf = RevDirFreqFlanking[2 * i];
                var revDirFd = RevDirFreqFlanking[2 * i + 1];
                var dirRevFf = _dirRevFreqFlanking[2 * i];
                var dirRevFd = _dirRevFreqFlanking[2 * i + 1];
                var rtReceiverLeft = _realTimeReceiver[2 * i];
                var rtReceiverRight = _realTimeReceiver[2 * i + 1];

                for (var n = 0; n < sumAllIrLeftFreqComplex.Length; n += 2)
                {
                    var ssBinReal = dirDirFf[n] * rtSourceFlanking[n] - dirDirFf[n + 1] * rtSourceFlanking[n + 1] +
                                    dirDirDf[n] * rtSourcePartition[n] - dirDirDf[n + 1] * rtSourcePartition[n + 1] +
                                    revDirFf[n] + revDirFd[n];
                    var ssBinImag = dirDirFf[n + 1] * rtSourceFlanking[n] + dirDirFf[n] * rtSourceFlanking[n + 1] +
                                    dirDirDf[n + 1] * rtSourcePartition[n] + dirDirDf[n] * rtSourcePartition[n + 1] +
                                    revDirFf[n + 1] + revDirFd[n + 1];

                    var ssMonoReal = dirRevFf[n] * rtSourceFlanking[n] - dirRevFf[n + 1] * rtSourceFlanking[n + 1] +
                        dirRevFd[n] * rtSourcePartition[n] - dirRevFd[n + 1] * rtSourcePartition[n + 1];
                    var ssMonoImag = dirRevFf[n + 1] * rtSourceFlanking[n] + dirRevFf[n] * rtSourceFlanking[n + 1] +
                                     dirRevFd[n + 1] * rtSourcePartition[n] + dirRevFd[n] * rtSourcePartition[n + 1];

                    if (ifDataLog)
                    {
                        ssBinComplex[n] = ssBinReal;
                        ssBinComplex[n + 1] = ssBinImag;
                        ssMonoComplex[n] = ssMonoReal;
                        ssMonoComplex[n + 1] = ssMonoImag;
                    }

                    sumAllIrLeftFreqComplex[n] +=
                        ssBinReal * rtReceiverLeft[n] - ssBinImag * rtReceiverLeft[n + 1] + ssMonoReal;
                    sumAllIrLeftFreqComplex[n + 1] +=
                        ssBinReal * rtReceiverLeft[n + 1] + ssBinImag * rtReceiverLeft[n] + ssMonoImag;

                    sumAllIrRightFreqComplex[n] +=
                        ssBinReal * rtReceiverRight[n] - ssBinImag * rtReceiverRight[n + 1] + ssMonoReal;
                    sumAllIrRightFreqComplex[n + 1] +=
                        ssBinReal * rtReceiverRight[n + 1] + ssBinImag * rtReceiverRight[n] + ssMonoImag;
                }
            }

            var irTimeLeft = FftHelper.Ifft(sumAllIrLeftFreqComplex);
            var irTimeRight = FftHelper.Ifft(sumAllIrRightFreqComplex);
            IfftShift(irTimeLeft);
            IfftShift(irTimeRight);

            if (audioRenderer != null)
            {
                audioRenderer.UpdateFilter(irTimeLeft, irTimeRight);
            }
            else
            {
                DataLogger.DebugData = true;
                DataLogger.Log(LogName + ".irTimeLeft(:," + (HrirCounter + 1) + ")", irTimeLeft);
                DataLogger.Log(LogName + ".irTimeRight(:," + (HrirCounter + 1) + ")", irTimeRight);
                DataLogger.DebugData = false;
            }

            sw.Stop();
            FilterUpdate = sw.Elapsed.TotalMilliseconds;
            if (LogRealtime)
            {
                LogAllData();
                DataLogger.DebugData = true;
                DataLogger.Log(LogName + ".irTimeLeft", irTimeLeft);
                DataLogger.Log(LogName + ".irTimeRight", irTimeRight);
                DataLogger.Log(LogName + ".sumAllIrLeftFreqComplex", sumAllIrLeftFreqComplex);
                DataLogger.Log(LogName + ".sumAllIrRightFreqComplex", sumAllIrRightFreqComplex);
                DataLogger.DebugData = false;
                LogRealtime = false;
            }
        }

        private void IfftShift(float[] ir)
        {
            var temp = new float[ir.Length / 2];
            Array.Copy(ir, 0, temp, 0, ir.Length / 2);
            Array.Copy(ir, ir.Length / 2, ir, 0, ir.Length / 2);
            Array.Copy(temp, 0, ir, ir.Length / 2, ir.Length / 2);
        }

        private bool CalcSourceUpdate()
        {
            var posUpdate = false;
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            {
                //Debug.Log("Receiver Update " + iBa);
                //rtSource[iBa] = buildingAcoustics[iBa].sourceRoom
                //    .GetSourceFiForPatches(filterLength, out riSamplesDelay[iBa], true, logId);

                _rtSourcePatches[iBa] = SoundSourceObjects[0]
                    .GetSourceFiForPatches(buildingAcoustics[iBa].partition, FilterResolution, out _, LogRealtime);


                posUpdate = true;
            }

            ForceSourceUpdate = false;
            return posUpdate;
        }


        private bool CalcReceiverUpdate()
        {
            var posUpdate = false;
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            {
                //Debug.Log("Receiver Update " + iBa);
                _rtReceiverPatches[iBa] = SoundReceiverObject
                    .GetHrtfForSsPatches(FilterResolution, buildingAcoustics[iBa].partition.SecondarySourcesPosPatches,
                        out _, out _, out _);
                posUpdate = true;
            }

            ForceReceiverUpdate = false;
            return posUpdate;
        }


        /// <summary>
        /// Implementation of Update Receiver Data for filter Construction if Receiver Update
        /// </summary>
        /// <returns>Receiver Update</returns>
        protected override bool RealTimeReceiverUpdate()
        {
            var sw = new Stopwatch();
            sw.Start();
            var posUpdate = false;
            if (SoundReceiverObject.SoundObjectChanged() | ForceReceiverUpdate)
            {
                for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
                {
                    _realTimeReceiver = SoundReceiverObject
                        .GetHrtfForSs(FilterResolution, buildingAcoustics[iBa].ReceiverPositionsSs);
                    posUpdate = true;
                    ForceReceiverUpdate = false;
                }

                CalcReceiverUpdate();
                //Debug.Log("Update Receiver Filter!");
            }

            sw.Stop();
            if (sw.Elapsed.TotalMilliseconds > 0)
                ReceiverUpdate = sw.Elapsed.TotalMilliseconds;
            return posUpdate;
        }

        /// <summary>
        /// Implementation of Update Source Data for filter Construction if Source Update
        /// </summary>
        /// <returns>Source Update</returns>
        protected override bool RealTimeSourceUpdate()
        {
            var sw = new Stopwatch();
            sw.Start();
            var posUpdate = false;
            if (SoundSourceObjects[0].SoundObjectChanged() | ForceSourceUpdate)
            {
                for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
                    SoundSourceObjects[0]
                        .GetSourceFiForSs(buildingAcoustics[iBa].sourceWalls, _realTimeSource, FilterResolution);

                CalcSourceUpdate();
                //Debug.Log("Update Source Filter!");
            }

            ForceSourceUpdate = false;
            sw.Stop();
            if (sw.Elapsed.TotalMilliseconds > 0)
                SourceUpdate = sw.Elapsed.TotalMilliseconds;
            return posUpdate;
        }

        #region Tests

        /// <summary>
        /// Run New Approach Indoor and Log results separably for each part of ir (dd, dr, rd, rr)
        /// </summary>
        public void TestCalcNewApproachWithLogging()
        {
            OfflineCalculation(null);
            LogRealtime = false;
            HrirCounter = 0;

            var dirDirPartPatches = ComplexFloats.CopyTo(_dirDirFreqPartitionPatches);
            var dirRevPartPatches = ComplexFloats.CopyTo(_dirRevFreqPartitionPatches);
            var revDirPartPatches = ComplexFloats.CopyTo(_revDirFreqPartitionPatches);
            var revRevPartPatches = ComplexFloats.CopyTo(_revRevNewApproachFreqPatches);

            var dirDirFlanking = ComplexFloats.CopyTo(_dirDirFreqFlanking);
            var dirRevFlanking = ComplexFloats.CopyTo(_dirRevFreqFlanking);
            var revDirFlanking = ComplexFloats.CopyTo(RevDirFreqFlanking);
            var dirDirPartition = ComplexFloats.CopyTo(_dirDirFreqPartition);
            var dirRevPartition = ComplexFloats.CopyTo(_dirRevFreqPartition);
            var revDirPartition = ComplexFloats.CopyTo(_revDirFreqPartition);
            var revRev = ComplexFloats.CopyTo(_revRevNewApproachFreq);

            // All Paths
            RealTimeCalculation();
            HrirCounter++;

            // Just DirDir
            SetAllNewApproachOfflineArraysZero();
            _dirDirFreqPartitionPatches = ComplexFloats.CopyTo(dirDirPartPatches);
            _dirDirFreqFlanking = ComplexFloats.CopyTo(dirDirFlanking);
            _dirDirFreqPartition = ComplexFloats.CopyTo(dirDirPartition);
            RealTimeCalculation();
            HrirCounter++;

            // Just DirRev
            SetAllNewApproachOfflineArraysZero();
            _dirRevFreqFlanking = ComplexFloats.CopyTo(dirRevFlanking);
            _dirRevFreqPartitionPatches = ComplexFloats.CopyTo(dirRevPartPatches);
            _dirRevFreqPartition = ComplexFloats.CopyTo(dirRevPartition);
            RealTimeCalculation();
            HrirCounter++;

            // Just RevDir
            SetAllNewApproachOfflineArraysZero();
            RevDirFreqFlanking = ComplexFloats.CopyTo(revDirFlanking);
            _revDirFreqPartitionPatches = ComplexFloats.CopyTo(revDirPartPatches);
            _revDirFreqPartition = ComplexFloats.CopyTo(revDirPartition);
            RealTimeCalculation();
            HrirCounter++;

            // Just RevRev
            SetAllNewApproachOfflineArraysZero();
            _revRevNewApproachFreqPatches = ComplexFloats.CopyTo(revRevPartPatches);
            _revRevNewApproachFreq = ComplexFloats.CopyTo(revRev);
            RealTimeCalculation();
            HrirCounter++;

            var timePrefix = DateTime.Now.ToString("-yyyyMMdd-HHmmss-");
            timePrefix = "TestCalcNewApproachWithLogging" + timePrefix;

            MoveLogFiles(timePrefix);
        }


        private void SetAllNewApproachOfflineArraysZero()
        {
            SetToZero(_dirDirFreqPartitionPatches);
            SetToZero(_dirRevFreqPartitionPatches);
            SetToZero(_revDirFreqPartitionPatches);
            SetToZero(_revRevNewApproachFreqPatches);
            SetToZero(_dirDirFreqFlanking);
            SetToZero(_dirRevFreqFlanking);
            SetToZero(RevDirFreqFlanking);
            SetToZero(_dirDirFreqPartition);
            SetToZero(_dirRevFreqPartition);
            SetToZero(_revDirFreqPartition);
            SetToZero(_revRevNewApproachFreq);
        }


        private void SetToZero(float[][] ir)
        {
            for (var i = 0; i < ir.Length; i++)
                SetToZero(ir[i]);
        }

        private void SetToZero(float[] ir)
        {
            for (var i = 0; i < ir.Length; i++)
                ir[i] = 0;
        }

        #endregion
    }
}