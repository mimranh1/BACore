﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using FFTWSharp;
using log4net;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace BA.BACore
{
    /// <summary>
    /// Implementation of new Approach Outdoor considering Direct and reverberation in Source and Receiver room
    /// First try to implement everything in realtime, but this one is very slow
    /// not working well
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    [Serializable]
    public class AcOutdoorDavyOutdoorPreCalcSinglePatch : AcNewApproachPatches
    {
        [SerializeField] private bool applyReceiverDirectSound = true;
        [SerializeField] private bool applyReceiverReverbSound = true;
        [SerializeField] private bool applyIrFactor = true;

        public override AuralisationMethod AuralisationMethod => AuralisationMethod.OutdoorDavyPreCalcSinglePatch;
        public override bool IsSingleSoundSource => false;

        public int NumOfPatches => buildingAcoustics.Sum(ff => ff.partition.NumOfPatches);

        private float[] _revReceiverTime;
        private float[][][] _rtReceiver;
        private float[][][] _rtSource;
        private int[][] _riSamplesDelay;
        private int[][] _rjSamplesDelay;


        private float[] _irLeftFreq = new float[44100 * 2];
        private float[] _irRightFreq = new float[44100 * 2];
        private float[] _irLeftTime = new float[44100];
        private float[] _irRightTime = new float[44100];

        private GCHandle _ifftInputLeftHandle;
        private GCHandle _ifftResultLeftHandle;
        private IntPtr _ifftPlanLeft;


        // IFFT Right
        private GCHandle _ifftInputRightHandle;
        private GCHandle _ifftResultRightHandle;
        private IntPtr _ifftPlanRight;

        private string _filePath;


        private float[][][] _irReceiverRoomFreq;
        private bool _applyReceiverDirectSoundShadow = true;
        private bool _applyReceiverReverbSoundShadow = true;
        private bool _applyIrFactorShadow = true;

        private static readonly ILog Log = LogManager.GetLogger(typeof(AcOutdoorDavyOutdoorPreCalcSinglePatch));

        /// <summary>
        /// Overload of Initialisation with Init BuildingAcoustics Array from FilterManager
        /// </summary>
        public override void Init()
        {
            moduleName = "AcOutdoorDavyOutdoorPreCalc";
            CultureInfo.CreateSpecificCulture("en-GB");
            init = true;
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            {
                buildingAcoustics[iBa].ProcessAll();
            }

            var postfix = "";
            _filePath =
                $"{BaSettings.Instance.RootPath}/Resources/Sounds/OutdoorStaticBakedRealTime/Irs{postfix}";
            _irLeftFreq = new float[FilterResolution * 2];
            _irRightFreq = new float[FilterResolution * 2];
            _irLeftTime = new float[FilterResolution];
            _irRightTime = new float[FilterResolution];
            InitIfftIr();
        }

        /// <summary>
        /// Implementation of Calculate pre-calculation and and set AudioRenderer
        /// </summary>
        /// <param name="ar">AudioRenderer from FilterManager</param>
        protected override void OfflineCalculation(AudioRender ar)
        {
            ForceReceiverUpdate = true;
            ForceSourceUpdate = true;
            _rtReceiver = new float[buildingAcoustics.Length][][];
            _rtSource = new float[buildingAcoustics.Length][][];
            _riSamplesDelay = new int[buildingAcoustics.Length][];

            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            {
                var numPatches = buildingAcoustics[iBa].partition.NumOfPatches;
                _rtReceiver[iBa] = new float[2 * numPatches][];
                _rtSource[iBa] = new float[numPatches][];
                _riSamplesDelay[iBa] = new int[numPatches];
                if (numPatches < 1)
                    Debug.LogWarning(buildingAcoustics[iBa].partition + ": Wall does not contain Patches.");
                for (var iPatch = 0; iPatch < numPatches; iPatch++)
                {
                    _rtReceiver[iBa][2 * iPatch] = new float[FilterResolution * 2];
                    _rtReceiver[iBa][2 * iPatch + 1] = new float[FilterResolution * 2];
                    _rtSource[iBa][iPatch] = new float[FilterResolution * 2];
                }

                buildingAcoustics[iBa].ProcessAll();
            }

            foreach (var soundSourceObject in SoundSourceObjects)
            {
                soundSourceObject.Init(FilterResolution);
            }
            SoundReceiverObject.Init(FilterResolution);

            var constFactorRev = Mathf.Sqrt(4 / buildingAcoustics[0].receiverRoom.Properties.EquivalentAbsorptionArea);
            _revReceiverTime = buildingAcoustics[0].receiverRoom.reverberation;
            for (var i = 0; i < _revReceiverTime.Length; i++)
                _revReceiverTime[i] *= constFactorRev;
            _rjSamplesDelay = new int[buildingAcoustics.Length][];
            audioRenderer = ar;
            UpdateMonoIrs();
        }


        /// <summary>
        /// Implementation of Realtime Update, called if Source or Receiver Updates
        /// </summary>
        /// <param name="logId">write log file if logId > 1</param>
        protected override void RealTimeCalculation(int logId = 0)
        {
            var sw = new Stopwatch();
            sw.Start();
            UpdateFilter(audioRenderer);
            sw.Stop();
            FilterUpdate = sw.Elapsed.TotalMilliseconds;
        }

        /// <summary>
        /// Implementation of Update Receiver Data for filter Construction if Receiver Update
        /// </summary>
        /// <returns>Receiver Update</returns>
        protected override bool RealTimeReceiverUpdate()
        {
            if (!init)
                return false;
            var posUpdate = false;

            if (_applyReceiverDirectSoundShadow != applyReceiverDirectSound ||
                _applyIrFactorShadow != applyIrFactor ||
                _applyReceiverReverbSoundShadow != applyReceiverReverbSound)
                ForceReceiverUpdate = true;
            
            if (SoundReceiverObject.SoundObjectChanged() | ForceReceiverUpdate)
            {
                if (SoundReceiverObject.SoundObjectMove() | ForceReceiverUpdate)
                {
                    UpdateMonoIrs();
                }
                var sw = new Stopwatch();
                sw.Start();
                for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
                {
                    // Debug.Log("Receiver Update " + iBa);
                    // rtReceiver[iBa] = buildingAcoustics[iBa].receiverRoom.GetJustHrtfForPatches(fftLength);
                    SoundReceiverObject.GetHrtfsForAllAddSs(_rtReceiver[iBa],
                        buildingAcoustics[iBa].partition.SecondarySourcesPosPatches);
                    posUpdate = true;
                }

                sw.Stop();
                ReceiverUpdate = sw.Elapsed.TotalMilliseconds;
            }

            ForceReceiverUpdate = false;
            return posUpdate;
        }

        /// <summary>
        /// Implementation of Update Source Data for filter Construction if Source Update
        /// </summary>
        /// <returns>Source Update</returns>
        protected override bool RealTimeSourceUpdate()
        {
            if (!init)
                return false;
            var posUpdate = false;
            if (SoundSourceObjects[0].SoundObjectChanged() | ForceSourceUpdate)
            {
                var sw = new Stopwatch();
                sw.Start();
                for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
                {
                    SoundSourceObjects[0]
                        .GetSourceTauDavyPatches(buildingAcoustics[iBa].partition, _rtSource[iBa]);
                    posUpdate = true;
                }

                sw.Stop();
                SourceUpdate = sw.Elapsed.TotalMilliseconds;
            }

            ForceSourceUpdate = false;
            return posUpdate;
        }

        private float[][][] CalcReceiverUpdate()
        {
            var rtReceiverMono = new float[buildingAcoustics.Length][][];
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            {
                SoundReceiverObject.SoundObjectChanged();
                rtReceiverMono[iBa] = SoundReceiverObject
                    .GetHrtfMonoForSsPatchesDavy(buildingAcoustics[iBa].partition.SecondarySourcesPosPatches, FilterResolution,
                        out _rjSamplesDelay[iBa]);
            }

            ForceReceiverUpdate = false;
            return rtReceiverMono;
        }

        private void UpdateMonoIrs(bool log=false)
        {
            var rtReceiverMono = CalcReceiverUpdate();
            _irReceiverRoomFreq = new float[buildingAcoustics.Length][][];
            var dmfpSamples = (int)buildingAcoustics[0].receiverRoom.Properties.DmfpSamples;
            var sourceId = 0;
            var numOfPatches = NumOfPatches;
            var samplesOut = new float[_irRightTime.Length * numOfPatches];

            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            {
                _irReceiverRoomFreq[iBa] = new float[buildingAcoustics[iBa].partition.NumOfPatches][];
                for (var iPatch = 0; iPatch < buildingAcoustics[iBa].partition.NumOfPatches; iPatch++)
                {
                    var irMono = new float[FilterResolution];
                
                    if (applyReceiverDirectSound)
                    {
                        // Add Direct Energy
                        rtReceiverMono[iBa][iPatch].CopyTo(irMono, 0);
                    }
                    else
                    {
                        if (!applyReceiverReverbSound)
                            irMono[3000] = 1;
                    }

                    if (applyReceiverReverbSound)
                    {
                        // Add Reverberation
                        for (var n = 0;
                            n < Mathf.Min(_revReceiverTime.Length, FilterResolution - _rjSamplesDelay[iBa][iPatch]);
                            n++)
                            irMono[n + dmfpSamples] += _revReceiverTime[n];
                    }

                    var factor = applyIrFactor
                        ? Mathf.Sqrt(SourceSoundPowerNewApproach * buildingAcoustics[iBa].partition.Patches[iPatch].Geometry.Area /
                                     (buildingAcoustics[iBa].partition.Geometry.Area * 4 * Mathf.PI))
                        : 1;
                
                    _irReceiverRoomFreq[iBa][iPatch] =
                        ComplexFloats.Multiply(FftHelper.Fft(irMono, true), factor);
                
                    if (!log) continue;
                
                    for (var n = 0; n < irMono.Length; n++)
                    {
                        samplesOut[sourceId + numOfPatches * n] = irMono[n];
                    }

                    sourceId++;
                }
            }

            _applyReceiverDirectSoundShadow = applyReceiverDirectSound;
            _applyIrFactorShadow = applyIrFactor;
            _applyReceiverReverbSoundShadow = applyReceiverReverbSound;
        
            if (!log) return;
        
            const string postFix = "irDavyOutdoorSinglePatch-irReceiverMono";
            SavWav.SaveWavFile(samplesOut, postFix, numOfPatches);

            SavWav.SaveSamples(rtReceiverMono, "rtReceiverDirect");
            SavWav.SaveSamples(_revReceiverTime, "revReceiverTime");
            SavWav.SaveSamples(_irReceiverRoomFreq, "irReceiverRoomFreq");
        }

        private void UpdateFilter(AudioRender ar,bool logIr=false)
        {
            var sourceId = 0;
            var samplesOut = new float[_irRightTime.Length * 2];
            var samplesOutSum = new float[_irRightTime.Length * 2];
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            {
                for (var iPatch = 0; iPatch < buildingAcoustics[iBa].partition.NumOfPatches; iPatch++)
                {
                    var rtSourceRef = _rtSource[iBa][iPatch];
                    var irReceiverRoomFreqRef = _irReceiverRoomFreq[iBa][iPatch];
                    var rtReceiverLeftRef = _rtReceiver[iBa][2 * iPatch];
                    var rtReceiverRightRef = _rtReceiver[iBa][2 * iPatch + 1];
                    for (var n = 0; n < rtSourceRef.Length; n += 2)
                    {
                        var irSsReal = rtSourceRef[n];
                        var irSsImag = rtSourceRef[n + 1];
                        var irReceiverRoomFreqReal = irReceiverRoomFreqRef[n];
                        var irReceiverRoomFreqImag = irReceiverRoomFreqRef[n + 1];
                        var rtLReal = rtReceiverLeftRef[n];
                        var rtLImag = rtReceiverLeftRef[n + 1];
                        var rtRReal = rtReceiverRightRef[n];
                        var rtRImag = rtReceiverRightRef[n + 1];

                        var monoReal = irSsReal * irReceiverRoomFreqReal - irSsImag * irReceiverRoomFreqImag;
                        var monoImag = irSsReal * irReceiverRoomFreqImag + irSsImag * irReceiverRoomFreqReal;

                        _irLeftFreq[n] = monoReal * rtLReal - monoImag * rtLImag;
                        _irLeftFreq[n + 1] = monoReal * rtLImag + monoImag * rtLReal;
                        _irRightFreq[n] = monoReal * rtRReal - monoImag * rtRImag;
                        _irRightFreq[n + 1] = monoReal * rtRImag + monoImag * rtRReal;
                    }
                
                    fftwf.execute(_ifftPlanLeft);
                    fftwf.execute(_ifftPlanRight);
                    if (logIr)
                    {
                        for (var n = 0; n < _irRightTime.Length; n++)
                        {
                            samplesOut[2 * n] = _irLeftTime[n];
                            samplesOut[2 * n + 1] = _irRightTime[n];
                        }
                    
                        var postFix1 = "irDavyOutdoorSinglePatch-FinalIrPerPatch_"+sourceId;
                        SavWav.SaveWavFile(samplesOut, postFix1, 2);
                    
                        for (var n = 0; n < _irRightTime.Length; n++)
                        {
                            samplesOutSum[2 * n] += _irLeftTime[n];
                            samplesOutSum[2 * n + 1] += _irRightTime[n];
                        }
                    }
                    else
                    {
                        ar.UpdateFilter(_irLeftTime, _irRightTime, sourceId);
                    }
                    sourceId++;
                }
            }

            if (logIr)
            {
                const string postFix2 = "irDavyOutdoorSinglePatch-FinalIrSum";
                SavWav.SaveWavFile(samplesOutSum, postFix2, 2);

                SavWav.SaveSamples(_rtSource, "rtSourceFreq");
                SavWav.SaveSamples(_irReceiverRoomFreq, "irReceiverRoomUpdateFreq");
                SavWav.SaveSamples(_rtReceiver, "rtReceiverFreq");
            }

            if (LogRealtime)
            {
                DataLogger.DebugData = true;
                DataLogger.Log(LogName + ".irLeftTime(:," + HrirCounter + ")", _irLeftTime);
                DataLogger.Log(LogName + ".irRightTime(:," + HrirCounter + ")", _irRightTime);
                DataLogger.DebugData = false;
            }
        }
    
        public override void SaveImpulseResponse()
        {
            UpdateMonoIrs(true);
            UpdateFilter(null, true);
        }


        private void InitIfftIr()
        {
            _ifftInputLeftHandle = GCHandle.Alloc(_irLeftFreq, GCHandleType.Pinned);
            _ifftResultLeftHandle = GCHandle.Alloc(_irLeftTime, GCHandleType.Pinned);
            _ifftPlanLeft = fftwf.dft_c2r_1d(FilterResolution, _ifftInputLeftHandle.AddrOfPinnedObject(),
                _ifftResultLeftHandle.AddrOfPinnedObject(), fftw_flags.Estimate);

            _ifftInputRightHandle = GCHandle.Alloc(_irRightFreq, GCHandleType.Pinned);
            _ifftResultRightHandle = GCHandle.Alloc(_irRightTime, GCHandleType.Pinned);
            _ifftPlanRight = fftwf.dft_c2r_1d(FilterResolution, _ifftInputRightHandle.AddrOfPinnedObject(),
                _ifftResultRightHandle.AddrOfPinnedObject(), fftw_flags.Estimate);
        }

        /// <summary>
        /// Log the current binaural impulse response (sum of all SS)
        /// </summary>
        public void LogBirSum()
        {
            var receiverString = Vec32Str(Receiver.position) + "-Rot" + Vec32Str(Receiver.rotation.eulerAngles);
            var fileName = $"{_filePath}/BinauralLog-ReceiverPos_{receiverString}.dat";
            var irLog = new float[2][];
            irLog[0] = _irLeftFreq;
            irLog[1] = _irRightFreq;
            if (File.Exists(fileName))
                return;

            using (var file = File.Open(fileName, FileMode.Create))
            {
                using (var writer = new BinaryWriter(file))
                {
                    WriteIrsToBinary(writer, irLog);
                }
            }

            fileName = $"{_filePath}/irReceiver-ReceiverPos_{receiverString}.dat";
            using (var file = File.Open(fileName, FileMode.Create))
            {
                using (var writer = new BinaryWriter(file))
                {
                    WriteIrsToBinary(writer, FftHelper.Ifft(_irReceiverRoomFreq), 0, true);
                }
            }
        }

        private string Vec32Str(Vector3 vec)
        {
            CultureInfo.CurrentCulture = new CultureInfo("en-GB");
            return vec.x.ToString("000.0") + "_" + vec.y.ToString("000.0") + "_" + vec.z.ToString("000.0");
        }

        private void WriteIrsToBinary(BinaryWriter bw, float[][][] array, int offset = 0, bool ignoreWarn = false)
        {
            bw.Write(offset);
            for (var iBa = 0; iBa < array.Length; iBa++)
            for (var iPatch = 0; iPatch < array[iBa].Length; iPatch++)
            {
                if (!ignoreWarn && array[iBa][iPatch].Length != 44100)
                    Log.Error("Invalid Length of Ir for writing to Binary.");

                for (var i = 0; i < array[iBa][iPatch].Length; i++) bw.Write(array[iBa][iPatch][i]);
            }
        }

        private void WriteIrsToBinary(BinaryWriter bw, float[][] array, int offset = 0)
        {
            bw.Write(offset);
            for (var i = 0; i < array.Length; i++)
            {
                if (array[i].Length != 44100)
                    Log.Error("Invalid Length of Ir for writing to Binary.");

                for (var n = 0; n < array[i].Length; n++) bw.Write(array[i][n]);
            }
        }
    }
}