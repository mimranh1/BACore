﻿using System;
using System.IO;
using log4net;
using TestMySpline;
using UnityEngine;
using VA;

namespace BA.BACore
{
    /// <summary>
    /// Implementation of new Approach Outdoor considering Direct and reverberation in Source and Receiver room
    /// First try to bake the results beforehand so that the moving secondary source is baked up to the secondary source of the patch in the receiver room
    /// not working well
    /// This class just run with VA and therefore it has an own renderer and deactivate the default one
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    [Serializable]
    public class AcOutdoorBaked : AcNewApproachPartition
    {
        private float[] revReceiverTime;
        private int filterLength;
        private float[][][] rtReceiver;
        private float[][][] rtSource;
        private int[][] rjSamplesDelay;

        private static VANet vaNet;
        private static readonly string vaServer = "localhost";
        private static readonly int vaPort = 12340;
        [SerializeField] public string[] vaSourceFilePaths;
        private int[] vaSourceIds;

        private static readonly ILog Log = LogManager.GetLogger(typeof(AcOutdoorBaked));

        /// <summary>
        /// AudioClip to Bake and Playback
        /// </summary>
        public AudioClip inputAudioToBake;

        private int audioPointer;

        /// <summary>
        /// block length for the Bake add overlap 
        /// </summary>
        public int blockLength;

        /// <summary>
        /// fft filter length of the baking process
        /// </summary>
        public int fftLength;


        public override AuralisationMethod AuralisationMethod => AuralisationMethod.OutdoorModelBaked;

        /// <summary>
        /// Implementation of Initialisation with Init BuildingAcoustics Array from FilterManager
        /// </summary>
        /// <param name="bas">buildingAcoustics Array from FilterManager</param>
        public override void Init()
        {
            moduleName = "AcOutdoorBaked";
            init = true;
            if (vaSourceFilePaths == null || vaSourceFilePaths.Length == 0)
            {
                var sumPatches = 0;
                var currId = 0;
                for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
                {
                    sumPatches += buildingAcoustics[iBa].partition.Patches.Count;
                }

                vaSourceFilePaths = new string[sumPatches];
                for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
                for (var iPatch = 0; iPatch < buildingAcoustics[iBa].NumOfPatches; iPatch++)
                {
                    vaSourceFilePaths[currId] = "SireneSig" + (iBa + 1) + "-" + (iPatch + 1) + ".wav";
                    currId++;
                }

                vaSourceIds = new int[sumPatches];
            }
        }


        /// <summary>
        /// Implementation of Calculate pre-calculation and and set AudioRenderer
        /// </summary>
        /// <param name="ar">AudioRenderer from FilterManager</param>
        protected override void OfflineCalculation(AudioRender ar)
        {
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++) buildingAcoustics[iBa].ProcessAll();
            SoundSourceObjects[0].Init(FilterResolution);
            SoundReceiverObject.Init(FilterResolution);

            CreateSoundSources();
            SetListener();

            VA.SetRenderingModuleMuted("MyGenericRenderer", false);
            VA.SetRenderingModuleGain("MyGenericRenderer", 1.0);

            var constFactorRev = Mathf.Sqrt(4 / buildingAcoustics[0].receiverRoom.Properties.EquivalentAbsorptionArea);
            revReceiverTime = buildingAcoustics[0].receiverRoom.reverberation;
            for (var i = 0; i < revReceiverTime.Length; i++)
                revReceiverTime[i] *= constFactorRev;
            rtReceiver = new float[buildingAcoustics.Length][][];
            rtSource = new float[buildingAcoustics.Length][][];
            rjSamplesDelay = new int[buildingAcoustics.Length][];
            filterLength = revReceiverTime.Length + (int) (BaSettings.Instance.SampleRate * 5f / BaSettings.Instance.C0);

            RealTimeReceiverUpdate();
            RealTimeCalculation();
        }


        /// <summary>
        /// Implementation of Realtime Update, called if Source or Receiver Updates
        /// </summary>
        /// <param name="logId">write log file if logId > 1</param>
        protected override void RealTimeCalculation(int logId = 0)
        {
            var patchTotCounter = 0;
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            {
                var patchesActive = buildingAcoustics[iBa].partition.patchesHandler.IsActive;
                for (var iPatch = 0; iPatch < patchesActive.Length; iPatch++)
                {
                    if (!patchesActive[iPatch] | (vaSourceFilePaths[patchTotCounter] == ""))
                    {
                        patchTotCounter++;
                        continue;
                    }

                    var irLeft = new float[filterLength];
                    var irRight = new float[filterLength];

                    // Add Direct Energy
                    rtReceiver[iBa][2 * iPatch].CopyTo(irLeft, 0);
                    rtReceiver[iBa][2 * iPatch + 1].CopyTo(irRight, 0);

                    // Add Reverberation
                    for (var n = 0; n < revReceiverTime.Length; n++)
                    {
                        irLeft[n + rjSamplesDelay[iBa][iPatch]] += revReceiverTime[n];
                        irRight[n + rjSamplesDelay[iBa][iPatch]] += revReceiverTime[n];
                    }

                    VA.UpdateGenericPath("MyGenericRenderer", vaSourceIds[patchTotCounter], 1,
                        1, 0.0f, irLeft.Length, irLeft);
                    VA.UpdateGenericPath("MyGenericRenderer", vaSourceIds[patchTotCounter], 1,
                        2, 0.0f, irRight.Length, irRight);
                    patchTotCounter++;
                }
            }
        }

        /// <summary>
        /// Implementation of Update Receiver Data for filter Construction if Receiver Update
        /// </summary>
        /// <returns>Receiver Update</returns>
        protected override bool RealTimeReceiverUpdate()
        {
            var posUpdate = false;
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
                if (SoundReceiverObject.SoundObjectChanged() | ForceReceiverUpdate)
                {
                    //Debug.Log("Receiver Update " + iBa);
                    rtReceiver[iBa] = SoundReceiverObject
                        .GetHrirForSsPatches(filterLength, buildingAcoustics[iBa].partition.SecondarySourcesPosPatches,
                            out rjSamplesDelay[iBa], out _, out _);
                    posUpdate = true;
                    ForceReceiverUpdate = false;
                }

            return posUpdate;
        }

        /// <summary>
        /// Implementation of Update Source Data for filter Construction if Source Update
        /// </summary>
        /// <returns>Source Update</returns>
        protected override bool RealTimeSourceUpdate()
        {
            var posUpdate = false;
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
                if (SoundSourceObjects[0].SoundObjectChanged() | ForceSourceUpdate)
                {
                    //Debug.Log("Receiver Update " + iBa);
                    rtSource[iBa] = SoundSourceObjects[0]
                        .GetSourceFiForPatches(buildingAcoustics[iBa].partition, filterLength, out var ri, true);
                    posUpdate = true;
                    ForceSourceUpdate = false;
                }

            return posUpdate;
        }

        // VA stuff

        private static VANet VA
        {
            get
            {
                if (vaNet == null)
                {
                    vaNet = new VANet();
                    VaInit(); // Try to connect as early as possible
                }

                return vaNet;
            }
        }

        private static void VaInit()
        {
            if (!VA.Connect(vaServer, vaPort))
            {
                Debug.LogWarning("Could not connect to VA server on " + vaServer + " using port " + vaPort);
                return;
            }

            //if (vaResetOnStart)
            //    VA.Reset();

            // Add Asset folder as search path for VA (only works if VA is running on same host PC)
            if (!VA.AddSearchPath(Application.dataPath))
                Debug.LogError(
                    "Could not add application assets folder to VA search path, VA server running on remote host?");
            if (!VA.AddSearchPath($"{BaSettings.Instance.RootPath}/Resources/Sounds"))
                Debug.LogError(
                    "Could not add BACore/Runtime/Sounds folder to VA search path.");
        }

        private void CreateSoundSources()
        {
            if (vaSourceFilePaths == null)
            {
                Debug.LogError("Set SourcePaths of ...");
                return;
            }

            var patchTotCounter = 0;
            vaSourceIds = new int[vaSourceFilePaths.Length];
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            for (var iPatch = 0; iPatch < buildingAcoustics[iBa].NumOfPatches; iPatch++)
            {
                if (vaSourceFilePaths[patchTotCounter] != "")
                {
                    var sourceName = "baId" + (iBa + 1) + "-patchId" + (iPatch + 1);

                    vaSourceIds[patchTotCounter] = VA.CreateSoundSource(sourceName);

                    var signalSourceId =
                        VA.CreateSignalSourceBufferFromFile(vaSourceFilePaths[patchTotCounter], sourceName);
                    VA.SetSignalSourceBufferLooping(signalSourceId, true);
                    VA.SetSoundSourceSignalSource(vaSourceIds[patchTotCounter], signalSourceId);
                    VA.SetSoundSourcePosition(vaSourceIds[patchTotCounter], new VAVec3(0f, 0f, 0f));
                    VA.SetSoundSourceOrientationVU(vaSourceIds[patchTotCounter], new VAVec3(1f, 0f, 0f),
                        new VAVec3(0f, 0f, 0f));
                    VA.SetSignalSourceBufferPlaybackAction(signalSourceId, "PLAY");
                }

                patchTotCounter++;
            }
        }

        private void SetListener()
        {
            var listenerId = VA.CreateSoundReceiver("Listenerrrrr");
            VA.SetSoundReceiverAuralizationMode(listenerId, "all");

            VA.SetSoundReceiverAnthropometricData(listenerId, 0.12, 0.10, 0.15);
            VA.SetSoundReceiverPosition(listenerId, new VAVec3(0f, 0f, 0f));
            VA.SetSoundReceiverOrientation(listenerId, new VAQuat(1f, 0f, 0f, 0));

            VA.SetArtificialReverberationTime("MyGenericRenderer", 0.3f);
        }

        private float[][][] GetTauPatchesFreqComplex(bool logTau = false)
        {
            var baLength = buildingAcoustics.Length;
            UpdateBaVisible();
            var tauSqrt1PhaseFreqComplex = new float[baLength][][];
            for (var iBa = 0; iBa < baLength; iBa++)
            {
                var partition = buildingAcoustics[iBa].partition;
                var numPatches = partition.patchesHandler.NumOfPatches;
                tauSqrt1PhaseFreqComplex[iBa] = new float[numPatches][];
                for (var iPatch = 0; iPatch < numPatches; iPatch++)
                {
                    var tau3RdOctaveFreq = 10 ^ (partition.patchesHandler.ReductionIndexPatches[iPatch] / -10f);
                    var tauInterpolatedFreq = CalcTauExtrapolatedFrom3RdOctaveTau(tau3RdOctaveFreq, FilterResolution);
                    tauSqrt1PhaseFreqComplex[iBa][iPatch] =
                        CalcSqrtTauComplexFromTauInterpolatedEven(tauInterpolatedFreq.Tf, LogName, 1, 2);
                    if (logTau)
                    {
                        DataLogger.DebugData = true;
                        DataLogger.Log("interpol.tau3RdOctaveFreq{" + (iBa + 1) + ",1}(:," + (iPatch + 1) + ")",
                            tau3RdOctaveFreq);
                        DataLogger.Log("interpol.tauInterpolatedFreq{" + (iBa + 1) + ",1}(:," + (iPatch + 1) + ")",
                            tauInterpolatedFreq);
                        DataLogger.Log("interpol.tauSqrt1PhaseFreqComplex{" + (iBa + 1) + ",1}(:," + (iPatch + 1) + ")",
                            tauSqrt1PhaseFreqComplex[iBa][iPatch]);
                        DataLogger.DebugData = false;
                    }
                }
            }

            if (logTau)
            {
                DataLogger.DebugData = true;
                var interpolationFreqBand = BaSettings.Instance.GetInterpolatedFrequencyBand(FilterResolution / 2);
                DataLogger.Log("interpol.fInterpolation", interpolationFreqBand);
                var thirdOctaveFrequencyBand = BaSettings.Instance.ThirdOctaveFrequencyBandNorm;
                var frequencyBand = BaSettings.Instance.ThirdOctaveFrequencyBandShort;
                var extendedFrequencyBand = BaSettings.Instance.ThirdOctaveFrequencyBandExtended;
                DataLogger.Log("interpol.thirdOctaveFrequencyBand", thirdOctaveFrequencyBand);
                DataLogger.Log("interpol.frequencyBand", frequencyBand);
                DataLogger.Log("interpol.extendedFrequencyBand", extendedFrequencyBand);
                DataLogger.DebugData = false;
            }

            return tauSqrt1PhaseFreqComplex;
        }

        /// <summary>
        /// run the Bake progress
        /// </summary>
        /// <param name="bas">input Building acoustics array</param>
        /// <param name="speedCarKmph"> speed of car in kmph</param>
        /// <param name="blockLengthInput">input block length</param>
        /// <param name="writeLog">if write logfiles on parallel</param>
        public void LogMovingOutdoorSourceSsIr( float speedCarKmph, int blockLengthInput,
            bool writeLog = false)
        {
            Log.Info("Start LogMovingOutdoorSourceSsIr");
            var deltaX = speedCarKmph / 3.6f * (blockLengthInput / 44100f);
            LoggingConfiguration.ConfigureViaFile();
            LogRealtime = false;
            DataLogger.DebugData = false;
            Init();
            filterLength = 44100;
            var source = audioRenderer.SourceMover;
            source.RestartPositionAndSound();
            var oldPos = source.transform.position;
            var numPatchesTot = 0;
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            {
                buildingAcoustics[iBa].ProcessAll();
                numPatchesTot += buildingAcoustics[iBa].NumOfPatches;
            }

            SoundSourceObjects[0].Init(FilterResolution);
            SoundReceiverObject.Init(FilterResolution);

            var maxOutSamples = (int) (source.WayLength * BaSettings.Instance.SampleRate / (speedCarKmph / 3.6f));
            var outputClips = new AudioClip[numPatchesTot];
            var tauPatchesFreqComplex = GetTauPatchesFreqComplex(true);
            if (writeLog)
                DataLogger.DebugData = false;
            var patchPos = new Vector3[buildingAcoustics.Length][];
            var bufferAddOverlap = new CircularMonauralBuffer[buildingAcoustics.Length][];
            rtSource = new float[buildingAcoustics.Length][][];
            var currId = 0;
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            {
                var patches = buildingAcoustics[iBa].partition.Patches;
                patchPos[iBa] = new Vector3[patches.Count];
                bufferAddOverlap[iBa] = new CircularMonauralBuffer[patches.Count];
                for (var iPatch = 0; iPatch < patches.Count; iPatch++)
                {
                    outputClips[currId] =
                        AudioClip.Create("irSS-" + iBa + "-" + iPatch, maxOutSamples + 44100 * 10, 1, 44100, false);
                    bufferAddOverlap[iBa][iPatch] = InitAddOverlap(filterLength, blockLengthInput);
                    patchPos[iBa][iPatch] = patches[iPatch].Geometry.Position;

                    if (writeLog)
                    {
                        DataLogger.Log(LogName + "PatchPos{" + (iBa + 1) + "," + (iPatch + 1) + "}",
                            buildingAcoustics[iBa].partition.Patches[iPatch].Geometry.Position);
                        DataLogger.Log(LogName + "tauPatchesFreqComplex{" + (iBa + 1) + "," + (iPatch + 1) + "}",
                            tauPatchesFreqComplex[iBa][iPatch]);
                    }

                    currId++;
                }
            }

            var data = new float[blockLengthInput];
            audioPointer = 0;
            var logId = writeLog ? 0 : -1;
            while (true)
            {
                if (writeLog)
                    Log.Info("LogMovingOutdoorSourceSsIr: audioPointer " + audioPointer + "/" + inputAudioToBake.samples +
                             " z " + source.Position.z);
                source.transform.position = source.UpdatePosition(source.transform.position, deltaX);
                HrirCounter++;
                RealTimeSourceUpdate();
                var irSs = CalcNewApproachOutdoorSsIr(tauPatchesFreqComplex, logId, writeLog);
                inputAudioToBake.GetData(data, audioPointer % inputAudioToBake.samples);
                var patchCounter = 0;
                for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
                {
                    var patchVisible = buildingAcoustics[iBa].IsVisible;
                    for (var iPatch = 0; iPatch < buildingAcoustics[iBa].partition.Patches.Count; iPatch++)
                    {
                        var dataOut = new float[data.Length];
                        if (irSs[iBa][iPatch] != null && patchVisible)
                        {
                            var irDoppler = DopplerShift(irSs[iBa][iPatch], source, patchPos[iBa][iPatch]);
                            if (writeLog)
                            {
                                DataLogger.DebugData = true;
                                dataOut = OverlapAddMonaural(bufferAddOverlap[iBa][iPatch], data, irDoppler,
                                    "{" + (iBa + 1) + "," + (iPatch + 1) + "}(:," + (logId + 1) + ")");
                                DataLogger.Log(
                                    LogName + "irSs{" + (iBa + 1) + "," + (iPatch + 1) + "}(:," + (logId + 1) + ")",
                                    irSs[iBa][iPatch]);
                                DataLogger.Log(
                                    LogName + "irDoppler{" + (iBa + 1) + "," + (iPatch + 1) + "}(:," + (logId + 1) + ")",
                                    irDoppler);
                                DataLogger.DebugData = false;
                            }
                            else
                            {
                                dataOut = OverlapAddMonaural(bufferAddOverlap[iBa][iPatch], data, irDoppler);
                            }
                        }

                        outputClips[patchCounter].SetData(dataOut, audioPointer);

                        if (source.endReached || writeLog && logId > 3)
                        {
                            var remainingBuffer = bufferAddOverlap[iBa][iPatch].GetRemainingBuffer();
                            outputClips[patchCounter].SetData(remainingBuffer, audioPointer + blockLengthInput);
                        }

                        patchCounter++;
                    }
                }

                if (source.endReached || writeLog && logId > 3)
                    break;

                audioPointer += blockLengthInput;

                if (writeLog)
                    logId++;
            }

            source.transform.position = oldPos;

            // write wav files
            var iCurr = 0;
            var subFolder = writeLog
                ? "OutdoorBaked/" + inputAudioToBake.name + "-speed_" + speedCarKmph + "-blockLen_" + blockLengthInput +
                  "/WayLen_" + source.WayLength + "-Log110/"
                : "OutdoorBaked/" + inputAudioToBake.name + "-speed_" + speedCarKmph + "-blockLen_" + blockLengthInput +
                  "/WayLen_" + source.WayLength + "/";
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            for (var iPatch = 0; iPatch < buildingAcoustics[iBa].partition.Patches.Count; iPatch++)
            {
                var fileName = "iBa_" + (iBa + 1) +
                               "-iPatch_" + (iPatch + 1);

                var filePath = Path.Combine(BaSettings.Instance.RootPath + "/Resources/Sounds", subFolder + fileName);
                SavWav.Save(filePath, outputClips[iCurr]);
                iCurr++;
            }

            // move log data file to output folder
            if (File.Exists($"{BaSettings.Instance.RootPath}/Log/logDataFile.txt"))
                File.Move($"{BaSettings.Instance.RootPath}/Log/logDataFile.txt",
                    $"{BaSettings.Instance.RootPath}/Resources/Sounds/OutdoorBaked/{subFolder}logDataFile.txt");
        }


        private float[][][] CalcNewApproachOutdoorBir(float[][][] tauPatchesFreqComplex, int logId = 0, bool logAll = false)
        {
            // Start Partition Wall
            var irSs = new float[buildingAcoustics.Length][][];
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            {
                var patchesActive = buildingAcoustics[iBa].partition.patchesHandler.IsActive;
                irSs[iBa] = new float[2 * tauPatchesFreqComplex[iBa].Length][];
                if (!buildingAcoustics[iBa].IsVisible)
                    continue;
                for (var iPatch = 0; iPatch < tauPatchesFreqComplex[iBa].Length; iPatch++)
                {
                    if (!patchesActive[iPatch])
                        continue;
                    var factor = SourceSoundPowerNewApproach * buildingAcoustics[iBa].partition.Patches[iPatch].Geometry.Area /
                                 (buildingAcoustics[iBa].partition.Geometry.Area * 4 * Mathf.PI);
                    var irPatchFreqComplex =
                        ComplexFloats.Multiply(rtSource[iBa][iPatch], tauPatchesFreqComplex[iBa][iPatch],
                            Mathf.Sqrt(factor));


                    var irRoomLeft = new float[filterLength];
                    var irRoomRight = new float[filterLength];

                    // Add Direct Energy
                    rtReceiver[iBa][2 * iPatch].CopyTo(irRoomLeft, 0);
                    rtReceiver[iBa][2 * iPatch + 1].CopyTo(irRoomRight, 0);

                    // Add Reverberation
                    for (var n = 0; n < revReceiverTime.Length; n++)
                    {
                        irRoomLeft[n + rjSamplesDelay[iBa][iPatch]] += revReceiverTime[n];
                        irRoomRight[n + rjSamplesDelay[iBa][iPatch]] += revReceiverTime[n];
                    }

                    var birLeftFreqComplex =
                        ComplexFloats.Multiply(irPatchFreqComplex, FftHelper.Fft(irRoomLeft, true));
                    var birRightFreqComplex =
                        ComplexFloats.Multiply(irPatchFreqComplex, FftHelper.Fft(irRoomRight, true));

                    if (logAll)
                    {
                        DataLogger.Log(
                            LogName + "irPatchFreqComplex{" + (iBa + 1) + "," + (iPatch + 1) + "}(:," + (logId + 1) + ")",
                            irPatchFreqComplex);
                        DataLogger.Log(
                            LogName + "rtSourceFreqComplex{" + (iBa + 1) + "," + (iPatch + 1) + "}(:," + (logId + 1) + ")",
                            rtSource[iBa][iPatch]);
                        DataLogger.Log(
                            LogName + "factor{" + (iBa + 1) + "," + (iPatch + 1) + "}(:," + (logId + 1) + ")",
                            Mathf.Sqrt(factor));
                    }

                    irSs[iBa][2 * iPatch] = FftHelper.Ifft(birLeftFreqComplex);
                    irSs[iBa][2 * iPatch + 1] = FftHelper.Ifft(birRightFreqComplex);

                    if (logId >= 0)
                        DataLogger.Log(LogName + "irSs{" + (iBa + 1) + "," + (iPatch + 1) + "}(:," + (logId + 1) + ")",
                            irSs[iBa][iPatch]);
                }
            }

            return irSs;
        }


        private float[][][] CalcNewApproachOutdoorSsIr(float[][][] tauPatchesFreqComplex, int logId = 0,
            bool logAll = false)
        {
            // Start Partition Wall
            var irSs = new float[buildingAcoustics.Length][][];
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
            {
                var patchesActive = buildingAcoustics[iBa].partition.patchesHandler.IsActive;
                irSs[iBa] = new float[tauPatchesFreqComplex[iBa].Length][];
                if (!buildingAcoustics[iBa].IsVisible)
                    continue;
                for (var iPatch = 0; iPatch < tauPatchesFreqComplex[iBa].Length; iPatch++)
                {
                    if (!patchesActive[iPatch])
                        continue;
                    var factor = SourceSoundPowerNewApproach * buildingAcoustics[iBa].partition.Patches[iPatch].Geometry.Area /
                                 (buildingAcoustics[iBa].partition.Geometry.Area * 4 * Mathf.PI);
                    var irPatchFreqComplex =
                        ComplexFloats.Multiply(rtSource[iBa][iPatch], tauPatchesFreqComplex[iBa][iPatch],
                            Mathf.Sqrt(factor));

                    if (logAll)
                    {
                        DataLogger.Log(
                            LogName + "irPatchFreqComplex{" + (iBa + 1) + "," + (iPatch + 1) + "}(:," + (logId + 1) + ")",
                            irPatchFreqComplex);
                        DataLogger.Log(
                            LogName + "rtSourceFreqComplex{" + (iBa + 1) + "," + (iPatch + 1) + "}(:," + (logId + 1) + ")",
                            rtSource[iBa][iPatch]);
                        DataLogger.Log(
                            LogName + "factor{" + (iBa + 1) + "," + (iPatch + 1) + "}(:," + (logId + 1) + ")",
                            Mathf.Sqrt(factor));
                    }

                    irSs[iBa][iPatch] = FftHelper.Ifft(irPatchFreqComplex);

                    if (logId >= 0)
                        DataLogger.Log(LogName + "irSs{" + (iBa + 1) + "," + (iPatch + 1) + "}(:," + (logId + 1) + ")",
                            irSs[iBa][iPatch]);
                }
            }

            return irSs;
        }


        private float[] DopplerShift(float[] ir, SoundSourceMover source, Vector3 patchPos)
        {
            var deltaDiff = source.Position - patchPos;
            var deltaZ = deltaDiff.z;
            var deltaX = deltaDiff.x;
            var alpha = Mathf.Atan(deltaX / deltaZ);
            var directionX = source.DirectionForward.x;
            var directionZ = source.DirectionForward.z;
            var v = source.speedKmh / 3.6f;
            var vDoppler = 0f;
            if (directionX != 0)
                vDoppler = Mathf.Sin(alpha) * v;
            else if (directionZ != 0)
                vDoppler = Mathf.Cos(alpha) * v;
            else
                Debug.LogError("Invalid Direction Given");

            var c = BaSettings.Instance.C0;
            var fCoeff = (c + vDoppler) / c;

            var kDoppler = (int) (fftLength * fCoeff);
            var hp = new float[kDoppler];
            for (var i = 0; i < Mathf.Min(ir.Length, hp.Length); i++)
                hp[i] = ir[i];

            var x = new float[kDoppler];
            for (var i = 0; i < kDoppler; i++)
                x[i] = i / (float) kDoppler;
            var xs = new float[fftLength];
            for (var i = 0; i < fftLength; i++)
                xs[i] = i / (float) fftLength;
            return CubicSpline.Compute(x, hp, xs);
            // hp = resample(hp, fftLength, kDoppler);
        }


        private CircularMonauralBuffer InitAddOverlap(int lengthIr, int blockLen)
        {
            blockLength = blockLen;

            fftLength = Mathf.Max(blockLength, blockLength + lengthIr - 1);
            // numBlock = (int) Mathf.Ceil((float) fftLength / blockLength);

            // Crate the output buffer for the final result
            return new CircularMonauralBuffer(fftLength + blockLength, blockLength);
        }


        private float[] OverlapAddMonaural(CircularMonauralBuffer bufferInput, float[] data, float[] irTime,
            string log = null)
        {
            ConvTimeInFreq(data, irTime, out var bufferTime);

            return bufferInput.SumAndIncreasePointer(bufferTime, log);
        }


        private void ConvTimeInFreq(float[] irTime1, float[] irTime2, out float[] bufferTime)
        {
            var irFreq1 = FftHelper.Fft(irTime1, true, fftLength);
            var irFreq2 = FftHelper.Fft(irTime2, true, fftLength);
            bufferTime = FftHelper.Ifft(MultiplyComplex(irFreq1, irFreq2));
        }

        private float[] MultiplyComplex(float[] tf1, float[] tf2)
        {
            if (tf1.Length != tf2.Length)
                Debug.LogError("Have to have same Length.");

            var tfResult = new float[tf1.Length];
            for (var n = 0; n < tf1.Length; n += 2)
            {
                tfResult[n] = tf1[n] * tf2[n] - tf1[n + 1] * tf2[n + 1];
                tfResult[n + 1] = tf1[n + 1] * tf2[n] + tf1[n] * tf2[n + 1];
            }

            return tfResult;
        }
    }
}