

        /// <summary>
        /// Test the Add-Overlap Algorithm from our Realtime Renderer (AudioRendererUnity)
        /// </summary>
        public void TestLogAddOverlap()
        {
            LoggingConfiguration.ConfigureViaFile();
            DataLogger.DebugData = false;

            Awake();
            Start();
            //arUnity.TestLogAddOverlap();
        }



        public void TestLatencies(int repetitions = 200)
        {
            if (!Application.isPlaying)
            {
                LoggingConfiguration.ConfigureViaFile();
                DataLogger.DebugData = false;
                Awake();
                Start();
                CalculateOfflineFilter();
            }

            var modelName = auralisationSelector.settings.moduleName;

            var sourceLatencies = new double[repetitions];
            var receiverLatencies = new double[repetitions];
            var filterLatencies = new double[repetitions];
            // Check Position Update
            for (var i = 0; i < repetitions; i++)
            {
                var sign = i % 2f * 2f - 1f;
                ShiftSoundObject(buildingAcoustics[0].soundSourceObjects.SoundSources[0].transform, sign * 1f);
                ShiftSoundObject(buildingAcoustics[0].soundReceiverObject.transform, sign * 1f);
                UpdateRealTimeFilter();
                sourceLatencies[i] = auralisationSelector.settings.SourceUpdate;
                receiverLatencies[i] = auralisationSelector.settings.ReceiverUpdate;
                filterLatencies[i] = auralisationSelector.settings.FilterUpdate;
            }

            DataLogger.DebugData = true;
            DataLogger.Log("modelName", modelName);
            DataLogger.Log("Position.SourceLatencies", sourceLatencies);
            DataLogger.Log("Position.ReceiverLatencies", receiverLatencies);
            DataLogger.Log("Position.FilterLatencies", filterLatencies);
            DataLogger.DebugData = false;

            sourceLatencies = new double[repetitions];
            receiverLatencies = new double[repetitions];
            filterLatencies = new double[repetitions];
            // Check Receiver Rotation Update
            for (var i = 0; i < repetitions; i++)
            {
                RotateSoundObject(buildingAcoustics[0].soundSourceObjects.SoundSources[0].transform, 10f);
                RotateSoundObject(buildingAcoustics[0].soundReceiverObject.transform, 10f);
                UpdateRealTimeFilter();
                sourceLatencies[i] = auralisationSelector.settings.SourceUpdate;
                receiverLatencies[i] = auralisationSelector.settings.ReceiverUpdate;
                filterLatencies[i] = auralisationSelector.settings.FilterUpdate;
            }

            DataLogger.DebugData = true;
            DataLogger.Log("Rotation.SourceLatencies", sourceLatencies);
            DataLogger.Log("Rotation.ReceiverLatencies", receiverLatencies);
            DataLogger.Log("Rotation.FilterLatencies", filterLatencies);
            DataLogger.DebugData = false;

            var timePrefix = DateTime.Now.ToString("yyyyMMdd-HHmmss-");
            timePrefix = modelName;

            auralisationSelector.settings.MoveLogFiles(timePrefix, "LatenciesTest");
        }

        private void ShiftSoundObject(Transform soundObject, float shift)
        {
            var pos = soundObject.position;
            pos.x += shift;
            soundObject.position = pos;
        }

        private void RotateSoundObject(Transform soundObject, float shift)
        {
            var pos = soundObject.rotation.eulerAngles;
            pos.y += shift;
            soundObject.eulerAngles = pos;
        }