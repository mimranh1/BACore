﻿using System;
using System.Diagnostics;
using log4net;
using UnityEngine;

namespace BA.BACore
{
    /// <summary>
    /// Implementation of new Approach Indoor considering Direct and reverberation in Source and Receiver room
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) 
    /// Version:        1.2  
    /// First release:  2017 
    /// Last revision:  2019 
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    [Serializable]
    public abstract class AcIndoor : AcNewApproachFlanking
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(AcIndoor));

        private float[][] _dirDirFreqPartition = new float[5][];
        private float[][] _dirRevFreqPartition = new float[5][];
        private float[][] _revDirFreqPartition = new float[5][];
        private float[][] _dirDirFreqFlanking = new float[8][];
        private float[][] _dirRevFreqFlanking = new float[8][];
        private float[][] _revDirFreqFlanking = new float[8][];
        private float[] _revRevNewApproachFreq;
        private float[][] _realTimeSource = new float[5][];
        private float[][] _realTimeReceiver = new float[5][];


        public override AuralisationMethod AuralisationMethod => AuralisationMethod.IndoorModel;

        /// <summary>
        /// Implementation of Initialisation with Init BuildingAcoustics Array from FilterManager
        /// </summary>
        /// <param name="bas">buildingAcoustics Array from FilterManager</param>
        public override void Init()
        {
            moduleName = "AcIndoor";
            init = true;
            LogName = "FilterManager.AcIndoor";

            SoundSourceObjects[0].Init(FilterResolution);
            SoundReceiverObject.Init(FilterResolution);
        }


        /// <summary>
        /// Implementation of Calculate pre-calculation and and set AudioRenderer
        /// </summary>
        /// <param name="ar">AudioRenderer from FilterManager</param>
        protected override void OfflineCalculation(AudioRender ar)
        {
            audioRenderer = ar;
            buildingAcoustics[0].ProcessAll();
            var areaSi = new float[5];
            areaSi[0] = buildingAcoustics[0].partition.Geometry.Area;
            for (var i = 0; i < 4; i++)
                areaSi[i + 1] = buildingAcoustics[0].sourceWalls[i].Geometry.Area;
            var areaSd = areaSi[0];
            var rhoC0 = BaSettings.Instance.C0 * BaSettings.Instance.Rho0;
            var equivalentAbsAreaSource = buildingAcoustics[0].sourceRoom.Properties.EquivalentAbsorptionArea;
            var equivalentAbsAreaReceiver = buildingAcoustics[0].receiverRoom.Properties.EquivalentAbsorptionArea;
            _revRevNewApproachFreq = new float[2 * FilterResolution];
            var sId = buildingAcoustics[0].SourceWallIndexes;
            var rId = buildingAcoustics[0].ReceiverWallIndexes;
            var pId = buildingAcoustics[0].PartitionWallIndexes;
            var revSourceFreqComplex = FftHelper.Fft(buildingAcoustics[0].sourceRoom.reverberation, true, FilterResolution);
            var revReceiverFreqComplex =
                FftHelper.Fft(buildingAcoustics[0].receiverRoom.reverberation, true, FilterResolution);
            var revAll = ComplexFloats.Multiply(revSourceFreqComplex, revReceiverFreqComplex);

            if (DataLogger.DebugData)
            {
                DataLogger.Log(LogName + "NewApproach.buildingAcoustics.sourceWallIndexes", sId);
                DataLogger.Log(LogName + "NewApproach.buildingAcoustics.partitionWallIndexes", pId);
                DataLogger.Log(LogName + "NewApproach.buildingAcoustics.receiverWallIndexes", rId);
                DataLogger.Log(LogName + "NewApproach.areaSd", areaSd);
                DataLogger.Log(LogName + "NewApproach.rhoC0", rhoC0);
                DataLogger.Log(LogName + "NewApproach.revSourceFreqComplex", revSourceFreqComplex);
                DataLogger.Log(LogName + "NewApproach.revReceiverFreqComplex", revReceiverFreqComplex);
                DataLogger.Log(LogName + "NewApproach.As", equivalentAbsAreaSource);
                DataLogger.Log(LogName + "NewApproach.Ar", equivalentAbsAreaReceiver);
            }

            for (var i = 0; i < 13; i++)
            {
                var pathId = (i - 1) / 4;
                var iJun = (i - 1) % 4;
                var sIdGlobal = 0;
                var metricId = 0;
                TransferFunction tau3RdOctaveFreq;
                if (i == 0)
                {
                    tau3RdOctaveFreq = 10 ^ (buildingAcoustics[0].partition.ReductionIndex.ReductionIndex / -10f);
                    sIdGlobal = 0;
                    iJun = 0;
                    metricId = 0;
                }
                else
                {
                    var reductionIndexFlanking = buildingAcoustics[0].GetReductionIndexFlanking();
                    switch (pathId)
                    {
                        case 0:
                            tau3RdOctaveFreq =
                                10 ^ (reductionIndexFlanking[iJun][sId[iJun], rId[iJun]] / -10f);
                            sIdGlobal = iJun + 1;
                            metricId = 2 * iJun + 0;
                            break;
                        case 1:
                            tau3RdOctaveFreq =
                                10 ^ (reductionIndexFlanking[iJun][sId[iJun], pId[iJun]] / -10f);
                            sIdGlobal = iJun + 1;
                            metricId = iJun + 1;
                            break;
                        case 2:
                            tau3RdOctaveFreq =
                                10 ^ (reductionIndexFlanking[iJun][pId[iJun], rId[iJun]] / -10f);
                            sIdGlobal = 0;

                            metricId = 2 * iJun + 1;
                            break;
                        default:
                            tau3RdOctaveFreq =
                                10 ^ (reductionIndexFlanking[iJun][sId[iJun], rId[iJun]] / -10f);
                            Log.Error("Cannot find Reduction Index!");
                            break;
                    }
                }

                DataLogger.Log(LogName + "NewApproach.pathId(:," + (i + 1) + ")", pathId);
                DataLogger.Log(LogName + "NewApproach.iJun(:," + (i + 1) + ")", iJun);
                DataLogger.Log(LogName + "NewApproach.sIdGlobal(:," + (i + 1) + ")", sIdGlobal);

                DataLogger.Log(LogName + "NewApproach.SourceSoundPowerNewApproach", sourceSoundPowerNewApproach);
                DataLogger.Log(LogName + "NewApproach.tau3rdOctaveFreq(:," + (i + 1) + ")", tau3RdOctaveFreq);
                var tauInterpolatedFreq = CalcTauExtrapolatedFrom3RdOctaveTau(tau3RdOctaveFreq, FilterResolution);
                var tau1PhaseFreqComplex = CalcSqrtTauComplexFromTauInterpolatedEven(tauInterpolatedFreq.Tf, LogName);
                //tau0PhaseFreqComplex = tau1PhaseFreqComplex;
                var preFactor = Mathf.Sqrt(400 * 400 * sourceSoundPowerNewApproach * areaSd / (areaSi[sIdGlobal] * rhoC0));

                DataLogger.Log(LogName + "NewApproach.tauInterpolatedFreq(:," + (i + 1) + ")", tauInterpolatedFreq);
                DataLogger.Log(LogName + "NewApproach.tauFreqComplex(:," + (i + 1) + ")", tau1PhaseFreqComplex);
                DataLogger.Log(
                    LogName + "NewApproach.tauIdJunIdPath3rdOctaveFreq{" + (iJun + 1) + "," + (pathId + 1) + "}",
                    tau3RdOctaveFreq);
                DataLogger.Log(
                    LogName + "NewApproach.tauIdJunIdPathFreqComplex{" + (iJun + 1) + "," + (pathId + 1) + "}",
                    tau1PhaseFreqComplex);
                DataLogger.Log(LogName + "NewApproach.preFactor(:," + (i + 1) + ")", preFactor);

                var factorDirDir = preFactor / (4 * Mathf.PI);
                var factorDirRev = preFactor * Mathf.Sqrt(1 / (Mathf.PI * equivalentAbsAreaReceiver));
                var factorRevDir = preFactor * Mathf.Sqrt(areaSi[sIdGlobal] / (4 * Mathf.PI * equivalentAbsAreaSource));
                var factorRevRev =
                    preFactor * Mathf.Sqrt(4 * areaSi[sIdGlobal] / (equivalentAbsAreaSource * equivalentAbsAreaReceiver));
                if (i == 0 || pathId == 1)
                {
                    // Calculate Direct-Direct Pre Filter
                    _dirDirFreqPartition[metricId] = ComplexFloats.Multiply(tau1PhaseFreqComplex, factorDirDir);

                    // Calculate Direct-Rev Pre Filter
                    _dirRevFreqPartition[metricId] =
                        ComplexFloats.Multiply(revReceiverFreqComplex, tau1PhaseFreqComplex, factorDirRev);

                    // Calculate Rev-Direct Pre Filter
                    _revDirFreqPartition[metricId] =
                        ComplexFloats.Multiply(revSourceFreqComplex, tau1PhaseFreqComplex, factorRevDir);

                    DataLogger.Log(LogName + "NewApproach.factorPartitionDirDir(:," + (metricId + 1) + ")", factorDirDir);
                    DataLogger.Log(LogName + "NewApproach.factorPartitionDirRev(:," + (metricId + 1) + ")", factorDirRev);
                    DataLogger.Log(LogName + "NewApproach.factorPartitionRevDir(:," + (metricId + 1) + ")", factorRevDir);
                    DataLogger.Log(LogName + "NewApproach.factorPartitionRevRev(:," + (metricId + 1) + ")", factorRevRev);
                }
                else
                {
                    // Calculate Direct-Direct Pre Filter
                    _dirDirFreqFlanking[metricId] = ComplexFloats.Multiply(tau1PhaseFreqComplex, factorDirDir);

                    // Calculate Direct-Rev Pre Filter
                    _dirRevFreqFlanking[metricId] =
                        ComplexFloats.Multiply(revReceiverFreqComplex, tau1PhaseFreqComplex, factorDirRev);

                    // Calculate Rev-Direct Pre Filter
                    _revDirFreqFlanking[metricId] =
                        ComplexFloats.Multiply(revSourceFreqComplex, tau1PhaseFreqComplex, factorRevDir);

                    DataLogger.Log(LogName + "NewApproach.factorFlankingDirDir(:," + (metricId + 1) + ")", factorDirDir);
                    DataLogger.Log(LogName + "NewApproach.factorFlankingDirRev(:," + (metricId + 1) + ")", factorDirRev);
                    DataLogger.Log(LogName + "NewApproach.factorFlankingRevDir(:," + (metricId + 1) + ")", factorRevDir);
                    DataLogger.Log(LogName + "NewApproach.factorFlankingRevRev(:," + (metricId + 1) + ")", factorRevRev);
                }

                // Calculate Rev-Rev Pre Filter
                var revRevFreqTemp = ComplexFloats.Multiply(revAll, tau1PhaseFreqComplex, factorRevRev);
                if (revRevFreqTemp.Length != _revRevNewApproachFreq.Length)
                    Log.Error("Length of irs are not matching!");
                for (var n = 0; n < revRevFreqTemp.Length; n++) _revRevNewApproachFreq[n] += revRevFreqTemp[n];
            }

            if (DataLogger.DebugData)
            {
                DataLogger.Log(LogName + "NewApproach.revRevNewApproachFreqComplex", _revRevNewApproachFreq);
                DataLogger.Log(LogName + "NewApproach.dirDirFlankingFreqComplex", _dirDirFreqFlanking);
                DataLogger.Log(LogName + "NewApproach.dirRevFlankingFreqComplex", _dirRevFreqFlanking);
                DataLogger.Log(LogName + "NewApproach.revDirFlankingFreqComplex", _revDirFreqFlanking);
                DataLogger.Log(LogName + "NewApproach.dirDirPartitionFreqComplex", _dirDirFreqPartition);
                DataLogger.Log(LogName + "NewApproach.dirRevPartitionFreqComplex", _dirRevFreqPartition);
                DataLogger.Log(LogName + "NewApproach.revDirPartitionFreqComplex", _revDirFreqPartition);
            }

            _realTimeSource = SoundSourceObjects[0]
                .GetSourceFiForSs(buildingAcoustics[0].sourceWalls, FilterResolution, true);
            _realTimeReceiver = SoundReceiverObject
                .GetHrtfForSs(FilterResolution, buildingAcoustics[0].ReceiverPositionsSs, true);
        }


        /// <summary>
        /// Implementation of Realtime Update, called if Source or Receiver Updates
        /// </summary>
        /// <param name="logId">write log file if logId > 1</param>
        protected override void RealTimeCalculation(int logId = 0)
        {
            var sw = new Stopwatch();
            sw.Start();
            var sumAllIrLeftFreqComplex = ComplexFloats.CopyTo(_revRevNewApproachFreq);
            var sumAllIrRightFreqComplex = ComplexFloats.CopyTo(_revRevNewApproachFreq);

            var ifDataLog = DataLogger.DebugData;

            var ssBinComplex = new float[sumAllIrLeftFreqComplex.Length];
            var ssMonoComplex = new float[sumAllIrLeftFreqComplex.Length];
            // Start Partition Wall
            for (var n = 0; n < 2 * FilterResolution; n += 2)
            {
                float ssBinReal = 0f, ssBinImag = 0f, ssMonoReal = 0f, ssMonoImag = 0f;

                for (var i = 0; i < 5; i++)
                {
                    ssBinReal += _dirDirFreqPartition[i][n] * _realTimeSource[i][n] -
                                 _dirDirFreqPartition[i][n + 1] * _realTimeSource[i][n + 1] +
                                 _revDirFreqPartition[i][n];

                    ssBinImag += _dirDirFreqPartition[i][n + 1] * _realTimeSource[i][n] +
                                 _dirDirFreqPartition[i][n] * _realTimeSource[i][n + 1] +
                                 _revDirFreqPartition[i][n + 1];

                    ssMonoReal += _dirRevFreqPartition[i][n] * _realTimeSource[i][n] -
                                  _dirRevFreqPartition[i][n + 1] * _realTimeSource[i][n + 1];

                    ssMonoImag += _dirRevFreqPartition[i][n + 1] * _realTimeSource[i][n] +
                                  _dirRevFreqPartition[i][n] * _realTimeSource[i][n + 1];
                }


                if (ifDataLog)
                {
                    ssBinComplex[n] = ssBinReal;
                    ssBinComplex[n + 1] = ssBinImag;
                    ssMonoComplex[n] = ssMonoReal;
                    ssMonoComplex[n + 1] = ssMonoImag;
                }

                sumAllIrLeftFreqComplex[n] += ssBinReal * _realTimeReceiver[0][n] -
                    ssBinImag * _realTimeReceiver[0][n + 1] + ssMonoReal;
                sumAllIrLeftFreqComplex[n + 1] += ssBinReal * _realTimeReceiver[0][n + 1] +
                                                  ssBinImag * _realTimeReceiver[0][n] + ssMonoImag;

                sumAllIrRightFreqComplex[n] += ssBinReal * _realTimeReceiver[1][n] -
                    ssBinImag * _realTimeReceiver[1][n + 1] + ssMonoReal;
                sumAllIrRightFreqComplex[n + 1] += ssBinReal * _realTimeReceiver[1][n + 1] +
                                                   ssBinImag * _realTimeReceiver[1][n] + ssMonoImag;
            }

            if (LogRealtime)
            {
                DataLogger.Log(
                    LogName + "NewApproach.ssMonoComplex{1," + (HrirCounter + 1) + "}(:," + 1 + ")",
                    ssMonoComplex);
                DataLogger.Log(
                    LogName + "NewApproach.ssBinComplex{1," + (HrirCounter + 1) + "}(:," + 1 + ")",
                    ssBinComplex);
                DataLogger.Log(
                    LogName + "NewApproach.realTimeReceiverNewApproachFreqComplex{1," + (HrirCounter + 1) + "}",
                    _realTimeReceiver);
                DataLogger.Log(
                    LogName + "NewApproach.realTimeSourceNewApproachFreqComplex{1," + (HrirCounter + 1) + "}",
                    _realTimeSource);
                DataLogger.Log(LogName + "NewApproach.IrSumLeftFreqComplex{1," + (HrirCounter + 1) + "}(:,1)",
                    sumAllIrLeftFreqComplex);
                DataLogger.Log(LogName + "NewApproach.IrSumRightFreqComplex{1," + (HrirCounter + 1) + "}(:,1)",
                    sumAllIrRightFreqComplex);
            }

            var rtSourcePartition = _realTimeSource[0];
            // Start Flanking Wall 
            for (var i = 0; i < 4; i++)
            {
                var dirDirFf = _dirDirFreqFlanking[2 * i];
                var dirDirDf = _dirDirFreqFlanking[2 * i + 1];
                var rtSourceFlanking = _realTimeSource[i + 1];
                var revDirFf = _revDirFreqFlanking[2 * i];
                var revDirFd = _revDirFreqFlanking[2 * i + 1];
                var dirRevFf = _dirRevFreqFlanking[2 * i];
                var dirRevFd = _dirRevFreqFlanking[2 * i + 1];
                var rtReceiverLeft = _realTimeReceiver[2 * i];
                var rtReceiverRight = _realTimeReceiver[2 * i + 1];

                for (var n = 0; n < sumAllIrLeftFreqComplex.Length; n += 2)
                {
                    var ssBinReal = dirDirFf[n] * rtSourceFlanking[n] - dirDirFf[n + 1] * rtSourceFlanking[n + 1] +
                                    dirDirDf[n] * rtSourcePartition[n] - dirDirDf[n + 1] * rtSourcePartition[n + 1] +
                                    revDirFf[n] + revDirFd[n];
                    var ssBinImag = dirDirFf[n + 1] * rtSourceFlanking[n] + dirDirFf[n] * rtSourceFlanking[n + 1] +
                                    dirDirDf[n + 1] * rtSourcePartition[n] + dirDirDf[n] * rtSourcePartition[n + 1] +
                                    revDirFf[n + 1] + revDirFd[n + 1];

                    var ssMonoReal = dirRevFf[n] * rtSourceFlanking[n] - dirRevFf[n + 1] * rtSourceFlanking[n + 1] +
                        dirRevFd[n] * rtSourcePartition[n] - dirRevFd[n + 1] * rtSourcePartition[n + 1];
                    var ssMonoImag = dirRevFf[n + 1] * rtSourceFlanking[n] + dirRevFf[n] * rtSourceFlanking[n + 1] +
                                     dirRevFd[n + 1] * rtSourcePartition[n] + dirRevFd[n] * rtSourcePartition[n + 1];

                    if (ifDataLog)
                    {
                        ssBinComplex[n] = ssBinReal;
                        ssBinComplex[n + 1] = ssBinImag;
                        ssMonoComplex[n] = ssMonoReal;
                        ssMonoComplex[n + 1] = ssMonoImag;
                    }

                    sumAllIrLeftFreqComplex[n] +=
                        ssBinReal * rtReceiverLeft[n] - ssBinImag * rtReceiverLeft[n + 1] + ssMonoReal;
                    sumAllIrLeftFreqComplex[n + 1] +=
                        ssBinReal * rtReceiverLeft[n + 1] + ssBinImag * rtReceiverLeft[n] + ssMonoImag;

                    sumAllIrRightFreqComplex[n] +=
                        ssBinReal * rtReceiverRight[n] - ssBinImag * rtReceiverRight[n + 1] + ssMonoReal;
                    sumAllIrRightFreqComplex[n + 1] +=
                        ssBinReal * rtReceiverRight[n + 1] + ssBinImag * rtReceiverRight[n] + ssMonoImag;
                }

                if (LogRealtime)
                {
                    DataLogger.Log(
                        LogName + "NewApproach.ssMonoComplex{1," + (HrirCounter + 1) + "}(:," + (i + 2) + ")",
                        ssMonoComplex);
                    DataLogger.Log(
                        LogName + "NewApproach.ssBinComplex{1," + (HrirCounter + 1) + "}(:," + (i + 2) + ")",
                        ssBinComplex);
                    DataLogger.Log(
                        LogName + "NewApproach.IrSumLeftFreqComplex{1," + (HrirCounter + 1) + "}(:," + (i + 2) + ")",
                        sumAllIrLeftFreqComplex);
                    DataLogger.Log(
                        LogName + "NewApproach.IrSumRightFreqComplex{1," + (HrirCounter + 1) + "}(:," + (i + 2) + ")",
                        sumAllIrRightFreqComplex);
                }
            }

            var irTimeLeft = FftHelper.Ifft(sumAllIrLeftFreqComplex);
            var irTimeRight = FftHelper.Ifft(sumAllIrRightFreqComplex);

            if (audioRenderer != null)
                audioRenderer.UpdateFilter(irTimeLeft, irTimeRight);

            if (LogRealtime)
            {
                DataLogger.Log(LogName + "NewApproach.IrTimeLeft{1," + (HrirCounter + 1) + "}", irTimeLeft);
                DataLogger.Log(LogName + "NewApproach.IrTimeRight{1," + (HrirCounter + 1) + "}", irTimeRight);
            }

            sw.Stop();
            FilterUpdate = sw.Elapsed.TotalMilliseconds;
        }


        /// <summary>
        /// Implementation of Update Receiver Data for filter Construction if Receiver Update
        /// </summary>
        /// <returns>Receiver Update</returns>
        protected override bool RealTimeReceiverUpdate()
        {
            var sw = new Stopwatch();
            sw.Start();
            var posUpdate = false;
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
                if (SoundReceiverObject.SoundObjectChanged() | ForceReceiverUpdate)
                {
                    _realTimeReceiver = SoundReceiverObject
                        .GetHrtfForSs(FilterResolution, buildingAcoustics[0].ReceiverPositionsSs);
                    posUpdate = true;
                    ForceReceiverUpdate = false;
                }

            sw.Stop();
            if (sw.ElapsedMilliseconds > 0)
                ReceiverUpdate = sw.Elapsed.TotalMilliseconds;
            return posUpdate;
        }

        /// <summary>
        /// Implementation of Update Source Data for filter Construction if Source Update
        /// </summary>
        /// <returns>Source Update</returns>
        protected override bool RealTimeSourceUpdate()
        {
            var sw = new Stopwatch();
            sw.Start();
            var posUpdate = false;
            for (var iBa = 0; iBa < buildingAcoustics.Length; iBa++)
                if (SoundSourceObjects[0].SoundObjectChanged() | ForceSourceUpdate)
                    _realTimeSource = SoundSourceObjects[0]
                        .GetSourceFiForSs(buildingAcoustics[0].sourceWalls, FilterResolution);

            ForceSourceUpdate = false;
            sw.Stop();
            if (sw.ElapsedMilliseconds > 0)
                SourceUpdate = sw.Elapsed.TotalMilliseconds;
            return posUpdate;
        }

        #region Tests

        /// <summary>
        /// Run New Approach Indoor and Log results separably for each part of ir (dd, dr, rd, rr)
        /// </summary>
        public void TestCalcNewApproachWithLogging()
        {
            OfflineCalculation(null);
            LogRealtime = true;
            HrirCounter = 0;

            var dirDirFlanking = ComplexFloats.CopyTo(_dirDirFreqFlanking);
            var dirRevFlanking = ComplexFloats.CopyTo(_dirRevFreqFlanking);
            var revDirFlanking = ComplexFloats.CopyTo(_revDirFreqFlanking);
            var dirDirPartition = ComplexFloats.CopyTo(_dirDirFreqPartition);
            var dirRevPartition = ComplexFloats.CopyTo(_dirRevFreqPartition);
            var revDirPartition = ComplexFloats.CopyTo(_revDirFreqPartition);
            var revRev = ComplexFloats.CopyTo(_revRevNewApproachFreq);

            // All Paths
            RealTimeCalculation();
            HrirCounter++;

            // Just DirDir
            SetAllNewApproachOfflineArraysZero();
            _dirDirFreqFlanking = ComplexFloats.CopyTo(dirDirFlanking);
            _dirDirFreqPartition = ComplexFloats.CopyTo(dirDirPartition);
            RealTimeCalculation();
            HrirCounter++;

            // Just DirRev
            SetAllNewApproachOfflineArraysZero();
            _dirRevFreqFlanking = ComplexFloats.CopyTo(dirRevFlanking);
            _dirRevFreqPartition = ComplexFloats.CopyTo(dirRevPartition);
            RealTimeCalculation();
            HrirCounter++;

            // Just RevDir
            SetAllNewApproachOfflineArraysZero();
            _revDirFreqFlanking = ComplexFloats.CopyTo(revDirFlanking);
            _revDirFreqPartition = ComplexFloats.CopyTo(revDirPartition);
            RealTimeCalculation();
            HrirCounter++;

            // Just RevRev
            SetAllNewApproachOfflineArraysZero();
            _revRevNewApproachFreq = ComplexFloats.CopyTo(revRev);
            RealTimeCalculation();
            HrirCounter++;

            var timePrefix = DateTime.Now.ToString("-yyyyMMdd-HHmmss-");
            timePrefix = "TestCalcNewApproachWithLogging" + timePrefix;

            MoveLogFiles(timePrefix);
        }


        private void SetAllNewApproachOfflineArraysZero()
        {
            for (var i = 0; i < _revDirFreqFlanking.Length; i++)
            {
                _dirDirFreqFlanking[i] = new float[_dirDirFreqFlanking[i].Length];
                _dirRevFreqFlanking[i] = new float[_dirRevFreqFlanking[i].Length];
                _revDirFreqFlanking[i] = new float[_revDirFreqFlanking[i].Length];
            }

            for (var i = 0; i < _revDirFreqPartition.Length; i++)
            {
                _dirDirFreqPartition[i] = new float[_dirDirFreqPartition[i].Length];
                _dirRevFreqPartition[i] = new float[_dirRevFreqPartition[i].Length];
                _revDirFreqPartition[i] = new float[_revDirFreqPartition[i].Length];
            }

            _revRevNewApproachFreq = new float[_revRevNewApproachFreq.Length];
        }

        #endregion
    }
}