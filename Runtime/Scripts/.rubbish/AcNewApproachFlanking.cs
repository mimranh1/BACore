using UnityEngine;
using UnityEngine.Serialization;

namespace BA.BACore
{
    /// <summary>
    /// Base class for the New Approaches considering Direct and reverberation in Source and Receiver room
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    [System.Serializable]
    public abstract class AcNewApproachFlanking : AuralisationCalculation
    {
        /// <summary>
        /// sound Power of the Source in Watts
        /// </summary>
        [Header("New Approach Settings")] [Rename("Source Power (W)")]
        public float sourceSoundPowerNewApproach = 0.1f;
        

        protected BuildingAcousticFlankingEnvironment[] buildingAcoustics;


        public override SourceReceiverEnvironment[] Environments => buildingAcoustics;
    }
}