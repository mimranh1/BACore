﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class RayDetectorWalls : RayDetector
    {
        public override RayDetectorType Type => RayDetectorType.Walls;
        
        public override FloatArrays[] ImpulseResponse => impulseResponse;
        [SerializeField][HideInInspector] private FloatArrays[] impulseResponse=null;

        public WallBehaviour[] wallBehaviourBuildings;
        public int distanceSamples;
        [SerializeField][HideInInspector] private GameObjectDictionary dict = new GameObjectDictionary();

        private static int ImpulseResponseLength => BaSettings.Instance.filterLengthInSamples;
      
        public override void DeleteRays()
        {
            //if (impulseResponse == null || impulseResponse.Length != ImpulseResponseLength)
                Init();

                foreach (var ir in impulseResponse)
                {
                    ir.Clear();
                }
        }

        public override void Init()
        {
            dict = new GameObjectDictionary();
            impulseResponse=new FloatArrays[wallBehaviourBuildings.Length];
            for (var i = 0; i < impulseResponse.Length; i++)
            {
                dict.Add(wallBehaviourBuildings[i].gameObject, i);
                impulseResponse[i] = new FloatArrays(ImpulseResponseLength);
            }
        }

        public override void RegisterRay(RayInfo ray, RaycastHit hit)
        {
            var index = dict[hit.transform.gameObject];
            var distance = ray.distanceTravelled + hit.distance;
            distanceSamples = (int)(distance * samplesPerMeter);
            var factor = ray.WallFactor / distance;
            var floatArrayses = impulseResponse[index].floats;
            if (floatArrayses.Length > distanceSamples)
                floatArrayses[distanceSamples] = factor;
        }
    }
}