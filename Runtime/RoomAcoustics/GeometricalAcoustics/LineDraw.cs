﻿using UnityEngine;

namespace BA.BACore
{
    public struct LineDraw
    {
        public Vector3 StartPoint;
        public Vector3 EndPoint;
        public int Order;

        public LineDraw(Ray ray, float distance, int order)
        {
            Order = order;
            StartPoint = ray.origin;
            EndPoint = ray.origin + (ray.direction * distance);
        }
    }
}