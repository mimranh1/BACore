﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class RayInfo
    {
        public Ray ray;
        public int order;
        public float distanceTravelled;
        public float WallFactor;
        public float lastDistance;

        public RayInfo(Vector3 origin, Vector3 endPosition, int order)
        {
            ray = new Ray(origin, endPosition-origin);
            this.order = order;
            distanceTravelled = 0;
            lastDistance = 0;
            WallFactor = 1;
        }
        
        public RayInfo(Ray ray, int order)
        {
            this.ray = ray;
            this.order = order;
            lastDistance = 0;
            distanceTravelled = 0;
            WallFactor = 1;
        }
        
        public RayInfo(RayInfo ray, Vector3 hitPoint, Vector3 normal, float distance,float alpha)
        {
            this.ray = new Ray(hitPoint, Vector3.Reflect(ray.ray.direction,normal));
            this.order = ray.order - 1;
            distanceTravelled = ray.distanceTravelled+distance;
            lastDistance = distance;
            WallFactor = ray.WallFactor * alpha;
        }
    }
}