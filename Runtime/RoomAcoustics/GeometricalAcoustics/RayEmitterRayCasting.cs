﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BA.BACore
{
    public class RayEmitterRayCasting : RayEmitter
    {
        public override RayEmitterType Type => RayEmitterType.RayCasting;

        public override List<RayInfo> Directions => directions;
        [SerializeField] private List<RayInfo> directions;

        public int numberOfRays = 1000;
        public int maxOrder = 20;
        public bool drawDirections;
        public override void Init(RoomAcousticsBehaviour roomAcousticsBehaviour)
        {
            directions = PointsOnSphere(numberOfRays);
        }

        public override bool OnReceiverUpdate()
        {
            return false;
        }

        public override bool OnSourceUpdate()
        {
            directions = PointsOnSphere(numberOfRays);
            return true;
        }
        
        
        
        private List<RayInfo> PointsOnSphere(int n)
        {
            var origin = source.position;
            var upts = new List<RayInfo>();
            var inc = Mathf.PI * (3 - Mathf.Sqrt(5));
            var off = 2.0f / n;
            float x = 0;
            float y = 0;
            float z = 0;
            float r = 0;
            float phi = 0;
           
            for (var k = 0; k < n; k++){
                y = k * off - 1 + (off /2);
                r = Mathf.Sqrt(1 - y * y);
                phi = k * inc;
                x = Mathf.Cos(phi) * r;
                z = Mathf.Sin(phi) * r;

                var direction = new Vector3(x, y, z);
                    upts.Add(new RayInfo(new Ray(origin, direction), maxOrder));
                    if (drawDirections)
                        Debug.DrawRay(origin, direction, Color.yellow, 10f);
            }
            return upts;
        }
    }
}