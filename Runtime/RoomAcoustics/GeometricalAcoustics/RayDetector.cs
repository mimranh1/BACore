﻿using UnityEngine;

namespace BA.BACore
{
    public abstract class RayDetector:ExtendedScriptableObject<RayDetector, RayDetectorType> 
    {
        public LayerMask layerMaskReceiver;
        public abstract FloatArrays[] ImpulseResponse { get; }
        public abstract void DeleteRays();
        public abstract void Init();
        public abstract void RegisterRay(RayInfo ray, RaycastHit hit);
        
        public float samplesPerMeter => _samplesPerMeter < 0 ? SetSamplesPerMeter() : _samplesPerMeter;
        private float _samplesPerMeter = -1f;

        private float SetSamplesPerMeter()
        {
            _samplesPerMeter = BaSettings.Instance.SampleRate / BaSettings.Instance.C0;
            return _samplesPerMeter;
        }

    }
}