﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class RayDetectorWall : RayDetector
    {
        public override RayDetectorType Type => RayDetectorType.Wall;
        
        public override FloatArrays[] ImpulseResponse => new []{new FloatArrays(impulseResponse)};
        [SerializeField][HideInInspector] private float[] impulseResponse=null;

        public WallBehaviour wallBehaviourBuilding;
        public int distanceSamples;

        private static int ImpulseResponseLength => BaSettings.Instance.filterLengthInSamples;
      
        public override void DeleteRays()
        {
            Array.Clear(impulseResponse,0,impulseResponse.Length);
        }

        public override void Init()
        {
            if (impulseResponse == null || impulseResponse.Length != ImpulseResponseLength)
                impulseResponse = new float[ImpulseResponseLength];
        }

        public override void RegisterRay(RayInfo ray, RaycastHit hit)
        {
            var distance = ray.distanceTravelled + hit.distance;
            distanceSamples = (int)(distance * samplesPerMeter);
            var factor = ray.WallFactor / distance;
            if (impulseResponse.Length > distanceSamples)
                impulseResponse[distanceSamples] = factor;
        }
    }
}