﻿using System.Collections.Generic;
using UnityEngine;

namespace BA.BACore
{
    public class RayEmitterImageSource : RayEmitter
    {
        public override RayEmitterType Type => RayEmitterType.ImageSource;

        [Range(0,5)]
        [SerializeField] private int maxOrder;
        public override List<RayInfo> Directions => directions;      
        [SerializeField] protected Transform[] receivers;


        [SerializeField] [HideInInspector] private List<RayInfo> directions=new List<RayInfo>();
        [SerializeField] [HideInInspector] private ImageSources[] imageSources;
        [SerializeField] [HideInInspector] private RoomAcousticsBehaviour roomAcousticsBehaviour;
        [SerializeField] private bool ignoreDirectSound = false;

        public override void Init(RoomAcousticsBehaviour roomAcousticsBehaviour)
        {
            this.roomAcousticsBehaviour = roomAcousticsBehaviour;
        }

        public override bool OnReceiverUpdate()
        {
            ProcessAll();
            return true;
        }

        public override bool OnSourceUpdate()
        {
            directions.Clear();
            var position = source.position;
            for (var order = 0; order < imageSources.Length; order++)
            {
                var imageSourceList = imageSources[order];
                foreach (var imageSource in imageSourceList.sources)
                {
                    directions.Add(new RayInfo(position,imageSource.Position - position, order));
                }
            }

            return true;
        }
        
        private void ProcessAll()
        {
            directions.Clear();
            foreach (var receiver in receivers)
            {
                var positionReceiver = receiver.position;
                CalcReceiverDirection(positionReceiver);
            }
        }

        private void CalcReceiverDirection(Vector3 positionReceiver)
        {
            imageSources = new ImageSources[maxOrder + 1];
            var positionSource = source.position;

            imageSources[0] = new ImageSources();
            var imageSource0 = new ImageSource
            {
                Position = positionReceiver,
                WallIndex = new List<int>(0),
                LastIndex = -1,
                factor = 1,
            };
            imageSources[0].sources.Add(imageSource0);
            if(!ignoreDirectSound)
            {
                
                directions.Add(new RayInfo(positionSource, positionReceiver, 0));
            }
            var roomGeometryWallBehaviours = roomAcousticsBehaviour.RoomGeometry.WallBehaviours;
            for (var order = 1; order < maxOrder + 1; order++)
            {
                imageSources[order] = new ImageSources();
                for (var index = 0; index < imageSources[order - 1].sources.Count; index++)
                {
                    var currentImage = imageSources[order - 1].sources[index];
                    for (var i = 0; i < roomGeometryWallBehaviours.Length; i++)
                    {
                        var wallBehaviour = roomGeometryWallBehaviours[i];
                        var wallPos = wallBehaviour.Geometry.Position;
                        var wallNormal = wallBehaviour.Geometry.Normal;
                        var image = 2 * Vector3E.MultiplyElementwise(wallPos - currentImage.Position, wallNormal) +
                                    currentImage.Position;
                        var imageSource = new ImageSource
                        {
                            Position = image,
                            WallIndex = new List<int>(),
                            LastIndex = index,
                            factor = currentImage.factor * 0.1f,
                        };
                        imageSource.WallIndex.AddRange(currentImage.WallIndex);
                        imageSource.WallIndex.Add(wallBehaviour.GetInstanceID());
                        imageSource.LastWallIndex = i;
                        imageSources[order].Add(imageSource);
                        directions.Add(new RayInfo(positionSource, image, order));
                    }
                }
            }
        }
    }
}