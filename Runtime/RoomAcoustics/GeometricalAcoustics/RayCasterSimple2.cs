﻿using System.Collections.Generic;
using UnityEngine;

namespace BA.BACore
{
    public class RayCasterSimple2 : RayCaster
    {
        public override RayCasterType Type => RayCasterType.Simple2;
        public LayerMask layerMaskWall;
        public float maxDistance;
        public bool drawRay = false;
        public bool drawAudibleRay = false;
        [Range(0f,1f)]
        public float alpha = 0.2f;

        public bool useHSVColors;
        public bool ignoreDirectSound;
        public override bool DrawingActive => drawRay || drawAudibleRay;

        public override void ProcessRays(List<RayInfo> directions, RayDetector rayDetector,Color color)
        {
            var layerMaskReceiver = rayDetector.layerMaskReceiver;

            foreach (var rayDirection in directions)
            {
                if(!drawAudibleRay)
                    CalcOneRay(rayDetector, rayDirection, layerMaskReceiver);
                else 
                    CalcOneRayDrawReceived(rayDetector, rayDirection, layerMaskReceiver,color);
            }
        }

        private void CalcOneRay(RayDetector rayDetector, RayInfo rayDirection, LayerMask layerMaskReceiver)
        {
            var ray = rayDirection.ray;
            for (int i = 0; i < rayDirection.order + 1; i++)
            {
                //DrawRay(rayDirection, Color.red, 10);
                if (Physics.Raycast(ray, out var hitReceiver, maxDistance, layerMaskReceiver.value))
                {
                    if (Physics.Raycast(ray, out var hitWall, maxDistance, layerMaskWall.value))
                    {
                        if (hitReceiver.distance > hitWall.distance)
                        {
                            // hit wall
                            if (drawRay)
                                Debug.DrawRay(ray.origin, ray.direction * hitWall.distance,
                                    Color.red);
                            rayDirection.lastDistance = hitWall.distance;
                            rayDirection.distanceTravelled += hitWall.distance;
                            rayDirection.WallFactor *= alpha;
                            ray = new Ray(hitWall.point, Vector3.Reflect(ray.direction, hitWall.normal));
                            continue;
                        }
                    }

                    rayDirection.lastDistance = hitReceiver.distance;
                    rayDirection.distanceTravelled += hitReceiver.distance;
                    rayDirection.WallFactor *= alpha;
                    rayDirection.ray = ray;
                    rayDetector.RegisterRay(rayDirection, hitReceiver);
                    if (drawRay)
                        Debug.DrawRay(ray.origin, ray.direction * hitReceiver.distance,
                            Color.green);
                    break;
                }

                if (Physics.Raycast(ray, out var hitWall2, maxDistance, layerMaskWall.value))
                {
                    if (drawRay)
                        Debug.DrawRay(ray.origin, ray.direction.normalized * hitWall2.distance,
                            Color.blue);
                    rayDirection.lastDistance = hitWall2.distance;
                    rayDirection.distanceTravelled += hitWall2.distance;
                    rayDirection.WallFactor *= alpha;
                    ray = new Ray(hitWall2.point, Vector3.Reflect(ray.direction, hitWall2.normal));
                    continue;
                }
                if (drawRay)
                    Debug.DrawRay(ray.origin, ray.direction.normalized,
                        Color.red);
            }
        }

     
        private void CalcOneRayDrawReceived(RayDetector rayDetector, RayInfo rayDirection, LayerMask layerMaskReceiver,Color color)
        {
        
            var ray = rayDirection.ray;
            var rays = new List<LineDraw>();
            for (int i = 0; i < rayDirection.order + 1; i++)
            {
                //DrawRay(rayDirection, Color.red, 10);
                if (Physics.Raycast(ray, out var hitReceiver, maxDistance, layerMaskReceiver.value))
                {
                    if (Physics.Raycast(ray, out var hitWall, maxDistance, layerMaskWall.value))
                    {
                        if (hitReceiver.distance > hitWall.distance)
                        {
                            // hit wall
                            if (drawRay)
                                Debug.DrawRay(ray.origin, ray.direction*hitWall.distance,
                                    Color.red);
                            rayDirection.lastDistance = hitWall.distance;
                            rayDirection.distanceTravelled += hitWall.distance;
                            rayDirection.WallFactor *= alpha;
                            rays.Add(new LineDraw(ray, hitWall.distance,rayDirection.order));
                            ray = new Ray(hitWall.point, Vector3.Reflect(ray.direction, hitWall.normal));
                            continue;
                        }
                    }

                    rayDirection.lastDistance = hitReceiver.distance;
                    rayDirection.distanceTravelled += hitReceiver.distance;
                    rayDirection.WallFactor *= alpha;
                    rayDirection.ray = ray;
                    if(ignoreDirectSound && rays.Count==0)
                        break;
                    rayDetector.RegisterRay(rayDirection, hitReceiver);
                    rays.Add(new LineDraw(ray, hitReceiver.distance,rayDirection.order));
                    if (drawRay)
                        Debug.DrawRay(ray.origin, ray.direction * hitReceiver.distance,
                            Color.green);
                    if (drawAudibleRay)
                    {
                        DrawLines(rays, color);
                    }
                    break;
                }

                if (Physics.Raycast(ray, out var hitWall2, maxDistance, layerMaskWall.value))
                {
                    if (drawRay)
                        Debug.DrawRay(ray.origin, ray.direction * hitWall2.distance,
                            Color.blue);
                    rayDirection.lastDistance = hitWall2.distance;
                    rayDirection.distanceTravelled += hitWall2.distance;
                    rayDirection.WallFactor *= alpha;
                    rays.Add(new LineDraw(ray, hitWall2.distance,rayDirection.order));
                    ray = new Ray(hitWall2.point, Vector3.Reflect(ray.direction, hitWall2.normal));
                }
            }
        }

        public void DrawLines(List<LineDraw> lineDraws, Color color)
        {
            var lineDrawsCount = lineDraws.Count;
            for (var index = 0; index < lineDrawsCount; index++)
            {
                var lineDraw = lineDraws[index];
                if(useHSVColors)
                {
                    Color.RGBToHSV(color, out var h, out var s, out var v);
                    color = Color.HSVToRGB(h, 1f - ((float)index / 3f), v);
                }
                Debug.DrawLine(lineDraw.StartPoint, lineDraw.EndPoint, color);
            }
        }
     
    }
}