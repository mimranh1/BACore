﻿using System;
using UnityEngine;

namespace BA.BACore
{
    public class RayDetectorBinaural : RayDetector
    {
        public override RayDetectorType Type => RayDetectorType.Binaural;
        public override FloatArrays[] ImpulseResponse => new []{new FloatArrays(impulseResponseLeft), new FloatArrays(impulseResponseRight)};
        [SerializeField][HideInInspector] private float[] impulseResponseLeft;
        [SerializeField][HideInInspector] private float[] impulseResponseRight;
        
        public int impulseResponseLength=44100;
        [SerializeField] [HideInInspector] private float[] binauralBuffer;
        [SerializeField] private SoundReceiverObject receiver;


        public override void DeleteRays()
        {
            Array.Clear(impulseResponseLeft, 0, impulseResponseLength);
            Array.Clear(impulseResponseRight, 0, impulseResponseLength);
        }

        public override void Init()
        {
            InitBinauralBuffer();
        }

        public void InitBinauralBuffer()
        {
            receiver.Init();
            if (impulseResponseLeft == null || impulseResponseLeft.Length != impulseResponseLength)
                impulseResponseLeft = new float[impulseResponseLength];
            if (impulseResponseRight == null || impulseResponseRight.Length != impulseResponseLength)
                impulseResponseRight = new float[impulseResponseLength];

            binauralBuffer = new float[receiver.Length];
        }

        public override void RegisterRay(RayInfo ray, RaycastHit hit)
        {
            var distance = ray.distanceTravelled + hit.distance;
            var distanceSamples = (int) (distance * samplesPerMeter);
            var factor = ray.WallFactor / distance;
            receiver.GetHrirDirect(ray.ray.origin, binauralBuffer, 0);
            for (var i = 0; i < binauralBuffer.Length; i++)
            {
                impulseResponseLeft[distanceSamples + i] += binauralBuffer[i] * factor;
            }
            receiver.GetHrirDirect(ray.ray.origin, binauralBuffer, 1);
            for (var i = 0; i < binauralBuffer.Length; i++)
            {
                impulseResponseRight[distanceSamples + i] += binauralBuffer[i] * factor;
            }
        }
    }
}