﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class GameObjectDictionary : SerializableDictionary<GameObject, int> { }
}