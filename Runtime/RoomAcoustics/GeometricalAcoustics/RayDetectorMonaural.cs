﻿using System;
using UnityEngine;

namespace BA.BACore
{
    public class RayDetectorMonaural : RayDetector
    {
        public override RayDetectorType Type => RayDetectorType.Monaural;
        public override FloatArrays[] ImpulseResponse => new []{new FloatArrays(impulseResponse), new FloatArrays(impulseResponse)};
        [SerializeField][HideInInspector] private float[] impulseResponse=null;
        
        public int impulseResponseLength=44100;

      
        public override void DeleteRays()
        {
            
            
            Array.Clear(impulseResponse,0,impulseResponse.Length);
        }

        public override void Init()
        {
            if (impulseResponse == null || impulseResponse.Length != impulseResponseLength)
                impulseResponse = new float[impulseResponseLength];
        }

        public override void RegisterRay(RayInfo ray, RaycastHit hit)
        {
            var distance = ray.distanceTravelled + hit.distance;
            var distanceSamples = (int)(distance * samplesPerMeter);
            var factor = ray.WallFactor / distance;
            if (impulseResponse.Length > distanceSamples)
                impulseResponse[distanceSamples] = factor;
        }
    }
}