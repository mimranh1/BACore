﻿using System.Collections.Generic;
using UnityEngine;

namespace BA.BACore
{
    public abstract class RayEmitter : ExtendedScriptableObject<RayEmitter, RayEmitterType>
    {
        [SerializeField] protected Transform source;
        
        public abstract List<RayInfo> Directions { get; }
        public abstract void Init(RoomAcousticsBehaviour roomAcousticsBehaviour);
        public abstract bool OnReceiverUpdate();
        public abstract bool OnSourceUpdate();
    }
}