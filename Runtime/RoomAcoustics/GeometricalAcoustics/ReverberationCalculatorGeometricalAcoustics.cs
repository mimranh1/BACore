﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class ReverberationCalculatorGeometricalAcoustics : ReverberationCalculator
    {
        public override ReverberationCalculationMethode Type => ReverberationCalculationMethode.GeometricalAcoustics;
        public override FloatArrays[] ReverberationBinaural => rayDetectors[0].@object.ImpulseResponse;

        public override FloatArrays[] ReverberationAll
        {
            get
            {
                var all = new List<FloatArrays>();
                foreach (var rayDetector in rayDetectors)
                {
                    all.AddRange(rayDetector.@object.ImpulseResponse);
                }

                return all.ToArray();
            }
        }

        public RayEmitterSelector[] rayEmitters = new RayEmitterSelector[1];
        public RayCasterSelector rayCaster;
        public List<RayDetectorSelector> rayDetectors = new List<RayDetectorSelector>();


        public override void OnDrawGizmosSelected(RoomAcousticsBehaviour roomAcousticsBehaviour)
        {
            if (rayCaster.@object.DrawingActive)
                CastRays();
        }

        public override void OnValidate()
        {
            for (var index = 0; index < rayEmitters?.Length; index++)
            {
                if (rayEmitters[index] == null)
                    rayEmitters[index] = CreateInstance<RayEmitterSelector>();
            }
            for (var index = 0; index < rayDetectors?.Count; index++)
            {
                if (rayDetectors[index] == null)
                    rayDetectors[index] = CreateInstance<RayDetectorSelector>();
                rayDetectors[index].@object.Init();
            }

            if (rayCaster == null)
                rayCaster = CreateInstance<RayCasterSelector>();
        }

        public override void Init(SoundSourceObject sourceObject, SoundReceiverObject receiverObject)
        {
            
        }

        public override void OnReceiverUpdate()
        {
            for (var index = 0; index < rayEmitters?.Length; index++)
            {
                var newRays = rayEmitters[index].@object.OnReceiverUpdate();
            }
        }

        public override void OnSourceUpdate()
        {
            for (var index = 0; index < rayEmitters?.Length; index++)
            {
                var newRays = rayEmitters[index].@object.OnSourceUpdate();
            }
        }

        public override void OnUpdateFilter(AudioRender audioRender)
        {
            var colors = new Color[2];
            colors[0]=Color.blue;
            colors[1]=Color.yellow;
            rayDetectors[0].@object.DeleteRays();
            for (var index = 0; index < rayEmitters?.Length; index++)
            {
                for (var i = 0; i < rayDetectors.Count; i++)
                {
                    rayCaster.@object.ProcessRays(rayEmitters[index].@object.Directions, rayDetectors[i].@object,
                        colors[index]);
                }
            }

            for (var index = 0; index < rayDetectors?.Count; index++)
            {
                if (!(audioRender is null))
                    audioRender.UpdateFilter(rayDetectors[index].@object.ImpulseResponse[0].floats,
                        rayDetectors[index].@object.ImpulseResponse[1].floats, index);
            }
        }

        public override float[] ProcessAll(RoomAcousticsBehaviour roomAcoustics)
        {
            for (var index = 0; index < rayEmitters?.Length; index++)
            {
               rayEmitters[index].@object.Init(roomAcoustics);
            }

            return CastRays();
        }

        private float[] CastRays()
        {
            for (var index = 0; index < rayDetectors.Count; index++)
            {
                rayDetectors[index].@object.DeleteRays();
            }

            OnSourceUpdate();
            OnReceiverUpdate();
            OnUpdateFilter(null);
            return rayDetectors[0].@object.ImpulseResponse[0].floats;
        }
    }
}