﻿using System.Collections.Generic;
using UnityEngine;

namespace BA.BACore
{
    public class RayCasterSimple : RayCaster
    {
        public override RayCasterType Type => RayCasterType.Simple;
        public LayerMask layerMaskWall;
        public float maxDistance;
        public bool drawRay = false;

        [Range(0f,1f)]
        public float alpha = 0.2f;

        public override bool DrawingActive => drawRay;

        public override void ProcessRays(List<RayInfo> directions, RayDetector rayDetector,Color color)
        {
            var layerMaskReceiver = rayDetector.layerMaskReceiver;
            while (true)
            {
                var reflections = new List<RayInfo>();
                foreach (var rayDirection in directions)
                {
                    //DrawRay(rayDirection, Color.red, 10);
                    if (Physics.Raycast(rayDirection.ray, out var hitReceiver, maxDistance, layerMaskReceiver.value))
                    {
                        if (Physics.Raycast(rayDirection.ray, out var hitWall, maxDistance, layerMaskWall.value))
                        {
                            if (hitReceiver.distance > hitWall.distance)
                            {
                                ProcessWallReflection(rayDirection, hitWall, reflections);
                                continue;
                            }
                        }
                        rayDetector.RegisterRay(rayDirection, hitReceiver);
                        if (drawRay)
                            Debug.DrawRay(rayDirection.ray.origin,rayDirection.ray.direction*hitReceiver.distance, Color.green, 10);
                    }

                    if (Physics.Raycast(rayDirection.ray, out hitReceiver, maxDistance, layerMaskWall.value))
                    {
                        ProcessWallReflection(rayDirection, hitReceiver, reflections);
                    }
                }

                if (reflections.Count == 0)
                    return;
                directions = reflections;
            }
        }

        private void ProcessWallReflection(RayInfo rayDirection, RaycastHit hit, List<RayInfo> reflections)
        {
            if (rayDirection.order - 1 >= 0)
            {
                var rayInfo = new RayInfo(rayDirection, hit.point, hit.normal, hit.distance,alpha);
                reflections.Add(rayInfo);
                if (drawRay)
                    Debug.DrawRay(rayDirection.ray.origin, rayDirection.ray.direction * hit.distance, Color.red, 10);
            }
        }
    }
}