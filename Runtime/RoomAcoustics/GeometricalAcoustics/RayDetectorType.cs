﻿namespace BA.BACore
{
    public enum RayDetectorType
    {
        Monaural,
        Binaural,
        Wall,
        Walls
    }
}