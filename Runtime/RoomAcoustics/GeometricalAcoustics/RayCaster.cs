﻿using System.Collections.Generic;
using UnityEngine;

namespace BA.BACore
{
    public abstract class RayCaster : ExtendedScriptableObject<RayCaster, RayCasterType>
    {
        public abstract void ProcessRays(List<RayInfo> directions, RayDetector rayDetector, Color color);

        public abstract bool DrawingActive { get; }
        protected virtual void DrawRay(RayInfo rayInfo, Color color, float duration)
        {
            if (rayInfo.lastDistance > 0)
                Debug.DrawRay(rayInfo.ray.origin, rayInfo.ray.direction.normalized * rayInfo.lastDistance, color,
                    duration);
            else
                Debug.DrawRay(rayInfo.ray.origin, rayInfo.ray.direction.normalized*100, color, duration);
        }
    }
}