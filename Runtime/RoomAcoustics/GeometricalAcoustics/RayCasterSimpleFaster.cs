﻿using System.Collections.Generic;
using UnityEngine;

namespace BA.BACore
{
    public class RayCasterSimpleFaster : RayCaster
    {

        public override RayCasterType Type => RayCasterType.SimpleFaster;
        [Tooltip("HAllo")] public LayerMask layerMaskWall;
        public float maxDistance;
        public bool drawRay = false;
        [Range(0f, 1f)] public float alpha = 0.2f;
        public override bool DrawingActive => drawRay;

        private bool IsNotNothing(LayerMask layer)
        {
            return layer.value == 0;
        }


        public override void ProcessRays(List<RayInfo> directions, RayDetector rayDetector,Color color)
        {
            var layerMaskReceiver = rayDetector.layerMaskReceiver;
            for (var index = 0; index < directions.Count; index++)
            {
                var rayDirection = directions[index];
                var loopCounter = 0;
                while (true)
                {
                    //DrawRay(rayDirection, Color.red, 10);
                    if (Physics.Raycast(rayDirection.ray, out var hitReceiver, maxDistance,
                        layerMaskReceiver.value))
                    {
                        if (Physics.Raycast(rayDirection.ray, out var hitWall, maxDistance, layerMaskWall.value))
                        {
                            if (hitReceiver.distance > hitWall.distance)
                            {
                                if (ProcessWallReflection(rayDirection, hitWall))
                                    break;
                            }
                        }

                        rayDetector.RegisterRay(rayDirection, hitReceiver);
                        if (drawRay)
                            Debug.DrawRay(rayDirection.ray.origin,
                                rayDirection.ray.direction * hitReceiver.distance,
                                Color.green);
                        break;
                    }

                    if (Physics.Raycast(rayDirection.ray, out hitReceiver, maxDistance, layerMaskWall.value))
                    {
                        if (ProcessWallReflection(rayDirection, hitReceiver))
                            break;
                    }
                    else
                    {
                        break;
                    }

                    loopCounter++;
                    if (loopCounter > 30)
                    {
                        Debug.LogError("Endless Loop", this);
                        break;
                    }
                }
            }
        }

        private bool ProcessWallReflection(RayInfo rayDirection, RaycastHit hit)
        {
            if (rayDirection.order - 1 >= 0)
            {
                rayDirection.ray = new Ray(hit.point, Vector3.Reflect(rayDirection.ray.direction, hit.normal));
                rayDirection.order -= 1;
                rayDirection.distanceTravelled += hit.distance;
                rayDirection.lastDistance = hit.distance;
                rayDirection.WallFactor *= alpha;

                if (drawRay)
                    Debug.DrawRay(rayDirection.ray.origin, rayDirection.ray.direction * hit.distance, Color.red, 10);
                return false;
            }
            else
            {
                return true;

            }
        }
    }
}