﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class AcImageSource : AuralisationCalculation
    {
        public override AuralisationCalculationType Type => AuralisationCalculationType.ImageSource;
        public override SoundSourceObject[] SoundSourceObjects => new[] {sourceObject};
        [SerializeField] private SoundSourceObject sourceObject;

        public override SoundReceiverObject SoundReceiverObject => receiverObject;
        [SerializeField] private  SoundReceiverObject receiverObject;

        [SerializeField] private RoomAcousticsBehaviour roomAcousticsBehaviour;
        private ReverberationCalculator _reverberationCalculator;

        public override void OnValidate()
        {
            
        }

        protected override void Init()
        {
            _reverberationCalculator = roomAcousticsBehaviour.ReverberationCalculator;
            _reverberationCalculator.Init(sourceObject, receiverObject);
            _reverberationCalculator.ProcessAll(roomAcousticsBehaviour);

        }

        protected override void OfflineCalculation()
        {
        }

        protected override void RealTimeCalculation(AudioRender audioRender, bool log = false)
        {
            _reverberationCalculator.OnUpdateFilter(audioRender);
            audioRender.UpdateFilter(_reverberationCalculator.ReverberationBinaural[0].floats,
                _reverberationCalculator.ReverberationBinaural[1].floats);
        }

        protected override bool RealTimeReceiverUpdate()
        {
            var soundObjectChanged = receiverObject.SoundObjectChanged();
            if (soundObjectChanged)
                _reverberationCalculator.OnReceiverUpdate();
            return soundObjectChanged;
        }

        protected override bool RealTimeSourceUpdate()
        {
            var soundObjectChanged = sourceObject.SoundObjectChanged();
            if (soundObjectChanged)
                _reverberationCalculator.OnSourceUpdate();
            return soundObjectChanged;
        }

        public override void SaveImpulseResponse(string name="")
        {
            var fullPath = $"{BaSettings.Instance.DataPath}/AcImageSource";
            Directory.CreateDirectory(fullPath);
            //receiverRoom.Save(fullPath);
            // File.WriteAllText($"{fullPath}/AcClassicalIndoor.json", JsonUtility.ToJson(this));


            // var setting = new JsonSerializerSettings
            // {
            //     Formatting = Formatting.Indented, 
            //     ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            // };
            //
            // // write
            // var json = JsonConvert.SerializeObject(_reverberationCalculator, setting);
            // var path = Path.Combine(fullPath, "hi.json");
            // File.WriteAllText(path, json);
            // Debug.Log($"FilterData is Logged to {path}");

        }
    }
}