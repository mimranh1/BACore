using System;
using System.Globalization;
using System.IO;
using System.Net.Http.Headers;
using UnityEditor;
using UnityEngine;

namespace BA.BACore
{
    [CreateAssetMenu(fileName = "acoustic Material", menuName = "VBA/Material", order = 0)]
    public class AcousticMaterialData : ScriptableObject
    {
        public string name;
        public string notes;
        public float[] absorptionThridOctave;
        public float[] scatteringThridOctave;
        public float[] absorptionOctave;
        public float[] scatteringOctave;
        public bool[] interpolation;

        /// <summary>
        /// Write the assets to Material Database based on ISO
        /// </summary>
        public static void CreateAssets()
        {
            var folder = BaSettings.Instance.RootPath;
            var files = Directory.GetFiles(folder, "AcousticData.csv", SearchOption.AllDirectories);
            if (files.Length != 1)
                Debug.LogError("File AcousticData.csv not found.");
            var dataFile = files[0];
            var dataPath = Path.GetDirectoryName(dataFile);
            var fullPath = new Uri(dataPath, UriKind.Absolute);
            var relRoot = new Uri($"{Application.dataPath}/../", UriKind.Absolute);
            var savePath = relRoot.MakeRelativeUri(fullPath).ToString() + "/AcousticMaterial/";
            Directory.CreateDirectory(savePath);
            using (var reader = new StreamReader(dataFile))
            {
                reader.ReadLine();
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    var name = values[0];
                    var assetPath = savePath + name + ".asset";
                    if (File.Exists(assetPath))
                    {
                        Debug.Log(name + " already exists.");
                        continue;
                    }

                    var asset = CreateInstance<AcousticMaterialData>();
                    asset.name = name;
                    asset.notes = values[1];
                    var alpha = new float[31];
                    var s = new float[31];
                    for (var i = 0; i < 31; i++)
                        alpha[i] = float.Parse(values[i+2].Replace(",", "."),new CultureInfo("en-US"));
                    
                    line = reader.ReadLine();
                    values = line.Split(';');
                    
                    for (var i = 0; i < 31; i++)
                        s[i] = float.Parse(values[i+2].Replace(",", "."),new CultureInfo("en-US"));

                    var assetPathAndName =
                        AssetDatabase.GenerateUniqueAssetPath(assetPath);
                    AssetDatabase.CreateAsset(asset, assetPathAndName);
                    asset.absorptionThridOctave = alpha;
                    asset.scatteringThridOctave = s;
                    //20|25|31.5|40|50|63|80|100|125|160|200|250|315|400|500|630|800|1000|1250|1600|2000|2500|3150|4000|5000|6300|8000|10000|12500|16000|20000
                    var octaveIndicies = new[] { 2};
                    var alphaShort = new float[10];
                    var scatteringShort = new float[10];

                    for (var i = 0; i < 10; i++)
                        alphaShort[i] = alpha[3 * i + 2];
                    for (var i = 0; i < 10; i++)
                        scatteringShort[i] = s[3 * i + 2];

                    asset.absorptionOctave = alphaShort;
                    asset.scatteringOctave = scatteringShort;
                    line = reader.ReadLine();

                    values = line.Split(';');
                    var interpol = new bool[31];
                    for (var i = 0; i < 31; i++)
                        interpol[i] = values[i+2]=="1";

                    asset.interpolation = interpol;
                    AssetDatabase.SaveAssets();
                    AssetDatabase.Refresh();
                    EditorUtility.FocusProjectWindow();
                    Selection.activeObject = asset;
                }
            }
        }
        
        public static void CreateAssets1()
        {
            var folder = $"{Application.dataPath}/../";
            var files = Directory.GetFiles(folder, "AcousticData.csv", SearchOption.AllDirectories);
            if (files.Length != 1)
                Debug.LogError("File AcousticData.csv not found.");
            var dataFile = files[0];
            var dataPath = Path.GetDirectoryName(dataFile);
            foreach (var file in files)
            {
                var fullPath = new Uri(file, UriKind.Absolute);
                var relRoot = new Uri($"{Application.dataPath}/../", UriKind.Absolute);
                var relPathAsset = relRoot.MakeRelativeUri(fullPath).ToString();
                var relRootDaff = new Uri($"{BaSettings.Instance.RootPath}/", UriKind.Absolute);
                var relPathDaff = relRootDaff.MakeRelativeUri(fullPath).ToString();

                
                    var savePath = relRoot.MakeRelativeUri(fullPath) + "/AcousticMaterial/";
                    using (var reader = new StreamReader(file))
                    {
                        reader.ReadLine();
                        while (!reader.EndOfStream)
                        {
                            var line = reader.ReadLine();
                            var values = line.Split(';');
                            var name = values[0];
                            var assetPath = savePath + name + ".asset";
                            if (File.Exists(assetPath))
                            {
                                Debug.Log(name + " already exists.");
                                continue;
                            }

                            var asset = CreateInstance<AcousticMaterialData>();
                            asset.name = name;
                            asset.notes = values[1];
                            var alpha = new float[31];
                            var s = new float[31];
                            for (var i = 0; i < 31; i++)
                                alpha[i] = float.Parse(values[i + 2].Replace(",", "."));
                            for (var i = 0; i < 31; i++)
                                s[i] = float.Parse(values[i + 2 + 31].Replace(",", "."));

                            var assetPathAndName =
                                AssetDatabase.GenerateUniqueAssetPath(assetPath);
                            AssetDatabase.CreateAsset(asset, assetPathAndName);
                            asset.absorptionThridOctave = alpha;
                            asset.scatteringThridOctave = s;
                            //20|25|31.5|40|50|63|80|100|125|160|200|250|315|400|500|630|800|1000|1250|1600|2000|2500|3150|4000|5000|6300|8000|10000|12500|16000|20000
                            var octaveIndicies = new[] {2};
                            var alphaShort = new float[10];
                            var scatteringShort = new float[10];

                            for (var i = 0; i < 10; i++)
                                alphaShort[i] = alpha[3 * i + 2];
                            for (var i = 0; i < 10; i++)
                                scatteringShort[i] = s[3 * i + 2];

                            asset.absorptionOctave = alphaShort;
                            asset.scatteringOctave = scatteringShort;
                            AssetDatabase.SaveAssets();
                            AssetDatabase.Refresh();
                            EditorUtility.FocusProjectWindow();
                            Selection.activeObject = asset;
                        
                    }

                    Debug.Log($"Assets{relPathAsset}.asset created.");
                }
            }
        }  
    }
}


