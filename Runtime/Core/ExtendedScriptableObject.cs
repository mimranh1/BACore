﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using UnityEngine;
using Object = System.Object;

namespace BA.BACore
{
    public abstract class ExtendedScriptableObject : ScriptableObject
    {
        public string ToJson(string json)
        {
            var name = ToString();
            var myComp = this;
            //var customAttributes = myComp.GetType().GetCustomAttributes(typeof(ExtendedScriptableObject),true);
            var myType = myComp.GetType();
            var strList = new StringBuilder("");
            var props = myType.GetFields();
            var propertyInfos = myType.GetProperties();
            foreach (var prop in propertyInfos)
            {
                var propValue = prop.GetValue(myComp);
                SerilizeJson(propValue, strList);
            }

            foreach (var prop in props)
            {
                var propValue = prop.GetValue(myComp);
                SerilizeJson(propValue, strList);
            }

            if (json == "")
                return $"{json}\"{name}\": [{strList}],";

            return strList.ToString();

        }

        private static void SerilizeJson(object propValue, StringBuilder strList)
        {
           
            switch (propValue)
            {
                case JsonClass ob:
                    strList.Append(ob.ToJson(""));
                    break;
                case ExtendedScriptableObject ob1:
                    strList.Append(ob1.ToJson(""));
                    break;
                case IEnumerable ab2:
                    foreach (var element in ab2)
                    {
                        strList.Append($"\"{propValue}\": [{JsonUtility.ToJson(element, true)}],");
                    }
                    break;
                default:
                    var newJason = JsonUtility.ToJson(propValue, true);
                    strList.Append($"\"{propValue}\": [{newJason}],");
                    break;
            }
        }
        
    }

    public class JsonClass
    {
        public string ToJson(string json)
        {
            var name = ToString();
            var myComp = this;
            //var customAttributes = myComp.GetType().GetCustomAttributes(typeof(ExtendedScriptableObject),true);
            var myType = myComp.GetType();
            var strList = new StringBuilder("");
            var props = myType.GetFields();
            var propertyInfos = myType.GetProperties();
            foreach (var prop in propertyInfos)
            {
                var propValue = prop.GetValue(myComp);
                SerilizeJson(propValue, strList);
            }

            foreach (var prop in props)
            {
                var propValue = prop.GetValue(myComp);
                SerilizeJson(propValue, strList);
            }

            if (json == "")
                return $"{json}\"{name}\": [{strList}],";

            return strList.ToString();

        }

        private static void SerilizeJson(object propValue, StringBuilder strList)
        {
           
            switch (propValue)
            {
                case JsonClass ob:
                    strList.Append(ob.ToJson(""));
                    break;
                case ExtendedScriptableObject ob1:
                    strList.Append(ob1.ToJson(""));
                    break;
                case IEnumerable ab2:
                    foreach (var element in ab2)
                    {
                        strList.Append($"\"{propValue}\": [{JsonUtility.ToJson(element, true)}],");
                    }
                    break;
                default:
                    var newJason = JsonUtility.ToJson(propValue, true);
                    strList.Append($"\"{propValue}\": [{newJason}],");
                    break;
            }
        }
    }
    
    public abstract class ExtendedScriptableObject<TClass,TType> : ExtendedScriptableObject where TClass : ExtendedScriptableObject<TClass,TType> where TType : Enum
    {
        public abstract TType Type { get; }
    }
}