using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace BA.BACore
{
    [Serializable]
    [RequireComponent(typeof(AudioSource))]
    public class SoundSourceObject : SoundObject 
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(SoundSourceObject));

        public string sourceFile => AssetDatabase.GetAssetPath(AudioSource.clip);

        public BinauralImpulseResponse binauralImpulseResponse;
        public LayerMask wallLayerMask;

        public AudioClip Audioclip
        {
            get => GetComponent<AudioSource>().clip;
            set => GetComponent<AudioSource>().clip = value;
        }

        private AudioSource AudioSource => GetComponent<AudioSource>();

        public bool hasChanged = false;

        public float AudioLength => AudioSource.clip.length;

        public bool AudioLoop
        {
            get => AudioSource.loop;
            set => AudioSource.loop = value;
        }
        public bool AudioPlayOnAwake
        {
            get => AudioSource.playOnAwake;
            set => AudioSource.playOnAwake = value;
        }

        public DirectivityIR directivityIr;
        public DirectivityMS directivityMs;

        private FftRealtimeR2C fftOut;
        private float[] fiTimeReal;

        private string LogName = "";

    
        private float[] directivityDataFunction = new float[2];
        private float[] directivityDataFunction2 = new float[2];
        private float[] directivityDataFunction3 = new float[2];

        [System.Serializable]
        public class VaSoundSource
        {
            [ReadOnly] public string path;
            [ReadOnly] public string signalSourceIdVa = "";
            [ReadOnly] public int sourceIdVa = 0;
            [ReadOnly] public bool signalIsActive = false;
            [ReadOnly] public float lengthInSeconds;

            public VaSoundSource(string path)
            {
                this.path = path;
            
            }
        }

        private List<VaSoundSource> vaSoundSources=new List<VaSoundSource>();
        [SerializeReference]
        public VaSoundSource currentVaSoundSource;
        [HideInInspector] public VaSoundSource oldVaSoundSource;
        public int sourceId;

        public BinauralImpulseResponse brir;

        public void CreateNewVaSoundSource(string path)
        {
            hasChanged = true;
            oldVaSoundSource = currentVaSoundSource;
            currentVaSoundSource = null;
            foreach (var vaSoundSource in vaSoundSources.Where(vaSoundSource => vaSoundSource.path == path))
            {
                currentVaSoundSource = vaSoundSource;
                break;
            }

            if (currentVaSoundSource == null)
            {
                var vaSource = new VaSoundSource(path);
                currentVaSoundSource = vaSource;
                vaSoundSources.Add(vaSource);
            }

            Audioclip = (AudioClip) AssetDatabase.LoadAssetAtPath(currentVaSoundSource.path, typeof(AudioClip));
            currentVaSoundSource.lengthInSeconds = Audioclip.length;

        }

        public void Init(int filterLength)
        {
            fftOut = new FftRealtimeR2C(filterLength);
            if (directivityDataFunction.Length != directivityIr.IrHandle.GetLength())
                directivityDataFunction = new float[directivityIr.IrHandle.GetLength()];
            if (directivityDataFunction2.Length != directivityIr.IrHandle.GetLength())
                directivityDataFunction2 = new float[directivityIr.IrHandle.GetLength()];
        }

        public void InitAudioSourceVA(int sourceId)
        {
            AudioSource.mute = true;
            brir = new BinauralImpulseResponse(44100) {sourceIds = new[] {sourceId}};
        }

        public void InitAudioSourceUnity()
        {
            AudioSource.mute = false;
        }


        private void GetAnglesForDirectivityAndIncidenceAngle(Vector3 wallPosition, Vector3 wallUp,
            Vector3 soundPos, Quaternion soundRotation, out float azimuth, out float elevation, out float cosIncidenceAngle)
        {
            var refView = soundRotation * Vector3.back;
            var refUp = soundRotation * Vector3.up;
            soundPos = transformObj.position;
            // var diffVector = ssWalls[i].transformObj.position - transformObj.parent.position;
            var diffVectorNorm = (soundPos - wallPosition);
            diffVectorNorm /= Mathf.Sqrt((diffVectorNorm.x * diffVectorNorm.x) + 
                                         (diffVectorNorm.y * diffVectorNorm.y) +
                                         (diffVectorNorm.z * diffVectorNorm.z));
            Vector3 wLocal;
            wLocal.x = Vector3.Dot(diffVectorNorm, Vector3.Cross(refView, refUp));
            wLocal.y = Vector3.Dot(diffVectorNorm, refUp);
            wLocal.z = Vector3.Dot(diffVectorNorm, refView);

            azimuth = Mathf.Atan2(-wLocal.x, -wLocal.z) * 180.0f / Mathf.PI;
            elevation = Mathf.Asin(wLocal.y) * 180.0f / Mathf.PI;
            cosIncidenceAngle = Mathf.Abs(Vector3.Dot(diffVectorNorm, wallUp));
        }
        
        private float GetFiBySqrtSi4Pi(bool isTimeDomain,Vector3 wallPosition, Vector3 wallNormal, Vector3 soundPos, Quaternion soundRot,
            float[] fiElement, int distanceSamples = 0, float distance = 1f)
        {
            if (directivityDataFunction.Length != directivityIr.IrHandle.GetLength())
                directivityDataFunction = new float[directivityIr.IrHandle.GetLength()];

            GetAnglesForDirectivityAndIncidenceAngle(wallPosition, wallNormal, soundPos,
                soundRot, out var azimuth, out var elevation, out var cosIncidenceAngle);
            var variableFiFactor = Mathf.Sqrt(OneBy4PI*Mathf.Abs(cosIncidenceAngle)) / distance;
            var sourceIndex = directivityIr.IrHandle.GetNearestNeighbourRecordIndex(azimuth, elevation);
            directivityIr.IrHandle.GetRecordData(directivityDataFunction, sourceIndex, 0);
            if(isTimeDomain)
                for (var i = 0; i < directivityDataFunction.Length; i++)
                {
                    fiElement[i + distanceSamples] = directivityDataFunction[i] * variableFiFactor;
                }
            else
            {
                var fiElementLength = fiElement.Length / 2;
                if (directivityDataFunction2.Length != fiElementLength)
                    directivityDataFunction2 = new float[fiElementLength];
                if (fftOut.Length != fiElementLength)
                    fftOut = new FftRealtimeR2C(fiElementLength);
                for (var i = 0; i < directivityDataFunction.Length; i++)
                    directivityDataFunction2[i + distanceSamples] = directivityDataFunction[i] * variableFiFactor;

                fftOut.RunFftReal(directivityDataFunction2, fiElement);
                Array.Clear(directivityDataFunction2, distanceSamples, directivityDataFunction.Length);
                
            }
            return cosIncidenceAngle;
        }

        
        private void GetFiBySqrtSi4Pi(bool isTimeDomain,float[] fiElement, Vector3 positionElement,Vector3 wallNormal, Vector3 soundPos, Quaternion soundRot,
             bool distanceFactor = false, bool distanceDelay = false)
        {
            var distance = Vector3.Distance(positionElement, soundPos);
            var distanceSamples = distanceDelay
                ? Mathf.Min((int) (distance * SamplesByMeter), fiElement.Length - directivityIr.IrHandle.GetLength())
                : 0;
            if (!distanceFactor) distance = 1f;

            GetFiBySqrtSi4Pi(isTimeDomain, positionElement, wallNormal, soundPos, soundRot, fiElement, distanceSamples, distance);
        }

        public void GetFiBySqrtSi4Pi(bool isTimeDomain, float[][] fiElement, Vector3[] positionElements, Vector3[] wallNormals,
            bool distanceFactor = false, bool distanceDelay = false)
        {
            var transform1 = transformObj;
            var soundPos = transform1.position;
            var soundRot = transform1.rotation;
            for (var i = 0; i < positionElements.Length; i++)
            {
                GetFiBySqrtSi4Pi(isTimeDomain, fiElement[i], positionElements[i], wallNormals[i], soundPos, soundRot,
                    distanceFactor, distanceDelay);
            }
        }
        
        public void GetFiBySqrtSi4Pi(bool isTimeDomain, float[] fiElement, Vector3 positionElements, Vector3 wallNormals,
            bool distanceFactor = false, bool distanceDelay = false)
        {
            var transform1 = transformObj;
            var soundPos = transform1.position;
            var soundRot = transform1.rotation;
            GetFiBySqrtSi4Pi(isTimeDomain, fiElement, positionElements, wallNormals, soundPos, soundRot,
                distanceFactor, distanceDelay);
        }
        public void GetFiBySqrtSi4PiSum(bool isTimeDomain, float[] fiElement, IGeometrySegments positionElements, float factor,
            bool distanceFactor = false, bool distanceDelay = false)
        {
            var transform1 = transformObj;
            var soundPos = transform1.position;
            var soundRot = transform1.rotation;
            
            var fiElementLength = fiElement.Length / 2;
            if (directivityDataFunction2.Length != fiElementLength)
                directivityDataFunction2 = new float[fiElementLength];
            if (fftOut.Length != fiElementLength)
                fftOut = new FftRealtimeR2C(fiElementLength);
                
            GetFiForAllSegments(fiElement.Length, positionElements, factor, distanceFactor, distanceDelay, soundPos, soundRot);

            if (isTimeDomain)
            {
                directivityDataFunction2.CopyTo(fiElement, 0);
            }
            else
            {
                fftOut.RunFftReal(directivityDataFunction2, fiElement);
            }
            Array.Clear(directivityDataFunction2, 0, directivityDataFunction2.Length);
        }   
        
        public void GetFiBySqrtSi4PiSum(float[] fiElement, IGeometrySegments[] positionElements,WallBehaviour[] patches,Interpolator interpolator,float factor,
            bool distanceFactor = false, bool distanceDelay = false)
        {
            var transform1 = transformObj;
            var soundPos = transform1.position;
            var soundRot = transform1.rotation;
            
            var fiElementLength = fiElement.Length / 2;
            if (directivityDataFunction2.Length != fiElementLength)
                directivityDataFunction2 = new float[fiElementLength];  
            if (directivityDataFunction3.Length != fiElementLength*2)
                directivityDataFunction3 = new float[fiElementLength*2];
            if (fftOut.Length != fiElementLength)
                fftOut = new FftRealtimeR2C(fiElementLength);

            for (var iPatch = 0; iPatch < positionElements.Length; iPatch++)
            {
                var cosIncidenceAngle = GetFiForAllSegments(fiElement.Length, positionElements[iPatch], factor,
                    distanceFactor, distanceDelay, soundPos, soundRot);
                var tau = patches[iPatch].reductionIndex.@object
                    .CalcSoundReductionIndexAngle(Mathf.Acos(cosIncidenceAngle[0]));
                var sqrtTauComplex = interpolator.CalcSqrtTauComplex(tau.Tf, fiElement.Length / 2);
                fftOut.RunFftReal(directivityDataFunction2, directivityDataFunction3);
                if (iPatch == 0)
                    ComplexFloats.Multiply(directivityDataFunction3, sqrtTauComplex, fiElement);
                else
                    ComplexFloats.MultiplyAdd(directivityDataFunction3, sqrtTauComplex, fiElement);
                Array.Clear(directivityDataFunction2, 0, directivityDataFunction2.Length);

            }

        }

        public void GetFiBySqrtSi4Pi(float[] fiElement, IGeometrySegments positionElements, WallBehaviour patches,
            Interpolator interpolator, float factor,
            bool distanceFactor = false, bool distanceDelay = false)
        {
            var transform1 = transformObj;
            var soundPos = transform1.position;
            var soundRot = transform1.rotation;
            

            var fiElementLength = fiElement.Length / 2;
            if (directivityDataFunction2.Length != fiElementLength)
                directivityDataFunction2 = new float[fiElementLength];
            if (directivityDataFunction3.Length != fiElementLength * 2)
                directivityDataFunction3 = new float[fiElementLength * 2];
            if (fftOut.Length != fiElementLength)
                fftOut = new FftRealtimeR2C(fiElementLength);


            var cosIncidenceAngle = GetFiForAllSegments(fiElement.Length, positionElements, factor,
                distanceFactor, distanceDelay, soundPos, soundRot);
            if (cosIncidenceAngle == null)
            {
                Array.Clear(fiElement,0,fiElement.Length);
                return;
            }
                
            var tau = patches.reductionIndex.@object
                .CalcSoundReductionIndexAngle(Mathf.Acos(Mathf.Abs(cosIncidenceAngle[0])),
                    BaSettings.Instance.GetInterpolatedFrequencyBand(fiElementLength / 2));
            var sqrtTauComplex = interpolator.CalcSqrtTauComplex(tau.Tf, fiElementLength);
            fftOut.RunFftReal(directivityDataFunction2, directivityDataFunction3);
            ComplexFloats.Multiply(directivityDataFunction3, sqrtTauComplex, fiElement);
            Array.Clear(directivityDataFunction2, 0, directivityDataFunction2.Length);
        }


        public float[] GetFiBySqrtSi4Pi(float[] fiElement, IGeometrySegments positionElements, WallBehaviour patches,
            Interpolator interpolator, float factor,AngleTauData angleTauData,
            bool distanceFactor = false, bool distanceDelay = false)
        {
            var transform1 = transformObj;
            var soundPos = transform1.position;
            var soundRot = transform1.rotation;
            
            var fiElementLength = fiElement.Length / 2;
            if (directivityDataFunction2.Length != 2*fiElementLength)
                directivityDataFunction2 = new float[2*fiElementLength];
            if (directivityDataFunction3.Length != fiElementLength * 2)
                directivityDataFunction3 = new float[fiElementLength * 2];
            if (fftOut.Length != fiElementLength)
                fftOut = new FftRealtimeR2C(fiElementLength);

            var cosIncidenceAngle = GetFiForAllSegments(fiElement.Length, positionElements, factor,
                distanceFactor, distanceDelay, soundPos, soundRot);
            if (cosIncidenceAngle == null)
            {
                Array.Clear(fiElement,0,fiElement.Length);
                return new float[0];
            }
            
            var sqrtTauComplex = angleTauData.GetNearestTauSqrtTauComplexRad(Mathf.Acos(cosIncidenceAngle[0]));
            fftOut.RunFft(directivityDataFunction2, directivityDataFunction3);
            ComplexFloats.Multiply(directivityDataFunction3, sqrtTauComplex, fiElement);
            Array.Clear(directivityDataFunction2, 0, directivityDataFunction2.Length);
            return cosIncidenceAngle;
        }

      
        private float[] GetFiForAllSegments(int fiElementLength, IGeometrySegments positionElements, float factor,
            bool distanceFactor, bool distanceDelay, Vector3 soundPos, Quaternion soundRot)
        {
            
            var cosIncidenceAngle = new float[positionElements.Segments.Length];
            for (var i = 0; i < positionElements.Segments.Length; i++)
            {
                var patchPos = positionElements.Segments[i].Position;
                if (Physics.Raycast(patchPos, soundPos - patchPos, out var hit,
                    Vector3.Distance(soundPos, patchPos) - positionElements.Segments[0].Thickness, wallLayerMask))
                {
                    // wall in between
                    Array.Clear(directivityDataFunction2,0,directivityDataFunction2.Length);
                    return null;
                    continue;
                }
                var distance = Vector3.Distance(positionElements.Segments[i].Position, soundPos);
                var distanceSamples = distanceDelay
                    ? Mathf.Min((int) (distance * SamplesByMeter),
                        fiElementLength - directivityIr.IrHandle.GetLength())
                    : 0;
                if (!distanceFactor) distance = 1f;
                cosIncidenceAngle[i] = GetFiBySqrtSi4Pi(true, positionElements.Segments[i].Position,
                    positionElements.Segments[i].Normal, soundPos, soundRot, directivityDataFunction);
                var variableFiFactor = Mathf.Sqrt(OneBy4PI * Mathf.Abs(cosIncidenceAngle[i])) / distance * factor;

                for (var n = 0; n < Math.Min(directivityDataFunction.Length,directivityDataFunction2.Length- distanceSamples); n++)
                    directivityDataFunction2[2*(n + distanceSamples)] += directivityDataFunction[n] * variableFiFactor;
            }

            return cosIncidenceAngle;
        }

        public void GetFiBySqrtSi4Pi(bool isTimeDomain, FloatArrays[] fiElement, Vector3[] positionElements, Vector3[] wallNormals, bool distanceFactor = false,
            bool distanceDelay = false)
        {
            var transform1 = transformObj;
            var soundPos = transform1.position;
            var soundRot = transform1.rotation;
            for (var i = 0; i < positionElements.Length; i++)
            {
                GetFiBySqrtSi4Pi(isTimeDomain, fiElement[i].floats, positionElements[i], wallNormals[i], soundPos, soundRot, 
                    distanceFactor, distanceDelay);
            }
        }

        /// <summary>
        /// Source Fi for all Ss
        /// </summary>
        /// <param name="inputWalls">input Walls or patches Array</param>
        /// <param name="filterLength">Length of filter</param>
        /// <param name="logRealtime">if true than log data</param>
        /// <returns>Fi in Freq for each Ss</returns>
        public float[][] GetSourceFiForSs(WallBehaviour[] inputWalls, int filterLength, bool logRealtime = false)
        {
            var fiFreq = new float[inputWalls.Length][];
            var soundPos = transformObj.position;
            var soundRot = transformObj.rotation;
            var directivityLength = directivityIr.IrHandle.GetLength();
            var constFiFactor = Mathf.Sqrt(1 / (4 * Mathf.PI));
            for (var iSs = 0; iSs < inputWalls.Length; iSs++)
            {
                var siPatches = Mathf.Sqrt(inputWalls[iSs].Geometry.Area / inputWalls[iSs].WallReceiverSegments.Positions.Count);
                var wallUp = inputWalls[iSs].Geometry.Normal;
                fiFreq[iSs] = CalcFiForOneWall(filterLength, soundPos, wallUp, soundRot, directivityLength,
                    constFiFactor, inputWalls[iSs].WallReceiverSegments.Positions.ToArray(), siPatches, out var delaySamples, logRealtime, iSs);
            }

            return fiFreq;
        }

        /// <summary>
        /// Source Fi for all Ss
        /// </summary>
        /// <param name="inputWalls">input Walls or patches Array</param>
        /// <param name="fiFreq"></param>
        /// <param name="filterLength">Length of filter</param>
        /// <returns>Fi in Freq for each Ss</returns>
        public void GetSourceFiForSs(WallBehaviour[] inputWalls, float[][] fiFreq, int filterLength)
        {
            var soundPos = transformObj.position;
            var soundRot = transformObj.rotation;
            var directivityLength = directivityIr.IrHandle.GetLength();
            var constFiFactor = Mathf.Sqrt(1 / (4 * Mathf.PI));
            if (fftOut.Length != filterLength)
                fftOut = new FftRealtimeR2C(filterLength);
            if (directivityDataFunction.Length != directivityLength)
                directivityDataFunction = new float[directivityLength];
            for (var iWall = 0; iWall < inputWalls.Length; iWall++)
            {
                var numSs = inputWalls[iWall].WallReceiverSegments.Positions.Count;
                var siPatches = inputWalls[iWall].Geometry.Area / numSs;
                var wallUp = inputWalls[iWall].Geometry.Normal;

                if (numSs > 1)
                {
                    throw new NotImplementedException();
                }
                else
                {
                    GetAnglesForDirectivityAndIncidenceAngle(inputWalls[iWall].WallReceiverSegments.Positions[0], wallUp, soundPos,
                        soundRot,
                        out var azimuth, out var elevation, out var cosIncidenceAngle);
                    var dist = Vector3.Distance(inputWalls[iWall].WallReceiverSegments.Positions[0], soundPos);
                    var variableFiFactor = constFiFactor * siPatches * Mathf.Sqrt(Mathf.Abs(cosIncidenceAngle)) / dist;
                    var sourceIndex = directivityIr.IrHandle.GetNearestNeighbourRecordIndex(azimuth, elevation);
                    directivityIr.IrHandle.GetRecordData(directivityDataFunction, sourceIndex, 0);

                    for (var i = 0; i < directivityDataFunction.Length; i++)
                        directivityDataFunction[i] *= variableFiFactor;

                    fftOut.RunFftReal(directivityDataFunction, fiFreq[iWall]);
                }

                // fiFreq[iSs] = CalcFiForOneWall(filterLength, soundPos, wallUp, soundRot, fsByC0, directivityLength,
                //    constFiFactor, ssAllPos[iSs], SsWallDistance[iSs], siPatches, out int delaySamples, logRealtime, iSs);
            }
        }

        /// <summary>
        /// Update Source Fi in Freq for input Walls 
        /// </summary>
        /// <param name="inputWalls">input Walls or patches Array</param>
        /// <param name="fiFreqWalls">output reference array for FiFreq</param>
        /// <param name="applyDelayWalls">if true time delay will be added, default is true</param>
        public void GetSourceFiForAllSumSs(WallBehaviour[] inputWalls, float[][] fiFreqWalls, bool applyDelayWalls = true)
        {
            if (fiFreqWalls == null)
                return;
            var filterLength = fiFreqWalls[0].Length / 2;
            var soundPos = transformObj.position;
            var soundRot = transformObj.rotation;
            var directivityLength = directivityIr.IrHandle.GetLength();
            var constFiFactor = Mathf.Sqrt(1 / (4 * Mathf.PI));
            var fsByC0 = BaSettings.Instance.SampleRate / BaSettings.Instance.C0;
            if (fftOut.Length != filterLength)
                fftOut = new FftRealtimeR2C(filterLength);
            if (directivityDataFunction.Length != directivityLength)
                directivityDataFunction = new float[directivityLength];
            if (directivityDataFunction2.Length != directivityLength)
                directivityDataFunction2 = new float[directivityLength];

            for (var iSs = 0; iSs < inputWalls.Length; iSs++)
            {
                var numSs = inputWalls[iSs].WallReceiverSegments.Positions.Count;
                var siPatches = Mathf.Sqrt(inputWalls[iSs].Geometry.Area / numSs);
                var wallUp = inputWalls[iSs].Geometry.Normal;

                var samplesDelay = 0;
                var wallPos = inputWalls[iSs].WallReceiverSegments.Positions;
                for (var imn = 0; imn < numSs; imn++)
                {
                    GetAnglesForDirectivityAndIncidenceAngle(wallPos[imn], wallUp, soundPos, soundRot,
                        out var azimuth, out var elevation, out var cosIncidenceAngle);
                    var dist = Vector3.Distance(wallPos[imn], soundPos);
                    var variableFiFactor = constFiFactor * siPatches * Mathf.Sqrt(Mathf.Abs(cosIncidenceAngle)) / dist;
                    var sourceIndex = directivityIr.IrHandle.GetNearestNeighbourRecordIndex(azimuth, elevation);
                    if (applyDelayWalls)
                        samplesDelay += (int) (dist * fsByC0);
                    if (imn == 0)
                    {
                        directivityIr.IrHandle.GetRecordData(directivityDataFunction, sourceIndex, 0);
                        for (var i = 0; i < directivityDataFunction.Length; i++)
                            directivityDataFunction[i] *= variableFiFactor;
                    }
                    else
                    {
                        directivityIr.IrHandle.GetRecordData(directivityDataFunction2, sourceIndex, 0);
                        for (var n = 0; n < directivityDataFunction2.Length; n++)
                            directivityDataFunction[n+samplesDelay] += variableFiFactor * directivityDataFunction2[n];
                    }
                }


                fftOut.RunFftReal(directivityDataFunction, fiFreqWalls[iSs]);
            }
        }


        // /// <summary>
        // /// Calc Fi Freq for each Patch 
        // /// </summary>
        // /// <param name="partition">reference to partition to calc Patches</param>
        // /// <param name="fiFreq">Ref of result FiFreq</param>
        // /// <param name="delaySamples">ref of result delay Samples</param>
        // /// <param name="fixedDelay">define Mode of output delay </param>
        // /// <exception cref="NotImplementedException">if more than one SS on Patch</exception>
        // public void GetSourceFiForPatches(WallBehaviour partition, float[][] fiFreq, int[] delaySamples,
        //     int fixedDelay = 0)
        // {
        //     var filterLength = fiFreq[0].Length / 2;
        //     var numPatches = partition.patchesHandler.NumOfPatches;
        //     var soundPos = transformObj.position;
        //     var soundRot = transformObj.rotation;
        //     var directivityLength = directivityIr.IrHandle.GetLength();
        //     var constFiFactor = Mathf.Sqrt(1 / (4 * Mathf.PI));
        //     var fsByC0 = BaSettings.Instance.SampleRate / BaSettings.Instance.C0;
        //     var wallUp = partition.Geometry.Normal;
        //
        //     if (fftOut.Length != filterLength)
        //         fftOut = new FftRealtimeR2C(filterLength);
        //     if (directivityDataFunction.Length != directivityLength)
        //         directivityDataFunction = new float[directivityLength];
        //     var ssPartitionPatches = partition.patchesHandler.SecondarySourcesPosPatches;
        //     for (var iPatch = 0; iPatch < numPatches; iPatch++)
        //     {
        //         var numSs = ssPartitionPatches[iPatch].Length;
        //         var siPatches = Mathf.Sqrt(partition.patchesHandler.Patches[iPatch].Geometry.Area / numSs);
        //         if (numSs > 1)
        //         {
        //             throw new NotImplementedException();
        //         }
        //         else
        //         {
        //             GetAnglesForDirectivityAndIncidenceAngle(ssPartitionPatches[iPatch][0], wallUp, soundPos, soundRot,
        //                 out var azimuth, out var elevation, out var cosIncidenceAngle);
        //             var dist = Vector3.Distance(ssPartitionPatches[iPatch][0], soundPos);
        //             var variableFiFactor = constFiFactor * siPatches * Mathf.Sqrt(Mathf.Abs(cosIncidenceAngle)) / dist;
        //             var sourceIndex = directivityIr.IrHandle.GetNearestNeighbourRecordIndex(azimuth, elevation);
        //             directivityIr.IrHandle.GetRecordData(directivityDataFunction, sourceIndex, 0);
        //
        //             if (fixedDelay > 0)
        //                 delaySamples[iPatch] = fixedDelay;
        //             else
        //                 delaySamples[iPatch] += (int) (dist * fsByC0) - 128;
        //
        //             for (var i = 0; i < directivityDataFunction.Length; i++)
        //                 directivityDataFunction[i] *= variableFiFactor;
        //
        //             fftOut.RunFft(directivityDataFunction, fiFreq[iPatch], delaySamples[iPatch]);
        //         }
        //     }
        // }


        // /// <summary>
        // /// Calc Fi Freq for each Patch 
        // /// </summary>
        // /// <param name="partition">reference to partition to calc Patches</param>
        // /// <param name="fiFreq">Ref of result FiFreq</param>
        // /// <exception cref="NotImplementedException">if more than one SS on Patch</exception>
        // public void GetSourceTauDavyPatches(WallPatchesHandler partition, float[][] fiFreq)
        // {
        //     var filterLength = fiFreq[0].Length / 2;
        //     var numPatches = partition.NumOfPatches;
        //     var transform1 = transformObj;
        //     var soundPos = transform1.position;
        //     var soundRot = transform1.rotation;
        //     var directivityLength = directivityIr.IrHandle.GetLength();
        //     var wallUp = partition.Geometry.Normal;
        //
        //     if (fftOut?.Length != filterLength)
        //         fftOut = new FftRealtimeR2C(filterLength);
        //     if (directivityDataFunction.Length != directivityLength)
        //         directivityDataFunction = new float[directivityLength];
        //     var ssPartitionPatches = partition.SecondarySourcesPosPatches;
        //     for (var iPatch = 0; iPatch < numPatches; iPatch++)
        //     {
        //         if (filterLength != fiFreq[iPatch].Length / 2)
        //             Debug.LogWarning("Length of Hrtf[" + iPatch + "]"); // todo
        //         var numSs = ssPartitionPatches[iPatch].Length;
        //         if (numSs > 1)
        //         {
        //             throw new NotImplementedException();
        //         }
        //         else
        //         {
        //             GetAnglesForDirectivityAndIncidenceAngle(ssPartitionPatches[iPatch][0], wallUp, soundPos, soundRot,
        //                 out var azimuth, out var elevation, out var cosIncidenceAngle);
        //             // var angle = Mathf.Abs(Mathf.Acos(cosIncidenceAngle) / (2 * Mathf.PI) * 360f + 90f);             // 180-90-180
        //             // var angle = Mathf.Abs(Mathf.Acos(Mathf.Abs(cosIncidenceAngle)) * 180f / Mathf.PI);             // 90-0-90
        //             var angle = Mathf.Abs(Mathf.Acos(Mathf.Abs(cosIncidenceAngle)) * 180f / Mathf.PI);             // 90-0-90
        //             // if (iPatch == 0)
        //             //     Debug.Log("incidentAngle: " + angle + " Acos: " + (Mathf.Acos(Mathf.Abs(cosIncidenceAngle)) * 180f / Mathf.PI));
        //
        //             var sourceIndex = directivityIr.IrHandle.GetNearestNeighbourRecordIndex(angle, 0);
        //             bool isPhaseIncluded = true;
        //             if (isPhaseIncluded)
        //             {
        //                 directivityIr.IrHandle.GetRecordData(fiFreq[iPatch], sourceIndex, 0);
        //             }
        //             else
        //             {
        //                 directivityIr.IrHandle.GetRecordData(directivityDataFunction, sourceIndex, 0);
        //                 var phaseFactor = -2f * directivityDataFunction.Length * Mathf.PI / BaSettings.Instance.SampleRate;
        //                 for (var k = 0; k < directivityDataFunction.Length; k++)
        //                 {
        //                     var phase = phaseFactor * k;
        //
        //                     // Mirror Signal
        //                     var magnitude = (directivityDataFunction[k]);
        //                     fiFreq[iPatch][2 * k] = (float) (magnitude * Math.Cos(phase));
        //                     fiFreq[iPatch][2 * k + 1] = (float) (magnitude * Math.Sin(phase));
        //                 }
        //             }
        //
        //             // for (var i = 0; i < directivityDataFunction.Length; i++)
        //             //     fiFreq[iPatch][2 * i] = directivityDataFunction[i];
        //
        //             //directivityDataFunction.CopyTo(fiFreq[iPatch],0);
        //         }
        //     }
        // }

        // /// <summary>
        // /// Calc Fi Freq for each Patch 
        // /// </summary>
        // /// <param name="partition">reference to partition to calc Patches</param>
        // /// <param name="filterLength">output Filter Length</param>
        // /// <param name="delaySamples">out delay in Samples for each Patch</param>
        // /// <param name="logRealtime">if true than log data</param>
        // /// <param name="logId">if logId > 0 than log</param>
        // /// <returns>new ref FiFreq for each Patch and </returns>
        // public float[][] GetSourceFiForPatches(WallBehaviour partition, int filterLength, out int[] delaySamples,
        //     bool logRealtime = false,
        //     int logId = 0)
        // {
        //     if (logRealtime)
        //         Log.Debug("Call GetSourceFiForPatches");
        //     var numPatches = partition.patchesHandler.NumOfPatches;
        //     var fiFreq = new float[numPatches][];
        //     var transform1 = transformObj;
        //     var soundPos = transform1.position;
        //     var soundRot = transform1.rotation;
        //     var directivityLength = directivityIr.IrHandle.GetLength();
        //     var constFiFactor = Mathf.Sqrt(1 / (4 * Mathf.PI));
        //     delaySamples = new int[numPatches];
        //     var ssPartitionPatches = partition.patchesHandler.SecondarySourcesPosPatches;
        //     for (var iPatch = 0; iPatch < numPatches; iPatch++)
        //     {
        //         var siPatches = Mathf.Sqrt(partition.patchesHandler.Patches[iPatch].Geometry.Area / ssPartitionPatches[iPatch].Length);
        //         var wallUp = partition.Geometry.Normal;
        //         fiFreq[iPatch] = CalcFiForOneWall(filterLength, soundPos, wallUp, soundRot, directivityLength,
        //             constFiFactor, ssPartitionPatches[iPatch], siPatches,
        //             out delaySamples[iPatch], logRealtime, iPatch, logId);
        //     }
        //
        //     return fiFreq;
        // }

        /// <summary>
        /// Calculate the FiFreq for each SS for One Wall
        /// </summary>
        /// <param name="filterLength">output Filter Length</param>
        /// <param name="soundPos">Source Position</param>
        /// <param name="wallUp">Wall Up Vector for Orientation</param>
        /// <param name="soundRot">source Rotation</param>
        /// <param name="ssPositions">Positions of SecondarySources</param>
        /// <param name="siPatches">Area of Patches</param>
        /// <param name="delaySamples">delay Samples</param>
        /// <param name="logRealtime">if true than log data</param>
        /// <param name="iSs">Id of SS for Logging</param>
        /// <param name="logId">if logId > 0 than log</param>
        /// <returns>return FiFreq</returns>
        public float[] CalcFiForOneWall(int filterLength, Vector3 soundPos, Vector3 wallUp, Quaternion soundRot,
            Vector3[] ssPositions, float siPatches, out int delaySamples, bool logRealtime = false, int iSs = 0,
            int logId = 0)
        {
            var ssDistance = new float[ssPositions.Length];
            for (var i = 0; i < ssPositions.Length; i++)
                ssDistance[i] = Vector3.Distance(soundPos, ssPositions[i]);
            var constFiFactor = Mathf.Sqrt(1 / (4 * Mathf.PI));
            return CalcFiForOneWall(filterLength, soundPos, wallUp, soundRot,
                directivityIr.IrHandle.GetLength(), constFiFactor, ssPositions,
                siPatches, out delaySamples, logRealtime, iSs, logId);
        }

        private float[] CalcFiForOneWall(int filterLength, Vector3 soundPos, Vector3 wallUp, Quaternion soundRot,
            int directivityLength, float constFiFactor, Vector3[] ssPositions,
            float siPatches, out int delaySamples, bool logRealtime = false, int iSs = 0, int logId = 0)
        {
            //wallUp = wallUp.y == 1 ? Vector3.right : Vector3.up;
            var fiTime = new float[2 * filterLength];
            var irSum = new float[directivityIr.IrHandle.GetLength()];
            delaySamples = 0;
            for (var imn = 0; imn < ssPositions.Length; imn++)
            {
                GetAnglesForDirectivityAndIncidenceAngle(ssPositions[imn], wallUp, soundPos, soundRot,
                    out var azimuth, out var elevation, out var cosIncidenceAngle);
                var dist = Vector3.Distance(ssPositions[imn], soundPos);
                var variableFiFactor = siPatches * Mathf.Sqrt(Mathf.Abs(cosIncidenceAngle)) / dist;
                var sourceIndex = directivityIr.IrHandle.GetNearestNeighbourRecordIndex(azimuth, elevation);
                var ir = directivityIr.IrHandle.GetRecordData(sourceIndex, 0);

                // delaySamples += (int) (ssDistance[imn] * fsByC0) - 128;
                delaySamples += 512 * 8 + 400;
                for (var i = 0; i < irSum.Length; i++)
                    irSum[i] += ir[i] * constFiFactor * variableFiFactor;

                if (logRealtime)
                {
                    DataLogger.Log(LogName + "sourceSS.azimuth(" + (iSs + 1) + "," + (imn + 1) + ")", azimuth);
                    DataLogger.Log(LogName + "sourceSS.elevation(" + (iSs + 1) + "," + (imn + 1) + ")", elevation);
                    DataLogger.Log(LogName + "sourceSS.cosIncidenceAngle(" + (iSs + 1) + "," + (imn + 1) + ")",
                        cosIncidenceAngle);
                    DataLogger.Log(LogName + "sourceSS.fiFactors(" + (iSs + 1) + "," + (imn + 1) + ")",
                        constFiFactor * variableFiFactor);
                    DataLogger.Log(LogName + "sourceSS.delaySamples(" + (iSs + 1) + "," + (imn + 1) + ")",
                        delaySamples);
                }
            }

            delaySamples /= ssPositions.Length;
            if (delaySamples < 0)
            {
                Log.Warn("Source to close to wall. Directivity will be cut by " + -delaySamples + " Samples.");
                for (var i = 0; i < directivityLength + delaySamples; i++) fiTime[2 * i] += irSum[i - delaySamples];
            }
            else
            {
                for (var i = 0; i < directivityLength; i++) fiTime[2 * (i + delaySamples)] += irSum[i];
            }

            var fOut = new float[2 * filterLength];
            fftOut.RunFftReal(fiTime, fOut);
            if (logId > 0)
            {
                DataLogger.DebugData = true;
                DataLogger.Log(LogName + "sourceSS.delaySamples(" + logId + "," + (iSs + 1) + ")",
                    delaySamples);
                DataLogger.Log(LogName + "sourceSS.ssPositions(" + logId + "," + (iSs + 1) + ",:)",
                    ssPositions[0]);
                DataLogger.Log(LogName + "sourceSS.soundPos(" + logId + "," + (iSs + 1) + ",:)",
                    soundPos);
                DataLogger.DebugData = false;
            }

            if (logRealtime)
            {
                logId = 1;
                DataLogger.Log(LogName + "sourceSS.fiTime(" + logId + "," + (iSs + 1) + ",:)", fiTime);
                DataLogger.Log(LogName + "sourceSS.fiFreqComplex(" + logId + "," + (iSs + 1) + ",:)", fOut);
            }

            return fOut;
        }
        
        private void OnDrawGizmosSelected()
        {
            if(transformObj!=null)
            {
                Gizmos.color = Color.red;
                var position = transformObj.position;
                Gizmos.DrawSphere(position, 0.3f);
                Gizmos.DrawRay(position, 2f*transformObj.forward);
                Gizmos.color = Color.blue;
                Gizmos.DrawRay(position, 2f*transformObj.up);
            }        
        }
    }
}