using UnityEngine;

namespace BA.BACore
{
    public class SoundObject : MonoBehaviour
    {
    
        public Transform transformObj;
    
        private Vector3 oldRotationEuler;
        private Vector3 oldPosition;

        private float _oneBy4PI = -1f;
        public float OneBy4PI
        {
            get
            {
                if (_oneBy4PI < 0f)
                    _oneBy4PI = Mathf.Sqrt(1 / (4 * Mathf.PI));
                return _oneBy4PI;
            }
        }

        public float SamplesByMeter
        {
            get
            {
                if (_samplesByMeter < 0f)
                    _samplesByMeter = BaSettings.Instance.SampleRate / BaSettings.Instance.C0;
                return _samplesByMeter;
            }
        }

        private float _samplesByMeter=-1f;

      
        /// <summary>
        /// Check if the Sound Object has moved or rotated
        /// </summary>
        /// <returns>true if Position changed</returns>
        public bool SoundObjectChanged()
        {
            return RotateMoreThan() || MoveMoreThan();
        }

        /// <summary>
        /// Check if the Sound Object has moved
        /// </summary>
        /// <returns>true if position moved</returns>
        public bool SoundObjectMove()
        {
            return MoveMoreThan(0.1f);
        }

        /// <summary>
        /// Check if the Sound Object has rotated
        /// </summary>
        /// <returns>true if object rotated</returns>
        public bool SoundObjectRotate()
        {
            return RotateMoreThan();
        }

        private bool RotateMoreThan(float limitInDegree = 5f)
        {
            var diffRotationAngles = oldRotationEuler - transformObj.rotation.eulerAngles;
            if (!AbsElementVectorLargerThanConst(diffRotationAngles, limitInDegree))
                return false;
            //Log.Info("Vector Rotation: " + transformObj.rotation.eulerAngles + diffRotationAngles);

            oldRotationEuler = transformObj.rotation.eulerAngles;
            return true;
        }

        private bool MoveMoreThan(float limitInCm = 0.01f)
        {
            if (Vector3.Distance(oldPosition, transformObj.position) < limitInCm)
                return false;

            //Log.Info("Vector Position: " + transformObj.position + diffPosition);
            oldPosition = transformObj.position;
            return true;
        }

        /// <summary>
        /// If Vector is larger than big, return false
        /// </summary>
        /// <param name="vec"></param>
        /// <param name="big"></param>
        /// <returns></returns>
        private bool AbsElementVectorLargerThanConst(Vector3 vec, float big)
        {
            for (var i = 0; i < 3; i++)
                if (Mathf.Abs(vec[i]) > big)
                    return true;

            return false;
        }
    }
}