﻿using System.Globalization;
using System.IO;
using UnityEngine;

namespace BA.BACore
{
    public class DopplerGenerator : MonoBehaviour
    {
        public bool running;

        [Tooltip("Speed in kmph")] public float speed = 1.0f;

        /// <summary>
        /// Start Position of the Moving Source
        /// </summary>
        public Vector3 startPos;

        /// <summary>
        /// End Position of the Moving Source
        /// </summary>
        public Vector3 endPos;

        /// <summary>
        /// Current Direction of Movement
        /// </summary>
        public float dir = 1f;

        /// <summary>
        /// set true if EndPos first time reached
        /// </summary>
        public bool endReached = false;

        public int sampleRate = 44100;

        public float wayLength;

        private int outputAudioOffset;
        private int limSamples;
        private float[] samplesOut;

        // Start is called before the first frame update
        private void Start()
        {
            if (!running)
                return;

            transform.position = startPos;
            dir = -1f;
            endReached = false;
            wayLength = Mathf.Abs(endPos.x - startPos.x);
            var time = wayLength / (speed / 3.6f);
            limSamples = (int) (time * sampleRate);
            samplesOut = new float[2 * limSamples];
            outputAudioOffset = 0;
        }

        private void OnAudioFilterRead(float[] data, int channels)
        {
            if (!running)
                return;

            if (outputAudioOffset < limSamples)
            {
                data.CopyTo(samplesOut, outputAudioOffset);
                outputAudioOffset += data.Length;
            }
        }

        /// <summary>
        /// Update call from Unity and update Position
        /// </summary>
        public void Update()
        {
            if (!running)
                return;

            transform.position = UpdatePosition(transform.position, speed / 3.6f * Time.deltaTime);
            if (endReached)
            {
                var max = Mathf.Max(Mathf.Max(samplesOut), -Mathf.Min(samplesOut));
                var sampleMono = new float[outputAudioOffset / 2];
                for (var i = 0; i < outputAudioOffset / 2; i++) sampleMono[i] = samplesOut[2 * i] / max;
                var outputAudio = AudioClip.Create("output", sampleMono.Length, 1, sampleRate, false);
                outputAudio.SetData(sampleMono, 0);
                //var timePrefix = DateTime.Now.ToString("yyyyMMdd-HHmmss-");
                var filename = "doppler" + "-" + speed + "kmph-Pos" + Vec32Str(startPos) + "-" + wayLength + ".wav";
                var filePath = Path.Combine(BaSettings.Instance.SoundsPath, filename);
                SavWav.Save(filePath, outputAudio);
                running = false;
                Debug.Log("Wrote audioFile");
            }
        }

        private string Vec32Str(Vector3 vec)
        {
            return vec.x.ToString("000.0") + "_" + vec.y.ToString("000.0") + "_" + vec.z.ToString("000.0");
        }

        /// <summary>
        /// Start is called before the first frame update
        /// </summary>
        public void ResetPositionAndSound()
        {
            /*
        transform.position = endPos;
        isX = false;
        dir = -1f;
        */
            transform.position = startPos;
            dir = -1f;
        }

        /// <summary>
        /// Update transform Position 
        /// </summary>
        /// <param name="pos">input Position</param>
        /// <param name="deltaX">length of way to move</param>
        /// <returns>new Position</returns>
        public Vector3 UpdatePosition(Vector3 pos, float deltaX)
        {
            pos.x += dir * deltaX;
            if (endPos.x > pos.x)
            {
                dir = 1f;
                endReached = true;
            }

            if (startPos.x < pos.x)
            {
                dir = -1f;
                endReached = true;
            }

            return pos;
        }
    }
}