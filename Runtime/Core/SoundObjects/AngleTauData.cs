﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class AngleTauData
    {
        private readonly TransferFunction[] _tau;
        private readonly FloatArrays[] _tauSqrtTauComplex;
        private readonly float _radAngleResolution;
        private readonly float[] _anglesRad;

        public AngleTauData(int resolution)
        {
            _radAngleResolution = Mathf.PI / 2 / resolution;
            _tau = new TransferFunction[resolution];
            _tauSqrtTauComplex = new FloatArrays[resolution];
            _anglesRad = new float[resolution];
            var currentAngle = 0f;
            for (var i = 0; i < resolution; i++)
            {   
                _anglesRad[i] = currentAngle;
                currentAngle += _radAngleResolution;
            }
        }
        public AngleTauData(int resolution, SoundReductionIndex soundReductionIndex, Interpolator interpolator, int fiElementLength) :this(resolution)
        {
            for (var i = 0; i < resolution; i++)
            {   
                _tau[i] = soundReductionIndex
                    .CalcSoundReductionIndexAngle(_anglesRad[i],
                        BaSettings.Instance.GetInterpolatedFrequencyBand(fiElementLength / 2));
                _tauSqrtTauComplex[i] = new FloatArrays(interpolator.CalcSqrtTauComplex(_tau[i].Tf, fiElementLength));
            }
        }

        public float[] GetNearestTauSqrtTauComplexRad(float radAngle)
        {
            var index = (int)(radAngle/_radAngleResolution);
            if (Mathf.Abs(_anglesRad[index] - radAngle) > _radAngleResolution)
                Debug.LogError("Hallo");
            return _tauSqrtTauComplex[index].floats;
        }
        public void GetNearestTauSqrtTauComplexRad(float radAngle,float[] outArray)
        {
            var index = (int)(radAngle/_radAngleResolution);
            if (Mathf.Abs(_anglesRad[index] - radAngle) > _radAngleResolution)
                Debug.LogError("Hallo");
            if (_tauSqrtTauComplex[index].floats.Length!= outArray.Length)
                Debug.LogError("LengthMissmatch");
            Array.Copy(_tauSqrtTauComplex[index].floats,outArray,outArray.Length);
        }
    }
}