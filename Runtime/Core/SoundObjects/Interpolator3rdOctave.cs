using System;
using TestMySpline;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class Interpolator3rdOctave
    {
        private int _filterLength;
        private float[] _extrapolatedData;
        private float[] _extrapolatedDataFreqBand;
        private float[] _interpolationFreqBand;
        private float[] _result;
        

        public Interpolator3rdOctave(int filterLength)
        {
            Init(filterLength);
        }

        private void Init(int filterLength)
        {
            _filterLength = filterLength;
            _extrapolatedData = new float[40];
            _extrapolatedDataFreqBand = new[]
            {
                0.0f, 5.0f, 10.0f, 15.0f, 20.0f, 25.0f, 31.5f, 40.0f, 50.0f, 63.0f, 80.0f, 100.0f, 125.0f, 160.0f,
                200.0f, 250.0f, 315.0f, 400.0f, 500.0f, 630.0f, 800.0f, 1000.0f, 1250.0f, 1600.0f, 2000.0f, 2500.0f,
                3150.0f, 4000.0f, 5000.0f, 6300.0f, 8000.0f, 10000.0f, 12500.0f, 16000.0f, 20000.0f, 25000.0f, 30000.0f,
                35000.0f, 40000.0f, 45000.0f
            };
            _interpolationFreqBand = BaSettings.Instance.GetInterpolatedFrequencyBand(_filterLength).Tf;
            _result = new float[_filterLength];
        }
        public float[] Interpolate(float[] inputData)
        {
            if (inputData.Length != 31)
            {
                Debug.LogError("Problem");
            }

            // Extrapolation after Schröder
            for (uint i = 0; i < 4; i++)
            {
                _extrapolatedData[i] = inputData[0];
            }

            for (uint i = 35; i < 40; i++)
            {
                _extrapolatedData[i] = inputData[30];
            }

            for (uint i = 4; i < 35; i++)
            {
                _extrapolatedData[i] = inputData[i - 4];
            }


            // Cubic Spline Interpolation
            CubicSpline.Compute(_extrapolatedDataFreqBand, _extrapolatedData, _interpolationFreqBand, _result);

          

            return _result;
        }
        
    }
}