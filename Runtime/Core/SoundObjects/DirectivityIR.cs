﻿using System;
using System.IO;
using DAFF;
using log4net;
using UnityEditor;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace BA.BACore
{
    [CreateAssetMenu(fileName = "SourceDirectivity", menuName = "BACore/Runtime/SourceDirectivity", order = 1)]

    public class DirectivityIR : DirectivityDaff
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(DirectivityIR));
    
        public string directivityFileSource = "/BACore/Runtime/Sounds/Directivities/dirac_512.ir.daff";
        [Header("Plotting Settings")] 
    
        public float azimuth;
        public float elevation;
        private float[] irData;
        private IR irHandle;
        public override IDaff IrHandle
        {
            get
            {
                if (irHandle != null) return irHandle;
                InitDaffReader();

                return irHandle;
            }
        }

        public override AnimationCurve Curve
        {
            get
            {
                var index = IrHandle.GetNearestNeighbourRecordIndex(azimuth, elevation);
                irData = IrHandle.GetRecordData(index,0);

                var curve = new AnimationCurve();
                for (var i = 0; i < irData.Length; i++)
                {
                        curve.AddKey( i,irData[i]);
                }

                return curve;
            }
        }

        public override float Min => Mathf.Max(irData);
        public override float Max => Mathf.Min(irData);
        
        /// <summary>
        /// Init the Daff directivity Database for Source or Receiver (hrtf)
        /// </summary>
        /// <param name="nameDatabase"></param>
        public override void InitDaffReader()
        {
            var daffReader = new DAFFReader();

            var filePath = BaSettings.Instance.RootPath + directivityFileSource;
            UnityEngine.Debug.Log("[ DAFF ] Trying to load DAFF file " + filePath);
            if (!File.Exists(filePath))
                Log.Error("[ DAFF ] Could not find file." + filePath);
            if (!daffReader.Load(filePath))
                Log.Error("[ DAFF ] Could not load DAFF file " + filePath);
            else
                irHandle = daffReader.GetContentIR();
        }

    
    
     
    }
}
