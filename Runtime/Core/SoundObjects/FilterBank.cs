﻿using System;
using System.IO;
using DAFF;
using log4net;
using UnityEditor;
using UnityEngine;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;

namespace BA.BACore
{
    [CreateAssetMenu(fileName = "FilterBank", menuName = "VBA/FilterBank", order = 1)]

    public class FilterBank : ScriptableObject
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(FilterBank));
    
        public string directivityFileSource;
        [SerializeField] private int channels;
        [SerializeField] private int fs;
        [SerializeField] private int lenPerChannel;
        [SerializeField][HideInInspector] private float[] filter;
        [SerializeField][HideInInspector] private TransferFunction currentFreqBand;

        
        [SerializeField][HideInInspector] private FloatArrays[] filterbankFreqComplex;
        
        private FftRealtimeR2C fft;
        
        private IfftRealtimeC2R ifft;
        
        [SerializeField][HideInInspector] private FloatArrays[] fftSignalOutFreqComplex;
        
        [SerializeField][HideInInspector] private FloatArrays[] fftSignalResultFreqComplex;
        
        public void LoadFilter(int fftSize)
        {
            
            ReadData();

            switch (channels)
            {
                // Octave Band
                case 10:
                    currentFreqBand = BaSettings.Instance.OctaveFrequencyBand;
                    break;
                case 31:
                    currentFreqBand = BaSettings.Instance.ThirdOctaveFrequencyBandNorm;
                    break;
                default:
                    Debug.LogError("Invalid Length of Filter.", this);
                    break;
            }
            filterbankFreqComplex = new FloatArrays[channels];
            fftSignalOutFreqComplex = new FloatArrays[channels];
            fftSignalResultFreqComplex = new FloatArrays[channels];
            fft = new FftRealtimeR2C(fftSize);
            ifft = new IfftRealtimeC2R(fftSize);
            for (var iBand = 0; iBand < channels; ++iBand)
            {
                var filterTimeComplex = new float[fftSize * 2];
                for (var n = 0; n < lenPerChannel; n++)
                {
                    filterTimeComplex[2 * n] = filter[iBand * lenPerChannel + n];
                }
                
                filterbankFreqComplex[iBand] = new FloatArrays(fftSize * 2);
                fftSignalOutFreqComplex[iBand] = new FloatArrays(fftSize * 2);
                fftSignalResultFreqComplex[iBand] = new FloatArrays(fftSize * 2);
                fft.RunFft(filterTimeComplex,filterbankFreqComplex[iBand].floats);
            }
        }

        [ContextMenu("Read MetaData")]
        private void ReadData()
        {
            var filePath = BaSettings.Instance.RootPath + directivityFileSource;
            using (var br = new BinaryReader(File.Open(filePath, FileMode.Open)))
            {
                // read the doubles out of the byte buffer into the two dimensional array
                // note this assumes machine-endian byte order
                channels = br.ReadInt32();
                fs = br.ReadInt32();
                lenPerChannel = br.ReadInt32();
                filter = new float[lenPerChannel * channels];
                for (var i = 0; i < lenPerChannel * channels; i++)
                    filter[i] = br.ReadSingle();
            }
        }

        public float[] GetFilter(int indexFreq)
        {
            var filterOut = new float[lenPerChannel];
            filter.CopyTo(filterOut, indexFreq * lenPerChannel);
            return filter;
        }

        public void GetFilter(int indexFreq, float[] filterOut)
        {
            filter.CopyTo(filterOut, indexFreq * lenPerChannel);
        }

        public void GetFilteredSignal(float[] input, float centerFrequency, float[] output)
        {
            var index = GetFilterIndex(centerFrequency, currentFreqBand);
            fft.RunFftReal(input, fftSignalOutFreqComplex[index].floats);
            ComplexFloats.Multiply(fftSignalOutFreqComplex[index].floats,filterbankFreqComplex[index].floats,  fftSignalResultFreqComplex[index].floats);
            ifft.RunFftReal(fftSignalResultFreqComplex[index].floats, output);
        }
    
        private static int GetFilterIndex(float centerFrequency, TransferFunction freqBand)
        {
            var indexFreq = -1;
            for (var i = 0; i < freqBand.NumFrequency; i++)
            {
                if (Math.Abs(freqBand.Tf[i] - centerFrequency) > 1e-5) continue;
                indexFreq = i;
                break;
            }

            return indexFreq;
        }
        /// <summary>
        /// Write the assets to The Directivity IR files
        /// </summary>
        public static void CreateAssets()
        {
            var folder = $"{Application.dataPath}/../";
            var files = Directory.GetFiles(folder, "*.filter.bin", SearchOption.AllDirectories);
            foreach (var file in files)
            {
                var fullPath = new Uri(file, UriKind.Absolute);
                var relRoot = new Uri($"{Application.dataPath}/../", UriKind.Absolute);
                var relPathAsset =  relRoot.MakeRelativeUri(fullPath).ToString();
                var relRootDaff = new Uri($"{BaSettings.Instance.RootPath}/", UriKind.Absolute);
                var relPathDaff =  relRootDaff.MakeRelativeUri(fullPath).ToString();
                if (File.Exists($"{file}.asset"))
                {
                    Debug.Log($"{file}.asset already exists.");
                    
                }
                else
                {
                    var asset = ScriptableObject.CreateInstance<FilterBank>();
                    asset.directivityFileSource = "/" + relPathDaff;
                    var assetPathAndName =
                        AssetDatabase.GenerateUniqueAssetPath(relPathAsset + ".asset");
                    AssetDatabase.CreateAsset(asset, assetPathAndName);
                    AssetDatabase.SaveAssets();
                    AssetDatabase.Refresh();
                    EditorUtility.FocusProjectWindow();
                    Selection.activeObject = asset;
                    Debug.Log($"Assets{relPathAsset}.asset created.");
                }
            }
        }
    

    
        public static void DeleteAssets()
        {
            var allGuids=AssetDatabase.FindAssets("t:FilterBank");
            foreach (var guid in allGuids)
            {
                var asset = AssetDatabase.GUIDToAssetPath(guid);
                if(AssetDatabase.DeleteAsset(asset))
                    Debug.Log("Deleted: " + asset);
            }
        }

        [ContextMenu("Write Filter to JSON")]
        private void SaveAll()
        {
            File.WriteAllText("Filter.json", JsonUtility.ToJson(this, true));
        }
    }
}
