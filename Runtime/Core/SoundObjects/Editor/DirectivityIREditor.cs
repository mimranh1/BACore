﻿using BA.BACore;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(DirectivityIR))]
public class DirectivityIREditor : Editor
{    
    private DirectivityIR directivityIr;

    private void OnEnable()
    {
        directivityIr = (DirectivityIR) target;
    }
    
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        
        GUILayout.BeginHorizontal();
        EditorGUILayout.CurveField(directivityIr.Curve, GUILayout.MinHeight(100));
        MinMaxNextToCurve(directivityIr.Min, directivityIr.Max);
        GUILayout.EndHorizontal();
    }
    
    
    private void MinMaxNextToCurve(float min, float max)
    {
        GUILayout.BeginVertical();
        GUILayout.Label(max.ToString("0.00"),
            GUILayout.MinHeight(90), GUILayout.Width(50));
        GUILayout.Label(min.ToString("0.00"),
            GUILayout.MinHeight(10), GUILayout.Width(50));
        GUILayout.EndVertical();
    }

}
