using System.Globalization;
using BA.BACore;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SoundSourceMover))]
[System.Serializable]
public class WayPointsEditor : Editor
{
    private SoundSourceMover sourceMover;
    private GUIStyle boldStyle;

    private void OnEnable()
    {
        sourceMover = (SoundSourceMover) target;
    }

    /// <summary>
    /// overrides the Inspector view in the Editor mode
    /// </summary>
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Restart Position and Update Sounds")) sourceMover.RestartPositionAndSound();
    }
}