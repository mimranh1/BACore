﻿// using System.Globalization;
// using BA.BACore;
// using UnityEditor;
// using UnityEngine;
//
// namespace BACore.Scripts.Editor
// {
//     [CustomEditor(typeof(SoundSignalController))]
//     public class SoundSignalControllerEditor : UnityEditor.Editor
//     {
//         private SoundSignalController controller;
//         
//         private void OnEnable()
//         {
//             controller = (SoundSignalController) target;
//         }
//         
//         public override void OnInspectorGUI()
//         {
//             
//             base.OnInspectorGUI();
//             
//             if (GUILayout.Button("Awake"))
//                 controller.Awake();
//             
//             if (GUILayout.Button("Play Sound"))
//                 controller.StartSound();
//         }
//     }
// }