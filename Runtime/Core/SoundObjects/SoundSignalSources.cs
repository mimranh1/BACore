﻿using UnityEngine;

namespace BA.BACore
{
    public class SoundSignalSources : MonoBehaviour
    {
        public SoundSourceObject[] SoundSources
        {
            get => _soundSources;
            set => _soundSources = value;
        }

        [SerializeReference][DisplayProperties]
        private SoundSourceObject[] _soundSources;


    }
}
