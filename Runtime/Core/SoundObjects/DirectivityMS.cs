using System.IO;
using DAFF;
using log4net;
using UnityEngine;

namespace BA.BACore
{
    [CreateAssetMenu(fileName = "SourceDirectivity", menuName = "BACore/Runtime/SourceDirectivity", order = 1)]

    public class DirectivityMS : DirectivityDaff
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(DirectivityMS));
    
        public string directivityFileSource = "/BACore/Runtime/Sounds/Directivities/dirac_512.ir.daff";
        [Header("Plotting Settings")] 
    
        public float azimuth;
        public float elevation;
        private float[] msData;
        private MS irHandle;
        public override IDaff IrHandle
        {
            get
            {
                if (irHandle != null) return irHandle;
                InitDaffReader();

                return irHandle;
            }
        }

        public override AnimationCurve Curve
        {
            get
            {
                var index = IrHandle.GetNearestNeighbourRecordIndex(azimuth, elevation);
                msData = IrHandle.GetRecordData(index,0);

                var curve = new AnimationCurve();
                for (var i = 0; i < msData.Length; i++)
                {
                        curve.AddKey(i,msData[i]);
                }

                return curve;
            }
        }

        public override float Min => (Mathf.Max(msData));
        public override float Max => (Mathf.Min(msData));

        private Interpolator3rdOctave _interpolator3Rd;
        
        /// <summary>
        /// Init the Daff directivity Database for Source or Receiver (hrtf)
        /// </summary>
        /// <param name="nameDatabase"></param>
        public override void InitDaffReader()
        {
            var daffReader = new DAFFReader();

            var filePath = BaSettings.Instance.RootPath + directivityFileSource;
            UnityEngine.Debug.Log("[ DAFF ] Trying to load DAFF file " + filePath);
            if (!File.Exists(filePath))
                Log.Error("[ DAFF ] Could not find file." + filePath);
            if (!daffReader.Load(filePath))
                Log.Error("[ DAFF ] Could not load DAFF file " + filePath);
            else
                irHandle = daffReader.GetContentMS();
            directivityDataFunction=new float[irHandle.GetLength()];
            _interpolator3Rd=new Interpolator3rdOctave(BaSettings.Instance.filterLengthInSamples);
        }


        public void ApplyOnSpectrum(float azimuth, float elevation, float[] tau)
        {
            var filterLength = tau.Length;
            var inputData = directivityDataFunction;
            var sourceIndex = IrHandle.GetNearestNeighbourRecordIndex(azimuth, elevation);
            IrHandle.GetRecordData(directivityDataFunction, sourceIndex, 0);
            var output = _interpolator3Rd.Interpolate(inputData);
            for (int n = 0; n < filterLength; n++)
            {
                tau[n] *= output[n / 2];
            }
        }

      
    }
}