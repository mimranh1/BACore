﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace BA.BACore
{
    public class SoundSourceMover : MonoBehaviour {
     
        // put the points from unity interface
        public WayPaths wayPointList;
        public enum Mode
        {
            CircleMode,
            ResetAfterLastPoint,
            RestartSoundAfterLastPoint,
            ResetSoundAfterLastPoint,
            ManualStart,
        }

        public Mode loopingMode = Mode.CircleMode;
     
        public int currentWayPoint = 0;
    
        private Transform targetWayPoint;
        public float speedKmh = 50f;
        private float SpeedMps => speedKmh / 3.6f;
    
        public bool endReached;
    
        public Vector3 StartPos => wayPointList.wayPoints[0].position;
    
        public Vector3 DirectionForward => transform.forward;

        public Vector3 Position => transform.position;

        private bool started = false;
        private bool stopped;

        public float WayLength
        {
            get
            {
                var dist = 0f;
                for (var i = 0; i < wayPointList.wayPoints.Length - 1; i++)
                {
                    dist += Vector3.Distance(wayPointList.wayPoints[i].position, wayPointList.wayPoints[i + 1].position);
                }

                return dist;
            }
        }

        private AudioRender audioRenderer;
        private UnityEvent unityEvent;

        public void Init(AudioRender render)
        {
            audioRenderer = render;
        }

        // Update is called once per frame
        private void Update () {
            if (!started && Mode.ManualStart != loopingMode)
            {
                if (Mode.RestartSoundAfterLastPoint == loopingMode || 
                    Mode.ResetSoundAfterLastPoint == loopingMode)
                    RestartPositionAndSound();
                started = true;
            }

            if (!started) return;
        
            // check if we have somewere to walk
            if (currentWayPoint >= wayPointList.wayPoints.Length) return;
        
            if(targetWayPoint == null)
                targetWayPoint = wayPointList.wayPoints[currentWayPoint];
        
            Move();
        }

        private void Move()
        {
            if (stopped) return;
            // rotate towards the target
            var transform1 = transform;
            var position = transform1.position;
            var position1 = targetWayPoint.position;
            transform1.forward = Vector3.RotateTowards(transform1.forward, position1 - position,
                speedKmh / 3.6f * Time.deltaTime, 0.0f);

            // move towards the target
            position =
                Vector3.MoveTowards(position, position1, SpeedMps * Time.deltaTime);
            transform1.position = position;

            if (transform1.position != targetWayPoint.position) return;
        
            currentWayPoint++;
            var pathFinished = currentWayPoint >= wayPointList.wayPoints.Length;
            currentWayPoint %= wayPointList.wayPoints.Length;
            switch (loopingMode)
            {
                case Mode.CircleMode:
                    break;
                case Mode.ResetAfterLastPoint:
                    if (currentWayPoint == 0)
                        ResetPosition();
                    break;

                case Mode.RestartSoundAfterLastPoint:
                    if (currentWayPoint == 0)
                        RestartPositionAndSound();
                    break;      
                case Mode.ResetSoundAfterLastPoint:
                    if (currentWayPoint == 0)
                        ResetPositionAndSound();
                    break; 
                case Mode.ManualStart:
                    if (currentWayPoint == 0)
                        stopped = true;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            
            if (pathFinished && unityEvent != null)
            {
                unityEvent.Invoke();
                unityEvent = null;
            }

            targetWayPoint = wayPointList.wayPoints[currentWayPoint];
        }

        private void ResetPosition(int positionId=0)
        {
            transform.position = wayPointList.wayPoints[positionId].position;
            targetWayPoint = wayPointList.wayPoints[positionId + 1];
            currentWayPoint = positionId + 1;
        }

        public void ResetPositionAndSound()
        {
            ResetPosition();
            if (audioRenderer!=null)
            {
                audioRenderer.ResetSounds();
                Debug.Log("Sound Position Reset.");
            }

            stopped = false;
        }
    
        public void StartPositionAndSound()
        {
            ResetPosition();
            if (audioRenderer!=null)
            {
                audioRenderer.PlaySounds();
                Debug.Log("Sound Position Reset.");
            }
            stopped = false;
        }
        
    
        public void RestartPositionAndSound(UnityEvent sourceReachedEvent=null, UnityEvent sourceStoppedPlaying=null)
        {
            this.unityEvent = sourceReachedEvent;
            started = true;
            ResetPosition();
            if (audioRenderer!=null)
            {
                var maxAudioLength = audioRenderer.ResetAndPlay();
                if (sourceStoppedPlaying != null)
                    StartCoroutine(ExecuteAfterTime(maxAudioLength + 1f, sourceStoppedPlaying));
                Debug.Log("Sound Position Reset.");
            }
            stopped = false;
        }
    
    
        IEnumerator ExecuteAfterTime(float time,UnityEvent soundStopped)
        {
            yield return new WaitForSeconds(time);
        
            // Code to execute after the delay
            soundStopped.Invoke();
            Debug.Log("Sound Source have stopped playing.");
        }

        public Vector3 UpdatePosition(Vector3 transformPosition, float deltaX)
        {
            return transformPosition + deltaX * Vector3.right;
        }
    }
}