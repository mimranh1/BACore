using System;
using System.IO;
using DAFF;
using UnityEditor;
using UnityEngine;

namespace BA.BACore
{
    public abstract class DirectivityDaff :ScriptableObject
    {
        public abstract AnimationCurve Curve { get; }
        public abstract float Min { get; }
        public abstract float Max { get; }
        
        public float[] directivityDataFunction = new float[2];
        public abstract IDaff IrHandle { get; }

        public virtual void InitDaffReader()
        {
            if (directivityDataFunction.Length != IrHandle.GetLength())
                directivityDataFunction = new float[IrHandle.GetLength()];
        }

        public void GetRecordData(float azimuth, float elevation, int channel = 0)
        {
            var sourceIndex = IrHandle.GetNearestNeighbourRecordIndex(azimuth, elevation);
            IrHandle.GetRecordData(directivityDataFunction, sourceIndex, 0);
        }

        public void GetAnglesForDirectivityAndIncidenceAngle(Vector3 wallPosition, Vector3 wallUp,
            Vector3 soundPos, Quaternion soundRotation, out float azimuth, out float elevation, out float cosIncidenceAngle, Vector3 forward= default(Vector3))
        {
            if(forward==Vector3.zero)
                forward=Vector3.back;
            var refView = soundRotation * forward;
            var refUp = soundRotation * Vector3.up;
            // var diffVector = ssWalls[i].transformObj.position - transformObj.parent.position;
            var diffVectorNorm = (soundPos - wallPosition);
            diffVectorNorm /= Mathf.Sqrt((diffVectorNorm.x * diffVectorNorm.x) + 
                                         (diffVectorNorm.y * diffVectorNorm.y) +
                                         (diffVectorNorm.z * diffVectorNorm.z));
            Vector3 wLocal;
            wLocal.x = Vector3.Dot(diffVectorNorm, Vector3.Cross(refView, refUp));
            wLocal.y = Vector3.Dot(diffVectorNorm, refUp);
            wLocal.z = Vector3.Dot(diffVectorNorm, refView);

            azimuth = Mathf.Atan2(-wLocal.x, -wLocal.z) * 180.0f / Mathf.PI;
            elevation = Mathf.Asin(wLocal.y) * 180.0f / Mathf.PI;
            cosIncidenceAngle = Mathf.Abs(Vector3.Dot(diffVectorNorm, wallUp));
        }
        
        public void GetAnglesForDirectivity(Vector3 wallPosition, Vector3 soundPos, Quaternion currentRotation,
            out float azimuth, out float elevation)
        {
            // var viewAngle = Vector3.back;
            var viewAngle = Vector3.forward;
            var refView = currentRotation * viewAngle;
            var refUp = currentRotation * Vector3.up;
            // var diffVector = ssWalls[i].transform.position - transform.parent.position;
            var diffVector = wallPosition - soundPos;
            Vector3 wLocal;
            wLocal.x = Vector3.Dot(diffVector, Vector3.Cross(refView, refUp));
            wLocal.y = Vector3.Dot(diffVector, refUp);
            wLocal.z = Vector3.Dot(diffVector, -refView);

            azimuth = Mathf.Atan2(-wLocal.x, -wLocal.z) * Mathf.Rad2Deg;
            elevation = Mathf.Asin(Vector3.Normalize(wLocal).y) * Mathf.Rad2Deg;
        }

        public void GetAngles(Vector3 pos, Vector3 view, Vector3 up, Vector3 targetPos, out float azimuthDeg,
            out float elevationDeg)
        {
            var directionG = targetPos - pos;

            var xDash = Vector3.Cross(view, up);
            var yDash = up;
            var zDash = -view;

            var wLocalX = Vector3.Dot(directionG, xDash);
            var wLocalY = Vector3.Dot(directionG, yDash);
            var wLocalZ = Vector3.Dot(directionG, zDash);
            var wLocal = new Vector3(wLocalX, wLocalY, wLocalZ);
            azimuthDeg = Mathf.Atan2(-wLocalX, -wLocalZ) * Mathf.Rad2Deg;

            var wLocalYNormalized =
                wLocalY / Mathf.Sqrt((wLocalX * wLocalX) + (wLocalY * wLocalY) + (wLocalZ * wLocalZ));
            elevationDeg = Mathf.Asin(wLocalYNormalized) * Mathf.Rad2Deg;

        }

        /// <summary>
        /// Write the assets to The Directivity IR files
        /// </summary>
        public static void CreateAssets()
        {
            var folder = $"{Application.dataPath}/../";
            CreateDirectivityMS(folder);
            CreateDirectivityIR(folder);
        }

        private static void CreateDirectivityMS(string folder)
        {
            var files = Directory.GetFiles(folder, "*.ms.daff", SearchOption.AllDirectories);
            foreach (var file in files)
            {
                var fullPath = new Uri(file, UriKind.Absolute);
                var relRoot = new Uri($"{Application.dataPath}/../", UriKind.Absolute);
                var relPathAsset = relRoot.MakeRelativeUri(fullPath).ToString();
                var relRootDaff = new Uri($"{BaSettings.Instance.RootPath}/", UriKind.Absolute);
                var relPathDaff = relRootDaff.MakeRelativeUri(fullPath).ToString();
                if (File.Exists($"{file}.asset"))
                {
                    Debug.Log($"{file}.asset already exists.");
                }
                else
                {
                    var asset = ScriptableObject.CreateInstance<DirectivityMS>();
                    asset.directivityFileSource = "/" + relPathDaff;
                    var assetPathAndName =
                        AssetDatabase.GenerateUniqueAssetPath(relPathAsset + ".asset");
                    AssetDatabase.CreateAsset(asset, assetPathAndName);
                    AssetDatabase.SaveAssets();
                    AssetDatabase.Refresh();
                    EditorUtility.FocusProjectWindow();
                    Selection.activeObject = asset;
                    Debug.Log($"Assets{relPathAsset}.asset created.");
                }
            }
        }   private static void CreateDirectivityIR(string folder)
        {
            var files = Directory.GetFiles(folder, "*.ir.daff", SearchOption.AllDirectories);
            foreach (var file in files)
            {
                var fullPath = new Uri(file, UriKind.Absolute);
                var relRoot = new Uri($"{Application.dataPath}/../", UriKind.Absolute);
                var relPathAsset = relRoot.MakeRelativeUri(fullPath).ToString();
                var relRootDaff = new Uri($"{BaSettings.Instance.RootPath}/", UriKind.Absolute);
                var relPathDaff = relRootDaff.MakeRelativeUri(fullPath).ToString();
                if (File.Exists($"{file}.asset"))
                {
                    Debug.Log($"{file}.asset already exists.");
                }
                else
                {
                    var asset = ScriptableObject.CreateInstance<DirectivityIR>();
                    asset.directivityFileSource = "/" + relPathDaff;
                    var assetPathAndName =
                        AssetDatabase.GenerateUniqueAssetPath(relPathAsset + ".asset");
                    AssetDatabase.CreateAsset(asset, assetPathAndName);
                    AssetDatabase.SaveAssets();
                    AssetDatabase.Refresh();
                    EditorUtility.FocusProjectWindow();
                    Selection.activeObject = asset;
                    Debug.Log($"Assets{relPathAsset}.asset created.");
                }
            }
        }

        public static void DeleteAssets()
        {
            DeleteDirectivityIr();
            DeleteDirectivityMs();
        }

        private static void DeleteDirectivityIr()
        {
            var allGuids = AssetDatabase.FindAssets("t:DirectivityIR");
            foreach (var guid in allGuids)
            {
                var asset = AssetDatabase.GUIDToAssetPath(guid);
                if (AssetDatabase.DeleteAsset(asset))
                    Debug.Log("Deleted: " + asset);
            }
        }
        private static void DeleteDirectivityMs()
        {
            var allGuids = AssetDatabase.FindAssets("t:DirectivityMS");
            foreach (var guid in allGuids)
            {
                var asset = AssetDatabase.GUIDToAssetPath(guid);
                if (AssetDatabase.DeleteAsset(asset))
                    Debug.Log("Deleted: " + asset);
            }
        }
    }
}