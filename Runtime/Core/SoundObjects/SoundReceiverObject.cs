using System;
using System.IO;
using System.Runtime.InteropServices;
using DAFF;
using FFTWSharp;
using log4net;
using UnityEngine;
using UnityEngine.Serialization;
using Object = UnityEngine.Object;

namespace BA.BACore
{
    [Serializable]
    public class SoundReceiverObject : SoundObject
    {
        public DirectivityIR hrir;
        private static readonly ILog Log = LogManager.GetLogger(typeof(SoundReceiverObject));
        private string _logName;
        private IDaff IrHandle=>hrir.IrHandle;
        private readonly int[] _hrirId = {-1, -1, -1, -1, 1};
        private int _hrirCounter = 0;

        private int _filterLength = 0;
        private float[] _directivityDataFunction = new float[2];
        private float[] _iDataComplexTime = new float[2];
        private float[] _directivityDataBinauralLeft = new float[2];
        private float[] _directivityDataBinauralRight = new float[2];

       
        private FftRealtimeR2C _fftOut = new FftRealtimeR2C(44100);
        public int Length => IrHandle.GetLength();
        
        private void OnEnable()
        {
            _logName = Path.GetFileNameWithoutExtension(hrir.directivityFileSource);
        }

        public void Init(int filterLength=-1)
        {
            _filterLength = filterLength;
            hrir.InitDaffReader();
            _logName = Path.GetFileNameWithoutExtension(hrir.directivityFileSource);
            if (filterLength <= 0) return;
            InitFft(filterLength);
        }

  


        /// <summary>
        /// Get all Hrtfs sum for each Wall or patch
        /// </summary>
        /// <param name="isTimeDomain"></param>
        /// <param name="hrtfElements">out reference hrtf</param>
        /// <param name="positionElements">positions of elements with its secondary sources</param>
        /// <param name="distanceFactor"></param>
        /// <param name="distanceDelay"></param>
        public void GetBinauralDirectivity(bool isTimeDomain,FloatArrays[] hrtfElements, Vector3[] positionElements, float factor=1f, bool distanceFactor = false,
            bool distanceDelay = false)
        {
            var transform1 = transformObj;
            var soundPos = transform1.position;
            var soundRot = transform1.rotation;
            if (_filterLength != hrtfElements[0].Length / 2)
                InitFft(hrtfElements[0].Length / 2);
                
            for (var i = 0; i < positionElements.Length; i++)
            {
                GetBinauralDirectivity(isTimeDomain,hrtfElements, positionElements[i], soundPos, soundRot, i, factor,distanceFactor, distanceDelay);
            }
        }
        /// <summary>
        /// Get all Hrtfs sum for each Wall or patch
        /// </summary>
        /// <param name="isTimeDomain"></param>
        /// <param name="hrtfElements">out reference hrtf</param>
        /// <param name="positionElements">positions of elements with its secondary sources</param>
        /// <param name="distanceFactor"></param>
        /// <param name="distanceDelay"></param>
        public void GetBinauralDirectivity(bool isTimeDomain,float[][] hrtfElements, Vector3[] positionElements, float factor=1f, bool distanceFactor = false,
            bool distanceDelay = false)
        {
            var transform1 = transformObj;
            var soundPos = transform1.position;
            var soundRot = transform1.rotation;
            for (var i = 0; i < positionElements.Length; i++)
            {
                GetBinauralDirectivity(isTimeDomain,hrtfElements, positionElements[i], soundPos, soundRot, i,factor, distanceFactor, distanceDelay);
            }
        }

        public void GetBinauralDirectivitySum(bool isTimeDomain, float[] hrtfElementsLeft, float[] hrtfElementsRight, Vector3[] positionElements,float factor=1f,
            bool distanceFactor = false, bool distanceDelay = false)
        {
            var transform1 = transformObj;
            var soundPos = transform1.position;
            var soundRot = transform1.rotation;
            var filterLength = hrtfElementsLeft.Length/2;
            if(_directivityDataBinauralLeft.Length!=filterLength)
             _directivityDataBinauralLeft = new float[filterLength];
            if(_directivityDataBinauralRight.Length!=filterLength)
                _directivityDataBinauralRight = new float[filterLength];
            foreach (var position in positionElements)
            {
                var distance = Vector3.Distance(position, soundPos);
                var distanceSamples = distanceDelay
                    ? Mathf.Min((int) (distance * SamplesByMeter), filterLength - IrHandle.GetLength())
                    : 0;
                if (!distanceFactor) distance = 1f;
                GetHrirChannel(position, soundPos, soundRot, _directivityDataFunction, 0);
                for (var n = 0; n < _directivityDataFunction.Length; n++)
                    _directivityDataBinauralLeft[n + distanceSamples] +=
                        factor * _directivityDataFunction[n] / distance;
                GetHrirChannel(position, soundPos, soundRot, _directivityDataFunction, 1);
                for (var n = 0; n < _directivityDataFunction.Length; n++)
                    _directivityDataBinauralRight[n + distanceSamples] +=
                        factor * _directivityDataFunction[n] / distance;
            }

            if (isTimeDomain)
            {
                _directivityDataBinauralLeft.CopyTo(hrtfElementsLeft, 0);
                _directivityDataBinauralRight.CopyTo(hrtfElementsRight, 0);
            }
            else
            {
                _fftOut.RunFftReal(_directivityDataBinauralLeft, hrtfElementsLeft);
                _fftOut.RunFftReal(_directivityDataBinauralRight, hrtfElementsRight);
            }
            Array.Clear(_directivityDataBinauralLeft,0,_directivityDataBinauralLeft.Length);
            Array.Clear(_directivityDataBinauralRight,0,_directivityDataBinauralRight.Length);
        }

        /// <summary>
        /// Get all Hrtfs sum for each Wall or patch
        /// </summary>
        /// <param name="isTimeDomain"></param>
        /// <param name="hrtfElements">out reference hrtf</param>
        /// <param name="positionElement">positions of elements with its secondary sources</param>
        /// <param name="distanceFactor"></param>
        /// <param name="distanceDelay"></param>
        public int  GetBinauralDirectivity(bool isTimeDomain,FloatArrays[] hrtfElements, Vector3 positionElement, float factor=1f, bool distanceFactor=false,bool distanceDelay=false)
        {
            var transform1 = transformObj;
            var soundPos = transform1.position;
            var soundRot = transform1.rotation;

           return GetBinauralDirectivity(isTimeDomain,hrtfElements, positionElement, soundPos, soundRot, 0,factor, distanceFactor, distanceDelay);
        }
        /// <summary>
        /// Get all Hrtfs sum for each Wall or patch
        /// </summary>
        /// <param name="isTimeDomain"></param>
        /// <param name="hrtfElements">out reference hrtf</param>
        /// <param name="positionElement">positions of elements with its secondary sources</param>
        /// <param name="distanceFactor"></param>
        /// <param name="distanceDelay"></param>
        public int[]  GetBinauralDirectivity(bool isTimeDomain,float[][] hrtfElements, Vector3 positionElement, float factor=1f, bool distanceFactor=false,bool distanceDelay=false)
        {
            var transform1 = transformObj;
            var soundPos = transform1.position;
            var soundRot = transform1.rotation;

           return GetBinauralDirectivity(isTimeDomain,hrtfElements, positionElement, soundPos, soundRot, 0, factor,distanceFactor, distanceDelay);
        }


        private int GetBinauralDirectivity(bool isTimeDomain,FloatArrays[] hrtfElements, Vector3 positionElement, Vector3 soundPos, Quaternion soundRot,
            int id = 0, float factor=1f, bool distanceFactor = false, bool distanceDelay = false)
        {
            var distance = Vector3.Distance(positionElement, soundPos);
            var distanceSamples = distanceDelay
                ? Mathf.Min((int) (distance * SamplesByMeter), hrtfElements[0].Length - IrHandle.GetLength())
                : 0;
            if (!distanceFactor) distance = 1f;

            if ((2 * id) >= hrtfElements.Length)
                Debug.LogError("hrtfElements length not valid");

            if(isTimeDomain)
            {
                // Left Channel
                GetHrirChannel(positionElement, soundPos, soundRot, hrtfElements[2 * id].floats, 0,factor, distanceSamples, distance);

                // Right Channel
                GetHrirChannel(positionElement, soundPos, soundRot, hrtfElements[2 * id + 1].floats, 1, factor,distanceSamples,
                    distance);
            }
            else
            {
                // Left Channel
                GetHrtfChannel(positionElement, soundPos, soundRot, hrtfElements[2 * id].floats, 0, factor,distanceSamples, distance);

                // Right Channel
                GetHrtfChannel(positionElement, soundPos, soundRot, hrtfElements[2 * id + 1].floats, 1, factor,distanceSamples,
                    distance);
            }

            return distanceSamples;
        }

        private int[] GetBinauralDirectivity(bool isTimeDomain,float[][] hrtfElements, Vector3 positionElement, Vector3 soundPos, Quaternion soundRot,
            int id = 0, float factor=1f, bool distanceFactor = false, bool distanceDelay = false)
        {
            var distance = Vector3.Distance(positionElement, soundPos);
            var distanceSamples = distanceDelay
                ? Mathf.Min((int) (distance * SamplesByMeter), hrtfElements[0].Length - IrHandle.GetLength())
                : 0;
            if (!distanceFactor) distance = 1f;

            if ((2 * id) >= hrtfElements.Length)
                Debug.LogError("hrtfElements length not valid");

            if(isTimeDomain)
            {
                // Left Channel
                GetHrirChannel(positionElement, soundPos, soundRot, hrtfElements[2 * id], 0, factor,distanceSamples, distance);

                // Right Channel
                GetHrirChannel(positionElement, soundPos, soundRot, hrtfElements[2 * id + 1], 1, factor,distanceSamples,
                    distance);
            }
            else
            {
                // Left Channel
                GetHrtfChannel(positionElement, soundPos, soundRot, hrtfElements[2 * id], 0, factor,distanceSamples, distance);

                // Right Channel
                GetHrtfChannel(positionElement, soundPos, soundRot, hrtfElements[2 * id + 1], 1, factor,distanceSamples,
                    distance);
            }

            return new[] {distanceSamples, IrHandle.GetLength()};
        }


        private void GetHrirChannel(Vector3 positionElements, Vector3 soundPos, Quaternion soundRot,
            float[] hrirElement, int channelId, float factor=1f,int distanceSamples = 0, float distance = 1f)
        {
            if (_directivityDataFunction.Length != IrHandle.GetLength())
                _directivityDataFunction = new float[IrHandle.GetLength()];

            GetHrirDirect(positionElements, _directivityDataFunction,soundPos, soundRot, channelId);
            var constFactor = factor / distance;
            for (var i = 0; i < _directivityDataFunction.Length; i++)
            {
                hrirElement[i + distanceSamples] = _directivityDataFunction[i] * constFactor;
            }
        }

        public void GetHrirDirect(Vector3 positionElements, float[] outArray, int channelId)
        {
            var transform1 = transformObj;
            var soundPos = transform1.position;
            var soundRot = transform1.rotation;
            GetHrirDirect(positionElements, outArray, soundPos, soundRot, channelId);
        }

        private void GetHrirDirect(Vector3 positionElements, float[] outArray,Vector3 soundPos, Quaternion soundRot, int channelId)
        {
            var hrirI = GetHrirIndex(positionElements, soundPos, soundRot);
            IrHandle.GetRecordData(outArray , hrirI, channelId);
        }

        private void GetHrtfChannel(Vector3 positionElements, Vector3 soundPos, Quaternion soundRot,
            float[] hrirElement, int channelId, float factor=1f,int distanceSamples = 0, float distance = 1f)
        {
            var hrirI = GetHrirIndex(positionElements, soundPos, soundRot);
            IrHandle.GetRecordData(_directivityDataFunction, hrirI, channelId);
            var constFactor = factor / distance;
            if (distanceSamples == 0 && Math.Abs(distance - 1f) < float.Epsilon)
            {
                _fftOut.RunFftReal(_directivityDataFunction, hrirElement);
            }
            else
            {
                Array.Clear(_iDataComplexTime, 0, _iDataComplexTime.Length);
                for (var i = 0; i < Mathf.Min(_directivityDataFunction.Length,
                    (_iDataComplexTime.Length - distanceSamples)/2); i++)
                    _iDataComplexTime[2*(i + distanceSamples)] = _directivityDataFunction[i] * constFactor;

                _fftOut.RunFft(_iDataComplexTime, hrirElement);
            }
        }

        private void InitFft(int lengthHrtf)
        {
            _filterLength = lengthHrtf;
            if (_directivityDataFunction.Length != IrHandle.GetLength())
                _directivityDataFunction = new float[IrHandle.GetLength()];
            if (_iDataComplexTime.Length != 2*lengthHrtf)
                _iDataComplexTime = new float[2*lengthHrtf];
            if (_fftOut.Length/2 != lengthHrtf)
                _fftOut = new FftRealtimeR2C(lengthHrtf);
        }

        private int GetHrirIndex(Vector3 wallPosition, Vector3 soundPos, Quaternion currentRotation, string logTxt=null)
        {
            GetAnglesForDirectivity(wallPosition, soundPos, currentRotation, out var azimuth, out var elevation);
            if (logTxt != null)
                Debug.Log(logTxt + ": azimuth " + azimuth + "  elevation " + elevation);
            return IrHandle.GetNearestNeighbourRecordIndex(azimuth, elevation);
        }


        private void GetAnglesForDirectivity(Vector3 wallPosition, Vector3 soundPos, Quaternion currentRotation,
            out float azimuth, out float elevation)
        {
            // var viewAngle = Vector3.back;
            var viewAngle = Vector3.back;
            var refView = currentRotation * viewAngle;
            var refUp = currentRotation * Vector3.up;
            // var diffVector = ssWalls[i].transform.position - transform.parent.position;
            var diffVector = wallPosition - soundPos;
            Vector3 wLocal;
            wLocal.x = Vector3.Dot(diffVector, Vector3.Cross(refView, refUp));
            wLocal.y = Vector3.Dot(diffVector, refUp);
            wLocal.z = Vector3.Dot(diffVector, refView);

            azimuth = Mathf.Atan2(-wLocal.x, -wLocal.z) * 180.0f / Mathf.PI;
            elevation = Mathf.Asin(Vector3.Normalize(wLocal).y) * 180.0f / Mathf.PI;
        }


        /// <summary>
        ///     calculate Azimuth and Elevation according to Position of Receiver and Source and Rotation of Receiver
        /// </summary>
        /// <returns>true if irHandle id changes</returns>
        public bool UpdateHrirIndex(Vector3[] positions)
        {
            var breturn = true;
            var transform1 = transformObj;
            var soundPos = transform1.position;
            var soundRot = transform1.rotation;
            for (var i = 0; i < positions.Length; i++)
            {
                var wallPosition = positions[i];
                var oldHrirId = _hrirId[i];
                if (IrHandle != null)
                    _hrirId[i] = GetHrirIndex(wallPosition, soundPos, soundRot);
                else
                    Log.Error("irHandle not init");

                if (_hrirId[i] != oldHrirId) continue;
                breturn = false;
                break;
            }

            DataLogger.Log(_logName + "position{1," + _hrirCounter + "}", soundPos);
            return breturn;
        }
        
        private void OnDrawGizmosSelected()
        {
            if(transformObj!=null)
            {
                Gizmos.color = Color.red;
                var position = transformObj.position;
                Gizmos.DrawSphere(position, 0.3f);
                Gizmos.DrawRay(position, -2f*transformObj.forward);
                Gizmos.color = Color.blue;
                Gizmos.DrawRay(position, 2f*transformObj.up);
            }        
        }
    }
}