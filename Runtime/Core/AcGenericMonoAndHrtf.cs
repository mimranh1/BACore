﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using FFTWSharp;
using log4net;
using UnityEngine;
using Object = UnityEngine.Object;

namespace BA.BACore
{
    /// <summary>
    /// Implementation of new Approach Outdoor considering Direct and reverberation in Source and Receiver room
    /// First try to bake the results beforehand so that the moving secondary source is baked up to the secondary source of the patch in the receiver room
    /// not working well
    /// This class just run with VA and therefore it has an own renderer and deactivate the default one
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    [Serializable]
    public class AcGenericMonoAndHrtf : AcGeneric
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(AcGenericMonoAndHrtf));
        public override AuralisationCalculationType Type => AuralisationCalculationType.GenericModelMonoAndHrtf;

        [SerializeField] public AudioLoaderFft audioLoaderFft;


        [SerializeField] public string filterPath =
            "C:/Users/anne/Mimcloud/LiverPool/sound source/1. Convolved_binaural/impulse_responses";

        private float[] irLeftFreq;
        private float[] irRightFreq;

        private GCHandle ifftInputLeftHandle;
        private GCHandle ifftResultLeftHandle;
        private IntPtr ifftPlanLeft;


        // IFFT Right
        private GCHandle ifftInputRightHandle;
        private GCHandle ifftResultRightHandle;
        private IntPtr ifftPlanRight;

        private float[] irReceiverRoomFreq;


        public override void OnValidate()
        {
            
        }

        /// <summary>
        /// Overload of Initialisation with Init BuildingAcoustics Array from FilterManager
        /// </summary>
        protected override void Init()
        {
            CultureInfo.CreateSpecificCulture("en-GB");
            init = true;
            audioLoaderFft = Object.FindObjectOfType<AudioLoaderFft>();
            // sourceMover = buildingAcoustics?[0].sourceRoom.soundObject.GetComponent<SourceMover>();
        }


        /// <summary>
        /// Implementation of Calculate pre-calculation and and set AudioRenderer
        /// </summary>
        /// <param name="ar">AudioRenderer from FilterManager</param>
        protected override void OfflineCalculation()
        {
            base.OfflineCalculation();

            irRightFreq = new float[FilterResolution * 2];
            irLeftFreq = new float[FilterResolution * 2];
            InitIfftIr();
            DefineAudioFilters();
            irReceiverRoomFreq = new float[FilterResolution * 2];
        }


        /// <summary>
        /// Implementation of Realtime Update, called if Source or Receiver Updates
        /// </summary>
        /// <param name="logId">write log file if logId > 1</param>
        protected override void RealTimeCalculation(AudioRender audioRender,bool log=false)
        {
            var sw = new Stopwatch();
            sw.Start();
            UpdateFilter(audioRender);
            sw.Stop();
            FilterUpdate = sw.Elapsed.TotalMilliseconds;
        }

        /// <summary>
        /// Implementation of Update Receiver Data for filter Construction if Receiver Update
        /// </summary>
        /// <returns>Receiver Update</returns>
        protected override bool RealTimeReceiverUpdate()
        {
            var posUpdate = false;
            if (SoundReceiverObject.SoundObjectChanged() ||
                soundSourceObject.SoundObjectChanged() | ForceReceiverUpdate | ForceReceiverUpdate)
            {
                var sw = new Stopwatch();
                sw.Start();
                var sourcePosition = soundSourceObject.transform.position;
                
                    SoundReceiverObject.GetBinauralDirectivity(false,RTReceiver, sourcePosition);
                    audioLoaderFft.GetFilterFreq(irReceiverRoomFreq, sourcePosition, soundReceiverObject.transform.position, 0);
                    posUpdate = true;

                sw.Stop();
                ReceiverUpdate = sw.Elapsed.TotalMilliseconds;
            }

            ForceReceiverUpdate = false;
            return posUpdate;
        }

        /// <summary>
        /// Implementation of Update Source Data for filter Construction if Source Update
        /// </summary>
        /// <returns>Source Update</returns>
        protected override bool RealTimeSourceUpdate()
        {
            return false;
        }

        public override void SaveImpulseResponse(string name="")
        {
            throw new NotImplementedException();
        }


        public void DefineAudioFilters()
        {
            if (!Directory.Exists(filterPath))
                //error
                return;

            audioLoaderFft.filterPath = filterPath;
            audioLoaderFft.filterLength = FilterResolution;
            audioLoaderFft.ReadAudioMonoFreq();
        }

        private void UpdateFilter(AudioRender ar)
        {
            if (!audioLoaderFft.allLoaded)
                return;
            for (var iBa = 0; iBa < SoundSourceObjects.Length; iBa++)
            {
                var rtSourceRef = new float[FilterResolution];
                var rtReceiverLeftRef = RTReceiver[0];
                var rtReceiverRightRef = RTReceiver[1];
                for (var n = 0; n < rtSourceRef.Length; n += 2)
                {
                    var rtLReal = rtReceiverLeftRef[n];
                    var rtLImag = rtReceiverLeftRef[n + 1];
                    var rtRReal = rtReceiverRightRef[n];
                    var rtRImag = rtReceiverRightRef[n + 1];

                    var monoReal = irReceiverRoomFreq[n];
                    var monoImag = irReceiverRoomFreq[n + 1];

                    if (iBa == 0)
                    {
                        irLeftFreq[n] = monoReal * rtLReal - monoImag * rtLImag;
                        irLeftFreq[n + 1] = monoReal * rtLImag + monoImag * rtLReal;
                        irRightFreq[n] = monoReal * rtRReal - monoImag * rtRImag;
                        irRightFreq[n + 1] = monoReal * rtRImag + monoImag * rtRReal;
                    }
                    else
                    {
                        irLeftFreq[n] += monoReal * rtLReal - monoImag * rtLImag;
                        irLeftFreq[n + 1] += monoReal * rtLImag + monoImag * rtLReal;
                        irRightFreq[n] += monoReal * rtRReal - monoImag * rtRImag;
                        irRightFreq[n + 1] += monoReal * rtRImag + monoImag * rtRReal;
                    }
                }
            }

            fftwf.execute(ifftPlanLeft);
            fftwf.execute(ifftPlanRight);
            ar.UpdateFilter(IrLeftTime, IrRightTime);
            if (LogRealtime)
            {
                DataLogger.DebugData = true;
                DataLogger.Log(LogName + ".irLeftTime(:," + HrirCounter + ")", IrLeftTime);
                DataLogger.Log(LogName + ".irRightTime(:," + HrirCounter + ")", IrRightTime);
                DataLogger.DebugData = false;
            }
        }

        private void InitIfftIr()
        {
            ifftInputLeftHandle = GCHandle.Alloc(irLeftFreq, GCHandleType.Pinned);
            ifftResultLeftHandle = GCHandle.Alloc(IrLeftTime, GCHandleType.Pinned);
            ifftPlanLeft = fftwf.dft_c2r_1d(FilterResolution, ifftInputLeftHandle.AddrOfPinnedObject(),
                ifftResultLeftHandle.AddrOfPinnedObject(), fftw_flags.Estimate);

            ifftInputRightHandle = GCHandle.Alloc(irRightFreq, GCHandleType.Pinned);
            ifftResultRightHandle = GCHandle.Alloc(IrRightTime, GCHandleType.Pinned);
            ifftPlanRight = fftwf.dft_c2r_1d(FilterResolution, ifftInputRightHandle.AddrOfPinnedObject(),
                ifftResultRightHandle.AddrOfPinnedObject(), fftw_flags.Estimate);
        }

        private string Vec32Str(Vector3 vec)
        {
            CultureInfo.CurrentCulture = new CultureInfo("en-GB");
            return vec.x.ToString("000.0") + "_" + vec.y.ToString("000.0") + "_" + vec.z.ToString("000.0");
        }
    }
}