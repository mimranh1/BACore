﻿namespace BA.BACore
{
    public class WallAbsorptionDatabase: WallAbsorption
    {
        public override float AbsorptionCoefficient => -1f;
        public override TransferFunction AbsorptionCoefficientTransferFunction => new TransferFunction(21);
    }
}