﻿namespace BA.BACore
{
    public enum RoomAcousticPropertiesTypes
    {
        Empty,
        Eyring,
        Sabine,
        AutoGeometrySabine,
        Manual,
        ManualDimensions
    }
}