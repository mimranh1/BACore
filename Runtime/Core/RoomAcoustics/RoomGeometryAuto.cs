﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace BA.BACore
{
    [Serializable]
    public class RoomGeometryAuto : RoomGeometry
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(RoomGeometryAuto));
        public Transform position;
        public override RoomGeometryType Type => RoomGeometryType.Auto;
        public override WallBehaviour[] WallBehaviours => wallBehaviours;
        public override bool[] IsUsedForTau => isUsedForTau;
        /// <summary>
        /// List of all Walls in the room
        /// </summary>
        [ReadOnly] public WallBehaviour[] wallBehaviours;
        [ReadOnly] public bool[] isUsedForTau;
        //public List<WallAbsorption> wallAbsorptions;
    
        private Vector3 dimensionRoom;
    


        /// <summary>
        /// returns the Volume of the Room if it is not Outdoor
        /// </summary>
        public override float Volume => volume;

        private float volume;
    

        /// <summary>
        /// Preprocess all and set if it is Outdoor or not
        /// </summary>
        public override void ProcessAll(bool inputIsFreeField = false)
        {
            if (position == null) return;
            CalcRoomDimensions();
            volume = dimensionRoom[0] * dimensionRoom[1] * dimensionRoom[2];
        }

        public override void OnValidate()
        {
            ProcessAll();
        }

        // /// <summary>
        // /// Select all Walls in Editor
        // /// </summary>
        // public void SelectWalls()
        // {
        //     var wallGameObjects = new List<GameObject>();
        //     foreach (var wall in wallBehaviours)
        //         if (wall != null)
        //             wallGameObjects.Add(wall.gameObject);
        //
        //     Selection.objects = wallGameObjects.ToArray();
        // }
    
 
    
        private void CalcRoomDimensions()
        {
            var wallList = new List<WallBehaviour>();
            wallList.Clear();
            dimensionRoom = Vector3.zero;
            var directions = new[]
                {Vector3.right, Vector3.left, Vector3.up, Vector3.down, Vector3.forward, Vector3.back};
            var ray = new Ray
            {
                origin = position.position,
            };

            for (var i = 0; i < directions.Length; i += 2)
            for (var j = 0; j < directions.Length; j++)
            {
                ray.direction = (directions[i] + directions[j]).normalized;
                if (Physics.Raycast(ray, out var hit, Mathf.Infinity, BaSettings.Instance.LayerMaskWall))
                {
                    if (i == j)
                        dimensionRoom += MultiplyVectors(hit.distance, directions[i]);

                    var wall = hit.transform.GetComponent<WallBehaviour>();
                    if (wall != null) wallList.Add(wall);
                }

                ray.direction = (directions[i + 1] + directions[j]).normalized;
                if (Physics.Raycast(ray, out hit, Mathf.Infinity, BaSettings.Instance.LayerMaskWall))
                {
                    if (i + 1 == j)
                        dimensionRoom += MultiplyVectors(hit.distance, directions[i]);
                    var wall = hit.transform.GetComponent<WallBehaviour>();
                    if (wall != null) wallList.Add(wall);
                }
            }


            wallList = wallList.Distinct().ToList();

            isUsedForTau = new bool[wallList.Count];
            wallBehaviours = new WallBehaviour[wallList.Count];
            wallBehaviours = wallList.ToArray();
            for (var i = 0; i < wallList.Count; i++)
            {
                isUsedForTau[i] = true;
            }
            // wallAbsorptions.Clear();
            // foreach (var wallGeometry in wallBehaviours)
            // {
            //     // var wallAbsorption = wallGeometry.GetComponent<WallAbsorption>();
            //     // if (wallAbsorption == null)
            //     //     Debug.LogError("Require wall absorption Component.", wallGeometry.gameObject);
            //     // TODO
            //     wallAbsorptions.Add(null);
            // }

            for (var i = 0; i < 3; i++)
                if (Math.Abs(dimensionRoom[i]) < 1e-5)
                    Log.Error("Could not find Dimensions of the Room!");
        }
    
    
        private static Vector3 MultiplyVectors(Vector3 vec1, Vector3 vec2)
        {
            for (var i = 0; i < 3; i++) vec1[i] *= vec2[i];

            return vec1;
        }

        private static Vector3 MultiplyVectors(float single, Vector3 vec2)
        {
            for (var i = 0; i < 3; i++) vec2[i] *= single;

            return vec2;
        }


        public TransferFunction GetEqualAbsorptionArea(float factor = 1f)
        {
            var absArea = new TransferFunction(31);
            for(var i=0;i< wallBehaviours.Length;i++)
            {
                absArea += wallBehaviours[i].Geometry.Area * 0.5f;  //wallAbsorptions[i].AbsorptionCoefficient;
            }

            return absArea / factor;
        }
    }
}
