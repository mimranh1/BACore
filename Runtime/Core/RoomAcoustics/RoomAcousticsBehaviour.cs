﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

namespace BA.BACore
{
    public class RoomAcousticsBehaviour : MonoBehaviour
    {
        public bool showRoom = true;
        public RoomAcousticProperties RoomAcousticProperties=>roomAcousticProperties.@object;
        [SerializeField] private RoomAcousticPropertiesSelector roomAcousticProperties;
        
        public RoomGeometry RoomGeometry=>roomGeometry.@object;
        [SerializeField] private RoomGeometrySelector roomGeometry;
        
        public ReverberationCalculator ReverberationCalculator=> reverberationCalculator.@object;
        [SerializeField] private ReverberationCalculatorSelector reverberationCalculator;
        
        [HideInInspector] public float[] reverberation;
        [Header("Segment Settings")] public bool isSourceRoom;


        public float[] Reverberation =>
            reverberation == null || reverberation.Length == 0 ? CalcReverberation() : reverberation;

        public float[] CalcReverberation()
        {
            reverberation = ReverberationCalculator.ProcessAll(this);
            Debug.Log("Calculate Reverberation in room " + name, gameObject);
            return reverberation;
        }

        private void OnValidate()
        {
            if (roomGeometry == null)
                roomGeometry = ScriptableObject.CreateInstance<RoomGeometrySelector>();
            RoomGeometry.ProcessAll();
            if (roomAcousticProperties == null)
                roomAcousticProperties = ScriptableObject.CreateInstance<RoomAcousticPropertiesSelector>();
            if (reverberationCalculator == null)
                reverberationCalculator = ScriptableObject.CreateInstance<ReverberationCalculatorSelector>();
            roomAcousticProperties.@object.OnValidate();
            ReverberationCalculator.OnValidate();
            reverberationCalculator.@object.OnValidate();
            roomAcousticProperties.@object.ProcessAll(this);
        }

        private void OnDrawGizmosSelected()
        {
            reverberationCalculator.@object.OnDrawGizmosSelected(this);
            if (roomGeometry.@object.WallBehaviours == null) return;
            if (!showRoom) return;
            Gizmos.color = Color.red;
            foreach (var wallBehaviour in roomGeometry.@object.WallBehaviours)
            {
                Gizmos.DrawWireCube(wallBehaviour.Geometry.Position, wallBehaviour.Geometry.Dimensions);
                // wallBehaviour.OnDrawGizmosSelected();
                if (isSourceRoom)
                    wallBehaviour.WallSourceSegments?.OnDrawGizmosSelected(new Color32(0, 255, 255, 150));
                else
                    wallBehaviour.WallReceiverSegments?.OnDrawGizmosSelected(new Color32(255, 0, 255, 100));
                // wallBehaviour.Junctions?.OnDrawGizmosSelected();
            }

            RoomAcousticProperties.OnDrawGizmosSelected();
        }

        [ContextMenu("Awake")]
        private void Awake()
        {
            roomAcousticProperties.@object.ProcessAll(this);
            reverberation =  reverberationCalculator.@object.ProcessAll(this);
        }

        public static implicit operator RoomGeometry(RoomAcousticsBehaviour behaviour)
        {
            return behaviour.RoomGeometry;
        }
        public static implicit operator ReverberationCalculator(RoomAcousticsBehaviour behaviour)
        {
            return behaviour.ReverberationCalculator;
        }
    }
}