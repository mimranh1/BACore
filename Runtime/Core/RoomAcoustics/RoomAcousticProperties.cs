﻿using System;
using System.Collections.Generic;
using log4net;
using Object = UnityEngine.Object;

namespace BA.BACore
{
    [Serializable]
    public abstract class RoomAcousticProperties : ExtendedScriptableObject<RoomAcousticProperties,RoomAcousticPropertiesTypes>
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(RoomAcousticProperties));

            
        /// <summary>
        /// Reverberation Time T60 for 3rd Octave Band 
        /// </summary>
        public abstract TransferFunction ReverberationTimeTransferFunction { get; }

    
        /// <summary>
        /// Reverberation Time T60 for 500 Hz
        /// </summary>
        public virtual float ReverberationTime
        {
            get
            {
                if (!ReferenceEquals(ReverberationTimeTransferFunction, null) &&
                    ReverberationTimeTransferFunction.NumFrequency != 0)
                    return ReverberationTimeTransferFunction.Tf[14];
                return -1f;
            }
        }
    
        /// <summary>
        /// returns EquivalentAbsorptionArea for 3rd Frequency Band
        /// </summary>
        public abstract TransferFunction EquivalentAbsorptionAreaTransferFunction { get; }

        /// <summary>
        /// return Single EquivalentAbsorptionArea for 500 Hz
        /// </summary>
        public virtual float EquivalentAbsorptionArea
        {
            get
            {
                if (!ReferenceEquals(EquivalentAbsorptionAreaTransferFunction, null) &&
                    EquivalentAbsorptionAreaTransferFunction.NumFrequency != 0)
                    if (EquivalentAbsorptionAreaTransferFunction.Tf[14] > 50f)
                        return 23.6f;
                    else
                        return EquivalentAbsorptionAreaTransferFunction.Tf[14];
                return -1f;
            }
        }

        public float volume;
        public float surface;

        [ReadOnly]
        public float dmfp;


        public int DmfpSamples => (int)(dmfp / BaSettings.Instance.C0 * BaSettings.Instance.SampleRate);


        public abstract void ProcessAll(RoomAcousticsBehaviour roomAcoustics);


        public abstract void OnValidate();
        public virtual void OnDrawGizmosSelected(){}

    }
}
