﻿using UnityEngine;

namespace BA.BACore
{
    public abstract class WallAbsorption
    {
        public virtual float AbsorptionCoefficient { get; }
        public virtual TransferFunction AbsorptionCoefficientTransferFunction { get; }

    }
}