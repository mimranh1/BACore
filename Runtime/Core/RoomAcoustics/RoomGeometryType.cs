﻿namespace BA.BACore
{
    public enum RoomGeometryType
    {
        Auto,
        Manual,
        FromChildren,
    }
}