﻿using System;
using System.Collections.Generic;
using log4net;

namespace BA.BACore
{
    [Serializable]
    public class RoomAcousticPropertiesEmpty : RoomAcousticProperties
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(RoomAcousticPropertiesEmpty));

        public override RoomAcousticPropertiesTypes Type=> RoomAcousticPropertiesTypes.Empty;

        public override TransferFunction ReverberationTimeTransferFunction => new TransferFunction(31);
        public override TransferFunction EquivalentAbsorptionAreaTransferFunction => new TransferFunction(31);

        public override void ProcessAll(RoomAcousticsBehaviour roomAcoustics)
        {
            surface = 0;
            dmfp = 4 * volume / surface;
        }

        public override void OnValidate()
        {
            
        }
    }
}
