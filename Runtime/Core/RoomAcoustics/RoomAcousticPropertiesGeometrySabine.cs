﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class RoomAcousticPropertiesGeometrySabine : RoomAcousticProperties
    {
        public override RoomAcousticPropertiesTypes Type => RoomAcousticPropertiesTypes.AutoGeometrySabine;
 
        public override TransferFunction ReverberationTimeTransferFunction => reverberationTimeTransferFunction;
        [SerializeReference] private TransferFunction reverberationTimeTransferFunction=new TransferFunction();

        public override TransferFunction EquivalentAbsorptionAreaTransferFunction => equivalentAbsorptionAreaTransferFunction;
        [SerializeReference] private TransferFunction equivalentAbsorptionAreaTransferFunction=new TransferFunction();

        [SerializeField][HideInInspector] private RoomAcousticsBehaviour roomAcousticsBehaviour;

     

        public override void ProcessAll(RoomAcousticsBehaviour roomAcoustics)
        {
            roomAcousticsBehaviour = roomAcoustics;
            var roomGeometry = roomAcoustics.RoomGeometry;
            roomGeometry.ProcessAll();
            volume = roomGeometry.Volume;
            surface = roomGeometry.WallBehaviours.Sum(wall => wall.Geometry.Area);
            dmfp = 4 * roomGeometry.Volume / surface;
            
            equivalentAbsorptionAreaTransferFunction = new TransferFunction(31) + 1;//TODo
            reverberationTimeTransferFunction = new TransferFunction(31) +
                                                BaSettings.Instance.Csab * volume / EquivalentAbsorptionArea;
            reverberationTimeTransferFunction.UpdateCurve();
            equivalentAbsorptionAreaTransferFunction.UpdateCurve();

        }

        public override void OnValidate()
        {
            if (roomAcousticsBehaviour != null)
                ProcessAll(roomAcousticsBehaviour);
            else
            {
                reverberationTimeTransferFunction = new TransferFunction(31);
                equivalentAbsorptionAreaTransferFunction = new TransferFunction(31);
                reverberationTimeTransferFunction.UpdateCurve();
                equivalentAbsorptionAreaTransferFunction.UpdateCurve();
            }
        }
    }
}
