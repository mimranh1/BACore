﻿

using System;
using System.Collections.Generic;
using UnityEngine;

namespace BA.BACore
{
    
    
    
    [Serializable]
    public class RoomAcousticPropertiesManual : RoomAcousticProperties
    {
        public float reverberationTime;
        public override RoomAcousticPropertiesTypes Type => RoomAcousticPropertiesTypes.Manual;
        public override TransferFunction ReverberationTimeTransferFunction => reverberationTimeTransferFunction;
        [SerializeReference] private TransferFunction reverberationTimeTransferFunction=new TransferFunction();

        public override TransferFunction EquivalentAbsorptionAreaTransferFunction => equivalentAbsorptionAreaTransferFunction;

        [SerializeReference] private TransferFunction equivalentAbsorptionAreaTransferFunction=new TransferFunction();

        public override void ProcessAll(RoomAcousticsBehaviour roomAcoustics)
        {
            reverberationTimeTransferFunction = new TransferFunction(31) + reverberationTime;
            equivalentAbsorptionAreaTransferFunction =
                new TransferFunction(31) + BaSettings.Instance.Csab * volume / ReverberationTime;
            dmfp = 4 * volume / surface;
            reverberationTimeTransferFunction.UpdateCurve();
            equivalentAbsorptionAreaTransferFunction.UpdateCurve();
        }

        public override void OnValidate()
        {
            if (surface > 0 && reverberationTime > 0)
                ProcessAll(null);
            else
            {
                reverberationTimeTransferFunction = new TransferFunction(31);
                equivalentAbsorptionAreaTransferFunction = new TransferFunction(31);
                reverberationTimeTransferFunction.UpdateCurve();
                equivalentAbsorptionAreaTransferFunction.UpdateCurve();
            }
        }
    }
}
