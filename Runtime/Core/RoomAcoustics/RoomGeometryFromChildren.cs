﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class RoomGeometryFromChildren : RoomGeometry
    {
        public override RoomGeometryType Type => RoomGeometryType.FromChildren;

        public override WallBehaviour[] WallBehaviours => wallBehaviours;
        [SerializeField] private WallBehaviour[] wallBehaviours;

        public override bool[] IsUsedForTau => isUsedForTau;
        [SerializeField] private bool[] isUsedForTau;

        public override float Volume => volume;
        [SerializeField] private float volume;

        [SerializeField] private GameObject parent;

        public override void ProcessAll(bool inputIsFreeField = false)
        {
            wallBehaviours = parent != null
                ? parent.GetComponentsInChildren<WallBehaviour>()
                : null;

            if (wallBehaviours != null) isUsedForTau = new bool[wallBehaviours.Length];
        }

        public override void OnValidate()
        {
            ProcessAll();
        }
    }
}