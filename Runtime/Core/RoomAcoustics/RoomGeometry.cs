﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BA.BACore
{
    [Serializable]
    public abstract class RoomGeometry  : ExtendedScriptableObject<RoomGeometry,RoomGeometryType>
    {
        public abstract WallBehaviour[] WallBehaviours { get; }
        public abstract bool[] IsUsedForTau{ get; }
        /// <summary>
        /// returns the Volume of the Room if it is not Outdoor
        /// </summary>
        public abstract float Volume { get; }

        public virtual float Surface =>WallBehaviours.Sum(wallBehaviour => wallBehaviour.Geometry.Area);


        /// <summary>
        /// Preprocess all and set if it is Outdoor or not
        /// </summary>
        public abstract void ProcessAll(bool inputIsFreeField = false);
        
        public virtual int FindWallIndex(int id)
        {
            for (int i = 0; i < WallBehaviours.Length; i++)
            {
                if (WallBehaviours[i].GetInstanceID() == id)
                    return i;
            }

            return -1;
        }

        public abstract void OnValidate();

    }
}