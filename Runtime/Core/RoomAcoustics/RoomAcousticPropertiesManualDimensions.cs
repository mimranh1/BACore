﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class RoomAcousticPropertiesManualDimensions:RoomAcousticProperties
    {
        public Vector3 dimensions;
        public override RoomAcousticPropertiesTypes Type => RoomAcousticPropertiesTypes.ManualDimensions;
        public float reverberationTime;
        public override TransferFunction ReverberationTimeTransferFunction => reverberationTimeTransferFunction;
        [SerializeReference] private TransferFunction reverberationTimeTransferFunction=new TransferFunction();

        public override TransferFunction EquivalentAbsorptionAreaTransferFunction => equivalentAbsorptionAreaTransferFunction;

        [SerializeReference] private TransferFunction equivalentAbsorptionAreaTransferFunction=new TransferFunction();
        [SerializeField] [HideInInspector] private RoomAcousticsBehaviour roomAcousticsBehaviour; 

        public override void ProcessAll(RoomAcousticsBehaviour roomAcoustics)
        {
            roomAcousticsBehaviour = roomAcoustics;
            Update();
        }

        private void Update()
        {
            volume = dimensions.x * dimensions.y * dimensions.z;
            surface = 2 *
                      ((dimensions.x * dimensions.y) + (dimensions.x * dimensions.z) + (dimensions.y * dimensions.z));
            reverberationTimeTransferFunction = new TransferFunction(31) + reverberationTime;
            equivalentAbsorptionAreaTransferFunction =
                new TransferFunction(31) + BaSettings.Instance.Csab * volume / ReverberationTime;
            dmfp = 4 * volume / surface;
            reverberationTimeTransferFunction.UpdateCurve();
            equivalentAbsorptionAreaTransferFunction.UpdateCurve();
        }

        public override void OnValidate()
        {
            if (surface > 0 && reverberationTime > 0)
                Update();
            else
            {
                reverberationTimeTransferFunction = new TransferFunction(31);
                equivalentAbsorptionAreaTransferFunction = new TransferFunction(31);
                reverberationTimeTransferFunction.UpdateCurve();
                equivalentAbsorptionAreaTransferFunction.UpdateCurve();
            }
        }

        public override void OnDrawGizmosSelected()
        {
            Gizmos.DrawWireCube(roomAcousticsBehaviour.transform.position,dimensions);
                
        } 
    }
}