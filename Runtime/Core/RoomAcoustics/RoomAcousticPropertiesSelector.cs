﻿using System;

namespace BA.BACore
{
    [Serializable]
    public class RoomAcousticPropertiesSelector : Selector<RoomAcousticProperties,RoomAcousticPropertiesTypes>{}
}