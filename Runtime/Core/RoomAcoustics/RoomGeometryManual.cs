﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class RoomGeometryManual : RoomGeometry
    {
        public override RoomGeometryType Type => RoomGeometryType.Manual;

        public override WallBehaviour[] WallBehaviours => wallBehaviours;
        [SerializeField]private  WallBehaviour[] wallBehaviours;


        public override bool[] IsUsedForTau => isUsedForTau;
        [SerializeField] private bool[] isUsedForTau;
        public override float Volume => volume;
        [SerializeField] private float volume;
        public override void ProcessAll(bool inputIsFreeField = false)
        {
            if (wallBehaviours != null)
                isUsedForTau = new bool[wallBehaviours.Length];
        }

        public override void OnValidate()
        {
            ProcessAll();
        }
    }
}