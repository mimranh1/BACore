﻿

using System;
using System.Collections.Generic;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class RoomAcousticPropertiesSabine : RoomAcousticProperties
    {
        public float alpha;

        public override RoomAcousticPropertiesTypes Type => RoomAcousticPropertiesTypes.Sabine;
        public override TransferFunction ReverberationTimeTransferFunction => reverberationTimeTransferFunction;
        [SerializeReference] private TransferFunction reverberationTimeTransferFunction=new TransferFunction();

        public override TransferFunction EquivalentAbsorptionAreaTransferFunction => equivalentAbsorptionAreaTransferFunction;

        [SerializeReference] private TransferFunction equivalentAbsorptionAreaTransferFunction=new TransferFunction();


        public override void ProcessAll(RoomAcousticsBehaviour roomAcoustics)
        {
            equivalentAbsorptionAreaTransferFunction = new TransferFunction(31) + alpha * surface;
            reverberationTimeTransferFunction = new TransferFunction(31) +
                                                BaSettings.Instance.Csab * volume / EquivalentAbsorptionArea;
            dmfp = 4 * volume / surface;
            reverberationTimeTransferFunction.UpdateCurve();
            equivalentAbsorptionAreaTransferFunction.UpdateCurve();

        }

        public override void OnValidate()
        {
            
            if (!(alpha > 0) || !(volume > 0) || !(surface > 0))
            {
                reverberationTimeTransferFunction=new TransferFunction(31);
                equivalentAbsorptionAreaTransferFunction=new TransferFunction(31);
                reverberationTimeTransferFunction.UpdateCurve();
                equivalentAbsorptionAreaTransferFunction.UpdateCurve();
            }
            else
            {
                ProcessAll(null);
            }
        }
    }
}
