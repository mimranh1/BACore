﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace BA.BACore
{
    [Serializable]
    public class ReverberationCalculatorImageSourceOutdoor : ReverberationCalculator
    {
        public override ReverberationCalculationMethode Type => ReverberationCalculationMethode.ImageSourceOutdoor;

        [Range(0, 5)] public int maxOrder;

        private RoomGeometry _roomGeometryAuto;
        [SerializeField] private Transform source;
        [SerializeField] private WallBehaviour[] receivers;

        [SerializeField] private bool drawRays;
        [SerializeField] private bool drawImages;
        [SerializeField] [HideInInspector] private int reverberationLength = 44100;

        [SerializeField] [HideInInspector] private ImageSources[][] imageSources;

        [SerializeField] [HideInInspector] private FloatArrays[] reverberationMono;
        public override FloatArrays[] ReverberationBinaural { get; }
        public override FloatArrays[] ReverberationAll => reverberationMono;
        [SerializeField] [HideInInspector] private FloatArrays[] binauralBuffer;
        [SerializeField] [HideInInspector] private Collider[] colliders;
        [SerializeField]  private LayerMask wallLayerMask;
        [SerializeField][HideInInspector] private WallBehaviour[] roomGeometryWallBehaviours;
        private Stopwatch sw;

        public float SamplesByMeter
        {
            get
            {
                if (_samplesByMeter <= 0f)
                    _samplesByMeter = BaSettings.Instance.SampleRate / BaSettings.Instance.C0;
                return _samplesByMeter;
            }
        }

        [SerializeField] private float _samplesByMeter = -1f;

        public override float[] ProcessAll(RoomAcousticsBehaviour roomAcoustics)
        {
            sw = new Stopwatch();
            Init(roomAcoustics);

            OnReceiverUpdate();

            if (drawImages)
                DrawImages();
            OnSourceUpdate();
            //DrawAudibleSources();


            if (drawRays)
                DrawRays();
            return new float[1];
        }

        public override void Init(SoundSourceObject sourceObject, SoundReceiverObject receiverObject)
        {
            source = sourceObject.transform;
        }

        private void Init(RoomAcousticsBehaviour roomAcoustics)
        {
            sw.Restart();
            wallLayerMask = BaSettings.Instance.LayerMaskWall;
            if (roomAcoustics != null)
                roomGeometryWallBehaviours = roomAcoustics.RoomGeometry.WallBehaviours;
            colliders = new Collider[roomGeometryWallBehaviours.Length];
            for (var i = 0; i < colliders.Length; i++)
            {
                colliders[i] = roomGeometryWallBehaviours[i].GetComponent<Collider>();
            }
            reverberationMono=new FloatArrays[receivers.Length];
            for (var i = 0; i < receivers.Length; i++)
            {
                reverberationMono[i] = new FloatArrays(reverberationLength);
            }

            if (roomAcoustics != null)
                _roomGeometryAuto = roomAcoustics.RoomGeometry;
            sw.Stop();
            Debug.Log("Init in " + sw.ElapsedMilliseconds + " ms.");
        }

        public override void OnDrawGizmosSelected(RoomAcousticsBehaviour roomAcousticsBehaviour)
        {
            if (drawImages || drawRays)
                ProcessAll(roomAcousticsBehaviour);
        }

        public override void OnSourceUpdate()
        {
            sw.Restart();
            CheckAudibiliy();
            sw.Stop();
            // Debug.Log("OnSourceUpdate in " + sw.ElapsedMilliseconds + " ms.");
        }

        public override void OnUpdateFilter(AudioRender audioRender)
        {
            sw.Restart();
            GenerateImpulseResponse(false);
            sw.Stop();
            // Debug.Log("OnUpdateFilter in " + sw.ElapsedMilliseconds + " ms.");
        }


        public override void OnReceiverUpdate()
        {
            sw.Restart();
            CreateImages();
            CheckAudibiliy();
            sw.Stop();
            // Debug.Log("OnReceiverUpdate in " + sw.ElapsedMilliseconds + " ms.");
        }

        public void AddImageToReverberationMono(ImageSource image, int i)
        {
                var distance = Vector3.Distance(source.position, image.Position);
                var distanceSamples = (int) (distance * SamplesByMeter);
                
                if (distanceSamples < reverberationLength)
                    reverberationMono[i].floats[distanceSamples] = image.factor / distance;
        }


        public void AddImageToReverberationMonoTau(ImageSource image, int i)
        {
            var distance = Vector3.Distance(source.position, image.Position);
            var incAngleRad = image.incidentAngleRad;
            var distanceSamples = (int) (distance * SamplesByMeter);
            
            if (distanceSamples < reverberationLength)
                reverberationMono[i].floats[distanceSamples] = image.factor / distance;
        }


        private void CheckAudibiliy()
        {
            for (var iReceiver = 0; iReceiver < imageSources.Length; iReceiver++)
            {
                for (var order = imageSources[iReceiver].Length - 1; order >= 0; order--)
                {
                    for (var j = 0; j < imageSources[iReceiver][order].sources.Count; j++)
                    {
                        imageSources[iReceiver][order].sources[j].isAudible =
                            CheckRay(iReceiver, order, j, source.position, Color.black);
                    }
                }
            }
        }

        private void GenerateImpulseResponse(bool isMono = true)
        {
            foreach (var reverberationMonoa in reverberationMono)
            {
                reverberationMonoa.Clear();
            }

            for (var iReceiver = 0; iReceiver < imageSources.Length; iReceiver++)
            for (var order = 1; order < imageSources[iReceiver].Length; order++)
            {
                var images = imageSources[iReceiver][order];
                foreach (var image in images.sources)
                {
                    if (image.isAudible)
                        AddImageToReverberationMono(image, iReceiver);
                }
            }
        }


        private void DrawRays()
        {
            var colors = new[] {Color.blue, Color.red, Color.green, Color.magenta};
            for (var iReceiver = 0; iReceiver < imageSources.Length; iReceiver++)
            {
                var imageSource = imageSources[iReceiver];
                for (var order = imageSource.Length - 1; order >= 0; order--)
                {
                    for (var j = 0; j < imageSource[order].sources.Count; j++)
                    {
                        if (!imageSource[order].sources[j].isAudible) continue;
                        if (drawRays)
                            CheckRay(iReceiver,order, j, source.position, colors[order % colors.Length], true);
                    }
                }
            }
        }

        private bool CheckRay(int receiverId, int order, int index, Vector3 pos, Color color, bool plot = false)
        {
            while (true)
            {
                var imageSource = imageSources[receiverId][order].sources[index];
                var lastIndexes = imageSource.LastIndex;
                if (lastIndexes == -1)
                {
                    //DrawImage(pos, receiver.position,Color.cyan);
                    if (Raycast(pos, imageSource.Position, out var hit1)) return false;
                    if (plot) DrawImage(pos, imageSource.Position, color);
                    var direction = imageSource.Position - pos; //Vector3.RotateTowards(pos, posImage, 360, 1);

                    var angleRad = Mathf.Acos(Vector3.Dot(receivers[receiverId].Geometry.Normal, direction.normalized));
                    angleRad = angleRad > (Mathf.PI / 2) ? Mathf.PI - angleRad : angleRad;
                    Debug.Log("incidentAngle " + angleRad * Mathf.Rad2Deg);
                    imageSources[receiverId][order].sources[index].incidentAngleRad = angleRad;
                    return true;
                }

                var posImage = imageSource.Position;

                //Debug.Log("PosOrg" + pos + " Ziel " + posImage);
                if (!Raycast(pos, posImage, out var hit)) return false;


                if (hit.collider != colliders[imageSource.LastWallIndex]) return false;
                var position = hit.point;
                if (plot) DrawImage(pos, position, color);
                order -= 1;
                index = lastIndexes;
                pos = position;
            }
        }

        private bool Raycast(Vector3 pos, Vector3 posImage, out RaycastHit hit)
        {
            var direction = posImage - pos; //Vector3.RotateTowards(pos, posImage, 360, 1);
            var dist = Vector3.Distance(pos, posImage);
            var raycast = Physics.Raycast(pos, direction, out hit, dist, wallLayerMask);
           // Debug.DrawRay(pos,direction*hit.distance,Color.red);
            return raycast;
        }

        private void DrawImage(Vector3 start, Vector3 end, Color color)
        {
            Debug.DrawRay(start, end - start, color);
            //Debug.Log($"{start} {end} {color.ToString()}");
        }

        private void CreateImages()
        {
            imageSources = new ImageSources[receivers.Length][];
            for (var iReceiver = 0; iReceiver < receivers.Length; iReceiver++)
            {
                imageSources[iReceiver] = new ImageSources[maxOrder + 1];
                var sourcePosition = receivers[iReceiver].Geometry.Position;
                var imageSource0 = new ImageSource
                {
                    Position = sourcePosition,
                    Normal = receivers[iReceiver].Geometry.Normal,
                    WallIndex = new List<int>(0),
                    LastIndex = -1,
                    factor = 1,
                };
                imageSources[iReceiver][0] = new ImageSources();
                imageSources[iReceiver][0].sources.Add(imageSource0);
                for (var order = 1; order < maxOrder + 1; order++)
                {
                    imageSources[iReceiver][order] = new ImageSources();
                    for (var index = 0; index < imageSources[iReceiver][order - 1].sources.Count; index++)
                    {
                        var currentImage = imageSources[iReceiver][order - 1].sources[index];
                        for (var i = 0; i < _roomGeometryAuto.WallBehaviours.Length; i++)
                        {
                            var wallBehaviour = _roomGeometryAuto.WallBehaviours[i];
                            var wallPos = wallBehaviour.Geometry.Position;
                            var wallNormal = wallBehaviour.Geometry.Normal;
                            var image = 2 * Vector3E.MultiplyElementwise(wallPos - currentImage.Position, wallNormal) +
                                        currentImage.Position;
                            var normal = currentImage.Normal + 180f * wallNormal;
                            var imageSource = new ImageSource
                            {
                                Position = image,
                                WallIndex = new List<int>(),
                                LastIndex = index,
                                Normal = normal,
                                factor = currentImage.factor * 0.8f,
                            };
                            imageSource.WallIndex.AddRange(currentImage.WallIndex);
                            imageSource.WallIndex.Add(wallBehaviour.GetInstanceID());
                            imageSource.LastWallIndex = i;
                            imageSources[iReceiver][order].Add(imageSource);
                        }
                    }
                }
            }

        }

        private void DrawImages()
        {
            for (int iReceiver = 0; iReceiver < imageSources.Length; iReceiver++)
                for (var order = 1; order < imageSources[iReceiver].Length; order++)
                {
                    var imageSource = imageSources[iReceiver][order];
                    foreach (var image in imageSource.sources)
                    {
                        Debug.DrawLine(image.Position, imageSources[iReceiver][order - 1].sources[image.LastIndex].Position,
                            Color.yellow, 0);
                    }
                }
        }

        private void DrawAudibleSources()
        {
            var colors = new[] {Color.blue, Color.red, Color.green, Color.magenta};
            for (var iReceiver = 0; iReceiver < imageSources.Length; iReceiver++)
            for (var order = 0; order < imageSources[iReceiver].Length; order++)
            {
                var imageSource = imageSources[iReceiver][order];
                for (var j = 0; j < imageSource.sources.Count; j++)
                {
                    var image = imageSource.sources[j];
                    if (!image.isAudible) continue;
                    if (drawRays)
                        CheckRay(iReceiver, order, j, source.position, colors[order % colors.Length], true);
                }
            }
        }
    }
}