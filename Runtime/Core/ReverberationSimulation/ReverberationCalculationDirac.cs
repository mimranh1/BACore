﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class ReverberationCalculationDirac : ReverberationCalculator
    {
        public override ReverberationCalculationMethode Type => ReverberationCalculationMethode.Dirac;
        public override FloatArrays[] ReverberationBinaural => reverberationBinaural;
        public override FloatArrays[] ReverberationAll => reverberationBinaural;
        [SerializeField] [HideInInspector] private FloatArrays[] reverberationBinaural;

        public override float[] ProcessAll(RoomAcousticsBehaviour roomAcoustics)
        {
            var rev = new float[BaSettings.Instance.filterLengthInSamples];
            rev[0] = 1f;
            reverberationBinaural = new FloatArrays[1];
            reverberationBinaural[0] = new FloatArrays(rev);
            return rev;
        }
    }
}