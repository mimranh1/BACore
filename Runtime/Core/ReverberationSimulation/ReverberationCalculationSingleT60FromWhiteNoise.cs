﻿using System;
using log4net;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class ReverberationCalculationSingleT60FromWhiteNoise : ReverberationCalculator
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(ReverberationCalculationSingleT60FromWhiteNoise));

        public override ReverberationCalculationMethode Type => ReverberationCalculationMethode.SingleReverberationTime;
        public override FloatArrays[] ReverberationBinaural => reverberationBinaural;
        public override FloatArrays[] ReverberationAll => reverberationBinaural;
        [SerializeField] [HideInInspector] private FloatArrays[] reverberationBinaural;


        public override float[] ProcessAll(RoomAcousticsBehaviour roomAcoustics)
        {
            reverberationBinaural=new FloatArrays[1];
            reverberationBinaural[0]= new FloatArrays(GetReverberationFromOneT60(roomAcoustics.RoomAcousticProperties.ReverberationTime));
            return reverberationBinaural[0].floats;
        }

    
        /// <summary>
        /// Calculate Reverberation From White Noise and reverberation Time
        /// </summary>
        /// <param name="reverberationTime">input Reverberation Time</param>
        /// <returns>return reverberation Tail</returns>
        public static float[] GetReverberationFromOneT60(float reverberationTime)
        {
            var factor = -6.9078f / (44100 * reverberationTime);
            var samplesRev = (int) (1f * reverberationTime * 44100f);
            samplesRev = BaSettings.Instance.filterLengthInSamples;
            var whiteNoise = GenerateWhiteNoise(samplesRev, 0f, 1f);
            if (samplesRev > whiteNoise.Length)
                Log.Error("input Noise file to short, Recommended Size is " + samplesRev / 44100.0f + " s.");
            var rev = new float[samplesRev];
            for (var i = 0; i < samplesRev; i++)
                rev[i] = Mathf.Exp(factor * i) * whiteNoise[i];


            DataLogger.Log("RevCalc.ReverberationTime", reverberationTime);
            DataLogger.Log("RevCalc.ReverberationTrailOneT60", rev);

           return NormalizeSignal(rev);
        }
    }
}
