﻿using System;
using System.Collections.Generic;

namespace BA.BACore
{
    [Serializable]
    public class ImageSources
    {
        public List<ImageSource> sources;

        public ImageSources()
        {
            sources = new List<ImageSource>();
        }

        public void Add(ImageSource imageSource)
        {
            if (sources == null)
                sources = new List<ImageSource>();
            sources.Add(imageSource);
        }
    }
}