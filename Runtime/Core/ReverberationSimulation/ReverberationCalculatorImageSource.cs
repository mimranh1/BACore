﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEditor.PackageManager;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace BA.BACore
{
    [Serializable]
    public class ReverberationCalculatorImageSource : ReverberationCalculator
    {
        public override ReverberationCalculationMethode Type => ReverberationCalculationMethode.ImageSource;

        [Range(0, 5)] public int maxOrder;

        private RoomGeometry _roomGeometryAuto;
        [SerializeField] private Transform source;
        [SerializeField] private SoundReceiverObject receiver;

        [SerializeField] private bool drawRays;
        [SerializeField] private bool drawImages;
        [SerializeField] [HideInInspector] private int reverberationLength = 44100;

        [SerializeField] [HideInInspector] private ImageSources1[] imageSources;

        [SerializeField] [HideInInspector] private float[] reverberationMono;
        [SerializeField] [HideInInspector] private FloatArrays[] reverberationBinaural;
        public override FloatArrays[] ReverberationBinaural => reverberationBinaural;
        public override FloatArrays[] ReverberationAll => reverberationBinaural;
        [SerializeField] [HideInInspector] private FloatArrays[] binauralBuffer;
        [SerializeField] [HideInInspector] private Collider[] colliders;
        [SerializeField] [HideInInspector] private int instanceLayerMaskWall;
        private Stopwatch sw;

        public float SamplesByMeter
        {
            get
            {
                if (_samplesByMeter <= 0f)
                    _samplesByMeter = BaSettings.Instance.SampleRate / BaSettings.Instance.C0;
                return _samplesByMeter;
            }
        }

        [SerializeField] private float _samplesByMeter = -1f;

        public override float[] ProcessAll(RoomAcousticsBehaviour roomAcoustics)
        {
            sw = new Stopwatch();
            Init(roomAcoustics);

            OnSourceUpdate();
            if (drawImages)
                DrawImages();
            //DrawAudibleSources();

            OnReceiverUpdate();

            if (drawRays)
                DrawRays();
            return new float[1];
        }

        public override void Init(SoundSourceObject sourceObject, SoundReceiverObject receiverObject)
        {
            source = sourceObject.transform;
            receiver = receiverObject;
        }

        private void Init(RoomAcousticsBehaviour roomAcoustics)
        {
            sw.Restart();
            InitBinauralBuffer();
            instanceLayerMaskWall = BaSettings.Instance.LayerMaskWall;
            var roomGeometryWallBehaviours = roomAcoustics.RoomGeometry.WallBehaviours;
            colliders = new Collider[roomGeometryWallBehaviours.Length];
            for (var i = 0; i < colliders.Length; i++)
            {
                colliders[i] = roomGeometryWallBehaviours[i].GetComponent<Collider>();
            }

            reverberationMono = new float[reverberationLength];
            _roomGeometryAuto = roomAcoustics.RoomGeometry;
            sw.Stop();
            Debug.Log("Init in " + sw.ElapsedMilliseconds + " ms.");
        }

        public override void OnSourceUpdate()
        {
            sw.Restart();
            CreateImages();
            CheckAudibiliy();
            sw.Stop();
            Debug.Log("OnSourceUpdate in " + sw.ElapsedMilliseconds + " ms.");
        }

        public override void OnUpdateFilter(AudioRender audioRender)
        {
            sw.Restart();
            GenerateImpulseResponse(false);
            sw.Stop();
            Debug.Log("OnUpdateFilter in " + sw.ElapsedMilliseconds + " ms.");
        }


        public override void OnReceiverUpdate()
        {
            sw.Restart();
            CheckAudibiliy();
            sw.Stop();
            Debug.Log("OnReceiverUpdate in " + sw.ElapsedMilliseconds + " ms.");
        }

        public void AddImageToReverberationMono(ImageSource1 image)
        {
            var distance = Vector3.Distance(receiver.transform.position, image.Position);
            var distanceSamples = (int) (distance * SamplesByMeter);

            if (distanceSamples < reverberationLength)
                reverberationMono[distanceSamples] = image.factor / distance;
        }

        public void AddImageToReverberationBinaural(ImageSource1 image)
        {
            var sampleDelay = receiver.GetBinauralDirectivity(true, binauralBuffer, image.Position, 1f,true, false);
            var imageFactor = image.factor;
            for (var i = 0; i < binauralBuffer[0].Length; i++)
            {
                reverberationBinaural[0][sampleDelay + i] += binauralBuffer[0][i] * imageFactor;
                reverberationBinaural[1][sampleDelay + i] += binauralBuffer[1][i] * imageFactor;
            }
        }

        public void InitBinauralBuffer()
        {
            receiver.Init();
            binauralBuffer = new FloatArrays[2];
            reverberationBinaural = new FloatArrays[2];
            for (var i = 0; i < reverberationBinaural.Length; i++)
            {
                if (reverberationBinaural[i] == null)
                    reverberationBinaural[i] = new FloatArrays(44100);
                reverberationBinaural[i].Clear();
            }

            binauralBuffer[0] = new FloatArrays(receiver.Length);
            binauralBuffer[1] = new FloatArrays(receiver.Length);
        }

        private void CheckAudibiliy()
        {
            for (var order = imageSources.Length - 1; order >= 0; order--)
            {
                for (var j = 0; j < imageSources[order].sources.Count; j++)
                {
                    imageSources[order].sources[j].isAudible =new []{
                        CheckRay(order, j, receiver.transform.position, Color.black)};
                }
            }
        }

        private void GenerateImpulseResponse(bool isMono = true)
        {
            if (isMono)
                Array.Clear(reverberationMono, 0, reverberationMono.Length);
            else
            {
                reverberationBinaural[0].Clear();
                reverberationBinaural[1].Clear();
            }

            foreach (var images in imageSources)
            {
                foreach (var image in images.sources.Where(image => image.isAudible[0]))
                {
                    if (isMono)
                        AddImageToReverberationMono(image); 
                    else
                        AddImageToReverberationBinaural(image);
                }
            }
        }


        private void DrawRays()
        {
            var colors = new[] {Color.blue, Color.red, Color.green, Color.magenta};
            for (var order = imageSources.Length - 1; order >= 0; order--)
            {
                for (var j = 0; j < imageSources[order].sources.Count; j++)
                {
                    if (!imageSources[order].sources[j].isAudible[0]) continue;
                    if (drawRays)
                        CheckRay(order, j, receiver.transform.position, colors[order % colors.Length], true);
                }
            }
        }

        private bool CheckRay(int order, int index, Vector3 pos, Color color, bool plot = false)
        {
            while (true)
            {
                var imageSource = imageSources[order].sources[index];
                var lastIndexes = imageSource.LastIndex;
                if (lastIndexes == -1)
                {
                    //DrawImage(pos, receiver.position,Color.cyan);
                    if (Raycast(pos, imageSource.Position, out var hit1)) return false;
                    if (plot) DrawImage(pos, imageSource.Position, color);
                    return true;
                }

                var posImage = imageSource.Position;

                //Debug.Log("PosOrg" + pos + " Zeil " + posImage);
                if (!Raycast(pos, posImage, out var hit)) return false;


                if (hit.collider != colliders[imageSource.LastWallIndex]) return false;
                var position = hit.point;
                if (plot) DrawImage(pos, position, color);
                order -= 1;
                index = lastIndexes;
                pos = position;
            }
        }

        private bool Raycast(Vector3 pos, Vector3 posImage, out RaycastHit hit)
        {
            var direction = posImage - pos; //Vector3.RotateTowards(pos, posImage, 360, 1);
            var dist = Vector3.Distance(pos, posImage);
            return Physics.Raycast(pos, direction, out hit, dist, instanceLayerMaskWall);
        }

        private void DrawImage(Vector3 start, Vector3 end, Color color)
        {
            Debug.DrawRay(start, end - start, color, 10);
            //Debug.Log($"{start} {end} {color.ToString()}");
        }

        private void CreateImages()
        {
            imageSources = new ImageSources1[maxOrder + 1];
            var imageSource0 = new ImageSource1
            {
                Position = source.position,
                WallIndex = new List<int>(0),
                LastIndex = -1,
                factor = 1,
            };
            imageSources[0] = new ImageSources1();
            imageSources[0].sources.Add(imageSource0);
            for (var order = 1; order < maxOrder + 1; order++)
            {
                imageSources[order] = new ImageSources1();
                for (var index = 0; index < imageSources[order - 1].sources.Count; index++)
                {
                    var currentImage = imageSources[order - 1].sources[index];
                    for (var i = 0; i < _roomGeometryAuto.WallBehaviours.Length; i++)
                    {
                        var wallBehaviour = _roomGeometryAuto.WallBehaviours[i];
                        var wallPos = wallBehaviour.Geometry.Position;
                        var wallNormal = wallBehaviour.Geometry.Normal;
                        var image = 2 * Vector3E.MultiplyElementwise(wallPos - currentImage.Position, wallNormal) +
                                    currentImage.Position;
                        var imageSource = new ImageSource1
                        {
                            Position = image,
                            WallIndex = new List<int>(),
                            LastIndex = index,
                            factor = currentImage.factor * 0.1f,
                        };
                        imageSource.WallIndex.AddRange(currentImage.WallIndex);
                        imageSource.WallIndex.Add(wallBehaviour.GetInstanceID());
                        imageSource.LastWallIndex = i;
                        imageSources[order].Add(imageSource);
                    }
                }
            }

        }

        private void DrawImages()
        {
            for (var order = 1; order < imageSources.Length; order++)
            {
                var imageSource = imageSources[order];
                foreach (var image in imageSource.sources)
                {
                    Debug.DrawLine(image.Position, imageSources[order - 1].sources[image.LastIndex].Position,
                        Color.yellow, 10);
                }
            }
        }

        private void DrawAudibleSources()
        {
            for (var order = 0; order < imageSources.Length; order++)
            {
                var imageSource = imageSources[order];
                foreach (var image in imageSource.sources)
                {
                    if (image.isAudible[0])
                        Debug.DrawLine(image.Position, receiver.transform.position, Color.yellow, 10);
                }
            }
        }
    }
}