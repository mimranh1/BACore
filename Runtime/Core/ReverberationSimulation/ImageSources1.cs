﻿using System;
using System.Collections.Generic;

namespace BA.BACore
{
    [Serializable]
    public class ImageSources1
    {
        public List<ImageSource1> sources;

        public ImageSources1()
        {
            sources = new List<ImageSource1>();
        }

        public void Add(ImageSource1 imageSource)
        {
            if (sources == null)
                sources = new List<ImageSource1>();
            sources.Add(imageSource);
        }
    }
}