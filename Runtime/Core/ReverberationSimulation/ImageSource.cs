﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class ImageSource
    {
        public Vector3 Position;
        public Vector3 Normal;
        public List<int> WallIndex;
        public int LastIndex;
        public int LastWallIndex;
        public bool isAudible;
        public float factor;
        public float incidentAngleRad;
    }
}