﻿using System;
using log4net;
using UnityEngine;
using Object = UnityEngine.Object;

namespace BA.BACore
{
    [Serializable]
    public class ReverberationCalculation3RdOctaveBand : ReverberationCalculator
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(ReverberationCalculationSingleT60FromWhiteNoise));

        [SerializeField] private FilterBank filterBank;
        
        public override ReverberationCalculationMethode Type =>
            ReverberationCalculationMethode.ThirdOctaveBandReverberationTime;

        public override FloatArrays[] ReverberationBinaural => reverberationBinaural;
        public override FloatArrays[] ReverberationAll => reverberationBinaural;
        [SerializeField] [HideInInspector] private FloatArrays[] reverberationBinaural;



        public override float[] ProcessAll(RoomAcousticsBehaviour roomAcoustics)
        {
            if(roomAcoustics.RoomAcousticProperties.ReverberationTimeTransferFunction==null)
            {
                Debug.LogError("ReverberationTimeTransferFunction ", this);
                return new float[0];
            }
            if(roomAcoustics.RoomAcousticProperties.ReverberationTime==0f)
            {
                Debug.LogError("ReverberationTime should not be 0 ", this);
                return new float[0];
            }
            roomAcoustics.RoomAcousticProperties.ProcessAll(roomAcoustics);
            reverberationBinaural=new FloatArrays[1];
            reverberationBinaural[0]= new FloatArrays(GetIsoReverberation3RdOctaveBand(roomAcoustics.RoomAcousticProperties.ReverberationTimeTransferFunction,
                BaSettings.Instance.ThirdOctaveFrequencyBandNorm.Tf));
            return reverberationBinaural[0].floats;
        }

     

        /// <summary>
        /// Calculate Reverberation From White Noise and 3rd Octave reverberation Time
        /// </summary>
        /// <param name="reverberationTimes">input reverberation Time for Reverberation</param>
        /// <param name="frequencyBand">Frequency Band for the reverberation Time input</param>
        /// <returns>return reverberation Tail</returns>
        public float[] GetIsoReverberation3RdOctaveBand(TransferFunction reverberationTimes, float[] frequencyBand)
        {
            if (reverberationTimes.NumFrequency != frequencyBand.Length)
                Log.Error("Reverberation Times and Frequency Band have to have the same length.");
            var samplesRev = BaSettings.Instance.filterLengthInSamples;
            filterBank.LoadFilter(samplesRev);
            var whiteNoise = GenerateWhiteNoise(samplesRev, 0f, 1f);
            DataLogger.Log("RevCalc.ISOWhiteNoise", whiteNoise);
            var rev = new float[samplesRev];
            for (var i = 0; i < reverberationTimes.NumFrequency; i++)
            {
                // reverberationTimes.Tf[i] = Math.Min(0.7f, reverberationTimes.Tf[i]);
                // if (reverberationTimes.Tf[i] > 0.69f)
                //   Log.Warn("Cut Reverberation time to 0.7s for f=" + frequencyBand[i] + "Hz");
                // create shaped Impulse response for Reverberation Time
                // -Log(10^3) = -6.9078
                var factor = -6.9078f / (BaSettings.Instance.SampleRate * reverberationTimes.Tf[i]);

                // 2*Log(10^3) = 13.8155
                var constFactor = Mathf.Sqrt(13.8155f / reverberationTimes.Tf[i]);
                var signal = new float[samplesRev];
                for (var j = 0; j < samplesRev; j++)
                    signal[j] = constFactor * Mathf.Exp(factor * (j + 1)) * whiteNoise[j];

                var filtered=new float[signal.Length];
                filterBank.GetFilteredSignal(signal, frequencyBand[i], filtered);

                var signalEnergy = GetSignalEnergy(filtered);

                var factorAlpha = Mathf.Sqrt(frequencyBand[i] * (Mathf.Pow(2, 1f / 6f) - Mathf.Pow(2, -1f / 6f))) /
                                  Mathf.Sqrt(signalEnergy);
                for (var j = 0; j < samplesRev; j++)
                    rev[j] += factorAlpha * filtered[j];
            }

            DataLogger.Log("RevCalc.ISOReverberationTimes", reverberationTimes);
            DataLogger.Log("RevCalc.ISOFrequencyBand", frequencyBand);
            DataLogger.Log("RevCalc.IsoRev", rev);
            return NormalizeSignal(rev);
        }
        
  
    }
}
