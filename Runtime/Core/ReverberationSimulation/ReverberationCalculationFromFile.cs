﻿using System;
using log4net;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class ReverberationCalculationFromFile : ReverberationCalculator
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(ReverberationCalculationFromFile));
    
        /// <summary>
        /// if reverberationSource is ReverberationSource.ReadReverberationFromFile this AudioClip is used
        /// </summary>
        public AudioClip impulseResponseAudioClip;

        public override ReverberationCalculationMethode Type => ReverberationCalculationMethode.FromFile;
        public override FloatArrays[] ReverberationBinaural => reverberationBinaural;
        public override FloatArrays[] ReverberationAll => reverberationBinaural;
        [SerializeField] [HideInInspector] private FloatArrays[] reverberationBinaural;

        public override float[] ProcessAll(RoomAcousticsBehaviour roomAcoustics)
        {
            if (impulseResponseAudioClip == null) return new float[1];
            
            reverberationBinaural=new FloatArrays[1];
            reverberationBinaural[0]= new FloatArrays(GetReverberationTrailFromFile());
            return reverberationBinaural[0].floats;
        }
    
    
        /// <summary>
        /// Read the ReverberationTail from File AudioClip
        /// </summary>
        /// <returns>return reverberation Tail</returns>
        private float[] GetReverberationTrailFromFile()
        {
            var rev = new float[impulseResponseAudioClip.samples * impulseResponseAudioClip.channels];
            impulseResponseAudioClip.GetData(rev, 0);

            var revCut = new float[44100];
            for (var i = 0; i < revCut.Length; i++)
                revCut[i] = rev[i];

            DataLogger.Log("RevCalc.FileReverberationTrail", revCut);

            return NormalizeSignal(revCut);
        }

    }
}
