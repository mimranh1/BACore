using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using log4net;
using UnityEngine;
using Random = System.Random;

namespace BA.BACore
{
    /// <summary>
    /// Calculate the reverberation Tail for different models
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    /// 
    public  abstract class ReverberationCalculator : ExtendedScriptableObject<ReverberationCalculator,ReverberationCalculationMethode>
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(ReverberationCalculator));
        public abstract FloatArrays[] ReverberationBinaural { get; }
        public virtual List<float> AllAnglesDeg { get; }
        public abstract FloatArrays[] ReverberationAll { get; }

        public abstract float[] ProcessAll(RoomAcousticsBehaviour roomAcoustics);
        
        public virtual void Init(SoundSourceObject sourceObject, SoundReceiverObject receiverObject)
        {
            
        }

        public virtual void OnDrawGizmosSelected(RoomAcousticsBehaviour roomAcousticsBehaviour)
        {
            
        }

        public virtual void OnValidate()
        {
        
        }

        public virtual void OnReceiverUpdate()
        {
            
        }
        public virtual void OnSourceUpdate()
        {
            
        }
        public virtual void OnUpdateFilter(AudioRender audioRender)
        {
            
        }
        protected static float[] GenerateWhiteNoise(int n, float mean, float stdDev)
        {
            var rand = new Random(); //reuse this if you are generating many
            var randNormal = new float[n];
            for (var i = 0; i < n; i++)
            {
                var u1 = 1.0f - (float) rand.NextDouble(); //uniform(0,1] random doubles
                var u2 = 1.0f - (float) rand.NextDouble();
                var randStdNormal =
                    Mathf.Sqrt(-2.0f * Mathf.Log(u1)) * Mathf.Sin(2.0f * Mathf.PI * u2); //random normal(0,1)
                randNormal[i] = mean + stdDev * randStdNormal; //random normal(mean,stdDev^2)
            }

            return randNormal;
        }



        protected static float GetSignalEnergy(float[] input)
        {
            var energy = 0f;
            for (var i = 0; i < input.Length; i++)
            {
                energy += input[i] * input[i];
            }

            return energy;
        }
    
    
        protected static float[] NormalizeSignal(float[] input)
        {
            var sqrtEnergyRev = 1f/Mathf.Sqrt(GetSignalEnergy(input));
            for (var i = 0; i < input.Length; i++)
                input[i] *= sqrtEnergyRev;
            return input;
        }
    
    }
}