﻿using System.Collections.Generic;
using UnityEngine;

namespace BA.BACore
{
    public class ImageSource1
    {
        public Vector3 Position;
        public List<int> WallIndex;
        public int LastIndex;
        public int LastWallIndex;
        public bool[] isAudible;
        public float factor;
    }
}