﻿namespace BA.BACore
{
    public enum ReverberationCalculationMethode
    {
        Dirac,
        FromFile,
        SingleReverberationTime,
        ThirdOctaveBandReverberationTime,
        ImageSource,
        GeometricalAcoustics,
        ImageSourceOutdoor,
        ImageSourceOutdoorTauAngle
    }
}