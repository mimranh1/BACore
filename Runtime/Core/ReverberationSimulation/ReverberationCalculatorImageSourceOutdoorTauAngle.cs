﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace BA.BACore
{
    [Serializable]
    public class ReverberationCalculatorImageSourceOutdoorTauAngle : ReverberationCalculator
    {
        public override ReverberationCalculationMethode Type => ReverberationCalculationMethode.ImageSourceOutdoorTauAngle;

        [Range(0, 6)] public int maxOrder;

        private RoomGeometry _roomGeometryAuto;
        [SerializeField] private Transform source;
        [SerializeField] private WallBehaviour[] receivers;

        [SerializeField] private bool drawRays;
        [SerializeField] private bool drawImages;
        [SerializeField] [HideInInspector] private int reverberationLength => BaSettings.Instance.filterLengthInSamples;

        [SerializeField] [HideInInspector] private ImageSources[][] imageSources;

        [SerializeField] [HideInInspector] private FloatArrays[] reverberationMonoComplexFreq;
        [SerializeField] [HideInInspector] private List<FloatArrays> AllTaus;
        [SerializeField] [HideInInspector] private List<float> allAnglesDeg;

        public override FloatArrays[] ReverberationBinaural => AllTaus.ToArray();
        public override List<float> AllAnglesDeg => allAnglesDeg;
        public override FloatArrays[] ReverberationAll => reverberationMonoComplexFreq;
        [SerializeField][HideInInspector] private Collider[] colliders;
        [SerializeField] private LayerMask wallLayerMask;
        [SerializeField][HideInInspector] private WallBehaviour[] roomGeometryWallBehaviours;
        [SerializeField][HideInInspector] private AngleTauData[] wallTaus;
        public InterpolatorSelector interpolator;
        private Stopwatch sw;
        [SerializeField] private float reflectionFactor = 0.8f;

        [SerializeField] private DirectivityMS directivityMs;

        //[SerializeField] private DirectivityMS directivityMS;

        private void OnEnable()
        {
            if (interpolator == null)
                interpolator = CreateInstance<InterpolatorSelector>();
        }

        public override void OnValidate()
        {
            OnEnable();
        }

        public float SamplesByMeter
        {
            get
            {
                if (samplesByMeter <= 0f)
                    samplesByMeter = BaSettings.Instance.SampleRate / BaSettings.Instance.C0;
                return samplesByMeter;
            }
        }

        [SerializeField] private float samplesByMeter = -1f;     
        public float ConstFactor
        {
            get
            {
                if (constFactor <= 0f)
                    constFactor = Mathf.Sqrt(1 / (Mathf.PI * 4f)) ;
                return constFactor;
            }
        }

        [SerializeField] private float constFactor = -1f;

        public override float[] ProcessAll(RoomAcousticsBehaviour roomAcoustics)
        {
            sw = new Stopwatch();
            Init(roomAcoustics);

            OnReceiverUpdate();

            if (drawImages)
                DrawImages();
            OnSourceUpdate();
            //DrawAudibleSources();

            if (drawRays)
                DrawRays();
            return new float[1];
        }

        public override void Init(SoundSourceObject sourceObject, SoundReceiverObject receiverObject)
        {
            source = sourceObject.transform;
        }

        private void Init(RoomAcousticsBehaviour roomAcoustics)
        {
            sw.Restart();
            if (directivityMs != null)
                directivityMs.InitDaffReader();
            AllTaus=new List<FloatArrays>();
            allAnglesDeg=new List<float>();
            wallLayerMask = BaSettings.Instance.LayerMaskWall;
            if (roomAcoustics != null)
                roomGeometryWallBehaviours = roomAcoustics.RoomGeometry.WallBehaviours;
            colliders = new Collider[roomGeometryWallBehaviours.Length];
            for (var i = 0; i < colliders.Length; i++)
            {
                colliders[i] = roomGeometryWallBehaviours[i].GetComponent<Collider>();
            }
            reverberationMonoComplexFreq=new FloatArrays[receivers.Length];
            for (var i = 0; i < receivers.Length; i++)
            {
                reverberationMonoComplexFreq[i] = new FloatArrays(2 * reverberationLength);
            }

            if (roomAcoustics != null)
                _roomGeometryAuto = roomAcoustics.RoomGeometry;
            wallTaus=new AngleTauData[receivers.Length];
            for (int i = 0; i < receivers.Length; i++)
            {
                wallTaus[i]= new AngleTauData(90, receivers[i].ReductionIndex, interpolator.@object,
                    reverberationLength);
            }
            sw.Stop();
            Debug.Log("Init in " + sw.ElapsedMilliseconds + " ms.");
        }

        public override void OnDrawGizmosSelected(RoomAcousticsBehaviour roomAcousticsBehaviour)
        {
            // if (drawImages || drawRays)
            //     ProcessAll(roomAcousticsBehaviour);
        }

        public override void OnSourceUpdate()
        {
            sw.Restart();
            CheckAudibiliy();
            sw.Stop();
            // Debug.Log("OnSourceUpdate in " + sw.ElapsedMilliseconds + " ms.");
        }

        public override void OnUpdateFilter(AudioRender audioRender)
        {
            sw.Restart();
            GenerateImpulseResponse(false);
            sw.Stop();
            // Debug.Log("OnUpdateFilter in " + sw.ElapsedMilliseconds + " ms.");
        }


        public override void OnReceiverUpdate()
        {
            sw.Restart();
            CreateImages();
            CheckAudibiliy();
            sw.Stop();
            // Debug.Log("OnReceiverUpdate in " + sw.ElapsedMilliseconds + " ms.");
        }

        public void AddImageToReverberationMonoTau(ImageSource image, int i)
        {
            var distance = Vector3.Distance(source.position, image.Position);
            var incAngleRad = image.incidentAngleRad;
            // incAngleRad = 45f*Mathf.Deg2Rad; // 45°
            Debug.Log("incidentAngle " + incAngleRad * Mathf.Rad2Deg);
            var distanceSamples = (int) (distance * SamplesByMeter)/2;
            distanceSamples *= 2;
            var tau = new float[reverberationLength * 2];
             wallTaus[i].GetNearestTauSqrtTauComplexRad(incAngleRad, tau);
            // var tau = new float[reverberationLength * 2];
            // for (int j = 0; j < tau.Length; j += 2)
            // {
            //     tau[j] = 1f;
            // }
            Debug.Log("distanceSamples " + distanceSamples);

            ComplexFloats.FreqAddDelay(tau, distanceSamples, 44100);
            if (directivityMs!=null)
            {
                directivityMs.GetAngles(source.position, source.forward, source.up,image.Position,
                    out float azimuth, out float elevation);
                Debug.Log($"azimuth: {azimuth} elevation:{elevation}");
                directivityMs.ApplyOnSpectrum(azimuth, elevation, tau);
            }            
            var factor = Mathf.Sqrt(Mathf.Abs(Mathf.Cos(incAngleRad))) * ConstFactor * image.factor / distance;
            for (int n = 0; n < tau.Length; n++)
            {
                tau[n] *= factor;
            }

            if (i == 0)
                AllTaus.Add(new FloatArrays(tau));
            allAnglesDeg.Add(image.factor);
            allAnglesDeg.Add(incAngleRad * Mathf.Rad2Deg);
            for (int n = 0; n < tau.Length; n++)
            {
                reverberationMonoComplexFreq[i].floats[n] += tau[n];
            }
        }

        private void CheckAudibiliy()
        {
            for (var iReceiver = 0; iReceiver < imageSources.Length; iReceiver++)
            {
                for (var order = imageSources[iReceiver].Length - 1; order >= 0; order--)
                {
                    for (var j = 0; j < imageSources[iReceiver][order].sources.Count; j++)
                    {
                        var angleRad = CheckRay(iReceiver, order, j, source.position, Color.black);
                        imageSources[iReceiver][order].sources[j].incidentAngleRad = angleRad;
                        imageSources[iReceiver][order].sources[j].isAudible = angleRad>0;
                    }
                }
            }
        }

        private void GenerateImpulseResponse(bool isMono = true)
        {
            foreach (var reverberationMonoa in reverberationMonoComplexFreq)
            {
                reverberationMonoa.Clear();
            }
            AllTaus.Clear();
            AllAnglesDeg.Clear();

            for (var iReceiver = 0; iReceiver < imageSources.Length; iReceiver++)
            for (var order = 0; order < imageSources[iReceiver].Length; order++)
            {
                var images = imageSources[iReceiver][order];
                foreach (var image in images.sources)
                {
                    if (image.isAudible)
                        AddImageToReverberationMonoTau(image, iReceiver);
                }
            }
        }


        private void DrawRays()
        {
            var colors = new[] {Color.blue, Color.red, Color.green, Color.magenta};
            for (var iReceiver = 0; iReceiver < imageSources.Length; iReceiver++)
            {
                var imageSource = imageSources[iReceiver];
                for (var order = imageSource.Length - 1; order >= 0; order--)
                {
                    for (var j = 0; j < imageSource[order].sources.Count; j++)
                    {
                        if (!imageSource[order].sources[j].isAudible) continue;
                        if (drawRays)
                            CheckRay(iReceiver,order, j, source.position, colors[order % colors.Length], true);
                    }
                }
            }
        }

        private float CheckRay(int receiverId, int order, int index, Vector3 pos, Color color, bool plot = false)
        {
            while (true)
            {
                var imageSource = imageSources[receiverId][order].sources[index];
                var lastIndexes = imageSource.LastIndex;
                if (lastIndexes == -1)
                {
                    //DrawImage(pos, receiver.position,Color.cyan);
                    if (Raycast(pos, imageSource.Position, out var hit1)) return -1f;
                    if (plot) DrawImage(pos, imageSource.Position, color);
                    var direction = imageSource.Position - pos; //Vector3.RotateTowards(pos, posImage, 360, 1);

                    var angleRad = Mathf.Acos(Vector3.Dot(receivers[receiverId].Geometry.Normal, direction.normalized));
                    angleRad = angleRad > (Mathf.PI / 2) ? Mathf.PI - angleRad : angleRad;
                    return angleRad;
                }

                var posImage = imageSource.Position;

                //Debug.Log("PosOrg" + pos + " Ziel " + posImage);
                if (!Raycast(pos, posImage, out var hit)) return -1f;


                if (hit.collider != colliders[imageSource.LastWallIndex]) return -1f;
                var position = hit.point;
                if (plot) DrawImage(pos, position, color);
                order -= 1;
                index = lastIndexes;
                pos = position;
            }
        }

        private bool Raycast(Vector3 pos, Vector3 posImage, out RaycastHit hit)
        {
            var direction = posImage - pos; //Vector3.RotateTowards(pos, posImage, 360, 1);
            var dist = Vector3.Distance(pos, posImage);
            var raycast = Physics.Raycast(pos, direction, out hit, dist, wallLayerMask);
            // Debug.DrawRay(pos,direction*hit.distance,Color.red);
            return raycast;
        }

        private void DrawImage(Vector3 start, Vector3 end, Color color)
        {
            Debug.DrawRay(start, end - start, color,10);
            //Debug.Log($"{start} {end} {color.ToString()}");
        }

        private void CreateImages()
        {
            imageSources = new ImageSources[receivers.Length][];
            for (var iReceiver = 0; iReceiver < receivers.Length; iReceiver++)
            {
                imageSources[iReceiver] = new ImageSources[maxOrder + 1];
                var sourcePosition = receivers[iReceiver].Geometry.Position;
                var imageSource0 = new ImageSource
                {
                    Position = sourcePosition,
                    WallIndex = new List<int>(0),
                    Normal = receivers[iReceiver].Geometry.Normal,
                    LastIndex = -1,
                    factor = 1f,
                };
                imageSources[iReceiver][0] = new ImageSources();
                imageSources[iReceiver][0].sources.Add(imageSource0);
                for (var order = 1; order < maxOrder + 1; order++)
                {
                    imageSources[iReceiver][order] = new ImageSources();
                    for (var index = 0; index < imageSources[iReceiver][order - 1].sources.Count; index++)
                    {
                        var currentImage = imageSources[iReceiver][order - 1].sources[index];
                        for (var i = 0; i < _roomGeometryAuto.WallBehaviours.Length; i++)
                        {
                            var wallBehaviour = _roomGeometryAuto.WallBehaviours[i];
                            var wallPos = wallBehaviour.Geometry.Position;
                            var wallNormal = wallBehaviour.Geometry.Normal;
                            var image = 2 * Vector3E.MultiplyElementwise(wallPos - currentImage.Position, wallNormal) +
                                        currentImage.Position;
                            var normal = currentImage.Normal + 180f * wallNormal;
                            var imageSource = new ImageSource
                            {
                                Position = image,
                                WallIndex = new List<int>(),
                                LastIndex = index,
                                Normal = normal,
                                factor = currentImage.factor * reflectionFactor,
                            };
                            imageSource.WallIndex.AddRange(currentImage.WallIndex);
                            imageSource.WallIndex.Add(wallBehaviour.GetInstanceID());
                            imageSource.LastWallIndex = i;
                            imageSources[iReceiver][order].Add(imageSource);
                        }
                    }
                }
            }

        }

        private void DrawImages()
        {
            for (int iReceiver = 0; iReceiver < imageSources.Length; iReceiver++)
                for (var order = 1; order < imageSources[iReceiver].Length; order++)
                {
                    var imageSource = imageSources[iReceiver][order];
                    foreach (var image in imageSource.sources)
                    {
                        Debug.DrawLine(image.Position, imageSources[iReceiver][order - 1].sources[image.LastIndex].Position,
                            Color.yellow, 10);
                    }
                }
        }

        private void DrawAudibleSources()
        {
            var colors = new[] {Color.blue, Color.red, Color.green, Color.magenta};
            for (var iReceiver = 0; iReceiver < imageSources.Length; iReceiver++)
            for (var order = 0; order < imageSources[iReceiver].Length; order++)
            {
                var imageSource = imageSources[iReceiver][order];
                for (var j = 0; j < imageSource.sources.Count; j++)
                {
                    var image = imageSource.sources[j];
                    if (!image.isAudible) continue;
                    if (drawRays)
                        CheckRay(iReceiver, order, j, source.position, colors[order % colors.Length], true);
                }
            }
        }

        private void OnDrawGizmosSelected()
        {
                
        }
    }
}