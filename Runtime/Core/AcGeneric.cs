using System;
using log4net;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public abstract class AcGeneric :AuralisationCalculation
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(AcGeneric));
        
        protected float[] IrLeftTime;
        protected float[] IrRightTime;
        protected float[][] RTReceiver;

        public override SoundSourceObject[] SoundSourceObjects => new[] {soundSourceObject};
        [SerializeField] protected SoundSourceObject soundSourceObject;

        public override SoundReceiverObject SoundReceiverObject => soundReceiverObject;
        [SerializeField] protected SoundReceiverObject soundReceiverObject;
        
        
        /// <summary>
        /// Implementation of Calculate pre-calculation and and set AudioRenderer
        /// </summary>
        /// <param name="ar">AudioRenderer from FilterManager</param>
        protected override void OfflineCalculation()
        {
            IrLeftTime = new float[FilterResolution];
            IrRightTime = new float[FilterResolution];

            ForceReceiverUpdate = true;
            ForceSourceUpdate = true;
            
            RTReceiver = new float[2][];
            RTReceiver[0] = new float[FilterResolution * 2];
            RTReceiver[1] = new float[FilterResolution * 2];

            SoundSourceObjects[0].Init(FilterResolution);
            SoundReceiverObject.Init(FilterResolution);
        }

    }
}