﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using log4net;
using UnityEngine;
using Object = UnityEngine.Object;

namespace BA.BACore
{
    /// <summary>
    /// Implementation of new Approach Outdoor considering Direct and reverberation in Source and Receiver room
    /// First try to bake the results beforehand so that the moving secondary source is baked up to the secondary source of the patch in the receiver room
    /// not working well
    /// This class just run with VA and therefore it has an own renderer and deactivate the default one
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    [Serializable]
    public class AcGenericBinaural : AcGeneric
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(AcGenericBinaural));

        [SerializeField] public AudioLoaderBase audioLoader;

        [SerializeField] public string filterPath =
            "C:/Users/anne/Mimcloud/LiverPool/sound source/1. Convolved_binaural/impulse_responses";


        public override AuralisationCalculationType Type => AuralisationCalculationType.GenericModelBinaural;

        public override void OnValidate()
        {
        }

        /// <summary>
        /// Overload of Initialisation with Init BuildingAcoustics Array from FilterManager
        /// </summary>
        /// <param name="bas">buildingAcoustics Array from FilterManager</param>
        protected override void Init()
        {
            CultureInfo.CreateSpecificCulture("en-GB");
            init = true;
            audioLoader = Object.FindObjectOfType<AudioLoaderBase>();
            // sourceMover = buildingAcoustics?[0].sourceRoom.soundObject.GetComponent<SourceMover>();
        }


        /// <summary>
        /// Implementation of Calculate pre-calculation and and set AudioRenderer
        /// </summary>
        /// <param name="ar">AudioRenderer from FilterManager</param>
        protected override void OfflineCalculation()
        {
            base.OfflineCalculation();

            DefineAudioFilters();
        }


        /// <summary>
        /// Implementation of Realtime Update, called if Source or Receiver Updates
        /// </summary>
        /// <param name="logId">write log file if logId > 1</param>
        protected override void RealTimeCalculation(AudioRender audioRender,bool log=false)
        {
            UpdateFilter(audioRender);
        }

        /// <summary>
        /// Implementation of Update Receiver Data for filter Construction if Receiver Update
        /// </summary>
        /// <returns>Receiver Update</returns>
        protected override bool RealTimeReceiverUpdate()
        {
            return false;
        }

        /// <summary>
        /// Implementation of Update Source Data for filter Construction if Source Update
        /// </summary>
        /// <returns>Source Update</returns>
        protected override bool RealTimeSourceUpdate()
        {
            var posUpdate = false;
            if (SoundSourceObjects[0].SoundObjectChanged() | ForceSourceUpdate)
            {
                var sourcePosition = soundSourceObject.transform.position;
                var receiverPosition = SoundReceiverObject.transform.position;
                audioLoader.GetFilterFreq(IrLeftTime, sourcePosition, receiverPosition, 0);
                audioLoader.GetFilterFreq(IrRightTime, sourcePosition, receiverPosition, 1);
                posUpdate = true;
            }

            ForceSourceUpdate = false;
            return posUpdate;
        }

        public override void SaveImpulseResponse(string name="")
        {
            throw new NotImplementedException();
        }


        public void DefineAudioFilters()
        {
            if (!Directory.Exists(filterPath))
                //error
                return;

            audioLoader.filterPath = filterPath;
            audioLoader.filterLength = FilterResolution;
            audioLoader.ReadAudio();
        }

        private void UpdateFilter(AudioRender ar)
        {
            if (!audioLoader.allLoaded)
                return;
            ar.UpdateFilter(IrLeftTime, IrRightTime);
            if (LogRealtime)
            {
                DataLogger.DebugData = true;
                DataLogger.Log(LogName + ".irLeftTime(:," + HrirCounter + ")", IrLeftTime);
                DataLogger.Log(LogName + ".irRightTime(:," + HrirCounter + ")", IrRightTime);
                DataLogger.DebugData = false;
            }
        }

        private string Vec32Str(Vector3 vec)
        {
            CultureInfo.CurrentCulture = new CultureInfo("en-GB");
            return vec.x.ToString("000.0") + "_" + vec.y.ToString("000.0") + "_" + vec.z.ToString("000.0");
        }
    }
}