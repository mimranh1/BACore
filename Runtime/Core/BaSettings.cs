﻿using System;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace BA.BACore
{
    /// <summary>
    /// Contains all default constants and variables
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    [CreateAssetMenu]
    public class BaSettings : ScriptableObject
    {
        private static BaSettings _instance = null;

        public static BaSettings Instance
        {
            get
            {
                if (!_instance)
                    _instance = Resources.FindObjectsOfTypeAll<BaSettings>().FirstOrDefault();
                if(!_instance)
                    Debug.LogError("BASettings not found, please reimport or create BASettings Object.");
                return _instance;
            }
        }
        
        
        [Subtitle] [SerializeField] private string constants;

        /// <summary>
        /// 
        /// </summary>
        public int filterLengthInSamples = 88200;

        public string RootPath
        {
            get
            {
                var projectRoot=Path.Combine( Application.dataPath, "../" );
                var path = $"{projectRoot}/{AssetDatabase.GetAssetPath(_instance)}";
                path=Path.GetFullPath( path );
                return Path.GetDirectoryName(path);
            }
        }

        public string SoundsPath => $"{RootPath}/Resources/Sounds";
        
        public string DataPath => $"{RootPath}/.Data";
        public string LogPath => $"{DataPath}/Log";
        /// <summary>
        /// Velocity of Sound in Air in m/s
        /// </summary>
        [Tooltip("Speed of sound m/s")]
        public float C0 = 340;

        /// <summary>
        /// density of air (kg/m^3)
        /// </summary>
        public float Rho0 = 1.29f;

        /// <summary>
        /// Standard Area (m^2)
        /// </summary>
        public float A0 = 10f;

        /// <summary>
        /// Reference Mass in kg
        /// </summary>
        public float M0 = 1;

        /// <summary>
        /// Reference Frequency in Hz
        /// </summary>
        public float Fref = 1000;

        /// <summary>
        /// Sabine constant
        /// </summary>
        public float Csab = 0.163f;

        /// <summary>
        /// Reference Mass in kg
        /// </summary>
        public float ReferenceMass = 1;

        /// <summary>
        /// Sample rate in Hz
        /// </summary>
        public float SampleRate = 44100;

        /// <summary>
        /// Euler number
        /// </summary>
        public float Euler = 2.71828182845904523536028747135266249775724709369995f;

        /// <summary>
        /// Define Culture Code for Project
        /// </summary>
        public string CultureCode = "en-gb";

        [Subtitle] [SerializeField] private string layer;
 
        /// <summary>
        /// Define the Layer of the Wall (used for Raytracing)
        /// </summary>
        public int LayerWall = 8;

        /// <summary>
        /// Return the Layer Mask of Wall based on LayerWall
        /// </summary>
        public  int LayerMaskWall => 1 << LayerWall;

        
        /// <summary>
        /// Frequency Band 1/3 Oct from 20 to 20k Hz
        /// Octave20 = [0 8 10 12.5 16 20 25 31.5 40 50 63 80 100 125 160 200 250 315 400 500 630 800 1000 1250 1600 2000 2500
        /// 3150 4000 5000 6300 8000 10000 12500 16000 20000 25000];
        /// </summary>
        public TransferFunction ThirdOctaveFrequencyBandExtended
        {
            get
            {
                var tf = new TransferFunction(37)
                {
                    Tf =
                    {
                        [0] = 0.0f,
                        [1] = 8.0f,
                        [2] = 10.0f,
                        [3] = 12.5f,
                        [4] = 16.0f,
                        [5] = 20.0f,
                        [6] = 25.0f,
                        [7] = 31.5f,
                        [8] = 40.0f,
                        [9] = 50.0f,
                        [10] = 63.0f,
                        [11] = 80.0f,
                        [12] = 100.0f,
                        [13] = 125.0f,
                        [14] = 160.0f,
                        [15] = 200.0f,
                        [16] = 250.0f,
                        [17] = 315.0f,
                        [18] = 400.0f,
                        [19] = 500.0f,
                        [20] = 630.0f,
                        [21] = 800.0f,
                        [22] = 1000.0f,
                        [23] = 1250.0f,
                        [24] = 1600.0f,
                        [25] = 2000.0f,
                        [26] = 2500.0f,
                        [27] = 3150.0f,
                        [28] = 4000.0f,
                        [29] = 5000.0f,
                        [30] = 6300.0f,
                        [31] = 8000.0f,
                        [32] = 10000.0f,
                        [33] = 12500.0f,
                        [34] = 16000.0f,
                        [35] = 20000.0f,
                        [36] = 25000.0f
                    }
                };

                return tf;
            }
        }


        /// <summary>
        /// Frequency Band 1/3 Oct from 20 to 20k Hz
        /// Octave20 = [20 25 31.5 40 50 63 80 100 125 160 200 250 315 400 500 630 800 1000 1250 1600 2000 2500
        /// 3150 4000 5000 6300 8000 10000 12500 16000 20000];
        /// </summary>
        public TransferFunction ThirdOctaveFrequencyBandNorm
        {
            get
            {
                var tf = new TransferFunction(31)
                {
                    Tf =
                    {
                        [0] = 20.0f,
                        [1] = 25.0f,
                        [2] = 31.5f,
                        [3] = 40.0f,
                        [4] = 50.0f,
                        [5] = 63.0f,
                        [6] = 80.0f,
                        [7] = 100.0f,
                        [8] = 125.0f,
                        [9] = 160.0f,
                        [10] = 200.0f,
                        [11] = 250.0f,
                        [12] = 315.0f,
                        [13] = 400.0f,
                        [14] = 500.0f,
                        [15] = 630.0f,
                        [16] = 800.0f,
                        [17] = 1000.0f,
                        [18] = 1250.0f,
                        [19] = 1600.0f,
                        [20] = 2000.0f,
                        [21] = 2500.0f,
                        [22] = 3150.0f,
                        [23] = 4000.0f,
                        [24] = 5000.0f,
                        [25] = 6300.0f,
                        [26] = 8000.0f,
                        [27] = 10000.0f,
                        [28] = 12500.0f,
                        [29] = 16000.0f,
                        [30] = 20000.0f
                    }
                };

                return tf;
            }
        }

        /// <summary>
        /// Frequency Band 1/3 Oct from 50 to 5k Hz
        /// </summary>
        public TransferFunction ThirdOctaveFrequencyBandShort
        {
            get
            {
                var tf = new TransferFunction(21)
                {
                    Tf =
                    {
                        [0] = 50.0f,
                        [1] = 63.0f,
                        [2] = 80.0f,
                        [3] = 100.0f,
                        [4] = 125.0f,
                        [5] = 160.0f,
                        [6] = 200.0f,
                        [7] = 250.0f,
                        [8] = 315.0f,
                        [9] = 400.0f,
                        [10] = 500.0f,
                        [11] = 630.0f,
                        [12] = 800.0f,
                        [13] = 1000.0f,
                        [14] = 1250.0f,
                        [15] = 1600.0f,
                        [16] = 2000.0f,
                        [17] = 2500.0f,
                        [18] = 3150.0f,
                        [19] = 4000.0f,
                        [20] = 5000.0f
                    }
                };

                return tf;
            }
        }
        /// <summary>
        /// Frequency Band Octave Band from 20 to 20k Hz
        /// </summary>
        public TransferFunction OctaveFrequencyBand
        {
            get
            {
                var tf = new TransferFunction(10)
                {
                    Tf = new [] {31.5f, 63.0f, 125.0f, 250.0f, 500.0f, 1000.0f, 2000.0f, 4000.0f, 8000.0f, 16000.0f}
                };

                return tf;
            }
        }

        
        
        /// <summary>
        /// get interpolated FrequencyBand from 20 to 20k
        /// </summary>
        /// <returns></returns>
        public TransferFunction GetInterpolatedFrequencyBand(int numBins)
        {
            var interpolatedFreqBand = new TransferFunction(numBins);
            var stepFreq = 22050.0f / numBins;
            for (var i = 0; i < numBins; i++)
                interpolatedFreqBand.Tf[i] = stepFreq * (i + 1);

            return interpolatedFreqBand;
        }

        /// <summary>
        /// Wave Numbers
        /// </summary>
        public TransferFunction K0 => 2.0f * (float) Math.PI / C0 * ThirdOctaveFrequencyBandShort;
        
        
  

        [Subtitle] private string directivity;

        [ContextMenu("WriteAcousticalMaterialDatabase")]
        private void WriteAcousticalMaterialDatabase()
        {
            AcousticMaterialData.CreateAssets();
        }

        [ContextMenu("WriteDirectivityDatabase")]
        private void WriteDirectivityDatabase()
        {
            DirectivityDaff.CreateAssets();
        }

        [ContextMenu("DeleteDirectivityDatabase")]
        private void DeleteDirectivityDatabase()
        {
            DirectivityDaff.DeleteAssets();
        }
        
        [ContextMenu("WriteFilterDatabase")]
        private void WriteFilterDatabase()
        {
                FilterBank.CreateAssets();
        }

        [ContextMenu("DeleteFilterDatabase")]
        private void DeleteFilterDatabase()
        {
            FilterBank.DeleteAssets();
        }
        
        [ContextMenu("WriteBastianDatabase")]
        private void WriteBastianDatabase()
        {
            ReductionIndexData.CreateAssets();
        }
        [ContextMenu("WriteMaterialDatabase")]
        private void WriteMaterialDatabase()
        {
            MaterialStructureData.CreateAssets();
        }
        [ContextMenu("WriteAdditionalLayerDatabase")]
        private void WriteAdditionalLayerDatabase()
        {
            MaterialAddLayerData.CreateAssets();
        }
    }
}