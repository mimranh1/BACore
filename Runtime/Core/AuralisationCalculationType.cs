﻿namespace BA.BACore
{
    public enum AuralisationCalculationType
    {
        FreeField,
        ClassicalIndoorModel,
        
        BuildingAcousticsModel,
        ImageSource,

        // /**< New Approach Indoor Model (AcIndoor) */
        // IndoorModelPatches,
        //
        // /**< New Approach Indoor Model with Patches (AcIndoor) */
        // IndoorModelPatchesBinRev,
        //
        // /**< New Approach Indoor Model with Patches and Binaural Reverberation(AcIndoor) */
        // OutdoorModel,
        //
        // /**< New Approach Outdoor Model (AcOutdoor) */
        // OutdoorModelBaked,
        //
        // /**< Offline Baked up to SS than realtime Receiver Update (AcOutdoorBaked) */
        // OutdoorModelStaticBaked,
        //
        // /**< Offline Baked full BRIR (AcOutdoorStaticBaked) */
        // OutdoorModelStaticBakedRealTime,
        //
        // /**< Based on AcOutdoorStaticBaked with ReceiverRoom Update (AcOutdoorStaticBakedRealtime) */
        // OutdoorModelRealTime,


        /**< Thaden Formula faster Implementation of AcClassicalIndoorThaden mono Rev (AcClassicalIndoorFormula) */
        GenericModelMonoAndHrtf,

        /**<  */
        GenericModelBinaural,

        // /**<  */
        // OutdoorDavyPreCalc,
        //
        // OutdoorDavyPreCalcSinglePatch,
    }
}