﻿namespace BA.BACore
{
    /// <summary>
    /// Base Class for the auralisation calculation
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>

    public class AuralisationCalculationSelector : Selector<AuralisationCalculation, AuralisationCalculationType>
    {
        
    }
}