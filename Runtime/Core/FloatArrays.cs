﻿using System;

namespace BA.BACore
{
    [Serializable]
    public class FloatArrays
    {
        public float[] floats;

        public int Length => floats.Length;

        public float this[int i]
        {
            get => floats[i];
            set => floats[i] = value;
        }

        public FloatArrays(int length)
        {
            floats = new float[length];
        }

        public FloatArrays()
        {
            floats = new float[0];
        }

        public FloatArrays(float[] input)
        {
            floats = input;
        }

        public void Clear()
        {
            Array.Clear(floats,0,floats.Length);
        }
    }
}