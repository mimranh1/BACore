﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class AcFreeField : AuralisationCalculation
    {
        public override AuralisationCalculationType Type => AuralisationCalculationType.FreeField;
        public override SoundSourceObject[] SoundSourceObjects => soundSourceObjects;
        [SerializeField] private SoundSourceObject[] soundSourceObjects;
        public override SoundReceiverObject SoundReceiverObject => soundReceiverObject;
        [SerializeField] private SoundReceiverObject soundReceiverObject;
        

       [SerializeField] private float[][] _hrtfTime;

        private bool[] updatedSources;

        [SerializeReference] private TimeBinauralPlotContainer timeBinauralPlots;

        public override void OnValidate()
        {
            ForceReceiverUpdate = true;
            if(_hrtfTime==null)
                InitHrtfArray();
        }
        private void OnValidateTimePlots(int index,float[] timeLeft,float[] timeRight)
        {
            if (timeBinauralPlots == null)
                timeBinauralPlots = new TimeBinauralPlotContainer(soundSourceObjects.Length);
            timeBinauralPlots.OnValidate(index, timeLeft, timeRight);
        }

      
        private void InitBinauralImpulseResponses()
        {

            updatedSources = new bool[soundSourceObjects.Length];
        }

        protected override void Init()
        {
            InitBinauralImpulseResponses();
            
        }

        protected override void OfflineCalculation()
        {
            // for (var i = 0; i < soundSourceObjects.Length; i++)
            //     soundSourceObjects[i].Init();
            soundReceiverObject.Init();
            InitHrtfArray();
        }

        protected override void RealTimeCalculation(AudioRender audioRender,bool log=false)
        {
            for (var i = 0; i < SoundSourceObjects.Length; i++)
            {
                if (!updatedSources[i] && !ForceReceiverUpdate) continue;

                var samplesChanges=soundReceiverObject.GetBinauralDirectivity(true, _hrtfTime, soundSourceObjects[i].transform.position,
                    1f,true, true);
                audioRender.UpdateFilter(_hrtfTime[0], _hrtfTime[1], i);
                Debug.Log($"Updated RealTimeCalculation {i}");
                
                OnValidateTimePlots(i, _hrtfTime[0], _hrtfTime[1]);
                Array.Clear(_hrtfTime[0], samplesChanges[0], samplesChanges[1]);
                Array.Clear(_hrtfTime[1], samplesChanges[0], samplesChanges[1]);

            }

            ForceReceiverUpdate = false;
            ForceSourceUpdate = false;
        }
        private void InitHrtfArray()
        {
            _hrtfTime = new float[2][];
            for (var i = 0; i < _hrtfTime.Length; i++)
            {
                _hrtfTime[i] = new float[FilterResolution];
            }
        }

        protected override bool RealTimeReceiverUpdate()
        {
            ForceReceiverUpdate = ForceReceiverUpdate || soundReceiverObject.SoundObjectChanged();
            return ForceReceiverUpdate;
        }

        protected override bool RealTimeSourceUpdate()
        {
            var anyUpdate = false;
            if (updatedSources == null)
                InitBinauralImpulseResponses();
            for (var i = 0; i < soundSourceObjects.Length; i++)
            {
                updatedSources[i] =  ForceSourceUpdate||soundSourceObjects[i].SoundObjectChanged();
                if (updatedSources[i])
                {
                    
                    anyUpdate = true;
                }
            }

            return anyUpdate;
        }

        public override void SaveImpulseResponse(string name="")
        {
            throw new System.NotImplementedException();
        }
    }
}
