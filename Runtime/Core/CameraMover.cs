﻿using System;
using UnityEngine;

namespace BA.BACore
{
    public class CameraMover : MonoBehaviour
    {
        public float speedH = 2.0f;
        public float speedV = 2.0f;

        private float _yaw = 0.0f;
        private float _pitch = 0.0f;

        private Transform _transform1;

        private void Start()
        {
            _transform1 = transform;
            var eulerAngles = _transform1.eulerAngles;
            _pitch = eulerAngles.x;
            _yaw = eulerAngles.y;
        }

        void Update()
        {
            if (Input.GetMouseButton(0))
            {
                _yaw += speedH * Input.GetAxis("Mouse X");
                _pitch -= speedV * Input.GetAxis("Mouse Y");

                _transform1.eulerAngles = new Vector3(_pitch, _yaw, 0.0f);
            }

            if (Input.GetKey(KeyCode.W))
                _transform1.position += _transform1.forward * Time.deltaTime;
            if (Input.GetKey(KeyCode.S))
                _transform1.position -= _transform1.forward * Time.deltaTime;
            if (Input.GetKey(KeyCode.D))
                _transform1.position += _transform1.right * Time.deltaTime;
            if (Input.GetKey(KeyCode.A))
                _transform1.position -= _transform1.right * Time.deltaTime;
        }
    }
}