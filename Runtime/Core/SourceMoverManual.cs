﻿using UnityEngine;

namespace BA.BACore
{
    public class SourceMoverManual : MonoBehaviour
    {
        public float speed=10f;
        private Transform _transform1;

        private void Start()
        {
            _transform1 = transform;
        }

        void Update()
        {
            if (Input.GetKey(KeyCode.LeftArrow))
                _transform1.position += Vector3.left * (Time.deltaTime * speed);
            if (Input.GetKey(KeyCode.RightArrow))
                _transform1.position += Vector3.right * (Time.deltaTime * speed);
        }
    }
}