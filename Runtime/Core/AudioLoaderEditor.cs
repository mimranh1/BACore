using System.Globalization;
using BA.BACore;
using UnityEditor;
using UnityEngine;


[CustomEditor(typeof(AudioLoaderFft))]
[System.Serializable]
public class AudioLoaderEditor : Editor
{
    private AudioLoaderFft audioLoaderFft;

    private void OnEnable()
    {
        audioLoaderFft = (AudioLoaderFft) target;
    }

    /// <summary>
    /// overrides the Inspector view in the Editor mode
    /// </summary>
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Start")) audioLoaderFft.ReadAudioMonoFreq();
    }
}