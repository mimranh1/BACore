﻿using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

namespace BA.BACore
{
    public class AudioLoaderBase : MonoBehaviour
    {
        public float stepLength = 1f;
        public int numIndexes = 300;

        public string pattern = "*.J";
        public string filterPath;
        public Vector3 directionLine;

        public int filterLength;

        private float[] _dirPoints;
        private bool[] _loaded;

        private float[][][] _irAudioData;

        public bool allLoaded;

        public void ReadAudio()
        {
            _loaded = new bool[numIndexes];
            _dirPoints = new float[numIndexes];
            _irAudioData = new float[numIndexes][][];

            for (var i = 0; i < numIndexes; i++)
            {
                _dirPoints[i] = (i - numIndexes / 2) / stepLength;
                var fileNames = Directory.GetFiles(filterPath, pattern + (i + 1) + ".?av");
                var fullPath = fileNames[0];
                fullPath = "file:///" + fullPath;
                StartCoroutine(GetAudioClip2Array(fullPath, i));
                // GetAudioClip2ArrayOffline(fullPath, i);
            }

            var rout = StartCoroutine(WaitForComplete());
        }

        public void GetFilter(float[] filter, Vector3 sourcePos, Vector3 receiverPos)
        {
            if (!allLoaded)
                StartCoroutine(WaitForComplete());

            var pos = Vector3.Dot(receiverPos - sourcePos, directionLine);
            var index = GetClosestIndex(_dirPoints, pos);
            // audioClips[index].GetData(filter, 0);
        }

        public void GetFilterFreq(float[] filter, Vector3 sourcePos, Vector3 receiverPos, int channel)
        {
            if (!allLoaded)
            {
                Debug.LogWarning("Filter not Loaded yet.");
                return;
            }

            var pos = Vector3.Dot(receiverPos - sourcePos, directionLine);
            var index = GetClosestIndex(_dirPoints, pos);

            _irAudioData[index][channel].CopyTo(filter, 0);


            // audioClips[index].GetData(filter, 0);
        }

        private int GetClosestIndex(float[] array, float cmp)
        {
            if (cmp < array[0])
            {
                if (array[0] - cmp > stepLength)
                    Debug.LogWarning("Position Out of Range");
                return 0;
            }

            var lastIndex = array.Length - 1;
            if (cmp > array[lastIndex])
            {
                if (array[lastIndex] - cmp > stepLength)
                    Debug.LogWarning("Position Out of Range");
                return lastIndex;
            }

            var minValue = Mathf.Infinity;

            for (var i = 0; i < array.Length; i++)
            {
                var diff = Mathf.Abs(array[i] - cmp);
                if (minValue < diff)
                {
                    minValue = diff;
                }
                else
                {
                    if (diff < minValue)
                        return i;
                    return i - 1;
                }
            }

            return -1;
        }

        private IEnumerator GetAudioClip2Array(string url, int index)
        {
            using (var uwr = UnityWebRequestMultimedia.GetAudioClip(url, AudioType.WAV))
            {
                yield return uwr.SendWebRequest();
                if (uwr.isNetworkError || uwr.isHttpError)
                {
                    Debug.LogError(uwr.error);
                    yield break;
                }

                var audioClip = DownloadHandlerAudioClip.GetContent(uwr);
                var numChannel = audioClip.channels;
                var numSamples = audioClip.samples;
                var audioClipData = new float[numChannel * numSamples];
                audioClip.GetData(audioClipData, 0);
                _irAudioData[index] = new float[numChannel][];
                for (var iChannel = 0; iChannel < numChannel; iChannel++)
                {
                    _irAudioData[index][iChannel] = new float[filterLength];
                    var irAudioDataRef = _irAudioData[index][iChannel];
                    for (var n = 0; n < filterLength; n++)
                        if (n < numSamples)
                            irAudioDataRef[n] = audioClipData[n * numChannel + iChannel];
                        else
                            irAudioDataRef[n] = 0;
                }

                _loaded[index] = true;
            }
        }


        private IEnumerator WaitForComplete()
        {
            for (var i = 0; i < _loaded.Length; i++)
                if (!_loaded[i])
                    yield return new WaitForSeconds(0.1f);
            Debug.Log("AudioLoaded.");
            allLoaded = true;
        }
    }
}