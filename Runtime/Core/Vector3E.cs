﻿using UnityEngine;

namespace BA.BACore
{
    public static class Vector3E
    {

        public static Vector3 AddElementwise(Vector3 v1, Vector3 v2)
        {
            var vec = Vector3.zero;
            vec.x = v1.x + v2.x;
            vec.y = v1.y + v2.y;
            vec.z = v1.z + v2.z;
            return vec;
        }
        public static Vector3 AddElementwise(Vector3 v1, float v2)
        {
            var vec = Vector3.zero;
            vec.x = v1.x + v2;
            vec.y = v1.y + v2;
            vec.z = v1.z + v2;
            return vec;
        }
        public static Vector3 MultiplyElementwise(Vector3 v1, Vector3 v2)
        {
            var vec = Vector3.zero;
            vec.x = v1.x * v2.x;
            vec.y = v1.y * v2.y;
            vec.z = v1.z * v2.z;
            return vec;
        }
        public static Vector3 MultiplyElementwise(Vector3 v1, float v2)
        {
            var vec = Vector3.zero;
            vec.x = v1.x * v2;
            vec.y = v1.y * v2;
            vec.z = v1.z * v2;
            return vec;
        }
        
    }
}