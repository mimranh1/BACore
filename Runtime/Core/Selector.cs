﻿using System;

namespace BA.BACore
{
    [Serializable]
    public abstract class Selector<TClass,TType> : ExtendedScriptableObject where TClass : ExtendedScriptableObject<TClass, TType> where TType : Enum
    {
        public TType Option;
        public TClass @object;
        
        private void OnEnable()
        {
            OnValidate();
        }

        private void OnValidate()
        {
            if (@object == null || !Equals(@object.Type, Option))
                @object = ScriptableObjectConstructor.Create<TClass, TType>(Option);
        }

        public void SetSettings(Selector<TClass, TType> selector)
        {
            Option = selector.Option;
            OnValidate();
        }

    }
}