using System.Runtime.InteropServices;
using FFTWSharp;
using log4net;
using UnityEngine;

namespace BA.BACore
{
    /// <summary>
    /// FFt and Ifft for offline
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    public static class FftHelper
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(FftHelper));

        /// <summary>
        /// Fft 
        /// </summary>
        /// <param name="input">fft input array</param>
        /// <param name="real">is the input real</param>
        /// <param name="length">length of fft if different from input.Length</param>
        /// <param name="numZeros"></param>
        /// <returns></returns>
        public static float[] Fft(float[] input, bool real, int length = -1, int numZeros = 0)
        {
            if (input == null || input.Length == 0)
            {
                Debug.LogError("Not valid input.");
                return new float[0];
            }

            if (real)
                input = ComplexFloats.ToComplex(input, length, numZeros);
            var fftResult = new float[input.Length];
            var hIn = GCHandle.Alloc(input, GCHandleType.Pinned);
            var hOut = GCHandle.Alloc(fftResult, GCHandleType.Pinned);
            var fftPlan = fftwf.dft_1d(input.Length / 2, hIn.AddrOfPinnedObject(), hOut.AddrOfPinnedObject(),
                fftw_direction.Forward, fftw_flags.Estimate);
            fftwf.execute(fftPlan);
            hIn.Free();
            hOut.Free();
            return fftResult;
        }

        /// <summary>
        /// Calculate TimeDelay in Frequency Domain
        /// </summary>
        /// <param name="deltaSample">Time delay in samples</param>
        /// <param name="lengthFilter">filter Length</param>
        /// <returns>return calculate time delay in Frequency Domain</returns>
        public static float[] GetPhaseWithDelay(int deltaSample, int lengthFilter)
        {
            var fIn = new float[2 * lengthFilter];
            fIn[2 * deltaSample] = 1;
            var fOut = new float[2 * lengthFilter];
            var hIn = GCHandle.Alloc(fIn, GCHandleType.Pinned);
            var hOut = GCHandle.Alloc(fOut, GCHandleType.Pinned);
            var fftPlanHrir = fftwf.dft_1d(lengthFilter, hIn.AddrOfPinnedObject(), hOut.AddrOfPinnedObject(),
                fftw_direction.Forward, fftw_flags.Estimate);
            fftwf.execute(fftPlanHrir);
            hIn.Free();
            hOut.Free();
            return fOut;
        }

        /// <summary>
        /// Fast Fourier Transform from real or complex arrays
        /// </summary>
        /// <param name="input">input vector in time Domain</param>
        /// <param name="real">tells if input is Real or Complex vector</param>
        /// <returns></returns>
        public static float[][] Fft(float[][] input, bool real)
        {
            var length = input.Length;
            var result = new float[length][];
            for (var i = 0; i < length; i++)
                result[i] = Fft(input[i], real);
            return result;
        }

        /// <summary>
        /// Fast Fourier Transform from real or complex arrays
        /// </summary>
        /// <param name="input">input vector in time Domain</param>
        /// <param name="real">tells if input is Real or Complex vector</param>
        /// <returns></returns>
        public static float[][][] Fft(float[][][] input, bool real)
        {
            var length = input.Length;
            var result = new float[length][][];
            for (var i = 0; i < length; i++)
                result[i] = Fft(input[i], real);
            return result;
        }

        /// <summary>
        /// Inverse Fast Fourier Transform from real or complex arrays
        /// </summary>
        /// <param name="input">Input vector in Frequency domain</param>
        /// <returns></returns>
        public static float[] Ifft(float[] input)
        {
            var fOut = new float[input.Length];
            var hIn = GCHandle.Alloc(input, GCHandleType.Pinned);
            var hOut = GCHandle.Alloc(fOut, GCHandleType.Pinned);
            var ifftPlan = fftwf.dft_1d(input.Length / 2, hIn.AddrOfPinnedObject(), hOut.AddrOfPinnedObject(),
                fftw_direction.Backward, fftw_flags.Estimate);
            fftwf.execute(ifftPlan);
            var result = new fftwf_complexarray(fOut).GetData_Real();
            var num = result.Length;
            for (var j = 0; j < num; j++)
                result[j] /= num;

            hIn.Free();
            hOut.Free();
            return result;
        }

        /// <summary>
        /// Inverse Fast Fourier Transform from real or complex arrays
        /// </summary>
        /// <param name="input">Input vector in Frequency domain</param>
        /// <returns></returns>
        public static float[][] Ifft(float[][] input)
        {
            if (input != null)
            {
                var n = input.Length;
                var result = new float[n][];
                for (var i = 0; i < n; i++)
                    result[i] = Ifft(input[i]);

                return result;
            }

            return input;
        }

        /// <summary>
        /// Inverse Fast Fourier Transform from real or complex arrays
        /// </summary>
        /// <param name="input">Input vector in Frequency domain</param>
        /// <returns></returns>
        public static float[][][] Ifft(float[][][] input)
        {
            if (input != null)
            {
                var n = input.Length;
                var result = new float[n][][];
                for (var i = 0; i < n; i++)
                    result[i] = Ifft(input[i]);

                return result;
            }

            return input;
        }

        /// <summary>
        /// convolve to time domain signals in Time domain
        /// </summary>
        /// <param name="input">input signal 1</param>
        /// <param name="filter">input signal 2</param>
        /// <returns></returns>
        public static float[] Convolution(float[] input, float[] filter)
        {
            var result = new float[input.Length + filter.Length - 1];
            for (var i = 0; i < input.Length; i++)
            for (var j = 0; j < filter.Length; j++)
                result[i + j] += input[i] * filter[j];
            return result;
        }
    }
}