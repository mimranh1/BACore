using System;
using System.Linq;
using System.Linq.Expressions;
using FFTWSharp;
using log4net;
using UnityEngine;

namespace BA.BACore
{
    /// <summary>
    /// Class to Calculate with complex float arrays alternate real imag in array (for fft)
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    public static class ComplexFloats
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(ComplexFloats));

        /// <summary>
        /// check if the arrays are almost equal
        /// </summary>
        /// <param name="f1">input array left</param>
        /// <param name="f2">input array right</param>
        /// <param name="epsilon">compare limit</param>
        /// <returns></returns>
        public static bool AlmostEqual(float[] f1, float[] f2, float epsilon)
        {
            if (f1.Length != f2.Length)
                return false;

            return !f1.Where((t, i) => Mathf.Abs(t - f2[i]) > epsilon).Any();
        }

        /// <summary>
        /// Calculate Magnitude of Complex Array
        /// </summary>
        /// <param name="f1">input complex Array</param>
        /// <returns>return magnitude of input</returns>
        public static float[] Abs(float[] f1)
        {
            if (f1.Length % 2 != 0)
                Log.Error("Length of input array need to be even!");
            var outputAbs = new float[f1.Length / 2];
            for (var n = 0; n < outputAbs.Length; n += 2)
            {
                outputAbs[n / 2] = f1[n] * f1[n] + f1[n + 1] * f1[n + 1];
            }

            return outputAbs;
        }

    
        /// <summary>
        /// Multiply 2 complex arrays
        /// </summary>
        /// <param name="x">input complex left</param>
        /// <param name="y">input complex right</param>
        /// <param name="result"></param>
        /// <returns>return complex result</returns>
        public static void Multiply(float[] x, float[] y, float[] result)
        {
            if (x.Length != y.Length || x.Length != result.Length)
                Log.Error("Multiplication Arrays have to have equal length!");
            for (var n = 0; n < y.Length; n += 2)
            {
                result[n] = x[n] * y[n] - x[n + 1] * y[n + 1];
                result[n + 1] = x[n] * y[n + 1] + x[n + 1] * y[n];
            }
        }
        
        /// <summary>
        /// Multiply 2 complex arrays
        /// </summary>
        /// <param name="x">input complex left</param>
        /// <param name="y">input complex right</param>
        /// <param name="result"></param>
        /// <returns>return complex result</returns>
        public static void Add(float[] x, float[] y, float[] result)
        {
            if (x.Length != y.Length || x.Length != result.Length)
                Log.Error("Multiplication Arrays have to have equal length!");
            for (var i = 0; i < y.Length; i++)
            {
                result[ i] = x[i] + y[ i] ;
            }
        } 
        
        
        /// <summary>
        /// Multiply 2 complex arrays
        /// </summary>
        /// <param name="x">input complex left</param>
        /// <param name="y">input complex right</param>
        /// <param name="result"></param>
        /// <returns>return complex result</returns>
        public static void AddSum(float[] x, float[] y, float[] result)
        {
            if (x.Length != y.Length || x.Length != result.Length)
                Log.Error("Multiplication Arrays have to have equal length!");
            for (var i = 0; i < y.Length; i++)
            {
                result[ i] += x[i] + y[ i] ;
            }
        }
        
        /// <summary>
        /// Multiply 2 complex arrays
        /// </summary>
        /// <param name="x">input complex left</param>
        /// <param name="y">input complex right</param>
        /// <param name="result"></param>
        /// <returns>return complex result</returns>
        public static void Sum(float[] x, float[] result)
        {
            if (x.Length != result.Length)
                Log.Error("Multiplication Arrays have to have equal length!");
            for (var i = 0; i < x.Length; i++)
            {
                result[i] += x[i];
            }
        }/// <summary>
        /// Multiply 2 complex arrays
        /// </summary>
        /// <param name="x">input complex left</param>
        /// <param name="y">input complex right</param>
        /// <param name="result"></param>
        /// <returns>return complex result</returns>
        public static void Copy(float[] x, float[] result)
        {
            if (x.Length != result.Length)
                Log.Error("Multiplication Arrays have to have equal length!");
            Array.Copy(x, result, result.Length);
        }
     
        /// <summary>
        /// Multiply 2 complex arrays
        /// </summary>
        /// <param name="x">input complex left</param>
        /// <param name="y">input complex right</param>
        /// <param name="result"></param>
        /// <returns>return complex result</returns>
        public static void MultiplyAdd(float[] x, float[] y, float[] result)
        {
            if (x.Length != y.Length || x.Length != result.Length)
                Log.Error("Multiplication Arrays have to have equal length!");
            for (var n = 0; n < y.Length; n+=2)
            {
                result[n] += x[n] * y[n] - x[n + 1] * y[n + 1];
                result[n + 1] += x[n] * y[n + 1] + x[n + 1] * y[n];
            }
        }
     

        /// <summary>
        /// Multiply 2 complex arrays
        /// </summary>
        /// <param name="x">input complex left</param>
        /// <param name="y">input complex right</param>
        /// <param name="z">constant factor</param>
        /// <param name="result">return complex result</param>
        public static void Multiply(float[] x, float[] y, float z, float[] result)
        {
            if (x.Length != y.Length || x.Length != result.Length)
                Log.Error("Multiplication Arrays have to have equal length!");
            for (var n = 0; n < y.Length; n += 2)
            {
                result[n] = (x[n] * y[n] - x[n + 1] * y[n + 1]) * z;
                result[n + 1] = (x[n] * y[n + 1] + x[n + 1] * y[n]) * z;
            }
        }
        /// <summary>
        /// Multiply 2 complex arrays
        /// </summary>
        /// <param name="x">input complex left</param>
        /// <param name="y">constant factor</param>
        /// <param name="result">return complex result</param>
        public static void Multiply(float[] x, float y, float[] result)
        {
            if (x.Length != result.Length)
                Log.Error("Multiplication Arrays have to have equal length!");
            for (var n = 0; n < x.Length; n++)
            {
                result[n] = x[n] * y;
            }
        }
        
        /// <summary>
        /// Multiply 2 complex arrays
        /// </summary>
        /// <param name="x">input complex left</param>
        /// <param name="y">input complex right</param>
        /// <param name="z">constant factor</param>
        /// <param name="result">return complex result</param>
        public static void MultiplyAdd(float[] x, float[] y, float z, float[] result)
        {
            if (x.Length != y.Length || x.Length != result.Length)
                Log.Error("Multiplication Arrays have to have equal length!");
            for (var n = 0; n < y.Length; n += 2)
            {
                var xReal = x[n];
                var yReal = y[n];
                var xImag = x[n + 1];
                var yImag = y[n + 1];
                result[n] += (xReal * yReal - xImag * yImag) * z;
                result[n + 1] += (xReal * yImag + xImag * yReal) * z;
            }
        } 
        
        public static void FreqAddDelay(float[] x, int samplesDelay,int fs)
        {
            var xLength = x.Length;
            for (var n = 0; n < xLength; n += 2)
            {
                var f = (n / 2f)/xLength*fs;
                var xReal = x[n];
                var xImag = x[n + 1];
                var exp = -2f * Mathf.PI * (n / 2f) * samplesDelay / xLength;    //TODO WRONG 2*pi*f*deltaT
                var yReal = Mathf.Cos(exp);
                var yImag = Mathf.Sin(exp);
                x[n] = xReal * yReal - xImag * yImag ;
                x[n + 1] = xReal * yImag + xImag * yReal;
            }
        }

 
        public static void FreqDelay(float[] x, float factor, float deltaTime)
        {
            var xLength = x.Length /2;
            for (var n = 0; n < xLength*2; n += 2)
            {
                var xReal = factor;
                var xImag = 0f;
                var exp = -2f * Mathf.PI * (n / 2f) * deltaTime / xLength;    //TODO WRONG 2*pi*f*deltaT
                var yReal = Mathf.Cos(exp);
                var yImag = Mathf.Sin(exp);
                x[n] = xReal * yReal - xImag * yImag ;
                x[n + 1] = xReal * yImag + xImag * yReal;
            }
        }

        /// <summary>
        /// Multiply 2 complex arrays
        /// </summary>
        /// <param name="xMono">input complex left</param>
        /// <param name="yBinaural">input complex right</param>
        /// <param name="zMono">constant factor</param>
        /// <param name="result">return complex result</param>
        public static void MultiplyAdd1(FloatArrays[] xMono, FloatArrays[] yBinaural, float zMono, int channel, float[] result)
        {
            if (xMono.Length*2 != yBinaural.Length || yBinaural.Length != result.Length)
                Log.Error("Multiplication Arrays have to have equal length!");
            var filterLength = xMono[0].Length;
            for (var n = 0; n < filterLength; n += 2)
            {
                var xReal = 0f;
                var xImag = 0f;
                var yReal = 0f;
                var yImag = 0f;
                for (var i = 0; i < xMono.Length; i++)
                {
                    xReal += xMono[i].floats[n];
                    xImag += xMono[i].floats[n+1];
                    yReal += yBinaural[2*i+channel].floats[n];
                    yImag += yBinaural[2*i+channel].floats[n+1];
                }

                result[n] += (xReal * yReal - xImag * yImag) * zMono;
                result[n + 1] += (xReal * yImag + xImag * yReal) * zMono;
            }
        }

        /// <summary>
        /// Multiply 2 complex arrays
        /// </summary>
        /// <param name="xFactorComplex">input complex left</param>
        /// <param name="yFactorComplex">input complex right</param>
        /// <param name="zFactor">constant factor</param>
        /// <param name="summandComplex"></param>
        /// <param name="result">return complex result</param>
        public static void MultiplyAndSumAdd(float[] xFactorComplex, float[] yFactorComplex, float zFactor, float[] summandComplex, float[] result)
        {
            if (xFactorComplex.Length != yFactorComplex.Length || xFactorComplex.Length != result.Length|| xFactorComplex.Length != summandComplex.Length)
            {
                Log.Error("Multiplication Arrays have to have equal length!");
            }
            for (var n = 0; n < yFactorComplex.Length; n += 2)
            {
                result[n] +=
                    (xFactorComplex[n] * yFactorComplex[n] -
                     xFactorComplex[n + 1] * yFactorComplex[n + 1]) * zFactor + summandComplex[n];
                result[n + 1] +=
                    (xFactorComplex[n] * yFactorComplex[n + 1] +
                     xFactorComplex[n + 1] * yFactorComplex[n]) * zFactor + summandComplex[n + 1];
            }
        }

        /// <summary>
        /// Multiply 2 complex arrays
        /// </summary>
        /// <param name="xFactorComplex">input complex left</param>
        /// <param name="yFactor">constant factor</param>
        /// <param name="summandComplex"></param>
        /// <param name="result">return complex result</param>
        public static void MultiplyAndSumAdd(float[] xFactorComplex, float yFactor, float[] summandComplex, float[] result)
        {
            if (xFactorComplex.Length != result.Length|| xFactorComplex.Length != summandComplex.Length)
            {
                Log.Error("Multiplication Arrays have to have equal length!");
            }
            for (var i = 0; i < xFactorComplex.Length; i++)
            {
                result[i] +=
                    (xFactorComplex[i] * yFactor) + summandComplex[i];
            }
        }

        /// <summary>
        /// Multiply 2 complex arrays
        /// </summary>
        /// <param name="xFactorComplex">input complex left</param>
        /// <param name="yFactor">constant factor</param>
        /// <param name="summandComplex"></param>
        /// <param name="result">return complex result</param>
        public static void MultiplyAndSum(float[] xFactorComplex, float yFactor, float[] summandComplex, float[] result)
        {
            if (xFactorComplex.Length != result.Length || xFactorComplex.Length != summandComplex.Length)
            {
                Log.Error("Multiplication Arrays have to have equal length!");
            }

            for (var i = 0; i < xFactorComplex.Length; i++)
            {
                result[i] =
                    (xFactorComplex[i] * yFactor) + summandComplex[i];
            }
        }

        /// <summary>
        /// Multiply 2 complex arrays
        /// </summary>
        /// <param name="xFactorComplex">input complex left</param>
        /// <param name="yFactor">constant factor</param>
        /// <param name="summandComplex"></param>
        /// <param name="multiplyAllComplex"></param>
        /// <param name="result">return complex result</param>
        public static void MultiplyAndSumAndMultiply(float[] xFactorComplex, float yFactor, float[] summandComplex, float[] multiplyAllComplex, float[] result)
        {
            if (xFactorComplex.Length != result.Length|| xFactorComplex.Length != summandComplex.Length|| xFactorComplex.Length != multiplyAllComplex.Length)
            {
                Log.Error("Multiplication Arrays have to have equal length!");
            }

            for (var n = 0; n < xFactorComplex.Length; n+=2)
            {
                var real = (xFactorComplex[n] * yFactor) + summandComplex[n];
                var imag = (xFactorComplex[n + 1] * yFactor) + summandComplex[n + 1];

                result[n] = (real * multiplyAllComplex[n] - imag * multiplyAllComplex[n + 1]);
                result[n + 1] = (real * multiplyAllComplex[n + 1] + imag * multiplyAllComplex[n]);
            }
        }
        
        public static void SumAndMultiply(float[] xFactorComplex, float[] summandComplex, float[] multiplyAllComplex, float[] result)
        {
            if (xFactorComplex.Length != result.Length|| xFactorComplex.Length != summandComplex.Length|| xFactorComplex.Length != multiplyAllComplex.Length)
            {
                Log.Error("Multiplication Arrays have to have equal length!");
            }

            for (var n = 0; n < xFactorComplex.Length; n+=2)
            {
                var real = xFactorComplex[n]  + summandComplex[n];
                var imag = xFactorComplex[n + 1] + summandComplex[n + 1];

                result[n] = (real * multiplyAllComplex[n] - imag * multiplyAllComplex[n + 1]);
                result[n + 1] = (real * multiplyAllComplex[n + 1] + imag * multiplyAllComplex[n]);
            }
        }
        
        /// <summary>
        /// Multiply 2 complex arrays
        /// </summary>
        /// <param name="xFactorComplex">input complex left</param>
        /// <param name="yFactor">constant factor</param>
        /// <param name="summandComplex"></param>
        /// <param name="multiplyAllComplex"></param>
        /// <param name="result">return complex result</param>
        public static void MultiplyAndSumAndMultiplyAdd(float[] xFactorComplex, float yFactor, float[] summandComplex, float[] multiplyAllComplex, float[] result)
        {
            if (xFactorComplex.Length != result.Length|| xFactorComplex.Length != summandComplex.Length|| xFactorComplex.Length != multiplyAllComplex.Length)
            {
                Log.Error("Multiplication Arrays have to have equal length!");
            }

            for (var n = 0; n < xFactorComplex.Length; n+=2)
            {
                var real = (xFactorComplex[n] * yFactor) + summandComplex[n];
                var imag = (xFactorComplex[n + 1] * yFactor) + summandComplex[n + 1];

                result[n] += (real * multiplyAllComplex[n] - imag * multiplyAllComplex[n + 1]);
                result[n + 1] += (real * multiplyAllComplex[n + 1] + imag * multiplyAllComplex[n]);
            }
        }

        /// <summary>
        /// Multiply 2 complex arrays
        /// </summary>
        /// <param name="xFactorComplex">input complex left</param>
        /// <param name="yFactorComplex">input complex right</param>
        /// <param name="zFactor">constant factor</param>
        /// <param name="summandComplex"></param>
        /// <param name="summandFactor"></param>
        /// <param name="result">return complex result</param>
        public static void MultiplyAndAdd(float[] xFactorComplex, float[] yFactorComplex, float zFactor,
            float[] summandComplex, float summandFactor, float[] result)
        {
            if (xFactorComplex.Length != yFactorComplex.Length || xFactorComplex.Length != result.Length ||
                xFactorComplex.Length != summandComplex.Length)
            {
                Log.Error("Multiplication Arrays have to have equal length!");
            }

            for (var n = 0; n < yFactorComplex.Length; n += 2)
            {
                result[n] =
                    (xFactorComplex[n] * yFactorComplex[n] -
                     xFactorComplex[n + 1] * yFactorComplex[n + 1]) * zFactor + summandComplex[n] * summandFactor;
                result[n + 1] =
                    (xFactorComplex[n] * yFactorComplex[n + 1] +
                     xFactorComplex[n + 1] * yFactorComplex[n]) * zFactor + summandComplex[n + 1] * summandFactor;
            }
        }


        /// <summary>
        /// Interlaces an array with zeros to match the fftw convention of representing complex numbers.
        /// </summary>
        /// <param name="real">An array of real numbers.</param>
        /// <param name="length">Final Length of array.</param>
        /// <param name="numOfZeros">Numbers of Zeros at the end of Complex Array.</param>
        /// <returns>Returns an array of complex numbers.</returns>
        public static float[] ToComplex(float[] real, int length = -1, int numOfZeros = 0)
        {
            if (numOfZeros < 0)
                Log.Error("Number of Zeros must positive");
            var n = length == -1 ? real.Length + numOfZeros : length;
            var comp = new float[n * 2];
            var lenArray = real.Length;
            for (var i = 0; i < n; i++)
            {
                if (lenArray <= i)
                    break;
                comp[2 * i] = real[i];
            }

            return comp;
        }

        /// <summary>
        /// Convert a complex array into real
        /// </summary>
        /// <param name="complex">input complex array</param>
        /// <returns>real array</returns>
        public static float[] ToReal(float[] complex)
        {
            var n = complex.Length / 2;
            var real = new float[n];
            for (var i = 0; i < n; i++)
                real[i] = complex[2 * i];
            return real;
        }
        
        
        /// <summary>
        /// return an copy of the input array
        /// </summary>
        /// <param name="arr1">input array to copy</param>
        /// <returns>new copied array</returns>
        public static float[][] CopyTo(float[][] arr1)
        {
            var arr2 = new float[arr1.Length][];
            for (var i = 0; i < arr1.Length; i++)
            {
                arr2[i] = new float[arr1[i].Length];
                arr1[i].CopyTo(arr2[i], 0);
            }

            return arr2;
        }

        /// <summary>
        /// return an copy of the input array
        /// </summary>
        /// <param name="arr1">input array to copy</param>
        /// <returns>new copied array</returns>
        public static float[] CopyTo(float[] arr1)
        {
            var arr2 = new float[arr1.Length];
            arr1.CopyTo(arr2, 0);

            return arr2;
        }
    }
}