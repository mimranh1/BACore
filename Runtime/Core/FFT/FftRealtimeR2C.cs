using System;
using System.Numerics;
using System.Runtime.InteropServices;
using FFTWSharp;
using UnityEngine;

namespace BA.BACore
{
    /// <summary>
    /// Realtime fft Class
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    public class FftRealtimeR2C
    {
        /// <summary>
        /// fft Length
        /// </summary>
        public int Length => _irFftInput.Length/2;

        // private readonly fftwf_complexarray _irFftInput;
        // private readonly fftwf_complexarray _irFftFreq;       
        private readonly float[] _irFftInput;
        private readonly float[] _irFftFreq;
        private GCHandle _irFftInputHandle;
        private GCHandle _irFftResultHandle;
        private readonly IntPtr _irFftPlan;

        /// <summary>
        /// define input output Array inside the class
        /// slower than the other method
        /// </summary>
        /// <param name="fftLength">Length of fft</param>
        public FftRealtimeR2C(int fftLength)
        {
            // _irFftInput = new fftwf_complexarray(fftLength );
            // _irFftFreq = new fftwf_complexarray(fftLength );
            _irFftInput = new float[fftLength*2];
            _irFftFreq = new float[fftLength*2];

            _irFftInputHandle = GCHandle.Alloc(_irFftInput, GCHandleType.Pinned);
            _irFftResultHandle = GCHandle.Alloc(_irFftFreq, GCHandleType.Pinned);
            // _irFftPlan = fftwf.dft_r2c_1d(fftLength, _irFftInput.Handle,
            //     _irFftFreq.Handle, fftw_flags.Patient);
            _irFftPlan = fftwf.dft_1d(fftLength, _irFftInputHandle.AddrOfPinnedObject(),
                _irFftResultHandle.AddrOfPinnedObject(), fftw_direction.Forward, fftw_flags.Measure);
        }

    
        /// <summary>
        /// Run the fft for input output arrays outside the 
        /// </summary>
        /// <param name="input">input reference complex with smaller hrtfLen</param>
        /// <param name="output">output reference real with hrtfLen</param>
        public void RunFftReal(float[] input, float[] output)
        {
            if(output.Length!=_irFftFreq.Length || input.Length*2!=_irFftInput.Length)
                Debug.LogError("length mismatch.");
            Array.Clear(_irFftInput, 0, _irFftInput.Length);
            for (var i = 0; i < input.Length; i++)
            {
                _irFftInput[2 * i] = input[i];
            }
            // _irFftInput.SetDataReal(input);
            fftwf.execute(_irFftPlan);
            _irFftFreq.CopyTo(output,0);
        }

    
        /// <summary>
        /// Run the fft for input output arrays outside the 
        /// </summary>
        /// <param name="input">input reference complex with smaller 2*hrtfLen</param>
        /// <param name="output">output reference real with hrtfLen</param>
        public void RunFft(float[] input, float[] output)
        {
            if(output.Length!=_irFftFreq.Length || input.Length!=_irFftInput.Length)
                Debug.LogError("length mismatch.");
            input.CopyTo(_irFftInput,0);
            // _irFftInput.SetDataReal(input);
            fftwf.execute(_irFftPlan);
            _irFftFreq.CopyTo(output,0);
            // _irFftFreq.GetData_Float(output);
        }

     
        /// <summary>
        /// Clean Up memory
        /// </summary>
        public void Destroy()
        {
            fftwf.destroy_plan(_irFftPlan);
            if (_irFftResultHandle.IsAllocated)
                _irFftResultHandle.Free();
            if (_irFftInputHandle.IsAllocated)
                _irFftInputHandle.Free();
        }
    }
}