﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Random = System.Random;

namespace BA.BACore
{
    public class MatrixFloatComplex
     {
        #region Delegates
        /// <summary>
        /// General purpose delegate for processing a number and giving
        /// a result.
        /// </summary>
        /// <param name="a">The number to process.</param>
        /// <returns>The result of performing an operation on the number.</returns>
        public delegate float ProcessNumber(float a);

        /// <summary>
        /// General purpose delegate for processing two numbers and giving
        /// a result.
        /// </summary>
        /// <param name="a">The first number to process.</param>
        /// <param name="b">The second number to process.</param>
        /// <returns>The result of performing an operation on both inputs.</returns>
        public delegate float ProcessNumbers(float a, float b);

        /// <summary>
        /// General purpose delegate for processing a Matrix and giving
        /// a result.
        /// </summary>
        /// <param name="a">The Matrix to process.</param>
        /// <returns>The result of performing an operation on the Matrix.</returns>
        public delegate float ProcessMatrix(MatrixFloatComplex a);

        #endregion

        #region Private Fields
        /// <summary>
        /// Storage array for the Matrix data.
        /// </summary>
        private float[] data;

        /// <summary>
        /// Dimensions of the Matrix
        /// </summary>
        private int samples;
        private int channels;
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor to create a new Matrix while specifying the number of
        /// rows and columns.
        /// </summary>
        /// <param name="samples">The number of rows to initialise the Matrix with.</param>
        /// <param name="channels">The number of columns to initialise the Matrix with.</param>
        public MatrixFloatComplex(int samples, int channels)
        {
            this.samples = samples;
            this.channels = channels;
            data = new float[samples * channels * 2];
        }

        /// <summary>
        /// Constructor to create a new square Matrix.
        /// </summary>
        /// <param name="dimensions">The number of rows and columns to initialise the
        /// Matrix with. There will be an equal number of rows and columns.</param>
        public MatrixFloatComplex(int dimensions) : this(dimensions, dimensions)
        {
        }

        public MatrixFloatComplex(float[,] array) : this(array.GetLength(0), array.GetLength(1))
        {
            var index = 0;
            for (var sample = 0; sample < samples; sample++)
            {
                for (var channel = 0; channel < channels; channel++)
                {
                    data[index++] = array[sample, channel];
                }
            }
        }
        public MatrixFloatComplex(float[][] array) : this(array.GetLength(0), array.GetLength(1))
        {
            var index = 0;
            for (var sample = 0; sample < samples; sample++)
            {
                for (var channel = 0; channel < channels; channel++)
                {
                    data[index++] = array[channel][sample];
                }
            }
        }

        public MatrixFloatComplex(MatrixFloatComplex m) : this(m.Samples, m.Channels)
        {
            for (var i = 0; i < data.Length; i++)
                data[i] = m.data[i];
        }
        #endregion

        #region Indexers
        /// <summary>
        /// Indexer to easily access a specific location in this Matrix.
        /// </summary>
        /// <param name="row">The row of the Matrix location to access.</param>
        /// <param name="column">The column of the Matrix location to access.</param>
        /// <returns>The value stored at the given row/column location.</returns>
        /// <remarks>Matrices are zero-indexed.</remarks>
        public float this[int row, int column]
        {
            get => data[row * Channels + 2 * column];
            set => data[row * Channels + 2 * column] = value;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Indicates whether or not this Matrix row and column dimensions are equal.
        /// </summary>
        public bool IsSquare => samples == channels;

        /// <summary>
        /// Get the dimensions of this Matrix in a single-dimensional array of the form
        /// [rows,columns].
        /// </summary>
        public int[] Dimensions => new [] { samples, channels };

        /// <summary>
        /// Get the number of rows in this Matrix.
        /// </summary>
        public int Samples => samples;

        /// <summary>
        /// Get the number of columns in this Matrix.
        /// </summary>
        public int Channels => channels;
        
        #endregion

        #region Operations
        /// <summary>
        /// Add two matrices together.
        /// </summary>
        /// <param name="m1">The first Matrix to add.</param>
        /// <param name="m2">The second Matrix to add.</param>
        /// <returns>The result of adding the two matrices together.</returns>
        /// <exception cref="InvalidMatrixDimensionsException">Thrown when both matrices have
        /// different dimensions.</exception>
        public static MatrixFloatComplex operator +(MatrixFloatComplex m1, MatrixFloatComplex m2)
        {
            if (!m1.HasSameDimensions(m2))
                throw new InvalidMatrixDimensionsException(
                    "Cannot add two Matrix objects whose dimensions do not match.");
            
            var output = new MatrixFloatComplex(m1.samples, m1.channels);
            for (var i = 0; i < m1.data.Length; i++)
            {
                output.data[i] = m1.data[i] + m2.data[i];
            }
            return output;
        }

        /// <summary>
        /// Add a number to each element in a Matrix.
        /// </summary>
        /// <param name="scalar">The number to add to each element in a Matrix.</param>
        /// <param name="m">The Matrix to add numbers to.</param>
        /// <returns>The result of adding the number to each element in a Matrix.</returns>
        public static MatrixFloatComplex operator +(float scalar, MatrixFloatComplex m)
        {
            var output = new MatrixFloatComplex(m.samples, m.channels);
            for (var i = 0; i < m.data.Length; i++)
            {
                output.data[i] = scalar + m.data[i];
            }
            return output;
        }

        /// <summary>
        /// Add a number to each element in a Matrix.
        /// </summary>
        /// <param name="m">The Matrix to add numbers to.</param>
        /// <param name="scalar">The number to add to each element in a Matrix.</param>
        /// <returns>The result of adding the number to each element in a Matrix.</returns>
        public static MatrixFloatComplex operator +(MatrixFloatComplex m, float scalar)
        {
            return scalar + m;
        }

        /// <summary>
        /// Unary negative operator.
        /// </summary>
        /// <param name="m1">The Matrix to negate.</param>
        /// <returns>The result of negating every element in the given Matrix.</returns>
        public static MatrixFloatComplex operator -(MatrixFloatComplex m1)
        {
            var output = new MatrixFloatComplex(m1.samples, m1.channels);
            for (var i = 0; i < m1.data.Length; i++)
            {
                output.data[i] = -m1.data[i];
            }
            return output;
        }

        /// <summary>
        /// Subtract one Matrix from another.
        /// </summary>
        /// <param name="m1">The first Matrix to subtract from.</param>
        /// <param name="m2">The second Matrix to subtract from the first.</param>
        /// <returns>The result of subtracting the second Matrix from the first.</returns>
        /// <exception cref="InvalidMatrixDimensionsException">Thrown when both matrices have
        /// different dimensions.</exception>
        public static MatrixFloatComplex operator -(MatrixFloatComplex m1, MatrixFloatComplex m2)
        {
            if (m1.HasSameDimensions(m2))
            {
                var output = new MatrixFloatComplex(m1.samples, m1.channels);
                for (var i = 0; i < m1.data.Length; i++)
                {
                    output.data[i] = m1.data[i] - m2.data[i];
                }
                return output;
            }
            else
            {
                throw new InvalidMatrixDimensionsException("Cannot subtract two Matrix objects whose dimensions do not match.");
            }
        }

        /// <summary>
        /// Subtract each element in a Matrix from a number.
        /// </summary>
        /// <param name="scalar">The number to subtract each element in a Matrix from.</param>
        /// <param name="m">The Matrix to subtract from the number.</param>
        /// <returns>The result of subracting each element from a given number.</returns>
        public static MatrixFloatComplex operator -(float scalar, MatrixFloatComplex m)
        {
            var output = new MatrixFloatComplex(m.samples, m.channels);
            for (var i = 0; i < m.data.Length; i++)
            {
                output.data[i] = scalar - m.data[i];
            }
            return output;
        }

        /// <summary>
        /// Subtract a number from each element in a Matrix.
        /// </summary>
        /// <param name="m">The Matrix to subtract the number from.</param>
        /// <param name="scalar">The number to subtract from each element in a Matrix.</param>
        /// <returns>The result of subracting a number from each element in a given Matrix.</returns>
        public static MatrixFloatComplex operator -(MatrixFloatComplex m, float scalar)
        {
            var output = new MatrixFloatComplex(m.samples, m.channels);
            for (var i = 0; i < m.data.Length; i++)
            {
                output.data[i] = m.data[i] - scalar;
            }
            return output;
        }


        /// <summary>
        /// Multiply two matrices together.
        /// </summary>
        /// <param name="m1">An nxm dimension Matrix.</param>
        /// <param name="m2">An mxp dimension Matrix.</param>
        /// <returns>An nxp Matrix that is the product of m1 and m2.</returns>
        /// <exception cref="InvalidMatrixDimensionsException">Thrown when the number of columns in the
        /// first Matrix don't match the number of rows in the second Matrix.</exception>
        public static MatrixFloatComplex operator *(MatrixFloatComplex m1, MatrixFloatComplex m2)
        {
            if (m1.channels == m2.samples)
            {
                var output = new MatrixFloatComplex(m1.samples, m2.channels);
                Parallel.For(0, m1.samples, i => MultiplySample(i, m1, m2, ref output));
                return output;
            }
            else
            {
                throw new InvalidMatrixDimensionsException("Multiplication cannot be performed on matrices with these dimensions.");
            }
        }

        /// <summary>
        /// Scalar multiplication of a Matrix.
        /// </summary>
        /// <param name="scalar">The scalar value to multiply each element of the Matrix by.</param>
        /// <param name="m">The Matrix to apply multiplication to.</param>
        /// <returns>A Matrix representing the scalar multiplication of scalar * m.</returns>
        public static MatrixFloatComplex operator *(float scalar, MatrixFloatComplex m)
        {
            var output = new MatrixFloatComplex(m.samples, m.channels);
            Parallel.For(0, m.samples, i => MultiplySample(i, m, scalar, ref output));
            return output;
        }

        /// <summary>
        /// Scalar multiplication of a Matrix.
        /// </summary>
        /// <param name="m">The Matrix to apply multiplication to.</param>
        /// <param name="scalar">The scalar value to multiply each element of the Matrix by.</param>
        /// <returns>A Matrix representing the scalar multiplication of m * scalar.</returns>
        public static MatrixFloatComplex operator *(MatrixFloatComplex m, float scalar)
        {
            // Same as above, but ensuring commutativity - i.e. (s * m) == (m * s).
            return scalar * m;
        }

        /// <summary>
        /// Scalar division of a Matrix.
        /// </summary>
        /// <param name="scalar">The scalar value to divide each element of the Matrix by.</param>
        /// <param name="m">The Matrix to apply division to.</param>
        /// <returns>A Matrix representing the scalar division of scalar / m.</returns>
        public static MatrixFloatComplex operator /(float scalar, MatrixFloatComplex m)
        {
            var output = new MatrixFloatComplex(m.samples, m.channels);
            Parallel.For(0, m.samples, i => DivideScalarByRow(i, m, scalar, ref output));
            return output;
        }

        /// <summary>
        /// Scalar division of a Matrix.
        /// </summary>
        /// <param name="m">The Matrix to apply division to.</param>
        /// <param name="scalar">The scalar value to division each element of the Matrix by.</param>
        /// <returns>A Matrix representing the scalar division of m / scalar.</returns>
        public static MatrixFloatComplex operator /(MatrixFloatComplex m, float scalar)
        {
            var output = new MatrixFloatComplex(m.samples, m.channels);
            Parallel.For(0, m.samples, i => DivideRow(i, m, scalar, ref output));
            return output;
        }

        /// <summary>
        /// Override the == operator to compare Matrix values.
        /// </summary>
        /// <param name="m1">The first Matrix to compare.</param>
        /// <param name="m2">The second Matrix to compare.</param>
        /// <returns>True if the values of both matrices match.</returns>
        public static bool operator ==(MatrixFloatComplex m1, MatrixFloatComplex m2)
        {
            return !(m1 is null) && m1.Equals(m2);
        }

        /// <summary>
        /// Override the != operator to compare Matrix values.
        /// </summary>
        /// <param name="m1">The first Matrix to compare.</param>
        /// <param name="m2">The second Matrix to compare.</param>
        /// <returns>True if the values of both matrices differ.</returns>
        public static bool operator !=(MatrixFloatComplex m1, MatrixFloatComplex m2)
        {
            return !(m1 == m2);
        }

        /// <summary>
        /// Create a Matrix truth table containing 1 and 0 representing
        /// values that are equal to the given scalar.
        /// </summary>
        /// <param name="m">The Matrix to compare to the scalar value.</param>
        /// <param name="scalar">A scalar value used for comparison.</param>
        /// <returns>A Matrix with 1s and 0s representing true and false for the
        /// comparison.</returns>
        public static MatrixFloatComplex operator ==(MatrixFloatComplex m, float scalar)
        {
            var output = new MatrixFloatComplex(m.samples, m.channels);
            Parallel.For(0, m.samples, i => EqualToRow(i, m, scalar, ref output));
            return output;
        }


        /// <summary>
        /// Create a Matrix truth table containing 1 and 0 representing
        /// values that are not equal to the given scalar.
        /// </summary>
        /// <param name="m">The Matrix to compare to the scalar value.</param>
        /// <param name="scalar">A scalar value used for comparison.</param>
        /// <returns>A Matrix with 1s and 0s representing true and false for the
        /// comparison.</returns>
        public static MatrixFloatComplex operator !=(MatrixFloatComplex m, float scalar)
        {
            var output = new MatrixFloatComplex(m.samples, m.channels);
            Parallel.For(0, m.samples, i => NotEqualToRow(i, m, scalar, ref output));
            return output;
        }

        /// <summary>
        /// Create a Matrix truth table containing 1 and 0 representing
        /// values that are less than the given scalar.
        /// </summary>
        /// <param name="m">The Matrix to compare to the scalar value.</param>
        /// <param name="scalar">A scalar value used for comparison.</param>
        /// <returns>A Matrix with 1s and 0s representing true and false for the
        /// comparison.</returns>
        public static MatrixFloatComplex operator <(MatrixFloatComplex m, float scalar)
        {
            MatrixFloatComplex output = new MatrixFloatComplex(m.samples, m.channels);
            Parallel.For(0, m.samples, i => LessThanRow(i, m, scalar, ref output));
            return output;
        }

        /// <summary>
        /// Create a Matrix truth table containing 1 and 0 representing
        /// values that are greater than the given scalar.
        /// </summary>
        /// <param name="m">The Matrix to compare to the scalar value.</param>
        /// <param name="scalar">A scalar value used for comparison.</param>
        /// <returns>A Matrix with 1s and 0s representing true and false for the
        /// comparison.</returns>
        public static MatrixFloatComplex operator >(MatrixFloatComplex m, float scalar)
        {
            MatrixFloatComplex output = new MatrixFloatComplex(m.samples, m.channels);
            Parallel.For(0, m.samples, i => GreaterThanRow(i, m, scalar, ref output));
            return output;
        }

        /// <summary>
        /// Create a Matrix truth table containing 1 and 0 representing
        /// values that are less than or equal to the given scalar.
        /// </summary>
        /// <param name="m">The Matrix to compare to the scalar value.</param>
        /// <param name="scalar">A scalar value used for comparison.</param>
        /// <returns>A Matrix with 1s and 0s representing true and false for the
        /// comparison.</returns>
        public static MatrixFloatComplex operator <=(MatrixFloatComplex m, float scalar)
        {
            MatrixFloatComplex output = new MatrixFloatComplex(m.samples, m.channels);
            Parallel.For(0, m.samples, i => LessThanOrEqualToRow(i, m, scalar, ref output));
            return output;
        }

        /// <summary>
        /// Create a Matrix truth table containing 1 and 0 representing
        /// values that are greater than or equal to the given scalar.
        /// </summary>
        /// <param name="m">The Matrix to compare to the scalar value.</param>
        /// <param name="scalar">A scalar value used for comparison.</param>
        /// <returns>A Matrix with 1s and 0s representing true and false for the
        /// comparison.</returns>
        public static MatrixFloatComplex operator >=(MatrixFloatComplex m, float scalar)
        {
            MatrixFloatComplex output = new MatrixFloatComplex(m.samples, m.channels);
            Parallel.For(0, m.samples, i => GreaterThanOrEqualToRow(i, m, scalar, ref output));
            return output;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Indicates if this Matrix has the same dimensions as another supplied Matrix.
        /// </summary>
        /// <param name="other">Another Matrix to compare this instance to.</param>
        /// <returns>true if both matrices have the same dimensions. Otherwise, false.</returns>
        public bool HasSameDimensions(MatrixFloatComplex other)
        {
            return (this.samples == other.samples) && (this.channels == other.channels);
        }

        /// <summary>
        /// Override the Object.Equals method to compare Matrix values.
        /// </summary>
        /// <param name="obj">The object to compare to this Matrix.</param>
        /// <returns>True if obj is a Matrix, and its values match the current
        /// Matrix values.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            MatrixFloatComplex m = obj as MatrixFloatComplex;
            if (object.ReferenceEquals(null, m)) return false;
            if (ReferenceEquals(this, m)) return true;

            if (!this.HasSameDimensions(m)) return false;

            for (int row = 0; row < samples; row++)
            {
                for (int column = 0; column < channels; column++)
                {
                    if (this[row, column] != m[row, column]) return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Compare this Matrix with a second Matrix by value.
        /// </summary>
        /// <param name="m">The Matrix to compare to this one.</param>
        /// <returns>True if both matrices contain the same values.</returns>
        public bool Equals(MatrixFloatComplex m)
        {
            if (object.ReferenceEquals(null, m)) return false;
            if (ReferenceEquals(this, m)) return true;

            if (!this.HasSameDimensions(m)) return false;

            for (int row = 0; row < samples; row++)
            {
                for (int column = 0; column < channels; column++)
                {
                    if (this[row, column] != m[row, column]) return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Override the default hash code.
        /// </summary>
        /// <returns>A bitwise XOR based on rows and columns of this Matrix.</returns>
        public override int GetHashCode()
        {
            return samples ^ channels;
        }

        /// <summary>
        /// Convert this Matrix to a string.
        /// </summary>
        /// <returns>A string representation of this Matrix.</returns>
        /// <remarks>All elements are rounded to two decimal places.</remarks>
        public override string ToString()
        {
            var sb = new StringBuilder();
            var index = 0;
            for (var i = 0; i < Samples; i++)
            {
                for (var j = 0; j < Channels; j++)
                {
                    sb.AppendFormat("{0:0.00} ", data[index++]);
                }
                sb.Append("\n");
            }
            return sb.ToString();
        }

        #region Private row/column operations
        
        /// <summary>
        /// Calculate a single row result of multiplying two matrices.
        /// </summary>
        /// <param name="sampleId">The zero-indexed row to calculate.</param>
        /// <param name="m1">The first Matrix to multiply.</param>
        /// <param name="m2">The second Matrix to multiply.</param>
        /// <param name="output">The Matrix to store the results in.</param>
        private static void MultiplySample(int sampleId, MatrixFloatComplex m1, MatrixFloatComplex m2, ref MatrixFloatComplex output)
        {
            var m1Index = sampleId * m1.channels;

            for (var column = 0; column < output.Channels; column++)
            {
                float result = 0;
                var m2Index = column;

                for (var i = 0; i < m1.Channels; i++)
                {
                    result += m1.data[m1Index + i] * m2.data[m2Index];
                    m2Index += m2.channels;
                }

                output[sampleId, column] = result;

            }
        }


        /// <summary>
        /// Calculate the results of multiplying each element in a Matrix
        /// row by a scalar value.
        /// </summary>
        /// <param name="sampleId">The zero-indexed row to calculate.</param>
        /// <param name="m">The Matrix to multiply by a scalar value.</param>
        /// <param name="scalar">The scalar value to multiply the Matrix by.</param>
        /// <param name="output">The Matrix that contains the results of multiplying the input
        /// Matrix by a scalar value.</param>
        private static void MultiplySample(int sampleId, MatrixFloatComplex m, float scalar, ref MatrixFloatComplex output)
        {
            var mIndex = sampleId * m.channels;

            for (var i = mIndex; i < mIndex + output.Channels; i++)
            {
                output.data[i] = scalar * m.data[i];
            }
        }


        /// <summary>
        /// Calculate a single row result of multiplying two matrices.
        /// </summary>
        /// <param name="row">The zero-indexed row from m1 to calculate.</param>
        /// <param name="m1">The first Matrix to multiply.</param>
        /// <param name="m2">The second Matrix to multiply.</param>
        /// <param name="output">The Matrix to store the results in.</param>
        private static void MultiplyByTransposedRow(int row, MatrixFloatComplex m1, MatrixFloatComplex m2, ref MatrixFloatComplex output)
        {
            var m1Index = row * m1.channels;
            var outputIndex = row * output.Channels;
            var m2Index = 0;

            for (var column = 0; column < output.Channels; column++)
            {
                float result = 0;

                for (var i = 0; i < m1.Channels; i++)
                {
                    result += m1.data[m1Index + i] * m2.data[m2Index++];
                }

                output.data[outputIndex++] = result;
            }
        }

        /// <summary>
        /// Calculate a single row result of multiplying row transposed into a column, by a corresponding
        /// row in a second Matrix.
        /// </summary>
        /// <param name="column">The zero-indexed row from m1 to transpose into a column.</param>
        /// <param name="m1">The first Matrix to multiply.</param>
        /// <param name="m2">The second Matrix to multiply.</param>
        /// <param name="output">The Matrix to store the results in.</param>
        private static void MultiplyByTransposedColumn(int column, MatrixFloatComplex m1, MatrixFloatComplex m2, ref MatrixFloatComplex output)
        {
            var output_index = column * output.Channels;
            int m1_index, m2_index;

            for (int m2Col = 0; m2Col < m2.Channels; m2Col++)
            {
                float result = 0;
                m1_index = column;
                m2_index = m2Col;

                for (int m2Row = 0; m2Row < m2.Samples; m2Row++)
                {
                    result += m1.data[m1_index] * m2.data[m2_index];
                    m1_index += m1.Channels;
                    m2_index += m2.Channels;
                }

                output.data[output_index++] = result;
            }
        }

        /// <summary>
        /// Calculate a single row result of multiplying one Matrix with its transpose.
        /// </summary>
        /// <param name="row">The zero-indexed row from m1 to calculate.</param>
        /// <param name="m1">The Matrix to multiply with its transpose.</param>
        /// <param name="output">The Matrix to store the results in.</param>
        private static void MultiplyByTransposedRow(int row, MatrixFloatComplex m1, ref MatrixFloatComplex output)
        {
            var m1Index = row * m1.channels;
            var outputIndex = row * output.Channels;
            var m2Index = 0;

            for (var column = 0; column < output.Channels; column++)
            {
                float result = 0;

                for (var i = 0; i < m1.Channels; i++)
                {
                    result += m1.data[m1Index + i] * m1.data[m2Index++];
                }

                output.data[outputIndex++] = result;
            }
        }

        /// <summary>
        /// Calculate a single row result of multiplying row transposed into a column, by a corresponding
        /// row in the original Matrix.
        /// </summary>
        /// <param name="column">The zero-indexed row from m1 to transpose into a column.</param>
        /// <param name="m1">The Matrix to transpose and multiply by the original.</param>
        /// <param name="output">The Matrix to store the results in.</param>
        private static void MultiplyByTransposedColumn(int column, MatrixFloatComplex m1, ref MatrixFloatComplex output)
        {
            MultiplyByTransposedColumn(column, m1, m1, ref output);
        }

        /// <summary>
        /// Calculate the results of dividing each element in a Matrix
        /// row by a scalar value.
        /// </summary>
        /// <param name="row">The zero-indexed row to calculate.</param>
        /// <param name="m">The Matrix to divide by a scalar value.</param>
        /// <param name="scalar">The scalar value to divide the Matrix by.</param>
        /// <param name="output">The Matrix that contains the results of dividing the input
        /// Matrix by a scalar value.</param>
        private static void DivideRow(int row, MatrixFloatComplex m, float scalar, ref MatrixFloatComplex output)
        {
            int m_index = row * m.channels;

            for (int i = m_index; i < m_index + output.Channels; i++)
            {
                output.data[i] = m.data[i] / scalar;
            }
        }

        /// <summary>
        /// Calculate the results of dividing a scalar value by each element
        /// in a Matrix.
        /// </summary>
        /// <param name="row">The zero-indexed row to calculate.</param>
        /// <param name="m">The Matrix to divide into a scalar value.</param>
        /// <param name="scalar">The scalar value to divide by the Matrix elements.</param>
        /// <param name="output">The Matrix that contains the results of dividing the scalar
        /// value by each element in the Matrix.</param>
        private static void DivideScalarByRow(int row, MatrixFloatComplex m, float scalar, ref MatrixFloatComplex output)
        {
            int m_index = row * m.channels;

            for (int i = m_index; i < m_index + output.Channels; i++)
            {
                output.data[i] = scalar / m.data[i];
            }
        }

        /// <summary>
        /// Calculate if each element in a row is greater than or equal to the given value, creating
        /// a 1.0 if true, or 0.0 otherwise.
        /// </summary>
        /// <param name="row">The zero-indexed row to calculate.</param>
        /// <param name="m">The Matrix to compare to the scalar value.</param>
        /// <param name="scalar">The scalar value to compare to the Matrix elements.</param>
        /// <param name="output">The Matrix that contains the comparison results.</param>
        private static void GreaterThanOrEqualToRow(int row, MatrixFloatComplex m, float scalar, ref MatrixFloatComplex output)
        {
            int m_index = row * m.channels;

            for (int i = m_index; i < m_index + output.Channels; i++)
            {
                output.data[i] = m.data[i] >= scalar ? 1.0f : 0.0f;
            }
        }

        /// <summary>
        /// Calculate if each element in a row is greater than the given value, creating
        /// a 1.0 if true, or 0.0 otherwise.
        /// </summary>
        /// <param name="row">The zero-indexed row to calculate.</param>
        /// <param name="m">The Matrix to compare to the scalar value.</param>
        /// <param name="scalar">The scalar value to compare to the Matrix elements.</param>
        /// <param name="output">The Matrix that contains the comparison results.</param>
        private static void GreaterThanRow(int row, MatrixFloatComplex m, float scalar, ref MatrixFloatComplex output)
        {
            int m_index = row * m.channels;

            for (int i = m_index; i < m_index + output.Channels; i++)
            {
                output.data[i] = m.data[i] > scalar ? 1.0f : 0.0f;
            }
        }

        /// <summary>
        /// Calculate if each element in a row is less than or equal to the given value, creating
        /// a 1.0 if true, or 0.0 otherwise.
        /// </summary>
        /// <param name="row">The zero-indexed row to calculate.</param>
        /// <param name="m">The Matrix to compare to the scalar value.</param>
        /// <param name="scalar">The scalar value to compare to the Matrix elements.</param>
        /// <param name="output">The Matrix that contains the comparison results.</param>
        private static void LessThanOrEqualToRow(int row, MatrixFloatComplex m, float scalar, ref MatrixFloatComplex output)
        {
            int m_index = row * m.channels;

            for (int i = m_index; i < m_index + output.Channels; i++)
            {
                output.data[i] = m.data[i] <= scalar ? 1.0f : 0.0f;
            }
        }

        /// <summary>
        /// Calculate if each element in a row is less than the given value, creating
        /// a 1.0 if true, or 0.0 otherwise.
        /// </summary>
        /// <param name="row">The zero-indexed row to calculate.</param>
        /// <param name="m">The Matrix to compare to the scalar value.</param>
        /// <param name="scalar">The scalar value to compare to the Matrix elements.</param>
        /// <param name="output">The Matrix that contains the comparison results.</param>
        private static void LessThanRow(int row, MatrixFloatComplex m, float scalar, ref MatrixFloatComplex output)
        {
            int m_index = row * m.channels;

            for (int i = m_index; i < m_index + output.Channels; i++)
            {
                output.data[i] = m.data[i] < scalar ? 1.0f : 0.0f;
            }
        }

        /// <summary>
        /// Calculate if each element in a row is equal to the given value, creating
        /// a 1.0 if true, or 0.0 otherwise.
        /// </summary>
        /// <param name="row">The zero-indexed row to calculate.</param>
        /// <param name="m">The Matrix to compare to the scalar value.</param>
        /// <param name="scalar">The scalar value to compare to the Matrix elements.</param>
        /// <param name="output">The Matrix that contains the comparison results.</param>
        private static void EqualToRow(int row, MatrixFloatComplex m, float scalar, ref MatrixFloatComplex output)
        {
            int m_index = row * m.channels;

            for (int i = m_index; i < m_index + output.Channels; i++)
            {
                output.data[i] = m.data[i] == scalar ? 1.0f : 0.0f;
            }
        }

        /// <summary>
        /// Calculate if each element in a row is not equal to the given value, creating
        /// a 1.0 if true, or 0.0 otherwise.
        /// </summary>
        /// <param name="row">The zero-indexed row to calculate.</param>
        /// <param name="m">The Matrix to compare to the scalar value.</param>
        /// <param name="scalar">The scalar value to compare to the Matrix elements.</param>
        /// <param name="output">The Matrix that contains the comparison results.</param>
        private static void NotEqualToRow(int row, MatrixFloatComplex m, float scalar, ref MatrixFloatComplex output)
        {
            int m_index = row * m.channels;

            for (int i = m_index; i < m_index + output.Channels; i++)
            {
                output.data[i] = m.data[i] != scalar ? 1.0f : 0.0f;
            }
        }

        #endregion

        #region Multiplying Transpose
        /// <summary>
        /// Multiply one Matrix by the transpose of the other.
        /// </summary>
        /// <param name="m1">The first Matrix to multiply.</param>
        /// <param name="m2">The Matrix to transpose and multiply.</param>
        /// <returns>The result of multiplying m1 with m2.Transpose.</returns>
        public static MatrixFloatComplex MultiplyByTranspose(MatrixFloatComplex m1, MatrixFloatComplex m2)
        {
            if (m1.Channels == m2.Channels)
            {
                MatrixFloatComplex output = new MatrixFloatComplex(m1.Samples, m2.Samples);
                Parallel.For(0, m1.Samples, i => MultiplyByTransposedRow(i, m1, m2, ref output));
                return output;
            }
            else
            {
                throw new InvalidMatrixDimensionsException("Multiplication cannot be performed on matrices with these dimensions.");
            }
        }

        /// <summary>
        /// Multiply a Matrix by its transpose.
        /// </summary>
        /// <param name="m1">The Matrix to multiply by its transpose.</param>
        /// <returns>The result of multiplying m1 with its transpose.</returns>
        public static MatrixFloatComplex MultiplyByTranspose(MatrixFloatComplex m1)
        {
            MatrixFloatComplex output = new MatrixFloatComplex(m1.Samples, m1.Samples);
            Parallel.For(0, m1.Samples, i => MultiplyByTransposedRow(i, m1, ref output));
            return output;
        }

        /// <summary>
        /// Multiply the Transpose of one Matrix by another Matrix.
        /// </summary>
        /// <param name="m1">The Matrix to transpose and multiply.</param>
        /// <param name="m2">The Matrix to multiply the Transpose of m1 by.</param>
        /// <returns>The result of multiplying m1.Transpose with m2.</returns>
        public static MatrixFloatComplex MultiplyTransposeBy(MatrixFloatComplex m1, MatrixFloatComplex m2)
        {
            if (m1.Samples == m2.Samples)
            {
                MatrixFloatComplex output = new MatrixFloatComplex(m1.Channels, m2.Channels);
                Parallel.For(0, m1.Channels, i => MultiplyByTransposedColumn(i, m1, m2, ref output));
                return output;
            }
            else
            {
                throw new InvalidMatrixDimensionsException("Multiplication cannot be performed on matrices with these dimensions.");
            }
        }

        /// <summary>
        /// Multiply the Transpose of a Matrix by the original Matrix.
        /// </summary>
        /// <param name="m1">The Matrix to transpose, and multiply by itself.</param>
        /// <returns>The result of multiplying m1.Transpose with m1.</returns>
        public static MatrixFloatComplex MultiplyTransposeBy(MatrixFloatComplex m1)
        {
            MatrixFloatComplex output = new MatrixFloatComplex(m1.Channels, m1.Channels);
            Parallel.For(0, m1.Channels, i => MultiplyByTransposedColumn(i, m1, ref output));
            return output;
        }
        #endregion

        #region Row / Column methods

        /// <summary>
        /// Swap two rows in this Matrix.
        /// </summary>
        /// <param name="row1">The first row to swap.</param>
        /// <param name="row2">The second row to swap.</param>
        public void SwapRows(int row1, int row2)
        {
            float[] tmp = new float[Channels];
            int indexRow1 = row1 * Channels;
            int indexRow2 = row2 * Channels;

            if (indexRow1 > data.Length || indexRow2 > data.Length)
                throw new IndexOutOfRangeException("SwapRow method called with non-existent rows.");

            for (int i = 0; i < Channels; i++)
            {
                tmp[i] = data[indexRow1 + i];
                data[indexRow1 + i] = data[indexRow2 + i];
                data[indexRow2 + i] = tmp[i];
            }
        }

        /// <summary>
        /// Join two Matrix objects together, side by side, or one above another.
        /// </summary>
        /// <param name="m1">The first Matrix to join.</param>
        /// <param name="m2">The second Matrix to join.</param>
        /// <param name="dimension">The dimensions to join them on.</param>
        /// <returns>A new Matrix containing both the original Matrices joined together.</returns>
        public static MatrixFloatComplex Join(MatrixFloatComplex m1, MatrixFloatComplex m2, MatrixDimensions dimension = MatrixDimensions.Auto)
        {
            MatrixFloatComplex result = null;
            switch (dimension)
            {
                case MatrixDimensions.Columns:
                    if (m1.Samples != m2.Samples)
                        throw new InvalidMatrixDimensionsException("Matrices cannot be joined as they don't have the same number of rows.");
                    result = new MatrixFloatComplex(m1.Samples, m1.Channels + m2.Channels);
                    int index = 0, indexM1 = 0, indexM2 = 0;
                    for (int row = 0; row < m1.Samples; row++)
                    {
                        for (int column = 0; column < m1.Channels; column++)
                        {
                            result.data[index++] = m1.data[indexM1++];
                        }
                        for (int column = 0; column < m2.Channels; column++)
                        {
                            result.data[index++] = m2.data[indexM2++];
                        }
                    }
                    break;
                case MatrixDimensions.Rows:
                    if (m1.Channels != m2.Channels)
                        throw new InvalidMatrixDimensionsException("Matrices cannot be joined as they don't have the same number of columns.");
                    result = new MatrixFloatComplex(m1.Samples + m2.Samples, m1.Channels);
                    for (int i = 0; i < m1.data.Length; i++)
                    {
                        result.data[i] = m1.data[i];
                    }
                    for (int i = 0; i < m2.data.Length; i++)
                    {
                        result.data[i + m1.data.Length] = m2.data[i];
                    }
                    break;
                case MatrixDimensions.Auto:
                    if (m1.Samples == m2.Samples)
                        goto case MatrixDimensions.Columns;
                    else
                        goto case MatrixDimensions.Rows;
                default:
                    break;
            }
            return result;
        }

        /// <summary>
        /// Add a column of a particular value at the start of a given Matrix.
        /// </summary>
        /// <param name="m1">The Matrix to add the identity column to.</param>
        /// <param name="identityValue">The identity value, default 1.</param>
        /// <returns>A new Matrix with an addition column at the start.</returns>
        public static MatrixFloatComplex AddIdentityColumn(MatrixFloatComplex m1, float identityValue = 1)
        {
            MatrixFloatComplex m2 = new MatrixFloatComplex(m1.Samples, m1.Channels + 1);

            int dataIndex1 = 0;
            int dataIndex2 = 0;

            for (int row = 0; row < m1.Samples; row++)
            {
                m2.data[dataIndex2++] = identityValue;
                for (int column = 0; column < m1.Channels; column++)
                {
                    m2.data[dataIndex2++] = m1.data[dataIndex1++];
                }
            }

            return m2;
        }

        /// <summary>
        /// Extract a row from this Matrix.
        /// </summary>
        /// <param name="row">The zero-index row to extract.</param>
        /// <returns>A row-vector form Matrix.</returns>
        public MatrixFloatComplex GetRow(int row)
        {
            if (row >= this.Samples)
                throw new IndexOutOfRangeException("The requested row is out of range");
            MatrixFloatComplex result = new MatrixFloatComplex(1, this.Channels);

            int index = row * this.Channels;
            for (int i = 0; i < this.Channels; i++)
            {
                result.data[i] = this.data[index + i];
            }

            return result;
        }

        /// <summary>
        /// Replace a given row with the contents of the first row in another
        /// Matrix.
        /// </summary>
        /// <param name="row">The row to change.</param>
        /// <param name="m">The Matrix to get new values from.</param>
        public void SetRow(int row, MatrixFloatComplex m)
        {
            if (row >= this.Samples)
                throw new IndexOutOfRangeException("The requested row is out of range");
            int index = row * this.Channels;
            for (int i = 0; i < Math.Min(this.Channels, m.Channels); i++)
            {
                this.data[index + i] = m.data[i];
            }
        }

        /// <summary>
        /// Extract a column from this Matrix.
        /// </summary>
        /// <param name="column">The zero-index column to extract.</param>
        /// <returns>A column-vector form Matrix.</returns>
        public MatrixFloatComplex GetColumn(int column)
        {
            if (column >= this.Channels)
                throw new IndexOutOfRangeException("The requested column is out of range");
            MatrixFloatComplex result = new MatrixFloatComplex(this.Samples, 1);

            int index = column;
            for (int i = 0; i < this.Samples; i++)
            {
                result.data[i] = this.data[index];
                index += this.Channels;
            }

            return result;
        }

        /// <summary>
        /// Remove a given column from a Matrix.
        /// </summary>
        /// <param name="column">The zero-based index of the column to remove.</param>
        /// <returns>A new Matrix containing the contents of the original Matrix
        /// without the specified column.</returns>
        public MatrixFloatComplex RemoveColumn(int column = 0)
        {
            if (column >= this.Channels)
                throw new IndexOutOfRangeException("The requested column is out of range");
            if (this.Channels == 1)
                throw new InvalidMatrixDimensionsException("Cannot remove column as there is only one column to begin with");

            MatrixFloatComplex result = new MatrixFloatComplex(this.Samples, this.Channels-1);
            int dataIndex = 0, resultIndex = 0;
            for (int i = 0; i < this.Samples; i++)
            {
                for (int j = 0; j < this.Channels; j++)
                {
                    if (j != column)
                    {
                        result.data[resultIndex++] = this.data[dataIndex++];
                    }
                    else
                    {
                        dataIndex++;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Expands two columns in a Matrix to a series of polynomial features.
        /// </summary>
        /// <param name="column1">The zero-index of the first column to expand.</param>
        /// <param name="column2">The zero-index of the second column to expand.</param>
        /// <param name="degree">The number of polynomial degrees to expand the columns to.</param>
        /// <returns>A copy of the original Matrix with column1 replaced with X1, X2, X1^2,
        /// X2^2, X1*X2, X1*X2^2, etc</returns>
        public MatrixFloatComplex ExpandPolynomials(int column1, int column2, int degree)
        {
            if (column1 >= this.Channels || column2 >= this.Channels)
                throw new IndexOutOfRangeException("A requested column is out of range");

            int outputColumns = (((degree + 1) * (degree + 2)) / 2);
            outputColumns += (this.Channels - 2);
            MatrixFloatComplex result = new MatrixFloatComplex(this.Samples, outputColumns);

            for (int row = 0; row < this.Samples; row++)
            {
                int colIndex = 0;
                float value1 = this[row, column1];
                float value2 = this[row, column2];

                for (int col = 0; col < this.Channels; col++)
                {
                    if (col == column1)
                    {
                        for (int i = 0; i <= degree; i++)
                        {
                            for (int j = 0; j <= i; j++)
                            {
                                float component1 = Mathf.Pow(value1, (i - j));
                                float component2 = Mathf.Pow(value2, j);
                                result[row, colIndex++] = component1 * component2;
                            }
                        }
                    }
                    else if (col != column2)
                    {
                        result[row, colIndex++] = this[row, col];
                    }
                }
            }

            return result;
        }
        #endregion

        /// <summary>
        /// Fills the Matrix with a given number.
        /// </summary>
        /// <param name="number">The number to assign to every element in the Matrix.</param>
        public void Fill(float number)
        {
            for (int i = 0; i < data.Length; i++)
                data[i] = number;
        }

        #region Matrix creation methods
        /// <summary>
        /// Create an identity Matrix
        /// </summary>
        /// <param name="dimensions">The number of rows and columns for this Matrix.</param>
        /// <returns>A square Matrix with zeros everywhere, except for the main diagonal which is filled with ones.</returns>
        public static MatrixFloatComplex Identity(int dimensions)
        {
            MatrixFloatComplex Midentity = new MatrixFloatComplex(dimensions, dimensions);
            int index = 0;
            while (index < Midentity.data.Length)
            {
                Midentity.data[index] = 1;
                index += dimensions + 1;
            }

            return Midentity;
        }

        /// <summary>
        /// Create a rows*columns size Matrix filled with 1's.
        /// </summary>
        /// <param name="rows">The number of rows to initialise the Matrix with.</param>
        /// <param name="columns">The number of columns to initialise the Matrix with.</param>
        /// <returns>A Matrix object filled with 1's.</returns>
        public static MatrixFloatComplex Ones(int rows, int columns)
        {
            MatrixFloatComplex result = new MatrixFloatComplex(rows, columns);
            result.Fill(1.0f);
            return result;
        }

        /// <summary>
        /// Create a square Matrix filled with 1's.
        /// </summary>
        /// <param name="dimensions">The number of rows and columns to initialise the
        /// Matrix with. There will be an equal number of rows and columns.</param>
        /// <returns>A square Matrix object filled with 1's.</returns>
        public static MatrixFloatComplex Ones(int dimension)
        {
            MatrixFloatComplex result = new MatrixFloatComplex(dimension);
            result.Fill(1.0f);
            return result;
        }

        /// <summary>
        /// Create a Magic Square for odd-numbered dimensions greater than 1.
        /// </summary>
        /// <param name="dimension">The dimension to use to create the Magic Square.</param>
        /// <returns>A Magic Square of the required dimensions.</returns>
        private static MatrixFloatComplex MagicSquareOdd(int dimension)
        {
            if (dimension <= 1)
                throw new InvalidMatrixDimensionsException("Dimensions must be greater than or equal to one.");

            if (dimension % 2 != 1)
                throw new InvalidMatrixDimensionsException("Dimensions must be an odd number.");

            MatrixFloatComplex output = new MatrixFloatComplex(dimension, dimension);

            // Set the first value and initialize current position to
            // halfway across the first row.
            int startColumn = dimension >> 1;
            int startRow = 0;
            output[startRow, startColumn] = 1;

            // Keep moving up and to the right until all squares are filled
            int newRow, newColumn;

            for (int i = 2; i <= dimension * dimension; i++)
            {
                newRow = startRow - 1; newColumn = startColumn + 1;
                if (newRow < 0) newRow = dimension - 1;
                if (newColumn >= dimension) newColumn = 0;

                if (output[newRow, newColumn] > 0)
                {
                    while (output[startRow, startColumn] > 0)
                    {
                        startRow++;
                        if (startRow >= dimension) startRow = 0;
                    }
                }
                else
                {
                    startRow = newRow; startColumn = newColumn;
                }
                output[startRow, startColumn] = i;
            }
            return output;
        }

        /// <summary>
        /// Create a Magic Square, where the numbers of each row, column and diagonal
        /// add up to the same number.
        /// </summary>
        /// <param name="dimensions">The number of rows and columns to initialise the
        /// Matrix with. There will be an equal number of rows and columns.</param>
        /// <returns>A Magic Square Matrix.</returns>
        public static MatrixFloatComplex Magic(int dimension)
        {
            // Handle special cases first
            if (dimension == 1)
                return MatrixFloatComplex.Identity(1);
            if (dimension == 2)
                throw new InvalidMatrixDimensionsException("A Magic Square cannot have a dimension of 2");

            // Handle odd-numbered dimensions first
            if (dimension % 2 == 1)
            {
                return (MagicSquareOdd(dimension));
            }

            // Handle 'doubly-even' dimensions (divisible by 4)
            if (dimension % 4 == 0)
            {
                MatrixFloatComplex floatEven = new MatrixFloatComplex(dimension, dimension);

                float index = 1;
                for (int i = 0, r = dimension - 1; i < dimension; i++, r--)
                {
                    for (int j = 0, c = dimension - 1; j < dimension; j++, c--)
                    {
                        // Fill in the diagonals
                        if (i == j || (j + i + 1) == dimension)
                        {
                            floatEven[i, j] = index;
                        }
                        else
                        {
                            // Otherwise, fill in diagonally opposite element
                            floatEven[r, c] = index;
                        }
                        index++;
                    }
                }

                return floatEven;
            }

            // Other even dimensions (divisible by 2, but not 4) using Strachey's method.
            int k = dimension;
            int n = (k - 2) / 4;
            int qDimension = k / 2;
            int qElementCount = qDimension * qDimension;
            MatrixFloatComplex A = MagicSquareOdd(qDimension);
            MatrixFloatComplex B = MatrixFloatComplex.ElementAdd(A, qElementCount);
            MatrixFloatComplex C = MatrixFloatComplex.ElementAdd(B, qElementCount);
            MatrixFloatComplex D = MatrixFloatComplex.ElementAdd(C, qElementCount);

            // Exchange first n columns in A with n columns in D
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < A.Samples; j++)
                {
                    float tmp = D[j, i];
                    D[j, i] = A[j, i];
                    A[j, i] = tmp;
                }
            }

            // Exchange right-most n-1 columns in C with B.
            for (int i = C.Channels - n + 1; i < C.Channels; i++)
            {
                for (int j = 0; j < A.Samples; j++)
                {
                    float tmp = C[j, i];
                    C[j, i] = B[j, i];
                    B[j, i] = tmp;
                }
            }

            // Exchange Middle and Centre-Middle squares in D with A
            int middle = (qDimension / 2);
            float swap = D[middle, 0]; D[middle, 0] = A[middle, 0]; A[middle, 0] = swap;
            swap = D[middle, middle]; D[middle, middle] = A[middle, middle]; A[middle, middle] = swap;


            MatrixFloatComplex row1 = MatrixFloatComplex.Join(A, C, MatrixDimensions.Columns);
            MatrixFloatComplex row2 = MatrixFloatComplex.Join(D, B, MatrixDimensions.Columns);

            MatrixFloatComplex output = MatrixFloatComplex.Join(row1, row2, MatrixDimensions.Rows);


            return output;
        }

        /// <summary>
        /// Create a Matrix filled with random numbers between 0.0 and 1.0
        /// </summary>
        /// <param name="rows">The number of rows to initialise the Matrix with.</param>
        /// <param name="columns">The number of columns to initialise the Matrix with.</param>
        /// <param name="seed">A number used to calculate a starting value for the pseudo-random
        /// number sequence.</param>
        /// <returns>A new Matrix filled with random numbers between 0.0 and 1.0</returns>
        public static MatrixFloatComplex Rand(int rows, int columns, int seed)
        {
            MatrixFloatComplex output = new MatrixFloatComplex(rows, columns);
            Random x = new Random(seed);

            for (int i = 0; i < output.data.Length; i++)
                output.data[i] = (float)x.NextDouble();

            return output;
        }

        /// <summary>
        /// Create a Matrix filled with random numbers between 0.0 and 1.0
        /// </summary>
        /// <param name="rows">The number of rows to initialise the Matrix with.</param>
        /// <param name="columns">The number of columns to initialise the Matrix with.</param>
        /// <returns>A new Matrix filled with random numbers between 0.0 and 1.0</returns>
        public static MatrixFloatComplex Rand(int rows, int columns)
        {
            return Rand(rows, columns, (int)DateTime.Now.Ticks & 0x0000FFFF);
        }
        #endregion

        #region Element Operations
        /// <summary>
        /// Run a given operation on every element of a Matrix.
        /// </summary>
        /// <param name="m">The Matrix to operate on.</param>
        /// <param name="number">The value to use in each operation.</param>
        /// <param name="operation">The delegate method to operate with.</param>
        /// <returns>A new Matrix with the original elements operated on appropriately.</returns>
        public static MatrixFloatComplex ElementOperation(MatrixFloatComplex m, float number, ProcessNumbers operation)
        {
            MatrixFloatComplex result = new MatrixFloatComplex(m.Samples, m.Channels);
            for (int i = 0; i < result.data.Length; i++)
                result.data[i] = operation(m.data[i], number);

            return result;
        }

        /// <summary>
        /// Run a given operation on every corresponding element in two Matrix
        /// objects with the same dimensions.
        /// </summary>
        /// <param name="m1">The first Matrix to operate on.</param>
        /// <param name="m2">The second Matrix to operate on.</param>
        /// <param name="operation">The delegate method to operate with.</param>
        /// <returns>A new Matrix with each element from both input Matrix objects
        /// operated on appropriately.</returns>
        public static MatrixFloatComplex ElementOperation(MatrixFloatComplex m1, MatrixFloatComplex m2, ProcessNumbers operation)
        {
            if (m1 == null || m2 == null)
                throw new ArgumentNullException("ElementOperation cannot accept null Matrix objects");
            if (m1.Channels != m2.Channels && m1.Samples != m2.Samples)
                throw new InvalidMatrixDimensionsException("ElementOperation requires both Matrix objects to have the same dimensions");

            MatrixFloatComplex result = new MatrixFloatComplex(m1.Samples, m1.Channels);

            if (m1.HasSameDimensions(m2))
            {
                for (int i = 0; i < result.data.Length; i++)
                    result.data[i] = operation(m1.data[i], m2.data[i]);
            }
            else
            {
                // Matrix/Vector operations.
                if (m1.Channels == m2.Channels)
                {
                    int index = 0;
                    for (int i = 0; i < result.data.Length; i++)
                    {
                        result.data[i] = operation(m1.data[i], m2.data[index++]);
                        if (index == m1.Channels) index = 0;
                    }
                }
                else
                {
                    // same number of rows
                    int index = 0;
                    for (int i = 0; i < result.Samples; i++)
                    {
                        for (int j=0; j < result.Channels; j++)
                        {
                            result.data[index] = operation(m1.data[index], m2.data[i]);
                            index++;
                        }
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Run a given operation on every element of a Matrix.
        /// </summary>
        /// <param name="m">The Matrix to operate on.</param>
        /// <param name="operation">The delegate method to operate with.</param>
        /// <returns>A new Matrix with the original elements operated on appropriately.</returns>
        public static MatrixFloatComplex ElementOperation(MatrixFloatComplex m, ProcessNumber operation)
        {
            MatrixFloatComplex result = new MatrixFloatComplex(m.Samples, m.Channels);
            for (int i = 0; i < result.data.Length; i++)
                result.data[i] = operation(m.data[i]);

            return result;
        }

        #region Specific implementations of ElementOperation (scalars)
        /// <summary>
        /// Add a fixed number to each element in a given Matrix.
        /// </summary>
        /// <param name="m">The Matrix to process.</param>
        /// <param name="number">The number to add to each Matrix element.</param>
        /// <returns>A new Matrix containing elements added to the given number.</returns>
        public static MatrixFloatComplex ElementAdd(MatrixFloatComplex m, float number)
        {
            return ElementOperation(m, number, (x, y) => x + y);
        }

        /// <summary>
        /// Subtract a fixed number from each element in a given Matrix.
        /// </summary>
        /// <param name="m">The Matrix to process.</param>
        /// <param name="number">The number to subract from each Matrix element.</param>
        /// <returns>A new Matrix containing elements subtracted by the given number.</returns>
        public static MatrixFloatComplex ElementSubtract(MatrixFloatComplex m, float number)
        {
            return ElementOperation(m, number, (x, y) => x - y);
        }

        /// <summary>
        /// Multiply each element in a given Matrix by a fixed number.
        /// </summary>
        /// <param name="m">The Matrix to process.</param>
        /// <param name="number">The number to multiply each Matrix element by.</param>
        /// <returns>A new Matrix containing elements multiplied by the given number.</returns>
        public static MatrixFloatComplex ElementMultiply(MatrixFloatComplex m, float number)
        {
            return ElementOperation(m, number, (x, y) => x * y);
        }

        /// <summary>
        /// Divide each element in a given Matrix by a fixed number.
        /// </summary>
        /// <param name="m">The Matrix to process.</param>
        /// <param name="number">The number to divide each Matrix element by.</param>
        /// <returns>A new Matrix containing elements divided by the given number.</returns>
        public static MatrixFloatComplex ElementDivide(MatrixFloatComplex m, float number)
        {
            return ElementOperation(m, number, (x, y) => x / y);
        }

        /// <summary>
        /// Raise each element in a given Matrix by an exponent.
        /// </summary>
        /// <param name="m">The Matrix to process.</param>
        /// <param name="exponent">The exponent to raise each Matrix element by.</param>
        /// <returns>A new Matrix containing elements raised to the power of the given exponent.</returns>
        public static MatrixFloatComplex ElementPower(MatrixFloatComplex m, float exponent)
        {
            return ElementOperation(m, exponent, (x, y) => Mathf.Pow(x, y));
        }

        /// <summary>
        /// Get the square root of each element in a given Matrix.
        /// </summary>
        /// <param name="m">The Matrix to process.</param>
        /// <returns>A new Matrix containing elements that are the square roots of the
        /// original Matrix elements.</returns>
        public static MatrixFloatComplex ElementSqrt(MatrixFloatComplex m)
        {
            return ElementOperation(m, (x) => Mathf.Sqrt(x));
        }

        /// <summary>
        /// Get the absolute value of all Matrix elements.
        /// </summary>
        /// <param name="m">The Matrix to process.</param>
        /// <returns>A new Matrix containing the absolute value of all elements.</returns>
        public static MatrixFloatComplex ElementAbs(MatrixFloatComplex m)
        {
            return ElementOperation(m, (x) => Mathf.Abs(x));
        }

        /// <summary>
        /// Calculate e to the power of m for each element in m.
        /// </summary>
        /// <param name="m">The Matrix to process.</param>
        /// <returns>A Matrix containing elements that are e ^ m for all elements
        /// in the original Matrix m.</returns>
        public static MatrixFloatComplex ElementExp(MatrixFloatComplex m)
        {
            return ElementOperation(m, (x) => Mathf.Pow((float)Math.E, x));
        }

        /// <summary>
        /// Calculate the natural logarithm for each element in m.
        /// </summary>
        /// <param name="m">The Matrix to process.</param>
        /// <returns>A Matrix containing elements a that are e ^ a = m for all elements
        /// in the original Matrix m.</returns>
        public static MatrixFloatComplex ElementLog(MatrixFloatComplex m)
        {
            return ElementOperation(m, (x) => Mathf.Log(x));
        }

        #endregion

        #region Specific implementations of ElementOperation (matrices)
        /// <summary>
        /// Add the corresponding elements in two Matrix objects with the same dimensions.
        /// </summary>
        /// <param name="m1">The first Matrix to process.</param>
        /// <param name="m2">The second Matrix to add values to the first.</param>
        /// <returns>A new Matrix containing elements added from both input Matrix objects.</returns>
        public static MatrixFloatComplex ElementAdd(MatrixFloatComplex m1, MatrixFloatComplex m2)
        {
            return ElementOperation(m1, m2, (x, y) => x + y);
        }

        /// <summary>
        /// Subtract the corresponding elements in two Matrix objects with the same dimensions.
        /// </summary>
        /// <param name="m1">The first Matrix to process.</param>
        /// <param name="m2">The second Matrix to subtract values from the first.</param>
        /// <returns>A new Matrix containing elements subtracted from both input Matrix objects.</returns>
        public static MatrixFloatComplex ElementSubtract(MatrixFloatComplex m1, MatrixFloatComplex m2)
        {
            return ElementOperation(m1, m2, (x, y) => x - y);
        }

        /// <summary>
        /// Multiply the corresponding elements in two Matrix objects with the same dimensions.
        /// </summary>
        /// <param name="m1">The first Matrix to process.</param>
        /// <param name="m2">The second Matrix to multiply values from the first.</param>
        /// <returns>A new Matrix containing elements multiplied from both input Matrix objects.</returns>
        public static MatrixFloatComplex ElementMultiply(MatrixFloatComplex m1, MatrixFloatComplex m2)
        {
            return ElementOperation(m1, m2, (x, y) => x * y);
        }

        /// <summary>
        /// Divide the corresponding elements in two Matrix objects with the same dimensions.
        /// </summary>
        /// <param name="m1">The first Matrix to process.</param>
        /// <param name="m2">The second Matrix to divide values from the first.</param>
        /// <returns>A new Matrix containing elements divided from both input Matrix objects.</returns>
        public static MatrixFloatComplex ElementDivide(MatrixFloatComplex m1, MatrixFloatComplex m2)
        {
            return ElementOperation(m1, m2, (x, y) => x / y);
        }
        #endregion
        #endregion

        #region Dimension Operations
        /// <summary>
        /// Run a given operation on all elements in a particular dimension to reduce that dimension
        /// to a single row or column.
        /// </summary>
        /// <param name="m">The Matrix to operate on.</param>
        /// <param name="dimension">Indicate whether to operate on rows or columns.</param>
        /// <param name="operation">The delegate method to operate with.</param>
        /// <returns>A Matrix populated with the results of performing the given operation.</returns>
        /// <remarks>If the current Matrix is a row or column vector, then a 1*1 Matrix
        /// will be returned, regardless of which dimension is chosen. If the dimension is
        /// set to 'Auto', then the first non-singleton dimension is chosen. If no singleton
        /// dimension exists, then columns are used as the default.</remarks>
        public static MatrixFloatComplex ReduceDimension(MatrixFloatComplex m, MatrixDimensions dimension, ProcessNumbers operation)
        {
            MatrixFloatComplex result = null;

            // Process calculations
            switch (dimension)
            {
                case MatrixDimensions.Auto:
                    // Inspired by Octave, 'Auto' will process the first non-singleton dimension.
                    if (m.Samples == 1 || m.Channels == 1)
                    {
                        result = new MatrixFloatComplex(1, 1);
                        for (int i = 0; i < m.data.Length; i++)
                            result.data[0] = operation(result.data[0], m.data[i]);
                        return result;
                    }
                    else
                    {
                        // No singleton case? Let's go with columns.
                        goto case MatrixDimensions.Columns; // goto?? Haven't used one in years, and it feels good!!!!
                    }
                case MatrixDimensions.Columns:
                    result = new MatrixFloatComplex(1, m.Channels);
                    for (int i = 0; i < m.data.Length; i += m.Channels)
                        for (int j = 0; j < m.Channels; j++)
                            result.data[j] = operation(result.data[j], m.data[i + j]);
                    break;
                case MatrixDimensions.Rows:
                    result = new MatrixFloatComplex(m.Samples, 1);
                    int index = 0;
                    for (int i = 0; i < m.Samples; i++)
                        for (int j = 0; j < m.Channels; j++)
                            result.data[i] = operation(result.data[i], m.data[index++]);
                    break;
                default:
                    break;
            }

            return result;
        }

        #region Specific implementations of ReduceDimension
        /// <summary>
        /// Sum all elements along a specified dimension.
        /// </summary>
        /// <param name="m">The Matrix whose elements need to be added together.</param>
        /// <param name="dimension">The dimension (row or column) to process.</param>
        /// <returns>A 1*n or n*1 Matrix containing the sum of each element along the
        /// processed dimension.</returns>
        public static MatrixFloatComplex Sum(MatrixFloatComplex m, MatrixDimensions dimension = MatrixDimensions.Auto)
        {
            return ReduceDimension(m, dimension, (x, y) => x + y);
        }
        #endregion

        /// <summary>
        /// Run a set of operations on all elements in a particular dimension to reduce that dimension
        /// to a single row, and then perform an aggregate operation to produce a statistical 
        /// </summary>
        /// <param name="m">The Matrix to operate on.</param>
        /// <param name="dimension">Indicate whether to operate on rows or columns.</param>
        /// <param name="operation">The delegate method to operate with.</param>
        /// <remarks>If the current Matrix is a row or column vector, then a 1*1 Matrix
        /// will be returned, regardless of which dimension is chosen. If the dimension is
        /// set to 'Auto', then the first non-singleton dimension is chosen. If no singleton
        /// dimension exists, then columns are used as the default.</remarks>
        public static MatrixFloatComplex StatisticalReduce(MatrixFloatComplex m, MatrixDimensions dimension, ProcessMatrix operation)
        {
            MatrixFloatComplex result = null;

            switch (dimension)
            {
                case MatrixDimensions.Auto:
                    if (m.Samples == 1)
                    {
                        result = new MatrixFloatComplex(1, 1);
                        result.data[0] = operation(m);
                        return result;
                    }
                    else if (m.Channels == 1)
                    {
                        result = new MatrixFloatComplex(1, 1);
                        result.data[0] = operation(m);
                        return result;
                    }
                    else
                    {
                        // No singleton case? Let's go with columns.
                        goto case MatrixDimensions.Columns;
                    }
                case MatrixDimensions.Columns:
                    result = new MatrixFloatComplex(1, m.Channels);
                    for (int i = 0; i < m.Channels; i++)
                        result.data[i] = operation(m.GetColumn(i));
                    break;
                case MatrixDimensions.Rows:
                    result = new MatrixFloatComplex(m.Samples, 1);
                    for (int i = 0; i < m.Samples; i++)
                        result.data[i] = operation(m.GetRow(i));
                    break;
                default:
                    break;
            }

            return result;
        }

        #region Specific implementations of StatisticalReduce
        /// <summary>
        /// Get the mean value of all elements in each dimension of a given Matrix.
        /// </summary>
        /// <param name="m">The Matrix whose elements need to be averaged.</param>
        /// <param name="dimension">The dimension (row or column) to process.</param>
        /// <returns>A 1*n or n*1 Matrix containing the mean of each element along the
        /// processed dimension.</returns>
        public static MatrixFloatComplex Mean(MatrixFloatComplex m, MatrixDimensions dimension = MatrixDimensions.Auto)
        {
            return StatisticalReduce(m, dimension, (x) => x.data.Average());
        }

        /// <summary>
        /// Get the mean value of all elements squared in each dimension of a given Matrix.
        /// </summary>
        /// <param name="m">The Matrix whose squared elements need to be averaged.</param>
        /// <param name="dimension">The dimension (row or column) to process.</param>
        /// <returns>A 1*n or n*1 Matrix containing the mean square of each element along the
        /// processed dimension.</returns>
        public static MatrixFloatComplex MeanSquare(MatrixFloatComplex m, MatrixDimensions dimension = MatrixDimensions.Auto)
        {
            return StatisticalReduce(m, dimension, GetMeanSquare);
        }
        private static float GetMeanSquare(MatrixFloatComplex m)
        {
            float result = 0.0f;
            foreach (float element in m)
            {
                result += Mathf.Pow(element, 2);
            }
            result /= m.data.Count();
            return result;
        }

        /// <summary>
        /// Get the maximum value of all elements in each dimension of a given Matrix.
        /// </summary>
        /// <param name="m">The Matrix to find the maximum value from.</param>
        /// <param name="dimension">The dimension (row or column) to process.</param>
        /// <returns>A 1*n or n*1 Matrix containing the maximum of each element along the
        /// processed dimension.</returns>
        public static MatrixFloatComplex Max(MatrixFloatComplex m, MatrixDimensions dimension = MatrixDimensions.Auto)
        {
            return StatisticalReduce(m, dimension, (x) => x.data.Max());
        }

        /// <summary>
        /// Get the minimum value of all elements in each dimension of a given Matrix.
        /// </summary>
        /// <param name="m">The Matrix to find the minimum value from.</param>
        /// <param name="dimension">The dimension (row or column) to process.</param>
        /// <returns>A 1*n or n*1 Matrix containing the minimum of each element along the
        /// processed dimension.</returns>
        public static MatrixFloatComplex Min(MatrixFloatComplex m, MatrixDimensions dimension = MatrixDimensions.Auto)
        {
            return StatisticalReduce(m, dimension, (x) => x.data.Min());
        }

        /// <summary>
        /// Find the index of the maximum value along a given dimension.
        /// </summary>
        /// <param name="m">The Matrix to find the maximum value index from.</param>
        /// <param name="dimension">The dimension (row or column) to process.</param>
        /// <returns>A 1*n or n*1 Matrix containing the maximum index of each element along the
        /// processed dimension.</returns>
        public static MatrixFloatComplex MaxIndex(MatrixFloatComplex m, MatrixDimensions dimension = MatrixDimensions.Auto)
        {
            return StatisticalReduce(m, dimension, (x) => GetMaxIndex(x));
        }
        private static int GetMaxIndex(MatrixFloatComplex m)
        {
            int maxIndex = 0;
            float maxValue = m.data[0];
            for (int i=0; i<m.data.Length; i++)
            {
                if (m.data[i] > maxValue)
                {
                    maxValue = m.data[i];
                    maxIndex = i;
                }
            }
            return maxIndex;
        }

        /// <summary>
        /// Find the index of the minimum value along a given dimension.
        /// </summary>
        /// <param name="m">The Matrix to find the minimum value index from.</param>
        /// <param name="dimension">The dimension (row or column) to process.</param>
        /// <returns>A 1*n or n*1 Matrix containing the minimum index of each element along the
        /// processed dimension.</returns>
        public static MatrixFloatComplex MinIndex(MatrixFloatComplex m, MatrixDimensions dimension = MatrixDimensions.Auto)
        {
            return StatisticalReduce(m, dimension, (x) => GetMinIndex(x));
        }
        private static int GetMinIndex(MatrixFloatComplex m)
        {
            int minIndex = 0;
            float minValue = m.data[0];
            for (int i = 0; i < m.data.Length; i++)
            {
                if (m.data[i] < minValue)
                {
                    minValue = m.data[i];
                    minIndex = i;
                }
            }
            return minIndex;
        }

        /// <summary>
        /// Get the range of values of all elements in each dimension of a given Matrix.
        /// </summary>
        /// <param name="m">The Matrix to find the range of.</param>
        /// <param name="dimension">The dimension (row or column) to process.</param>
        /// <returns>A 1*n or n*1 Matrix containing the range of each element along the
        /// processed dimension.</returns>
        public static MatrixFloatComplex Range(MatrixFloatComplex m, MatrixDimensions dimension = MatrixDimensions.Auto)
        {
            return StatisticalReduce(m, dimension, (x) => x.data.Max() - x.data.Min());
        }

        /// <summary>
        /// Get the interquartile range of values of all elements in each dimension of a given Matrix.
        /// </summary>
        /// <param name="m">The Matrix to find the interquartile range of.</param>
        /// <param name="dimension">The dimension (row or column) to process.</param>
        /// <returns>A 1*n or n*1 Matrix containing the interquartile range of each element along the
        /// processed dimension.</returns>
        public static MatrixFloatComplex IQR(MatrixFloatComplex m, MatrixDimensions dimension = MatrixDimensions.Auto)
        {
            return StatisticalReduce(m, dimension, (x) => GetQuartile3(x) - GetQuartile1(x) );
        }

        /// <summary>
        /// Get the median value of all elements in each dimension of a given Matrix.
        /// </summary>
        /// <param name="m">The Matrix to find the median of.</param>
        /// <param name="dimension">The dimension (row or column) to process.</param>
        /// <returns>A 1*n or n*1 Matrix containing the median of each element along the
        /// processed dimension.</returns>
        public static MatrixFloatComplex Median(MatrixFloatComplex m, MatrixDimensions dimension = MatrixDimensions.Auto)
        {
            return StatisticalReduce(m, dimension, GetMedian);
        }
        private static float GetMedian(MatrixFloatComplex vector)
        {
            if (vector.data.Length == 1)
                return vector.data[0];
            if (vector.data.Length == 2)
                return (vector.data[0] + vector.data[1]) / 2;

            List<float> data = vector.data.ToList(); data.Sort();
            int index = data.Count / 2;
            if (data.Count % 2 != 0)
                return data[index];
            else
                return (data[index] + data[index + 1]) / 2;
        }

        /// <summary>
        /// Get the first quartile value of all elements in each dimension of a given Matrix.
        /// </summary>
        /// <param name="m">The Matrix to find the first quartile of.</param>
        /// <param name="dimension">The dimension (row or column) to process.</param>
        /// <returns>A 1*n or n*1 Matrix containing the first quartile of each element along the
        /// processed dimension.</returns>
        public static MatrixFloatComplex Quartile1(MatrixFloatComplex m, MatrixDimensions dimension = MatrixDimensions.Auto)
        {
            return StatisticalReduce(m, dimension, GetQuartile1);
        }
        private static float GetQuartile1(MatrixFloatComplex vector)
        {
            if (vector.data.Length == 1)
                return vector.data[0];

            List<float> data = vector.data.ToList(); data.Sort();
            // Handle even number of elements
            if (data.Count % 2 == 0)
            {
                int upperBound = data.Count / 2;
                int index = upperBound / 2;
                if (upperBound % 2 != 0)
                    return data[index];
                else
                    return (data[index - 1] + data[index]) / 2;
            }
            // Handle 4n+1 number of elements
            if ((data.Count - 1) % 4 == 0)
            {
                int n = (data.Count - 1) / 4;
                return ((data[n - 1] * 0.25f) + (data[n] * 0.75f));
            }
            // Handle 4n+3 number of elements
            if ((data.Count - 3) % 4 == 0)
            {
                int n = (data.Count - 3) / 4;
                return ((data[n] * 0.75f) + (data[n + 1] * 0.25f));
            }

            return 0.0f;
        }

        /// <summary>
        /// Get the third quartile value of all elements in each dimension of a given Matrix.
        /// </summary>
        /// <param name="m">The Matrix to find the third quartile of.</param>
        /// <param name="dimension">The dimension (row or column) to process.</param>
        /// <returns>A 1*n or n*1 Matrix containing the third quartile of each element along the
        /// processed dimension.</returns>
        public static MatrixFloatComplex Quartile3(MatrixFloatComplex m, MatrixDimensions dimension = MatrixDimensions.Auto)
        {
            return StatisticalReduce(m, dimension, GetQuartile3);
        }
        private static float GetQuartile3(MatrixFloatComplex vector)
        {
            if (vector.data.Length == 1)
                return vector.data[0];

            List<float> data = vector.data.ToList(); data.Sort();
            // Handle even number of elements
            if (data.Count % 2 == 0)
            {
                int upperBound = data.Count / 2;
                int index = upperBound / 2;
                if (upperBound % 2 != 0)
                    return data[index + upperBound];
                else
                    return (data[index + upperBound - 1] + data[index + upperBound]) / 2;
            }
            // Handle 4n+1 number of elements
            if ((data.Count - 1) % 4 == 0)
            {
                int n = (data.Count - 1) / 4;
                int index = 3 * n;
                return ((data[index] * 0.75f) + (data[index + 1] * 0.25f));
            }
            // Handle 4n+3 number of elements
            if ((data.Count - 3) % 4 == 0)
            {
                int n = (data.Count - 3) / 4;
                int index = 3 * n;
                return ((data[index + 1] * 0.25f) + (data[index + 2] * 0.75f));
            }

            return 0.0f;
        }

        /// <summary>
        /// Get the mode from all elements in each dimension of a given Matrix.
        /// </summary>
        /// <param name="m">The Matrix to find the mode of.</param>
        /// <param name="dimension">The dimension (row or column) to process.</param>
        /// <returns>A 1*n or n*1 Matrix containing the mode of each element along the
        /// processed dimension.</returns>
        public static MatrixFloatComplex Mode(MatrixFloatComplex m, MatrixDimensions dimension = MatrixDimensions.Auto)
        {
            return StatisticalReduce(m, dimension, GetMode);
        }
        private static float GetMode(MatrixFloatComplex vector)
        {
            if (vector.data.Length == 1)
                return vector.data[0];

            // Group all the elements by identical values, and find the maximum
            // count. Then return the minimum value that matches the maximum count.
            var groups = vector.data.GroupBy(element => element);
            int maxCount = groups.Max(group => group.Count());
            var modeValues = groups.Where(group => (group.Count() == maxCount));

            if (modeValues.Count() == 1)
                return modeValues.First().Key;

            // For consistency with Octave, if there are multiple modes, return
            // the one with the smallest value.
            float minValue = modeValues.First().Key;
            foreach (var modeValue in modeValues)
            {
                if (modeValue.Key < minValue)
                    minValue = modeValue.Key;
            }

            return minValue;
        }

        /// <summary>
        /// Calculate the variance of the elements in a given Matrix.
        /// </summary>
        /// <param name="m">The Matrix to find the variance of.</param>
        /// <param name="dimension">The dimension (row or column) to process.</param>
        /// <returns>A 1*n or n*1 Matrix containing the variance of each element along the
        /// processed dimension.</returns>
        public static MatrixFloatComplex Variance(MatrixFloatComplex m, MatrixDimensions dimension = MatrixDimensions.Auto)
        {
            return StatisticalReduce(m, dimension, GetVariance);
        }
        private static float GetVariance(MatrixFloatComplex vector)
        {
            float mean = vector.data.Average();
            float result = 0.0f;
            int n = vector.data.Count();

            foreach(float element in vector)
            {
                result += Mathf.Pow(element - mean, 2);
            }

            return result/(n-1);
        }


        /// <summary>
        /// Calculate the standard deviation of values of all elements in each dimension of a given Matrix.
        /// </summary>
        /// <param name="m">The Matrix to find the standard deviation of.</param>
        /// <param name="dimension">The dimension (row or column) to process.</param>
        /// <returns>A 1*n or n*1 Matrix containing the standard deviation of each element along the
        /// processed dimension.</returns>
        public static MatrixFloatComplex StandardDeviation(MatrixFloatComplex m, MatrixDimensions dimension = MatrixDimensions.Auto)
        {
            return StatisticalReduce(m, dimension, (x) => Mathf.Sqrt(GetVariance(x)));
        }

        #endregion

        /// <summary>
        /// Extract a new Matrix from an existing one, filling in each column sequentially.
        /// </summary>
        /// <param name="m">The Matrix to extract data from.</param>
        /// <param name="startingIndex">The zero-based starting index of the Matrix to start
        /// extracting data from.</param>
        /// <param name="rows">The number of rows in the reshaped Matrix.</param>
        /// <param name="columns">The number of columns in the reshaped Matrix.</param>
        /// <returns>A new Matrix based on the given dimensions.</returns>
        public static MatrixFloatComplex Reshape(MatrixFloatComplex m, int startingIndex, int rows, int columns)
        {
            if (m.data.Length < (startingIndex + (rows * columns)))
                throw new InvalidMatrixDimensionsException("There are not enough elements to reshape the Matrix.");

            MatrixFloatComplex output = new MatrixFloatComplex(rows, columns);

            int dataIndex = startingIndex;
            for (int i = 0; i < columns; i++)
            {
                for (int j = 0; j < rows; j++)
                {
                    output[j, i] = m.data[dataIndex++];
                }
            }
            return output;
        }
        #endregion

        #endregion

        /// <summary>
        /// Implement the GetEnumerator method to run against the data array.
        /// </summary>
        /// <returns>Returns an enumerator for the data array.</returns>
        public IEnumerator GetEnumerator()
        {
            return data.GetEnumerator();
        }

    }
}