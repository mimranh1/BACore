using System;
using System.Runtime.InteropServices;
using FFTWSharp;

namespace BA.BACore
{
    /// <summary>
    /// Realtime Ifft Class
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    public class IfftRealtimeC2R
    {
        /// <summary>
        /// return ifft Length
        /// </summary>
        public int Length => _irIfftInput.Length;

        private readonly float[] _irIfftInput;
        private readonly float[] _irIfftOutput;
        private GCHandle _irIfftInputHandle;
        private GCHandle _irIfftResultHandle;
        private readonly IntPtr _irIfftPlan;

        /// <summary>
        /// define input output Array inside the class
        /// slower than the other method
        /// </summary>
        /// <param name="fftLength">Length of fft</param>
        public IfftRealtimeC2R(int fftLength)
        {
            _irIfftInput = new float[2 * fftLength];
            _irIfftOutput = new float[2 *fftLength];
            _irIfftInputHandle = GCHandle.Alloc(_irIfftInput, GCHandleType.Pinned);
            _irIfftResultHandle = GCHandle.Alloc(_irIfftOutput, GCHandleType.Pinned);
            _irIfftPlan = fftwf.dft_1d(fftLength, _irIfftInputHandle.AddrOfPinnedObject(),
                _irIfftResultHandle.AddrOfPinnedObject(), fftw_direction.Backward,fftw_flags.Estimate);
        }


        /// <summary>
        /// Run the Ifft for input output arrays outside the 
        /// </summary>
        /// <param name="input">input reference complex with smaller 2*hrtfLen</param>
        /// <param name="output">output reference complex with 2*hrtfLen</param>
        public void RunFft(float[] input, float[] output)
        {
            var fftLen = output.Length / 2;
            input.CopyTo(_irIfftInput, 0);
            fftwf.execute(_irIfftPlan);
            
            for (var i = 0; i < output.Length; i++)
            {
                output[i] = _irIfftOutput[i] / fftLen;
            }
            Array.Clear(_irIfftInput, 0, input.Length);
        }

        /// <summary>
        /// Run the Ifft for input output arrays outside the 
        /// </summary>
        /// <param name="input">input reference complex with smaller 2*hrtfLen</param>
        /// <param name="output">output reference real with hrtfLen</param>
        public void RunFftReal(float[] input, float[] output)
        {
            var fftLen = input.Length/2;
            input.CopyTo(_irIfftInput, 0);
            fftwf.execute(_irIfftPlan);

            for (int i = 0; i < output.Length; i++)
            {
                output[i] = _irIfftOutput[2 * i]/fftLen;
            }
            Array.Clear(_irIfftInput, 0, input.Length);
        }

       

        /// <summary>
        /// Clean Up memory
        /// </summary>
        public void Destroy()
        {
            fftwf.destroy_plan(_irIfftPlan);
            if (_irIfftResultHandle.IsAllocated)
                _irIfftResultHandle.Free();
            if (_irIfftInputHandle.IsAllocated)
                _irIfftInputHandle.Free();
        }
    }
}