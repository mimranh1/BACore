using System.IO;
using log4net.Config;
using UnityEngine;

namespace BA.BACore
{
    /// <summary>
    /// Configure the Logging 
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    public static class LoggingConfiguration
    {
        /// <summary>
        /// Configuration for the Log Output
        /// </summary>
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void ConfigureViaFile()
        {
            XmlConfigurator.Configure(new FileInfo($"{BaSettings.Instance.LogPath}/log4net.xml"));
            Debug.Log("Logging configured!");
            DataLogger.DebugData = !Application.isPlaying;
        }

        /// <summary>
        /// Configuration for the Log Output for Latency Real time
        /// </summary>
        public static void ConfigureViaFileRealTimeLatency()
        {
            XmlConfigurator.Configure(new FileInfo($"{BaSettings.Instance.LogPath}/log4net_Latency.xml"));
            Debug.Log("Logging configured!");
            DataLogger.DebugData = false;
        }
        
        /// <summary>
        /// rename the logFiles if exists
        /// </summary>
        /// <param name="timePrefix">prefix of Rename</param>
        public static void MoveLogFiles(string timePrefix, string path = null)
        {
            var sourcePath = $"{BaSettings.Instance.LogPath}";
            if (path != null)
            {
                path = $"{sourcePath}/{path}";
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
            }
            else
            {
                path = $"{sourcePath}";
            }

            if (File.Exists($"{path}/" + timePrefix + "logDataFile.txt"))
                File.Delete($"{path}/" + timePrefix + "logDataFile.txt");
            if (File.Exists($"{path}/" + timePrefix + "logEventFile.txt"))
                File.Delete($"{path}/" + timePrefix + "logEventFile.txt");


            if (File.Exists($"{sourcePath}/logEventFile.txt"))
                File.Move($"{sourcePath}/logEventFile.txt", $"{path}/" + timePrefix + "logEventFile.txt");
            if (File.Exists($"{sourcePath}/logDataFile.txt"))
                File.Move($"{sourcePath}/logDataFile.txt", $"{path}/" + timePrefix + "logDataFile.txt");
        }
    }
}