using System;
using System.Globalization;
using log4net;
using UnityEngine;

namespace BA.BACore
{
    /// <summary>
    /// Log The data to DataLog File
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    public static class DataLogger
    {
        /// <summary>
        /// is the Class active
        /// </summary>
        public static bool DebugData { get; set; }

        private static readonly ILog log = LogManager.GetLogger(typeof(DataLogger));

        /// <summary>
        /// Log the data to DataLog.txt
        /// </summary>
        /// <param name="prefix">name of data in log file</param>
        /// <param name="tf">log TransferFunction</param>
        public static void Log(string prefix, TransferFunction tf)
        {
            if (!DebugData) return;

            log.Debug("data." + prefix + " " + string.Join(" ", Array.ConvertAll(tf.Tf, FloatFToString)));
        }

        /// <summary>
        /// Log the data to DataLog.txt
        /// </summary>
        /// <param name="prefix">name of data in log file</param>
        /// <param name="str">log String</param>
        public static void Log(string prefix, string str)
        {
            if (!DebugData) return;

            log.Debug("data." + prefix + " " + str);
        }

        /// <summary>
        /// Log the data to DataLog.txt
        /// </summary>
        /// <param name="prefix">name of data in log file</param>
        /// <param name="strs">log String Array</param>
        public static void Log(string prefix, string[] strs)
        {
            if (!DebugData) return;

            log.Debug("data." + prefix + " " + string.Join(" ", strs));
        }


        /// <summary>
        /// Log the data to DataLog.txt
        /// </summary>
        /// <param name="prefix">name of data in log file</param>
        /// <param name="f">float array</param>
        public static void Log(string prefix, float[] f)
        {
            if (!DebugData) return;

            log.Debug("data." + prefix + " " + string.Join(" ", Array.ConvertAll(f, FloatFToString)));
        }

        /// <summary>
        /// Log the data to DataLog.txt
        /// </summary>
        /// <param name="prefix">name of data in log file</param>
        /// <param name="f">double array</param>
        public static void Log(string prefix, double[] f)
        {
            if (!DebugData) return;

            log.Debug("data." + prefix + " " + string.Join(" ", Array.ConvertAll(f, DoubleToString)));
        }

        /// <summary>
        /// Log the data to DataLog.txt
        /// </summary>
        /// <param name="prefix">name of data in log file</param>
        /// <param name="i">int array</param>
        public static void Log(string prefix, int[] i)
        {
            if (!DebugData) return;

            log.Debug("data." + prefix + " " + string.Join(" ", Array.ConvertAll(i, IntIToString)));
        }

        /// <summary>
        /// Log the data to DataLog.txt
        /// </summary>
        /// <param name="prefix">name of data in log file</param>
        /// <param name="l">long array</param>
        public static void Log(string prefix, long[] l)
        {
            if (!DebugData) return;

            log.Debug("data." + prefix + " " + string.Join(" ", Array.ConvertAll(l, LongToString)));
        }

        /// <summary>
        /// Log the data to DataLog.txt
        /// </summary>
        /// <param name="prefix">name of data in log file</param>
        /// <param name="i">int array</param>
        public static void Log(string prefix, int i)
        {
            if (!DebugData) return;

            log.Debug("data." + prefix + " " + i);
        }

        /// <summary>
        /// Log the data to DataLog.txt
        /// </summary>
        /// <param name="prefix">name of data in log file</param>
        /// <param name="tfs">TransferFunction array</param>
        public static void Log(string prefix, TransferFunction[] tfs)
        {
            if (!DebugData) return;

            for (var i = 0; i < tfs.Length; i++)
                log.Debug("data." + prefix + "(:," + (i + 1) + ") " +
                          string.Join(" ", Array.ConvertAll(tfs[i].Tf, FloatFToString)));
        }

    

        /// <summary>
        /// Log the data to DataLog.txt
        /// </summary>
        /// <param name="prefix">name of data in log file</param>
        /// <param name="f">float linked array</param>
        public static void Log(string prefix, float[][] f)
        {
            if (!DebugData) return;

            for (var i = 0; i < f.Length; i++)
                log.Debug("data." + prefix + "(:," + (i + 1) + ") " +
                          string.Join(" ", Array.ConvertAll(f[i], FloatFToString)));
        }

        /// <summary>
        /// Log the data to DataLog.txt
        /// </summary>
        /// <param name="prefix">name of data in log file</param>
        /// <param name="f">float double linked array</param>
        /// <param name="index">log index</param>
        public static void Log(string prefix, float[][][] f, int index = 1)
        {
            if (!DebugData) return;
            for (var i = 0; i < f.Length; i++)
            for (var j = 0; j < f[i].Length; j++)
                log.Debug("data." + prefix + "{" + (i + 1) + "," + index + "}(:," + (j + 1) + ") " +
                          string.Join(" ", Array.ConvertAll(f[i][j], FloatFToString)));
        }

        /// <summary>
        /// Log the data to DataLog.txt
        /// </summary>
        /// <param name="prefix">name of data in log file</param>
        /// <param name="single">float</param>
        public static void Log(string prefix, float single)
        {
            if (!DebugData) return;

            log.Debug("data." + prefix + " " + FloatFToString(single));
        }

        /// <summary>
        /// Log the data to DataLog.txt
        /// </summary>
        /// <param name="prefix">name of data in log file</param>
        /// <param name="vec">Vector3 array</param>
        public static void Log(string prefix, Vector3 vec)
        {
            if (!DebugData) return;

            log.Debug(
                "data." + prefix + " " + FloatFToString(vec.x) + " " + FloatFToString(vec.y) + " " + FloatFToString(vec.z));
        }

        /// <summary>
        /// Log the data to DataLog.txt
        /// </summary>
        /// <param name="prefix">name of data in log file</param>
        /// <param name="single">double</param>
        public static void Log(string prefix, double single)
        {
            if (!DebugData) return;

            log.Debug("data." + prefix + " " + DoubleToString(single));
        }

        /// <summary>
        /// Log the data to DataLog.txt
        /// </summary>
        /// <param name="prefix">name of data in log file</param>
        /// <param name="vec">Vector3 linked array</param>
        /// <param name="numSs">number of secondary Sources</param>
        public static void Log(string prefix, Vector3[][] vec, int numSs = -1)
        {
            if (!DebugData) return;

            for (var wallId = 0; wallId < vec.Length; wallId++)
            {
                var x = 0;
                var y = 0;
                for (var j = 0; j < vec[wallId].Length; j++)
                {
                    log.Debug("data." + prefix + "{" + (wallId + 1) + "}(" + (x + 1) + "," + (y + 1) + ",:) " +
                              FloatFToString(vec[wallId][j].x) + " " + FloatFToString(vec[wallId][j].y) + " " +
                              FloatFToString(vec[wallId][j].z));

                    y++;
                    if (y == numSs)
                    {
                        x++;
                        y = 0;
                    }
                }
            }
        }

        /// <summary>
        /// Log the data to DataLog.txt
        /// </summary>
        /// <param name="prefix">name of data in log file</param>
        /// <param name="vec">Vector3 array</param>
        public static void Log(string prefix, Vector3[] vec)
        {
            if (!DebugData) return;

            for (var i = 0; i < vec.Length; i++)
                log.Debug("data." + prefix + "(:," + (i + 1) + ") " + FloatFToString(vec[i].x) + " " +
                          FloatFToString(vec[i].y) + " " + FloatFToString(vec[i].z));
        }

        private static string FloatFToString(float f)
        {
            return f.ToString("e", new CultureInfo("en-US"));
        }

        private static string DoubleToString(double f)
        {
            return f.ToString("e");
        }

        private static string IntIToString(int i)
        {
            return i.ToString();
        }

        private static string LongToString(long i)
        {
            return i.ToString();
        }
    }
}