using System.IO;
using log4net.Appender;
using log4net.Core;
using UnityEngine;

namespace BA.BACore
{
    /// <summary>
    /// Data Log Appender For Realtime Latency Measurement for log4net to File  
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    public class UnityDataPathAppenderLatency : AppenderSkeleton
    {
        /// <summary>
        /// Data Log Appender For Realtime Latency Measurement for log4net to File  
        /// </summary>
        protected override void Append(LoggingEvent loggingEvent)
        {
            //string time = System.DateTime.Now.ToString("yyyyMMdd-HHmmss");
            //string path = @"Log\" + time + "-" + Name + ".txt";
            //string path = @"Log\" + filename + ".txt";
            File.AppendAllText($"{BaSettings.Instance.LogPath}/logRealTimeLatency.txt",
                RenderLoggingEvent(loggingEvent));
        }
    }
}