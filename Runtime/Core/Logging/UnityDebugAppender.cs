using log4net.Appender;
using log4net.Core;
using UnityEditor.MemoryProfiler;
using UnityEngine;

namespace BA.BACore
{
    /// <summary>
    /// Event Log Appender for log4net to Unity Debug Log  
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    public class UnityDebugAppender : AppenderSkeleton
    {
        /// <summary>
        /// Event Log Appender for log4net to Unity Debug Log  
        /// </summary>
        protected override void Append(LoggingEvent loggingEvent)
        {
            var message = RenderLoggingEvent(loggingEvent);

            var objectUnity = loggingEvent.MessageObject as Object;
            if (Level.Compare(loggingEvent.Level, Level.Error) >= 0)
                // everything above or equal to error is an error
                Debug.LogError(message, objectUnity);
            else if (Level.Compare(loggingEvent.Level, Level.Warn) >= 0)
                // everything that is a warning up to error is logged as warning
                Debug.LogWarning(message, objectUnity);
            else
                // everything else we'll just log normally
                Debug.Log(message, objectUnity);
        }
    }
}