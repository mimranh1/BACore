﻿using System.Linq;
using System.Numerics;
using TestMySpline;
using UnityEngine;

namespace BA.BACore
{
    /// <summary>
    /// Used for ReductionIndex index calculation for easier calculation 
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    [System.Serializable]
    public class TransferFunction
    {
        #region Private Members

        [SerializeField] private int numFrequency;

        /// <summary>
        /// store length of transferFunction
        /// </summary>
        [SerializeField] private float[] transferFunction;

        #endregion


        #region Constructors

        /// <summary>
        /// Empty Constructor
        /// </summary>
        public TransferFunction()
        {
            numFrequency = 0;
            transferFunction = new float[0];
        }

        /// <summary>
        /// Constructor to set TransferFunction
        /// </summary>
        /// <param name="transferFunction">Input TransferFunction</param>
        public TransferFunction(float[] transferFunction)
        {
            this.transferFunction = transferFunction;
            numFrequency = transferFunction.Length;
        }

        /// <summary>
        /// Constructor to set empty tf with Length of TransferFunction
        /// </summary>
        /// <param name="numFrequencies">Input Length of TransferFunction</param>
        public TransferFunction(int numFrequencies)
        {
            transferFunction = new float[numFrequencies];
            numFrequency = numFrequencies;
        }

        #endregion


        #region Properties

        /// <summary>
        /// Returns and set the Transfer Function
        /// </summary>
        public float[] Tf
        {
            get => transferFunction;
            set => transferFunction = value;
        }

        /// <summary>
        /// Properties for Length of TransferFunction
        /// </summary>
        public int NumFrequency => numFrequency;

        /// <summary>
        /// Return Max Value in TransferFunction
        /// </summary>
        public float Max => Mathf.Max(Tf);

        /// <summary>
        /// Return Min Value in TransferFunction
        /// </summary>
        public float Min => Mathf.Min(Tf);

        [SerializeField] private float max;
        [SerializeField] private float min;

        /// <summary>
        /// Return Animation Curve based on TransferFunction for plotting purpose 
        /// </summary>
        public AnimationCurve Curve
        {
            get
            {
                if (curve == null)
                {
                    UpdateCurve();
                }

                return curve;
            }
            set
            {
                for (var i = 0; i < value.length; i++)
                    Tf[i] = value[i].value;
            }
        }

        public void UpdateCurve()
        {
            curve = new AnimationCurve();
            // for (var i = 0; i < Tf.Length; i++) curve.AddKey(i, -10 * Mathf.Log10(Tf[i]));
            for (var i = 0; i < Tf.Length; i++) curve.AddKey(i, Tf[i]);
            max = Tf.Max();
            min = Tf.Min();
        }

        [SerializeField] private AnimationCurve curve;

        #endregion


        #region Public Methods

        /// <summary>
        /// Calculate Log for p for each Element
        /// </summary>
        /// <param name="p">input</param>
        /// <returns>Return new TransferFunction</returns>
        public TransferFunction Log(float p)
        {
            var tfReturn = new TransferFunction(numFrequency);
            for (var i = 0; i < numFrequency; i++)
                tfReturn.Tf[i] = Mathf.Log(Tf[i], p);
            return tfReturn;
        }

        /// <summary>
        /// Calculate Log for p for each Element
        /// </summary>
        /// <param name="p">input</param>
        /// <returns>Return new TransferFunction</returns>
        public TransferFunction Log()
        {
            var tfReturn = new TransferFunction(numFrequency);
            for (var i = 0; i < numFrequency; i++)
                tfReturn.Tf[i] = Mathf.Log(Tf[i]);
            return tfReturn;
        }

        /// <summary>
        /// Calculate Log 10 in a new Object of current TransferFunction 
        /// </summary>
        /// <returns>TransferFunction</returns>
        public TransferFunction Log10()
        {
            var tfReturn = new TransferFunction(numFrequency);
            for (var i = 0; i < numFrequency; i++)
                tfReturn.Tf[i] = Mathf.Log10(Tf[i]);
            return tfReturn;
        }

        public static TransferFunction MaxEach(TransferFunction tf1, TransferFunction tf2)
        {
            if (tf1.NumFrequency!=tf2.NumFrequency)
            {
                Debug.LogError("LengthNotValid");
            }

            var result = new TransferFunction(tf1.NumFrequency);
            for (var i = 0; i < tf1.numFrequency; i++)
                result.Tf[i] = Mathf.Max(tf1.Tf[i], tf2.Tf[i]);
            return result;
        }

        public TransferFunction ATan()
        {
            var tfReturn = new TransferFunction(numFrequency);
            for (var i = 0; i < numFrequency; i++)
                tfReturn.Tf[i] = Mathf.Atan(Tf[i]);
            return tfReturn;
        }
        public TransferFunction ASin()
        {
            var tfReturn = new TransferFunction(numFrequency);
            for (var i = 0; i < numFrequency; i++)
                tfReturn.Tf[i] = Mathf.Asin(Tf[i]);
            return tfReturn;
        }

        /// <summary>
        /// Sum up all TF Values
        /// </summary>
        /// <returns>Sum</returns>
        public float Sum()
        {
            var sum = 0.0f;
            for (var i = 0; i < numFrequency; i++) sum += Tf[i];
            return sum;
        }

        public TransferFunction ACos()
        {
            var tfReturn = new TransferFunction(numFrequency);
            for (var i = 0; i < numFrequency; i++) tfReturn.Tf[i] = Mathf.Acos(Tf[i]);
            return tfReturn;
        }

        public TransferFunction Cos()
        {
            var tfReturn = new TransferFunction(numFrequency);
            for (var i = 0; i < numFrequency; i++) tfReturn.Tf[i] = Mathf.Cos(Tf[i]);
            return tfReturn;
        }

        /// <summary>
        /// Calculate Sqrt for each Element and return new TransferFunction
        /// </summary>
        /// <returns>TransferFunction</returns>
        public TransferFunction Sqrt()
        {
            var tfReturn = new TransferFunction(numFrequency);
            for (var i = 0; i < numFrequency; i++) tfReturn.Tf[i] = Mathf.Sqrt(Tf[i]);
            return tfReturn;
        }


        /// <summary>
        /// set each element bigger den Input to Input
        /// </summary>
        /// <param name="max">input maximum</param>
        public void SetMaxValue(float max)
        {
            for (var i = 0; i < numFrequency; i++)
                if (Tf[i] > max)
                    Tf[i] = max;
        }

        /// <summary>
        /// set each element lower den Input to Input
        /// </summary>
        /// <param name="min">input minimum</param>
        public void SetMinValue(float min)
        {
            for (var i = 0; i < numFrequency; i++)
                if (Tf[i] < min)
                    Tf[i] = min;
        }

        #endregion


        #region Override Functions

        /// <summary>
        /// Override Equals to override == Operator
        /// </summary>
        /// <param name="obj">Object</param>
        /// <returns>true if TransferFunction are equal</returns>
        public override bool Equals(object obj)
        {
            var item = obj as TransferFunction;

            if (item == null) return false;

            return this == item;
        }

        /// <summary>
        /// Override GetHashCode to override == Operator
        /// </summary>
        /// <returns>HashCode</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        #endregion


        #region Override Operators

        /// <summary>
        /// compare each frequencyBand
        /// </summary>
        /// <param name="tf1">left input</param>
        /// <param name="tf2">right input</param>
        /// <returns>true if tf1 and tf2 are elementwise same</returns>
        public static bool operator ==(TransferFunction tf1, TransferFunction tf2)
        {
            if (ReferenceEquals(tf2, null)) return ReferenceEquals(tf1, null);
            if (ReferenceEquals(tf1, null)) return false;
            
            const float epsilon = 0.0001f;
    
                for (var i = 0; i < Mathf.Max(new float[] {tf1.numFrequency, tf2.numFrequency}); i++)
                    if (tf1.transferFunction[i] - tf2.transferFunction[i] > epsilon)
                        return false;
            return true;
        }

        /// <summary>
        /// compare each frequencyBand
        /// </summary>
        /// <param name="tf1">left input</param>
        /// <param name="tf2">right input</param>
        /// <returns>true if tf1 and tf2 are elementwise not same</returns>
        public static bool operator !=(TransferFunction tf1, TransferFunction tf2)
        {
            if (ReferenceEquals(tf1, null))
            {
                return !ReferenceEquals(tf2, null);
            }
            if (ReferenceEquals(tf2, null)) return true;

            const float epsilon = 0.0001f;
            for (var i = 0; i < Mathf.Max(new float[] {tf1.numFrequency, tf2.numFrequency}); i++)
                if (tf1.transferFunction[i] - tf2.transferFunction[i] > epsilon)
                    return true;
            return false;
        }

        /// <summary>
        /// calculate pow for each Element in tf with exponent
        /// </summary>
        /// <param name="tf1">transfer function which will be the base</param>
        /// <param name="exponent">exponent</param>
        /// <returns>new TransferFunction</returns>
        public static TransferFunction operator ^(TransferFunction tf1, float exponent)
        {
            var tf = new TransferFunction(tf1.numFrequency);

            for (var i = 0; i < tf1.numFrequency; i++)
                tf.transferFunction[i] = Mathf.Pow(tf1.transferFunction[i], exponent);

            return tf;
        }

        /// <summary>
        /// calculate pow element by element
        /// </summary>
        /// <param name="tf1">transfer function which will be the base</param>
        /// <param name="tf2">transfer function which will be the exponent</param>
        /// <returns>new TransferFunction</returns>
        public static TransferFunction operator ^(TransferFunction tf1, TransferFunction tf2)
        {
            var tf = new TransferFunction(tf1.numFrequency);

            for (var i = 0; i < tf1.numFrequency; i++)
                tf.transferFunction[i] = Mathf.Pow(tf1.transferFunction[i], tf2.transferFunction[i]);

            return tf;
        }

        /// <summary>
        /// calculate pow for each Element in tf with exponent
        /// </summary>
        /// <param name="base">float base</param>
        /// <param name="tf1">tf exponent</param>
        /// <returns>new TransferFunction</returns>
        public static TransferFunction operator ^(float @base, TransferFunction tf1)
        {
            var tf = new TransferFunction(tf1.numFrequency);

            for (var i = 0; i < tf1.numFrequency; i++) tf.transferFunction[i] = Mathf.Pow(@base, tf1.transferFunction[i]);

            return tf;
        }

        /// <summary>
        /// Overload +-Operator
        /// use + for each transferFunction Element and compare frequency band
        /// </summary>
        /// <param name="tf1">left input. tf</param>
        /// <param name="tf2">right input. tf</param>
        /// <returns>new TransferFunction</returns>
        public static TransferFunction operator +(TransferFunction tf1, TransferFunction tf2)
        {
            var tf = new TransferFunction(tf2.numFrequency);

            if (tf1.numFrequency == tf2.numFrequency)
                for (var i = 0; i < tf1.numFrequency; i++)
                    tf.transferFunction[i] = tf1.transferFunction[i] + tf2.transferFunction[i];
            else
                Debug.LogError("TransferFunctions have to have the same length.");
            return tf;
        }

        /// <summary>
        /// Overload +-Operator
        /// use + for each transferFunction Element and compare frequency band
        /// </summary>
        /// <param name="value">left input. float</param>
        /// <param name="tf1">right input. tf</param>
        /// <returns>new TransferFunction</returns>
        public static TransferFunction operator +(float value, TransferFunction tf1)
        {
            var tf = new TransferFunction(tf1.numFrequency);
            for (var i = 0; i < tf1.numFrequency; i++) tf.transferFunction[i] = tf1.transferFunction[i] + value;
            return tf;
        }

        /// <summary>
        /// Overload +-Operator
        /// use + for each transferFunction Element and compare frequency band
        /// </summary>
        /// <param name="tf1">left input. tf</param>
        /// <param name="value">right input. float</param>
        /// <returns>new TransferFunction</returns>
        public static TransferFunction operator +(TransferFunction tf1, float value)
        {
            var tf = new TransferFunction(tf1.numFrequency);
            for (var i = 0; i < tf1.numFrequency; i++) tf.transferFunction[i] = value + tf1.transferFunction[i];

            return tf;
        }

        /// <summary>
        /// Overload --Operator
        /// use - for each transferFunction Element and compare frequency band
        /// </summary>
        /// <param name="tf1">left input</param>
        /// <param name="tf2">right input</param>
        /// <returns>new TransferFunction</returns>
        public static TransferFunction operator -(TransferFunction tf1, TransferFunction tf2)
        {
            var tf = new TransferFunction(tf1.numFrequency);
            if (tf1.numFrequency == tf2.numFrequency)
                for (var i = 0; i < tf1.numFrequency; i++)
                    tf.transferFunction[i] = tf1.transferFunction[i] - tf2.transferFunction[i];
            else
                Debug.LogError("TransferFunctions have to have the same length. " + tf1.numFrequency + " " +
                               tf2.numFrequency);
            return tf;
        }

        /// <summary>
        /// Overload --Operator
        /// use - for each transferFunction Element and compare frequency band
        /// </summary>
        /// <param name="tf1">left input</param>
        /// <param name="value">right input</param>
        /// <returns>new TransferFunction</returns>
        public static TransferFunction operator -(TransferFunction tf1, float value)
        {
            var tf = new TransferFunction(tf1.numFrequency);
            for (var i = 0; i < tf1.numFrequency; i++) tf.transferFunction[i] = tf1.transferFunction[i] - value;
            return tf;
        }

        /// <summary>
        /// Overload --Operator
        /// use - for each transferFunction Element and compare frequency band
        /// </summary>
        /// <param name="value">left input</param>
        /// <param name="tf1">right input</param>
        /// <returns>new TransferFunction</returns>
        public static TransferFunction operator -(float value, TransferFunction tf1)
        {
            var tf = new TransferFunction(tf1.numFrequency);
            for (var i = 0; i < tf1.numFrequency; i++) tf.transferFunction[i] = value - tf1.transferFunction[i];
            return tf;
        }

        /// <summary>
        /// Overload *-Operator
        /// use * for each transferFunction Element and compare frequency band
        /// </summary>
        /// <param name="tf1">left input</param>
        /// <param name="tf2">right input</param>
        /// <returns>new TransferFunction</returns>
        public static TransferFunction operator *(TransferFunction tf1, TransferFunction tf2)
        {
            var tf = new TransferFunction(tf1.numFrequency);
            if (tf1.numFrequency == tf2.numFrequency)
                for (var i = 0; i < tf1.numFrequency; i++)
                    tf.transferFunction[i] = tf1.transferFunction[i] * tf2.transferFunction[i];
            else
                Debug.LogError(
                    "TransferFunctions have to have the same length." + tf1.numFrequency + " " + tf2.numFrequency);
            return tf;
        }

        /// <summary>
        /// Overload *-Operator
        /// use * for each transferFunction Element and compare frequency band
        /// </summary>
        /// <param name="tf">left input</param>
        /// <param name="cin">right input</param>
        /// <returns>new Complex[]</returns>
        public static Complex[] operator *(TransferFunction tf, Complex[] cin)
        {
            var retComplex = new Complex[cin.Length];
            if (tf.numFrequency == cin.Length)
                for (var i = 0; i < tf.numFrequency; i++)
                    retComplex[i] = tf.transferFunction[i] * cin[i];
            else
                Debug.LogError("TransferFunctions have to have the same length." + tf.numFrequency + " " + cin.Length);
            return retComplex;
        }

        /// <summary>
        /// Overload *-Operator
        /// use * for each transferFunction Element and compare frequency band
        /// </summary>
        /// <param name="tf">left input</param>
        /// <param name="cin">right input</param>
        /// <returns>new Complex[]</returns>
        public static Complex[] operator *(Complex[] cin, TransferFunction tf)
        {
            var retComplex = new Complex[cin.Length];
            if (tf.numFrequency == cin.Length)
                for (var i = 0; i < tf.numFrequency; i++)
                    retComplex[i] = tf.transferFunction[i] * cin[i];
            else
                Debug.LogError("TransferFunctions have to have the same length." + tf.numFrequency + " " + cin.Length);
            return retComplex;
        }

        /// <summary>
        /// Overload *-Operator
        /// use * for each transferFunction Element and compare frequency band
        /// </summary>
        /// <param name="value">left input</param>
        /// <param name="tf1">right input</param>
        /// <returns>new TransferFunction</returns>
        public static TransferFunction operator *(float value, TransferFunction tf1)
        {
            var tf = new TransferFunction(tf1.numFrequency);
            for (var i = 0; i < tf1.numFrequency; i++) tf.transferFunction[i] = value * tf1.transferFunction[i];
            return tf;
        }

        /// <summary>
        /// Overload *-Operator
        /// use * for each transferFunction Element and compare frequency band
        /// </summary>
        /// <param name="tf1">left input</param>
        /// <param name="value">right input</param>
        /// <returns>new TransferFunction</returns>
        public static TransferFunction operator *(TransferFunction tf1, float value)
        {
            var tf = new TransferFunction(tf1.numFrequency);
            for (var i = 0; i < tf1.numFrequency; i++) tf.transferFunction[i] = value * tf1.transferFunction[i];
            return tf;
        }

        /// <summary>
        /// Overload /-Operator
        /// use / for each transferFunction Element and compare frequency band
        /// </summary>
        /// <param name="tf1">left input</param>
        /// <param name="tf2">right input</param>
        /// <returns>new TransferFunction</returns>
        public static TransferFunction operator /(TransferFunction tf1, TransferFunction tf2)
        {
            var tf = new TransferFunction(tf1.numFrequency);
            if (tf1.numFrequency == tf2.numFrequency)
                for (var i = 0; i < tf1.numFrequency; i++)
                    tf.transferFunction[i] = tf1.transferFunction[i] / tf2.transferFunction[i];
            else
                Debug.LogError("TransferFunctions have to have the same length.");
            return tf;
        }

        /// <summary>
        /// Overload /-Operator
        /// use / for each transferFunction Element and compare frequency band
        /// </summary>
        /// <param name="tf1">left input</param>
        /// <param name="value">right input</param>
        /// <returns>new TransferFunction</returns>
        public static TransferFunction operator /(TransferFunction tf1, float value)
        {
            var tf = new TransferFunction(tf1.numFrequency);
            for (var i = 0; i < tf1.numFrequency; i++) tf.transferFunction[i] = tf1.transferFunction[i] / value;
            return tf;
        }

        /// <summary>
        /// Overload /-Operator
        /// use / for each transferFunction Element and compare frequency band
        /// </summary>
        /// <param name="value">left input</param>
        /// <param name="tf1">right input</param>
        /// <returns>new TransferFunction</returns>
        public static TransferFunction operator /(float value, TransferFunction tf1)
        {
            var tf = new TransferFunction(tf1.numFrequency);
            for (var i = 0; i < tf1.numFrequency; i++) tf.transferFunction[i] = value / tf1.transferFunction[i];
            return tf;
        }

        #endregion
    }
}