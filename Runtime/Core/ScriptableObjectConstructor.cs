﻿using System;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace BA.BACore
{
    public static class ScriptableObjectConstructor
    {
        public static TClass Create<TClass, TType>(TType propertiesTypes) where TClass : ExtendedScriptableObject<TClass, TType> where TType : Enum
        {
            var allDetectorTypes = Assembly.GetAssembly(typeof(TClass)).GetTypes()
                .Where(t => typeof(TClass).IsAssignableFrom(t) && t.IsAbstract == false);

            foreach (var detectorType in allDetectorTypes)
            {
                var detector = ScriptableObject.CreateInstance(detectorType) as TClass;
                if (Equals(detector.Type, propertiesTypes))
                {
                    return detector;
                }
            }

            Debug.LogError($"Room Acoustic Property Type {propertiesTypes} not found");
            return null;
        }
    }
}