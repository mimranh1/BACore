﻿using System;
using UnityEngine;
using UnityEngine.Scripting;

namespace BA.BACore
{
    public class AuralisationBehavior : MonoBehaviour
    {
        [SerializeReference] private AuralisationCalculationSelector _auralisationCalculation;
        public AuralisationCalculation AuralisationCalculation => _auralisationCalculation.@object;

        [Header("Audio Renderer")]
        //[SerializeField] private AudioRendererType audioRendererType;
        [SerializeField] public AudioRender audioRenderer;

        private bool init = false;
        
        private void OnValidate()
        {
            Debug.Log("OnValidate", this);
            Awake();
            if (!Application.isPlaying || !init)
            {
                _auralisationCalculation.@object.OnValidate();
                return;
            }
            
            _auralisationCalculation.@object.RealTimeCalculationMeasure(audioRenderer, true);
            _auralisationCalculation.@object.OnValidate();
        }

        public void Awake()
        {
            if (_auralisationCalculation == null)
                _auralisationCalculation = ScriptableObject.CreateInstance<AuralisationCalculationSelector>();
            _auralisationCalculation.@object.OnValidate();
        }

        public void Start()
        {
            Init();
            if (audioRenderer != null)
                audioRenderer.Init(_auralisationCalculation.@object);
            CalculateOfflineFilter();

            init = true;
        }

        [ContextMenu("Init")]
        public void Init()
        {
            _auralisationCalculation.@object.InitMeasure();
        }

        public void Update()
        {
            UpdateRealTimeFilter();
        }
        
        public void CalculateOfflineFilter()
        {
            _auralisationCalculation.@object.OfflineCalculationMeasure();
            ForceUpdate();
            _auralisationCalculation.@object.RealTimeSourceUpdateMeasure();
            _auralisationCalculation.@object.RealTimeReceiverUpdateMeasure();
            _auralisationCalculation.@object.RealTimeCalculationMeasure(audioRenderer);
        }

        public void ForceUpdate()
        {
            _auralisationCalculation.@object.ForceReceiverUpdate = true;
            _auralisationCalculation.@object.ForceSourceUpdate = true;
        }

        private void UpdateRealTimeFilter()
        {
            if (_auralisationCalculation.@object.RealTimeSourceUpdateMeasure() |
                _auralisationCalculation.@object.RealTimeReceiverUpdateMeasure())
                _auralisationCalculation.@object.RealTimeCalculationMeasure(audioRenderer);
        }

        private void OnDisable()
        {
            _auralisationCalculation.@object.OnDisable();
            audioRenderer.Disable();
        }

        [ContextMenu("Save Data")]
        public void SaveData()
        {
            SaveData("");
        } 
        
        public void SaveData(string name="")
        {
            _auralisationCalculation.@object.SaveImpulseResponse(name);
        }

        [ContextMenu("SaveAllDnT")]
        public void SaveAllDnT()
        {
            var dnts = GetComponents<MeasureDnt>();
            foreach (var dnt in dnts)
            {
                dnt.MeasureDnT();
            }
        }
    }
}