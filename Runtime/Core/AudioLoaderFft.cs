﻿using System;
using System.Collections;
using System.IO;
using System.Runtime.InteropServices;
using FFTWSharp;
using UnityEngine;
using UnityEngine.Networking;

namespace BA.BACore
{
    public class AudioLoaderFft : MonoBehaviour
    {
        public float stepLength = 1f;
        public int numIndexes = 300;

        public string pattern = "*.J";
        public string filterPath;
        public Vector3 directionLine;

        private float[] dirPoints;
        private AudioClip[] audioClips;
        private bool[] loaded;

        public int filterLength = 44100 * 5 / 2;
        private float[] fftIrTimeComplex;
        private GCHandle fftIrTimeHandle;
        private float[] fftIrFreqComplex;
        private GCHandle fftIrFreqHandle;
        private IntPtr fftIrPlan;

        private float[][][] irAudioDataFreq;

        public bool allLoaded;

        public void ReadAudioMonoFreq()
        {
            audioClips = new AudioClip[numIndexes];
            loaded = new bool[numIndexes];
            dirPoints = new float[numIndexes];

            fftIrTimeComplex = new float[2*filterLength];
            fftIrTimeHandle = GCHandle.Alloc(fftIrTimeComplex, GCHandleType.Pinned);
            fftIrFreqComplex = new float[filterLength * 2];
            fftIrFreqHandle = GCHandle.Alloc(fftIrFreqComplex, GCHandleType.Pinned);
            fftIrPlan = fftwf.dft_1d(filterLength, fftIrTimeHandle.AddrOfPinnedObject(),
                fftIrFreqHandle.AddrOfPinnedObject(), fftw_direction.Forward,fftw_flags.Estimate);
            irAudioDataFreq = new float[numIndexes][][];

            for (var i = 0; i < numIndexes; i++)
            {
                dirPoints[i] = (i - numIndexes / 2) / stepLength;
                var fileNames = Directory.GetFiles(filterPath, pattern + (i + 1) + ".?av");
                var fullPath = fileNames[0];
                fullPath = "file:///" + fullPath;
                StartCoroutine(GetAudioClip2ArrayAndFft(fullPath, i));
                // GetAudioClip2ArrayOffline(fullPath, i);
            }

            var rout = StartCoroutine(WaitForComplete());
        }

        public void GetFilter(float[] filter, Vector3 sourcePos, Vector3 receiverPos)
        {
            if (!allLoaded)
                StartCoroutine(WaitForComplete());

            var pos = Vector3.Dot(receiverPos - sourcePos, directionLine);
            var index = GetClosestIndex(dirPoints, pos);


            // audioClips[index].GetData(filter, 0);
        }

        public void GetFilterFreq(float[] filter, Vector3 sourcePos, Vector3 receiverPos, int channel)
        {
            if (!allLoaded)
            {
                Debug.LogWarning("Filter not Loaded yet.");
                return;
            }

            var pos = Vector3.Dot(receiverPos - sourcePos, directionLine);
            var index = GetClosestIndex(dirPoints, pos);

            irAudioDataFreq[index][channel].CopyTo(filter, 0);


            // audioClips[index].GetData(filter, 0);
        }

        private int GetClosestIndex(float[] array, float cmp)
        {
            if (cmp < array[0])
            {
                if (array[0] - cmp > stepLength)
                    Debug.LogWarning("Position Out of Range");
                return 0;
            }

            var lastIndex = array.Length - 1;
            if (cmp > array[lastIndex])
            {
                if (array[lastIndex] - cmp > stepLength)
                    Debug.LogWarning("Position Out of Range");
                return lastIndex;
            }

            var minValue = Mathf.Infinity;

            for (var i = 0; i < array.Length; i++)
            {
                var diff = Mathf.Abs(array[i] - cmp);
                if (minValue < diff)
                {
                    minValue = diff;
                }
                else
                {
                    if (diff < minValue)
                        return i;
                    return i - 1;
                }
            }

            return -1;
        }

        private IEnumerator GetAudioClip(string url, int index)
        {
            using (var uwr = UnityWebRequestMultimedia.GetAudioClip(url, AudioType.WAV))
            {
                yield return uwr.SendWebRequest();
                if (uwr.isNetworkError || uwr.isHttpError)
                {
                    Debug.LogError(uwr.error);
                    yield break;
                }

                var names = url.Split(new[] {'/', '\\'});
                audioClips[index] = DownloadHandlerAudioClip.GetContent(uwr);
                audioClips[index].name = names[names.Length - 1];
                // use audio clip
                Debug.Log("Audio Downloaded.");
                loaded[index] = true;
            }
        }

        private IEnumerator GetAudioClip2ArrayAndFft(string url, int index)
        {
            using (var uwr = UnityWebRequestMultimedia.GetAudioClip(url, AudioType.WAV))
            {
                yield return uwr.SendWebRequest();
                if (uwr.isNetworkError || uwr.isHttpError)
                {
                    Debug.LogError(uwr.error);
                    yield break;
                }

                var audioClip = DownloadHandlerAudioClip.GetContent(uwr);
                var numChannel = audioClip.channels;
                var numSamples = audioClip.samples;
                var audioClipData = new float[numChannel * numSamples];
                audioClip.GetData(audioClipData, 0);
                irAudioDataFreq[index] = new float[numChannel][];
                for (var iChannel = 0; iChannel < numChannel; iChannel++)
                {
                    irAudioDataFreq[index][iChannel] = new float[2 * filterLength];
                    for (var i = 0; i < fftIrTimeComplex.Length/2; i++)
                        if (i < numSamples)
                            fftIrTimeComplex[2*i] = audioClipData[i * numChannel + iChannel];
                        else
                            fftIrTimeComplex[2*i] = 0;
                    fftwf.execute(fftIrPlan);
                    fftIrFreqComplex.CopyTo(irAudioDataFreq[index][iChannel], 0);
                }

                loaded[index] = true;
            }
        }

        private IEnumerator GetAudioClip2Array(string url, int index)
        {
            using (var uwr = UnityWebRequestMultimedia.GetAudioClip(url, AudioType.WAV))
            {
                yield return uwr.SendWebRequest();
                if (uwr.isNetworkError || uwr.isHttpError)
                {
                    Debug.LogError(uwr.error);
                    yield break;
                }

                var audioClip = DownloadHandlerAudioClip.GetContent(uwr);
                var numChannel = audioClip.channels;
                var numSamples = audioClip.samples;
                var audioClipData = new float[numChannel * numSamples];
                audioClip.GetData(audioClipData, 0);
                irAudioDataFreq[index] = new float[numChannel][];
                for (var iChannel = 0; iChannel < numChannel; iChannel++)
                {
                    irAudioDataFreq[index][iChannel] = new float[2 * filterLength];
                    for (var n = 0; n < fftIrTimeComplex.Length; n++)
                        if (n < numSamples)
                            fftIrTimeComplex[n] = audioClipData[n * numChannel + iChannel];
                        else
                            fftIrTimeComplex[n] = 0;
                    fftwf.execute(fftIrPlan);
                    fftIrFreqComplex.CopyTo(irAudioDataFreq[index][iChannel], 0);
                }

                loaded[index] = true;
            }
        }

        private IEnumerator WaitForComplete()
        {
            for (var i = 0; i < loaded.Length; i++)
                if (!loaded[i])
                    yield return new WaitForSeconds(0.1f);
            Debug.Log("AudioLoaded.");
            allLoaded = true;
        }
    }
}