﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]   
    public abstract class WallGeometryAuto : WallGeometry
    {
  
        
        protected Vector3 RoundDimensions(Vector3 dimensions)
        {
            return new Vector3(Mathf.Round(Math.Abs(dimensions.x) * 100.0f) / 100.0f,
                Mathf.Round(Math.Abs(dimensions.y) * 100.0f) / 100.0f,
                Mathf.Round(Math.Abs(dimensions.z) * 100f) / 100f);

        }

        protected Vector3 GetNormalVector(Vector3 dimensions)
        {
            var min = Mathf.Min(dimensions.x, dimensions.y, dimensions.z);
            if (Math.Abs(min - dimensions.x) < float.Epsilon) return Vector3.right;
            if (Math.Abs(min - dimensions.y) < float.Epsilon) return Vector3.up;
            if (Math.Abs(min - dimensions.z) < float.Epsilon) return Vector3.forward;
            Debug.LogError("Could not calculate Normal Vector for Wall");
            return Vector3.zero;
        }
        
        protected Vector3 GetNormalVector(Vector3 dimensions, Quaternion rotation)
        {
            var min = Mathf.Min(dimensions.x, dimensions.y, dimensions.z);
            if (Math.Abs(min - dimensions.x) < float.Epsilon) return rotation*Vector3.right;
            if (Math.Abs(min - dimensions.y) < float.Epsilon) return rotation*Vector3.up;
            if (Math.Abs(min - dimensions.z) < float.Epsilon) return rotation*Vector3.forward;
            Debug.LogError("Could not calculate Normal Vector for Wall");
            return Vector3.zero;
        }
    }
}