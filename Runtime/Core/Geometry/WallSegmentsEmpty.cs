﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class WallSegmentsEmpty : WallSegments
    {
        public override WallSegmentType Type => WallSegmentType.Empty;
        public override List<GeometrySs> SecondarySources => secondarySources;

        [SerializeField] private List<GeometrySs> secondarySources=new List<GeometrySs>();
        public override void ProcessAll(WallBehaviour wall)
        {
            base.ProcessAll(wall);
            if (secondarySources.Count ==0)
                secondarySources.Add(new GeometrySs(wall));
        }

    }
}