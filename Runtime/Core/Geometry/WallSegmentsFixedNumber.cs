﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class WallSegmentsFixedNumber : WallSegments
    {
        public override WallSegmentType Type => WallSegmentType.FixedNumber;
        public override List<GeometrySs> SecondarySources => secondarySources;
      
        [SerializeField] private List<GeometrySs> secondarySources;

        public int numberOfSecondarySourcesPerDim;

        public override void ProcessAll(WallBehaviour wall)
        {
            secondarySources = GenerateSecondarySourcePositionsFromFixedNumber(wall);
            if (secondarySources.Count ==0)
                secondarySources.Add(new GeometrySs(wall));
        }


  

        private List<GeometrySs> GenerateSecondarySourcePositionsFromFixedNumber(WallBehaviour wall, int id = -1)
        {
            var boundWall = wall.Geometry.Dimensions;
            var posWall = wall.Geometry.Position;
            var normWall = wall.Geometry.Normal;

            var pos1 = posWall - 0.5f * boundWall;
            var pos2 = posWall + 0.5f * boundWall;

            var deltaX = (pos2.x - pos1.x) / numberOfSecondarySourcesPerDim;
            var deltaY = (pos2.y - pos1.y) / numberOfSecondarySourcesPerDim;
            var deltaZ = (pos2.z - pos1.z) / numberOfSecondarySourcesPerDim;
            
            return GetSsPositionsFromDelta(normWall, deltaX, deltaY, deltaZ, pos1,numberOfSecondarySourcesPerDim);
        }
    }
}