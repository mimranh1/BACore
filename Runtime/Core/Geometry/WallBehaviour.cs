﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace BA.BACore
{
    public class WallBehaviour : MonoBehaviour
    {
        public WallGeometry Geometry => geometry.@object;
        [SerializeField] private WallGeometrySelector geometry;

        public SoundReductionIndex ReductionIndex => reductionIndex.@object;
        [SerializeField] public SoundReductionIndexSelector reductionIndex;

        [Header("Flanking Settings")] public bool applyFlanking;

        [SerializeField] private GeometryDetectionType geometryDetectionType;

        [SerializeReference] public Junctions Junctions;

        [Header("Patches Settings")] public bool applyPatches;

        [SerializeReference] public WallPatchesHandler patchesHandler;

        [Header("Segments Settings")] public bool applySegments;
        public WallSegments WallReceiverSegments => wallReceiverSegments?.@object;

        [SerializeField] protected WallSegmentsSelector wallReceiverSegments;

        public WallSegments WallSourceSegments => wallSourceSegments?.@object;
        [SerializeField] protected WallSegmentsSelector wallSourceSegments;

        /// <summary>
        /// Return Mass per m^2 of the Wall
        /// </summary>
        public float Mass => reductionIndex.@object.Mass;

        /// <summary>
        /// Get Critical Frequency of the Wall
        /// </summary>
        public float CriticalFrequency => reductionIndex.@object.CriticalFrequency;

        /// <summary>
        /// correction term for fc
        /// </summary>
        public TransferFunction CriticalFrequencyEff => reductionIndex.@object.CriticalFrequencyEff;

        public List<WallBehaviour> Patches => patchesHandler?.Patches;

        [ContextMenu("Copy Component")]
        private void CopyComponent()
        {
            
        }
        
        protected void OnValidate()
        {
            Awake();
            Geometry?.OnValidate();

            ReductionIndex?.OnValidate();
            Junctions?.OnValidate();

            wallReceiverSegments?.@object.ProcessAll(this);
            wallSourceSegments?.@object.ProcessAll(this);
        }

        [ContextMenu("Init")]
        public void Awake()
        {
            if (geometry == null)
                geometry = ScriptableObject.CreateInstance<WallGeometrySelector>();
            geometry.@object.Init(gameObject);
            if (applySegments)
            {
                if (wallReceiverSegments == null)
                    wallReceiverSegments = ScriptableObject.CreateInstance<WallSegmentsSelector>();
                wallReceiverSegments.@object.ProcessAll(this);

                if (wallSourceSegments == null)
                    wallSourceSegments = ScriptableObject.CreateInstance<WallSegmentsSelector>();
                wallSourceSegments.@object.ProcessAll(this);
            }
            else
            {
                wallReceiverSegments = null;
                wallSourceSegments = null;
            }

            if (reductionIndex == null)
                reductionIndex = ScriptableObject.CreateInstance<SoundReductionIndexSelector>();
            reductionIndex.@object.Init(gameObject);


            if (applyPatches)
            {
                if (patchesHandler == null)
                    patchesHandler = new WallPatchesHandler(GetInstanceID(), Geometry, transform);
            }
            else
            {
                patchesHandler = null;
            }

            if (applyFlanking)
            {
                if (Junctions == null)
                    Junctions = new Junctions(Geometry.Position, Geometry.Dimensions, Geometry.Normal,
                        geometryDetectionType);
            }
            else
            {
                Junctions = null;
            }
        }

        [ContextMenu("CalcSoundReductionIndex")]
        private void CalcSoundReductionIndex()
        {
            ReductionIndex?.CalcSoundReductionIndex();
        }

        public void OnDrawGizmosSelected()
        {
            WallSourceSegments?.OnDrawGizmosSelected(new Color32(0, 255, 255, 150));
            WallSourceSegments?.OnDrawGizmosSelected(new Color32(255, 0, 255, 100));
            Junctions?.OnDrawGizmosSelected();

        }


        public static implicit operator WallPatchesHandler(WallBehaviour behaviour)
        {
            return behaviour.patchesHandler;
        }
        public static implicit operator Junctions(WallBehaviour behaviour)
        {
            return behaviour.Junctions;
        }
        public static implicit operator SoundReductionIndex(WallBehaviour behaviour)
        {
            return behaviour.ReductionIndex;
        }
        public static implicit operator WallGeometry(WallBehaviour behaviour)
        {
            return behaviour.Geometry;
        }

    }
}