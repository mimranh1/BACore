﻿
using log4net;
using UnityEngine;

namespace BA.BACore
{
    public abstract class JunctionGeometry : IElementGeometry
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(WallGeometry));
        public int id;
        public virtual Vector3 Dimensions => Vector3.zero;
        public virtual Vector3 Normal => Vector3.zero;
        public virtual Vector3 Position => Vector3.zero;
        public virtual Vector3[] SecondarySourcesPosition => null;

        public virtual float JunctionLength { get; }
        
        public virtual int JunctionType => -1;
        

    }

}