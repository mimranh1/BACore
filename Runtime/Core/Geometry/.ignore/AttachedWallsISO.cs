﻿using System;
using log4net;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class AttachedWallsISO : AttachedWalls
    {        
        private static readonly ILog Log = LogManager.GetLogger(typeof(AttachedWallsISO));


        [SerializeField] private float[] alphaKSitu;
        [SerializeField] private float[] alphaKLab;

        public AttachedWallsISO(int id, string name, Vector3 position, Vector3 normal) : base(id, name, position, normal)
        {
            alphaKSitu=new float[attachedWallDatas.Length];
            alphaKLab=new float[attachedWallDatas.Length];
            for (var i = 0; i < attachedWallDatas.Length; i++)
            {
                alphaKSitu[i] = GetAbsorptionCoefficientSitu(attachedWallDatas[i]);
                alphaKLab[i] = GetAbsorptionCoefficientLab(attachedWallDatas[i]);
            }
        }

        public float GetAlphaKSitu(SoundReductionIndexIso soundReductionIndexIso)
        {
            var id = FindWallInstanceIndex(soundReductionIndexIso.id);
            if (id >= alphaKSitu.Length)
                Debug.LogError(Name + ": Array alphaKSitu[" + id + "] Out of range");
            return alphaKSitu[id];
        }
        public float GetAlphaKLab(SoundReductionIndexIso soundReductionIndexIso)
        {
            var id = FindWallInstanceIndex(soundReductionIndexIso.id);
            if (id >=alphaKLab.Length)
                Debug.LogError(Name + ": Array alphaKLab["+id+"] Out of range");
            return alphaKLab[id];
        }


        /// <summary>
        ///     find Index of Wall by Name
        /// </summary>
        /// <param name="wall"></param>
        /// <returns>Index in _wallsGameObjects</returns>
        private int FindWallInstanceIndex(AttachedWallData wall)
        {
            for (var i = 0; i < NumberOfWalls; i++)
                if (wall.id == attachedWallDatas[i].id)
                    return i;
            //Log.Error("Wall " + name + " was not found!");
            return -1;
        }

        /// <summary>
        ///     find Index of Wall by Name
        /// </summary>
        /// <param name="index"></param>
        /// <returns>Index in _wallsGameObjects</returns>
        private int FindWallInstanceIndex(int index)
        {
            for (var i = 0; i < NumberOfWalls; i++)
                if (index == attachedWallDatas[i].id)
                    return i;
            //Log.Error("Wall " + name + " was not found!");
            return -1;
        }


        /// <summary>
        /// Return perpendicular Mass for wall
        /// </summary>
        /// <param name="wall">WallData</param>
        /// <returns>float perpendicular Mass</returns>
        public float GetPerpendicularMass(SoundReductionIndexIso wall)
        {
            var i = (FindWallInstanceIndex(wall.id) + 1) % NumberOfWalls;
            return attachedWallDatas[i].Mass;
        }

        /// <summary>
        /// Returns Absorption Coefficient in Situ
        /// </summary>
        /// <param name="wall">WallData</param>
        /// <returns>float alphaKSitu</returns>
        private float GetAbsorptionCoefficientSitu(AttachedWallData wall)
        {
            CalcKij();
            var alphaK = -1.0f;
            var i = FindWallInstanceIndex(wall);
            if (i == -1)
                Debug.LogWarning("The Wall: " + wall + " cannot be found in Junction " + Name);
            if (junctionType == JunctionTypes.L_Junction)
            {
                // L Junction
                alphaK = Mathf.Pow(10, kij[i, (i + 1) % 2].Tf[0] / -10.0f) *
                         Mathf.Sqrt(attachedWallDatas[(i + 1) % 2].CriticalFrequency / BaSettings.Instance.Fref);
            }
            else if (junctionType == JunctionTypes.T_Junction)
            {
                // T Junction
                if (i == 1)
                {
                    alphaK = Mathf.Pow(10, kij[i, 0].Tf[0] / -10.0f) *
                             Mathf.Sqrt(attachedWallDatas[0].CriticalFrequency / BaSettings.Instance.Fref)
                             + Mathf.Pow(10, kij[i, 2].Tf[0] / -10.0f) *
                             Mathf.Sqrt(attachedWallDatas[2].CriticalFrequency / BaSettings.Instance.Fref);
                }
                else
                {
                    var j = i == 0 ? 2 : 0;
                    alphaK = Mathf.Pow(10, kij[i, 1].Tf[0] / -10.0f) *
                             Mathf.Sqrt(attachedWallDatas[1].CriticalFrequency / BaSettings.Instance.Fref)
                             + Mathf.Pow(10, kij[i, j].Tf[0] / -10.0f)
                             * Mathf.Sqrt(attachedWallDatas[j].CriticalFrequency / BaSettings.Instance.Fref);
                }
            }
            else if (junctionType == JunctionTypes.X_Junction)
            {
                // Cross Junction
                alphaK = Mathf.Pow(10, kij[i, (i + 1) % 4].Tf[0] / -10.0f) *
                         Mathf.Sqrt(attachedWallDatas[(i + 1) % 4].CriticalFrequency / BaSettings.Instance.Fref)
                         + Mathf.Pow(10, kij[i, (i + 2) % 4].Tf[0] / -10.0f) *
                         Mathf.Sqrt(attachedWallDatas[(i + 2) % 4].CriticalFrequency / BaSettings.Instance.Fref)
                         + Mathf.Pow(10, kij[i, (i + 3) % 4].Tf[0] / -10.0f) *
                         Mathf.Sqrt(attachedWallDatas[(i + 3) % 4].CriticalFrequency / BaSettings.Instance.Fref);
            }

            DataLogger.Log("Junctions." + Name + ".alphaKSitu(:," + (i + 1) + ")", alphaK);
            return alphaK;
        }

        /// <summary>
        /// Returns Absorption Coefficient in Laboratory
        /// </summary>
        /// <param name="wall">WallData</param>
        /// <returns>float alphaKLab</returns>
        public float GetAbsorptionCoefficientLab(AttachedWallData wall)
        {
            //Debug.Log(" wall.CriticalFrequency " + wall.CriticalFrequency);
            var chi = Mathf.Sqrt(31.1f / wall.CriticalFrequency);
            var psy = 44.3f * (wall.CriticalFrequency / wall.Mass);
            var alpha = 1.0f / 3.0f *
                        Mathf.Pow(
                            2 * Mathf.Sqrt(chi * psy) * (1 + chi) * (1 + psy) /
                            (chi * Mathf.Pow(1 + psy, 2) + 2 * psy * (1 + Mathf.Pow(chi, 2))), 2);
            if (Mathf.Abs(alpha) < 0.0001f) Debug.LogError("AlphaK cannot be zero. psy: " + psy + " chi " + chi);
            var alphaKLab = alpha * (1 - 0.9999f * alpha);
            var i = FindWallInstanceIndex( wall);
            if (i == -1)
                Debug.LogError("Cannot find index of " + wall + " in " + Name);
            DataLogger.Log("Junctions." + Name + ".alphaKLab(:," + (i + 1) + ")", alphaKLab);
            return alphaKLab;
        }

        /// <summary>
        /// Calculate all possible Kijs
        /// </summary>
        public void CalcKij()
        {
            kij = new TransferPaths(NumberOfWalls, true);
            var mass0 = attachedWallDatas[0].Mass;
            var mass1 = attachedWallDatas[1].Mass;
            switch (junctionType)
            {
                case JunctionTypes.L_Junction:
                    // L-Junction
                    kij.numberOfWalls = 2;
                    kij[0, 1] = new TransferFunction(new[] {15 * Mathf.Abs(Mathf.Log10(mass1 / mass0)) - 3});
                    kij[0, 1].SetMinValue(-2);
                    kij[1, 0] = new TransferFunction(new[] {15 * Mathf.Abs(Mathf.Log10(mass1 / mass0)) - 3});
                    kij[1, 0].SetMinValue(-2);

                    break;
                case JunctionTypes.T_Junction:
                    // T-Junction
                    kij.numberOfWalls = 3;
                    kij[0, 1] = new TransferFunction(new[]
                        {5.7f + 5.7f * Mathf.Pow(Mathf.Log10(mass1 / mass0), 2)});
                    kij[2, 1] = new TransferFunction(new[]
                        {5.7f + 5.7f * Mathf.Pow(Mathf.Log10(mass1 / mass0), 2)});
                    kij[1, 0] = new TransferFunction(new[]
                        {5.7f + 5.7f * Mathf.Pow(Mathf.Log10(mass0 / mass1), 2)});
                    kij[1, 2] = new TransferFunction(new[]
                        {5.7f + 5.7f * Mathf.Pow(Mathf.Log10(mass1 / mass0), 2)});
                    kij[0, 2] = kij[0, 1] + 14.1f * Mathf.Log10(mass1 / mass0);
                    kij[2, 0] = kij[0, 2];

                    break;
                case JunctionTypes.X_Junction:
                    // Cross Junction
                    kij.numberOfWalls = 4;
                    kij[0, 1] = new TransferFunction(new[] {5.7f + 5.7f * Mathf.Pow(Mathf.Log10(mass1 / mass0), 2)});

                    kij[1, 2] = kij[0, 1];
                    kij[2, 3] = kij[0, 1];
                    kij[0, 3] = kij[0, 1];
                    kij[0, 2] = new TransferFunction(new[] {kij[0, 1].Tf[0] + 17.1f * Mathf.Log10(mass1 / mass0)});
                    kij[1, 3] = new TransferFunction(new[] {kij[0, 1].Tf[0] + 17.1f * Mathf.Log10(mass0 / mass1)});
                    break;
            }

            DataLogger.Log("Junctions." + Name + ".Kij", kij);
        }

        private string GetFcjs()
        {
            var fcjs = "";
            for (var i = 0; i < NumberOfWalls; i++) fcjs = fcjs + " " + attachedWallDatas[i].CriticalFrequency;
            return fcjs;
        }
        
          /// <summary>
        /// Calculate All Dvijs
        /// </summary>
        private TransferPaths CalcDvijs()
        {
            CalcKij();
            var alpha = new TransferFunction[NumberOfWalls];
            for (var k = 0; k < NumberOfWalls; k++)
            {
                alpha[k] = attachedWallDatas[k].WallBehaviour.ReductionIndex.EqualAbsorptionLength;
                if (ReferenceEquals(alpha[k], null) || alpha[k].NumFrequency == 0)
                {
                    Log.Error(attachedWallDatas[k].WallBehaviour.name + " not Processed jet!");
                    Debug.LogError(attachedWallDatas[k].WallBehaviour.name + " not Processed jet.", attachedWallDatas[k].WallBehaviour);
                }
            }

            DataLogger.Log("Junctions." + Name + ".alphaK", alpha);
            var coordinates = dvijs.GetCoordinatesForRij(NumberOfWalls);
            dvijs.numberOfWalls = NumberOfWalls;
            foreach (var coordinate in coordinates)
            {
                var i = coordinate[0];
                var j = coordinate[1];
                if (ReferenceEquals(alpha[i], null) || alpha[i].NumFrequency == 0)
                {
                    Log.Error(attachedWallDatas[i].WallBehaviour.name + " not Processed jet!");
                    Debug.LogError(attachedWallDatas[i].WallBehaviour.name + " not Processed jet.", attachedWallDatas[i].WallBehaviour);
                }

                if (ReferenceEquals(alpha[j], null) || alpha[j].NumFrequency == 0)
                {
                    Log.Error(attachedWallDatas[j].WallBehaviour.name + " not Processed jet!");
                    Debug.LogError(attachedWallDatas[j].WallBehaviour.name + " not Processed jet.", attachedWallDatas[j].WallBehaviour);
                }

                dvijs[i, j] = kij[i, j].Tf[0] - 10 * (junctionLength / (alpha[i] * alpha[j]).Sqrt()).Log(10);
            }

            DataLogger.Log("Junctions." + Name + ".Dvijs", dvijs);

            return dvijs;
        }

        /// <summary>
        /// This Function Calculates the normalized flanking level difference
        /// </summary>
        /// <param name="i">Path Wall start</param>
        /// <param name="j">Path Wall end</param>
        private TransferFunction GetNormFlankLevelDiff(int i, int j)
        {
            var reductionIndexI = attachedWallDatas[i].WallBehaviour.ReductionIndex.ReductionIndex;
            var reductionIndexJ = attachedWallDatas[j].WallBehaviour.ReductionIndex.ReductionIndex;

            return dvijs[i, j] + reductionIndexI / 2 + reductionIndexJ / 2 +
                   10 * Mathf.Log10(BaSettings.Instance.A0 / junctionLength);
        }

    }
}