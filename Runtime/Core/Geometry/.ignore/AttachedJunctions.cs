﻿// using System;
// using log4net;
// using UnityEngine;
// using Object = UnityEngine.Object;
//
// namespace BA.BACore
// {
//     /// <summary>
//     /// Saves the Attached Junctions of each Wall
//     /// </summary>
//     /// <remarks>
//     /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
//     ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
//     /// Version:        1.2  <br>
//     /// First release:  2017 <br>
//     /// Last revision:  2019 <br>
//     /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
//     /// </remarks>
//     [Serializable]
//     public class AttachedJunctions
//     {
//         private static readonly ILog Log = LogManager.GetLogger(typeof(AttachedJunctions));
//         public int id;
//         #region Private Members
//
//         /// <summary>
//         ///     Store all Junctions which are attached to the parent Wall
//         /// </summary>
//         [SerializeField] private JunctionBehaviour[] junctionBehaviours;
//
//         [SerializeField] private Transform transform;
//         private string _name;
//         public AttachedJunctions(int id, string name, Transform transform)
//         {
//             this.id = id;
//             _name = name;
//             this.transform = transform;
//             try
//             {
//                 GetJunctions();
//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine(e);
//                 Debug.LogError(name + ": Cannot find Attached Junctions.");
//                 throw;
//             }
//
//         }
//
//         #endregion
//
//         #region Public Methods
//
//         /// <summary>
//         ///     find all attached Junctions from Parent Wall (transform)
//         /// </summary>
//         /// <returns>normal vector of wall</returns>
//         public Vector3 GetJunctions()
//         {
//             if(transform==null)
//                 Debug.LogError(_name+": dont have transform.");
//             var ray = new Ray
//             {
//                 origin = transform.position
//             };
//
//             var directions = new Vector3[6]
//                 {Vector3.right, Vector3.left, Vector3.up, Vector3.down, Vector3.forward, Vector3.back};
//             var junctions = new GameObject[6] {null, null, null, null, null, null};
//             junctionBehaviours = new JunctionBehaviour[4] {null, null, null, null};
//
//             // Look for next Walls for every direction
//             for (var i = 0; i < 6; i++)
//             {
//                 RaycastHit hit;
//                 ray.direction = directions[i];
//                 if (!Physics.Raycast(ray, out hit, 10f, BaSettings.Instance.LayerMaskBoth))
//                     continue;
//
//                 if (hit.collider.gameObject.layer == BaSettings.Instance.LayerJunction)
//                     junctions[i] = hit.collider.gameObject;
//                 //else if (hit.collider.gameObject.transform == transform)
//                 //   if (Physics.Raycast(ray, out hit, Mathf.Infinity, Constants.LayerMaskJunction))
//                 //     junctions[i] = hit.collider.gameObject;
//
//                 //log.Info("next " + hit.collider.gameObject.name + " Dir " + i + " Dist " + hit.distance);
//             }
//
//             if (junctions[2] == null && junctions[3] == null)
//             {
//                 // Junction x-z-Plane
//                 junctionBehaviours[0] = junctions[4].GetComponent<JunctionBehaviour>();
//                 junctionBehaviours[1] = junctions[0].GetComponent<JunctionBehaviour>();
//                 junctionBehaviours[2] = junctions[5].GetComponent<JunctionBehaviour>();
//                 junctionBehaviours[3] = junctions[1].GetComponent<JunctionBehaviour>();
//
//                 return Vector3.up;
//             }
//
//             if (junctions[0] == null && junctions[1] == null)
//             {
//                 // Junction y-z-Plane
//                 junctionBehaviours[0] = junctions[4].GetComponent<JunctionBehaviour>();
//                 junctionBehaviours[1] = junctions[3].GetComponent<JunctionBehaviour>();
//                 junctionBehaviours[2] = junctions[5].GetComponent<JunctionBehaviour>();
//                 junctionBehaviours[3] = junctions[2].GetComponent<JunctionBehaviour>();
//
//                 return Vector3.right;
//             }
//
//             if (junctions[4] == null && junctions[5] == null)
//             {
//                 // Junction x-y-Plane
//                 junctionBehaviours[0] = junctions[1].GetComponent<JunctionBehaviour>();
//                 junctionBehaviours[1] = junctions[3].GetComponent<JunctionBehaviour>();
//                 junctionBehaviours[2] = junctions[0].GetComponent<JunctionBehaviour>();
//                 junctionBehaviours[3] = junctions[2].GetComponent<JunctionBehaviour>();
//
//                 return Vector3.forward;
//             }
//
//             Log.Warn("Cannot detect Junction Plane.");
//             return Vector3.one;
//         }
//
//         #endregion
//
//         #region Properties
//
//         /// <summary>
//         ///     shows attatched Junctions as array
//         /// </summary>
//         /// <param name="i">index of array</param>
//         /// <returns></returns>
//         public JunctionBehaviour this[int i]
//         {
//             get
//             {
//                 if (junctionBehaviours[i] == null)
//                     return null;
//                 return junctionBehaviours[i].gameObject.GetComponent<JunctionBehaviour>();
//             }
//         }
//
//         /// <summary>
//         ///     returns float[4] of all Junction Types
//         ///     -1 - no Wall
//         ///     1 - L-Junction
//         ///     2 - T-Junction
//         ///     3 - X-Junction
//         /// </summary>
//         public JunctionTypes[] JunctionTypes
//         {
//             get
//             {
//                 var junctionTypes = new JunctionTypes[4];
//                 for (var i = 0; i < 4; i++)
//                     if (junctionBehaviours[i] != null)
//                         junctionTypes[i] = junctionBehaviours[i].AttachedWalls.JunctionType;
//                     else
//                         junctionTypes[i] = BACore.JunctionTypes.Undefined;
//
//                 return junctionTypes;
//             }
//         }
//
//         /// <summary>
//         ///     returns string[4]-Array with all Junction Names
//         ///     if no Junction it will return ""
//         /// </summary>
//         public string[] JunctionNames
//         {
//             get
//             {
//                 var junctionNames = new string[4];
//                 for (var i = 0; i < 4; i++)
//                     if (junctionBehaviours[i] != null)
//                         junctionNames[i] = junctionBehaviours[i].name;
//                     else
//                         junctionNames[i] = "";
//
//                 return junctionNames;
//             }
//         }
//
//         /// <summary>
//         ///     returns int with the number of Junctions
//         /// </summary>
//         public int NumberOfJunctions => junctionBehaviours.Length;
//
//         /// <summary>
//         ///     returns JunctionProperties[4] of all attached Walls
//         /// </summary>
//         public JunctionBehaviour[] JunctionsProp => junctionBehaviours;
//
//         /// <summary>
//         ///     returns GameObject[4] of all attached Walls
//         /// </summary>
//         public GameObject[] JunctionsGameObjects
//         {
//             get
//             {
//                 var junctionsProp = junctionBehaviours;
//                 var length = new GameObject[junctionsProp.Length];
//                 for (var i = 0; i < junctionsProp.Length; i++) length[i] = junctionsProp[i].gameObject;
//
//                 return length;
//             }
//         }
//         /// <summary>
//         /// return an array of the length for each junctions  
//         /// </summary>
//         public float[] JunctionLengths
//         {
//             get
//             {
//                 var junctionsProp = JunctionsProp;
//                 var length = new float[junctionsProp.Length];
//                 for (var i = 0; i < junctionsProp.Length; i++) length[i] = junctionsProp[i].Geometry.JunctionLength;
//
//                 return length;
//             }
//         }
//
//         #endregion
//          
//         
//     }
// }