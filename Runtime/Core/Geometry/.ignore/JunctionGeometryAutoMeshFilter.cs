﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class JunctionGeometryAutoMeshFilter : JunctionGeometry
    {
        public override Vector3 Dimensions => dimensions;
        [SerializeField][ReadOnly]  private Vector3 dimensions;

        public override Vector3 Normal => normal;
        [SerializeField][ReadOnly]  private Vector3 normal;
        
        public override Vector3 Position => position;
        [SerializeField][ReadOnly] private Vector3 position;

        public override float JunctionLength => junctionLength;
        [SerializeField][ReadOnly] private float junctionLength;

        private readonly Transform _transform;
        private readonly MeshFilter _meshFilter;

        public JunctionGeometryAutoMeshFilter(int id, Transform transform, MeshFilter meshFilter)
        {
            this.id = id;
            this._transform = transform;
            _meshFilter = meshFilter;
            CalcAndSetNormalVector3();
            position = _transform.position;
        }


        private void CalcAndSetNormalVector3()
        {
            dimensions = _transform.rotation * _meshFilter.sharedMesh.bounds.size;
            var maxVec = Mathf.Max(dimensions.x, dimensions.y, dimensions.z);
            if (Math.Abs(maxVec - dimensions.x) < float.Epsilon)
            {
                normal = Vector3.right;
                junctionLength = dimensions.x;
            }
            else if (Math.Abs(maxVec - dimensions.y) < float.Epsilon)
            {
                normal = Vector3.up;
                junctionLength = dimensions.y;
            }
            else if (Math.Abs(maxVec - dimensions.z) < float.Epsilon)
            {
                normal = Vector3.forward;
                junctionLength = dimensions.z;
            }
            else
            {
                normal = Vector3.zero;
                Debug.LogError("Normal Vector for Junction " +  " cannot be found");
            }
        }
    }
}