﻿// using System;
// using log4net;
// using UnityEngine;
//
// namespace BA.BACore
// {
//     public class JunctionBehaviour : MonoBehaviour
//     {
//         [SerializeField] private WallSettings wallSettings;
//
//         public JunctionGeometry Geometry => _geometry ?? SetGeometry();
//         [SerializeReference] private JunctionGeometry _geometry;
//
//         public AttachedWalls AttachedWalls => _attachedWalls ?? SetAttachesWalls();
//         [SerializeReference] private AttachedWalls _attachedWalls;
//         public AttachedWallsISO AttachedWallsIso => (AttachedWallsISO)_attachedWalls;
//
//         private static readonly ILog Log = LogManager.GetLogger(typeof(JunctionBehaviour));
//
//         private void OnValidate()
//         {
//             
//             SetAttachesWalls();           
//             
//             SetGeometry();
//         }
//
//         private JunctionGeometry SetGeometry()
//         {
//             if (wallSettings == null)
//             {
//                 _geometry = null;
//                 return null;
//             }
//             switch (wallSettings.GeometryOption)
//             {
//                 case GeometryMethode.Manual:
//                     if (!(_geometry is JunctionGeometryManuel))
//                         _geometry = new JunctionGeometryManuel(GetInstanceID());
//                     break;
//                 case GeometryMethode.AutoTransform:
//                     if (!(_geometry is JunctionGeometryAutoTransform))
//                         _geometry = new JunctionGeometryAutoTransform(GetInstanceID(),transform);
//                     break;
//                 case GeometryMethode.AutoMeshFilter:
//                     if (!(_geometry is JunctionGeometryAutoMeshFilter))
//                         _geometry = new JunctionGeometryAutoMeshFilter(GetInstanceID(),transform, GetComponent<MeshFilter>());
//                     break;
//                 default:
//                     _geometry = null;
//                     throw new ArgumentOutOfRangeException();
//             }
//
//             return _geometry;
//         }
//
//         private AttachedWalls SetAttachesWalls()
//         {
//             if (wallSettings == null)
//             {
//                 _attachedWalls = null;
//                 return null;
//             }
//             switch (wallSettings.ReductionIndexOption)
//             {
//                 case ReductionIndexOption.Database:
//                     if (_attachedWalls == null || _attachedWalls is AttachedWallsISO)
//                     {
//                         _attachedWalls = new AttachedWalls(GetInstanceID(),name, this);
//                     }
//
//                     break;
//                 case ReductionIndexOption.ISO:
//                     if (!(_attachedWalls is AttachedWallsISO))
//                     {
//                         _attachedWalls = new AttachedWallsISO(GetInstanceID(),name, this);
//                     }
//
//                     break;
//                 case ReductionIndexOption.None:
//                     _attachedWalls = null;
//                     break;
//                 default:
//                     throw new ArgumentOutOfRangeException();
//             }
//
//             return _attachedWalls;
//         }
//
//
//         /// <summary>
//          /// This Function Calculates the reduction Index for all Partition and attached Walls
//          /// it uses CalcDvijs()
//          /// </summary>
//          /// <param name="partition">WallData</param>
//          public TransferPaths CalcReductionIndexPathsRij(SoundReductionIndexIso partition)
//          {
//              var rijs = new TransferPaths(AttachedWalls.JunctionType.GetHashCode() + 1, false);
//              var dvij = AttachedWalls.Dvijs;
//              var dvijCoords = dvij.GetCoordinatesForRij(AttachedWalls.NumberOfWalls);
//              foreach (var coordinate in dvijCoords)
//              {
//                  var i = coordinate[0];
//                  var j = coordinate[1];
//                  var redi = AttachedWalls[i].ReductionIndex.ReductionIndex;
//                  var redj = AttachedWalls[j].ReductionIndex.ReductionIndex;
//
//                  if (redi.NumFrequency == 0)
//                      Log.Warn(AttachedWalls[i].name + " Cannot find Reduction Index!");
//                  if (redj.NumFrequency == 0)
//                      Log.Warn(AttachedWalls[j].name + " Cannot find Reduction Index!");
//
//                  rijs[i, j] =
//                      dvij[i, j] +
//                      redi / 2 +
//                      redj / 2 +
//                      10 * Mathf.Log(partition.Geometry.Area /
//                                     Mathf.Sqrt(AttachedWalls[i].Geometry.Area * AttachedWalls[j].Geometry.Area));
//
//                  try
//                  {
//                      var b = rijs[i, j].NumFrequency;
//                  }
//                  catch
//                  {
//                      Log.Error("Error");
//                  }
//              }
//
//              DataLogger.Log("Junctions." + name + ".Rij", rijs);
//              return rijs;
//          }
//          
//     }
// }