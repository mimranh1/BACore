﻿using System;
using System.Linq;
using log4net;
using UnityEngine;
using Object = UnityEngine.Object;

namespace BA.BACore
{
    /// <summary>
    /// Saves the Attached Walls of each Junction and define the Junction Type
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    [Serializable]
    public class AttachedWalls
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(AttachedWalls));

        public int id;
        
        [Serializable]
        public class AttachedWallData
        {
            public WallBehaviour WallBehaviour;
            public float CriticalFrequency;
            public float Mass;
            public int id;

            public AttachedWallData(WallBehaviour wallBehaviour)
            {
                WallBehaviour = wallBehaviour;
                WallBehaviour.Awake();
                id = wallBehaviour.GetInstanceID();
                Mass = WallBehaviour.Mass;
                CriticalFrequency = WallBehaviour.CriticalFrequency;


            }
        }

        [SerializeReference][DisplayProperties] protected AttachedWallData[] attachedWallDatas;
        public JunctionTypes JunctionType => junctionType;
        [SerializeField][ReadOnly]  protected JunctionTypes junctionType;

        protected float junctionLength => Vector3.Dot(normal, attachedWallDatas[0].WallBehaviour.Geometry.Dimensions);
        
        protected TransferPaths kij;
        protected TransferPaths dvijs = null;
        protected TransferPaths rij;

        public TransferPaths Dvijs => dvijs ?? CalcDvijs();

        protected string Name;
        protected Vector3 position;
        protected Vector3 normal;

        public AttachedWalls(int id, string name, Vector3 position, Vector3 normal)
        {
            this.id = id;
            Name = name;
            this.position = position;
            this.normal = normal;
            try
            {
                junctionType = GetWalls(position, normal);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Debug.LogError(name + ": Cannot find Attached Walls.");
                throw;
            }
        }
        
        /// <summary>
        ///     shows attached Junctions as array
        /// </summary>
        /// <param name="i">index of array</param>
        /// <returns></returns>
        public WallBehaviour this[int i]
        {
            get
            {
                if (attachedWallDatas == null) return null;
                if (attachedWallDatas[i] == null)
                    return null;
                return attachedWallDatas[i].WallBehaviour;
            }
        }
        
        /// <summary>
        ///     Number of Walls
        /// </summary>
        public int NumberOfWalls
        {
            get
            {
                if (!_bInit)
                    return 0;
                var i = 0;
                foreach (var wall in attachedWallDatas) i++;
                return i;
            }
        }

        private bool _bInit = false;


        /// <summary>
        ///     reorganize Order of Walls
        ///     necessary if it isn't Cross Walls
        /// </summary>
        /// <param name="walls">Array of Walls.</param>
        /// <returns>Junction Type</returns>
        private JunctionTypes SetAttachedWalls(GameObject[] walls)
        {
            Log.Debug("Attached Walls configured: ");
            _bInit = true;
            var numberOfWalls = 0;
            var wallCounter = 0;

            // Find JunctionType
            foreach (var wall in walls)
                if (wall != null)
                    numberOfWalls++;

            // Sort Junctions
            var wallsGameObjects = new GameObject[numberOfWalls];
            switch (numberOfWalls)
            {
                case 2:
                    // L Junction
                    foreach (var wall in walls)
                        if (wall != null)
                        {
                            wallsGameObjects[wallCounter] = wall;
                            wallCounter++;
                        }

                    junctionType = JunctionTypes.L_Junction;
                    break;
                case 3:
                    // T-Junction
                    for (wallCounter = 0; wallCounter < 4; wallCounter++)
                        if (walls[wallCounter] == null)
                        {
                            wallsGameObjects[0] = walls[(wallCounter + 1) % 4];
                            wallsGameObjects[1] = walls[(wallCounter + 2) % 4];
                            wallsGameObjects[2] = walls[(wallCounter + 3) % 4];
                        }

                    junctionType = JunctionTypes.T_Junction;
                    break;
                case 4:
                    // X-Junction
                    wallsGameObjects = walls;
                    junctionType = JunctionTypes.X_Junction;
                    break;
                default:
                    // ERROR
                    Log.Warn("No Walls Found!");
                    junctionType = JunctionTypes.Undefined;
                    break;
            }

            attachedWallDatas = new AttachedWallData[wallsGameObjects.Length];
            for (var i = 0; i < wallsGameObjects.Length; i++)
            {
                attachedWallDatas[i]=new AttachedWallData(wallsGameObjects[i].GetComponent<WallBehaviour>());
                if (attachedWallDatas[i].WallBehaviour == null)
                    Debug.LogError("WallBehaviour need to be attached to " + wallsGameObjects[i].name,
                        wallsGameObjects[i]);
                
            }

            return junctionType;

        }


        #region Public Methods

        /// <summary>
        ///     Find all attached Walls to the Parent Object GameObject
        /// </summary>
        /// <param name="position"></param>
        /// <param name="normal">normal Vector of Wall</param>
        /// <returns>Type of Junction</returns>
        public JunctionTypes GetWalls(Vector3 position, Vector3 normal)
        {
            Log.Debug("Set Attached Walls for ");
            var directions = new Vector3[4];
            if (normal.x == 1)
                directions = new[] {Vector3.forward, Vector3.down, Vector3.back, Vector3.up};
            if (normal.z == 1)
                directions = new[] {Vector3.left, Vector3.down, Vector3.right, Vector3.up};
            if (normal.y == 1)
                directions = new[] {Vector3.forward, Vector3.right, Vector3.back, Vector3.left};

            var walls = new GameObject[] {null, null, null, null};

            // Look for next Walls for every direction
            for (var i = 0; i < 4; i++)
            {
                var ray = new Ray
                {
                    origin = position,
                    direction = directions[i]
                };

                if (!Physics.Raycast(ray, out var hit, 3f, BaSettings.Instance.LayerMaskWall))
                    continue;

                if (hit.collider.gameObject.layer == BaSettings.Instance.LayerWall)
                    walls[i] = hit.collider.gameObject;
            }

            var numNotNull = walls.Count(wall => wall != null);
            if (numNotNull == 0) Debug.LogError("Cannot detect Attached Walls in " + Name);

            return SetAttachedWalls(walls);
        }

        /// <summary>
        ///     find Index of Wall by Name
        /// </summary>
        /// <param name="gameObject"></param>
        /// <returns>Index in _wallsGameObjects</returns>
        public int FindWallInstanceIndex(GameObject gameObject)
        {
            for (var i = 0; i < NumberOfWalls; i++)
                if (gameObject.GetInstanceID() == attachedWallDatas[i].WallBehaviour.gameObject.GetInstanceID())
                    return i;
            //Log.Error("Wall " + name + " was not found!");
            return -1;
        }

        /// <summary>
        ///     find Index of Wall by Name
        /// </summary>
        /// <param name="wallBehaviour"></param>
        /// <returns>Index in _wallsGameObjects</returns>
        public int FindWallInstanceIndex(WallBehaviour wallBehaviour)
        {
            for (var i = 0; i < NumberOfWalls; i++)
                if (wallBehaviour.GetInstanceID() == attachedWallDatas[i].WallBehaviour.GetInstanceID())
                    return i;
            //Log.Error("Wall " + name + " was not found!");
            return -1;
        }

        private TransferPaths CalcDvijs()
        {
            return new TransferPaths(NumberOfWalls, false);
        }

        #endregion
    }
}