﻿using UnityEngine;

namespace BA.BACore
{
    [CreateAssetMenu(fileName = "JunctionSettings", menuName = "BACore/JunctionStteings", order = 0)]
    public class JunctionSettings : ScriptableObject
    {
        [SerializeField] private GeometryMethode geometryOption;

        public GeometryMethode GeometryOption => geometryOption;
    }
}