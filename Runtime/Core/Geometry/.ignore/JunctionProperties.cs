﻿// using log4net;
// using UnityEngine;
//
// namespace BA.BACore
// {
//     /// <summary>
//     /// Handle the Junction Geometry
//     /// </summary>
//     /// <remarks>
//     /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
//     ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
//     /// Version:        1.2  <br>
//     /// First release:  2017 <br>
//     /// Last revision:  2019 <br>
//     /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
//     /// </remarks>
//     public class JunctionProperties : MonoBehaviour
//     {
//         /// <summary>
//         /// unique junction Id for each junction
//         /// </summary>
//         public int junctionId;
//
//         private static int junctionIdCounter = 1;
//
//         private static readonly ILog Log = LogManager.GetLogger(typeof(JunctionProperties));
//         private string logName;
//
//         [SerializeField] private AttachedWalls attachedWalls = new AttachedWalls();
//         [SerializeField] private int junctionType;
//         private TransferPaths kij;
//         private TransferPaths rijs;
//         private TransferPaths dvijs;
//         [SerializeField] private Vector3 normalVector3 = Vector3.zero;
//         private int counterRijsdR = 0;
//         private BaSettings _baSettings;
//
//         /// <summary>
//         /// Return the global position of the Wall
//         /// </summary>
//         public Vector3 Position => transform.position;
//
//         #region MONOBEHAVIOR
//
//         /// <summary>
//         /// Unity Event Function Called at Init
//         /// Init the Junction
//         /// </summary>
//         private void Awake()
//         {
//             _baSettings = FindObjectOfType<BaSettings>();
//             logName = JunctionName.Replace(" ", "");
//             junctionId = junctionIdCounter++;
//             counterRijsdR = 0;
//             FinishAttachedWalls();
//             LogDataWall();
//             Log.Debug("Awake Wall junctionId " + junctionId + " with Name " + name);
//         }
//
//         #endregion
//
//
//         #region Properties
//
//         /// <summary>
//         /// return the attachedWalls
//         /// </summary>
//         public AttachedWalls AttachedWalls => attachedWalls;
//
//
//         /// <summary>
//         /// returns Normal Vector3 of Junction
//         /// </summary>
//         public Vector3 Normal => normalVector3;
//
//         /// <summary>
//         /// returns Length of Junction
//         /// </summary>
//         public float JunctionLength
//         {
//             get
//             {
//                 float length = 1000;
//                 if (attachedWalls.NumberOfWalls == 0 || attachedWalls[0] == null)
//                     FinishAttachedWalls();
//
//                 for (var i = 0; i < attachedWalls.NumberOfWalls; i++)
//                     length = Mathf.Min(length,
//                         Mathf.Abs(Vector3.Dot(attachedWalls.WallsProp[i].BoundVector, Normal)));
//
//                 if (Mathf.Abs(length) < 0.00001f)
//                     Log.Error("Junction Length of " + JunctionName + " cannot be 0!");
//                 else if (Mathf.Abs(length - 0.4f) < 0.00001f)
//                     Log.Error("Junction Length of " + JunctionName + " cannot be 0.4!");
//
//                 return length;
//             }
//         }
//
//         /// <summary>
//         /// returns Type of Junction int
//         /// </summary>
//         public int JunctionType => junctionType;
//
//         /// <summary>
//         /// returns Type of Junction int
//         /// </summary>
//         public string JunctionTypeString
//         {
//             get
//             {
//                 switch (junctionType)
//                 {
//                     case 1:
//                         return "L-Junction";
//                     case 2:
//                         return "T-Junction";
//                     case 3:
//                         return "X-Junction";
//                     default:
//                         Log.Error("Invalid Junction Type index!");
//                         return "";
//                 }
//             }
//         }
//
//         /// <summary>
//         /// Returns name of Junction
//         /// </summary>
//         public string JunctionName => gameObject.name;
//
//         /// <summary>
//         /// Returns all Names of Walls in string[]
//         /// </summary>
//         public string[] WallNames
//         {
//             get
//             {
//                 var wallNames = new string[attachedWalls.NumberOfWalls];
//                 for (var i = 0; i < attachedWalls.NumberOfWalls; i++) wallNames[i] = attachedWalls.WallsProp[i].Name;
//                 return wallNames;
//             }
//         }
//
//         /// <summary>
//         /// Returns Amount of Walls
//         /// </summary>
//         public int NumOfWalls => junctionType + 1;
//
//         #endregion
//
//         #region Public Methods
//
//         /// <summary>
//         /// get all attached Walls via Ray Tracing
//         /// </summary>
//         public void FinishAttachedWalls()
//         {
//             logName = JunctionName.Replace(" ", "");
//             CalcAndSetNormalVector3();
//             junctionType = attachedWalls.GetWalls(this, normalVector3);
//             Log.Debug("Attached Junctions calculated for " + name);
//             if (junctionType == -1)
//             {
//                 Log.Warn(name + " cannot find attached Walls");
//                 Debug.LogWarning(name + " cannot find attached Walls", this);
//             }
//
//             dvijs = new TransferPaths(junctionType + 1, false);
//             rijs = new TransferPaths(junctionType + 1, false);
//             kij = new TransferPaths(NumOfWalls, true);
//             DataLogger.Log("Junctions." + logName + ".Name", JunctionName);
//             DataLogger.Log("Junctions." + logName + ".JunctionType", junctionType);
//         }
//
//         /// <summary>
//         /// Return perpendicular Mass for wall
//         /// </summary>
//         /// <param name="wall">WallData</param>
//         /// <returns>float perpendicular Mass</returns>
//         public float GetPerpendicularMass(WallProperties wall)
//         {
//             var i = (attachedWalls.FindWallIndex(wall.Name) + 1) % attachedWalls.NumberOfWalls;
//             return attachedWalls.WallsProp[i].Mass;
//         }
//
//         /// <summary>
//         /// Returns Absorption Coefficient in Situ
//         /// </summary>
//         /// <param name="wall">WallData</param>
//         /// <returns>float alphaKSitu</returns>
//         public float GetAbsorptionCoefficientSitu(SoundReductionIndexISO wall)
//         {
//             CalcKij();
//             var alphaK = -1.0f;
//             var i = attachedWalls.FindWallIndex(wall.isPatch ? wall.parent.Name : wall.Name);
//             if (i == -1)
//                 Log.Warn("The Wall: " + wall.Name + " cannot be found in Junction " + JunctionName);
//             if (JunctionType == 1)
//             {
//                 // L Junction
//                 alphaK = Mathf.Pow(10, kij[i, (i + 1) % 2].Tf[0] / -10.0f) *
//                          Mathf.Sqrt(attachedWalls.WallsProp[(i + 1) % 2].CriticalFrequency / _baSettings.Fref);
//             }
//             else if (junctionType == 2)
//             {
//                 // T Junction
//                 if (i == 1)
//                 {
//                     alphaK = Mathf.Pow(10, kij[i, 0].Tf[0] / -10.0f) *
//                              Mathf.Sqrt(attachedWalls.WallsProp[0].CriticalFrequency / _baSettings.Fref)
//                              + Mathf.Pow(10, kij[i, 2].Tf[0] / -10.0f) *
//                              Mathf.Sqrt(attachedWalls.WallsProp[2].CriticalFrequency / _baSettings.Fref);
//                 }
//                 else
//                 {
//                     var j = i == 0 ? 2 : 0;
//                     alphaK = Mathf.Pow(10, kij[i, 1].Tf[0] / -10.0f) *
//                              Mathf.Sqrt(attachedWalls.WallsProp[1].CriticalFrequency / _baSettings.Fref)
//                              + Mathf.Pow(10, kij[i, j].Tf[0] / -10.0f)
//                              * Mathf.Sqrt(attachedWalls.WallsProp[j].CriticalFrequency / _baSettings.Fref);
//                 }
//             }
//             else if (JunctionType == 3)
//             {
//                 // Cross Junction
//                 alphaK = Mathf.Pow(10, kij[i, (i + 1) % 4].Tf[0] / -10.0f) *
//                          Mathf.Sqrt(attachedWalls.WallsProp[(i + 1) % 4].CriticalFrequency / _baSettings.Fref)
//                          + Mathf.Pow(10, kij[i, (i + 2) % 4].Tf[0] / -10.0f) *
//                          Mathf.Sqrt(attachedWalls.WallsProp[(i + 2) % 4].CriticalFrequency / _baSettings.Fref)
//                          + Mathf.Pow(10, kij[i, (i + 3) % 4].Tf[0] / -10.0f) *
//                          Mathf.Sqrt(attachedWalls.WallsProp[(i + 3) % 4].CriticalFrequency / _baSettings.Fref);
//             }
//
//             DataLogger.Log("Junctions." + logName + ".alphaKSitu(:," + (i + 1) + ")", alphaK);
//             return alphaK;
//         }
//
//         /// <summary>
//         /// Returns Absorption Coefficient in Laboratory
//         /// </summary>
//         /// <param name="wall">WallData</param>
//         /// <returns>float alphaKLab</returns>
//         public float GetAbsorptionCoefficientLab(WallProperties wall)
//         {
//             //Debug.Log(" wall.CriticalFrequency " + wall.CriticalFrequency);
//             var chi = Mathf.Sqrt(31.1f / wall.CriticalFrequency);
//             var psy = 44.3f * (wall.CriticalFrequency / wall.Mass);
//             var alpha = 1.0f / 3.0f *
//                         Mathf.Pow(
//                             2 * Mathf.Sqrt(chi * psy) * (1 + chi) * (1 + psy) /
//                             (chi * Mathf.Pow(1 + psy, 2) + 2 * psy * (1 + Mathf.Pow(chi, 2))), 2);
//             if (Mathf.Abs(alpha) < 0.0001f) Log.Error("AlphaK cannot be zero. psy: " + psy + " chi " + chi);
//             var alphaKLab = alpha * (1 - 0.9999f * alpha);
//             var i = attachedWalls.FindWallIndex(wall.isPatch ? wall.parent.name : wall.name);
//             if (i == -1)
//                 Log.Error("Cannot find index of " + wall.Name + " in " + name);
//             DataLogger.Log("Junctions." + logName + ".alphaKLab(:," + (i + 1) + ")", alphaKLab);
//             return alphaKLab;
//         }
//
//         /// <summary>
//         /// Calculate all possible Kijs
//         /// </summary>
//         public void CalcKij()
//         {
//             FinishAttachedWalls();
//
//             var mass0 = attachedWalls.WallsProp[0].Mass;
//             var mass1 = attachedWalls.WallsProp[1].Mass;
//             switch (JunctionType)
//             {
//                 case 1:
//                     // L-Junction
//                     kij.numberOfWalls = 2;
//                     kij[0, 1] = new TransferFunction(new[] {15 * Mathf.Abs(Mathf.Log10(mass1 / mass0)) - 3});
//                     kij[0, 1].SetMinValue(-2);
//                     kij[1, 0] = new TransferFunction(new[] {15 * Mathf.Abs(Mathf.Log10(mass1 / mass0)) - 3});
//                     kij[1, 0].SetMinValue(-2);
//
//                     break;
//                 case 2:
//                     // T-Junction
//                     kij.numberOfWalls = 3;
//                     kij[0, 1] = new TransferFunction(new[]
//                         {5.7f + 5.7f * Mathf.Pow(Mathf.Log10(mass1 / mass0), 2)});
//                     kij[2, 1] = new TransferFunction(new[]
//                         {5.7f + 5.7f * Mathf.Pow(Mathf.Log10(mass1 / mass0), 2)});
//                     kij[1, 0] = new TransferFunction(new[]
//                         {5.7f + 5.7f * Mathf.Pow(Mathf.Log10(mass0 / mass1), 2)});
//                     kij[1, 2] = new TransferFunction(new[]
//                         {5.7f + 5.7f * Mathf.Pow(Mathf.Log10(mass1 / mass0), 2)});
//                     kij[0, 2] = kij[0, 1] + 14.1f * Mathf.Log10(mass1 / mass0);
//                     kij[2, 0] = kij[0, 2];
//
//                     break;
//                 case 3:
//                     // Cross Junction
//                     kij.numberOfWalls = 4;
//                     kij[0, 1] = new TransferFunction(new[] {5.7f + 5.7f * Mathf.Pow(Mathf.Log10(mass1 / mass0), 2)});
//
//                     kij[1, 2] = kij[0, 1];
//                     kij[2, 3] = kij[0, 1];
//                     kij[0, 3] = kij[0, 1];
//                     kij[0, 2] = new TransferFunction(new[] {kij[0, 1].Tf[0] + 17.1f * Mathf.Log10(mass1 / mass0)});
//                     kij[1, 3] = new TransferFunction(new[] {kij[0, 1].Tf[0] + 17.1f * Mathf.Log10(mass0 / mass1)});
//                     break;
//             }
//
//             DataLogger.Log("Junctions." + logName + ".Kij", kij);
//         }
//
//         private string GetFcjs()
//         {
//             var fcjs = "";
//             for (var i = 0; i < NumOfWalls; i++) fcjs = fcjs + " " + attachedWalls.WallsProp[i].CriticalFrequency;
//             return fcjs;
//         }
//
//         /// <summary>
//         /// write Results of calculation to File
//         /// </summary>
//         public void WriteDataFile()
//         {
//             //var time = DateTime.Now.ToString("yyyyMMdd-HHmmss");
//             //string path = @"Log\" + time + "-" + Name + ".txt";
//             var path = @"Log\" + JunctionName + ".txt";
//
//             DataLogger.Log("Junctions." + logName + ".Fcj", GetFcjs());
//             DataLogger.Log("Junctions." + logName + ".WallNames", WallNames);
//             DataLogger.Log("Junctions." + logName + ".JunLen", JunctionLength);
//             DataLogger.Log("Junctions." + logName + ".JunPosition", Position);
//             DataLogger.Log("Junctions." + logName + ".JunLocalPosition", transform.localPosition);
//
//             for (var i = 0; i < AttachedWalls.NumberOfWalls; i++)
//             {
//                 DataLogger.Log("Junctions." + logName + ".Masses(:," + (i + 1) + ")", AttachedWalls[i].Mass);
//                 DataLogger.Log("Junctions." + logName + ".WallsLength(:," + (i + 1) + ")", AttachedWalls[i].Length);
//                 DataLogger.Log("Junctions." + logName + ".WallsWidth(:," + (i + 1) + ")", AttachedWalls[i].Width);
//                 DataLogger.Log("Junctions." + logName + ".WallLocalPosition(:," + (i + 1) + ")",
//                     AttachedWalls[i].gameObject.transform.localPosition);
//                 DataLogger.Log("Junctions." + logName + ".WallPosition(:," + (i + 1) + ")",
//                     AttachedWalls[i].gameObject.transform.position);
//                 DataLogger.Log("Junctions." + logName + ".WallDimension(:," + (i + 1) + ")", AttachedWalls[i].BoundVector);
//             }
//         }
//
//
//         /// <summary>
//         /// Helper Function: Returns Index of Wall by name
//         /// </summary>
//         /// <param name="wantedName">string</param>
//         /// <returns>int index</returns>
//         public int GetWallId(string wantedName)
//         {
//             if (attachedWalls.NumberOfWalls == 0)
//                 attachedWalls.GetWalls(this, Normal);
//             return attachedWalls.FindWallIndex(wantedName);
//         }
//
//
//         private Vector3 CalcAndSetNormalVector3()
//         {
//             var boundVector = transform.rotation * transform.gameObject.GetComponent<MeshFilter>().sharedMesh.bounds.size;
//             var maxVec = Mathf.Max(boundVector.x, boundVector.y, boundVector.z);
//             if (maxVec == boundVector.x)
//             {
//                 normalVector3 = Vector3.right;
//             }
//             else if (maxVec == boundVector.y)
//             {
//                 normalVector3 = Vector3.up;
//             }
//             else if (maxVec == boundVector.z)
//             {
//                 normalVector3 = Vector3.forward;
//             }
//             else
//             {
//                 normalVector3 = Vector3.zero;
//                 Log.Error("Normal Vector for Junction " + name + " cannot be found");
//             }
//
//             return normalVector3;
//         }
//
//
//         /// <summary>
//         /// Returns a TransferFunction for SecondarySource and its Partition
//         /// </summary>
//         /// <param name="partitionId">id from partition</param>
//         /// <param name="secondarySourceId">id for SecondarySource</param>
//         /// <param name="directPath">if is direct paths or flanking path</param>
//         /// <param name="directionWallReceiver">is is direct paths or flanking path</param>
//         /// <param name="directionPartitionReceiver">is is direct paths or flanking path</param>
//         /// <param name="writeLogFile">is is direct paths or flanking path</param>
//         /// <returns>TransferFunction for Secondary Source</returns>
//         public TransferFunction GetSecondarySource(int partitionId, int secondarySourceId, bool directPath,
//             int directionWallReceiver, int directionPartitionReceiver, bool writeLogFile = false)
//         {
//             var rij = CalcReductionIndexPathsRij(attachedWalls.WallsProp[partitionId]);
//             var sourceWallId = -1;
//             //Rijs.DebugLog();
//             //Debug.Log("calculatedRijs " + calculatedRijs + " junctionType " + JunctionType);
//             // For L-Junction SourceWallId has to be -1
//             switch (JunctionType)
//             {
//                 case 1:
//                     // sourceWallId = partitionId;
//                     break;
//                 case 2:
//                     // T-Junction
//                     if (partitionId > 2 || secondarySourceId > 2)
//                         Log.Error("PartitionId (" + partitionId + ") or SecondarySourceId (" + secondarySourceId +
//                                   ") bigger than 2!");
//                     if (partitionId == 1)
//                         switch (secondarySourceId)
//                         {
//                             case 0:
//                                 sourceWallId = 2;
//                                 break;
//                             case 2:
//                                 sourceWallId = 0;
//                                 break;
//                             default:
//                                 Log.Error("Cannot find source Wall Id!");
//                                 break;
//                         }
//
//                     if (partitionId == 0)
//                     {
//                         secondarySourceId = 1;
//                         sourceWallId = 2;
//                     }
//
//                     if (partitionId == 2)
//                     {
//                         secondarySourceId = 1;
//                         sourceWallId = 0;
//                     }
//
//                     break;
//                 case 3:
//                     // X-Junction
//                     if (partitionId > 3 || secondarySourceId > 3)
//                         Log.Error("PartitionId (" + partitionId + ") or SecoundarySourceId (" + secondarySourceId +
//                                   ") bigger than 3!");
//
//                     sourceWallId = (secondarySourceId + 2) % 4;
//                     break;
//             }
//
//             var secondarySource = new TransferFunction();
//
//             if (directPath || writeLogFile)
//             {
//                 if (sourceWallId != -1)
//                 {
//                     TransferFunction rijTemp;
//                     if (sourceWallId == partitionId)
//                         rijTemp = attachedWalls.WallsProp[sourceWallId].ReductionIndex;
//                     else
//                         rijTemp = rij[sourceWallId, partitionId];
//
//                     var rijDeltaR = rijTemp +
//                                     attachedWalls.WallsProp[sourceWallId].DeltaRForAdditionalLayer[directionWallReceiver] +
//                                     attachedWalls.WallsProp[partitionId]
//                                         .DeltaRForAdditionalLayer[(directionPartitionReceiver + 1) % 2];
//                     secondarySource = 10 ^ (rijDeltaR / -10);
//
//                     DataLogger.Log("Junctions." + logName + ".RijsdR{" + (sourceWallId + 1) + "," + (partitionId + 1) + "}",
//                         rijDeltaR);
//                     DataLogger.Log("Junctions." + logName + ".DirectSS", secondarySource);
//                 }
//
//                 // if Partition = SS return TF from SourceWallId and PartitionId
//                 if (DataLogger.DebugData)
//                 {
//                     var rijDelatRDirect = attachedWalls.WallsProp[partitionId].ReductionIndex +
//                                           attachedWalls.WallsProp[partitionId]
//                                               .DeltaRForAdditionalLayer[directionPartitionReceiver] +
//                                           attachedWalls.WallsProp[partitionId]
//                                               .DeltaRForAdditionalLayer[(directionPartitionReceiver + 1) % 2];
//
//                     if (sourceWallId != -1)
//                         DataLogger.Log(
//                             "Junctions." + logName + ".Rijs{" + (sourceWallId + 1) + "," + (partitionId + 1) + "}",
//                             rij[sourceWallId, partitionId]);
//
//                     DataLogger.Log("Junctions." + logName + ".RijsdR{" + (partitionId + 1) + "," + (partitionId + 1) + "}",
//                         rijDelatRDirect);
//                     counterRijsdR++;
//
//                     DataLogger.Log("Junctions." + logName + ".PathRijd(:," + counterRijsdR + ")",
//                         sourceWallId + 1 + " " + (partitionId + 1));
//                 }
//             }
//
//             if (!directPath || writeLogFile)
//             {
//                 var rPartitionSSourceDeltaR = rij[partitionId, secondarySourceId] +
//                                               attachedWalls.WallsProp[secondarySourceId]
//                                                   .DeltaRForAdditionalLayer[directionWallReceiver] +
//                                               attachedWalls.WallsProp[partitionId]
//                                                   .DeltaRForAdditionalLayer[(directionPartitionReceiver + 1) % 2];
//
//
//                 if (DataLogger.DebugData)
//                 {
//                     counterRijsdR++;
//                     DataLogger.Log(
//                         "Junctions." + logName + ".Rijs{" + (partitionId + 1) + "," + (secondarySourceId + 1) + "}",
//                         rij[partitionId, secondarySourceId]);
//                     DataLogger.Log(
//                         "Junctions." + logName + ".RijsdR{" + (partitionId + 1) + "," + (secondarySourceId + 1) + "}",
//                         rPartitionSSourceDeltaR);
//                     DataLogger.Log("Junctions." + logName + ".PathRijd(:," + counterRijsdR + ")",
//                         partitionId + 1 + " " + (secondarySourceId + 1));
//                 }
//
//                 if (sourceWallId == -1)
//                 {
//                     secondarySource = 10 ^ (rPartitionSSourceDeltaR / -10);
//                 }
//                 else
//                 {
//                     var rSourceWallSSourceDelatR = rij[sourceWallId, secondarySourceId] +
//                                                    attachedWalls.WallsProp[sourceWallId]
//                                                        .DeltaRForAdditionalLayer[(directionPartitionReceiver + 1) % 2] +
//                                                    attachedWalls.WallsProp[partitionId]
//                                                        .DeltaRForAdditionalLayer[(directionPartitionReceiver + 1) % 2];
//                     secondarySource = (10 ^ (rPartitionSSourceDeltaR / -10)) + (10 ^ (rSourceWallSSourceDelatR / -10));
//                     if (DataLogger.DebugData)
//                     {
//                         counterRijsdR++;
//
//                         DataLogger.Log(
//                             "Junctions." + logName + ".Rijs{" + (sourceWallId + 1) + "," + (secondarySourceId + 1) + "}",
//                             rij[sourceWallId, secondarySourceId]);
//                         DataLogger.Log(
//                             "Junctions." + logName + ".RijsdR{" + (sourceWallId + 1) + "," + (secondarySourceId + 1) + "}",
//                             rSourceWallSSourceDelatR);
//                         DataLogger.Log("Junctions." + logName + ".PathRijd(:," + counterRijsdR + ")",
//                             sourceWallId + 1 + " " + (secondarySourceId + 1));
//                     }
//                 }
//             }
//
//             if (DataLogger.DebugData)
//             {
//                 DataLogger.Log("Junctions." + logName + ".partitionId", partitionId + 1);
//                 DataLogger.Log("Junctions." + logName + ".sourceWallId", sourceWallId + 1);
//                 DataLogger.Log("Junctions." + logName + ".receiverId", secondarySourceId + 1);
//             }
//
//             return secondarySource;
//         }
//
//         private void LogDataWall()
//         {
//             if (DataLogger.DebugData)
//                 for (var i = 0; i < AttachedWalls.NumberOfWalls; i++)
//                 {
//                     DataLogger.Log("Junctions." + logName + ".Length(:," + (i + 1) + ")", AttachedWalls[i].Length);
//                     DataLogger.Log("Junctions." + logName + ".Width(:," + (i + 1) + ")", AttachedWalls[i].Width);
//                     DataLogger.Log("Junctions." + logName + ".Thickness(:," + (i + 1) + ")", AttachedWalls[i].Thickness);
//                     DataLogger.Log("Junctions." + logName + ".fc(:," + (i + 1) + ")", AttachedWalls[i].CriticalFrequency);
//                     DataLogger.Log("Junctions." + logName + ".quasiLongPhaseVelocity(:," + (i + 1) + ")",
//                         AttachedWalls[i].QuasiLongPhaseVelocity);
//                     DataLogger.Log("Junctions." + logName + ".ReductionIndex(:," + (i + 1) + ")",
//                         AttachedWalls[i].ReductionIndex);
//                 }
//         }
//
//
//         /// <summary>
//         /// This Function Calculates the reduction Index for all Partition and attached Walls
//         /// it uses CalcDvijs()
//         /// </summary>
//         /// <param name="partition">WallData</param>
//         public TransferPaths CalcReductionIndexPathsRij(WallProperties partition)
//         {
//             FinishAttachedWalls();
//             var rijs = new TransferPaths(junctionType + 1, false);
//             var dvij = CalcDvijs();
//             var dvijCoords = dvij.GetCoordinatesForRij(NumOfWalls);
//             foreach (var coordinate in dvijCoords)
//             {
//                 var i = coordinate[0];
//                 var j = coordinate[1];
//                 var redi = attachedWalls.WallsProp[i].ReductionIndex;
//                 var redj = attachedWalls.WallsProp[j].ReductionIndex;
//
//                 if (redi.NumFrequency == 0)
//                     Log.Warn(attachedWalls.WallsProp[i].Name + " Cannot find Reduction Index!");
//                 if (redj.NumFrequency == 0)
//                     Log.Warn(attachedWalls.WallsProp[j].Name + " Cannot find Reduction Index!");
//
//                 rijs[i, j] =
//                     dvij[i, j] +
//                     redi / 2 +
//                     redj / 2 +
//                     10 * Mathf.Log(partition.Area /
//                                    Mathf.Sqrt(attachedWalls.WallsProp[i].Area * attachedWalls.WallsProp[j].Area));
//
//                 try
//                 {
//                     var b = rijs[i, j].NumFrequency;
//                 }
//                 catch
//                 {
//                     Log.Error("Error");
//                 }
//             }
//
//             DataLogger.Log("Junctions." + logName + ".Rij", rijs);
//             return rijs;
//         }
//
//         #endregion
//
//         #region Private Methods
//
//         /// <summary>
//         /// Calculate All Dvijs
//         /// </summary>
//         private TransferPaths CalcDvijs()
//         {
//             CalcKij();
//             var alpha = new TransferFunction[attachedWalls.NumberOfWalls];
//             for (var k = 0; k < attachedWalls.NumberOfWalls; k++)
//             {
//                 attachedWalls.WallsProp[k].CalcReductionIndexPatches();
//                 alpha[k] = attachedWalls.WallsProp[k].EquivalentAbsorptionCoefficient;
//                 if (ReferenceEquals(alpha[k], null) || alpha[k].NumFrequency == 0)
//                 {
//                     Log.Error(attachedWalls.WallsProp[k].Name + " not Processed jet!");
//                     Debug.LogError(attachedWalls.WallsProp[k].Name + " not Processed jet.", attachedWalls.WallsProp[k]);
//                 }
//             }
//
//             DataLogger.Log("Junctions." + logName + ".alphaK", alpha);
//             var coordinates = dvijs.GetCoordinatesForRij(NumOfWalls);
//             dvijs.numberOfWalls = NumOfWalls;
//             foreach (var coordinate in coordinates)
//             {
//                 var i = coordinate[0];
//                 var j = coordinate[1];
//                 if (ReferenceEquals(alpha[i], null) || alpha[i].NumFrequency == 0)
//                 {
//                     Log.Error(attachedWalls.WallsProp[i].Name + " not Processed jet!");
//                     Debug.LogError(attachedWalls.WallsProp[i].Name + " not Processed jet.", attachedWalls.WallsProp[i]);
//                 }
//
//                 if (ReferenceEquals(alpha[j], null) || alpha[j].NumFrequency == 0)
//                 {
//                     Log.Error(attachedWalls.WallsProp[j].Name + " not Processed jet!");
//                     Debug.LogError(attachedWalls.WallsProp[j].Name + " not Processed jet.", attachedWalls.WallsProp[j]);
//                 }
//
//                 dvijs[i, j] = kij[i, j].Tf[0] - 10 * (JunctionLength / (alpha[i] * alpha[j]).Sqrt()).Log(10);
//             }
//
//             DataLogger.Log("Junctions." + logName + ".Dvijs", dvijs);
//
//             return dvijs;
//         }
//
//         /// <summary>
//         /// This Function Calculates the normalized flanking level difference
//         /// </summary>
//         /// <param name="i">Path Wall start</param>
//         /// <param name="j">Path Wall end</param>
//         private TransferFunction GetNormFlankLevelDiff(int i, int j)
//         {
//             var reductionIndexI = attachedWalls.WallsProp[i].ReductionIndex;
//             var reductionIndexJ = attachedWalls.WallsProp[j].ReductionIndex;
//
//             return dvijs[i, j] + reductionIndexI / 2 + reductionIndexJ / 2 +
//                    10 * Mathf.Log10(_baSettings.A0 / JunctionLength);
//         }
//
//         #endregion
//     }
// }