﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class JunctionGeometryManuel : JunctionGeometry
    {
        public override Vector3 Dimensions => dimensions;
        [SerializeField]  private Vector3 dimensions;

        public override Vector3 Normal => normal;
        [SerializeField] private Vector3 normal;
        
        public override Vector3 Position => position;
        [SerializeField] private Vector3 position;

        public override float JunctionLength => junctionLength;
        [SerializeField] private float junctionLength;

        public JunctionGeometryManuel(int id)
        {
            this.id = id;

        }
    }
}