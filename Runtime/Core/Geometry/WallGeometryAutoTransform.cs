﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]   
    public class WallGeometryAutoTransform : WallGeometryAuto
    {
        public override WallGeometryType Type => WallGeometryType.AutoTransform;
        public override Vector3 Dimensions => dimensions;
        [SerializeField][ReadOnly]  private Vector3 dimensions;

        public override Vector3 Normal => normal;
        [SerializeField][ReadOnly]  private Vector3 normal;
        
        public override Vector3 Position => position;
        [SerializeField][ReadOnly] private Vector3 position;
        
        public override void OnValidate()
        {
            
        }

        public override void Init(GameObject gameObject)
        {
            id = gameObject.GetInstanceID();
            transform = gameObject.transform;
            SetDimensions();
            normal = GetNormalVector(dimensions,transform.rotation);
        }

        private Transform transform;
        
        private void SetDimensions()
        {
            var transform1 = transform;
            var localScale = transform1.localScale;
            dimensions = transform1.rotation * localScale;
            
            //dimensions = RoundDimensions(dimensions);

            position = transform1.position;
        }

    }
}