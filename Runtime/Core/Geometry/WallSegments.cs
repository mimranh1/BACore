﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public abstract class WallSegments : ExtendedScriptableObject<WallSegments, WallSegmentType>
    {
        public abstract List<GeometrySs> SecondarySources { get; }


        public List<Vector3> Positions =>
            SecondarySources.Select(secondarySource => secondarySource.position).ToList();
        public List<Vector3> Normals =>
            SecondarySources.Select(secondarySource => secondarySource.Normal).ToList();

        [SerializeField] [HideInInspector] protected WallBehaviour wallParent; 

        public virtual void ProcessAll(WallBehaviour wall)
        {
            wallParent = wall;
        }
        
        public void OnDrawGizmosSelected(Color color)
        {
            Gizmos.color = color;
            if (SecondarySources== null) return;
            
            foreach (var geometrySse in SecondarySources)
            {
                Gizmos.DrawWireCube(geometrySse.position, geometrySse.dimensions);
                Gizmos.DrawSphere(geometrySse.position, geometrySse.Thickness);
            }
        }
        

        protected List<GeometrySs> GetSsPositionsFromDelta(Vector3 normWall, float deltaX, float deltaY, float deltaZ,
            Vector3 pos1, int numberOfSecondarySourcesPerDim)
        {
            return GetSsPositionsFromDelta(normWall, deltaX, deltaY, deltaZ, pos1, numberOfSecondarySourcesPerDim,
                numberOfSecondarySourcesPerDim, numberOfSecondarySourcesPerDim);
        }
        
        protected List<GeometrySs> GetSsPositionsFromDelta(Vector3 normWall, float deltaX, float deltaY, float deltaZ, Vector3 pos1,int numX,int numY,int numZ)
        {
            var ssPos = new List<GeometrySs>();
            if (Math.Abs(normWall.x - 1) < 1e-10)
                numX = 1;
            else if (Math.Abs(normWall.y - 1) < 1e-10)
                numY = 1;
            else if (Math.Abs(normWall.z - 1) < 1e-10)
                numZ = 1;

            for (var ix = 0; ix < numX; ix++)
            for (var iy = 0; iy < numY; iy++)
            for (var iz = 0; iz < numZ; iz++)
            {
                var pos = new Vector3(
                    pos1.x + deltaX / 2 + ix * deltaX, 
                    pos1.y + deltaY / 2 + iy * deltaY,
                    pos1.z + deltaZ / 2 + iz * deltaZ);
                var geo = new GeometrySs(pos, normWall, new Vector3(deltaX, deltaY, deltaZ));
                ssPos.Add(geo);
            }

            return ssPos;
        }
    }
}