﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class WallGeometryManual : WallGeometry
    {
        public override WallGeometryType Type => WallGeometryType.Manual;
        public override Vector3 Dimensions => dimensions;
        [SerializeField] private Vector3 dimensions;

        public override Vector3 Normal => normal;
        [SerializeField] private Vector3 normal;
        
        public override Vector3 Position => position;
        [SerializeField] private Vector3 position;

        public override void OnValidate()
        {
            
        }

        public override void Init(GameObject gameObject)
        {
            id = gameObject.GetInstanceID();
        }

    }
}