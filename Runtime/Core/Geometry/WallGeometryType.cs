﻿namespace BA.BACore
{
    public enum WallGeometryType
    {
        Manual,
        AutoTransform,
        AutoMeshFilter,
    }
}