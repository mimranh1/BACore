﻿using System;
using log4net;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public abstract class WallGeometry : ExtendedScriptableObject<WallGeometry,WallGeometryType>
    {
        [HideInInspector] public int id;
        public abstract Vector3 Dimensions { get; }
        public abstract Vector3 Normal { get; }
        public abstract Vector3 Position { get; }
        
        /// <summary>
        /// Return Thickness of the Wall
        /// </summary>
        public float Thickness => Vector3.Dot(Normal, Dimensions);

        /// <summary>
        /// Return Length of the Wall
        /// </summary>
        public float Length
        {
            get
            {
                var bound = Dimensions;
                return Mathf.Max(bound.x, bound.y, bound.z);
            }
        }

        /// <summary>
        /// Return Width of the Wall
        /// </summary>
        public float Width
        {
            get
            {
                var bound = Dimensions;
                if (Normal == Vector3.right) return Mathf.Min(bound.y, bound.z);
                if (Normal == Vector3.up) return Mathf.Min(bound.x, bound.z);
                if (Normal == Vector3.forward) return Mathf.Min(bound.x, bound.y);
                Log.Warn("Normal Vector has to be wrong.");
                return -1;
            }
        }

        /// <summary>
        /// Return Area of the Wall
        /// </summary>
        public float Area => Length * Width;
        
        private static readonly ILog Log = LogManager.GetLogger(typeof(WallGeometry));
        
        public abstract void OnValidate();


        public abstract void Init(GameObject gameObject);
    }
}