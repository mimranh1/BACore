﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using Object = System.Object;

namespace BA.BACore
{
    public static class WallGeometryConstructor
    {
        public static WallGeometry Create(WallGeometryType propertiesTypes, WallBehaviour wallBehaviour)
        {
            var allDetectorTypes = Assembly.GetAssembly(typeof(WallGeometry)).GetTypes()
                .Where(t => typeof(WallGeometry).IsAssignableFrom(t) && t.IsAbstract == false);

            var parameter = new[] {(Object)wallBehaviour};
            foreach (var detectorType in allDetectorTypes)
            {
                var detector = Activator.CreateInstance(detectorType, parameter) as WallGeometry;
                if (detector != null && detector.Type == propertiesTypes)
                    return detector;
            }

            Debug.LogError($"WallGeometry Property Type {propertiesTypes} not found");
            return null;

        }
     

    }
}