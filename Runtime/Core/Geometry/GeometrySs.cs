﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class GeometrySs : Geometry
    {
        public override Vector3 Dimensions => dimensions;
        [SerializeField][ReadOnly] public Vector3 dimensions;

        public override Vector3 Normal => normal;
        [SerializeField][ReadOnly] public Vector3 normal;

        public override Vector3 Position => position;
        [SerializeField][ReadOnly] public Vector3 position;

        public GeometrySs(WallBehaviour wall)
        {
            dimensions = wall.Geometry.Dimensions;
            normal = wall.Geometry.Normal;
            position = wall.Geometry.Position;
        }
        public GeometrySs(Vector3 position,Vector3 normal, Vector3 dimensions)
        {
            this.dimensions = dimensions;
            this.normal = normal;
            this.position = position;
        }
    }
}