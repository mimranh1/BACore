﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]   
    public class WallGeometryAutoMeshFilter : WallGeometryAuto
    {
        public override WallGeometryType Type => WallGeometryType.AutoMeshFilter;
        public override Vector3 Dimensions => dimensions;
        [SerializeField][ReadOnly]  private Vector3 dimensions;

        public override Vector3 Normal => normal;
        [SerializeField][ReadOnly]  private Vector3 normal;
        
        public override Vector3 Position => position;
        [SerializeField][ReadOnly] private Vector3 position;


        public override void OnValidate()
        {
            
        }


        private Transform _transform;
        private MeshFilter _meshFilter;
        public override void Init(GameObject gameObject)
        {
            id = gameObject.GetInstanceID();
            _meshFilter = gameObject.GetComponent<MeshFilter>();
            _transform = gameObject.transform;
            SetDimensions();
            normal = GetNormalVector(dimensions);
        }
    
        private void SetDimensions()
        {
            var transform1 = _transform;
            var localScale = transform1.localScale;
            dimensions = transform1.rotation * _meshFilter.sharedMesh.bounds.size;
            dimensions.x *= localScale.x;
            dimensions.y *= localScale.y;
            dimensions.z *= localScale.z;
            dimensions = RoundDimensions(dimensions);

            position = transform1.position;
        }

    }
}