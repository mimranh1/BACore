﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class WallSegmentsMaxLength : WallSegments
    {
        public override WallSegmentType Type => WallSegmentType.MaxLength;

        public override List<GeometrySs> SecondarySources => secondarySources;
        [SerializeField]private List<GeometrySs> secondarySources;

        public float maxLength;

        public override void ProcessAll(WallBehaviour wall)
        {
            
            base.ProcessAll(wall);
            secondarySources= GenerateSecondarySourcePositionsFromMaxLength(wall);
            
            if (secondarySources.Count ==0)
                secondarySources.Add(new GeometrySs(wall));
        }
     
        private List<GeometrySs> GenerateSecondarySourcePositionsFromMaxLength(WallBehaviour wall, int id = -1)
        {
            var boundWall = wall.Geometry.Dimensions;
            var posWall = wall.Geometry.Position;
            var normWall = wall.Geometry.Normal;

            var pos1 = posWall - 0.5f * boundWall;
            var pos2 = posWall + 0.5f * boundWall;


            var numX = Math.Max((int)Mathf.Round((pos2.x - pos1.x) / maxLength), 1);
            var numY = Math.Max((int)Mathf.Round((pos2.y - pos1.y) / maxLength), 1);
            var numZ = Math.Max((int)Mathf.Round((pos2.z - pos1.z) / maxLength), 1);

            var deltaX = (pos2.x - pos1.x) / numX;
            var deltaY = (pos2.y - pos1.y) / numY;
            var deltaZ = (pos2.z - pos1.z) / numZ;
            return GetSsPositionsFromDelta(normWall, deltaX, deltaY, deltaZ, pos1,numX,numY,numZ);
        }
        
    }
}