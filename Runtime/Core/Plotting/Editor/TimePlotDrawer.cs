﻿using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace BA.BACore.Editor
{
    [CustomPropertyDrawer(typeof(TimePlot))]
    public class TimePlotDrawer : PropertyDrawer
    {
        private bool isActive = false;
        private Texture2D texture;
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty (position, label, property);
            var pos2 = new Rect {x = position.x, y = position.y, width = 100, height = 20};
            if (GUI.Button(pos2, "Refresh"))
            {
                var updateProp= property.FindPropertyRelative("updatePlotFlag") ;
                updateProp.boolValue = true;
                updateProp.serializedObject.ApplyModifiedProperties();

            }
            
            var imageProp= property.FindPropertyRelative("timePlot") ;
            var sprite = imageProp.objectReferenceValue as Texture2D;
            // position.width = 300;
            // position = EditorGUI.PrefixLabel (position, GUIUtility.GetControlID(FocusType.Passive), label);
            isActive = sprite != null;
            if (isActive)
            {
                position.width = 400;
                position.height = 120;
                //EditorGUI.DrawPreviewTexture(position, sprite.texture, null, ScaleMode.ScaleToFit, 0);
                GUI.DrawTexture(position, sprite, ScaleMode.ScaleToFit);
            }
            EditorGUI.EndProperty ();
        }
        
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            // return unfold ? base.GetPropertyHeight(property, label)*5*4 : base.GetPropertyHeight(property, label)*7;
            return isActive ? 120 : base.GetPropertyHeight(property, label);
        }
 
    }
}