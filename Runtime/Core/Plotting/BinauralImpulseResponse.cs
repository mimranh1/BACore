﻿using System;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class BinauralImpulseResponse : IImpulseResponse
    {
        public bool IsInitialized => irLeft.IsInitialized && irRight.IsInitialized;
        
        public ImpulseResponseIfftChannel IrLeft=>irLeft;
        [SerializeField][HideInInspector] private ImpulseResponseIfftChannel irLeft;
        public ImpulseResponseIfftChannel IrRight=>irRight;
        [SerializeField][HideInInspector] private ImpulseResponseIfftChannel irRight;
        
        [SerializeField] private TimeBinauralPlot irTimeBinauralPlot;
        public int[] sourceIds;

        public BinauralImpulseResponse(int filterResolution)
        {
            InitArrays(filterResolution);
            Debug.Log("BinauralImpulseResponse created.");
        }

        public void InitArrays(int filterResolution)
        {
            irLeft = new ImpulseResponseIfftChannel(filterResolution);
            irRight = new ImpulseResponseIfftChannel(filterResolution);
        }

        public void Clear()
        {
            irLeft.Clear();
            irRight.Clear();
        }

        public void OnValidate()
        {
            irLeft.OnValidate();
            irRight.OnValidate();
        }

        public void UpdatePlot()
        {
            if (irTimeBinauralPlot == null)
                irTimeBinauralPlot = new TimeBinauralPlot();
            irTimeBinauralPlot.SetUpdateFlag();
            irTimeBinauralPlot.OnValidate(IrLeft.time, IrRight.time);
        }
        public void OnDisable()
        {
            irLeft.OnDisable();
            irRight.OnDisable();
        }

        public void RunIfft()
        {
            irLeft.RunIfft();
            irRight.RunIfft();
        }
    }
}