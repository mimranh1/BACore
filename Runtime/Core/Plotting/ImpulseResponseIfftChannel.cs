﻿using System;
using System.Runtime.InteropServices;
using FFTWSharp;
using UnityEngine;
using UnityEngine.UI;

namespace BA.BACore
{
    [Serializable]
    public class ImpulseResponseIfftChannel : IImpulseResponse
    {
        [HideInInspector] public float[] freqComplex;
        [HideInInspector] public float[] timeComplex;
        [HideInInspector] public float[] time;
        private GCHandle _irIfftInputHandle;
        private GCHandle _irIfftResultHandle;
        private IntPtr _irIfftPlan;
        
        public ImpulseResponseIfftChannel(int filterResolution)
        {
            freqComplex = new float[filterResolution * 2];
            timeComplex = new float[filterResolution * 2];
            time = new float[filterResolution];
            _irIfftInputHandle = GCHandle.Alloc(freqComplex, GCHandleType.Pinned);
            _irIfftResultHandle = GCHandle.Alloc(timeComplex, GCHandleType.Pinned);
            _irIfftPlan = fftwf.dft_1d(filterResolution, _irIfftInputHandle.AddrOfPinnedObject(),
                _irIfftResultHandle.AddrOfPinnedObject(), fftw_direction.Backward,fftw_flags.Estimate);
        }

        public bool IsInitialized => _irIfftInputHandle.IsAllocated;

        public void RunIfft()
        {
            if(!_irIfftInputHandle.IsAllocated )
            {
                Debug.LogError("IFFt not inti yet.");
                return;
            }

            fftwf.execute(_irIfftPlan);
            for (var i = 0; i < time.Length; i++)
            {
                time[i] = timeComplex[2 * i];
            }
        }

        public void Clear()
        {
            Array.Clear(freqComplex, 0, freqComplex.Length);
        }

        public void OnValidate()
        {
        }


        public void OnDisable()
        {
            _irIfftInputHandle.Free();
            _irIfftResultHandle.Free();
        }
        
       
    }
}