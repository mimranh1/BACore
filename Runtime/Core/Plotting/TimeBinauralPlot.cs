﻿using System;
using System.Linq;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class TimeBinauralPlot : TimePlot
    {

        public TimeBinauralPlot() : base()
        {
        }

        public override void SetUpdateFlag()
        {
            updatePlotFlag = true;
        }

        public void OnValidate(float[] timeLeft,float[] timeRight)
        {
            if (!updatePlotFlag) return;
            const int width = 500;
            const int height = 200;
            var tex = InitTexture2D(width, height);
            var waveformLeft = GetWaveform(timeLeft, width, 1);
            var waveformRight = GetWaveform(timeRight, width, 1);
            var maxCorrectionFactor = 1 / Mathf.Max(Max(waveformLeft), Max(waveformRight));
            tex = PaintWaveformSpectrum(tex, waveformLeft, Color.blue, maxCorrectionFactor);
            timePlot = PaintWaveformSpectrum(tex, waveformRight, Color.red, maxCorrectionFactor);
            updatePlotFlag = false;
        }

        
    }
}