﻿using UnityEngine;

namespace BA.BACore
{
    [SerializeField]
    public class TimeBinauralPlotContainer
    {
        [SerializeField] private TimeBinauralPlot[] timeBinauralPlots;

        public TimeBinauralPlotContainer(int amoung)
        {
            timeBinauralPlots = new TimeBinauralPlot[amoung];
            for (var i = 0; i < amoung; i++)
                timeBinauralPlots[i] = new TimeBinauralPlot();
        }

        public void OnValidate(int index,float[] timeLeft,float[] timeRight)
        {
            timeBinauralPlots[index].OnValidate(timeLeft, timeRight);
        }
    }
}