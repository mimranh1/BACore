﻿using System;
using System.Linq;
using UnityEngine;

namespace BA.BACore
{
    [Serializable]
    public class TimePlot
    {
        public bool updatePlotFlag;
        [SerializeField] protected Texture2D timePlot;

        public TimePlot()
        {
        }

        public virtual void SetUpdateFlag()
        {
            updatePlotFlag = true;
        }

        public void OnValidate(float[] time)
        {
            if (!updatePlotFlag) return;
            
            var width = 500;
            var height = 200;
            var tex = InitTexture2D(width, height);
            var waveformLeft = GetWaveform(time, width, 1);
            var maxCorrectionFactor = 1 / Max(waveformLeft);
            timePlot = PaintWaveformSpectrum(tex, waveformLeft, Color.gray, maxCorrectionFactor);
            updatePlotFlag = false;
        }
        
       
        protected float Max(float[] time)
        {
            return time.Aggregate(0f, (current, sample) => Mathf.Max(Mathf.Abs(sample), current));
        } 
        protected Texture2D PaintWaveformSpectrum(Texture2D tex,float[] waveform, Color col, float correctionFactor=1f)
        {
            var height = tex.height;
            for (var x = 0; x < waveform.Length; x++) {
                for (var y = 0; y <= waveform[x] *correctionFactor* ((float)height * .5f); y++) {
                    tex.SetPixel(x, ( height / 2 ) + y, col);
                    tex.SetPixel(x, ( height / 2 ) - y, col);
                }
            }
            tex.Apply();
 
            return tex;
        }

        protected static Texture2D InitTexture2D(int width, int height)
        {
            var tex = new Texture2D(width, height, TextureFormat.RGBA32, false);
            // var audioSamples = samples.Length;
            // var waveform = new float[width];
            // var packSize = ( audioSamples / width ) + 1;
            // var s = 0;
            // for (var i = 0; i < audioSamples; i += packSize) {
            //     waveform[s] = Mathf.Abs(samples[i]);
            //     s++;
            // }
            for (var x = 0; x < width; x++)
            {
                for (var y = 0; y < height; y++)
                {
                    tex.SetPixel(x, y, Color.grey);
                }
            }

            return tex;
        }

        protected static float[] GetWaveform(float[] samples, int size, float sat = 1f)
        {
            var audioSamples = samples.Length;
            var waveform = new float[size];
            var packSize = audioSamples / size;

            var max = 0f;
            var c = 0;
            var s = 0;
            for (var i = 0; i < audioSamples; i++)
            {
                waveform[c] += Mathf.Abs(samples[i]);
                s++;
                if (s <= packSize) continue;
                if (max < waveform[c])
                    max = waveform[c];
                c++;
                s = 0;
            }


            return waveform;
        }
    }
}