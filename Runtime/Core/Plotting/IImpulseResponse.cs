﻿namespace BA.BACore
{
    public interface IImpulseResponse
    {
        bool IsInitialized { get; }
        void RunIfft();
        void Clear();
        void OnValidate();

        void OnDisable();
    }
}