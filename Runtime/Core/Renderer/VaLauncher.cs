﻿using UnityEngine;
using System;
using System.IO;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Debug = UnityEngine.Debug;

namespace BA.BACore
{
    public class VaLauncher
    {
        // [DllImport("user32.dll")]
        // static extern bool PostMessage(IntPtr hWnd, UInt32 Msg, int wParam, int lParam);
        private Process _process = null;
        private StreamWriter _messageStream;

        public void StartProcess(string vaServerPath, bool redirectOutput=false)
        {
            try
            {
				var workingDirectory = $"{BaSettings.Instance.RootPath}/{vaServerPath}";
                if(!Directory.Exists(workingDirectory))
                    Debug.LogError("Cannot find VA Dir " + workingDirectory);
                    
                _process = new Process
                {
                    EnableRaisingEvents = false,
                    StartInfo =
                    {
                        WorkingDirectory = workingDirectory,
                        FileName = "cmd.exe",
                        // Arguments = " run_VAServer.bat",
                        Arguments = "/c" + "run_VAServer.bat",
                        UseShellExecute = !redirectOutput,
                        // CreateNoWindow = true,
                        RedirectStandardOutput = redirectOutput,
                        // RedirectStandardInput = true,
                        RedirectStandardError = redirectOutput
                    }
                };
                if(redirectOutput)
                {
                    _process.OutputDataReceived += new DataReceivedEventHandler(DataReceived);
                    _process.ErrorDataReceived += new DataReceivedEventHandler(ErrorReceived);
                }
                _process.Start();
                if(redirectOutput)
                {
                    _process.BeginOutputReadLine();
                    _process.BeginErrorReadLine();
                }
                Debug.Log("Successfully launched app");
            }
            catch (Exception e)
            {
                Debug.LogError("Unable to launch app: " + e.Message);
            }
        }

        public void SentInput(string inputCommand)
        {
            // process.StandardInput.Write("+");
            // messageStream.Write(inputCommand);
            // messageStream.Flush();
            // messageStream.Close();
        }

        private void DataReceived(object sender, DataReceivedEventArgs eventArgs)
        {
            Debug.Log($"<b>[VA]</b> {eventArgs.Data}");
        }


        private void ErrorReceived(object sender, DataReceivedEventArgs eventArgs)
        {
            Debug.LogError($"<color=red><b>[VAError]</b> {eventArgs.Data}</color>");
        }

        public void OnApplicationQuit()
        {
            // const UInt32 WM_KEYDOWN = 0x0100;
            // const UInt32 WM_KEYUP = 0x0101;
            // const int VK_F5 = 0x74;
            // const int VK_Q = 0x51;
            //
            // PostMessage(process.MainWindowHandle, WM_KEYDOWN, VK_Q, 0);
            // PostMessage(process.MainWindowHandle, WM_KEYUP, VK_Q, 0);
            // SentInput("+");
            // SentInput("q");
            // process.WaitForExit();
            // if (process != null && !process.HasExited )
            // {
            //     process.Kill();
            // }
        }
    }
}