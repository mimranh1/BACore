﻿using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.Audio;

namespace BA.BACore
{
    [RequireComponent(typeof(AudioSource))]
    public class AudioRendererUnityPlugin : AudioRender
    {
        [DllImport("AudioPluginDemo")]
        private static extern bool ConvolutionReverb_UploadSample(int index, float[] data, int numsamples, int numchannels, int samplerate, [MarshalAs(UnmanagedType.LPStr)] string name);
        
        public override AudioRendererType Type => AudioRendererType.UnityPlugin;
        public override SoundSourceMover SourceMover { get; }

        private float[] irLeft;
        private float[] irAlternate;
        [SerializeField] private AudioMixer audioMixer;
        
        public override void Init(AuralisationCalculation auralisationCalculation)
        {
            irAlternate = new float[auralisationCalculation.FilterResolution * 2];
        }

        public override void UpdateFilter(float[] irLeftNew, float[] irRightNew, int sourceIndex = 0, int offset = 0)
        {
            Alternate(irLeftNew, irRightNew);
            ConvolutionReverb_UploadSample(0, irAlternate, irAlternate.Length / 2, 2, 444100, "");
            audioMixer.SetFloat("useSample", 0);
        }

        private void Alternate(float[] irLeftNew, float[] irRightNew)
        {
            if (irAlternate.Length / 2 != irLeftNew.Length || irAlternate.Length / 2 != irRightNew.Length)
                Debug.LogError("");
            for (var n = 0; n < irAlternate.Length; n++)
            {
                irAlternate[n] = n % 2 == 0 ? irLeftNew[n / 2] : irRightNew[(n - 1) / 2];
            }
        }

        public void SetGain(float gain)
        {
        }

        public float GetGain()
        {
            return 0f;
        }

        public override void Disable()
        {
            throw new System.NotImplementedException();
        }

        public override void OnValidate()
        {
            
        }

        public override float ResetAndPlay()
        {
            return 0f;
        }

        public override void ResetSounds()
        {
        }

        public override void PlaySounds()
        {
        }
    }
}