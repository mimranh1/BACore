using System;
using UnityEngine;

namespace BA.BACore
{
    /// <summary>
    /// Base class for different Renderer implementations
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    public abstract class AudioRender : MonoBehaviour
    {
        public abstract AudioRendererType Type { get; }
        public abstract SoundSourceMover SourceMover { get; }

        /// <summary>
        /// Init Interface for the Renderer 
        /// </summary>
        /// <param name="lengthIr">length of impulse response</param>
        /// <exception cref="NotImplementedException">the implementation is in the class which inherit this one</exception>
        public abstract void Init(AuralisationCalculation auralisationCalculation);

        /// <summary>
        /// Update Filter Interface for the Renderer
        /// </summary>
        /// <param name="irLeftNew">impulse response left time</param>
        /// <param name="irRightNew">impulse response right time</param>
        /// <param name="offset">sample delay for both channels</param>
        /// <exception cref="NotImplementedException">the implementation is in the class which inherit this one</exception>
        public abstract void UpdateFilter(float[] irLeftNew, float[] irRightNew, int sourceIndex = 0, int offset = 0);

        /// <summary>
        /// Called at the end of program to wind up 
        /// </summary>
        /// <exception cref="NotImplementedException">the implementation is in the class which inherit this one</exception>
        public abstract void Disable();

        public abstract void OnValidate();

        public abstract float ResetAndPlay();

        public abstract void ResetSounds();
        
        public abstract void PlaySounds();
    }
}