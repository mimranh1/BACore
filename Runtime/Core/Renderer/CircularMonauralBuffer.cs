namespace BA.BACore
{
    /// <summary>
    /// Circular buffer for add overlap algorithm for mono signal
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    public class CircularMonauralBuffer
    {
        private readonly float[] buffer;
        private int nextFree;
        private readonly int blockSize;
        private readonly int bufferLength;
        private readonly float[] currentBuffer;

        /// <summary>
        /// Init Buffer
        /// </summary>
        /// <param name="length">Length of Buffer</param>
        /// <param name="blockSize">Block length</param>
        public CircularMonauralBuffer(int length, int blockSize)
        {
            bufferLength = (length / blockSize + 1) * blockSize;
            buffer = new float[bufferLength];
            nextFree = 0;
            this.blockSize = blockSize;
            currentBuffer = new float[blockSize];
        }

        /// <summary>
        /// Fill buffer and return current bock
        /// </summary>
        /// <param name="ir"></param>
        /// <param name="log"></param>
        /// <returns></returns>
        public float[] SumAndIncreasePointer(float[] ir, string log = null)
        {
            for (var i = 0; i < ir.Length; i++)
            {
                var bufferIndex = (i + nextFree) % bufferLength;
                buffer[bufferIndex] += ir[i];
            }

            for (var i = 0; i < blockSize; i++)
            {
                var bufferIndex = (i + nextFree) % bufferLength;
                currentBuffer[i] = buffer[bufferIndex];
                buffer[bufferIndex] = 0;
            }

            nextFree = (nextFree + blockSize) % buffer.Length;

            if (log != null)
            {
                DataLogger.Log("buffer.buffer" + log, buffer);
                DataLogger.Log("buffer.ir" + log, ir);
                DataLogger.Log("buffer.nextFree" + log, nextFree);
                DataLogger.Log("buffer.currentBlockOut" + log, currentBuffer);
            }

            return currentBuffer;
        }

        /// <summary>
        /// return the remaining Buffer and clear it
        /// </summary>
        /// <returns></returns>
        public float[] GetRemainingBuffer()
        {
            var currentBuffer = new float[bufferLength];
            for (var i = 0; i < bufferLength; i++)
            {
                var bufferIndex = (i + nextFree) % bufferLength;
                currentBuffer[i] = buffer[bufferIndex];
                buffer[bufferIndex] = 0;
            }

            nextFree = 0;
            return currentBuffer;
        }
    }
}