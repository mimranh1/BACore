﻿namespace BA.BACore
{
    public enum AudioRendererType
    {
        Unity,
        UnityPlugin,
        /**< New Approach Indoor Model (AcIndoor) */
        VA, /**< New Approach Indoor Model with Patches (AcIndoor) */
    }
}