namespace BA.BACore
{
    /// <summary>
    /// Circular buffer for add overlap algorithm for binaural signal
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    public class CircularBinauralBuffer
    {
        private readonly float[] bufferLeft;
        private readonly float[] bufferRight;
        private int nextFree;
        private readonly int blockSize;
        private readonly int bufferLength;

        /// <summary>
        /// Init Buffer
        /// </summary>
        /// <param name="length">Length of Buffer</param>
        /// <param name="blockSize">Block length</param>
        public CircularBinauralBuffer(int length, int blockSize)
        {
            bufferLength = (length / blockSize + 1) * blockSize;
            bufferLeft = new float[bufferLength];
            bufferRight = new float[bufferLength];
            nextFree = 0;
            this.blockSize = blockSize;
        }

        /// <summary>
        /// Fill buffer and return current bock
        /// </summary>
        /// <param name="dataOut">Output array interleave left right channel</param>
        /// <param name="irLeft">input left channel</param>
        /// <param name="irRight">input right channel</param>
        public void SumAndIncreasePointer(float[] dataOut, float[] irLeft, float[] irRight)
        {
            for (var i = 0; i < irLeft.Length; i++)
            {
                var bufferIndex = (i + nextFree) % bufferLength;
                bufferLeft[bufferIndex] += irLeft[i];
                bufferRight[bufferIndex] += irRight[i];
            }

            for (var i = 0; i < blockSize; i++)
            {
                var bufferIndex = (i + nextFree) % bufferLength;
                dataOut[2 * i] = bufferLeft[bufferIndex];
                bufferLeft[bufferIndex] = 0;
                dataOut[2 * i + 1] = bufferRight[bufferIndex];
                bufferRight[bufferIndex] = 0;
            }

            nextFree = (nextFree + blockSize) % bufferRight.Length;
        }
    }
}