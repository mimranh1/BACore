using System;
using System.IO;
using System.Runtime.InteropServices;
using FFTWSharp;
using log4net;
using UnityEngine;

namespace BA.BACore
{
    /// <summary>
    /// Audio Renderer implementation for the Unity API
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2019 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    [Serializable]
    public class AudioRendererUnity : AudioRender
    {
        private float gain = 1f;
        [Header("Unity Settings")] [ReadOnly] public double sampleRate = 0.0F;

        /**< Samplerate from Unity */
        [ReadOnly] public double dspTime = 0.0F;

        /**< DSP Time from Unity */
        private bool running = false;

        private int fftLength;
        private float[] irLeftFreq;
        private float[] irRightFreq;

        [ReadOnly] public int blockLength;

        /**< Block length from Unity */
        private CircularBinauralBuffer buffer;

        // FFT Irs
        private GCHandle irFftResultLeftHandle;
        private GCHandle irFftResultRightHandle;
        private float[] irFftLeftInput;
        private float[] irFftRightInput;
        private GCHandle irFftInputLeftHandle;
        private GCHandle irFftInputRightHandle;
        private IntPtr irFftPlanLeft;
        private IntPtr irFftPlanRight;

        // FFT Data
        private float[] dataFftInput;
        private float[] dataFftOutput;
        private GCHandle dataFftInputHandle;
        private GCHandle dataFftResultHandle;
        private IntPtr dataFftPlan;

        // IFFT Left
        private float[] ifftLeftInput;
        private float[] ifftLeftOutput;
        private GCHandle ifftInputLeftHandle;
        private GCHandle ifftResultLeftHandle;
        private IntPtr ifftPlanLeft;

        // IFFT Right
        private float[] ifftRightInput;
        private float[] ifftRightOutput;
        private GCHandle ifftInputRightHandle;
        private GCHandle ifftResultRightHandle;
        private IntPtr ifftPlanRight;

        [Header("Save Output")] public bool writeOutput;

        /**< write output of renderer to file */
        public string postFix = "";

        /**< postfix of log file */
        public float outputLength;

        /**< number of seconds that will be logged to wav file */

        // Test Settings
        [Header("Test Settings")] [ContextMenuItem("Test", "TestLogAddOverlap")]
        public AudioClip audioClip;

        /**< AudioClip for Test */
        private AudioClip outputAudio;

        private int outputAudioOffset;
        private int limSamples;
        private float[] samplesOut;
        [Header("Source Settings")] [SerializeReference] private SoundSourceMover sourceMover = null;

        public override AudioRendererType Type => AudioRendererType.Unity;
        public override SoundSourceMover SourceMover => sourceMover;

        private static readonly ILog Log = LogManager.GetLogger(typeof(AudioRendererUnity));

        private void Start()
        {
            if (AudioSettings.outputSampleRate != 44100)
                Debug.LogError($"outputSampleRate not matching {AudioSettings.outputSampleRate}");
            irLeftFreq = new float[44100];
            irRightFreq = new float[44100];
        }

        private void OnAudioFilterRead(float[] data, int channels)
        {
            //Debug.Log("Call OnAudioFilterRead");
            //Debug.Log("DataLength " + data.Length + "; channel " + channels);
            if (!running | isAllZero(data))
                return;

            if (channels == 2)
            {
                OverlapAddBinaural(data);
                if (writeOutput)
                    if (outputAudioOffset < limSamples)
                    {
                        data.CopyTo(samplesOut, outputAudioOffset);
                        outputAudioOffset += data.Length;
                    }

                for (var i = 0; i < data.Length; i++)
                    data[i] *= gain;
            }
        }

        private bool isAllZero(float[] input)
        {
            for (var i = 0; i < input.Length; i++)
                if (input[i] != 0)
                    return false;
            return true;
        }

        /// <summary>
        /// Init add overlap algorithm 
        /// </summary>
        public override void Init(AuralisationCalculation auralisationCalculation)
        {
            if (sourceMover != null)
                sourceMover.Init(this);
        
            outputAudioOffset = 0;
            dspTime = AudioSettings.dspTime;
            sampleRate = AudioSettings.outputSampleRate;
            AudioSettings.GetDSPBufferSize(out blockLength, out _);
            samplesOut = new float[(int) (outputLength * sampleRate) * 2];
            limSamples = (int) (outputLength * sampleRate) * 2 - 2 * blockLength;
            fftLength = Mathf.Max(blockLength, blockLength + auralisationCalculation.FilterResolution - 1);
            fftLength = 65536;

            // Crate the output buffer for the final result
            buffer = new CircularBinauralBuffer(fftLength + blockLength, blockLength);

            // Init FFT for ir 
            irFftLeftInput = new float[fftLength];
            irLeftFreq = new float[2 * fftLength];
            irFftInputLeftHandle = GCHandle.Alloc(irFftLeftInput, GCHandleType.Pinned);
            irFftResultLeftHandle = GCHandle.Alloc(irLeftFreq, GCHandleType.Pinned);
            irFftPlanLeft = fftwf.dft_r2c_1d(fftLength, irFftInputLeftHandle.AddrOfPinnedObject(),
                irFftResultLeftHandle.AddrOfPinnedObject(), fftw_flags.Estimate);

            irFftRightInput = new float[fftLength];
            irRightFreq = new float[2 * fftLength];
            irFftInputRightHandle = GCHandle.Alloc(irFftRightInput, GCHandleType.Pinned);
            irFftResultRightHandle = GCHandle.Alloc(irRightFreq, GCHandleType.Pinned);
            irFftPlanRight = fftwf.dft_r2c_1d(fftLength, irFftInputRightHandle.AddrOfPinnedObject(),
                irFftResultRightHandle.AddrOfPinnedObject(), fftw_flags.Estimate);

            // init FFT for Audio Data
            dataFftInput = new float[fftLength];
            dataFftOutput = new float[2 * fftLength];
            dataFftInputHandle = GCHandle.Alloc(dataFftInput, GCHandleType.Pinned);
            dataFftResultHandle = GCHandle.Alloc(dataFftOutput, GCHandleType.Pinned);
            dataFftPlan = fftwf.dft_r2c_1d(fftLength, dataFftInputHandle.AddrOfPinnedObject(),
                dataFftResultHandle.AddrOfPinnedObject(), fftw_flags.Estimate);


            // init IFFT for Audio Data
            ifftLeftInput = new float[2 * fftLength];
            ifftLeftOutput = new float[fftLength];
            ifftInputLeftHandle = GCHandle.Alloc(ifftLeftInput, GCHandleType.Pinned);
            ifftResultLeftHandle = GCHandle.Alloc(ifftLeftOutput, GCHandleType.Pinned);
            ifftPlanLeft = fftwf.dft_c2r_1d(fftLength, ifftInputLeftHandle.AddrOfPinnedObject(),
                ifftResultLeftHandle.AddrOfPinnedObject(), fftw_flags.Estimate);

            ifftRightInput = new float[2 * fftLength];
            ifftRightOutput = new float[fftLength];
            ifftInputRightHandle = GCHandle.Alloc(ifftRightInput, GCHandleType.Pinned);
            ifftResultRightHandle = GCHandle.Alloc(ifftRightOutput, GCHandleType.Pinned);
            ifftPlanRight = fftwf.dft_c2r_1d(fftLength, ifftInputRightHandle.AddrOfPinnedObject(),
                ifftResultRightHandle.AddrOfPinnedObject(), fftw_flags.Estimate);
        }


        /// <summary>
        /// Wind up the memory allocation of fft
        /// </summary>
        public override void Disable()
        {
            // Write Output Stream
            if (writeOutput)
            {
                outputAudio = AudioClip.Create("output", samplesOut.Length, 2, (int) sampleRate, false);
                outputAudio.SetData(samplesOut, 0);
                var timePrefix = DateTime.Now.ToString("yyyyMMdd-HHmmss-");
                var filename = "AudioStreamOut/" + timePrefix + outputLength.ToString("0.0") + "s-" + postFix +
                               "-OutputStream.wav";
                var filePath = Path.Combine(BaSettings.Instance.SoundsPath, filename);
                SavWav.Save(filePath, outputAudio);
            }

            fftwf.destroy_plan(irFftPlanLeft);
            fftwf.destroy_plan(irFftPlanRight);
            irFftInputLeftHandle.Free();
            irFftInputRightHandle.Free();
            irFftResultLeftHandle.Free();
            irFftResultRightHandle.Free();

            fftwf.destroy_plan(dataFftPlan);
            dataFftInputHandle.Free();
            dataFftResultHandle.Free();

            fftwf.destroy_plan(ifftPlanLeft);
            fftwf.destroy_plan(ifftPlanRight);
            ifftInputLeftHandle.Free();
            ifftInputRightHandle.Free();
            ifftResultLeftHandle.Free();
            ifftResultRightHandle.Free();

            fftwf.cleanup();
        }

        public override void OnValidate()
        {
            
        }

        public override float ResetAndPlay()
        {
            throw new NotImplementedException();
        }

        public override void ResetSounds()
        {
            throw new NotImplementedException();
        }

        public override void PlaySounds()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Update Filter for the Unity Renderer
        /// </summary>
        /// <param name="irLeftNew">impulse response left time</param>
        /// <param name="irRightNew">impulse response right time</param>
        /// <param name="sourceIndex"></param>
        /// <param name="offset">sample delay for both channels</param>
        public override void UpdateFilter(float[] irLeftNew, float[] irRightNew, int sourceIndex = 0, int offset = 0)
        {
            if (offset != 0)
                Log.Warn("Offset for Unity Renderer not supported jet.");
            running = true;

            // Transform it into the frequency domain
            irLeftNew.CopyTo(irFftLeftInput, 0);
            irRightNew.CopyTo(irFftRightInput, 0);

            fftwf.execute(irFftPlanLeft);
            fftwf.execute(irFftPlanRight);
        }

        /// <summary>
        /// Set Output volume Gain for the Renderer
        /// </summary>
        /// <param name="gainNew">current gain in dB</param>
        public void SetGain(float gainNew)
        {
            gain = Mathf.Pow(10f, gainNew / 10f);
        }


        /// <summary>
        /// Returns Output volume Gain for the Unity Renderer
        /// </summary>
        /// <returns>input gain in dB</returns>
        public float GetGain()
        {
            return 10f * Mathf.Log10(gain);
        }
    
 

        /// <summary>
        /// OLA Overlap-Add convolution using the FFT
        /// </summary>
        /// <param name="data"></param>
        private void OverlapAddBinaural(float[] data)
        {
            var channelLength = data.Length / 2;
            var dataLeft = new float[channelLength];
            for (var i = 0; i < channelLength; i++)
                dataLeft[i] = data[2 * i];

            ConvTimeAndFreq(dataLeft, out var bufferLeft, out var bufferRight);

            buffer.SumAndIncreasePointer(data, bufferLeft, bufferRight);
        }

        private void ConvTimeAndFreq(float[] irTime, out float[] bufferLeft, out float[] bufferRight)
        {
            // FFT Audio Data
            irTime.CopyTo(dataFftInput, 0);
            fftwf.execute(dataFftPlan);

            // Convolve (multiply) Data
            MultiplyComplex(irLeftFreq, dataFftOutput, ifftLeftInput);
            MultiplyComplex(irRightFreq, dataFftOutput, ifftRightInput);

            // Ifft
            fftwf.execute(ifftPlanLeft);
            fftwf.execute(ifftPlanRight);

            bufferLeft = ifftLeftOutput;
            bufferRight = dataFftInput;
        }

        private void MultiplyComplex(float[] tf1, float[] tf2, float[] ftOut)
        {
            if (tf1.Length != tf2.Length || tf2.Length != ftOut.Length)
                Debug.LogError("Have to have same Length.");

            for (var n = 0; n < tf1.Length; n += 2)
            {
                ftOut[n] = tf1[n] * tf2[n] - tf1[n + 1] * tf2[n + 1];
                ftOut[n + 1] = tf1[n + 1] * tf2[n] + tf1[n] * tf2[n + 1];
            }
        }

        /// <summary>
        /// Run Add Overlap offline and write to log file
        /// </summary>
        public void TestLogAddOverlap()
        {
            var data = new float[blockLength];
            var offsetSample = 0;
            DataLogger.DebugData = true;
            DataLogger.Log("irLeftTime", irFftLeftInput);
            DataLogger.Log("irRightTime", irFftRightInput);
            DataLogger.Log("irLeftFreqComplex", irLeftFreq);
            DataLogger.Log("irRightFreqComplex", irRightFreq);
            var oldGain = gain;
            gain = 1f;

            var dataInterleave = new float[2 * blockLength];
            var dataLeft = new float[blockLength];
            var dataRight = new float[blockLength];
            var lastBlockCounter = 0;
            var logIndex = 1;
            while (true)
            {
                if (audioClip.samples > offsetSample + blockLength)
                {
                    audioClip.GetData(data, offsetSample);
                    DataLogger.Log("data(:," + logIndex + ")", data);
                    for (var i = 0; i < blockLength; i++)
                    {
                        dataInterleave[2 * i] = data[i];
                        dataInterleave[2 * i + 1] = data[i];
                    }
                }
                else
                {
                    dataInterleave = new float[2 * blockLength];
                    DataLogger.Log("data(:," + logIndex + ")", new float[blockLength]);
                    lastBlockCounter++;
                }


                OnAudioFilterRead(dataInterleave, 2);

                for (var i = 0; i < blockLength; i++)
                {
                    dataLeft[i] = dataInterleave[2 * i];
                    dataRight[i] = dataInterleave[2 * i + 1];
                }

                DataLogger.Log("dataLeft(:," + logIndex + ")", dataLeft);
                DataLogger.Log("dataRight(:," + logIndex + ")", dataRight);
                offsetSample += blockLength;
                logIndex++;

                if (lastBlockCounter > fftLength / blockLength)
                    break;
            }

            gain = oldGain;
        }
    }
}