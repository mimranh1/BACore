﻿using System;
using System.Collections;
using System.Collections.Generic;
using log4net;
using UnityEngine;
using VA;

namespace BA.BACore
{
    /// <summary>
    /// Audio Renderer for Virtual Acoustics, start VA server before 
    /// </summary>
    /// <remarks>
    /// Authors:        Muhammad Imran (mim@akustik.rwth-aachen.de), 
    ///                 Anne Heimes (anne.heimes@rwth-aachen.de) <br>
    /// Version:        1.2  <br>
    /// First release:  2017 <br>
    /// Last revision:  2020 <br>
    /// Copyright:      Institute of Technical Acoustics, RWTH Aachen University
    /// </remarks>
    [Serializable]
    public class AudioRendererVaBasic : AudioRender
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(AudioRendererVaBasic));

        private float gainInDb = 0f;
        private double currentGain;

        [Header("Source Settings")] public SoundSourceMover sourceMover = null;


        // VA
        private static VANet vaNet;
        [Rename("Source File")] private string[] vaSourceFilePaths;

        /**< Source File for Unity */
        [Header("VA Settings")] [Rename("VA Server")]
        public string vaServer = "localhost";

        [Rename("VA Port")] public int vaPort = 12340;
        private int[] vaSourceIds;
        private string[] signalSourceIds;

        private VANet VA
        {
            get
            {
                if (vaNet != null) return vaNet;

                vaNet = new VANet();
                VaInit(); // Try to connect as early as possible

                return vaNet;
            }
        }

        public override AudioRendererType Type { get; }
        public override SoundSourceMover SourceMover { get; }

        /// <summary>
        /// Init VA Connection
        /// </summary>
        public override void Init(AuralisationCalculation auralisationCalculation)
        {
            var sources = auralisationCalculation.SoundSourceObjects;
            vaSourceFilePaths = new string[sources.Length];
            vaSourceIds = new int[sources.Length];
            signalSourceIds = new string[sources.Length];
            for (var i = 0; i < sources.Length; i++)
            {
                vaSourceFilePaths[i] = sources[i].sourceFile;
                sources[i].InitAudioSourceVA(i);
            }

            if (sourceMover != null)
                sourceMover.Init(this);
            CreateSoundSources(sources);
            SetListener();
        }

        /// <summary>
        /// Update Filter in VA
        /// </summary>
        /// <param name="irLeftNew">impulse response left time</param>
        /// <param name="irRightNew">impulse response right time</param>
        /// <param name="offset">sample delay for both channels</param>
        public override void UpdateFilter(float[] irLeftNew, float[] irRightNew, int sourceIndex = 0, int offset = 0)
        {
            if (!Application.isPlaying)
                return;
            var delaySeconds = offset / BaSettings.Instance.SampleRate;
            if (delaySeconds > 25f || delaySeconds < 0f)
                Log.Warn("offset not in range of 0 to 25 seconds " + delaySeconds);

            VA.UpdateGenericPath("MyGenericRenderer", vaSourceIds[sourceIndex], 1, 1, delaySeconds, irLeftNew.Length,
                irLeftNew);
            VA.UpdateGenericPath("MyGenericRenderer", vaSourceIds[sourceIndex], 1, 2, delaySeconds, irRightNew.Length,
                irRightNew);
        }

        /// <summary>
        /// Set Output volume Gain for the Renderer
        /// </summary>
        /// <param name="gainNew">current gain in dB</param>
        public void SetGain(float gainNew)
        {
            gainInDb = gainNew;
            VA.SetInputGain(gainNew);
        }

        /// <summary>
        /// Returns Output volume Gain for the Unity Renderer
        /// </summary>
        /// <returns>input gain in dB</returns>
        public float GetGain()
        {
            return gainInDb;
        }

        /// <summary>
        /// no clean up needed
        /// </summary>
        public override void Disable()
        {
            VA.Disconnect();
        }

        public override void OnValidate()
        {
            
        }


        private void SetIrToVa(float[][] impulseResponse, int sourceIndex = 0)
        {
            if (!Application.isPlaying)
                return;
            if (impulseResponse.Length != 2)
                Log.Error("ImpulseResponse must have 2 Channels for VA");
            VA.UpdateGenericPath("MyGenericRenderer", vaSourceIds[sourceIndex], 1, 1, 0.0f, impulseResponse[0].Length,
                impulseResponse[0]);
            VA.UpdateGenericPath("MyGenericRenderer", vaSourceIds[sourceIndex], 1, 2, 0.0f, impulseResponse[1].Length,
                impulseResponse[1]);
        }

        private void SetIrToVa(float[] irLeft, float[] irRight, int sourceIndex = 0)
        {
            if (!Application.isPlaying)
                return;
            VA.UpdateGenericPath("MyGenericRenderer", vaSourceIds[sourceIndex], 1, 1, 0.0f, irLeft.Length, irLeft);
            VA.UpdateGenericPath("MyGenericRenderer", vaSourceIds[sourceIndex], 1, 2, 0.0f, irRight.Length, irRight);
        }


        private void VaInit()
        {
            if (!VA.Connect(vaServer, vaPort))
            {
                Debug.LogWarning("Could not connect to VA server on " + vaServer + " using port " + vaPort);
                return;
            }

            //if (vaResetOnStart)
            //    VA.Reset();

            // Add Asset folder as search path for VA (only works if VA is running on same host PC)
            VA.AddSearchPath(System.IO.Directory.GetCurrentDirectory());

            if (!VA.AddSearchPath(Application.dataPath))
                Debug.LogError(
                    "Could not add application assets folder to VA search path, VA server running on remote host?");
            if (!VA.AddSearchPath($"{BaSettings.Instance.SoundsPath}"))
                Debug.LogError(
                    "Could not add "+BaSettings.Instance.SoundsPath+" folder to VA search path.");
        }

        private void CreateSoundSources(SoundSourceObject[] sources)
        {
            if (vaSourceFilePaths == null)
            {
                Debug.LogError("Set SourcePaths of ...");
                return;
            }

            for (var iSource = 0; iSource < vaSourceFilePaths.Length; iSource++)
            {
                var sourceName = "Source " + 0;

                vaSourceIds[iSource] = VA.CreateSoundSource(sourceName);

                signalSourceIds[iSource] = VA.CreateSignalSourceBufferFromFile(vaSourceFilePaths[iSource], sourceName);
                VA.SetSignalSourceBufferLooping(signalSourceIds[iSource], sources[iSource].AudioLoop);
                VA.SetSoundSourceSignalSource(vaSourceIds[iSource], signalSourceIds[iSource]);
                VA.SetSoundSourcePosition(vaSourceIds[iSource], new VAVec3(0f, 0f, 0f));
                VA.SetSoundSourceOrientationVU(vaSourceIds[iSource], new VAVec3(1f, 0f, 0f),
                    new VAVec3(0f, 0f, 0f));
                if (sources[iSource].AudioPlayOnAwake)
                    VA.SetSignalSourceBufferPlaybackAction(signalSourceIds[iSource], "PLAY");
            }

        }

        public override float ResetAndPlay()
        {
            for (var iSource = 0; iSource < vaSourceFilePaths.Length; iSource++)
            {
                VA.SetSignalSourceBufferPlaybackPosition(signalSourceIds[iSource], 0.0);
                VA.SetSignalSourceBufferPlaybackAction(signalSourceIds[iSource], "PLAY");
            }

            return 1f;
        }

        public override void ResetSounds()
        {
            throw new NotImplementedException();
        }

        public override void PlaySounds()
        {
            throw new NotImplementedException();
        }

        private void SetListener()
        {
            var listenerId = VA.CreateSoundReceiver("Listener");
            VA.SetSoundReceiverAuralizationMode(listenerId, "all");

            VA.SetSoundReceiverAnthropometricData(listenerId, 0.12, 0.10, 0.15);
            VA.SetSoundReceiverPosition(listenerId, new VAVec3(0f, 0f, 0f));
            VA.SetSoundReceiverOrientation(listenerId, new VAQuat(1f, 0f, 0f, 0));

            VA.SetArtificialReverberationTime("MyGenericRenderer", 0.3f);
        }
    }

}
