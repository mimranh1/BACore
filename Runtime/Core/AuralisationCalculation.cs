﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using log4net;
using UnityEngine;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;

namespace BA.BACore
{
    [Serializable]
    public abstract class
        AuralisationCalculation : ExtendedScriptableObject<AuralisationCalculation, AuralisationCalculationType>
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(AuralisationCalculation));

        [SerializeField] protected bool logUpdateTime;
        [HideInInspector] public string label;
        [HideInInspector] public Vector3 refPosition;

        [Subtitle] [SerializeField] private string generalFilterSettings = "";

        [Range(-100, 100)] [Rename("Gain (dB)")]
        public float defaultGainDb = 0;

        public Stopwatch Stopwatch
        {
            get
            {
                if (_stopwatch == null)
                    _stopwatch = new Stopwatch();
                return _stopwatch;
            }
        }

        private Stopwatch _stopwatch;

        // [SerializeField][HideInInspector] protected AudioRender _audioRender;

        /// <summary>
        /// Filter Resolution of the Signal Processing
        /// </summary>
        public int FilterResolution => BaSettings.Instance.filterLengthInSamples;

        /// <summary>
        /// Set true if Source Update should process even if position and orientation is not updated
        /// </summary>
        public bool ForceSourceUpdate;

        /// <summary>
        /// Set true if Receiver Update should process even if position and orientation is not updated
        /// </summary>
        public bool ForceReceiverUpdate;

        /// <summary>
        /// log all filter one time and write to logFile (not working)
        /// </summary>
        [HideInInspector] public bool LogRealtime;

        protected int HrirCounter;


        /// <summary>
        /// return true if Init was called
        /// </summary>
        protected bool init = false;

        /// <summary>
        /// LogName for logging the Data
        /// </summary>
        [HideInInspector] protected string LogName;

        [Subtitle] public string CurrentLatencies = "";

        /// <summary>
        /// last measured time for the source update
        /// </summary>
        [ReadOnly] public double InitTime;
        /// <summary>
        /// last measured time for the source update
        /// </summary>
        [ReadOnly] public double Offline;

        /// <summary>
        /// last measured time for the source update
        /// </summary>
        [ReadOnly] public double SourceUpdate;

        /// <summary>
        /// last measured time for the receiver update
        /// </summary>
        [ReadOnly] public double ReceiverUpdate;

        /// <summary>
        /// last measured time for the filter update
        /// </summary>
        [ReadOnly] public double FilterUpdate;
        /// <summary>
        /// last measured time for the source update
        /// </summary>
        [ReadOnly] public List<double> SourceUpdates;

        /// <summary>
        /// last measured time for the receiver update
        /// </summary>
        [ReadOnly] public List<double> ReceiverUpdates;

        /// <summary>
        /// last measured time for the filter update
        /// </summary>
        [ReadOnly] public List<double> FilterUpdates;


        public abstract SoundSourceObject[] SoundSourceObjects { get; }


        public abstract SoundReceiverObject SoundReceiverObject { get; }


        public abstract void OnValidate();

        /// <summary>
        /// Form for Initialisation with Init BuildingAcoustics Array from FilterManager
        /// </summary>
        protected abstract void Init();
        public virtual void InitMeasure()
        {
            Stopwatch.Restart();
            Init();
            Stopwatch.Stop();
            InitTime = Stopwatch.Elapsed.TotalMilliseconds;
            if (logUpdateTime)
                Debug.Log("InitTime " + InitTime);
        }

        /// <summary>
        /// Form for Calculate pre-calculation and and set AudioRenderer
        /// </summary>
        protected abstract void OfflineCalculation();

        public virtual void OfflineCalculationMeasure()
        {
            Stopwatch.Restart();
            OfflineCalculation();
            Stopwatch.Stop();
            Offline = Stopwatch.Elapsed.TotalMilliseconds;
            if (logUpdateTime)
                Debug.Log("Offline " + Offline);
        }

        /// <summary>
        /// Form for Realtime Update, called if Source or Receiver Updates
        /// </summary>
        /// <param name="log">write log file if logId > 1</param>
        protected abstract void RealTimeCalculation(AudioRender audioRender, bool log = false);

        public virtual void RealTimeCalculationMeasure(AudioRender audioRender, bool log = false)
        {
            Stopwatch.Restart();
            RealTimeCalculation(audioRender, log);
            Stopwatch.Stop();
            FilterUpdate = Stopwatch.Elapsed.TotalMilliseconds;
            if (logUpdateTime)
                FilterUpdates.Add(FilterUpdate);
        }

        /// <summary>
        /// Form for Update Receiver Data for filter Construction if Receiver Update
        /// </summary>
        /// <returns>Receiver Update</returns>
        protected abstract bool RealTimeReceiverUpdate();

        public virtual bool RealTimeReceiverUpdateMeasure()
        {
            Stopwatch.Restart();
            var result = RealTimeReceiverUpdate();
            Stopwatch.Stop();
            if (!result) return false;
            ReceiverUpdate = Stopwatch.Elapsed.TotalMilliseconds;
            if (logUpdateTime)
                ReceiverUpdates.Add(ReceiverUpdate);
            return true;
        }

        /// <summary>
        /// Form for Update Source Data for filter Construction if Source Update
        /// </summary>
        /// <returns>Source Update</returns>
        protected abstract bool RealTimeSourceUpdate();

        public virtual bool RealTimeSourceUpdateMeasure()
        {
            Stopwatch.Start();
            var result = RealTimeSourceUpdate();
            Stopwatch.Stop();
            if (!result) return false;
            SourceUpdate = Stopwatch.Elapsed.TotalMilliseconds;
            if (logUpdateTime)
                SourceUpdates.Add(SourceUpdate);
            return true;
        }

        public abstract void SaveImpulseResponse(string name="");

        /// <summary>
        /// Form for Call on Disable
        /// </summary>
        public virtual void OnDisable()
        {

        }
    }
}