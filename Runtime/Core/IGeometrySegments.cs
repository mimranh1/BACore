﻿namespace BA.BACore
{
    public interface IGeometrySegments
    {
        Geometry[] Segments { get; }
    }
}