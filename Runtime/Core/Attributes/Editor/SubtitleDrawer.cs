using BA.BACore;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(SubtitleAttribute))]
public class SubtitleDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position,
        SerializedProperty property,
        GUIContent label)
    {
        position.y += 5;
        EditorGUI.LabelField(position, label, new GUIStyle {fontStyle = FontStyle.Bold});
    }
}