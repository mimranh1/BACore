using UnityEngine;

namespace BA.BACore
{
    public class RenameAttribute : PropertyAttribute
    {
        public string NewName { get; private set; }

        public RenameAttribute(string name)
        {
            NewName = name;
        }
    }
}