﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace BA.BACore.Editor
{
    [CustomPropertyDrawer(typeof(ExtendedScriptableObject), true)]
    public class ExtendedScriptableObjectDrawer : PropertyDrawer
    {
    // Use the following area to change the style of the expandable ScriptableObject drawers;
    #region Style Setup
    private enum BackgroundStyles
    {
        None,
        HelpBox,
        Darken,
        Lighten
    }
 
    /// <summary>
    /// Whether the default editor Script field should be shown.
    /// </summary>
    private static bool SHOW_SCRIPT_FIELD = false;
 
    /// <summary>
    /// The spacing on the inside of the background rect.
    /// </summary>
    private static float INNER_SPACING = 6.0f;
 
    /// <summary>
    /// The spacing on the outside of the background rect.
    /// </summary>
    private static float OUTER_SPACING = 4.0f;
 
    /// <summary>
    /// The style the background uses.
    /// </summary>
    private static BackgroundStyles BACKGROUND_STYLE = BackgroundStyles.HelpBox;
 
    /// <summary>
    /// The colour that is used to darken the background.
    /// </summary>
    private static Color DARKEN_COLOUR = new Color(0.0f, 0.0f, 0.0f, 0.2f);
 
    /// <summary>
    /// The colour that is used to lighten the background.
    /// </summary>
    private static Color LIGHTEN_COLOUR = new Color(1.0f, 1.0f, 1.0f, 0.2f);
    #endregion

   public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        float totalHeight = 0.0f;
 
        totalHeight += EditorGUIUtility.singleLineHeight;
 
        if (property.objectReferenceValue == null)
            return totalHeight;
 
        if (!property.isExpanded)
            return totalHeight;
 
        SerializedObject targetObject = new SerializedObject(property.objectReferenceValue);
 
        if (targetObject == null)
            return totalHeight;
 
        SerializedProperty field = targetObject.GetIterator();
 
        field.NextVisible(true);
 
        if (SHOW_SCRIPT_FIELD)
        {
            totalHeight += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
        }
 
        while (field.NextVisible(false))
        {
            totalHeight += EditorGUI.GetPropertyHeight(field, true) + EditorGUIUtility.standardVerticalSpacing;
        }
 
        totalHeight += INNER_SPACING * 2;
        totalHeight += OUTER_SPACING * 2;
 
        return totalHeight;
    }
 
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        Rect fieldRect = new Rect(position);
        fieldRect.height = EditorGUIUtility.singleLineHeight;
 
        EditorGUI.PropertyField(fieldRect, property, label, true);
 
        if (property.objectReferenceValue == null)
            return;
 
        property.isExpanded = EditorGUI.Foldout(fieldRect, property.isExpanded, GUIContent.none, true);
 
        if (!property.isExpanded)
            return;
 
        SerializedObject targetObject = new SerializedObject(property.objectReferenceValue);
 
        if (targetObject == null)
            return;
 
 
        #region Format Field Rects
        List<Rect> propertyRects = new List<Rect>();
        Rect marchingRect = new Rect(fieldRect);
 
        Rect bodyRect = new Rect(fieldRect);
        bodyRect.xMin += EditorGUI.indentLevel * 14;
        bodyRect.yMin += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing
            + OUTER_SPACING;
 
        SerializedProperty field = targetObject.GetIterator();
        field.NextVisible(true);
 
        marchingRect.y += INNER_SPACING + OUTER_SPACING;
 
        if (SHOW_SCRIPT_FIELD)
        {
            propertyRects.Add(marchingRect);
            marchingRect.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
        }
 
        while (field.NextVisible(false))
        {
            marchingRect.y += marchingRect.height + EditorGUIUtility.standardVerticalSpacing;
            marchingRect.height = EditorGUI.GetPropertyHeight(field, true);
            propertyRects.Add(marchingRect);
        }
 
        marchingRect.y += INNER_SPACING;
 
        bodyRect.yMax = marchingRect.yMax;
        #endregion
 
        DrawBackground(bodyRect);
 
        #region Draw Fields
        EditorGUI.indentLevel++;
 
        int index = 0;
        field = targetObject.GetIterator();
        field.NextVisible(true);
 
        if (SHOW_SCRIPT_FIELD)
        {
            //Show the disabled script field
            EditorGUI.BeginDisabledGroup(true);
            EditorGUI.PropertyField(propertyRects[index], field, true);
            EditorGUI.EndDisabledGroup();
            index++;
        }
 
        //Replacement for "editor.OnInspectorGUI ();" so we have more control on how we draw the editor
        while (field.NextVisible(false))
        {
            try
            {
                EditorGUI.PropertyField(propertyRects[index], field, true);
            }
            catch (StackOverflowException)
            {
                field.objectReferenceValue = null;
                Debug.LogError("Detected self-nesting cauisng a StackOverflowException, avoid using the same " +
                    "object iside a nested structure.");
            }
 
            index++;
        }
 
        targetObject.ApplyModifiedProperties();
 
        EditorGUI.indentLevel--;
        #endregion
    }
 
    /// <summary>
    /// Draws the Background
    /// </summary>
    /// <param name="rect">The Rect where the background is drawn.</param>
    private void DrawBackground(Rect rect)
    {
        switch (BACKGROUND_STYLE)
        {
 
            case BackgroundStyles.HelpBox:
                EditorGUI.HelpBox(rect, "", MessageType.None);
                break;
 
            case BackgroundStyles.Darken:
                EditorGUI.DrawRect(rect, DARKEN_COLOUR);
                break;
 
            case BackgroundStyles.Lighten:
                EditorGUI.DrawRect(rect, LIGHTEN_COLOUR);
                break;
        }
    }
}
 
    //
    // {
    //     // Cached scriptable object editor
    //     UnityEditor.Editor editor = null;
    //
    //     public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    //     {
    //         // Draw label
    //         EditorGUI.PropertyField(position, property, label, true);
    //
    //         // Draw foldout arrow
    //         if (property.objectReferenceValue != null)
    //         {
    //             property.isExpanded = EditorGUI.Foldout(position, property.isExpanded, GUIContent.none);
    //         }
    //
    //         // Draw foldout properties
    //         if (property.isExpanded)
    //         {
    //             // Make child fields be indented
    //             EditorGUI.indentLevel++;
    //
    //             // background
    //             GUILayout.BeginVertical("box");
    //
    //             if (!editor)
    //                 UnityEditor.Editor.CreateCachedEditor(property.objectReferenceValue, null, ref editor);
    //
    //             // Draw object properties
    //             EditorGUI.BeginChangeCheck();
    //             if (editor) // catch empty property
    //             {
    //                 editor.OnInspectorGUI ();
    //             }
    //             if (EditorGUI.EndChangeCheck())
    //                 property.serializedObject.ApplyModifiedProperties();
    //
    //             GUILayout.EndVertical ();
    //
    //             // Set indent back to what it was
    //             EditorGUI.indentLevel--;
    //         }
    //     }
    // }
    //
    // [CanEditMultipleObjects]
    // [CustomEditor(typeof(UnityEngine.Object), true)]
    // public class UnityObjectEditor : UnityEditor.Editor
    // {
    // }
    //

}
// // Developed by Tom Kail at Inkle
// // Released under the MIT Licence as held at https://opensource.org/licenses/MIT
//
// // Must be placed within a folder named "Editor"
// using System;
// using System.Reflection;
// using System.Collections;
// using System.Collections.Generic;
// using UnityEngine;
// using UnityEditor;
// using Object = UnityEngine.Object;
//
// namespace BA.BACore.Editor
// {
//
//
// 	/// <summary>
// 	/// Extends how ScriptableObject object references are displayed in the inspector
// 	/// Shows you all values under the object reference
// 	/// Also provides a button to create a new ScriptableObject if property is null.
// 	/// </summary>
// 	[CustomPropertyDrawer(typeof(ExtendedScriptableObject), true)]
// 	public class ExtendedScriptableObjectDrawer : PropertyDrawer
// 	{
//
// 		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
// 		{
// 			var type = GetFieldType();
// 			float totalHeight = EditorGUIUtility.singleLineHeight;
// 			if (property.objectReferenceValue == null || !AreAnySubPropertiesVisible(property))
// 			{
// 				return totalHeight;
// 			}
//
// 			if (property.isExpanded)
// 			{
// 				var data = property.objectReferenceValue as ScriptableObject;
// 				if (data == null) return EditorGUIUtility.singleLineHeight;
// 				var serializedObject = new SerializedObject(data);
// 				var prop = serializedObject.GetIterator();
// 				if (prop.NextVisible(true))
// 				{
// 					do
// 					{
// 						if (prop.name == "m_Script") continue;
// 						var subProp = serializedObject.FindProperty(prop.name);
// 						float height = EditorGUI.GetPropertyHeight(subProp, null, true) +
// 						               EditorGUIUtility.standardVerticalSpacing;
// 						totalHeight += height;
// 					} while (prop.NextVisible(false));
// 				}
//
// 				// Add a tiny bit of height if open for the background
// 				totalHeight += EditorGUIUtility.standardVerticalSpacing;
// 			}
// 			// if(!type.IsAbstract)
// 			// totalHeight -= 2*EditorGUIUtility.singleLineHeight;
// 			return totalHeight;
// 		}
//
// 		const int buttonWidth = 0;
//
// 		static readonly List<string> ignoreClassFullNames = new List<string> {"TMPro.TMP_FontAsset"};
//
// 		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
// 		{
// 			EditorGUI.BeginProperty(position, label, property);
// 			var type = GetFieldType();
//
// 			if (type == null || ignoreClassFullNames.Contains(type.FullName))
// 			{
// 				EditorGUI.PropertyField(position, property, label);
// 				EditorGUI.EndProperty();
// 				return;
// 			}
//
// 			ScriptableObject propertySO = null;
// 			if (!property.hasMultipleDifferentValues && property.serializedObject.targetObject != null &&
// 			    property.serializedObject.targetObject is ScriptableObject)
// 			{
// 				propertySO = (ScriptableObject) property.serializedObject.targetObject;
// 			}
//
// 			var propertyRect = Rect.zero;
// 			var guiContent = new GUIContent(property.displayName);
// 			var foldoutRect = new Rect(position.x, position.y, EditorGUIUtility.labelWidth,
// 				EditorGUIUtility.singleLineHeight);
// 			if (property.objectReferenceValue != null && AreAnySubPropertiesVisible(property))
// 			{
// 				
// 				property.isExpanded = type.IsAbstract || EditorGUI.Foldout(foldoutRect, property.isExpanded, guiContent, true);
// 			}
// 			else
// 			{
// 				// So yeah having a foldout look like a label is a weird hack 
// 				// but both code paths seem to need to be a foldout or 
// 				// the object field control goes weird when the codepath changes.
// 				// I guess because foldout is an interactable control of its own and throws off the controlID?
// 				foldoutRect.x += 12;
// 				EditorGUI.Foldout(foldoutRect, property.isExpanded, guiContent, true, EditorStyles.label);
// 			}
//
// 			var indentedPosition = EditorGUI.IndentedRect(position);
// 			var indentOffset = indentedPosition.x - position.x;
// 			propertyRect = new Rect(position.x + (EditorGUIUtility.labelWidth - indentOffset), position.y,
// 				position.width - (EditorGUIUtility.labelWidth - indentOffset), EditorGUIUtility.singleLineHeight);
//
// 			if (propertySO != null || property.objectReferenceValue == null)
// 			{
// 				propertyRect.width -= buttonWidth;
// 			}
//
// 			// EditorGUI.LabelField(propertyRect,type.FullName);
// 			// property.objectReferenceValue =
// 			// 	EditorGUI.ObjectField(propertyRect, GUIContent.none, property.objectReferenceValue, type, false);
// 			if (GUI.changed) property.serializedObject.ApplyModifiedProperties();
//
// 			if (property.propertyType == SerializedPropertyType.ObjectReference &&
// 			    property.objectReferenceValue != null)
// 			{
// 				var data = (ScriptableObject) property.objectReferenceValue;
//
// 				if (property.isExpanded)
// 				{
// 					// Draw a background that shows us clearly which fields are part of the ScriptableObject
// 					var singleLineHeight = type.IsAbstract?0:EditorGUIUtility.singleLineHeight;
// 					var singleLineHeight1 = !type.IsAbstract?0:EditorGUIUtility.singleLineHeight;
// 					GUI.Box(
// 						new Rect(0,
// 							position.y + singleLineHeight + EditorGUIUtility.standardVerticalSpacing -
// 							1,
// 							Screen.width,
// 							position.height - singleLineHeight1-2*singleLineHeight-
// 							EditorGUIUtility.standardVerticalSpacing),
// 						"");
//
// 					EditorGUI.indentLevel++;
// 					SerializedObject serializedObject = new SerializedObject(data);
//
// 					// Iterate over all the values and draw them
// 					SerializedProperty prop = serializedObject.GetIterator();
//
// 					float y = position.y + singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
// 					if (prop.NextVisible(true))
// 					{
// 						do
// 						{
// 							// Don't bother drawing the class file
// 							if (prop.name == "m_Script") continue;
// 							float height = EditorGUI.GetPropertyHeight(prop, new GUIContent(prop.displayName), true);
// 							EditorGUI.PropertyField(new Rect(position.x, y, position.width - buttonWidth, height), prop,
// 								true);
// 							y += height + EditorGUIUtility.standardVerticalSpacing;
// 						} while (prop.NextVisible(false));
// 					}
//
// 					if (GUI.changed)
// 						serializedObject.ApplyModifiedProperties();
//
// 					EditorGUI.indentLevel--;
// 				}
// 			}
// 			
//
// 			property.serializedObject.ApplyModifiedProperties();
// 			EditorGUI.EndProperty();
// 		}
//
// 		private static T _GUILayout<T>(string label, T objectReferenceValue, ref bool isExpanded)
// 			where T : ScriptableObject
// 		{
// 			return _GUILayout<T>(new GUIContent(label), objectReferenceValue, ref isExpanded);
// 		}
//
// 		private static T _GUILayout<T>(GUIContent label, T objectReferenceValue, ref bool isExpanded)
// 			where T : ScriptableObject
// 		{
// 			Rect position = EditorGUILayout.BeginVertical();
//
// 			var propertyRect = Rect.zero;
// 			var guiContent = label;
// 			var foldoutRect = new Rect(position.x, position.y, EditorGUIUtility.labelWidth,
// 				EditorGUIUtility.singleLineHeight);
// 			if (objectReferenceValue != null)
// 			{
// 				isExpanded = EditorGUI.Foldout(foldoutRect, isExpanded, guiContent, true);
//
// 				var indentedPosition = EditorGUI.IndentedRect(position);
// 				var indentOffset = indentedPosition.x - position.x;
// 				propertyRect = new Rect(position.x + EditorGUIUtility.labelWidth - indentOffset, position.y,
// 					position.width - EditorGUIUtility.labelWidth - indentOffset, EditorGUIUtility.singleLineHeight);
// 			}
// 			else
// 			{
// 				// So yeah having a foldout look like a label is a weird hack 
// 				// but both code paths seem to need to be a foldout or 
// 				// the object field control goes weird when the codepath changes.
// 				// I guess because foldout is an interactable control of its own and throws off the controlID?
// 				foldoutRect.x += 12;
// 				EditorGUI.Foldout(foldoutRect, isExpanded, guiContent, true, EditorStyles.label);
//
// 				var indentedPosition = EditorGUI.IndentedRect(position);
// 				var indentOffset = indentedPosition.x - position.x;
// 				propertyRect = new Rect(position.x + EditorGUIUtility.labelWidth - indentOffset, position.y,
// 					position.width - EditorGUIUtility.labelWidth - indentOffset - 60,
// 					EditorGUIUtility.singleLineHeight);
// 			}
//
// 			EditorGUILayout.BeginHorizontal();
// 			objectReferenceValue =
// 				EditorGUILayout.ObjectField(new GUIContent(" "), objectReferenceValue, typeof(T), false) as T;
//
// 			if (objectReferenceValue != null)
// 			{
//
// 				EditorGUILayout.EndHorizontal();
// 				if (isExpanded)
// 				{
// 					DrawScriptableObjectChildFields(objectReferenceValue);
// 				}
// 			}
// 			else
// 			{
// 				if (GUILayout.Button("Create", GUILayout.Width(buttonWidth)))
// 				{
// 					string selectedAssetPath = "Assets";
// 					var newAsset = CreateAssetWithSavePrompt(typeof(T), selectedAssetPath);
// 					if (newAsset != null)
// 					{
// 						objectReferenceValue = (T) newAsset;
// 					}
// 				}
//
// 				EditorGUILayout.EndHorizontal();
// 			}
//
// 			EditorGUILayout.EndVertical();
// 			return objectReferenceValue;
// 		}
//
// 		private static void DrawScriptableObjectChildFields<T>(T objectReferenceValue) where T : ScriptableObject
// 		{
// 			// Draw a background that shows us clearly which fields are part of the ScriptableObject
// 			EditorGUI.indentLevel++;
// 			EditorGUILayout.BeginVertical(GUI.skin.box);
//
// 			var serializedObject = new SerializedObject(objectReferenceValue);
// 			// Iterate over all the values and draw them
// 			SerializedProperty prop = serializedObject.GetIterator();
// 			if (prop.NextVisible(true))
// 			{
// 				do
// 				{
// 					// Don't bother drawing the class file
// 					if (prop.name == "m_Script") continue;
// 					EditorGUILayout.PropertyField(prop, true);
// 				} while (prop.NextVisible(false));
// 			}
//
// 			if (GUI.changed)
// 				serializedObject.ApplyModifiedProperties();
// 			EditorGUILayout.EndVertical();
// 			EditorGUI.indentLevel--;
// 		}
//
// 		private static T DrawScriptableObjectField<T>(GUIContent label, T objectReferenceValue, ref bool isExpanded)
// 			where T : ScriptableObject
// 		{
// 			Rect position = EditorGUILayout.BeginVertical();
//
// 			var propertyRect = Rect.zero;
// 			var guiContent = label;
// 			var foldoutRect = new Rect(position.x, position.y, EditorGUIUtility.labelWidth,
// 				EditorGUIUtility.singleLineHeight);
// 			if (objectReferenceValue != null)
// 			{
// 				isExpanded = EditorGUI.Foldout(foldoutRect, isExpanded, guiContent, true);
//
// 				var indentedPosition = EditorGUI.IndentedRect(position);
// 				var indentOffset = indentedPosition.x - position.x;
// 				propertyRect = new Rect(position.x + EditorGUIUtility.labelWidth - indentOffset, position.y,
// 					position.width - EditorGUIUtility.labelWidth - indentOffset, EditorGUIUtility.singleLineHeight);
// 			}
// 			else
// 			{
// 				// So yeah having a foldout look like a label is a weird hack 
// 				// but both code paths seem to need to be a foldout or 
// 				// the object field control goes weird when the codepath changes.
// 				// I guess because foldout is an interactable control of its own and throws off the controlID?
// 				foldoutRect.x += 12;
// 				EditorGUI.Foldout(foldoutRect, isExpanded, guiContent, true, EditorStyles.label);
//
// 				var indentedPosition = EditorGUI.IndentedRect(position);
// 				var indentOffset = indentedPosition.x - position.x;
// 				propertyRect = new Rect(position.x + EditorGUIUtility.labelWidth - indentOffset, position.y,
// 					position.width - EditorGUIUtility.labelWidth - indentOffset - 60,
// 					EditorGUIUtility.singleLineHeight);
// 			}
//
// 			EditorGUILayout.BeginHorizontal();
// 			objectReferenceValue =
// 				EditorGUILayout.ObjectField(new GUIContent(" "), objectReferenceValue, typeof(T), false) as T;
//
// 			if (objectReferenceValue != null)
// 			{
// 				EditorGUILayout.EndHorizontal();
// 				if (isExpanded)
// 				{
//
// 				}
// 			}
// 			else
// 			{
// 				if (GUILayout.Button("Create", GUILayout.Width(buttonWidth)))
// 				{
// 					string selectedAssetPath = "Assets";
// 					var newAsset = CreateAssetWithSavePrompt(typeof(T), selectedAssetPath);
// 					if (newAsset != null)
// 					{
// 						objectReferenceValue = (T) newAsset;
// 					}
// 				}
//
// 				EditorGUILayout.EndHorizontal();
// 			}
//
// 			EditorGUILayout.EndVertical();
// 			return objectReferenceValue;
// 		}
//
// 		// Creates a new ScriptableObject via the default Save File panel
// 		private static ScriptableObject CreateAssetWithSavePrompt(Type type, string path)
// 		{
// 			path = EditorUtility.SaveFilePanelInProject("Save ScriptableObject", type.Name + ".asset", "asset",
// 				"Enter a file name for the ScriptableObject.", path);
// 			if (path == "") return null;
// 			ScriptableObject asset = ScriptableObject.CreateInstance(type);
// 			AssetDatabase.CreateAsset(asset, path);
// 			AssetDatabase.SaveAssets();
// 			AssetDatabase.Refresh();
// 			AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
// 			EditorGUIUtility.PingObject(asset);
// 			return asset;
// 		}
//
// 		private Type GetFieldType()
// 		{
// 			Type type = fieldInfo.FieldType;
// 			if (type.IsArray) type = type.GetElementType();
// 			else if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(List<>))
// 				type = type.GetGenericArguments()[0];
// 			return type;
// 		}
//
// 		private static bool AreAnySubPropertiesVisible(SerializedProperty property)
// 		{
// 			var data = (ScriptableObject) property.objectReferenceValue;
// 			SerializedObject serializedObject = new SerializedObject(data);
// 			SerializedProperty prop = serializedObject.GetIterator();
// 			while (prop.NextVisible(true))
// 			{
// 				if (prop.name == "m_Script") continue;
// 				return true; //if theres any visible property other than m_script
// 			}
//
// 			return false;
// 		}
// 	}
// }