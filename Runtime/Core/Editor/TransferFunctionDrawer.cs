﻿using UnityEditor;
using UnityEngine;
using UnityEditor.UIElements;
using UnityEngine.UIElements;


namespace BA.BACore.Editor
{
    [CustomPropertyDrawer(typeof(TransferFunction))]
    public class TransferFunctionDrawer : PropertyDrawer
    {
        public bool unfold = false;
        // Draw the property inside the given rect
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // Using BeginProperty / EndProperty on the parent property means that
            // prefab override logic works on the entire property.
            EditorGUI.BeginProperty(position, label, property);

            // Draw label
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            // Don't make child fields be indented
            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            // Calculate rects
            var amountRect = new Rect(position.x, position.y, position.width - 30, 100);
            var maxRect = new Rect(position.x + position.width - 30, position.y, 30, 20);
            var minRect = new Rect(position.x + position.width - 30, position.y + 100 - 20, 30, 20);
            var floatsRect = new Rect(position.x , position.y + 120 , position.width, 20);
            var floatsRectSmall = new Rect(position.x , position.y + 100 , position.width, 20);

            // Draw fields - passs GUIContent.none to each so they are drawn without labels
            EditorGUI.PropertyField(amountRect, property.FindPropertyRelative("curve"), GUIContent.none);
            EditorGUI.PropertyField(minRect, property.FindPropertyRelative("min"), GUIContent.none);
            EditorGUI.PropertyField(maxRect, property.FindPropertyRelative("max"), GUIContent.none);

            // position.height = 16;
            unfold = EditorGUI.Foldout (floatsRectSmall, unfold, new GUIContent("Edit"));
            if (unfold){
                
                // EditorGUI.indentLevel -=1;
                /*Draw some properties*/
                var size = property.FindPropertyRelative("transferFunction");
                // EditorGUI.PropertyField(floatsRect, size, GUIContent.none);
                EditorGUILayout.PropertyField(size);

                // Show(size);
            }
            
            // Set indent back to what it was
            EditorGUI.indentLevel = indent;

            EditorGUI.EndProperty();
        }
        
        public static void Show (SerializedProperty list, bool showListSize = true) {
            EditorGUILayout.PropertyField(list);
            EditorGUI.indentLevel += 1;
            if (list.isExpanded) {
                if (showListSize) {
                    EditorGUILayout.PropertyField(list.FindPropertyRelative("Array.size"));
                }
                for (int i = 0; i < list.arraySize; i++) {
                    EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i));
                }
            }
            EditorGUI.indentLevel -= 1;
        }
        
        // public override VisualElement CreatePropertyGUI(SerializedProperty property)
        // {
        //     // Create property container element.
        //     var container = new VisualElement();
        //
        //     // Create property fields.
        //     var curveField = new PropertyField(property.FindPropertyRelative("curve"));
        //     // var unitField = new PropertyField(property.FindPropertyRelative("unit"));
        //     // var nameField = new PropertyField(property.FindPropertyRelative("name"), "Fancy Name");
        //
        //     // Add fields to the container.
        //     container.Add(curveField);
        //     // container.Add(unitField);
        //     // container.Add(nameField);
        //
        //     return container;
        // }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            // return unfold ? base.GetPropertyHeight(property, label)*5*4 : base.GetPropertyHeight(property, label)*7;
            return  base.GetPropertyHeight(property, label)*7;
            // return base.GetPropertyHeight(property, label)*5;
        }
    }
}