﻿// using UnityEditor;
// using UnityEngine;
//
// namespace BA.BACore
// {
//     [CustomEditor(typeof(BaSettings))]
//     public class BASettingsEditor : ObjectEditor 
//     {
//         private GUIStyle _headerStyle;
//
//         private void OnEnable()
//         {
//             _headerStyle = new GUIStyle {fontStyle = FontStyle.Bold};
//         }
//
//         public override void OnInspectorGUI()
//         {
//             
//             base.OnInspectorGUI();
//             
//             // Database
//             GUILayout.Label("", GUILayout.Height(5));
//             GUILayout.Label("Write Database", _headerStyle);
//
//             if (GUILayout.Button("Write BASTIAN Database"))
//                 ReductionIndexData.CreateAssets();
//
//             if (GUILayout.Button("Write Material Database"))
//                 MaterialStructureData.CreateAssets();
//
//             if (GUILayout.Button("Write Additional Layer Material Database"))
//                 MaterialAddLayerData.CreateAssets();
//
//             if (GUILayout.Button("Write Directivity Database"))
//                 DirectivityIR.CreateAssets();
//
//         }
//     }
// }