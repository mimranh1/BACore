﻿using BA.BACore;
using NUnit.Framework;

namespace Tests
{
    public class IfftRealtimeC2rTests
    {
        [TestCase(new[] {1f, 0f, 1f, 0f, 1f, 0f, 1f, 0f},new[] {1f, 0f, 0f, 0f})]
        [TestCase( new[] {1f, 0f, 1f, 0f, 1f, 0f},new[] {1f, 0f, 0f})]
        [TestCase(
            new[] {1f, 0f, -0.5f, -0.86603f, -0.5f, 0.86603f},new[] {0f, 1f, 0f})]
        [TestCase(
            new[] {-1f, 0f, 1f, 0f, -1f, 0f, 1f, 0f},new[] {0f, 0f, -1f, 0f})]
        [TestCase(
            new[] {1f, 0f, -1f, 0f, 1f, 0f, -1f, 0f},new[] {0f, 0f, 1f, 0f})]
        [TestCase(
            new[] {1f, 0f, -1f, -4f, 1f, 0f, -1f, 4f},new[] {0f, 2f, 1f, -2f})]
        [TestCase(new[] {1f, 0f, 1f, 0f},new[] {1f, 0f})]
        [TestCase(new[] {1f, 0f},new[] {1f})]
        [TestCase(
            new[]
            {
                55f, 0f, -5f, 15.3884f, -5f, 6.8819f, -5f, 3.6327f, -5f, 1.6246f, -5f, 0f, -5f, -1.6246f, -5f, -3.6327f,
                -5f, -6.8819f, -5f, -15.3884f
            }, new[] {1f, 2f, 3f, 4f, 5f, 6f, 7f, 8f, 9f, 10f})]
        public void IfftComplexToRealWithCopyRun(float[] input, float[] expected)
        {
            var fft = new IfftRealtimeC2R(input.Length / 2);
            var actual = new float[input.Length/2];
            fft.RunFftReal(input, actual);
            Assert.That(actual, Is.EqualTo(expected).AsCollection.Within(1e-4));
        }


        [TestCase(new[] {1f, 0f, 1f, 0f, 1f, 0f, 1f, 0f}, new[] {1f, 0f, 0f, 0f})]
        [TestCase(new[] {1f, 0f, 1f, 0f, 1f, 0f}, new[] {1f, 0f, 0f})]
        [TestCase(
            new[] {1f, 0f, -0.5f, -0.86603f, -0.5f, 0.86603f}, new[] {0f, 1f, 0f})]
        [TestCase(
            new[] {-1f, 0f, 1f, 0f, -1f, 0f, 1f, 0f}, new[] {0f, 0f, -1f, 0f})]
        [TestCase(
            new[] {1f, 0f, -1f, 0f, 1f, 0f, -1f, 0f}, new[] {0f, 0f, 1f, 0f})]
        [TestCase(
            new[] {1f, 0f, -1f, -4f, 1f, 0f, -1f, 4f}, new[] {0f, 2f, 1f, -2f})]
        [TestCase(new[] {1f, 0f, 1f, 0f}, new[] {1f, 0f})]
        [TestCase(new[] {1f, 0f}, new[] {1f})]
        [TestCase(
            new[]
            {
                55f, 0f, -5f, 15.3884f, -5f, 6.8819f, -5f, 3.6327f, -5f, 1.6246f, -5f, 0f, -5f, -1.6246f, -5f, -3.6327f,
                -5f, -6.8819f, -5f, -15.3884f
            }, new[] {1f, 2f, 3f, 4f, 5f, 6f, 7f, 8f, 9f, 10f})]
        public void IfftComplexToComplexWithCopyRun(float[] input, float[] expected)
        {
            var inputLength = input.Length / 2;
            var fft = new IfftRealtimeC2R(inputLength);
            var outComplex = new float[input.Length];
            fft.RunFft(input, outComplex);
            var actual = new float[inputLength];
            for (var i = 0; i < actual.Length; i++)
            {

                actual[i] = outComplex[2 * i];
            }

            Assert.That(actual, Is.EqualTo(expected).AsCollection.Within(1e-4));
        }


    }
}