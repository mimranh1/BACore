﻿using BA.BACore;
using NUnit.Framework;

namespace Tests
{
    public class ReverberationDataTest
    {
        [Test]
        public void CheckFactor()
        {
            ReverberationData reverberationData =
                A.ReverberationData.WithReverberation(new[] {1f, 0f, 0f}).WithFactor(2f).WithFilterLength(4);
            var expected = new []{2f,0f,2f,0f,2f,0f,2f,0f};
            var actual = reverberationData.reverberationFactorComplexFreq;
            Assert.That(actual, Is.EqualTo(expected).AsCollection.Within(1e-5));
        }
    }
}