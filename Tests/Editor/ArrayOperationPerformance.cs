﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using BA.BACore;
using NUnit.Framework;
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;

namespace Tests
{
    public class ArrayOperationPerformance 
    {
      
        [Test]
        public void TestMethod2OfMultiplyIndex()
        {
            var length = 10;
            var floatArray = FloatArray(length);
            var floatArray2 = FloatArray(length*2);
            var irLeftFreqComplex =new float[44100];
            var irRightFreqComplex =new float[44100];
            var factors = GenerateRandomArray(length);
            
            var sw =new Stopwatch();
            sw.Start();
            for (var i = 0; i < 1000; i++)
            {
                var flankingFactorsLength = length;
                var sourceRoomIrFlankingOutComplexFreq = floatArray;
                var receiverRoomIrFlankingOutComplexFreq = floatArray2;
                for (var index = 0; index < flankingFactorsLength; index++)
                {
                    var factor = factors[index];

                    var sourceArray = sourceRoomIrFlankingOutComplexFreq[index].floats;
                    var receiverLeftArray = receiverRoomIrFlankingOutComplexFreq[2 * index].floats;
                    var receiverRightArray = receiverRoomIrFlankingOutComplexFreq[2 * index + 1].floats;
                    for (var n = 0; n < sourceArray.Length; n += 2)
                    {
                        var sourceReal = sourceArray[n];
                        var sourceImag = sourceArray[n + 1];
                        var receiverLeftReal = receiverLeftArray[n];
                        var receiverLeftImag = receiverLeftArray[n + 1];
                        var receiverRightReal = receiverRightArray[n];
                        var receiverRightImag = receiverRightArray[n + 1];

                        irLeftFreqComplex[n] += (sourceReal * receiverLeftReal - sourceImag * receiverLeftImag) * factor;
                        irLeftFreqComplex[n + 1] += (sourceReal * receiverLeftImag + sourceImag * receiverLeftReal) *
                                                    factor;
                        irRightFreqComplex[n] += (sourceReal * receiverRightReal - sourceImag * receiverRightImag) *
                                                 factor;
                        irRightFreqComplex[n + 1] += (sourceReal * receiverRightImag + sourceImag * receiverRightReal) *
                                                     factor;
                    }
                }
            }
            sw.Stop();
            Debug.Log("Run in " + sw.ElapsedMilliseconds/1000f +"ms");
        }

        [Test]
        public void TestMethod2OfMultiplySampleParallel()
        {
            var length = 10;
            var floatArray = FloatArray(length);
            var floatArray2 = FloatArray(length*2);
            var irLeftFreqComplex =new float[44100];
            var irRightFreqComplex =new float[44100];
            var factors = GenerateRandomArray(length);
            
            var sw =new Stopwatch();
            sw.Start();
            for (var i = 0; i < 1000; i++)
            {
                var flankingFactorsLength = length;
                var sourceRoomIrFlankingOutComplexFreq = floatArray;
                var receiverRoomIrFlankingOutComplexFreq = floatArray2;
                var receiverWallIds = new int[flankingFactorsLength];
                var sourceWallSourcesIds = new int[flankingFactorsLength];

                Parallel.For(0, sourceRoomIrFlankingOutComplexFreq[sourceWallSourcesIds[0]].floats.Length / 2, j =>
                {
                    var n = 2 * j;
                    var sourceReal = 0f;
                    var sourceImag = 0f;
                    var receiverLeftReal = 0f;
                    var receiverLeftImag = 0f;
                    var receiverRightReal = 0f;
                    var receiverRightImag = 0f;
                    for (var index = 0; index < flankingFactorsLength; index++)
                    {
                        sourceReal += sourceRoomIrFlankingOutComplexFreq[sourceWallSourcesIds[index]].floats[n] *
                                      factors[index];
                        sourceImag += sourceRoomIrFlankingOutComplexFreq[sourceWallSourcesIds[index]].floats[n + 1] *
                                      factors[index];
                        receiverLeftReal += receiverRoomIrFlankingOutComplexFreq[2 * receiverWallIds[index]].floats[n] *
                                            factors[index];
                        receiverLeftImag +=
                            receiverRoomIrFlankingOutComplexFreq[2 * receiverWallIds[index]].floats[n + 1] *
                            factors[index];
                        receiverRightReal +=
                            receiverRoomIrFlankingOutComplexFreq[2 * receiverWallIds[index] + 1].floats[n] *
                            factors[index];
                        receiverRightImag += receiverRoomIrFlankingOutComplexFreq[2 * receiverWallIds[index] + 1]
                            .floats[n + 1] * factors[index];
                    }

                    irLeftFreqComplex[n] += (sourceReal * receiverLeftReal - sourceImag * receiverLeftImag);
                    irLeftFreqComplex[n + 1] += (sourceReal * receiverLeftImag + sourceImag * receiverLeftReal);
                    irRightFreqComplex[n] += (sourceReal * receiverRightReal - sourceImag * receiverRightImag);
                    irRightFreqComplex[n + 1] += (sourceReal * receiverRightImag + sourceImag * receiverRightReal);
                });
            }
            sw.Stop();
            Debug.Log("Run TestMethod2OfMultiplySampleParallel in " + sw.ElapsedMilliseconds/1000f +"ms");
        }

        [Test]
        public void TestMethod2OfMultiplySample()
        {
            var length = 10;
            var floatArray = FloatArray(length);
            var floatArray2 = FloatArray(length*2);
            var irLeftFreqComplex =new float[44100];
            var irRightFreqComplex =new float[44100];
            var factors = GenerateRandomArray(length);
            
            var sw =new Stopwatch();
            sw.Start();
            for (var i = 0; i < 1000; i++)
            {
                var flankingFactorsLength = length;
                var sourceRoomIrFlankingOutComplexFreq = floatArray;
                var receiverRoomIrFlankingOutComplexFreq = floatArray2;
                var receiverWallIds = new int[flankingFactorsLength];
                var sourceWallSourcesIds = new int[flankingFactorsLength];

                for(var n=0; n<sourceRoomIrFlankingOutComplexFreq[sourceWallSourcesIds[0]].floats.Length;n+=2)
                {
                    var sourceReal = 0f;
                    var sourceImag = 0f;
                    var receiverLeftReal = 0f;
                    var receiverLeftImag = 0f;
                    var receiverRightReal = 0f;
                    var receiverRightImag = 0f;
                    for (var index = 0; index < flankingFactorsLength; index++)
                    {
                        sourceReal += sourceRoomIrFlankingOutComplexFreq[sourceWallSourcesIds[index]].floats[n] *
                                      factors[index];
                        sourceImag += sourceRoomIrFlankingOutComplexFreq[sourceWallSourcesIds[index]].floats[n + 1] *
                                      factors[index];
                        receiverLeftReal += receiverRoomIrFlankingOutComplexFreq[2 * receiverWallIds[index]].floats[n] *
                                            factors[index];
                        receiverLeftImag +=
                            receiverRoomIrFlankingOutComplexFreq[2 * receiverWallIds[index]].floats[n + 1] *
                            factors[index];
                        receiverRightReal +=
                            receiverRoomIrFlankingOutComplexFreq[2 * receiverWallIds[index] + 1].floats[n] *
                            factors[index];
                        receiverRightImag += receiverRoomIrFlankingOutComplexFreq[2 * receiverWallIds[index] + 1]
                            .floats[n + 1] * factors[index];
                    }

                    irLeftFreqComplex[n] += (sourceReal * receiverLeftReal - sourceImag * receiverLeftImag);
                    irLeftFreqComplex[n + 1] += (sourceReal * receiverLeftImag + sourceImag * receiverLeftReal);
                    irRightFreqComplex[n] += (sourceReal * receiverRightReal - sourceImag * receiverRightImag);
                    irRightFreqComplex[n + 1] += (sourceReal * receiverRightImag + sourceImag * receiverRightReal);
                }
            }
            sw.Stop();
            Debug.Log("Run TestMethod2OfMultiplySample in " + sw.ElapsedMilliseconds/1000f +"ms");
        }

        private FloatArrays[] FloatArray(int length,int lengthFilter=44100)
        {
            var floatArray = new FloatArrays[length];
            for (int i = 0; i < floatArray.Length; i++)
            {
                floatArray[i] = new FloatArrays(GenerateRandomArray(lengthFilter));
            }

            return floatArray;
        }

        private float[] GenerateRandomArray(int length,float min=0,float max=1)
        {
            var test2 = new float[length]; 
            for (var i = 0; i < length; i++)
            {
                test2[i] = Random.Range(0f,1f);
            }

            return test2;
        }
    }
}
