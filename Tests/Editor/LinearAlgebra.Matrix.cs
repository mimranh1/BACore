﻿// using System;
// using BA.BACore;
// using NUnit.Framework;
//
// namespace Tests
// {
//     public class MatrixTests
//     {
//         #region Constructor Tests
//         /// <summary>
//         /// Test the Matrix constructor using two ints to define dimensions.
//         /// </summary>
//         [Test]
//         public void ShouldCreateA10x12Matrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(10, 12);
//             Assert.AreEqual(10, m1.Dimensions[0]);
//             Assert.AreEqual(12, m1.Dimensions[1]);
//             Assert.AreEqual(10, m1.Rows);
//             Assert.AreEqual(12, m1.Columns);
//             Assert.IsFalse(m1.IsSquare);
//         }
//
//         /// <summary>
//         /// Test the Matrix constructor using one int to define square dimensions.
//         /// </summary>
//         [Test]
//         public void ShouldCreateA20x20Matrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(20);
//             Assert.AreEqual(20, m1.Dimensions[0]);
//             Assert.AreEqual(20, m1.Dimensions[1]);
//             Assert.AreEqual(20, m1.Rows);
//             Assert.AreEqual(20, m1.Columns);
//             Assert.IsTrue(m1.IsSquare);
//         }
//
//         /// <summary>
//         /// Test the Matrix constructor by passing a multidimensional array.
//         /// </summary>
//         [Test]
//         public void ShouldCreateA2x3Matrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             Assert.AreEqual(2, m1.Dimensions[0]);
//             Assert.AreEqual(3, m1.Dimensions[1]);
//             Assert.AreEqual(2, m1.Rows);
//             Assert.AreEqual(3, m1.Columns);
//             Assert.IsFalse(m1.IsSquare);
//         }
//         #endregion
//
//         #region Property Tests
//         /// <summary>
//         /// Test transposing a Matrix (i.e. columns become rows)
//         /// </summary>
//         [Test]
//         public void TransposeMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//
//             MatrixDouble m2 = m1.Transpose;
//
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0, 4.0 },
//                 { 2.0, 5.0 },
//                 { 3.0, 6.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m2);
//
//         }
//
//         [Test]
//         public void Inverse2x2Matrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,]
//             {
//                 { 1.0, 2.0 },
//                 { 3.0, 4.0 }
//             });
//
//             MatrixDouble m2 = m1.Inverse;
//
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { -2.0, 1.0 },
//                 { 1.5, -0.5 }
//             });
//
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [Test]
//         public void Inverse3x3Matrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,]
//             {
//                 { 1.0, 2.0, 3.0 },
//                 { 0.0, 1.0, 4.0 },
//                 { 5.0, 6.0, 0.0 }
//             });
//
//             MatrixDouble m2 = m1.Inverse;
//
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 {-24.0, 18.0, 5.0 },
//                 { 20.0,-15.0,-4.0 },
//                 { -5.0,  4.0, 1.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [Test]
//         public void InverseTricky3x3Matrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,]
//             {
//                 { 1.0, 0.0, 1.0 },
//                 { 2.0, 0.0, 4.0 },
//                 { 0.0, 1.0, 1.0 }
//             });
//
//             MatrixDouble m2 = m1.Inverse;
//
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 2.0,-0.5, 0.0 },
//                 { 1.0,-0.5, 1.0 },
//                 {-1.0, 0.5, 0.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [Test]
//         public void Inverse3x3Identity()
//         {
//             MatrixDouble m1 = MatrixDouble.Identity(3);
//
//             MatrixDouble m2 = m1.Inverse;
//
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0, 0.0, 0.0 },
//                 { 0.0, 1.0, 0.0 },
//                 { 0.0, 0.0, 1.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [Test]
//         [ExpectedException(typeof(NonInvertibleMatrixException), "Matrix is not invertible.")]
//         public void NonInvertibleMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,]
//             {
//                 { 1.0, 0.0, 1.0 },
//                 { 2.0, 0.0, 4.0 },
//                 { 5.0, 0.0, 6.0 }
//             });
//
//             MatrixDouble m2 = m1.Inverse;
//         }
//
//         [Test]
//         [ExpectedException(typeof(InvalidMatrixDimensionsException), "Non-square Matrix is not invertible.")]
//         public void InvertingNonSquareMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,]
//             {
//                 { 2.0, 0.0, 4.0 },
//                 { 5.0, 0.0, 6.0 }
//             });
//
//             MatrixDouble m2 = m1.Inverse;
//         }
//
//         [Test]
//         public void SumOfAllElementsInAMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,]
//             {
//                 { 1.0, 2.0, 3.0 }, //6
//                 { 4.0, 5.0, 6.0 }, //15
//                 { 7.0, 8.0, 9.0 }  //24
//             });
//             double output = m1.SumAllElements;
//
//             double expectedValue = 45;
//             Assert.AreEqual(expectedValue, output);
//         }
//
//         [Test]
//         public void UnrollMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,]
//             {
//                 { 2.0, 0.0, 4.0 },
//                 { 5.0, 0.0, 6.0 }
//             });
//             MatrixDouble m2 = m1.Unrolled;
//             MatrixDouble expectedResult = new MatrixDouble(new double[,]
//             {
//                 { 2.0 },
//                 { 5.0 },
//                 { 0.0 },
//                 { 0.0 },
//                 { 4.0 },
//                 { 6.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//         #endregion
//
//         #region Creation methods
//         /// <summary>
//         /// Test creating a 1x1 identity Matrix
//         /// </summary>
//         [Test]
//         public void Create1x1IdentityMatrix()
//         {
//             MatrixDouble m1 = MatrixDouble.Identity(1);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m1);
//
//         }
//
//         /// <summary>
//         /// Test creating a 2x2 identity Matrix (i.e. diagonal values are 1, rest is zero)
//         /// </summary>
//         [Test]
//         public void Create2x2IdentityMatrix()
//         {
//             MatrixDouble m1 = MatrixDouble.Identity(2);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0, 0.0 },
//                 { 0.0, 1.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m1);
//
//         }
//
//         /// <summary>
//         /// Test creating a 4x4 identity Matrix (i.e. diagonal values are 1, rest is zero)
//         /// </summary>
//         [Test]
//         public void Create4x4IdentityMatrix()
//         {
//             MatrixDouble m1 = MatrixDouble.Identity(4);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0, 0.0, 0.0, 0.0 },
//                 { 0.0, 1.0, 0.0, 0.0 },
//                 { 0.0, 0.0, 1.0, 0.0 },
//                 { 0.0, 0.0, 0.0, 1.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m1);
//
//         }
//
//         /// <summary>
//         /// Test creating a 4x4 Matrix filled with ones
//         /// </summary>
//         [Test]
//         public void Create3x4MatrixFilledWithOnes()
//         {
//             MatrixDouble m1 = MatrixDouble.Ones(3, 4);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0, 1.0, 1.0, 1.0 },
//                 { 1.0, 1.0, 1.0, 1.0 },
//                 { 1.0, 1.0, 1.0, 1.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m1);
//         }
//
//         /// <summary>
//         /// Test creating a 4x4 Matrix filled with ones
//         /// </summary>
//         [Test]
//         public void Create4x4MatrixFilledWithOnes()
//         {
//             MatrixDouble m1 = MatrixDouble.Ones(4);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0, 1.0, 1.0, 1.0 },
//                 { 1.0, 1.0, 1.0, 1.0 },
//                 { 1.0, 1.0, 1.0, 1.0 },
//                 { 1.0, 1.0, 1.0, 1.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m1);
//         }
//
//         #region Magic Square
//         [TestCategory("Matrix: Magic Square"), Test]
//         public void Create1x1MagicSquare()
//         {
//             MatrixDouble m1 = MatrixDouble.Magic(1);
//             MatrixDouble expectedResult = MatrixDouble.Identity(1);
//             Assert.AreEqual(expectedResult, m1);
//         }
//
//         [TestCategory("Matrix: Magic Square"), Test]
//         [ExpectedException(typeof(InvalidMatrixDimensionsException), "Cannot create a Magic Square of dimension 2.")]
//         public void Create2x2MagicSquare()
//         {
//             MatrixDouble m1 = MatrixDouble.Magic(2);
//         }
//
//         [TestCategory("Matrix: Magic Square"), Test]
//         public void Create3x3MagicSquare()
//         {
//             MatrixDouble m1 = MatrixDouble.Magic(3);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 8.0, 1.0, 6.0 },
//                 { 3.0, 5.0, 7.0 },
//                 { 4.0, 9.0, 2.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m1);
//         }
//
//         [TestCategory("Matrix: Magic Square"), Test]
//         public void Create4x4MagicSquare()
//         {
//             MatrixDouble m1 = MatrixDouble.Magic(4);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0, 15.0, 14.0, 4.0 },
//                 { 12.0, 6.0, 7.0, 9.0 },
//                 { 8.0, 10.0, 11.0, 5.0 },
//                 { 13.0, 3.0, 2.0, 16.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m1);
//         }
//
//         [TestCategory("Matrix: Magic Square"), Test]
//         public void Create5x5MagicSquare()
//         {
//             MatrixDouble m1 = MatrixDouble.Magic(5);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 17.0, 24.0, 1.0, 8.0, 15.0 },
//                 { 23.0, 5.0, 7.0, 14.0, 16.0 },
//                 { 4.0, 6.0, 13.0, 20.0, 22.0 },
//                 { 10.0, 12.0, 19.0, 21.0, 3.0 },
//                 { 11.0, 18.0, 25.0, 2.0, 9.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m1);
//         }
//
//         [TestCategory("Matrix: Magic Square"), Test]
//         public void Create6x6MagicSquare()
//         {
//             MatrixDouble m1 = MatrixDouble.Magic(6);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 35.0, 1.0, 6.0, 26.0, 19.0, 24.0 },
//                 { 3.0, 32.0, 7.0, 21.0, 23.0, 25.0 },
//                 { 31.0, 9.0, 2.0, 22.0, 27.0, 20.0 },
//                 { 8.0, 28.0, 33.0, 17.0, 10.0, 15.0 },
//                 { 30.0, 5.0, 34.0, 12.0, 14.0, 16.0 },
//                 { 4.0, 36.0, 29.0, 13.0, 18.0, 11.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m1);
//         }
//
//         [TestCategory("Matrix: Magic Square"), Test]
//         public void Create8x8MagicSquare()
//         {
//             MatrixDouble m1 = MatrixDouble.Magic(8);
//             Assert.IsTrue(m1.IsMagic);
//         }
//
//         [TestCategory("Matrix: Magic Square"), Test]
//         public void Create9x9MagicSquare()
//         {
//             MatrixDouble m1 = MatrixDouble.Magic(9);
//             Assert.IsTrue(m1.IsMagic);
//         }
//
//         [TestCategory("Matrix: Magic Square"), Test]
//         public void Create10x10MagicSquare()
//         {
//             MatrixDouble m1 = MatrixDouble.Magic(10);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 92.0, 99.0,  1.0,  8.0, 15.0, 67.0, 74.0, 51.0, 58.0, 40.0 },
//                 { 98.0, 80.0,  7.0, 14.0, 16.0, 73.0, 55.0, 57.0, 64.0, 41.0 },
//                 {  4.0, 81.0, 88.0, 20.0, 22.0, 54.0, 56.0, 63.0, 70.0, 47.0 },
//                 { 85.0, 87.0, 19.0, 21.0,  3.0, 60.0, 62.0, 69.0, 71.0, 28.0 },
//                 { 86.0, 93.0, 25.0,  2.0,  9.0, 61.0, 68.0, 75.0, 52.0, 34.0 },
//                 { 17.0, 24.0, 76.0, 83.0, 90.0, 42.0, 49.0, 26.0, 33.0, 65.0 },
//                 { 23.0,  5.0, 82.0, 89.0, 91.0, 48.0, 30.0, 32.0, 39.0, 66.0 },
//                 { 79.0,  6.0, 13.0, 95.0, 97.0, 29.0, 31.0, 38.0, 45.0, 72.0 },
//                 { 10.0, 12.0, 94.0, 96.0, 78.0, 35.0, 37.0, 44.0, 46.0, 53.0 },
//                 { 11.0, 18.0,100.0, 77.0, 84.0, 36.0, 43.0, 50.0, 27.0, 59.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m1);
//         }
//
//         [TestCategory("Matrix: Magic Square"), Test]
//         public void Create11x11MagicSquare()
//         {
//             MatrixDouble m1 = MatrixDouble.Magic(11);
//             Assert.IsTrue(m1.IsMagic);
//         }
//
//         [TestCategory("Matrix: Magic Square"), Test]
//         public void Create12x12MagicSquare()
//         {
//             MatrixDouble m1 = MatrixDouble.Magic(12);
//             Assert.IsTrue(m1.IsMagic);
//         }
//
//         [TestCategory("Matrix: Magic Square"), Test]
//         public void CheckIfNonSquareIsMagicSquare()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 15.0, 14.0, 4.0 },
//                 { 12.0, 6.0, 7.0, 9.0 },
//                 { 8.0, 10.0, 11.0, 5.0 }
//             });
//
//             Assert.IsFalse(m1.IsMagic);
//         }
//
//         [TestCategory("Matrix: Magic Square"), Test]
//         public void CheckIf1x1MatrixIsMagicSquare()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0 }
//             });
//
//             Assert.IsTrue(m1.IsMagic);
//         }
//
//         [TestCategory("Matrix: Magic Square"), Test]
//         public void CheckIf2x2MatrixIsMagicSquare()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0 },
//                 { 3.0, 4.0 }
//             });
//
//             Assert.IsFalse(m1.IsMagic);
//         }
//
//         [TestCategory("Matrix: Magic Square"), Test]
//         public void CheckIfRepeatingNumbersMatrixIsMagicSquare()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 15.0, 14.0, 1.0 },
//                 { 9.0, 6.0, 7.0, 9.0 },
//                 { 8.0, 10.0, 8.0, 5.0 },
//                 { 13.0, 0.0, 2.0, 16.0 }
//             });
//
//             Assert.IsFalse(m1.IsMagic);
//         }
//
//         [TestCategory("Matrix: Magic Square"), Test]
//         public void CheckIfMagicSquareIsMagicSquare()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 15.0, 14.0, 4.0 },
//                 { 12.0, 6.0, 7.0, 9.0 },
//                 { 8.0, 10.0, 11.0, 5.0 },
//                 { 13.0, 3.0, 2.0, 16.0 }
//             });
//
//             Assert.IsTrue(m1.IsMagic);
//         }
//
//         [TestCategory("Matrix: Magic Square"), Test]
//         public void CheckIfMagicRowsIsNotMagicSquare()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0, 4.0 },
//                 { 1.0, 2.0, 3.0, 4.0 },
//                 { 1.0, 2.0, 3.0, 4.0 },
//                 { 1.0, 2.0, 3.0, 4.0 }
//             });
//
//             Assert.IsFalse(m1.IsMagic);
//         }
//
//         [TestCategory("Matrix: Magic Square"), Test]
//         public void CheckIfNonMagicSquareIsMagicSquare()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0, 4.0 },
//                 { 5.0, 6.0, 7.0, 8.0 },
//                 { 9.0, 10.0, 11.0, 12.0 },
//                 { 13.0, 14.0, 15.0, 16.0 }
//             });
//
//             Assert.IsFalse(m1.IsMagic);
//         }
//         #endregion
//         #endregion
//
//         #region Operator Tests
//         #region Addition
//         [TestCategory("Matrix: Operator Overloads"), Test]
//         public void AddingTwoDefinedMatricesTogether()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble m2 = new MatrixDouble(new double[,] {
//                 { 7.0, 8.0, 9.0 },
//                 { 10.0, 11.0, 12.0 }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 8.0, 10.0, 12.0 },
//                 { 14.0, 16.0, 18.0 }
//             });
//
//             MatrixDouble m3 = m1 + m2; // 15,10,21 // 4,16,6
//             Assert.AreEqual(expectedResult, m3);
//         }
//
//         [TestCategory("Matrix: Operator Overloads"), Test]
//         [ExpectedException(typeof(NullReferenceException), "Cannot add a null Matrix.")]
//         public void AddingDefinedMatrixToNullMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble m2 = null;
//
//             MatrixDouble m3 = m1 + m2;
//         }
//
//         [TestCategory("Matrix: Operator Overloads"), Test]
//         [ExpectedException(typeof(InvalidMatrixDimensionsException), "Matrix dimensions must match.")]
//         public void AddingTwoUnevenDefinedMatricesTogether()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble m2 = new MatrixDouble(new double[,] {
//                 { 7.0, 8.0 },
//                 { 10.0, 11.0 }
//             });
//             MatrixDouble m3 = m1 + m2;
//         }
//
//         [TestCategory("Matrix: Operator Overloads"), Test]
//         public void AddingScalarAndMatrixTogether()
//         {
//             double number = 4.0;
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 5.0, 6.0, 7.0 },
//                 { 8.0, 9.0, 10.0 }
//             });
//
//             MatrixDouble m2 = number + m1;
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Operator Overloads"), Test]
//         public void AddingMatrixAndScalarTogether()
//         {
//             double number = 4.0;
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 5.0, 6.0, 7.0 },
//                 { 8.0, 9.0, 10.0 }
//             });
//
//             MatrixDouble m2 = m1 + number;
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         #endregion
//
//         #region Subtraction
//         [TestCategory("Matrix: Operator Overloads"), Test]
//         public void SubractingOneDefinedMatrixFromAnother()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble m2 = new MatrixDouble(new double[,] {
//                 { 7.0, 8.0, 9.0 },
//                 { 10.0, 11.0, 12.0 }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { -6.0, -6.0, -6.0 },
//                 { -6.0, -6.0, -6.0 }
//             });
//
//             MatrixDouble m3 = m1 - m2;
//             Assert.AreEqual(expectedResult, m3);
//         }
//
//         [TestCategory("Matrix: Operator Overloads"), Test]
//         [ExpectedException(typeof(NullReferenceException), "Cannot subtract a null Matrix.")]
//         public void SubtractingNullMatrixFromDefinedMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble m2 = null;
//
//             MatrixDouble m3 = m1 - m2;
//         }
//
//         [TestCategory("Matrix: Operator Overloads"), Test]
//         [ExpectedException(typeof(InvalidMatrixDimensionsException), "Matrix dimensions must match.")]
//         public void SubractingTwoUnevenDefinedMatrices()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble m2 = new MatrixDouble(new double[,] {
//                 { 7.0, 8.0 },
//                 { 10.0, 11.0 }
//             });
//             MatrixDouble m3 = m1 - m2;
//         }
//
//         [TestCategory("Matrix: Operator Overloads"), Test]
//         public void SubractingMatrixFromScalar()
//         {
//             double number = 10.0;
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 9.0, 8.0, 7.0 },
//                 { 6.0, 5.0, 4.0 }
//             });
//
//             MatrixDouble m2 = number - m1;
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Operator Overloads"), Test]
//         public void SubractingScalarFromMatrix()
//         {
//             double number = 10.0;
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { -9.0, -8.0, -7.0 },
//                 { -6.0, -5.0, -4.0 }
//             });
//
//             MatrixDouble m2 = m1 - number;
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         #endregion
//
//         #region Unary Operators
//         [TestCategory("Matrix: Operator Overloads"), Test]
//         public void NegatingAMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, -2.0, 3.0 },
//                 { 4.0, 5.0, -6.0 }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { -1.0, 2.0,-3.0 },
//                 { -4.0,-5.0, 6.0 }
//             });
//
//             MatrixDouble m2 = -m1;
//             Assert.AreEqual(expectedResult, m2);
//         }
//         #endregion
//
//         #region Multiplication
//         [TestCategory("Matrix: Operator Overloads"), Test]
//         public void MultiplyingTwoDefinedMatricesTogether()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 6.0, 3.0, 0.0 },
//                 { 2.0, 5.0, 1.0 },
//                 { 9.0, 8.0, 6.0 }
//             });
//             MatrixDouble m2 = new MatrixDouble(new double[,] {
//                 { 7.0, 4.0 },
//                 { 6.0, 7.0 },
//                 { 5.0, 0.0 }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 60.0, 45.0 },
//                 { 49.0, 43.0 },
//                 { 141.0, 92.0 }
//             });
//
//             MatrixDouble m3 = m1 * m2;
//             Assert.AreEqual(expectedResult, m3);
//         }
//
//         [TestCategory("Matrix: Operator Overloads"), Test]
//         [ExpectedException(typeof(NullReferenceException), "Cannot multiply a null Matrix.")]
//         public void MultiplyingDefinedMatrixWithNull()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 6.0, 3.0, 0.0 },
//                 { 2.0, 5.0, 1.0 },
//                 { 9.0, 8.0, 6.0 }
//             });
//             MatrixDouble m2 = null;
//
//             MatrixDouble m3 = m1 * m2;
//         }
//
//         [TestCategory("Matrix: Operator Overloads"), Test]
//         [ExpectedException(typeof(InvalidMatrixDimensionsException), "Matrix 1 column count must match matrix 2 row count.")]
//         public void MultiplyingTwoIncorrectDimensionMatricesTogether()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 6.0, 3.0, 0.0 },
//                 { 2.0, 5.0, 1.0 },
//                 { 9.0, 8.0, 6.0 }
//             });
//             MatrixDouble m2 = new MatrixDouble(new double[,] {
//                 { 7.0, 4.0 },
//                 { 5.0, 0.0 }
//             });
//
//             MatrixDouble m3 = m1 * m2;
//         }
//
//         [TestCategory("Matrix: Operator Overloads"), Test]
//         public void ScalarMultiplicationOfDefinedMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 6.0, 3.0, 0.0 },
//                 { 2.0, 5.0, 1.0 },
//                 { 9.0, 8.0, 6.0 }
//             });
//             double scalar = 2;
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 12.0, 6.0, 0.0 },
//                 { 4.0, 10.0, 2.0 },
//                 { 18.0, 16.0, 12.0 }
//             });
//
//             MatrixDouble m2 = scalar * m1;
//             MatrixDouble m3 = m1 * scalar;
//             Assert.AreEqual(expectedResult, m2);
//             Assert.AreEqual(expectedResult, m3);
//         }
//
//         [TestCategory("Matrix: Operator Overloads"), Test]
//         [ExpectedException(typeof(NullReferenceException), "Cannot multiply a null Matrix.")]
//         public void ScalarMultiplicationOfNullMatrix()
//         {
//             MatrixDouble m1 = null;
//             double scalar = 2;
//             MatrixDouble m2 = scalar * m1;
//         }
//
//         [TestCategory("Matrix: Operator Overloads"), Test]
//         public void MultiplyingMatrixAndVectorTogether()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 6.0, 3.0, 0.0 },
//                 { 2.0, 5.0, 1.0 }
//             });
//             MatrixDouble v1 = new MatrixDouble(new double[,] {
//                 { 7.0 },
//                 { 6.0 },
//                 { 5.0 }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 60.0 },
//                 { 49.0 }
//             });
//
//             MatrixDouble m2 = m1 * v1;
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [Test]
//         public void MultiplyByTransposeValidMatrices()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 6.0, 3.0, 0.0 },
//                 { 2.0, 5.0, 1.0 },
//                 { 9.0, 8.0, 6.0 }
//             });
//             MatrixDouble m2 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble m3 = MatrixDouble.MultiplyByTranspose(m1, m2);
//
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 12.0, 39.0 },
//                 { 15.0, 39.0 },
//                 { 43.0, 112.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m3);
//         }
//
//         [Test]
//         public void MultiplyByTransposeSameSquareMatrixTwoParameters()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 6.0, 3.0, 0.0 },
//                 { 2.0, 5.0, 1.0 },
//                 { 9.0, 8.0, 6.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.MultiplyByTranspose(m1, m1);
//
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 45.0, 27.0, 78.0 },
//                 { 27.0, 30.0, 64.0 },
//                 { 78.0, 64.0,181.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [Test]
//         public void MultiplyTransposeByValidMatrices()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 6.0, 3.0 },
//                 { 2.0, 5.0 },
//                 { 9.0, 8.0 }
//             });
//             MatrixDouble m2 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 },
//                 { 7.0, 8.0, 9.0 }
//             });
//             MatrixDouble m3 = MatrixDouble.MultiplyTransposeBy(m1, m2);
//
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 77, 94, 111 },
//                 { 79, 95, 111 }
//             });
//
//             Assert.AreEqual(expectedResult, m3);
//         }
//
//         [Test]
//         public void MultiplyByTransposeSameMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 6.0, 3.0 },
//                 { 2.0, 5.0 },
//                 { 9.0, 8.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.MultiplyByTranspose(m1);
//
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 45.0, 27.0, 78.0 },
//                 { 27.0, 29.0, 58.0 },
//                 { 78.0, 58.0,145.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [Test]
//         public void MultiplyTransposeBySameMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 6.0, 3.0, 0.0 },
//                 { 9.0, 8.0, 6.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.MultiplyTransposeBy(m1);
//
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 {117.0, 90.0, 54.0 },
//                 { 90.0, 73.0, 48.0 },
//                 { 54.0, 48.0, 36.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [Test]
//         public void MultiplyTransposeBySameMatrixTwoParameters()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 6.0, 3.0, 0.0 },
//                 { 9.0, 8.0, 6.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.MultiplyTransposeBy(m1, m1);
//
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 {117.0, 90.0, 54.0 },
//                 { 90.0, 73.0, 48.0 },
//                 { 54.0, 48.0, 36.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [Test]
//         public void MultiplyTransposeBySameMatrixTwoInstancesTwoParameters()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 6.0, 3.0, 0.0 },
//                 { 9.0, 8.0, 6.0 }
//             });
//             MatrixDouble m2 = new MatrixDouble(new double[,] {
//                 { 6.0, 3.0, 0.0 },
//                 { 9.0, 8.0, 6.0 }
//             });
//             MatrixDouble m3 = MatrixDouble.MultiplyTransposeBy(m1, m2);
//
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 {117.0, 90.0, 54.0 },
//                 { 90.0, 73.0, 48.0 },
//                 { 54.0, 48.0, 36.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m3);
//         }
//
//         [Test]
//         public void MultiplyTransposeBySameSquareMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 6.0, 3.0, 0.0 },
//                 { 2.0, 5.0, 1.0 },
//                 { 9.0, 8.0, 6.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.MultiplyTransposeBy(m1);
//
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 {121.0,100.0, 56.0 },
//                 {100.0, 98.0, 53.0 },
//                 { 56.0, 53.0, 37.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [Test]
//         public void MultiplyTransposeBySameSquareMatrixTwoParameters()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 6.0, 3.0, 0.0 },
//                 { 2.0, 5.0, 1.0 },
//                 { 9.0, 8.0, 6.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.MultiplyTransposeBy(m1, m1);
//
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 {121.0,100.0, 56.0 },
//                 {100.0, 98.0, 53.0 },
//                 { 56.0, 53.0, 37.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [Test]
//         public void MultiplyTransposeBySameSquareMatrixTwoInstancesTwoParameters()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 6.0, 3.0, 0.0 },
//                 { 2.0, 5.0, 1.0 },
//                 { 9.0, 8.0, 6.0 }
//             });
//             MatrixDouble m2 = new MatrixDouble(new double[,] {
//                 { 6.0, 3.0, 0.0 },
//                 { 2.0, 5.0, 1.0 },
//                 { 9.0, 8.0, 6.0 }
//             });
//             MatrixDouble m3 = MatrixDouble.MultiplyTransposeBy(m1, m2);
//
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 {121.0,100.0, 56.0 },
//                 {100.0, 98.0, 53.0 },
//                 { 56.0, 53.0, 37.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m3);
//         }
//
//         [Test]
//         [ExpectedException(typeof(InvalidMatrixDimensionsException), "Cannot multiply matrices of these dimensions.")]
//         public void MultiplyTransposeInvalidMatrices()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 6.0, 3.0, 0.0 },
//                 { 2.0, 5.0, 1.0 },
//                 { 9.0, 8.0, 6.0 }
//             });
//             MatrixDouble m2 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0 },
//                 { 4.0, 5.0 }
//             });
//             MatrixDouble m3 = MatrixDouble.MultiplyByTranspose(m1, m2);
//         }
//         #endregion
//
//         #region Division
//         [TestCategory("Matrix: Operator Overloads"), Test]
//         public void DivideScalarByMatrix()
//         {
//             double number = 12.0;
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 2.0, 3.0, 4.0 },
//                 { 6.0, 10.0, 12.0 }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 6.0, 4.0, 3.0 },
//                 { 2.0, 1.2, 1.0 }
//             });
//
//             MatrixDouble m2 = number / m1;
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Operator Overloads"), Test]
//         public void DivideMatrixByScalar()
//         {
//             double number = 4.0;
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 8.0, 12.0, 16.0 },
//                 { 20.0, 24.0, 28.0 }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 2.0, 3.0, 4.0 },
//                 { 5.0, 6.0, 7.0 }
//             });
//
//             MatrixDouble m2 = m1 / number;
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         #endregion
//
//         #region Equality
//         [TestCategory("Matrix: Operator Overloads"), Test]
//         public void EqualityOperatorOnTwoMatrices()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble m2 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//
//             Assert.IsTrue(m1 == m2);
//         }
//         #endregion
//
//         #region Comparisons
//         [TestCategory("Matrix: Operator Overloads"), Test]
//         public void ElementsInMatrixEqualToScalar()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble m2 = m1 == 3;
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 0.0, 0.0, 1.0 },
//                 { 0.0, 0.0, 0.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Operator Overloads"), Test]
//         public void ElementsInMatrixNotEqualToScalar()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble m2 = m1 != 3;
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0, 1.0, 0.0 },
//                 { 1.0, 1.0, 1.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Operator Overloads"), Test]
//         public void ElementsInMatrixLessThanScalar()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble m2 = m1 < 3;
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0, 1.0, 0.0 },
//                 { 0.0, 0.0, 0.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Operator Overloads"), Test]
//         public void ElementsInMatrixLessThanOrEqualToScalar()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble m2 = m1 <= 3;
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0, 1.0, 1.0 },
//                 { 0.0, 0.0, 0.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Operator Overloads"), Test]
//         public void ElementsInMatrixGreaterThanScalar()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble m2 = m1 > 3;
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 0.0, 0.0, 0.0 },
//                 { 1.0, 1.0, 1.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Operator Overloads"), Test]
//         public void ElementsInMatrixGreaterThanOrEqualToScalar()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble m2 = m1 >= 3;
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 0.0, 0.0, 1.0 },
//                 { 1.0, 1.0, 1.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m2);
//         }
//         #endregion
//
//         #region Element Operations (scalar)
//         /// <summary>
//         /// Test adding 5 to each matrix element
//         /// </summary>
//         [TestCategory("Matrix: Element Operations"), Test]
//         public void ElementAddFiveToMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 6.0, 7.0, 8.0 },
//                 { 9.0, 10.0, 11.0 }
//             });
//
//             MatrixDouble m2 = MatrixDouble.ElementAdd(m1, 5);
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         /// <summary>
//         /// Test subtracting five from each matrix element
//         /// </summary>
//         [TestCategory("Matrix: Element Operations"), Test]
//         public void ElementSubtractFiveFromMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 6.0, 7.0, 8.0 },
//                 { 9.0, 10.0, 11.0 }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//
//             MatrixDouble m2 = MatrixDouble.ElementSubtract(m1, 5);
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         /// <summary>
//         /// Test multiplying each matrix element by 3
//         /// </summary>
//         [TestCategory("Matrix: Element Operations"), Test]
//         public void ElementMultiplyMatrixByThree()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 3.0, 6.0, 9.0 },
//                 { 12.0, 15.0, 18.0 }
//             });
//
//             MatrixDouble m2 = MatrixDouble.ElementMultiply(m1, 3);
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         /// <summary>
//         /// Test dividing each matrix element by 10
//         /// </summary>
//         [TestCategory("Matrix: Element Operations"), Test]
//         public void ElementDivideMatrixByTen()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 0.1, 0.2, 0.3 },
//                 { 0.4, 0.5, 0.6 }
//             });
//
//             MatrixDouble m2 = MatrixDouble.ElementDivide(m1, 10);
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         /// <summary>
//         /// Test raising the power of each element by 2
//         /// </summary>
//         [TestCategory("Matrix: Element Operations"), Test]
//         public void ElementSquareMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0, 4.0, 9.0 },
//                 { 16.0, 25.0, 36.0 }
//             });
//
//             MatrixDouble m2 = MatrixDouble.ElementPower(m1, 2);
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         /// <summary>
//         /// Test getting the square root of each matrix element.
//         /// </summary>
//         [TestCategory("Matrix: Element Operations"), Test]
//         public void ElementSqrtMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 4.0, 9.0, 16.0 },
//                 { 25.0, 36.0, 49.0 }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 2.0, 3.0, 4.0 },
//                 { 5.0, 6.0, 7.0 }
//             });
//
//             MatrixDouble m2 = MatrixDouble.ElementSqrt(m1);
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         /// <summary>
//         /// Test getting the absolute value of each matrix element.
//         /// </summary>
//         [TestCategory("Matrix: Element Operations"), Test]
//         public void ElementAbsMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, -2.0, 3.0 },
//                 { -4.0, 5.0, -6.0 }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//
//             MatrixDouble m2 = MatrixDouble.ElementAbs(m1);
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         /// <summary>
//         /// Test getting the absolute value of each matrix element.
//         /// </summary>
//         [TestCategory("Matrix: Element Operations"), Test]
//         public void ElementExpMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 0.0, 1.0 }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0, Math.E }
//             });
//
//             MatrixDouble m2 = MatrixDouble.ElementExp(m1);
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         /// <summary>
//         /// Test getting the natural logarithm value of each matrix element.
//         /// </summary>
//         [TestCategory("Matrix: Element Operations"), Test]
//         public void ElementLogMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, Math.E }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 0.0, 1.0 }
//             });
//
//             MatrixDouble m2 = MatrixDouble.ElementLog(m1);
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         #endregion
//
//         #region Element Operations (matrix)
//         /// <summary>
//         /// Test adding two Matrix objects together
//         /// </summary>
//         [TestCategory("Matrix: Element Operations"), Test]
//         public void ElementAddMatrixToMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble m2 = new MatrixDouble(new double[,] {
//                 { 7.0, 8.0, 9.0 },
//                 { 10.0, 11.0, 12.0 }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 8.0, 10.0, 12.0 },
//                 { 14.0, 16.0, 18.0 }
//             });
//
//             MatrixDouble m3 = MatrixDouble.ElementAdd(m1, m2);
//             Assert.AreEqual(expectedResult, m3);
//         }
//
//         /// <summary>
//         /// Test adding Matrix and Vector objects together
//         /// </summary>
//         [TestCategory("Matrix: Element Operations"), Test]
//         public void ElementAddVectorToMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble m2 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 2.0, 4.0, 6.0 },
//                 { 5.0, 7.0, 9.0 }
//             });
//
//             MatrixDouble m3 = MatrixDouble.ElementAdd(m1, m2);
//             Assert.AreEqual(expectedResult, m3);
//         }
//
//         /// <summary>
//         /// Test adding Matrix and Vector objects together
//         /// </summary>
//         [TestCategory("Matrix: Element Operations"), Test]
//         public void ElementAddRowVectorToMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble m2 = new MatrixDouble(new double[,] {
//                 { 1.0 },
//                 { 2.0 }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 2.0, 3.0, 4.0 },
//                 { 6.0, 7.0, 8.0 }
//             });
//
//             MatrixDouble m3 = MatrixDouble.ElementAdd(m1, m2);
//             Assert.AreEqual(expectedResult, m3);
//         }
//
//
//         /// <summary>
//         /// Test subtracting one Matrix from another
//         /// </summary>
//         [TestCategory("Matrix: Element Operations"), Test]
//         public void ElementSubtractMatrixFromMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 7.0, 8.0, 9.0 },
//                 { 10.0, 11.0, 12.0 }
//             });
//             MatrixDouble m2 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 6.0, 6.0, 6.0 },
//                 { 6.0, 6.0, 6.0 }
//             });
//
//             MatrixDouble m3 = MatrixDouble.ElementSubtract(m1, m2);
//             Assert.AreEqual(expectedResult, m3);
//         }
//
//         /// <summary>
//         /// Test subtracting a vector from a Matrix
//         /// </summary>
//         [TestCategory("Matrix: Element Operations"), Test]
//         public void ElementSubtractVectorFromMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 7.0, 8.0, 9.0 },
//                 { 10.0, 11.0, 12.0 }
//             });
//             MatrixDouble m2 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 6.0, 6.0, 6.0 },
//                 { 9.0, 9.0, 9.0 }
//             });
//
//             MatrixDouble m3 = MatrixDouble.ElementSubtract(m1, m2);
//             Assert.AreEqual(expectedResult, m3);
//         }
//
//         /// <summary>
//         /// Test subtracting a vector from a Matrix
//         /// </summary>
//         [TestCategory("Matrix: Element Operations"), Test]
//         public void ElementSubtractRowVectorFromMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 7.0, 8.0, 9.0 },
//                 { 10.0, 11.0, 12.0 }
//             });
//             MatrixDouble m2 = new MatrixDouble(new double[,] {
//                 { 1.0 },
//                 { 2.0 }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 6.0, 7.0, 8.0 },
//                 { 8.0, 9.0, 10.0 }
//             });
//
//             MatrixDouble m3 = MatrixDouble.ElementSubtract(m1, m2);
//             Assert.AreEqual(expectedResult, m3);
//         }
//
//         /// <summary>
//         /// Test multiplying each matrix element together
//         /// </summary>
//         [TestCategory("Matrix: Element Operations"), Test]
//         public void ElementMultiplyMatrixByMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble m2 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 1.0 },
//                 { 2.0, 1.0, 2.0 }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0, 4.0, 3.0 },
//                 { 8.0, 5.0, 12.0 }
//             });
//
//             MatrixDouble m3 = MatrixDouble.ElementMultiply(m1, m2);
//             Assert.AreEqual(expectedResult, m3);
//         }
//
//         /// <summary>
//         /// Test multiplying a Matrix by a vector.
//         /// </summary>
//         [TestCategory("Matrix: Element Operations"), Test]
//         public void ElementMultiplyMatrixByVector()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble m2 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 1.0 },
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0, 4.0, 3.0 },
//                 { 4.0, 10.0, 6.0 }
//             });
//
//             MatrixDouble m3 = MatrixDouble.ElementMultiply(m1, m2);
//             Assert.AreEqual(expectedResult, m3);
//         }
//
//         /// <summary>
//         /// Test multiplying a Matrix by a vector.
//         /// </summary>
//         [TestCategory("Matrix: Element Operations"), Test]
//         public void ElementMultiplyMatrixByRowVector()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble m2 = new MatrixDouble(new double[,] {
//                 { 1.0 },
//                 { 2.0 }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 8.0, 10.0, 12.0 }
//             });
//
//             MatrixDouble m3 = MatrixDouble.ElementMultiply(m1, m2);
//             Assert.AreEqual(expectedResult, m3);
//         }
//
//         /// <summary>
//         /// Test dividing one Matrix by another
//         /// </summary>
//         [TestCategory("Matrix: Element Operations"), Test]
//         public void ElementDivideMatrixByMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 10.0, 2.0, 4.0 },
//                 { 6.0, 8.0, 2.0 }
//             });
//             MatrixDouble m2 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 1.0 },
//                 { 2.0, 1.0, 2.0 }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 10.0, 1.0, 4.0 },
//                 { 3.0, 8.0, 1.0 }
//             });
//
//             MatrixDouble m3 = MatrixDouble.ElementDivide(m1, m2);
//             Assert.AreEqual(expectedResult, m3);
//         }
//
//         /// <summary>
//         /// Test dividing a Matrix by a vector
//         /// </summary>
//         [TestCategory("Matrix: Element Operations"), Test]
//         public void ElementDivideMatrixByVector()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 10.0, 2.0, 4.0 },
//                 { 6.0, 8.0, 2.0 }
//             });
//             MatrixDouble m2 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 1.0 }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 10.0, 1.0, 4.0 },
//                 { 6.0, 4.0, 2.0 }
//             });
//
//             MatrixDouble m3 = MatrixDouble.ElementDivide(m1, m2);
//             Assert.AreEqual(expectedResult, m3);
//         }
//
//         /// <summary>
//         /// Test dividing a Matrix by a vector
//         /// </summary>
//         [TestCategory("Matrix: Element Operations"), Test]
//         public void ElementDivideMatrixByRowVector()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 10.0, 2.0, 4.0 },
//                 { 6.0, 8.0, 2.0 }
//             });
//             MatrixDouble m2 = new MatrixDouble(new double[,] {
//                 { 1.0 },
//                 { 2.0 }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 10.0, 2.0, 4.0 },
//                 { 3.0, 4.0, 1.0 }
//             });
//
//             MatrixDouble m3 = MatrixDouble.ElementDivide(m1, m2);
//             Assert.AreEqual(expectedResult, m3);
//         }
//
//         /// <summary>
//         /// Test that a null Matrix will throw a NullReferenceException exception.
//         /// </summary>
//         [TestCategory("Matrix: Element Operations"), Test]
//         [ExpectedException(typeof(NullReferenceException), "Cannot add a null Matrix.")]
//         public void ElementMultiplyMatrixByNullMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 7.0, 8.0, 9.0 },
//                 { 10.0, 11.0, 12.0 }
//             });
//             MatrixDouble m2 = null;
//             MatrixDouble m3 = m1 * m2;
//         }
//
//         /// <summary>
//         /// Test that two different sized Matrix objects will throw an
//         /// InvalidMatrixDimensionsException exception.
//         /// </summary>
//         [TestCategory("Matrix: Element Operations"), Test]
//         [ExpectedException(typeof(InvalidMatrixDimensionsException), "Matrix dimensions must match.")]
//         public void ElementMultiplyMatrixByNonMatchingMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 7.0, 8.0, 9.0 },
//                 { 10.0, 11.0, 12.0 }
//             });
//             MatrixDouble m2 = new MatrixDouble(new double[,] {
//                 { 7.0, 8.0 },
//                 { 10.0, 11.0 }
//             });
//             MatrixDouble m3 = m1 * m2;
//         }
//         #endregion
//
//         #region Dimension Operations
//         [Test]
//         public void SumOfMatrixColumns()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 5.0, 7.0, 9.0 }
//             });
//
//             MatrixDouble m2 = MatrixDouble.Sum(m1);
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [Test]
//         public void SumOfMatrixRows()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 6.0 },
//                 { 15.0 }
//             });
//
//             MatrixDouble m2 = MatrixDouble.Sum(m1, MatrixDimensions.Rows);
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         #endregion
//
//         #region Statistical Operations
//         #region Mean
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetMeanColumnsRectangularMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Mean(m1, MatrixDimensions.Columns);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 2.5, 3.5, 4.5 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetMeanRowsRectangularMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Mean(m1, MatrixDimensions.Rows);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 2.0 },
//                 { 5.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetMeanColumnsVector()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Mean(m1);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 2.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetMeanRowsVector()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0 },
//                 { 2.0 },
//                 { 3.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Mean(m1);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 2.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//         #endregion
//
//         #region Mean Square
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetMeanSquareColumnsRectangularMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.MeanSquare(m1, MatrixDimensions.Columns);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 8.5, 14.5, 22.5 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetMeanSquareRowsRectangularMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0, 4.0 },
//                 { 4.0, 5.0, 6.0, 7.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.MeanSquare(m1, MatrixDimensions.Rows);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 7.5 },
//                 { 31.5 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetMeanSquareColumnsVector()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0, 4.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.MeanSquare(m1);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 7.5 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetMeanSquareRowsVector()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0 },
//                 { 2.0 },
//                 { 3.0 },
//                 { 4.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.MeanSquare(m1);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 7.5 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//         #endregion
//
//         #region Max
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetMaxColumnsRectangularMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 5.0, 3.0 },
//                 { 4.0, 2.0, 6.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Max(m1, MatrixDimensions.Columns);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 4.0, 5.0, 6.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetMaxRowsRectangularMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Max(m1, MatrixDimensions.Rows);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 3.0 },
//                 { 6.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetMaxColumnsVector()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Max(m1);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 3.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetMaxRowsVector()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0 },
//                 { 2.0 },
//                 { 3.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Max(m1);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 3.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//         #endregion
//
//         #region Min
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetMinColumnsRectangularMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 5.0, 3.0 },
//                 { 4.0, 2.0, 6.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Min(m1, MatrixDimensions.Columns);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetMinRowsRectangularMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Min(m1, MatrixDimensions.Rows);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0 },
//                 { 4.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetMinColumnsVector()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Min(m1);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetMinRowsVector()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0 },
//                 { 2.0 },
//                 { 3.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Min(m1);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//         #endregion
//
//         #region MaxIndex
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetMaxIndexColumnsRectangularMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 5.0, 3.0 },
//                 { 4.0, 2.0, 6.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.MaxIndex(m1, MatrixDimensions.Columns);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0, 0.0, 1.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetMaxIndexRowsRectangularMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.MaxIndex(m1, MatrixDimensions.Rows);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 2.0 },
//                 { 2.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetMaxIndexColumnsVector()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.MaxIndex(m1);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 2.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetMaxIndexRowsVector()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0 },
//                 { 2.0 },
//                 { 3.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.MaxIndex(m1);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 2.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//         #endregion
//
//         #region MinIndex
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetMinIndexColumnsRectangularMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 5.0, 3.0 },
//                 { 4.0, 2.0, 6.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.MinIndex(m1, MatrixDimensions.Columns);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 0.0, 1.0, 0.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetMinIndexRowsRectangularMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.MinIndex(m1, MatrixDimensions.Rows);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 0.0 },
//                 { 0.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetMinIndexColumnsVector()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.MinIndex(m1);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 0.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetMinIndexRowsVector()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0 },
//                 { 2.0 },
//                 { 3.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.MinIndex(m1);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 0.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//         #endregion
//
//
//         #region Range
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetRangeColumnsRectangularMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 5.0, 3.0 },
//                 { 4.0, 2.0, 6.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Range(m1, MatrixDimensions.Columns);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 3.0, 3.0, 3.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetRangeRowsRectangularMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Range(m1, MatrixDimensions.Rows);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 2.0 },
//                 { 2.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetRangeColumnsVector()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Range(m1);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 2.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetRangeRowsVector()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0 },
//                 { 2.0 },
//                 { 3.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Range(m1);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 2.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//         #endregion
//
//         #region Median
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetMedianColumnsRectangularMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 5.0, 3.0 },
//                 { 4.0, 2.0, 6.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Median(m1, MatrixDimensions.Columns);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 2.5, 3.5, 4.5 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetMedianRowsRectangularMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Median(m1, MatrixDimensions.Rows);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 2.0 },
//                 { 5.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetMedianColumnsVector()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 2.0, 4.0, 3.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Median(m1);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 3.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetMedianRowsVector()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 6.0 },
//                 { 2.0 },
//                 { 3.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Median(m1);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 3.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//         #endregion
//
//         #region Quartile 1
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetQuartile1ColumnsFourRowsMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 5.0, 3.0 },
//                 { 4.0, 2.0, 6.0 },
//                 { 7.0, 8.0, 9.0 },
//                 { 10.0, 11.0, 12.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Quartile1(m1, MatrixDimensions.Columns);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 2.5, 3.5, 4.5 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetQuartile1ColumnsSixRowsMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 5.0, 3.0 },
//                 { 4.0, 2.0, 6.0 },
//                 { 7.0, 8.0, 9.0 },
//                 { 10.0, 11.0, 12.0 },
//                 { 13.0, 14.0, 15.0 },
//                 { 16.0, 17.0, 18.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Quartile1(m1, MatrixDimensions.Columns);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 4, 5, 6 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetQuartile1ColumnsOddRowsMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 5.0, 3.0 },
//                 { 4.0, 2.0, 6.0 },
//                 { 7.0, 8.0, 9.0 },
//                 { 10.0, 11.0, 12.0 },
//                 { 14.0, 15.0, 16.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Quartile1(m1, MatrixDimensions.Columns);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 3.25, 4.25, 5.25 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetQuartile1SingleRowsMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 5.0, 3.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Quartile1(m1, MatrixDimensions.Auto);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.5 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetQuartile1TwoRowsMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 5.0, 3.0 },
//                 { 4.0, 2.0, 6.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Quartile1(m1, MatrixDimensions.Columns);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         #endregion
//
//         #region Quartile 3
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetQuartile3ColumnsFourRowsMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 5.0, 3.0 },
//                 { 4.0, 2.0, 6.0 },
//                 { 7.0, 8.0, 9.0 },
//                 { 10.0, 11.0, 12.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Quartile3(m1, MatrixDimensions.Columns);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 8.5, 9.5, 10.5 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetQuartile3ColumnsSixRowsMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 5.0, 3.0 },
//                 { 4.0, 2.0, 6.0 },
//                 { 7.0, 8.0, 9.0 },
//                 { 10.0, 11.0, 12.0 },
//                 { 13.0, 14.0, 15.0 },
//                 { 16.0, 17.0, 18.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Quartile3(m1, MatrixDimensions.Columns);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 13, 14, 15 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetQuartile3ColumnsOddRowsMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 5.0, 3.0 },
//                 { 4.0, 2.0, 6.0 },
//                 { 7.0, 8.0, 9.0 },
//                 { 10.0, 11.0, 12.0 },
//                 { 14.0, 15.0, 16.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Quartile3(m1, MatrixDimensions.Columns);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 11, 12, 13 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetQuartile3SingleRowsMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 5.0, 3.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Quartile3(m1, MatrixDimensions.Auto);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 4.5 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetQuartile3TwoRowsMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 5.0, 3.0 },
//                 { 4.0, 2.0, 6.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Quartile3(m1, MatrixDimensions.Columns);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 4.0, 5.0, 6.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         #endregion
//
//         #region Interquartile Range
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetIQRColumnsFourRowsMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 5.0, 3.0 },
//                 { 4.0, 2.0, 6.0 },
//                 { 7.0, 8.0, 9.0 },
//                 { 10.0, 11.0, 12.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.IQR(m1, MatrixDimensions.Columns);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 6.0, 6.0, 6.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetIQRColumnsSixRowsMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 5.0, 3.0 },
//                 { 4.0, 2.0, 6.0 },
//                 { 7.0, 8.0, 9.0 },
//                 { 10.0, 11.0, 12.0 },
//                 { 13.0, 14.0, 15.0 },
//                 { 16.0, 17.0, 18.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.IQR(m1, MatrixDimensions.Columns);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 9.0, 9.0, 9.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetIQRColumnsOddRowsMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 5.0, 3.0 },
//                 { 4.0, 2.0, 6.0 },
//                 { 7.0, 8.0, 9.0 },
//                 { 10.0, 11.0, 12.0 },
//                 { 14.0, 15.0, 16.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.IQR(m1, MatrixDimensions.Columns);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 6.0, 6.0, 6.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetIQRSingleRowsMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 5.0, 3.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.IQR(m1, MatrixDimensions.Auto);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 4.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetIQRTwoRowsMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 5.0, 3.0 },
//                 { 4.0, 2.0, 6.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.IQR(m1, MatrixDimensions.Columns);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 3.0, 3.0, 3.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         #endregion
//
//         #region Mode
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetModeColumnsRectangularMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 5.0, 3.0 },
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 2.0, 6.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Mode(m1, MatrixDimensions.Columns);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetModeRowsRectangularMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 3.0, 3.0 },
//                 { 4.0, 4.0, 6.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Mode(m1, MatrixDimensions.Rows);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 3.0 },
//                 { 4.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetModeColumnsVector()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 2.0, 4.0, 2.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Mode(m1);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 2.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetModeRowsVector()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 6.0 },
//                 { 2.0 },
//                 { 2.0 },
//                 { 3.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Mode(m1);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 2.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetModeWhereNoMultipleValues()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Mode(m1);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetModeWhereSeveralPossibilities()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 2.0, 2.0, 1.0, 1.0, 3.0, 3.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Mode(m1);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetModeWhereSingleElement()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 3.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Mode(m1);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 3.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         #endregion
//
//         #region Variance
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetVarianceColumnsRectangularMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Variance(m1, MatrixDimensions.Columns);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 4.5, 4.5, 4.5 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetVarianceRowsRectangularMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Variance(m1, MatrixDimensions.Rows);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0 },
//                 { 1.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetVarianceColumnsVector()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Variance(m1);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetVarianceRowsVector()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0 },
//                 { 2.0 },
//                 { 3.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Variance(m1);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//         #endregion
//
//         #region Standard Deviation
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetStandardDeviationColumnsRectangularMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 4.0 },
//                 { 2.0, 5.0 },
//                 { 3.0, 6.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.StandardDeviation(m1, MatrixDimensions.Columns);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0, 1.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetStandardDeviationRowsRectangularMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.StandardDeviation(m1, MatrixDimensions.Rows);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0 },
//                 { 1.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetStandardDeviationColumnsVector()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.StandardDeviation(m1);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetStandardDeviationColumnsLongerVector()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 2.0, 4.0, 4.0, 4.0, 5.0, 5.0, 5.0, 7.0, 9.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.StandardDeviation(m1);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 2.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Statistical Operations"), Test]
//         public void GetStandardDeviationRowsVector()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0 },
//                 { 2.0 },
//                 { 3.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.StandardDeviation(m1);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//         #endregion
//
//         #endregion
//         #endregion
//
//         #region Other Methods
//         #region Swap Rows
//         [Test]
//         public void SwapTwoExistingRows()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,]
//             {
//                 { 1.0, 2.0 },
//                 { 3.0, 4.0 },
//                 { 5.0, 6.0 }
//             });
//
//             m1.SwapRows(0, 1);
//
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 3.0, 4.0 },
//                 { 1.0, 2.0 },
//                 { 5.0, 6.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m1);
//         }
//
//
//         [Test]
//         [ExpectedException(typeof(IndexOutOfRangeException), "Rows do not exist.")]
//         public void SwapTwoNonExistentRows()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,]
//             {
//                 { 1.0, 2.0 },
//                 { 3.0, 4.0 },
//                 { 5.0, 6.0 }
//             });
//
//             m1.SwapRows(9, 10);
//         }
//         #endregion
//
//         #region Join
//         [Test]
//         public void JoinTwoValidColumnMatrices()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,]
//             {
//                 { 1.0, 2.0 },
//                 { 4.0, 5.0 },
//                 { 7.0, 8.0 }
//             });
//
//             MatrixDouble m2 = new MatrixDouble(new double[,]
//             {
//                 { 3.0 },
//                 { 6.0 },
//                 { 9.0 }
//             });
//
//             MatrixDouble m3 = MatrixDouble.Join(m1, m2, MatrixDimensions.Columns);
//
//             MatrixDouble expectedResult = new MatrixDouble(new double[,]
//             {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 },
//                 { 7.0, 8.0, 9.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m3);
//         }
//
//         [Test]
//         [ExpectedException(typeof(InvalidMatrixDimensionsException), "Cannot join two columns with differing row counts.")]
//         public void JoinTwoInvalidColumnMatrices()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,]
//             {
//                 { 1.0, 2.0 },
//                 { 7.0, 8.0 }
//             });
//
//             MatrixDouble m2 = new MatrixDouble(new double[,]
//             {
//                 { 3.0 },
//                 { 6.0 },
//                 { 9.0 }
//             });
//
//             MatrixDouble m3 = MatrixDouble.Join(m1, m2, MatrixDimensions.Columns);
//         }
//
//         [Test]
//         public void JoinTwoValidRowMatrices()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,]
//             {
//                 { 1.0, 2.0 },
//                 { 3.0, 4.0 }
//             });
//
//             MatrixDouble m2 = new MatrixDouble(new double[,]
//             {
//                 { 5.0, 6.0 },
//                 { 7.0, 8.0 },
//                 { 9.0, 10.0 }
//             });
//
//             MatrixDouble m3 = MatrixDouble.Join(m1, m2, MatrixDimensions.Rows);
//
//             MatrixDouble expectedResult = new MatrixDouble(new double[,]
//             {
//                 { 1.0, 2.0 },
//                 { 3.0, 4.0 },
//                 { 5.0, 6.0 },
//                 { 7.0, 8.0 },
//                 { 9.0, 10.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m3);
//         }
//
//         [Test]
//         [ExpectedException(typeof(InvalidMatrixDimensionsException), "Cannot join two rows with differing column counts.")]
//         public void JoinTwoInvalidRowMatrices()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,]
//             {
//                 { 1.0, 2.0 },
//                 { 7.0, 8.0 }
//             });
//
//             MatrixDouble m2 = new MatrixDouble(new double[,]
//             {
//                 { 3.0 },
//                 { 6.0 },
//                 { 9.0 }
//             });
//
//             MatrixDouble m3 = MatrixDouble.Join(m1, m2, MatrixDimensions.Rows);
//         }
//
//         [Test]
//         public void JoinTwoValidAutoColumnMatrices()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,]
//             {
//                 { 1.0, 2.0 },
//                 { 4.0, 5.0 },
//                 { 7.0, 8.0 }
//             });
//
//             MatrixDouble m2 = new MatrixDouble(new double[,]
//             {
//                 { 3.0 },
//                 { 6.0 },
//                 { 9.0 }
//             });
//
//             MatrixDouble m3 = MatrixDouble.Join(m1, m2, MatrixDimensions.Auto);
//
//             MatrixDouble expectedResult = new MatrixDouble(new double[,]
//             {
//                 { 1.0, 2.0, 3.0 },
//                 { 4.0, 5.0, 6.0 },
//                 { 7.0, 8.0, 9.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m3);
//         }
//
//         [Test]
//         public void JoinTwoValidAutoRowMatrices()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,]
//             {
//                 { 1.0, 2.0 },
//                 { 3.0, 4.0 }
//             });
//
//             MatrixDouble m2 = new MatrixDouble(new double[,]
//             {
//                 { 5.0, 6.0 },
//                 { 7.0, 8.0 },
//                 { 9.0, 10.0 }
//             });
//
//             MatrixDouble m3 = MatrixDouble.Join(m1, m2, MatrixDimensions.Auto);
//
//             MatrixDouble expectedResult = new MatrixDouble(new double[,]
//             {
//                 { 1.0, 2.0 },
//                 { 3.0, 4.0 },
//                 { 5.0, 6.0 },
//                 { 7.0, 8.0 },
//                 { 9.0, 10.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m3);
//         }
//
//         [Test]
//         [ExpectedException(typeof(InvalidMatrixDimensionsException), "Cannot join two matrices with differing rows and columns.")]
//         public void JoinTwoInvalidAutoMatrices()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,]
//             {
//                 { 1.0, 2.0 },
//                 { 7.0, 8.0 }
//             });
//
//             MatrixDouble m2 = new MatrixDouble(new double[,]
//             {
//                 { 3.0 },
//                 { 6.0 },
//                 { 9.0 }
//             });
//
//             MatrixDouble m3 = MatrixDouble.Join(m1, m2, MatrixDimensions.Auto);
//         }
//
//         #endregion
//
//         #region Add Identity Column
//         [Test]
//         public void AddDefaultIdentityColumn()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,]
//             {
//                 { 1.0, 2.0 },
//                 { 4.0, 5.0 },
//                 { 7.0, 8.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.AddIdentityColumn(m1);
//
//             MatrixDouble expectedResult = new MatrixDouble(new double[,]
//             {
//                 { 1.0, 1.0, 2.0 },
//                 { 1.0, 4.0, 5.0 },
//                 { 1.0, 7.0, 8.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [Test]
//         public void AddSpecificIdentityColumn()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,]
//             {
//                 { 1.0, 2.0 },
//                 { 4.0, 5.0 },
//                 { 7.0, 8.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.AddIdentityColumn(m1, 2.0);
//
//             MatrixDouble expectedResult = new MatrixDouble(new double[,]
//             {
//                 { 2.0, 1.0, 2.0 },
//                 { 2.0, 4.0, 5.0 },
//                 { 2.0, 7.0, 8.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m2);
//         }
//         #endregion
//
//         #region Extract row/column
//         [TestCategory("Matrix: Extract Rows/Columns"), Test]
//         public void ExtractValidMatrixRow()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0, 4.0 },
//                 { 5.0, 6.0, 7.0, 8.0 },
//                 { 9.0, 10.0, 11.0, 12.0 },
//                 { 13.0, 14.0, 15.0, 16.0 }
//             });
//             MatrixDouble m2 = m1.GetRow(2);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 9.0, 10.0, 11.0, 12.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Extract Rows/Columns"), Test]
//         public void ExtractValidMatrixColumn()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0, 4.0 },
//                 { 5.0, 6.0, 7.0, 8.0 },
//                 { 9.0, 10.0, 11.0, 12.0 },
//                 { 13.0, 14.0, 15.0, 16.0 }
//             });
//             MatrixDouble m2 = m1.GetColumn(2);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 3.0 }, { 7.0 }, { 11.0 }, { 15.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Extract Rows/Columns"), Test]
//         public void ExtractFirstMatrixRow()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0, 4.0 },
//                 { 5.0, 6.0, 7.0, 8.0 },
//                 { 9.0, 10.0, 11.0, 12.0 },
//                 { 13.0, 14.0, 15.0, 16.0 }
//             });
//             MatrixDouble m2 = m1.GetRow(0);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0, 4.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Extract Rows/Columns"), Test]
//         public void ExtractLastMatrixRow()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0, 4.0 },
//                 { 5.0, 6.0, 7.0, 8.0 },
//                 { 9.0, 10.0, 11.0, 12.0 },
//                 { 13.0, 14.0, 15.0, 16.0 }
//             });
//             MatrixDouble m2 = m1.GetRow(3);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 13.0, 14.0, 15.0, 16.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Extract Rows/Columns"), Test]
//         public void ExtractFirstMatrixColumn()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0, 4.0 },
//                 { 5.0, 6.0, 7.0, 8.0 },
//                 { 9.0, 10.0, 11.0, 12.0 },
//                 { 13.0, 14.0, 15.0, 16.0 }
//             });
//             MatrixDouble m2 = m1.GetColumn(0);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0 }, { 5.0 }, { 9.0 }, { 13.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Extract Rows/Columns"), Test]
//         public void ExtractLastMatrixColumn()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0, 4.0 },
//                 { 5.0, 6.0, 7.0, 8.0 },
//                 { 9.0, 10.0, 11.0, 12.0 },
//                 { 13.0, 14.0, 15.0, 16.0 }
//             });
//             MatrixDouble m2 = m1.GetColumn(3);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 4.0 }, { 8.0 }, { 12.0 }, { 16.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [TestCategory("Matrix: Extract Rows/Columns"), Test]
//         [ExpectedException(typeof(IndexOutOfRangeException), "Requested row is out of range.")]
//         public void ExtractInvalidMatrixRow()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0, 4.0 },
//                 { 5.0, 6.0, 7.0, 8.0 },
//                 { 9.0, 10.0, 11.0, 12.0 },
//                 { 13.0, 14.0, 15.0, 16.0 }
//             });
//             MatrixDouble m2 = m1.GetRow(10);
//         }
//
//         [TestCategory("Matrix: Extract Rows/Columns"), Test]
//         [ExpectedException(typeof(IndexOutOfRangeException), "Requested column is out of range.")]
//         public void ExtractInvalidMatrixColumn()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0, 4.0 },
//                 { 5.0, 6.0, 7.0, 8.0 },
//                 { 9.0, 10.0, 11.0, 12.0 },
//                 { 13.0, 14.0, 15.0, 16.0 }
//             });
//             MatrixDouble m2 = m1.GetColumn(10);
//         }
//
//         #endregion
//
//         #region Set Rows/Columns
//         [Test]
//         public void ReplaceRowFromASingleRowMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0, 4.0 },
//                 { 5.0, 6.0, 7.0, 8.0 },
//                 { 9.0, 10.0, 11.0, 12.0 },
//                 { 13.0, 14.0, 15.0, 16.0 }
//             });
//             MatrixDouble m2 = new MatrixDouble(new double[,] {
//                 { 0.0, 1.0, 2.0, 3.0 }
//             });
//             m1.SetRow(2, m2);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0, 4.0 },
//                 { 5.0, 6.0, 7.0, 8.0 },
//                 { 0.0, 1.0, 2.0, 3.0 },
//                 { 13.0, 14.0, 15.0, 16.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m1);
//         }
//         #endregion
//
//         #region Remove Rows/Columns
//         [Test]
//         public void RemoveDefaultColumnFromMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0, 4.0 },
//                 { 5.0, 6.0, 7.0, 8.0 },
//                 { 9.0, 10.0, 11.0, 12.0 },
//                 { 13.0, 14.0, 15.0, 16.0 }
//             });
//             MatrixDouble m2 = m1.RemoveColumn();
//
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 2.0, 3.0, 4.0 },
//                 { 6.0, 7.0, 8.0 },
//                 { 10.0, 11.0, 12.0 },
//                 { 14.0, 15.0, 16.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [Test]
//         public void RemoveArbitraryColumnFromMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0, 4.0 },
//                 { 5.0, 6.0, 7.0, 8.0 },
//                 { 9.0, 10.0, 11.0, 12.0 },
//                 { 13.0, 14.0, 15.0, 16.0 }
//             });
//             MatrixDouble m2 = m1.RemoveColumn(2);
//
//             MatrixDouble expectedResult = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 4.0 },
//                 { 5.0, 6.0, 8.0 },
//                 { 9.0, 10.0, 12.0 },
//                 { 13.0, 14.0, 16.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [Test]
//         [ExpectedException(typeof(IndexOutOfRangeException), "Column out of range.")]
//         public void RemoveOutOfRangeColumnFromMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0, 2.0, 3.0, 4.0 },
//                 { 5.0, 6.0, 7.0, 8.0 },
//                 { 9.0, 10.0, 11.0, 12.0 },
//                 { 13.0, 14.0, 15.0, 16.0 }
//             });
//             MatrixDouble m2 = m1.RemoveColumn(10);
//         }
//
//         [Test]
//         [ExpectedException(typeof(InvalidMatrixDimensionsException), "Cannot remove only column.")]
//         public void RemoveOnlyColumnFromMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,] {
//                 { 1.0 },
//                 { 5.0 },
//                 { 9.0 },
//                 { 13.0 }
//             });
//             MatrixDouble m2 = m1.RemoveColumn(0);
//         }
//
//         #endregion
//
//         #region Reshape Matrix
//         [Test]
//         [ExpectedException(typeof(InvalidMatrixDimensionsException), "Reshaping failed due to invalid dimensions.")]
//         public void ReshapeInvalidMatrixDimensions()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,]
//             {
//                 { 1.0 },
//                 { 2.0 },
//                 { 3.0 },
//                 { 4.0 },
//                 { 5.0 },
//                 { 6.0 },
//                 { 7.0 },
//                 { 8.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Reshape(m1, 0, 3, 3);
//         }
//
//         [Test]
//         public void ReshapeFullMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,]
//             {
//                 { 1.0 },
//                 { 2.0 },
//                 { 3.0 },
//                 { 4.0 },
//                 { 5.0 },
//                 { 6.0 },
//                 { 7.0 },
//                 { 8.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Reshape(m1, 0, 2, 4);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,]
//             {
//                 { 1.0, 3.0, 5.0, 7.0 },
//                 { 2.0, 4.0, 6.0, 8.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [Test]
//         public void ReshapeStartOfMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,]
//             {
//                 { 1.0 },
//                 { 2.0 },
//                 { 3.0 },
//                 { 4.0 },
//                 { 5.0 },
//                 { 6.0 },
//                 { 7.0 },
//                 { 8.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Reshape(m1, 0, 2, 2);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,]
//             {
//                 { 1.0, 3.0 },
//                 { 2.0, 4.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [Test]
//         public void ReshapeEndOfMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,]
//             {
//                 { 1.0 },
//                 { 2.0 },
//                 { 3.0 },
//                 { 4.0 },
//                 { 5.0 },
//                 { 6.0 },
//                 { 7.0 },
//                 { 8.0 }
//             });
//             MatrixDouble m2 = MatrixDouble.Reshape(m1, 4, 2, 2);
//             MatrixDouble expectedResult = new MatrixDouble(new double[,]
//             {
//                 { 5.0, 7.0 },
//                 { 6.0, 8.0 }
//             });
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         #endregion
//
//         #region Expand Polynomials
//         [Test]
//         public void ExpandThirdDegreePolynomialsTwoColumnMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,]
//             {
//                 { 1.0, 3.0 },
//                 { 2.0, 4.0 }
//             });
//             MatrixDouble m2 = m1.ExpandPolynomials(0, 1, 3);
//
//             MatrixDouble expectedResult = new MatrixDouble(new double[,]
//             {
//                 { 1.0, 1.0, 3.0, 1.0, 3.0, 9.0, 1.0, 3.0, 9.0, 27.0 },
//                 { 1.0, 2.0, 4.0, 4.0, 8.0, 16.0, 8.0, 16.0, 32.0, 64.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [Test]
//         public void ExpandThirdDegreePolynomialsStartOfThreeColumnMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,]
//             {
//                 { 1.0, 3.0, 10.0 },
//                 { 2.0, 4.0, 11.0 }
//             });
//             MatrixDouble m2 = m1.ExpandPolynomials(0, 1, 3);
//
//             MatrixDouble expectedResult = new MatrixDouble(new double[,]
//             {
//                 { 1.0, 1.0, 3.0, 1.0, 3.0, 9.0, 1.0, 3.0, 9.0, 27.0, 10.0 },
//                 { 1.0, 2.0, 4.0, 4.0, 8.0, 16.0, 8.0, 16.0, 32.0, 64.0, 11.0 }
//             });
//
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//
//         [Test]
//         public void ExpandThirdDegreePolynomialsEndOfThreeColumnMatrix()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,]
//             {
//                 { 10.0, 1.0, 3.0 },
//                 { 11.0, 2.0, 4.0 }
//             });
//             MatrixDouble m2 = m1.ExpandPolynomials(1, 2, 3);
//
//             MatrixDouble expectedResult = new MatrixDouble(new double[,]
//             {
//                 { 10.0, 1.0, 1.0, 3.0, 1.0, 3.0, 9.0, 1.0, 3.0, 9.0, 27.0 },
//                 { 11.0, 1.0, 2.0, 4.0, 4.0, 8.0, 16.0, 8.0, 16.0, 32.0, 64.0 }
//             });
//
//
//             Assert.AreEqual(expectedResult, m2);
//         }
//
//         [Test]
//         [ExpectedException(typeof(IndexOutOfRangeException), "Column 1 out of range")]
//         public void ExpandThirdDegreepolynomialsColumn1OutOfRange()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,]
//             {
//                 { 10.0, 1.0, 3.0 },
//                 { 11.0, 2.0, 4.0 }
//             });
//             MatrixDouble m2 = m1.ExpandPolynomials(5, 2, 3);
//         }
//
//         [Test]
//         [ExpectedException(typeof(IndexOutOfRangeException), "Column 2 out of range")]
//         public void ExpandThirdDegreepolynomialsColumn2OutOfRange()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,]
//             {
//                 { 10.0, 1.0, 3.0 },
//                 { 11.0, 2.0, 4.0 }
//             });
//             MatrixDouble m2 = m1.ExpandPolynomials(1, 5, 3);
//         }
//
//         [Test]
//         [ExpectedException(typeof(IndexOutOfRangeException), "Both Columns out of range")]
//         public void ExpandThirdDegreepolynomialsBothColumnsOutOfRange()
//         {
//             MatrixDouble m1 = new MatrixDouble(new double[,]
//             {
//                 { 10.0, 1.0, 3.0 },
//                 { 11.0, 2.0, 4.0 }
//             });
//             MatrixDouble m2 = m1.ExpandPolynomials(6, 5, 3);
//         }
//         #endregion
//         #endregion
//     }
// }
