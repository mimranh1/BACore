﻿using System.Runtime.CompilerServices;
using BA.BACore;
using UnityEngine;

namespace Tests
{
    public class ReverberationDataBuilder
    {
        private float[] _reverberation;
        private int _filterLength=44100;
        private float _factor=1f;
        private int _delaySamples=0;
        
        public ReverberationDataBuilder WithReverberation(float[] reverberation)
        {
            _reverberation = reverberation;
            return this;
        }
        public ReverberationDataBuilder WithDelaySamples(int delaySamples)
        {
            _delaySamples = delaySamples;
            return this;
        }
        public ReverberationDataBuilder WithFilterLength(int filterLength)
        {
            _filterLength = filterLength;
            return this;
        }
        public ReverberationDataBuilder WithFactor(float factor)
        {
            _factor = factor;
            return this;
        }

        private ReverberationData Build()
        {
            var reverberationData = new ReverberationData();
            reverberationData.InitDataArray(_reverberation, _filterLength, _factor, _delaySamples);
            return reverberationData;
        }

        public static implicit operator ReverberationData(ReverberationDataBuilder builder)
        {
            return builder.Build();
        }
    }
}