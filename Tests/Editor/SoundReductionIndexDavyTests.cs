﻿using System.Collections;
using System.Collections.Generic;
using BA.BACore;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class SoundReductionIndexDavyTests
    {
        private const float Fc = 2047.73909249848f;
        private const float Length1 = 1.8f;
        private const float Length2 = 1.2f;
        private const float V = 0.22f;
        private const float G = 2.6639e+10f;
        private const float E = 6.5000e+10f;
        private const float Rho0 = 1.29f;
        private const float Rho = 2500f;
        private const float Thickness = 0.006f;
        private const float Mass = 15f;
        private const float C0 = 340f;
        private const float EtaInit = 0.05f;
        private const float IncAngleRad = Mathf.PI/4;
        private static TransferFunction F => BaSettings.Instance.ThirdOctaveFrequencyBandShort;

        private static TransferFunction Omega => 2f * Mathf.PI * F;

        // [TestCase(C0, Rho0, EtaInit, Mass, V, G, E, Rho, Thickness,
        //     Length1, Length2, Fc, new[]
        //     {
        //         0.0172412274630184, 0.0153701707193820, 0.0128137881316527, 0.0101276493817788, 0.00756572977915997,
        //         0.00523317902858184, 0.00366242543337676, 0.00269058605457023, 0.00191574435533043, 0.00132701481316782,
        //         0.000931207085436650, 0.000639466297796609, 0.000430236223481023, 0.000295364679304792,
        //         0.000201786608769254, 0.000131739678252937, 8.92606350160769e-05, 0.00158165821730783,
        //         0.000451905913277203, 0.000165411243938636, 7.18654345388320e-05
        //     })]
        [TestCase(C0, Rho0, EtaInit, 20f, 0.2f,27322404371.5847f, 65573770491.8033f, 2500f, 0.008f,
            6.5f, 3f, 1535.80431937386f, new[]
            {
                0.582715847560751f, 0.519646065678519f, 0.461688008657858f, 0.413466797405491f, 0.370341517767787f,
                0.327921658093948f, 0.293835944576311f, 0.263352456460814f, 0.235173098775519f, 0.209279255114715f,
                0.187740585512957f, 0.167840731813497f, 0.149581003571168f, 0.134462399908905f, 0.121294055136648f,
                0.115724370997428f, 0.103210947641903f, 0.0910950630832299f, 0.0807898074062449f, 0.0717330611174826f,
                0.0643764469948250f
            }, new[]
            {
                0.0303864813199875, 0.0213713514556558, 0.0151626763662007, 0.0111505183072127, 0.00809844646848955,
                0.00563377276021807, 0.00404117466904667, 0.00289734429328257, 0.00205967268681955, 0.00146054213163842,
                0.00107590117802976, 0.000806077654035721, 0.000631818437383711, 0.000558554421704963,
                0.000646485067926536, 0.00172960044878932, 0.000536066459272132, 0.000198774087950833,
                8.67202148092068e-05, 4.04337788932921e-05, 2.08075000700359e-05
            })]
        public void CalcDavyCompleteDiffuseTest(float c0, float rho0, float etaInit, float m, float v,
            float shearModulus, float youngsModulus, float rho, float h, float length1, float length2, float fc,
            float[] eta,
            double[] expected)
        {
            var ss1 = SoundReductionIndexDavy.CalcDavyComplete(F, c0, rho0, etaInit, m, v, shearModulus, youngsModulus,
                rho, h, length1, length2, fc, float.PositiveInfinity, new TransferFunction(eta));
            //(omega, new TransferFunction(sigma), new TransferFunction(eta), m,rho,c0,f,fc);
            AreEqual(expected, ss1.Tf, 1e-3f);
        }

        [Test]
        // [TestCase(C0, Rho0, EtaInit, Mass, V, G, E, Rho, Thickness,
        //     Length1, Length2, Fc, IncAngleRad, new[]
        //     {
        //         0.582715847560751f, 0.519646065678519f, 0.461688008657858f, 0.413466797405491f, 0.370341517767787f,
        //         0.327921658093948f, 0.293835944576311f, 0.263352456460814f, 0.235173098775519f, 0.209279255114715f,
        //         0.187740585512957f, 0.167840731813497f, 0.149581003571168f, 0.134462399908905f, 0.121294055136648f,
        //         0.115724370997428f, 0.103210947641903f, 0.0910950630832299f, 0.0807898074062449f, 0.0717330611174826f,
        //         0.0643764469948250f
        //     }, new[]
        //     {
        //         0.00167115379457845, 0.00201334844848369, 0.00220547806387658, 0.00226430969304550, 0.00247917562100555,
        //         0.00264252185116509, 0.00255842064731583, 0.00194857664392276, 0.00138849046504198,
        //         0.000936518412573257, 0.000630768883762759, 0.000411182262004458, 0.000260800623563561,
        //         0.000169154894344042, 0.000109192890535981, 6.70451741677266e-05, 4.30539688769616e-05,
        //         0.000126025330382570, 0.000116592763638549, 0.00222231870977197, 3.02677618613152e-05
        //     })]
        [TestCase(C0, Rho0, EtaInit, 20f, 0.2f, 2.7322e+10f, 6.5574e+10f, 2500f, 0.0080f,
            1.6250f, 1.5f, 1.5358e+03f, 1.1368f,
            new[]
            {
                0.582715847560751f, 0.519646065678519f, 0.461688008657858f, 0.413466797405491f, 0.370341517767787f,
                0.327921658093948f, 0.293835944576311f, 0.263352456460814f, 0.235173098775519f, 0.209279255114715f,
                0.187740585512957f, 0.167840731813497f, 0.149581003571168f, 0.134462399908905f, 0.121294055136648f,
                0.115724370997428f, 0.103210947641903f, 0.0910950630832299f, 0.0807898074062449f, 0.0717330611174826f,
                0.0643764469948250f
            }, new[]
            {
                0.110746924845188, 0.0698869902333282, 0.0434542748600913, 0.0279028834036062, 0.0179375661831054,
                0.0110229145934823, 0.00711536590303907, 0.00460856749872416, 0.00295485534158747, 0.00188361707625106,
                0.00125453095267349, 0.000845089403619130, 0.000593143086818262, 0.000475008424683895,
                0.000491837925349749, 0.00112389967398305, 0.00161089189760835, 4.34652116817206e-05,
                5.12547971673738e-06, 9.64512782328335e-07, 2.56094954256279e-07
            })]
        public void CalcDavyCompleteAngle45Test(float c0, float rho0, float etaInit, float m, float v,
            float shearModulus, float youngsModulus, float rho, float h, float length1, float length2, float fc,
            float incAngleRad, float[] eta,
            double[] expected)
        {
            var ss1 = SoundReductionIndexDavy.CalcDavyComplete(F, c0, rho0, etaInit, m, v, shearModulus, youngsModulus,
                rho, h, length1, length2, fc, incAngleRad, new TransferFunction(eta));

            AreEqual(expected, ss1.Tf, 1e-3f);
        }

        [Test]
        public void EtaDavyTest() 
        {
            var expected = new[]
            {
                0.0543738563784735f, 0.0538965409588580f, 0.0534578370786079f, 0.0530927835051546f, 0.0527662696628863f,
                0.0524450600465220f, 0.0521869281892367f, 0.0519560480372176f, 0.0517425860922238f, 0.0515463917525773f,
                0.0513831348314432f, 0.0512321944426128f, 0.0510934640946184f, 0.0509780240186088f, 0.0508747712756947f,
                0.0507731958762887f, 0.0506915674157216f, 0.0506185567010309f, 0.0505510541070359f, 0.0504890120093044f,
                0.0504373856378474
            };
            var ss = SoundReductionIndexDavy.EtaDavy(F, EtaInit, Mass);
            AreEqual(expected, ss.Tf, 1e-4f);
        }

        [Test]
        public void SigmaObliqueDiffuseTest()
        {
            var thetaC = SoundReductionIndexDavy.GetThetaC(F, Fc);
            var ss1 = SoundReductionIndexDavy.SigmaOblique(F, Fc, C0, Length1, Length2, thetaC);
            var expected = new[]
            {
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2.34361429052188, 1.68804219846429, 1.43031025500896,
                1.30081373071840
            };
            AreEqual(expected, ss1.Tf, 1e-2f);
        }
        [Test]
        public void SigmaObliqueAngle45Test()
        {
            var ss1 = SoundReductionIndexDavy.SigmaOblique(F, Fc, C0, Length1, Length2, IncAngleRad);
            var expected = new[]
            {
                0.219609295352078, 0.303719394957899, 0.403658277532981, 0.511258353734861, 0.668707453182429,
                0.883693744394433, 1.08689721446490, 1.18569027941667, 1.26111447076033, 1.31519653464233,
                1.34920405979173, 1.37255652757740, 1.38808737467964, 1.39738085393808, 1.40339426929901,
                1.40759024606840, 1.40996748095533, 1.41149312981583, 1.41249879113828, 1.41314964626606,
                1.41353247160558
            };
            AreEqual(expected, ss1.Tf, 1e-4f);
        }

        [Test]
        public void GetDavyAbcTest()
        {
            var ss = SoundReductionIndexDavy.GetDavyAbc(2f * Mathf.PI * F, V, G, E, Rho, Thickness);
            var expected = new[]
            {
                1.00021467913380f, 1.00027050137211f, 1.00034350321166f, 1.00042939284875f, 1.00053676268084f,
                1.00068709498392f, 1.00085892410764f, 1.00107374169436f, 1.00135305638703f, 1.00171840254043f,
                1.00214835005687f, 1.00270748980124f, 1.00343902786375f, 1.00430017749951f, 1.00537740194613f,
                1.00688699080802f, 1.00861435046624f, 1.01077673910256f, 1.01359317516144f, 1.01728537347216f,
                1.02164257100480
            };
            AreEqual(expected, ss.Tf, 1e-4f);
        }

        [Test]
        public void TauResAngleTest()
        {
            var sigma = new[]
            {
                0.1713f, 0.2173f, 0.2657f, 0.3131f, 0.3710f, 0.4404f, 0.5092f, 0.5854f, 0.6745f, 0.7805f, 0.8964f,
                1.0396f, 1.2222f, 1.4398f, 1.7301f, 2.2075f, 2.9668f, 2.3436f, 1.6880f, 1.4303f, 1.3008f
            };
            var eta = new[]
            {
                0.0544f, 0.0539f, 0.0535f, 0.0531f, 0.0528f, 0.0524f, 0.0522f, 0.0520f, 0.0517f, 0.0515f, 0.0514f,
                0.0512f, 0.0511f, 0.0510f, 0.0509f, 0.0508f, 0.0507f, 0.0506f, 0.0506f, 0.0505f, 0.0504f
            };
            var ss1 = SoundReductionIndexDavy.TauResAngle(Omega, new TransferFunction(sigma), new TransferFunction(eta), Mass,
                Rho0, C0, F, Fc,Mathf.PI/4f);
            var expected = new[]
            {
                0.000260104026881198, 0.000265360198832389, 0.000248076904286439, 0.000222617840988789,
                0.000202658812281556, 0.000177418202618941, 0.000154930667968312, 0.000134501567532144,
                0.000116351901611111, 0.000101122547837102, 9.01820323822425e-05, 8.22336392306820e-05,
                7.79402046492921e-05, 7.84495017712068e-05, 8.57667988453498e-05, 0.000110733944623561,
                0.000181317188061555, 0.000124681668569524, 0.000115029152223701, 0.00218455781211800,
                2.96265667860204e-05
            };
            AreEqual(expected, ss1.Tf, 1e-4f);
        }
        [Test]
        public void TauResTest()
        {
           
            var sigma = new[]
            {
                0.1713f, 0.2173f, 0.2657f, 0.3131f, 0.3710f, 0.4404f, 0.5092f, 0.5854f, 0.6745f, 0.7805f, 0.8964f,
                1.0396f, 1.2222f, 1.4398f, 1.7301f, 2.2075f, 2.9668f, 2.3436f, 1.6880f, 1.4303f, 1.3008f
            };
            var eta = new[]
            {
                0.0544f, 0.0539f, 0.0535f, 0.0531f, 0.0528f, 0.0524f, 0.0522f, 0.0520f, 0.0517f, 0.0515f, 0.0514f,
                0.0512f, 0.0511f, 0.0510f, 0.0509f, 0.0508f, 0.0507f, 0.0506f, 0.0506f, 0.0505f, 0.0504f
            };
            var ss1 = SoundReductionIndexDavy.TauRes(Omega, new TransferFunction(sigma), new TransferFunction(eta), Mass,
                Rho0, C0, F, Fc);
            var expected = new[]
            {
                0.000260143589273468, 0.000265424696891539, 0.000248174986084704, 0.000222756802403147,
                0.000202859068962820, 0.000177710810729426, 0.000155338615211548, 0.000135070385960772,
                0.000117162458879387, 0.000102317101606041, 9.19540096830953e-05, 8.50335656505472e-05,
                8.28003324241827e-05, 8.75447843627096e-05, 0.000106134645501799, 0.000186879972268449,
                0.00138081879498319, 0.00156479483165802, 0.000445845457873399, 0.000162600631299810,
                7.03430305064046e-05
            };
            AreEqual(expected, ss1.Tf, 1e-4f);
        }

        [Test]
        public void TauRes2Test()
        {
          
            var sigma = new[]
            {
                0.1713f, 0.2173f, 0.2657f, 0.3131f, 0.3710f, 0.4404f, 0.5092f, 0.5854f, 0.6745f, 0.7805f, 0.8964f,
                1.0396f, 1.2222f, 1.4398f, 1.7301f, 2.2075f, 2.9668f, 2.3436f, 1.6880f, 1.4303f, 1.3008f
            };
            var eta = new[]
            {
                0.0544f, 0.0539f, 0.0535f, 0.0531f, 0.0528f, 0.0524f, 0.0522f, 0.0520f, 0.0517f, 0.0515f, 0.0514f,
                0.0512f, 0.0511f, 0.0510f, 0.0509f, 0.0508f, 0.0507f, 0.0506f, 0.0506f, 0.0505f, 0.0504f
            };
            var ss1 = SoundReductionIndexDavy.TauRes2(Omega, new TransferFunction(sigma), new TransferFunction(eta), Mass,
                Rho0, C0, F, Fc);
            var expected = new[]
            {
                0.000260143589273468, 0.000265424696891539, 0.000248174986084704, 0.000222756802403147,
                0.000202859068962820, 0.000177710810729426, 0.000155338615211548, 0.000135070385960772,
                0.000117162458879387, 0.000102317101606041, 9.19540096830953e-05, 8.50335656505472e-05,
                8.28003324241827e-05, 8.75447843627096e-05, 0.000106134645501799, 0.000186879972268449,
                0.00138081879498319, 0.00156479483165802, 0.000445845457873399, 0.000162600631299810,
                7.03430305064046e-05
            };
            AreEqual(expected, ss1.Tf, 1e-4f);
        }


        private void AreEqual(double[] expected, float[] actural, float delta)
        {
            Assert.That(actural, Is.EqualTo(expected).AsCollection.Within(delta));       
        }
        private void AreEqual(float[] expected, float[] actural, float delta)
        {
            Assert.That(actural, Is.EqualTo(expected).AsCollection.Within(delta));
        }
    }
}
