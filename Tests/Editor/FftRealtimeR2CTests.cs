﻿using BA.BACore;
using NUnit.Framework;

namespace Tests
{
    public class FftRealtimeR2CTests
    {
        [TestCase(new[] {1f, 0f, 0f, 0f}, new[] {1f, 0f, 1f, 0f, 1f, 0f, 1f, 0f})]
        [TestCase(new[] {1f, 0f, 0f}, new[] {1f, 0f, 1f, 0f, 1f, 0f})]
        [TestCase(new[] {0f, 1f, 0f},
            new[] {1f, 0f, -0.5f, -0.86603f, -0.5f, 0.86603f})]
        [TestCase(new[] {0f, 0f, -1f, 0f},
            new[] {-1f, 0f, 1f, 0f, -1f, 0f, 1f, 0f})]
        [TestCase(new[] {0f, 0f, 1f, 0f},
            new[] {1f, 0f, -1f, 0f, 1f, 0f, -1f, 0f})]
        [TestCase(new[] {0f, 2f, 1f, -2f},
            new[] {1f, 0f, -1f, -4f, 1f, 0f, -1f, 4f})]
        [TestCase(new[] {1f, 0f}, new[] {1f, 0f, 1f, 0f})]
        [TestCase(new[] {1f}, new[] {1f, 0f})]
        [TestCase(new[] {1f, 2f, 3f, 4f, 5f, 6f, 7f, 8f, 9f, 10f},
            new[]
            {
                55f, 0f, -5f, 15.3884f, -5f, 6.8819f, -5f, 3.6327f, -5f, 1.6246f, -5f, 0f, -5f, -1.6246f, -5f, -3.6327f,
                -5f, -6.8819f, -5f, -15.3884f
            })]
        public void FftRealToComplexWithCopyRun(float[] input, float[] expected)
        {
            var fft = new FftRealtimeR2C(input.Length);
            var actual = new float[input.Length * 2];
            fft.RunFftReal(input, actual);
            Assert.That(actual, Is.EqualTo(expected).AsCollection.Within(1e-4));
        }
        
        [TestCase(new[] {1f, 0f, 0f, 0f}, new[] {1f, 0f, 1f, 0f, 1f, 0f, 1f, 0f})]
        [TestCase(new[] {1f, 0f, 0f}, new[] {1f, 0f, 1f, 0f, 1f, 0f})]
        [TestCase(new[] {0f, 1f, 0f},
            new[] {1f, 0f, -0.5f, -0.86603f, -0.5f, 0.86603f})]
        [TestCase(new[] {0f, 0f, -1f, 0f},
            new[] {-1f, 0f, 1f, 0f, -1f, 0f, 1f, 0f})]
        [TestCase(new[] {0f, 0f, 1f, 0f},
            new[] {1f, 0f, -1f, 0f, 1f, 0f, -1f, 0f})]
        [TestCase(new[] {0f, 2f, 1f, -2f},
            new[] {1f, 0f, -1f, -4f, 1f, 0f, -1f, 4f})]
        [TestCase(new[] {1f, 0f}, new[] {1f, 0f, 1f, 0f})]
        [TestCase(new[] {1f}, new[] {1f, 0f})]
        [TestCase(new[] {1f, 2f, 3f, 4f, 5f, 6f, 7f, 8f, 9f, 10f},
            new[]
            {
                55f, 0f, -5f, 15.3884f, -5f, 6.8819f, -5f, 3.6327f, -5f, 1.6246f, -5f, 0f, -5f, -1.6246f, -5f, -3.6327f,
                -5f, -6.8819f, -5f, -15.3884f
            })]
        public void FftComplexToComplexWithCopyRun(float[] input, float[] expected)
        {
            var fft = new FftRealtimeR2C(input.Length);
            var actual = new float[input.Length * 2];
            var inComplex = new float[input.Length * 2];
            for (var i = 0; i < input.Length; i++)
            {
                inComplex[2 * i] = input[i];
            }
            fft.RunFft(inComplex, actual);
            Assert.That(actual, Is.EqualTo(expected).AsCollection.Within(1e-4));
        }


    }
}